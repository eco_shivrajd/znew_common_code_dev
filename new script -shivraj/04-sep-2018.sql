--for tbl_units

ALTER TABLE `tbl_units` 
		ADD `isdeleted` TINYINT NOT NULL DEFAULT '0' AFTER `unitname`, 
		ADD `deleted_on` DATETIME NOT NULL AFTER `isdeleted`;
		
		
		
CREATE TABLE `tbl_route_by_map` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `tbl_route_by_map`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tbl_route_by_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
 
CREATE TABLE `tbl_route_details_by_map` (
  `id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `lattitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `tbl_route_details_by_map`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tbl_route_details_by_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
  
CREATE  VIEW `tbl_shop_view`  AS  select `sp`.`id` AS `id`,`sp`.`status` AS `status`,`sp`.`status_seen_log` 
AS `status_seen_log`,`u`.`firstname` AS `sstockist`,`u1`.`firstname` AS `stockist`,`sp`.`name` AS `shop_name`,`sp`.`contact_person` 
AS `contact_person`,`sp`.`mobile` AS `mobile`,`c`.`name` AS `cityname`,`s`.`name` AS `statename`,`su`.`suburbnm` 
AS `suburbnm`,`sub`.`subarea_name` AS `subarea_name`,`sp`.`billing_name` 
AS `billing_name`,`sp`.`bank_name` AS `bank_name`,`sp`.`bank_acc_name` AS `bank_acc_name`,`sp`.`bank_acc_no` 
AS `bank_acc_no`,`sp`.`bank_b_name` AS `bank_b_name`,`sp`.`bank_ifsc` AS `bank_ifsc`,`sp`.`added_on` AS `added_on`,
`sp`.`stockist_id` AS `stockist_id` ,`sp`.`sstockist_id` AS `sstockist_id` ,
 `sp`.`state` AS `state_id`,`sp`.`city` AS `city_id` 
from ((((((`tbl_shops` `sp` left join `tbl_user` `u` on((`u`.`id` = `sp`.`sstockist_id`))) 
left join `tbl_user` `u1` on((`u1`.`id` = `sp`.`stockist_id`))) 
left join `tbl_state` `s` on((`sp`.`state` = `s`.`id`))) 
left join `tbl_city` `c` on((`sp`.`city` = `c`.`id`))) 
left join `tbl_area` `su` on((`sp`.`suburbid` = `su`.`id`))) 
left join `tbl_subarea` `sub` on((`sp`.`subarea_id` = `sub`.`subarea_id`))) 
where (`sp`.`isdeleted` <> '1') order by `sp`.`id` desc ;

ALTER TABLE `tbl_user` CHANGE `user_type` `user_type` VARCHAR(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;


CREATE TABLE `tbl_usertype` (
  `id` int(11) NOT NULL,
  `user_type` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `isdeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdon` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `tbl_usertype` (`id`, `user_type`, `isdeleted`, `createdon`) VALUES
(1, 'Superstockist', 0, '0000-00-00 00:00:00'),
(2, 'Stockist', 0, '0000-00-00 00:00:00'),
(3, 'Distributor', 0, '0000-00-00 00:00:00'),
(4, 'SalesPerson', 0, '0000-00-00 00:00:00'),
(5, 'DeliveryPerson', 0, '0000-00-00 00:00:00'),
(6, 'Accountant', 0, '0000-00-00 00:00:00'),
(7, 'DeliveryChannelPerson', 0, '0000-00-00 00:00:00'),
(8, 'Substockist', 0, '0000-00-00 00:00:00'),
(9, 'Subsuperstockist', 0, '0000-00-00 00:00:00'),
(10, 'Shopkeeper', 0, '0000-00-00 00:00:00');

ALTER TABLE `tbl_usertype`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tbl_usertype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
  
ALTER TABLE `tbl_usertype` ADD `ischecked` TINYINT NOT NULL DEFAULT '0' AFTER `user_type`, ADD `margin` FLOAT(10,2) NOT NULL AFTER `ischecked`;
ALTER TABLE `tbl_userlevel` ADD `ischecked` TINYINT NOT NULL DEFAULT '0' AFTER `margin`, ADD `isdeleted` TINYINT NOT NULL DEFAULT '0' AFTER `ischecked`;

ALTER TABLE `tbl_userlevel` ADD `usertypelabel` VARCHAR(128) NOT NULL AFTER `usertype`;

ALTER TABLE `tbl_userlevel` ADD `external_level_id` INT NOT NULL AFTER `id`;

--new tables
--tbl_action_add
--tbl_action_delete
--tbl_action_edit
--tbl_action_view

ALTER TABLE `tbl_userConf` ADD `user_role` VARCHAR(128) NOT NULL AFTER `user_type`, ADD `depth` INT NOT NULL AFTER `user_role`, ADD `parent_ids` VARCHAR(255) NOT NULL AFTER `depth`;
ALTER TABLE `tbl_userConf` ADD `profile_id` INT NOT NULL AFTER `user_role`;

update tbl_userConf set parent_ids = replace(parent_ids, ' ', '');

update tbl_userConf set parent_ids = replace(parent_ids, ' ', '');


CREATE  VIEW `tbl_user_view`  AS  select `u`.`id` AS `id`,`u`.`external_id` 
			AS `external_id`,`u`.`user_role` AS `user_role`,`u`.`reset_key` 
			AS `reset_key`,`u`.`user_status` AS `user_status`,`u`.`firstname` 
			AS `firstname`,`u`.`username` AS `username`,`u`.`pwd` AS `pwd`,`u`.`user_type` 
			AS `user_type`,`u`.`address` AS `address`,`c`.`name` AS `cityname`,`u`.`city` 
			AS `city_id`,`u`.`state` AS `state_id`,`s`.`name` AS `statename`,`u`.`suburb_ids` 
			AS `suburb_ids`,(select group_concat(`su`.`suburbnm` order by `su`.`id` ASC separator ',') 
			from (`tbl_user` `innerarea` join `tbl_area` `su` 
			on((find_in_set(`su`.`id`,`innerarea`.`suburb_ids`) > 0))) 
			where (`innerarea`.`id` = `u`.`id`) group by `innerarea`.`id`) 
			AS `suburbnm`,`u`.`subarea_ids` AS `subarea_ids`,
			(select group_concat(`sub`.`subarea_name` order by `sub`.`subarea_id` ASC separator ',') 
			from (`tbl_user` `inneru` join `tbl_subarea` `sub` 
			on((find_in_set(`sub`.`subarea_id`,`inneru`.`subarea_ids`) > 0))) 
			where (`inneru`.`id` = `u`.`id`) group by `inneru`.`id`) 
			AS `subarea_name`,`u`.`mobile` AS `mobile`,`u`.`email` 
			AS `email`,`u`.`parent_ids` AS `parent_ids`,
			(select group_concat(`inneru1`.`firstname` order by `inneru1`.`id` ASC separator ',') 
			from (`tbl_user` `inneru2` join `tbl_user` `inneru1` 
			on((find_in_set(`inneru1`.`id`,`inneru2`.`parent_ids`) > 0))) 
			where (`inneru2`.`id` = `u`.`id`) group by `inneru2`.`id`) 
			AS `parent_names`,(select group_concat(`inner_usertype`.`user_type` 
			order by `inner_usertype`.`id` ASC separator ',') 
			from (`tbl_user` `inneru123` join `tbl_user` `inner_usertype` 
			on((find_in_set(`inner_usertype`.`id`,`inneru123`.`parent_ids`) > 0))) 
			where (`inneru123`.`id` = `u`.`id`) group by `inneru123`.`id`) 
			AS `parent_usertypes`,`u`.`office_phone_no` AS `office_phone_no`,`u`.`accname` 
			AS `accname`,`u`.`accno` AS `accno`,`u`.`bank_name` AS `bank_name`,`u`.`accbrnm` 
			AS `accbrnm`,`u`.`accifsc` AS `accifsc`,`u`.`gst_number_sss` AS `gst_number_sss`,`u`.`latitude` 
			AS `latitude`,`u`.`longitude` AS `longitude`,`u`.`is_default_user` 
			AS `is_default_user`,`u`.`is_common_user` AS `is_common_user`,`u`.`added_on` AS `added_on` 
			from ((`tbl_user` `u` left join `tbl_state` `s` on((`u`.`state` = `s`.`id`))) 
			left join `tbl_city` `c` on((`u`.`city` = `c`.`id`))) 
			where ((`u`.`isdeleted` <> '1') 
			and (`u`.`is_common_user` <> '1')) group by `u`.`id` order by `u`.`id` desc ;

					
ALTER TABLE `tbl_users` ADD `user_level` VARCHAR(255) NOT NULL AFTER `level`;

UPDATE `tbl_user_tree` SET `level` = 'level2' WHERE `tbl_user_tree`.`depth` = 2;
ALTER TABLE `tbl_user_tree` ADD `isdeleted` TINYINT NOT NULL DEFAULT '0' AFTER `depth`;

ALTER TABLE `tbl_user_tree` ADD `level` VARCHAR(64) NOT NULL AFTER `user_type`, ADD `margin` DECIMAL(8,2) NOT NULL AFTER `level`;

ALTER TABLE `tbl_shops` ADD `user_parent_ids` VARCHAR(128) NOT NULL AFTER `subarea_id`;

ALTER TABLE `tbl_shops` ADD `pan_number` VARCHAR(64) NOT NULL AFTER `isdeleted`, ADD `fssai_number` VARCHAR(64) NOT NULL AFTER `pan_number`;

ALTER TABLE `tbl_leads` ADD `user_parent_ids` VARCHAR(128) NOT NULL AFTER `subarea_id`;

CREATE  VIEW `tbl_shop_view`  AS  select `sp`.`id` AS `id`,`sp`.`status` 
				AS `status`,`sp`.`status_seen_log` AS `status_seen_log`,`u`.`firstname` 
				AS `shop_added_by_name`,`u`.`parent_ids` AS `parent_ids`,`sp`.`name` AS `shop_name`,`sp`.`contact_person` 
				AS `contact_person`,`sp`.`mobile` AS `mobile`,`c`.`name` AS `cityname`,`s`.`name` 
				AS `statename`,`su`.`suburbnm` AS `suburbnm`,`sub`.`subarea_name` AS `subarea_name`,`sp`.`billing_name` 
				AS `billing_name`,`sp`.`bank_name` AS `bank_name`,`sp`.`bank_acc_name` AS `bank_acc_name`,`sp`.`bank_acc_no` 
				AS `bank_acc_no`,`sp`.`bank_b_name` AS `bank_b_name`,`sp`.`bank_ifsc` 
				AS `bank_ifsc`,`sp`.`added_on` AS `added_on`,`sp`.`state` AS `state_id`,`sp`.`city` AS `city_id` 
				from ((((((`tbl_shops` `sp` 
				left join `tbl_user` `u` on((`u`.`id` = `sp`.`shop_added_by`)))) 
				left join `tbl_state` `s` on((`sp`.`state` = `s`.`id`))) 
				left join `tbl_city` `c` on((`sp`.`city` = `c`.`id`))) 
				left join `tbl_area` `su` on((`sp`.`suburbid` = `su`.`id`))) 
				left join `tbl_subarea` `sub` on((`sp`.`subarea_id` = `sub`.`subarea_id`))) 
				where (`sp`.`isdeleted` <> '1') order by `sp`.`id` desc ;
				
ALTER TABLE `tbl_order_details`
  DROP `brand_id`,
  DROP `cat_id`,
  DROP `product_id`,
  DROP `producthsn`,
  DROP `product_variant_weight1`,
  DROP `product_variant_unit1`,
  DROP `product_variant_weight2`,
  DROP `product_variant_unit2`;
  
  ALTER TABLE `tbl_orders`
  DROP `region_id`,
  DROP `superstockistid`,
  DROP `distributorid`;
  
  ALTER TABLE `tbl_orders` ADD `sended_to_production` TINYINT NOT NULL DEFAULT '0' AFTER `offer_provided`;
  
  ALTER TABLE `tbl_shops` 
	ADD `service_by_usertype_id` INT NOT NULL AFTER `user_parent_ids`, 
	ADD `service_by_user_id` INT NOT NULL AFTER `service_by_usertype_id`;
UPDATE `tbl_shops` SET `service_by_usertype_id` = '1' WHERE `tbl_shops`.`service_by_usertype_id` = 0;
UPDATE `tbl_shops` SET `service_by_user_id` = '1' WHERE `tbl_shops`.`service_by_user_id` = 0;

  ALTER TABLE `tbl_leads` 
	ADD `service_by_usertype_id` INT NOT NULL AFTER `user_parent_ids`, 
	ADD `service_by_user_id` INT NOT NULL AFTER `service_by_usertype_id`;
	
ALTER TABLE `tbl_user_tree` CHANGE `margin` `usertype_margin` DECIMAL(8,2) NOT NULL;

ALTER TABLE `tbl_user` ADD `marginType` TINYINT NOT NULL COMMENT '0=rolewise,1=usertypewise,2=useridwise' AFTER `longitude`, ADD `userid_margin` DECIMAL(8,2) NOT NULL AFTER `marginType`;

ALTER TABLE `tbl_product_variant`
  DROP `price_level1`,
  DROP `price_level2`,
  DROP `price_level3`,
  DROP `price_level4`,
  DROP `price_level5`,
  DROP `price_level6`,
  DROP `price_level7`,
  DROP `price_level8`,
   DROP `mrp_or_margin_flag`;
   
   ALTER TABLE `tbl_product`
  DROP `mrp_or_margin_flag`;
   
DROP VIEW tbl_user_view;
CREATE  VIEW `tbl_user_view`  AS  select `u`.`id` AS `id`,`u`.`external_id` AS `external_id`,`u`.`user_role` AS `user_role`,`u`.`reset_key` AS `reset_key`,`u`.`user_status` AS `user_status`,`u`.`firstname` AS `firstname`,`u`.`username` AS `username`,`u`.`pwd` AS `pwd`,`u`.`user_type` AS `user_type`,`u`.`address` AS `address`,`c`.`name` AS `cityname`,`u`.`city` AS `city_id`,`u`.`state` AS `state_id`,`s`.`name` AS `statename`,`u`.`suburb_ids` AS `suburb_ids`,(select group_concat(`su`.`suburbnm` order by `su`.`id` ASC separator ',') from (`tbl_user` `innerarea` join `tbl_area` `su` on((find_in_set(`su`.`id`,`innerarea`.`suburb_ids`) > 0))) where (`innerarea`.`id` = `u`.`id`) group by `innerarea`.`id`) AS `suburbnm`,`u`.`subarea_ids` AS `subarea_ids`,(select group_concat(`sub`.`subarea_name` order by `sub`.`subarea_id` ASC separator ',') from (`tbl_user` `inneru` join `tbl_subarea` `sub` on((find_in_set(`sub`.`subarea_id`,`inneru`.`subarea_ids`) > 0))) where (`inneru`.`id` = `u`.`id`) group by `inneru`.`id`) AS `subarea_name`,`u`.`mobile` AS `mobile`,`u`.`email` AS `email`,`u`.`parent_ids` AS `parent_ids`,(select group_concat(`inneru1`.`firstname` order by `inneru1`.`id` ASC separator ',') from (`tbl_user` `inneru2` join `tbl_user` `inneru1` on((find_in_set(`inneru1`.`id`,`inneru2`.`parent_ids`) > 0))) where (`inneru2`.`id` = `u`.`id`) group by `inneru2`.`id`) AS `parent_names`,(select group_concat(`inner_usertype`.`user_type` order by `inner_usertype`.`id` ASC separator ',') from (`tbl_user` `inneru123` join `tbl_user` `inner_usertype` on((find_in_set(`inner_usertype`.`id`,`inneru123`.`parent_ids`) > 0))) where (`inneru123`.`id` = `u`.`id`) group by `inneru123`.`id`) AS `parent_usertypes`,`u`.`office_phone_no` AS `office_phone_no`,`u`.`accname` AS `accname`,`u`.`accno` AS `accno`,`u`.`bank_name` AS `bank_name`,`u`.`accbrnm` AS `accbrnm`,`u`.`accifsc` AS `accifsc`,`u`.`gst_number_sss` AS `gst_number_sss`,`u`.`latitude` AS `latitude`,`u`.`longitude` AS `longitude`,`u`.`marginType` AS `marginType`,`u`.`userid_margin` AS `userid_margin`,`u`.`is_default_user` AS `is_default_user`,`u`.`is_common_user` AS `is_common_user`,`u`.`added_on` AS `added_on` from ((`tbl_user` `u` left join `tbl_state` `s` on((`u`.`state` = `s`.`id`))) left join `tbl_city` `c` on((`u`.`city` = `c`.`id`))) where ((`u`.`isdeleted` <> '1') and (`u`.`is_common_user` <> '1')) group by `u`.`id` order by `u`.`id` desc ;

ALTER TABLE `tbl_product_variant` ADD `productvarid_margin` DECIMAL(8,2) NOT NULL AFTER `price`;



CREATE TABLE `tbl_campaign_web` (
  `id` int(11) NOT NULL,
  `campaign_name` varchar(255) DEFAULT NULL,
  `campaign_description` varchar(255) DEFAULT NULL,
  `campaign_start_date` date DEFAULT NULL,
  `campaign_end_date` date DEFAULT NULL,
  `campaign_type` varchar(255) DEFAULT NULL,
  `added_on` date DEFAULT NULL,
  `updated_on` date DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT 'Active/Inactive, 0/1',
  `deleted` int(11) DEFAULT '0' COMMENT 'not deleted/deleted, 0/1',
  `brand_all` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





ALTER TABLE `tbl_campaign_web`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tbl_campaign_web`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `tbl_campaign_web` ADD  `level` varchar(255) DEFAULT NULL COMMENT 'state,city,suburb,shop',
							ADD  `state_id` varchar(255) DEFAULT NULL COMMENT 'comma separated values of state',
							ADD  `city_id` varchar(255) DEFAULT NULL COMMENT 'comma separated values of city',
							ADD  `suburb_id` varchar(255) DEFAULT NULL COMMENT 'comma separated values of suburb',
							ADD  `subarea_id` varchar(255) DEFAULT NULL;
ALTER TABLE `tbl_campaign_web` ADD  `campaign_area_id` int(11) DEFAULT NULL,
								ADD	`product_price` varchar(255) DEFAULT NULL,
								ADD	`discount_percent` varchar(255) DEFAULT NULL;
								
ALTER TABLE `tbl_campaign_web` ADD  `c_product_id` int(11) DEFAULT NULL,
								ADD  `c_product_var_id` int(11) DEFAULT NULL,
								ADD `c_p_quantity` varchar(255) DEFAULT NULL,
								ADD  `f_product_id` int(11) DEFAULT NULL,
								ADD  `f_product_var_id` int(11) DEFAULT NULL,
								ADD `f_p_quantity` varchar(255) DEFAULT NULL;

  
  