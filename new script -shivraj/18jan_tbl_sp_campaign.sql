--test
CREATE TABLE `tbl_sp_campaign_assign` (
  `id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `sp_id` int(11) NOT NULL,
  `added_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `tbl_sp_campaign_assign`
  ADD PRIMARY KEY (`id`);
  
ALTER TABLE `tbl_sp_campaign_assign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
  
  --14jan
  --export import tbl_campaign_product
ALTER TABLE `tbl_campaign_product` ADD PRIMARY KEY(`id`);
ALTER TABLE `tbl_campaign_product` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

UPDATE `tbl_state` SET `name` = 'Andaman & Nicobar Islands' WHERE `tbl_state`.`id` = 1; 
UPDATE `tbl_state` SET `name` = 'Dadra & Nagar Haveli' WHERE `tbl_state`.`id` = 8; 
UPDATE `tbl_state` SET `name` = 'Daman & Diu' WHERE `tbl_state`.`id` = 9; 
UPDATE `tbl_state` SET `name` = 'Jammu & Kashmir' WHERE `tbl_state`.`id` = 15;

ALTER TABLE `tbl_orders` ADD `margin_percentage` DECIMAL(3,2) NOT NULL AFTER `sended_to_production`;

--after migration of all instances. 11-March-2019
--this changes are done on only dev and live instances not qa and demo 
DROP VIEW `tbl_shop_view`;
CREATE  VIEW `tbl_shop_view`  AS  (select `sp`.`id` AS `id`,`sp`.`shop_added_by` AS `shop_added_by`,`sp`.`service_by_usertype_id` AS `service_by_usertype_id`,`sp`.`service_by_user_id` 
AS `service_by_user_id`,`sp`.`status` AS `status`,`sp`.`shop_status` AS `shop_status`,`sp`.`status_seen_log` 
AS `status_seen_log`,`u`.`firstname` AS `shop_added_by_name`,`u`.`parent_ids` AS `parent_ids`,`sp`.`name` 
AS `shop_name`,`sp`.`address` AS `address`,`sp`.`contact_person` AS `contact_person`,`sp`.`mobile` AS `mobile`,`c`.`name` AS `cityname`,`s`.`name` 
AS `statename`,`su`.`suburbnm` AS `suburbnm`,`sub`.`subarea_name` AS `subarea_name`,`sp`.`billing_name` 
AS `billing_name`,`sp`.`bank_name` AS `bank_name`,`sp`.`bank_acc_name` AS `bank_acc_name`,`sp`.`bank_acc_no` 
AS `bank_acc_no`,`sp`.`bank_b_name` AS `bank_b_name`,`sp`.`bank_ifsc` AS `bank_ifsc`,`sp`.`added_on` 
AS `added_on`,`sp`.`state` AS `state_id`,`sp`.`city` AS `city_id`,`sp`.`suburbid` AS `suburbid`,`sp`.`subarea_id` 
AS `subarea_id`,`sp`.`latitude` AS `latitude`,`sp`.`longitude` AS `longitude` 
from (((((`tbl_shops` `sp` left join `tbl_user` `u` on((`u`.`id` = `sp`.`shop_added_by`))) 
left join `tbl_state` `s` on((`sp`.`state` = `s`.`id`))) left join `tbl_city` `c` on((`sp`.`city` = `c`.`id`))) 
left join `tbl_area` `su` on((`sp`.`suburbid` = `su`.`id`))) 
left join `tbl_subarea` `sub` on((`sp`.`subarea_id` = `sub`.`subarea_id`))) 
where (`sp`.`isdeleted` <> '1') order by `sp`.`id` desc) ;


ALTER TABLE `tbl_leads` ADD `fssai_number` VARCHAR(64) NOT NULL AFTER `isdeleted`;

DROP VIEW `tbl_user_view`;
CREATE view `tbl_user_view` 
AS 
  SELECT `u`.`id`                     AS `id`, 
         `u`.`external_id`            AS `external_id`, 
         `u`.`user_role`              AS `user_role`, 
         `u`.`reset_key`              AS `reset_key`, 
         `u`.`user_status`            AS `user_status`, 
         `u`.`firstname`              AS `firstname`, 
         `u`.`username`               AS `username`, 
         `u`.`pwd`                    AS `pwd`, 
         `u`.`user_type`              AS `user_type`, 
         `u`.`depth`                  AS `depth`, 
         `u`.`address`                AS `address`, 
         `c`.`name`                   AS `cityname`, 
         `u`.`city`                   AS `city_id`, 
         `u`.`state`                  AS `state_id`, 
         `s`.`name`                   AS `statename`, 
         `u`.`suburb_ids`             AS `suburb_ids`, 
         (SELECT Group_concat(`su`.`suburbnm` ORDER BY `su`.`id` ASC SEPARATOR 
                 ',' 
                 ) 
          FROM   (`tbl_user` `innerarea` 
                  JOIN `tbl_area` `su` 
                    ON(( Find_in_set(`su`.`id`, `innerarea`.`suburb_ids`) > 0 )) 
                 ) 
          WHERE  ( `innerarea`.`id` = `u`.`id` ) 
          GROUP  BY `innerarea`.`id`) AS `suburbnm`, 
         `u`.`subarea_ids`            AS `subarea_ids`, 
         (SELECT Group_concat(`sub`.`subarea_name` ORDER BY `sub`.`subarea_id` 
                 ASC 
                 SEPARATOR ',') 
          FROM   (`tbl_user` `inneru` 
                  JOIN `tbl_subarea` `sub` 
                    ON(( Find_in_set(`sub`.`subarea_id`, `inneru`.`subarea_ids`) 
                         > 
                         0 ))) 
          WHERE  ( `inneru`.`id` = `u`.`id` ) 
          GROUP  BY `inneru`.`id`)    AS `subarea_name`, 
         `u`.`mobile`                 AS `mobile`, 
         `u`.`email`                  AS `email`, 
         `u`.`parent_ids`             AS `parent_ids`, 
         (SELECT Group_concat(`inneru1`.`firstname` ORDER BY `inneru1`.`depth` ASC 
                 SEPARATOR 
                 ',') 
          FROM   (`tbl_user` `inneru2` 
                  JOIN `tbl_user` `inneru1` 
                    ON(( Find_in_set(`inneru1`.`id`, `inneru2`.`parent_ids`) > 0 
                       ) 
                      )) 
          WHERE  ( `inneru2`.`id` = `u`.`id` ) 
          GROUP  BY `inneru2`.`id`)   AS `parent_names`, 
         (SELECT Group_concat(`inner_usertype`.`user_type` ORDER BY 
                 `inner_usertype`.`depth` 
                 ASC 
                 SEPARATOR ',') 
          FROM   (`tbl_user` `inneru123` 
                  JOIN `tbl_user` `inner_usertype` 
                    ON(( Find_in_set(`inner_usertype`.`id`, 
                         `inneru123`.`parent_ids`) > 0 
                       ))) 
          WHERE  ( `inneru123`.`id` = `u`.`id` ) 
          GROUP  BY `inneru123`.`id`) AS `parent_usertypes`, 
         `u`.`office_phone_no`        AS `office_phone_no`, 
         `u`.`accname`                AS `accname`, 
         `u`.`accno`                  AS `accno`, 
         `u`.`bank_name`              AS `bank_name`, 
         `u`.`accbrnm`                AS `accbrnm`, 
         `u`.`accifsc`                AS `accifsc`, 
         `u`.`gst_number_sss`         AS `gst_number_sss`, 
         `u`.`latitude`               AS `latitude`, 
         `u`.`longitude`              AS `longitude`, 
         `u`.`margintype`             AS `marginType`, 
         `u`.`userid_margin`          AS `userid_margin`, 
         `u`.`is_default_user`        AS `is_default_user`, 
         `u`.`is_common_user`         AS `is_common_user`, 
         `u`.`added_on`               AS `added_on` 
  FROM   ((`tbl_user` `u` 
           LEFT JOIN `tbl_state` `s` 
                  ON(( `u`.`state` = `s`.`id` ))) 
          LEFT JOIN `tbl_city` `c` 
                 ON(( `u`.`city` = `c`.`id` ))) 
  WHERE  ( ( `u`.`isdeleted` <> '1' ) 
           AND ( `u`.`is_common_user` <> '1' ) ) 
  GROUP  BY `u`.`id` 
  ORDER  BY `u`.`id` DESC; 

--viraly changes 14 march
-- it is in all dbs added
ALTER TABLE `tbl_shops` ADD `landline_number_other` VARCHAR(255) NOT NULL AFTER `mobile_number_other`;
ALTER TABLE `tbl_leads` ADD `landline_number_other` VARCHAR(255) NOT NULL AFTER `mobile_number_other`;

--free product campaign changes 25-march-2019
ALTER TABLE `tbl_campaign_product` ADD `left_count` INT NOT NULL AFTER `f_p_quantity_measure`, ADD `right_count` 
INT NOT NULL AFTER `left_count`;

ALTER TABLE `tbl_order_details` CHANGE `order_status` `order_status` INT(11) NULL DEFAULT NULL COMMENT '(1:received, 2:assigned_delivery, 3: Assigned to Transport office 4:delivered, 5:partial_delivered, 6:payment_done, 7:payment_pending, 8:partial_payment_done,9:cancel_order)';

--new table customer 

CREATE TABLE `tbl_customer` (
  `cust_id` int(11) NOT NULL,
  `cust_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cust_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cust_mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cust_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`cust_id`);

ALTER TABLE `tbl_customer`
  MODIFY `cust_id` int(11) NOT NULL AUTO_INCREMENT;
  
--new orders table for customer who can get order from web 
--this flow is for who direct call distributor or salzpoint web user 
--to get the order by phone call
--05-Apr-2019

CREATE TABLE `tbl_customer_orders_new` (
  `id` int(11) NOT NULL,
  `order_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_from` int(11) NOT NULL,
  `order_to` int(11) NOT NULL,
  `invoice_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `current_lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `current_long` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `brand_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `producthsn` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `product_variant_id` int(11) DEFAULT NULL,
  `product_quantity` int(11) DEFAULT NULL,
  `product_variant_weight1` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `product_variant_unit1` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `product_variant_weight2` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `product_variant_unit2` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `product_unit_cost` float DEFAULT NULL,
  `product_total_cost` decimal(14,2) DEFAULT NULL,
  `product_cgst` float DEFAULT NULL,
  `product_sgst` float DEFAULT NULL,
  `p_cost_cgst_sgst` decimal(14,2) DEFAULT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `tbl_customer_orders_new`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tbl_customer_orders_new`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
ALTER TABLE `tbl_customer_orders_new` ADD `delivery_assing_date` DATETIME NULL DEFAULT NULL AFTER `order_status`;
ALTER TABLE `tbl_customer_orders_new` ADD  `delivery_assign_to` int(11) DEFAULT NULL AFTER `delivery_assing_date`;
ALTER TABLE `tbl_customer_orders_new` ADD `trans_office_id` int(11) NOT NULL AFTER `delivery_assign_to`;
ALTER TABLE `tbl_customer_orders_new` ADD  `transport_date` datetime DEFAULT NULL AFTER `trans_office_id`;
 ALTER TABLE `tbl_customer_orders_new` ADD `challan_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL AFTER `transport_date`;
 ALTER TABLE `tbl_customer_orders_new` ADD `vehicle_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL AFTER `challan_no`;
 ALTER TABLE `tbl_customer_orders_new` ADD `transport_mode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL AFTER `vehicle_no`;
ALTER TABLE `tbl_customer_orders_new` ADD  `date_time_supply` datetime DEFAULT NULL AFTER `transport_mode`;
ALTER TABLE `tbl_customer_orders_new` ADD  `place_of_supply` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL AFTER `date_time_supply`;
ALTER TABLE `tbl_customer_orders_new` ADD  `quantity_delivered` int(11) DEFAULT NULL AFTER `place_of_supply`;
ALTER TABLE `tbl_customer_orders_new` ADD  `delivery_date` datetime DEFAULT NULL AFTER `quantity_delivered`;
ALTER TABLE `tbl_customer_orders_new` ADD  `amount_paid` float DEFAULT NULL AFTER `delivery_date`;
ALTER TABLE `tbl_customer_orders_new` ADD  `payment_date` datetime DEFAULT NULL AFTER `amount_paid`;
ALTER TABLE `tbl_customer_orders_new` ADD  `signature_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL AFTER `payment_date`;

--21-May-2019
ALTER TABLE `tbl_order_details` CHANGE `comment` `comment` VARCHAR(512) NOT NULL;
ALTER TABLE `tbl_order_details` CHANGE `payment_status` `payment_status` VARCHAR(50) 
CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'blank,delay,confirmed';