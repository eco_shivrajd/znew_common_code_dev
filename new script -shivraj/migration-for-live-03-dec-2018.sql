
UPDATE tbl_users SET user_level = level;

TRUNCATE `tbl_campaign_product`;
TRUNCATE `tbl_product`;
TRUNCATE `tbl_campaign_area_price`;
TRUNCATE `tbl_subarea`;
TRUNCATE `tbl_orders`;
TRUNCATE `tbl_van`;
TRUNCATE `tbl_campaign_area`;
TRUNCATE `tbl_leads`;
TRUNCATE `tbl_stock_management_hist`;
TRUNCATE `tbl_order_stockist`;
TRUNCATE `tbl_campaign`;
TRUNCATE `tbl_stock_management`;
TRUNCATE `tbl_brand`;
TRUNCATE `tbl_area`;
TRUNCATE `tbl_order_purchase_p_weight`;
TRUNCATE `tbl_user_tree`;
TRUNCATE `tbl_sp_attendance`;
TRUNCATE `tbl_shops`;
TRUNCATE `tbl_category`;
TRUNCATE `tbl_record_tracker`;
TRUNCATE `tbl_order_details`;
TRUNCATE `tbl_user_location`;
TRUNCATE `tbl_shop_visit`;
TRUNCATE `tbl_campaign_web`;
TRUNCATE `tbl_order_cp_discount`;
TRUNCATE `tbl_user`;
TRUNCATE `tbl_campaign_product_weight`;
TRUNCATE `tbl_product_variant`;
TRUNCATE `tbl_units`;
TRUNCATE `tbl_order_c_n_f_product`;
TRUNCATE `tbl_target`;
TRUNCATE `tbl_route_details_by_map`;
TRUNCATE `tbl_lead_details`;
TRUNCATE `tbl_route_details`;
TRUNCATE `tbl_dcp_stock`;
TRUNCATE `tbl_sp_tadabill`;
TRUNCATE `tbl_route_by_map`;
TRUNCATE `tbl_dcp_orders`;
TRUNCATE `tbl_route`;
TRUNCATE `tbl_dcp_cartons_assign`;
TRUNCATE `tbl_dcp_cartons`;
TRUNCATE `tbl_customer_orders`;

DELETE FROM `tbl_action_profile`  WHERE `profile_id` != 1;

--export result of this query from old db
SELECT `id`, `common_user_id`, `external_id`, `firstname`, `username`, `pwd`, `user_type`, `subarea_ids`, `suburb_ids`, `city`, `state`, `address`, `mobile`, `email`, `stockist_id`, `sstockist_id`, `office_phone_no`, `accname`, `accno`, `bank_name`, `accbrnm`, `accifsc`, `gst_number_sss`, `latitude`, `longitude`, `reset_key`, `is_default_user`, `isdeleted`, `user_status`, `is_common_user`, `added_on` FROM `tbl_user` WHERE 1;
--import to new db using insert query
INSERT INTO `tbl_user_tree` (`id`, `user_role`, `user_type`, `level`, `usertype_margin`, `parent_id`, `all_parent_id`, `depth`, `isdeleted`, `ul_added_on`) VALUES
(1, 'Admin', 'Admin', 'level1', '0.00', '0', '0', 1, 0, '2018-03-12 09:00:00');

UPDATE `tbl_user` SET `profile_id` = '1' WHERE `tbl_user`.`id` = 1;
UPDATE `tbl_user` SET `user_role` = 'Admin' WHERE `tbl_user`.`id` = 1;

SELECT *  FROM `tbl_user` WHERE `user_type` LIKE 'Superstockist';
UPDATE `tbl_user` SET `profile_id` = '2',`user_role`='Superstockist',`depth`='2',`parent_ids`='1' WHERE `user_type` LIKE 'Superstockist';

UPDATE `tbl_user` SET `profile_id` = '3',`user_role`='Distributor',`depth`='3',`parent_ids`='1,2' WHERE `user_type` LIKE 'Distributor';


UPDATE tbl_user SET user_role = user_type;

UPDATE `tbl_user` SET `profile_id` = '4',`user_role`='SalesPerson',`depth`='4',`parent_ids`='1,2,3' WHERE `user_type` LIKE 'SalesPerson';
UPDATE `tbl_user` SET `profile_id` = '5',`user_role`='DeliveryPerson',`depth`='4',`parent_ids`='1,2,3' WHERE `user_type` LIKE 'DeliveryPerson';
UPDATE `tbl_user` SET `profile_id` = '7',`user_role`='DeliveryChannelPerson',`depth`='4',`parent_ids`='1,2,3' WHERE `user_type` LIKE 'DeliveryChannelPerson';


UPDATE `tbl_user` SET `profile_id` = '6',`user_role`='Accountant',`depth`='2',`parent_ids`='1' WHERE `user_type` LIKE 'Accountant';

UPDATE `tbl_userrole` SET `userrole_margin` = '0.00' ;
/*import and export folowing*/
SELECT `id`, `catid`, `productname`, `added_on`, `isdeleted`, `deleted_on`, 
`added_by_userid`, `added_by_usertype` FROM `tbl_product` WHERE 1;

SELECT `id`, `productid`, `units_variant_id`, `numunits`, `variantrownumber`, `price`, `variant_1`, `variant_2`,
 `variant1_unit_id`, `variant2_unit_id`, `variant_cnt`, `productimage`, `productbarcodeimage`, 
`producthsn`, `cgst`, `sgst`, `carton_quantities` FROM `tbl_product_variant` WHERE 1;

SELECT `id`, `order_no`, `invoice_no`, `ordered_by`, `delivery_person_id`, `order_date`,
 `shop_id`, `superstockistid`, `distributorid`, `total_items`, `total_cost`, `total_order_gst_cost`, 
 `c_o_d`, `cod_percent`, `lat`, `long`, 
`offer_provided`, `shop_order_status` FROM `tbl_orders` WHERE 1;

ALTER TABLE `tbl_orders` ADD `superstockistid` INT NOT NULL AFTER `sended_to_production`;
ALTER TABLE `tbl_orders` ADD `distributorid` INT NOT NULL AFTER `superstockistid`;
ALTER TABLE `tbl_orders` ADD `shop_order_status` INT NOT NULL AFTER `distributorid`;

UPDATE tbl_orders SET order_from = ordered_by;
UPDATE tbl_orders SET order_to = distributorid;

ALTER TABLE `tbl_order_details` ADD `supp_chnz_status` INT NOT NULL AFTER `signature_image`;
ALTER TABLE `tbl_orders`
  DROP `superstockistid`,
  DROP `distributorid`,
  DROP `shop_order_status`;
  ALTER TABLE `tbl_order_details`
  DROP `supp_chnz_status`;
