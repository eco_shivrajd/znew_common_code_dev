

UPDATE `tbl_clients_maintenance` SET `wsbasepath` = 'http://salzpoint.net/uatnew/kaysons' ;

UPDATE `tbl_clients_maintenance` 
SET `logo` = 'http://salzpoint.net/uatnew/kaysons/assets/SiteLogo/vph_logo.png' 
WHERE `tbl_clients_maintenance`.`id` = 3;

UPDATE `tbl_clients_maintenance` 
SET `logo` = 'http://salzpoint.net/uatnew/kaysons/assets/SiteLogo/bablifoods_logo.png' 
WHERE `tbl_clients_maintenance`.`id` = 4;


ALTER TABLE `tbl_user` 
ADD `wstate_ids` VARCHAR(400) NOT NULL AFTER `added_on`, 
ADD `wcity_ids` VARCHAR(400) NOT NULL AFTER `wstate_ids`, 
ADD `warea_ids` VARCHAR(400) NOT NULL AFTER `wcity_ids`, 
ADD `wsubarea_ids` VARCHAR(400) NOT NULL AFTER `warea_ids`;



SELECT `user_id`, `state_ids` as wstate_ids, `city_ids` 
as wcity_ids, `suburb_ids` as wsuburb_ids, `subarea_ids` as wsubarea_ids 
FROM `tbl_user_working_area` WHERE 1;


CREATE TABLE `tbl_user_working_area` (
  `user_id` int(11) NOT NULL,
  `state_ids` varchar(400) NOT NULL,
  `city_ids` varchar(400) NOT NULL,
  `suburb_ids` varchar(400) NOT NULL,
  `subarea_ids` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_user_working_area`
--
ALTER TABLE `tbl_user_working_area`
  ADD PRIMARY KEY (`user_id`);
  
  
  

UPDATE `tbl_user` SET `wstate_ids` = '22',`wcity_ids`='2836',`warea_ids`='',`wsubarea_ids`='' 
WHERE `tbl_user`.`old_id` = 21000;

UPDATE `tbl_product` SET `added_by_userid` = '1';
UPDATE `tbl_product` SET `added_by_usertype` = 'Admin'

ALTER TABLE `tbl_units` ADD `variantid` INT NOT NULL AFTER `id`;
UPDATE `tbl_units` SET `variantid` = '41' WHERE `tbl_units`.`id` = 5;


