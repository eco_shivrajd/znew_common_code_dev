
CREATE TABLE `tbl_sp_campaign_assign` (
  `id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `sp_id` int(11) NOT NULL,
  `added_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `tbl_sp_campaign_assign`
  ADD PRIMARY KEY (`id`);
  
ALTER TABLE `tbl_sp_campaign_assign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
  
  --14jan
  --export import tbl_campaign_product
ALTER TABLE `tbl_campaign_product` ADD PRIMARY KEY(`id`);
ALTER TABLE `tbl_campaign_product` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

UPDATE `tbl_state` SET `name` = 'Andaman & Nicobar Islands' WHERE `tbl_state`.`id` = 1; 
UPDATE `tbl_state` SET `name` = 'Dadra & Nagar Haveli' WHERE `tbl_state`.`id` = 8; 
UPDATE `tbl_state` SET `name` = 'Daman & Diu' WHERE `tbl_state`.`id` = 9; 
UPDATE `tbl_state` SET `name` = 'Jammu & Kashmir' WHERE `tbl_state`.`id` = 15;