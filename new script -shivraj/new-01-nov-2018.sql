ALTER TABLE `tbl_order_details` ADD `signature_image` VARCHAR(255) NOT NULL AFTER `payment_date`;

`shop_status` tinyint(4) DEFAULT '0' COMMENT '0=Active,1=Inactive',

CREATE TABLE `tbl_shop_view`

ALTER TABLE `tbl_shops` ADD `pincode` VARCHAR(10) NULL DEFAULT NULL AFTER `address`;


CREATE ALGORITHM=UNDEFINED DEFINER=`salzpoint`@`localhost` SQL SECURITY DEFINER VIEW `tbl_user_view`  
AS  select `u`.`id` AS `id`,`u`.`external_id` 
AS `external_id`,`u`.`user_role` AS `user_role`,`u`.`reset_key` AS `reset_key`,`u`.`user_status` 
AS `user_status`,`u`.`firstname` AS `firstname`,`u`.`username` AS `username`,`u`.`pwd` 
AS `pwd`,`u`.`user_type` AS `user_type`,`u`.`depth` AS `depth`,`u`.`address` AS `address`,`c`.`name` AS `cityname`,`u`.`city` 
AS `city_id`,`u`.`state` AS `state_id`,`s`.`name` AS `statename`,`u`.`suburb_ids` 
AS `suburb_ids`,(select group_concat(`su`.`suburbnm` order by `su`.`id` ASC separator ',') 
from (`tbl_user` `innerarea` join `tbl_area` `su` on((find_in_set(`su`.`id`,`innerarea`.`suburb_ids`) > 0))) 
where (`innerarea`.`id` = `u`.`id`) group by `innerarea`.`id`) AS `suburbnm`,`u`.`subarea_ids` 
AS `subarea_ids`,(select group_concat(`sub`.`subarea_name` order by `sub`.`subarea_id` ASC separator ',') 
from (`tbl_user` `inneru` join `tbl_subarea` `sub` on((find_in_set(`sub`.`subarea_id`,`inneru`.`subarea_ids`) > 0))) 
where (`inneru`.`id` = `u`.`id`) group by `inneru`.`id`) AS `subarea_name`,`u`.`mobile` 
AS `mobile`,`u`.`email` AS `email`,`u`.`parent_ids` 
AS `parent_ids`,(select group_concat(`inneru1`.`firstname` order by `inneru1`.`id` ASC separator ',') 
from (`tbl_user` `inneru2` join `tbl_user` `inneru1` on((find_in_set(`inneru1`.`id`,`inneru2`.`parent_ids`) > 0))) 
where (`inneru2`.`id` = `u`.`id`) group by `inneru2`.`id`) 
AS `parent_names`,(select group_concat(`inner_usertype`.`user_type` 
order by `inner_usertype`.`id` ASC separator ',') from (`tbl_user` `inneru123` 
join `tbl_user` `inner_usertype` on((find_in_set(`inner_usertype`.`id`,`inneru123`.`parent_ids`) > 0))) 
where (`inneru123`.`id` = `u`.`id`) group by `inneru123`.`id`) AS `parent_usertypes`,`u`.`office_phone_no` 
AS `office_phone_no`,`u`.`accname` AS `accname`,`u`.`accno` AS `accno`,`u`.`bank_name` 
AS `bank_name`,`u`.`accbrnm` AS `accbrnm`,`u`.`accifsc` AS `accifsc`,`u`.`gst_number_sss` 
AS `gst_number_sss`,`u`.`latitude` AS `latitude`,`u`.`longitude` AS `longitude`,`u`.`marginType` 
AS `marginType`,`u`.`userid_margin` AS `userid_margin`,`u`.`is_default_user` 
AS `is_default_user`,`u`.`is_common_user` AS `is_common_user`,`u`.`added_on` AS `added_on` 
from ((`tbl_user` `u` left join `tbl_state` `s` on((`u`.`state` = `s`.`id`))) 
left join `tbl_city` `c` on((`u`.`city` = `c`.`id`))) where ((`u`.`isdeleted` <> '1') 
and (`u`.`is_common_user` <> '1')) group by `u`.`id` order by `u`.`id` desc ;


ALTER TABLE `tbl_pages` ADD `php_page_add` VARCHAR(30) NOT NULL AFTER `php_page_name`, ADD `php_page_edit` VARCHAR(30) NOT NULL AFTER `php_page_add`, ADD `php_page_other` VARCHAR(30) NOT NULL AFTER `php_page_edit`;