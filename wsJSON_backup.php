<?php
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);

class wsJSON {

    private $connection;
    private $json_array = array("status" => array("responsecode" => "1", "entity" => "1"), "data" => array());

    function __construct($con) {
        $this->connection = $con;
    }

    function executeQuery($query) {
        $result = $this->connection->query($query) or die(mysql_error());
        return $result;
    }

    function addslashes_to_string($str) {
        $tempStr = addslashes(trim($str));
        return str_replace("\'", "", $tempStr);
        // return str_replace("\r\n","",str_replace("\'","'",$tempStr));
        //return str_replace("\r\n","",str_replace("\'","'",$tempStr));
    }

    function GetLatitudeLongitude($geoaddress) {
        $geoaddress = urlencode($geoaddress);
        $geoaddress = str_replace("#", "", $geoaddress);
        $geoaddress = str_replace(" ", "+", $geoaddress);
        $geoaddress = str_replace("++", "+", $geoaddress);
        //echo "http://maps.googleapis.com/maps/api/geocode/json?address=".$geoaddress."&sensor=true";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://maps.googleapis.com/maps/api/geocode/json?address=" . $geoaddress . "&sensor=true");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $geocode = curl_exec($ch);
        $output2 = json_decode($geocode);

        curl_close($ch);
        $latitude = $output2->results[0]->geometry->location->lat;
        $longitude = $output2->results[0]->geometry->location->lng;

        $coordinate = array($latitude, $longitude);
        return $coordinate;
    }

    function fnSendInvitation($senderid, $receiverid) {

        //check invitation already exists:	 
        $sqluser = "SELECT * FROM users WHERE uid='" . $receiverid . "'";
        $proRowuser = $this->executeQuery($sqluser);
        $rowuser = mysql_fetch_array($proRowuser);

        //contactssource
        $sql = "SELECT * from contactssource WHERE phone='" . $rowuser['phone'] . "' AND feloze_uid='" . $senderid . "'";
        $result = $this->executeQuery($sql);
        $rowcount = mysql_num_rows($result);
        if ($rowcount > 0) {
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['invitation']['msg'] = "Contact already exist in contacts.";
        } else {
            $sql = "SELECT *  FROM tbl_invitation WHERE senderid='$senderid' AND receiverid='$receiverid' AND status!='4'";
            $proRow = $this->executeQuery($sql);
            if (mysql_num_rows($proRow) != 0) {
                $this->json_array['status']['responsecode'] = '1';
                $this->json_array['status']['entity'] = '1';
                $this->json_array['data']['invitation']['msg'] = "Invitation already sent.";
            } else {
                $sql = "INSERT INTO tbl_invitation(senderid,receiverid)VALUES('$senderid', '$receiverid') ";
                $proRow = $this->executeQuery($sql);
                $user_id = mysql_insert_id();
                if ($user_id) {
                    $this->json_array['status']['responsecode'] = '0';
                    $this->json_array['status']['entity'] = '1';
                    $this->json_array['data']['invitation']['msg'] = "Invitation sent successfully.";
                } else {
                    $this->json_array['status']['responsecode'] = '0';
                    $this->json_array['status']['entity'] = '1';
                    $this->json_array['data']['invitation']['msg'] = "Unsuccessful.";
                }
            }
        }
        return json_encode($this->json_array);
    }

    function fnplaceorder($total_order_cost, $total_order_gst_cost, $sale_count, $free_count, $total_count) {
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '1';
        $this->json_array['data']['invitation']['msg'] = "Order Placed Successfully.";
        $this->json_array['data']['total_cost'] = "$total_order_cost";
        $this->json_array['data']['total_gst_cost'] = "$total_order_gst_cost";
        $this->json_array['data']['total_quantity'] = "$sale_count";
        $this->json_array['data']['sale_quantity'] = "$free_count";
        $this->json_array['data']['free_quantity'] = "$total_count";
        return json_encode($this->json_array);
    }

    function get_userdata($userid) {
         $sql = " SELECT tu.id, tu.external_id,
		 (select tu1.firstname from tbl_user tu1 where tu1.id=tu.external_id ) as externalid_name, 
		 (select tu2.mobile from tbl_user tu2 where tu2.id=tu.external_id ) as externalid_mobile,
		 tu.firstname, 
		tu.username, tu.pwd, tu.user_type, tu.user_role, tu.address, tu.city, tu.state,tuv.statename,tuv.cityname, 
		tu.mobile, tu.email, tu.reset_key, tu.suburb_ids as suburb_ids,tuv.suburbnm as suburbnm, 
		tu.subarea_ids as subarea_ids,tuv.subarea_name as subarea_name,
		tuv.parent_ids,tuv.parent_names
		FROM  `tbl_user` tu
		left join tbl_user_view tuv on tu.id=tuv.id
		WHERE tu.id='$userid'";//LEFT JOIN tbl_user_working_area ON tbl_user.id = tbl_user_working_area.user_id
	
		//die();
        $proRow = $this->executeQuery($sql);
        $attendance_count = $this->checkAttendance($userid);

        if (mysql_num_rows($proRow) != 0) {
            $row = mysql_fetch_array($proRow);
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '6';           
           
			$this->json_array['data']['Login']['external_id'] = $row['external_id'];
			$this->json_array['data']['Login']['externalid_name'] = $row['externalid_name'];
			$this->json_array['data']['Login']['externalid_mobile'] = $row['externalid_mobile'];
			
			if($row['external_id']=='1'){
				$this->json_array['data']['Login']['is_admin_child'] = "yes";
			}else{
				$this->json_array['data']['Login']['is_admin_child'] = "no";
			}
           
            $this->json_array['data']['Login']['id'] = $row['id'];            
            $this->json_array['data']['Login']['parent_ids'] = $row['parent_ids'];
			$this->json_array['data']['Login']['parent_names'] = $row['parent_names'];
            $this->json_array['data']['Login']['username'] = $row['username'];
            $this->json_array['data']['Login']['firstname'] = $row['firstname'];
            $this->json_array['data']['Login']['surname'] = '';
            $this->json_array['data']['Login']['address'] = $row['address'];
            $this->json_array['data']['Login']['email'] = $row['email'];
            $this->json_array['data']['Login']['mobile'] = $row['mobile'];
			$this->json_array['data']['Login']['stateid'] = $row['state'];
            $this->json_array['data']['Login']['statenm'] = $row['statename'];
			$this->json_array['data']['Login']['cityid'] = $row['city'];
            $this->json_array['data']['Login']['citynm'] = $row['cityname'];

            /*             * ******* get city, state, suburbid*********** */

            $suburb_ids = "";
            $suburbnms = "";
            if ($row['suburb_ids'] != "") {
				$suburb_ids=$row['suburb_ids'];
				$suburbnms=$row['suburbnm'];
            }
            $this->json_array['data']['Login']['suburbids'] = $suburb_ids;
            $this->json_array['data']['Login']['suburbnms'] = $suburbnms;
            $this->json_array['data']['Login']['day_start_or_not'] = $attendance_count['count'];

            $subareaids = "";
            $subareaname = "";
            if ($row['subarea_ids'] != "") {
                $subareaids=$row['subarea_ids'];
				$subareaname=$row['subarea_name'];
            }
            $this->json_array['data']['Login']['subareaids'] = $subareaids;
            $this->json_array['data']['Login']['subareaname'] = $subareaname;
        }
        else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['Login']['message'] = "Invalid userid.";
        }
        return json_encode($this->json_array);
    }

    function fngetOwners($id, $role) {
        $sql = "SELECT *  FROM `tbl_user` WHERE id='$id' AND user_type='" . $role . "'";
        $proRow = $this->executeQuery($sql);
        if (mysql_num_rows($proRow) != 0) {
            $row = mysql_fetch_array($proRow);
            return $row;
        } else {
            return 0;
        }
    }

    function get_login($uname, $password) {
        $password = md5($password);
        $sql = "SELECT *  FROM `tbl_user` WHERE username='$uname' AND pwd='$password'";
        $proRow = $this->executeQuery($sql);
        if (mysql_num_rows($proRow) != 0) {
            $row = mysql_fetch_array($proRow);
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '6';
            $this->json_array['data']['Login']['id'] = $row['id'];
            $this->json_array['data']['Login']['username'] = $row['username'];
            $this->json_array['data']['Login']['firstname'] = $row['firstname'];
            $this->json_array['data']['Login']['surname'] = $row['surname'];
            $this->json_array['data']['Login']['address'] = $row['address'];
            $this->json_array['data']['Login']['email'] = $row['email'];
        } else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['Login']['message'] = "Invalid username or password.";
        }
        return json_encode($this->json_array);
    }

    //for only b2c
    function customer_login($uname, $password) {
        $password = md5($password);
        $sql = "SELECT *  FROM `tbl_customer_new` WHERE username='$uname' AND password='$password'";
        $proRow = $this->executeQuery($sql);
        if (mysql_num_rows($proRow) != 0) {
            //password,ship_addr_lat,ship_addr_long
            $row = mysql_fetch_array($proRow);
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '6';
            $this->json_array['data']['Login']['id'] = $row['id'];
            $this->json_array['data']['Login']['customer_name'] = $row['customer_name'];
            $this->json_array['data']['Login']['customer_emailid'] = $row['customer_emailid'];
            $this->json_array['data']['Login']['username'] = $row['username'];
            $this->json_array['data']['Login']['customer_phone_no'] = $row['customer_phone_no'];
            $this->json_array['data']['Login']['customer_address'] = $row['customer_address'];
            $this->json_array['data']['Login']['customer_type'] = $row['customer_type'];
            $this->json_array['data']['Login']['customer_shipping_address'] = $row['customer_shipping_address'];
        } else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['Login']['message'] = "Invalid username or password.";
        }
        return json_encode($this->json_array);
    }

    function get_login_m($uname, $password) {
        $password = md5($password);
        $sql = "SELECT *  FROM `tbl_users` WHERE username='$uname' AND passwd='$password' AND level='SalesPerson'";
        $proRow = $this->executeQuery($sql);
        if (mysql_num_rows($proRow) != 0) {
            $row = mysql_fetch_array($proRow);
            $sqlcom = "SELECT *  FROM tbl_user_company WHERE userid='" . $row['id'] . "'";
            $proRowcom = $this->executeQuery($sqlcom);
            $rowcom = mysql_fetch_array($proRowcom);
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '6';
            $this->json_array['data']['Login']['id'] = $row['id'];
            $this->json_array['data']['Login']['username'] = $row['username'];
            $this->json_array['data']['Login']['firstname'] = $row['firstname'];
            $this->json_array['data']['Login']['surname'] = $row['surname'];
            $this->json_array['data']['Login']['address'] = $row['address'];
            $this->json_array['data']['Login']['email'] = $row['email'];
        } else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['Login']['message'] = "Invalid username or password.";
        }
        return json_encode($this->json_array);
    }
	function get_pricing_userlevel($user_role){
		 $sql = "SELECT level,usertype_margin FROM tbl_user_tree  where user_role='".$user_role."' ";
        $proRow = $this->executeQuery($sql);
		$row = mysql_fetch_assoc($proRow) ;
		return $row;
	}

    function fnGetProductCat($data) {
		$countstockist=substr_count($data['parent_ids'],",");
		if($countstockist==0){
			  $sql = "SELECT * FROM pcatbrandvariant1 where  added_by_userid = '" . $data['parent_ids'] . "' 
			or added_by_userid = '0' or added_by_userid = '1' ";
		}else{
			 $sql = "SELECT * FROM pcatbrandvariant1 where  added_by_userid in (" . $data['parent_ids'] . ") 
			or added_by_userid = '0' or added_by_userid = '1' ";
		}
        //$sql = "SELECT * FROM pcatbrandvariant1 where  added_by_userid = " . $data['stockiestid'] . " or added_by_userid = '0' or added_by_userid = '1' ";
        $proRow = $this->executeQuery($sql);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '1';
        while ($row = mysql_fetch_array($proRow)) {
            $p_array_temp['brand_name'] = $row['brand_name'];
            $p_array_temp['category_name'] = $row['category_name'];

            if ($row['category_image'] != '') {
                $p_array_temp['category_image'] = SITEURL . "/templates/admin/upload/" . $row['category_image'];
            } else {
                $p_array_temp['category_image'] = "";
            }

            $p_array_temp['product_name'] = $row['product_name'];
            if ($row['product_image'] != '') {
                $p_array_temp['product_image'] = SITEURL . "/templates/admin/upload/" . $row['product_image'];
            } else {
                $p_array_temp['product_image'] = "";
            }
			$userlevel=$this->get_pricing_userlevel($data['userrole']);
			
			$product_price=$row['product_price'];
			if($userlevel['margin']!=''){
				$product_price=$row['product_price']-(($row['product_price']*$userlevel['margin'])/100);
			}
			
            $p_array_temp['product_price'] = (string)$product_price;
			$p_array_temp['product_price1'] = $row['product_price'];
            $p_array_temp['product_hsn'] = $row['product_hsn'];
            $p_array_temp['product_cgst'] = $row['product_cgst'];
            $p_array_temp['product_sgst'] = $row['product_sgst'];
            if ($row['product_barcode_img'] != '') {
                $p_array_temp['product_barcode_img'] = SITEURL . "/templates/admin/upload/" . $row['product_barcode_img'];
            } else {
                $p_array_temp['product_barcode_img'] = "";
            }
            $p_array_temp['brand_id'] = $row['brand_id'];
            $p_array_temp['category_id'] = $row['category_id'];
            $p_array_temp['product_id'] = $row['product_id']; //variant row
            $p_array_temp['product_variant_rowcnt'] = $row['product_variant_id'];
           

            $p_array_temp['weight'] = $row['product_variant1'];
            $p_array_temp['weightunit'] = $row['variant1_unit_name'];

           
            $p_array_temp['size'] = $row['product_variant2'];
            $p_array_temp['sizeunit'] = $row['variant2_unit_name'];

            $this->json_array['data']['procats'][] = $p_array_temp;
        }
        $this->json_array['data']['campaigns'] = $this->get_campaigndata();
        return json_encode($this->json_array);
    }

    function fnGetProductCat_dcp($data) {
        $dcp_id = $data['dcpuserid'];
        $sql_for_carton_assign = "SELECT tdca.no_of_carton FROM  tbl_dcp_cartons_assign  tdca                       
                         where    tdca.dcp_id= '$dcp_id'";
        $proRow_stock_assign = $this->executeQuery($sql_for_carton_assign);
        $count_row_assign= mysql_num_rows($proRow_stock_assign);
        if($count_row_assign>0){
            $sql = "SELECT viewprod.*,tds.assigned_qnty,tds.stock_qnty,tds.sale_qnty,tdca.no_of_carton 
                        FROM pcatbrandvariant viewprod 
                        LEFT JOIN tbl_dcp_stock  tds 
                            on viewprod.product_id = tds.prod_id and 
                            viewprod.product_variant_id = tds.prod_var_id  
                        LEFT JOIN tbl_dcp_cartons_assign tdca on tds.dcp_id=tdca.dcp_id 
                         where  (viewprod.added_by_userid = '0' or viewprod.added_by_userid = '1')  ";
            $proRow = $this->executeQuery($sql);
            $stock_array = array();
            $sql_for_stock = "SELECT tds.prod_id,tds.prod_var_id,tds.cartons_id,tds.assigned_qnty,tds.stock_qnty,tds.sale_qnty,tdca.no_of_carton 
                            FROM  tbl_dcp_stock  tds
                            LEFT JOIN tbl_dcp_cartons_assign tdca on tds.dcp_id=tdca.dcp_id 
                             where    tds.dcp_id= '$dcp_id'";
            $proRow_stock = $this->executeQuery($sql_for_stock);
            while ($row_stock = mysql_fetch_array($proRow_stock)) {
                $var_string = "'" . $row_stock['prod_id'] . "-" . $row_stock['prod_var_id'] . "'";
                $var_string_no = "'" . $row_stock['prod_id'] . "-" . $row_stock['prod_var_id'] . "1'";
                $var_string_carton_id = "'" . $row_stock['prod_id'] . "-" . $row_stock['prod_var_id'] . "2'";
                $stock_array[$var_string] = $row_stock['stock_qnty'];
                $stock_array[$var_string_no] = $row_stock['no_of_carton'];
                $stock_array[$var_string_carton_id] = $row_stock['cartons_id'];
            }
            //LEFT JOIN tbl_dcp_cartons_assign tdca on tds.dcp_id=tdca.dcp_id 
            //,tdca.no_of_carton 
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '1';
            while ($row = mysql_fetch_array($proRow)) {
                //echo "<pre>";print_r($row);
                $p_array_temp=array();
                $p_array_temp['brand_name'] = $row['brand_name'];
                $p_array_temp['category_name'] = $row['category_name'];

                if ($row['category_image'] != '') {
                    $p_array_temp['category_image'] = SITEURL . "/templates/admin/upload/" . $row['category_image'];
                } else {
                    $p_array_temp['category_image'] = "";
                }
                $p_array_temp['product_name'] = $row['product_name'];

                if ($row['product_image'] != '') {
                    $p_array_temp['product_image'] = SITEURL . "/templates/admin/upload/" . $row['product_image'];
                } else {
                    $p_array_temp['product_image'] = "";
                }
                $p_array_temp['product_price'] = $row['product_price'];
                $p_array_temp['product_hsn'] = $row['product_hsn'];
                $p_array_temp['product_cgst'] = $row['product_cgst'];
                $p_array_temp['product_sgst'] = $row['product_sgst'];
                if ($row['product_barcode_img'] != '') {
                    $p_array_temp['product_barcode_img'] = SITEURL . "/templates/admin/upload/" . $row['product_barcode_img'];
                } else {
                    $p_array_temp['product_barcode_img'] = "";
                }
                if ($row['product_barcode_img'] != '') {
                    $p_array_temp['product_barcode_img'] = SITEURL . "/templates/admin/upload/" . $row['product_barcode_img'];
                } else {
                    $p_array_temp['product_barcode_img'] = "";
                }
                $p_array_temp['brand_id'] = $row['brand_id'];
                $p_array_temp['category_id'] = $row['category_id'];
                $p_array_temp['product_id'] = $row['product_id']; //variant row
                $p_array_temp['product_variant_rowcnt'] = $row['product_variant_id'];

				$p_array_temp['weight'] = $row['product_variant1'];
				$p_array_temp['weightunit'] = $row['variant1_unit_name'];

				$p_array_temp['size'] = $row['product_variant2'];
				$p_array_temp['sizeunit'] = $row['variant2_unit_name'];

                $var_string1 = "'" . $row['product_id'] . "-" . $row['product_variant_id'] . "'";
                $var_string_no1 = "'" . $row['product_id'] . "-" . $row['product_variant_id'] . "1'";
                $var_string_no2 = "'" . $row['product_id'] . "-" . $row['product_variant_id'] . "2'";
                if (key_exists($var_string1, $stock_array)) {
                    $stock_qnty = $stock_array[$var_string1];
                    $carton_id = $stock_array[$var_string_no2];
                } else {
                    $stock_qnty = '0';
                    $no_of_carton = 0;
                    $carton_id = 0;
                }
                //$no_of_carton                       
                $p_array_temp['carton_quantities'] = $stock_qnty;
                $p_array_temp['carton_id'] = $carton_id;
                $p_array_temp['carton_name'] = "" . $data['clientnm'] . "_cartons" . $carton_id;

                if($stock_qnty>0){
                    $this->json_array['data']['procats'][] = $p_array_temp;
                }
            }
        }else{
            $this->json_array['data']['procats'][] = "No cartons assigned!";
        }
        
        $this->json_array['data']['campaigns'] = $this->get_campaigndata();
        return json_encode($this->json_array);
    }
    function fnGetProductCat_customer($lat,$long) { 
        $sql = "SELECT * FROM pcatbrandvariant ";
        $proRow = $this->executeQuery($sql);       
        while ($row = mysql_fetch_array($proRow)) {
            $p_array_temp['brand_name'] = $row['brand_name'];
            $p_array_temp['category_name'] = $row['category_name'];

            if ($row['category_image'] != '') {
                $p_array_temp['category_image'] = SITEURL . "/templates/admin/upload/" . $row['category_image'];
            } else {
                $p_array_temp['category_image'] = "";
            }
            $p_array_temp['product_name'] = $row['product_name'];

            if ($row['product_image'] != '') {
                $p_array_temp['product_image'] = SITEURL . "/templates/admin/upload/" . $row['product_image'];
            } else {
                $p_array_temp['product_image'] = "";
            }
            $p_array_temp['product_price'] = $row['product_price'];
            $p_array_temp['product_hsn'] = $row['product_hsn'];
            $p_array_temp['product_cgst'] = $row['product_cgst'];
            $p_array_temp['product_sgst'] = $row['product_sgst'];
            if ($row['product_barcode_img'] != '') {
                $p_array_temp['product_barcode_img'] = SITEURL . "/templates/admin/upload/" . $row['product_barcode_img'];
            } else {
                $p_array_temp['product_barcode_img'] = "";
            }
            $p_array_temp['brand_id'] = $row['brand_id'];
            $p_array_temp['category_id'] = $row['category_id'];
            $p_array_temp['product_id'] = $row['product_id']; //variant row
            $p_array_temp['product_variant_rowcnt'] = $row['product_variant_id'];
			
			$p_array_temp['weight'] = $row['product_variant1'];
			$p_array_temp['weightunit'] = $row['variant1_unit_name'];

			$p_array_temp['size'] = $row['product_variant2'];
			$p_array_temp['sizeunit'] = $row['variant2_unit_name'];

            $this->json_array['data']['procats'][] = $p_array_temp;
        }
        $this->json_array['data']['campaigns'] = $this->get_campaigndata();
        $sql_nearest_distributor=" SELECT id,firstname , (3956 * 2 * ASIN(SQRT( POWER(SIN(( $lat - `latitude`) *  pi()/180 / 2), 2) +COS( $lat * pi()/180) * COS(`latitude` * pi()/180) * POWER(SIN(( $long - `longitude`) * pi()/180 / 2), 2) )))*1.60934 as distance  
                        from tbl_user 
                        where user_type='Distributor'
                        order by distance limit 1";
        $proRow_nd = $this->executeQuery($sql_nearest_distributor);       
        while ($row_nd = mysql_fetch_array($proRow_nd)) {
             $this->json_array['data']['distributor_info']['dist_id']=$row_nd['id'];  
             $this->json_array['data']['distributor_info']['dist_name']=$row_nd['firstname'];  
             $this->json_array['data']['distributor_info']['distance']=$row_nd['distance'];  
        }
        return json_encode($this->json_array['data']);
    }

    function fnGetUnit($id) {
        $sql = "SELECT unitname FROM tbl_units WHERE id='" . $id . "'";
        $proRow = $this->executeQuery($sql);
        $row = mysql_fetch_array($proRow);
        return $row['unitname'];
    }

    function fnProfileUpdate($profileid, $name, $address, $state, $city, $suburbids, $subareaids, $mobile, $external_id) {
		
        $sql = "UPDATE tbl_user SET
		firstname='$name',address='$address',mobile='$mobile', 
		external_id='$external_id'
		where id='$profileid'";
        //,state='$state',city='$city'

        $proRow = $this->executeQuery($sql);
        $external_ids = explode(',', $external_id);
        $stockistids = join("','", $external_ids);
        $sql = "SELECT state as state_ids,city as city_ids,suburb_ids,subarea_ids 
		FROM tbl_user WHERE id in ('" . $stockistids . "')";
        $proRow = $this->executeQuery($sql);
        $state_ids1 = "";
        $city_ids1 = "";
        $suburb_ids1 = "";
        $subarea_ids1 = "";
        while ($row = mysql_fetch_array($proRow)) {
			if($row['state_ids']!='' && $row['state_ids']!='0'){
				 $state_ids1 .= $row['state_ids'] . ', ';
			}
           if($row['city_ids']!='' && $row['city_ids']!='0'){
				 $city_ids1 .= $row['city_ids'] . ', ';
			}
			if($row['suburb_ids']!='' && $row['suburb_ids']!='0'){
				 $suburb_ids1 .= $row['suburb_ids'] . ', ';
			}
			if($row['subarea_ids']!='' && $row['subarea_ids']!='0'){
				 $subarea_ids1 .= $row['subarea_ids'] . ', ';
			}
           // $city_ids1 .= $row['city_ids'] . ', ';
           // $suburb_ids1 .= $row['suburb_ids'] . ', ';
           // $subarea_ids1 .= $row['subarea_ids'] . ', ';
        }
        $state = rtrim($state_ids1, ",");
        $city = rtrim($city_ids1, ",");
        $suburbids = rtrim($suburb_ids1, ",");
        $subareaids = rtrim($subarea_ids1, ",");


        $state = implode(',', array_unique(explode(',', $state)));
        $city = implode(',', array_unique(explode(',', $city)));
        $suburbids = implode(',', array_unique(explode(',', $suburbids)));
        $subareaids = implode(',', array_unique(explode(',', $subareaids)));


          $sql = "UPDATE tbl_user SET 
				state = '$state' , 
				city = '$city' , 
				suburb_ids='$suburbids' , 
				subarea_ids = '$subareaids' 
			WHERE id = '$profileid'";
        // echo $sql;
		//die();
        $proRow = $this->executeQuery($sql);
        $sqlstockistnames = "SELECT firstname FROM tbl_user WHERE id in ('" . $stockistids . "')";
        $resultstockistnames = $this->executeQuery($sqlstockistnames);
        $stockisnames = "";
        while ($rowssnames = mysql_fetch_array($resultstockistnames)) {
            $stockisnames .= $rowssnames['firstname'] . ", ";
        }
        $stockisnames = rtrim($stockisnames, ', ');

        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '7';
        $this->json_array['data']['Profile']['id'] = $profileid;
        $this->json_array['data']['Profile']['external_id'] = $external_id;
        $this->json_array['data']['Profile']['external_id_names'] = $stockisnames;
        $this->json_array['data']['Profile']['firstname'] = $name;
        $this->json_array['data']['Profile']['address'] = $address;
        $this->json_array['data']['Profile']['state'] = $state;
        $this->json_array['data']['Profile']['city'] = $city;
        $this->json_array['data']['Profile']['suburbids'] = $suburbids;
        $this->json_array['data']['Profile']['subareaids'] = $subareaids;
        $this->json_array['data']['Profile']['mobile'] = $mobile;

        return json_encode($this->json_array);
    }
	function fnProfileUpdateVanraj($profileid, $name, $address, $state, $city, $suburbids, $subareaids, $mobile, $external_id) {
		
        $sql = "UPDATE tbl_user SET
		firstname='$name',address='$address',mobile='$mobile', 
		external_id='$external_id',
		state = '$state' , 
		city = '$city' , 
		suburb_ids='$suburbids' , 
		subarea_ids = '$subareaids' 
		where id='$profileid'";
        //,state='$state',city='$city'

        $proRow = $this->executeQuery($sql);

        $sqlstockistnames = "SELECT firstname FROM tbl_user WHERE id in ('" . $external_id . "')";
        $resultstockistnames = $this->executeQuery($sqlstockistnames);
        $stockisnames = "";
        while ($rowssnames = mysql_fetch_array($resultstockistnames)) {
            $stockisnames .= $rowssnames['firstname'] . ", ";
        }
        $stockisnames = rtrim($stockisnames, ', ');

        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '7';
        $this->json_array['data']['Profile']['id'] = $profileid;
        $this->json_array['data']['Profile']['external_id'] = $external_id;
        $this->json_array['data']['Profile']['external_id_names'] = $stockisnames;
        $this->json_array['data']['Profile']['firstname'] = $name;
        $this->json_array['data']['Profile']['address'] = $address;
        $this->json_array['data']['Profile']['state'] = $state;
        $this->json_array['data']['Profile']['city'] = $city;
        $this->json_array['data']['Profile']['suburbids'] = $suburbids;
        $this->json_array['data']['Profile']['subareaids'] = $subareaids;
        $this->json_array['data']['Profile']['mobile'] = $mobile;

        return json_encode($this->json_array);
    }

    function fnProfileUpdatemain($profileid, $name, $address, $state, $city, $mobile) {
        $sql = "UPDATE tbl_users SET firstname='$name',address='$address',state='$state',city='$city' , mobile='$mobile' where id='$profileid'";
        $proRow = $this->executeQuery($sql);
    }

    function get_state() {
        $sql = "SELECT id, name FROM tbl_state WHERE country_id='101'";
        $proRow = $this->executeQuery($sql);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '9';
        while ($row = mysql_fetch_array($proRow)) {
            $s_array_temp['name'] = $row['name'];
            $s_array_temp['id'] = $row['id'];
            $this->json_array['data']['state'][] = $s_array_temp;
        }
        return json_encode($this->json_array);
    }

    function get_city($stateid) {
        $sqlcity = "SELECT id, name,state_id FROM  tbl_city WHERE state_id='" . $stateid . "'";
        $proRowcity = $this->executeQuery($sqlcity);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '3';
        while ($rowcity = mysql_fetch_array($proRowcity)) {
            $c_array_temp['name'] = $rowcity['name'];
            $c_array_temp['id'] = $rowcity['id'];
            $c_array_temp['state_id'] = $rowcity['state_id'];
            $this->json_array['data']['city'][] = $c_array_temp;
        }
        return json_encode($this->json_array);
    }
	function get_shop_type() {
        $sqlcity = "SELECT id, shop_type FROM  tbl_shop_type ";
        $proRowcity = $this->executeQuery($sqlcity);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '3';
        while ($rowcity = mysql_fetch_array($proRowcity)) {
            $c_array_temp['shop_type'] = $rowcity['shop_type'];
            $c_array_temp['id'] = $rowcity['id'];         
            $this->json_array['data']['shop_types'][] = $c_array_temp;
        }
        return json_encode($this->json_array);
    }
	function get_serviceby_usertype($userid,$is_admin_child) {
		if($is_admin_child=='yes'){
			 $sqlcity = "SELECT id, user_type as serviceby_usertype FROM  tbl_user_tree where user_role='Admin' 
		or user_role='Superstockist' or user_role='Distributor'";
		}else{
			 $sqlcity = "SELECT id, user_type as serviceby_usertype 
			 FROM  tbl_user_tree  
			 WHERE FIND_IN_SET(user_type, 
		(SELECT parent_usertypes 
         FROM tbl_user_view 
         WHERE id=$userid) )";//tbl_user_view
		}
       
        $proRowcity = $this->executeQuery($sqlcity);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '3';
        while ($rowcity = mysql_fetch_array($proRowcity)) {
            $c_array_temp['serviceby_usertype'] = $rowcity['serviceby_usertype'];
            $c_array_temp['id'] = $rowcity['id'];         
            $this->json_array['data']['serviceby_usertype_list'][] = $c_array_temp;
        }
        return json_encode($this->json_array);
    }
	function get_serviceby_usernames($userid,$is_admin_child,$serviceby_usertypeid) {
		$this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '3';
		if($is_admin_child=='yes'){
			$sqlcity = "SELECT id, firstname as serviceby_username 
			 FROM  tbl_user where profile_id='".$serviceby_usertypeid."' ";
			$proRowcity = $this->executeQuery($sqlcity);
			while ($rowcity = mysql_fetch_array($proRowcity)) {
				$c_array_temp['serviceby_username'] = $rowcity['serviceby_username'];
				$c_array_temp['id'] = $rowcity['id'];         
				$this->json_array['data']['serviceby_username_list'][] = $c_array_temp;
			}
		}else{
			 $sqlcity = "SELECT  `outer1`.`parent_ids`, `outer1`.`parent_names`, `outer1`.`parent_usertypes`,
			 (select group_concat(`tu`.`profile_id` order by `tu`.`id` ASC separator ',') 
			from (`tbl_user_view` `inneru2` join `tbl_user` `tu` 
			on((find_in_set(`tu`.`id`,`inneru2`.`parent_ids`) > 0))) 
			where (`inneru2`.`id` = `outer1`.`id`) group by `inneru2`.`id`) 
			AS `parent_usertype_ids` 
			 FROM  tbl_user_view as outer1 			 
			 WHERE outer1.id='".$userid."'";//tbl_user_view
			 $proRowcity = $this->executeQuery($sqlcity);
			 $i=0;
			while ($rowcity = mysql_fetch_array($proRowcity)) {
				$parent_usertype_ids = explode(',', $rowcity['parent_usertype_ids']);
				$parent_ids = explode(',', $rowcity['parent_ids']);
				$parent_names = explode(',', $rowcity['parent_names']);
				$parent_usertypes = explode(',', $rowcity['parent_usertypes']);
				$key = array_search ($serviceby_usertypeid, $parent_usertype_ids);
				$c_array_temp['serviceby_username'] = $parent_names[$key];
				$c_array_temp['id'] = $parent_ids[$key];
				$this->json_array['data']['serviceby_username_list'][] = $c_array_temp;
			}
		}
        return json_encode($this->json_array);
    }

    function get_suburb($cityid) {
        $sqlcity = "SELECT id,suburbnm FROM tbl_area WHERE cityid='" . $cityid . "' AND isdeleted!='1'";
        $proRowcity = $this->executeQuery($sqlcity);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '2';
        while ($rowcity = mysql_fetch_array($proRowcity)) {
            $c_array_temp['suburbnm'] = $rowcity['suburbnm'];
            $c_array_temp['id'] = $rowcity['id'];
            $this->json_array['data']['suburb'][] = $c_array_temp;
        }
        return json_encode($this->json_array);
    }

    /*     * ******************* City, state and suburb by id ************************** */

    function get_stateid($id) {
        $sql = "SELECT id, name FROM tbl_state WHERE id='" . $id . "'";
        $proRow = $this->executeQuery($sql);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '2';
        $row = mysql_fetch_array($proRow);
        return $row;
    }

    function get_cityid($cityid) {
        $sqlcity = "SELECT id, name FROM  tbl_city WHERE id='" . $cityid . "'";
        $proRowcity = $this->executeQuery($sqlcity);
        $rowcity = mysql_fetch_array($proRowcity);
        return $rowcity;
    }

    function get_suburbid($suburbid) {
        $sqlcity = "SELECT id,suburbnm FROM tbl_area WHERE id='" . $suburbid . "'";
        $proRowcity = $this->executeQuery($sqlcity);
        $rowsuburb = mysql_fetch_array($proRowcity);
        return $rowsuburb;
    }

    /*     * **************************************************************************** */

    function fnAddNewStore($salespersonid, $store_name, $owner_name, 
	$contact_no, $store_Address, $lat, $lng, $city, $state, $parent_ids, $suburbid, $subarea_id, $billing_name, 
	$bank_acc_name, $bank_acc_no, $bank_name, $bank_b_name, $bank_ifsc, $gst_number,$service_by_usertype_id,$service_by_user_id,$shop_type_id) {
       
		 /* Get  State, District, Taluka from name and pincode */
        $sql_state = " SELECT  id,name	FROM  `tbl_state` WHERE name like '$state' and country_id ='101' limit 1";
        $result_states = $this->executeQuery($sql_state);
        $state_rowcount = mysql_num_rows($result_states);
        $state_details = mysql_fetch_array($result_states);
        $stateid = $state_details['id'];

        $sql_city = " SELECT  id,name	FROM  `tbl_city` WHERE name like '$city' and state_id='$stateid' limit 1";
        $result_citys = $this->executeQuery($sql_city);
        $city_rowcount = mysql_num_rows($result_citys);
        $city_details = mysql_fetch_array($result_citys);
        $cityid = $city_details['id'];

        $sql_area = " SELECT  id,suburbnm	FROM  `tbl_area` WHERE suburbnm like '$suburbid' and cityid='$cityid' limit 1";
        $result_areas = $this->executeQuery($sql_area);
        $area_rowcount = mysql_num_rows($result_areas);
        $area_details = mysql_fetch_array($result_areas);
        $suburbidnew = $area_details['id'];

        $sql_subarea = " SELECT  subarea_id,subarea_name	FROM  `tbl_subarea` 
		WHERE subarea_name like '$subarea_id' and state_id='$stateid' 
		and city_id='$cityid' and suburb_id='$suburbidnew' limit 1";
        $result_subareas = $this->executeQuery($sql_subarea);
        $subarea_rowcount = mysql_num_rows($result_subareas);
        $subarea_details = mysql_fetch_array($result_subareas);
        $subareaidnew = $subarea_details['id'];

        //echo "st:".$state_rowcount." city:".$city_rowcount." area:".$area_rowcount." subarea:".$subareaidnew;
        if ($area_rowcount != 1) {
            $sql = "INSERT INTO tbl_area(cityid,stateid,suburbnm) VALUES ( '" . $cityid . "','" . $stateid . "','" . $suburbid . "' )";
            $proRow = $this->executeQuery($sql);
            $suburbidnew = mysql_insert_id();

            $sql = "INSERT INTO tbl_subarea(state_id,city_id,suburb_id,subarea_name) VALUES ( '" . $stateid . "','" . $cityid . "','" . $suburbidnew . "','" . $subarea_id . "' )";
            $proRow = $this->executeQuery($sql);
            $subareaidnew = mysql_insert_id();
        }
        if ($subarea_rowcount != 1) {
            $sql = "INSERT INTO tbl_subarea(state_id,city_id,suburb_id,subarea_name) VALUES ( '" . $stateid . "','" . $cityid . "','" . $suburbidnew . "','" . $subarea_id . "' )";
            $proRow = $this->executeQuery($sql);
            $subareaidnew = mysql_insert_id();
        }

        $added_on = date("Y-m-d H:i:s");
		
		
		$sql = "INSERT INTO tbl_shops(name, address, city, state, contact_person, mobile,shop_added_by,shop_type_id,
		latitude,longitude,suburbid,subarea_id,user_parent_ids,service_by_usertype_id, service_by_user_id,
		billing_name,bank_acc_name,bank_acc_no,bank_name,bank_b_name,bank_ifsc,gst_number,added_on
		)
		VALUES('" . $store_name . "','" . $store_Address . "','" . $cityid . "',
		'" . $stateid . "','" . $owner_name . "','" . $contact_no . "','" . $salespersonid . "','" . $shop_type_id . "',
		'" . $lat . "','" . $lng . "','" . $suburbidnew . "','" . $subareaidnew . "','" . $parent_ids . "',
		'" . $service_by_usertype_id . "','" . $service_by_user_id . "',
		'" . $billing_name . "','" . $bank_acc_name . "','" . $bank_acc_no . "','" . $bank_name . "','" . $bank_b_name . "',
		'" . $bank_ifsc . "','" . $gst_number . "','" . $added_on . "')";
		
        //exit();
        $proRow = $this->executeQuery($sql);
        $last_id = mysql_insert_id();

        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '1';
        $this->json_array['data']['Store']['msg'] = "Store added successfully.";
        $this->json_array['data']['Store']['id'] = $last_id;

        return json_encode($this->json_array);
    }
	function fnAddNewLead($lead) {       
		$parent_ids=$lead['parent_ids'];
		$state=$lead['state'];
		$city=$lead['city'];
		$suburbid=$lead['suburbid'];
		$subarea_id=$lead['subarea_id'];
		

        /* Get  State, District, Taluka from name and pincode */
         $sql_state = " SELECT  id,name	FROM  `tbl_state` WHERE name like '$state' and country_id ='101' limit 1";
        $result_states = $this->executeQuery($sql_state);
        $state_rowcount = mysql_num_rows($result_states);
        $state_details = mysql_fetch_array($result_states);
        $stateid = $state_details['id'];

         $sql_city = " SELECT  id,name	FROM  `tbl_city` WHERE name like '$city' and state_id='$stateid' limit 1";
        $result_citys = $this->executeQuery($sql_city);
        $city_rowcount = mysql_num_rows($result_citys);
        $city_details = mysql_fetch_array($result_citys);
        $cityid = $city_details['id'];

         $sql_area = " SELECT  id,suburbnm	FROM  `tbl_area` WHERE suburbnm like '$suburbid' and cityid='$cityid' limit 1";
        $result_areas = $this->executeQuery($sql_area);
        $area_rowcount = mysql_num_rows($result_areas);
        $area_details = mysql_fetch_array($result_areas);
        $suburbidnew = $area_details['id'];

         $sql_subarea = " SELECT  subarea_id,subarea_name	FROM  `tbl_subarea` 
		WHERE subarea_name like '$subarea_id' and state_id='$stateid' 
		and city_id='$cityid' and suburb_id='$suburbidnew' limit 1";
        $result_subareas = $this->executeQuery($sql_subarea);
        $subarea_rowcount = mysql_num_rows($result_subareas);
        $subarea_details = mysql_fetch_array($result_subareas);
        $subareaidnew = $subarea_details['id'];

        //echo "st:".$state_rowcount." city:".$city_rowcount." area:".$area_rowcount." subarea:".$subareaidnew;
        if ($area_rowcount == 0) {
            $sql = "INSERT INTO tbl_area(cityid,stateid,suburbnm) VALUES ( '" . $cityid . "','" . $stateid . "','" . $suburbid . "' )";
            $proRow = $this->executeQuery($sql);
            $suburbidnew = mysql_insert_id();

             $sql = "INSERT INTO tbl_subarea(state_id,city_id,suburb_id,subarea_name) VALUES ( '" . $stateid . "','" . $cityid . "','" . $suburbidnew . "','" . $subarea_id . "' )";
            $proRow = $this->executeQuery($sql);
            $subareaidnew = mysql_insert_id();
        }
        if ($subarea_rowcount == 0) {
            $sql = "INSERT INTO tbl_subarea(state_id,city_id,suburb_id,subarea_name) VALUES ( '" . $stateid . "','" . $cityid . "','" . $suburbidnew . "','" . $subarea_id . "' )";
            $proRow = $this->executeQuery($sql);
            $subareaidnew = mysql_insert_id();
        }

        $added_on = date("Y-m-d H:i:s");
		
		
	   $sql = "INSERT INTO tbl_leads(name, address, city, state, contact_person, mobile,shop_added_by,shop_type_id,
		latitude,longitude,suburbid,subarea_id,user_parent_ids,service_by_usertype_id, service_by_user_id,
		billing_name,bank_acc_name,bank_acc_no,bank_name,bank_b_name,bank_ifsc,gst_number,added_on
		)
		VALUES('" . $lead['store_name'] . "','" . 
		$lead['store_Address'] . "','" . $cityid . "','" . $stateid . "','" . 
		$lead['owner_name'] . "','" . $lead['contact_no'] . "','" . $lead['salespersonid'] . "','" . $lead['shop_type_id'] . "','" . 
		$lead['lat'] . "','" . $lead['lng'] . "','" . $suburbidnew . "','" . $subareaidnew 
		. "','" . $parent_ids . "',
		'" . $lead['service_by_usertype_id'] . "','" . $lead['service_by_user_id'] . "',
		'" . $lead['billing_name'] . "','" . $lead['bank_acc_name'] . "','" . $lead['bank_acc_no'] . "'
		,'" . $lead['bank_name'] . "','" . $lead['bank_b_name'] . "',
		'" . $lead['bank_ifsc'] . "','" . $lead['gst_number'] . "',now())";
		
		$added_reminder = date("Y-m-d H:i:s",strtotime($lead['reminder']));
		
        //exit();
        $proRow = $this->executeQuery($sql);
        $last_id = mysql_insert_id();
		
		  $sql = "INSERT INTO tbl_lead_details(`shop_id`, `reminder`, `visited_date`, `notification`,
		`lead_status`, `created_on`)
		VALUES('" . $last_id . "','" . $added_reminder . "',
		now(),'" . $lead['notification'] . "','" . $lead['lead_status'] . "',
		now())";
		 $proRow = $this->executeQuery($sql);

        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '1';
        $this->json_array['data']['Store']['msg'] = "Lead added successfully.";
        $this->json_array['data']['Store']['id'] = $last_id;

        return json_encode($this->json_array);
    }

    function fnAddNewStorecommon($salespersonid, $store_name, $owner_name, $contact_no, $store_Address, $lat, $lng, $city, $state, $distributorid, $suburbid, $subarea_id) {

        $sqls = " SELECT 
		`external_id`		
		FROM  `tbl_user` 		
		WHERE id='$salespersonid'";

        $proRowUsers = $this->executeQuery($sqls);
        $rows = mysql_fetch_array($proRowUsers);
        $stockist_id = $rows['external_id'];

        $sqlss = " SELECT 
		`external_id`		
		FROM  `tbl_user` 		
		WHERE id='$stockist_id'";

        $proRowUserss = $this->executeQuery($sqlss);
        $rowss = mysql_fetch_array($proRowUserss);
        $sup_stockist = $rowss['external_id'];

        /* Get Stockist's State, District, Taluka assign to shop */
        $sql_wa = " SELECT 
		 `state_ids`, `city_ids`, `suburb_ids`, `subarea_ids` 	
		FROM  `tbl_user` 		
		WHERE id='$stockist_id'";

        $user_wa = $this->executeQuery($sql_wa);
        $user_wa_details = mysql_fetch_array($user_wa);
        $state = $user_wa_details['state_ids'];
        $city = $user_wa_details['city_ids'];
        $suburbid = $user_wa_details['suburb_ids'];
        $subarea_id = $user_wa_details['subarea_ids'];

        $sql = "INSERT INTO tbl_shops(name, address, city, state, contact_person, mobile,shop_added_by,latitude,longitude,suburbid,subarea_id,sstockist_id,stockist_id)VALUES('" . $store_name . "','" . $store_Address . "','" . $city . "','" . $state . "','" . $owner_name . "','" . $contact_no . "','" . $salespersonid . "','" . $lat . "','" . $lng . "','" . $suburbid . "','" . $subarea_id . "','" . $sup_stockist . "','" . $stockist_id . "')";

        $proRow = $this->executeQuery($sql);
        $last_id = mysql_insert_id();

       
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '1';
        $this->json_array['data']['Store']['msg'] = "Store added successfully.";
        $this->json_array['data']['Store']['id'] = $last_id;

        return json_encode($this->json_array);
    }
	function get_shoplist($userid) {
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '9';

        $sql = "SELECT GROUP_CONCAT(external_id) AS all_external_id 
	  FROM tbl_user WHERE id = '" . $userid . "'";

        $extIdResult = $this->executeQuery($sql);
        $rowcount = mysql_num_rows($extIdResult);
        $extIdRow = mysql_fetch_array($extIdResult);

        if ($rowcount > 0 && $extIdRow['all_external_id'] != "") {

            $sql = " SELECT 
			tbl_shops.id,  tbl_shops.name,  tbl_shops.address,  
			tbl_shops.city,  tbl_shops.state,  tbl_shops.suburbid, 
			tbl_shops.contact_person,  tbl_shops.mobile,  tbl_shops.shop_added_by, 
			tbl_shops.latitude,  tbl_shops.longitude,  closedday,  
			opentime,  closetime,  
			tbl_shops.gst_number, tbl_shops.subarea_id, subarea_name, tbl_shops.state, tbl_state.name as state_name, tbl_shops.city, tbl_city.name as city_name, tbl_area.suburbnm as suburb  , tbl_shops.stockist_id , tbl_user.firstname       
		FROM tbl_shops     
		LEFT JOIN tbl_subarea ON tbl_shops.subarea_id = tbl_subarea.subarea_id     
		LEFT JOIN tbl_state ON tbl_state.id = tbl_shops.state     
		LEFT JOIN tbl_city ON tbl_city.id = tbl_shops.city     
		LEFT JOIN  tbl_user ON  tbl_user.id = tbl_shops.stockist_id     
		LEFT JOIN tbl_area ON tbl_area.id = tbl_shops.suburbid  WHERE tbl_shops.stockist_id IN (" . $extIdRow['all_external_id'] . ") AND tbl_shops.isdeleted!='1' ";

            $proRow = $this->executeQuery($sql);
            while ($row = mysql_fetch_array($proRow)) {
                $shop_array_temp['id'] = $row['id'];
                $shop_array_temp['name'] = $row['name'];
                $shop_array_temp['address'] = $row['address'];
                $shop_array_temp['contact_person'] = $uname;
                $shop_array_temp['lat'] = $row['latitude'];
                $shop_array_temp['lon'] = $row['longitude'];
                $shop_array_temp['closedday'] = $this->fnGetday($row['closedday']);
                $shop_array_temp['opentime'] = $row['opentime'];
                $shop_array_temp['closetime'] = $row['closetime'];
                $shop_array_temp['mobile'] = $row['mobile'];
                $shop_array_temp['gst_number'] = $row['gst_number'];
                $shop_array_temp['city_id'] = $row['city'];
                $shop_array_temp['city'] = $row['city_name'];
                $shop_array_temp['state_id'] = $row['state'];
                $shop_array_temp['state'] = $row['state_name'];
                $shop_array_temp['suburb_id'] = $row['suburbid'];
                $shop_array_temp['suburb'] = $row['suburb'];
                $shop_array_temp['subarea_id'] = $row['subarea_id'];
                $shop_array_temp['subarea_name'] = $row['subarea_name'];
                $shop_array_temp['destributorid'] = $row['stockist_id'];
                $shop_array_temp['destributorname'] = $row['firstname'];


                $this->json_array['data']['shops'][] = $shop_array_temp;
            }
        } else {
            $this->json_array['data']['shops'][] = "Stockist not available";
        }
        return json_encode($this->json_array);
    }
    function get_shoplistvanraj($userid) {
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '9';
		
		 $sql1 = "SELECT state as state_ids,city as city_ids,suburb_ids,subarea_ids 
		FROM tbl_user WHERE id = '".$userid."'";
        $proRow1 = $this->executeQuery($sql1);		
       
		if (mysql_num_rows($proRow1) != 0) {
			$state_ids1 = "";
			$city_ids1 = "";
			$suburb_ids1 = "";
			$subarea_ids1 = "";
			while ($row1 = mysql_fetch_array($proRow1)) {
				$state_ids1 .= $row1['state_ids'] . ', ';
				$city_ids1 .= $row1['city_ids'] . ', ';
				$suburb_ids1 .= $row1['suburb_ids'] . ', ';
				$subarea_ids1 .= $row1['subarea_ids'] . ', ';
			}
			$state = rtrim($state_ids1, ", ");
			$city = rtrim($city_ids1, ", ");
			$suburbids = rtrim($suburb_ids1, ", ");
			$subareaids = rtrim($subarea_ids1, ", ");
			$state = implode(',', array_unique(explode(',', $state)));
			$city = implode(',', array_unique(explode(',', $city)));
			$suburbids = implode(',', array_unique(explode(',', $suburbids)));
			$subareaids = implode(',', array_unique(explode(',', $subareaids)));
			$where_condition='';
			if($state!=''){
				$where_condition  .=" AND tbl_shops.state IN (" . $state . ") ";
			}
			if($city!=''){
				$where_condition  .=" 	AND tbl_shops.city IN (" . $city . ") ";
			}
			if($suburbids!=''){
				$where_condition  .=" AND tbl_shops.suburbid IN (" . $suburbids . ") ";
			}
			if($subareaids!=''){
				$where_condition  .=" AND tbl_shops.subarea_id IN (" . $subareaids . ") ";
			}
			
				$sql = " SELECT 
				tbl_shops.id,  tbl_shops.name,  tbl_shops.address,  
				tbl_shops.city,  tbl_shops.state,  tbl_shops.suburbid, 
				tbl_shops.contact_person,  tbl_shops.mobile,  tbl_shops.shop_added_by,tbl_shops.shop_type_id, 
				(select shop_type from tbl_shop_type where id=tbl_shops.shop_type_id) as shop_type,
				tbl_shops.latitude,  tbl_shops.longitude,  closedday,  
				opentime,  closetime,  
				tbl_shops.gst_number, tbl_shops.subarea_id, tbl_subarea.subarea_name,  tbl_state.name as state_name, 
				tbl_shops.city, tbl_city.name as city_name, tbl_area.suburbnm as suburb  ,
				tbl_user_view.parent_ids, tbl_user_view.parent_names      
			FROM tbl_shops     
			LEFT JOIN tbl_subarea ON tbl_shops.subarea_id = tbl_subarea.subarea_id     
			LEFT JOIN tbl_state ON tbl_state.id = tbl_shops.state     
			LEFT JOIN tbl_city ON tbl_city.id = tbl_shops.city   
			LEFT JOIN tbl_area ON tbl_area.id = tbl_shops.suburbid 
			LEFT JOIN  tbl_user_view ON  tbl_user_view.id = tbl_shops.shop_added_by 
			WHERE tbl_shops.isdeleted!='1' $where_condition ";

				$proRow = $this->executeQuery($sql);
				if (mysql_num_rows($proRow) != 0) {
					while ($row = mysql_fetch_array($proRow)) {
						$shop_array_temp['id'] = $row['id'];
						$shop_array_temp['name'] = $row['name'];
						$shop_array_temp['shop_type_id'] = $row['shop_type_id'];
						$shop_array_temp['shop_type'] = $row['shop_type'];
						$shop_array_temp['address'] = $row['address'];
						$shop_array_temp['contact_person'] = $uname;
						$shop_array_temp['lat'] = $row['latitude'];
						$shop_array_temp['lon'] = $row['longitude'];
						$shop_array_temp['closedday'] = $this->fnGetday($row['closedday']);
						$shop_array_temp['opentime'] = $row['opentime'];
						$shop_array_temp['closetime'] = $row['closetime'];
						$shop_array_temp['mobile'] = $row['mobile'];
						$shop_array_temp['gst_number'] = $row['gst_number'];
						$shop_array_temp['city_id'] = $row['city'];
						$shop_array_temp['city'] = $row['city_name'];
						$shop_array_temp['state_id'] = $row['state'];
						$shop_array_temp['state'] = $row['state_name'];
						$shop_array_temp['suburb_id'] = $row['suburbid'];
						$shop_array_temp['suburb'] = $row['suburb'];
						$shop_array_temp['subarea_id'] = $row['subarea_id'];
						$shop_array_temp['subarea_name'] = $row['subarea_name'];
						$shop_array_temp['parent_ids'] = $row['parent_ids'];
						$shop_array_temp['parent_names'] = $row['parent_names'];
						$this->json_array['data']['shops'][] = $shop_array_temp;
					}
				}else{
					$this->json_array['data']['shops'][] = "Shop not available for this user area!";
				}
		}else{
			$this->json_array['data']['shops'][] = "Shop not available for this user area!";
		}
        return json_encode($this->json_array);
    }
	function get_shoplist_shopkeeper($userid) {

        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '9';
       
            $sql = " SELECT 
			tbl_shops.id,  tbl_shops.name,  tbl_shops.address,  
			tbl_shops.city,  tbl_shops.state,  tbl_shops.suburbid, 
			tbl_shops.contact_person,  tbl_shops.mobile,  tbl_shops.shop_added_by,tbl_shops.shop_type_id, 
				(select shop_type from tbl_shop_type where id=tbl_shops.shop_type_id) as shop_type, 
			tbl_shops.latitude,  tbl_shops.longitude,  closedday,  
			opentime,  closetime,  
			tbl_shops.gst_number, tbl_shops.subarea_id, tbl_subarea.subarea_name, 
			tbl_shops.state, tbl_state.name as state_name, tbl_shops.city, tbl_city.name as city_name,
			tbl_area.suburbnm as suburb  ,
			tbl_user_view.parent_ids, tbl_user_view.parent_names       
		FROM tbl_shops     
		LEFT JOIN tbl_subarea ON tbl_shops.subarea_id = tbl_subarea.subarea_id     
		LEFT JOIN tbl_state ON tbl_state.id = tbl_shops.state     
		LEFT JOIN tbl_city ON tbl_city.id = tbl_shops.city 
		LEFT JOIN  tbl_user_view ON  tbl_user_view.id = tbl_shops.shop_added_by 
		LEFT JOIN tbl_area ON tbl_area.id = tbl_shops.suburbid  WHERE tbl_shops.shop_owner_id ='".$userid."' AND tbl_shops.isdeleted!='1' ";

            $proRow = $this->executeQuery($sql);
			  $rowcount = mysql_num_rows($proRow);
		  if($rowcount>0){
			   while ($row = mysql_fetch_array($proRow)) {
					$shop_array_temp['id'] = $row['id'];
					$shop_array_temp['name'] = $row['name'];
					$shop_array_temp['shop_type_id'] = $row['shop_type_id'];
					$shop_array_temp['shop_type'] = $row['shop_type'];
					$shop_array_temp['address'] = $row['address'];
					$shop_array_temp['contact_person'] = $uname;
					$shop_array_temp['lat'] = $row['latitude'];
					$shop_array_temp['lon'] = $row['longitude'];
					$shop_array_temp['closedday'] = $this->fnGetday($row['closedday']);
					$shop_array_temp['opentime'] = $row['opentime'];
					$shop_array_temp['closetime'] = $row['closetime'];
					$shop_array_temp['mobile'] = $row['mobile'];
					$shop_array_temp['gst_number'] = $row['gst_number'];
					$shop_array_temp['city_id'] = $row['city'];
					$shop_array_temp['city'] = $row['city_name'];
					$shop_array_temp['state_id'] = $row['state'];
					$shop_array_temp['state'] = $row['state_name'];
					$shop_array_temp['suburb_id'] = $row['suburbid'];
					$shop_array_temp['suburb'] = $row['suburb'];
					$shop_array_temp['subarea_id'] = $row['subarea_id'];
					$shop_array_temp['subarea_name'] = $row['subarea_name'];
					$shop_array_temp['parent_ids'] = $row['parent_ids'];
					$shop_array_temp['parent_names'] = $row['parent_names'];
					$this->json_array['data']['shops'][] = $shop_array_temp;
				}
		  }else{
			   $this->json_array['data']['shops'][] = "Shop not available";
		  }
        return json_encode($this->json_array);
    }
	function get_leadlist($userid) {
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '9';
		$sql = " SELECT tbl_leads.id as leadid,  tbl_leads.name,  tbl_leads.address,  
					tbl_leads.city,  tbl_leads.state,  tbl_leads.suburbid, 
					tbl_leads.contact_person,  tbl_leads.mobile,  tbl_leads.shop_added_by, 
					tbl_leads.latitude,  tbl_leads.longitude,  closedday,  
					opentime,  closetime,  
					tbl_leads.gst_number, tbl_leads.subarea_id, tbl_subarea.subarea_name, 
					tbl_leads.state, tbl_state.name as state_name, tbl_leads.city, 
					tbl_city.name as city_name, tbl_area.suburbnm as suburb  , 
					tbl_user_view.parent_ids, tbl_user_view.parent_names      
				FROM tbl_leads     
				LEFT JOIN tbl_subarea ON tbl_leads.subarea_id = tbl_subarea.subarea_id     
				LEFT JOIN tbl_state ON tbl_state.id = tbl_leads.state     
				LEFT JOIN tbl_city ON tbl_city.id = tbl_leads.city  
				LEFT JOIN  tbl_user_view ON  tbl_user_view.id = tbl_leads.shop_added_by 
				LEFT JOIN tbl_area ON tbl_area.id = tbl_leads.suburbid  
				WHERE tbl_leads.shop_added_by='".$userid."'  
					AND tbl_leads.isdeleted!='1' ";

		$proRow = $this->executeQuery($sql);
		 $rowcount = mysql_num_rows($proRow);
		if($rowcount>0){
            while ($row = mysql_fetch_array($proRow)) {
                $shop_array_temp['id'] = $row['leadid'];
				
				 $sql3 = "SELECT max(id) as lead_detail_id,reminder,notification,lead_status as lead_status 
				 FROM `tbl_lead_details` 
				 where `shop_id` = '".$row['leadid']."' group by id ";
				 $result3 = $this->executeQuery($sql3);
				 $lead_status='';$lead_detail_id='';
				 while($row3 = mysql_fetch_array($result3))
				 {
					$lead_status=$row3['lead_status'];
					$lead_detail_id=$row3['lead_detail_id'];
					$reminder=$row3['reminder'];
					$notification=$row3['notification'];
				 }
				 
                $shop_array_temp['name'] = $row['name'];
                $shop_array_temp['address'] = $row['address'];
                $shop_array_temp['contact_person'] = $uname;
                $shop_array_temp['lat'] = $row['latitude'];
                $shop_array_temp['lon'] = $row['longitude'];
                $shop_array_temp['closedday'] = $this->fnGetday($row['closedday']);
                $shop_array_temp['opentime'] = $row['opentime'];
                $shop_array_temp['closetime'] = $row['closetime'];
                $shop_array_temp['mobile'] = $row['mobile'];
                $shop_array_temp['gst_number'] = $row['gst_number'];
                $shop_array_temp['city_id'] = $row['city'];
                $shop_array_temp['city'] = $row['city_name'];
                $shop_array_temp['state_id'] = $row['state'];
                $shop_array_temp['state'] = $row['state_name'];
                $shop_array_temp['suburb_id'] = $row['suburbid'];
                $shop_array_temp['suburb'] = $row['suburb'];
                $shop_array_temp['subarea_id'] = $row['subarea_id'];
                $shop_array_temp['subarea_name'] = $row['subarea_name'];
                $shop_array_temp['parent_ids'] = $row['parent_ids'];
                $shop_array_temp['parent_names'] = $row['parent_names'];
				$shop_array_temp['lead_detail_id'] = $lead_detail_id;
				$shop_array_temp['lead_status'] = $lead_status;
				$lead_status_name=array('','Pending','Confirmed','Cancelled','Added_in_shop');
				$shop_array_temp['lead_status_name']=$lead_status_name[$lead_status];
				$shop_array_temp['reminder'] = $reminder;
				$shop_array_temp['reminder_date'] = date("d/m/Y",strtotime($reminder));
				$shop_array_temp['reminder_time'] = date("H:i:s",strtotime($reminder));
				$shop_array_temp['notification'] = $notification;
                $this->json_array['data']['leads'][] = $shop_array_temp;
            }
		}else {
            $this->json_array['data']['leads'][] = "Lead not available";
        }
        return json_encode($this->json_array);
    }

    //new service for chetrans
    function get_shoplist_by_latlong($userid, $lat, $lng) {

        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '9';

        $sql = "SELECT GROUP_CONCAT(external_id) AS all_external_id 
	  FROM tbl_user WHERE id = '" . $userid . "'";

        $extIdResult = $this->executeQuery($sql);
        $rowcount = mysql_num_rows($extIdResult);
        $extIdRow = mysql_fetch_array($extIdResult);

        if ($rowcount > 0 && $extIdRow['all_external_id'] != "") {

            $sql = " SELECT 
			tbl_shops.id,  tbl_shops.name,  tbl_shops.address,  
			tbl_shops.city,  tbl_shops.state,  tbl_shops.suburbid, 
			tbl_shops.contact_person,  tbl_shops.mobile,  tbl_shops.shop_added_by, 
			tbl_shops.latitude,  tbl_shops.longitude,  closedday,  
			opentime,  closetime,  
			tbl_shops.gst_number, tbl_shops.subarea_id, subarea_name, tbl_shops.state, tbl_state.name as state_name, tbl_shops.city, tbl_city.name as city_name, tbl_area.suburbnm as suburb  , tbl_shops.stockist_id , tbl_user.firstname       
		FROM tbl_shops     
		LEFT JOIN tbl_subarea ON tbl_shops.subarea_id = tbl_subarea.subarea_id     
		LEFT JOIN tbl_state ON tbl_state.id = tbl_shops.state     
		LEFT JOIN tbl_city ON tbl_city.id = tbl_shops.city     
		LEFT JOIN  tbl_user ON  tbl_user.id = tbl_shops.stockist_id     
		LEFT JOIN tbl_area ON tbl_area.id = tbl_shops.suburbid  
		WHERE tbl_shops.stockist_id IN (" . $extIdRow['all_external_id'] . ") AND tbl_shops.isdeleted!='1' 
		AND latitude IS NOT NULL AND longitude IS NOT NULL";

            $proRow = $this->executeQuery($sql);
            $rowcountshops = mysql_num_rows($proRow);
            $countrec = 0;
            $shop_array_temp123 = array();
            while ($row = mysql_fetch_array($proRow)) {
                $shop_array_temp['id'] = $row['id'];
                $shop_array_temp['lat'] = $row['latitude'];
                $shop_array_temp['lon'] = $row['longitude'];
                $google_d = $this->GetDrivingDistance($shop_array_temp['lat'], $lat, $shop_array_temp['lon'], $lng);
                $shop_array_temp['google_d'] = $google_d['distance'];

                $shop_array_temp123[] = $shop_array_temp;
            }
            //echo "<pre>";print_r($shop_array_temp123);die();
            $shop_array_temp123 = $this->array_sort($shop_array_temp123, 'google_d', SORT_ASC);
            $count_sd = count($shop_array_temp123) - 1;
            for ($i = 0; $i < $count_sd; $i++) {
                $shop_array_temp1234[$i]['lat_s'] = $shop_array_temp123[$i]['lat'];
                $shop_array_temp1234[$i]['lon_s'] = $shop_array_temp123[$i]['lon'];
                $shop_array_temp1234[$i]['lat_d'] = $shop_array_temp123[$i + 1]['lat'];
                $shop_array_temp1234[$i]['lon_d'] = $shop_array_temp123[$i + 1]['lon'];
                $shop_array_temp1234[$i]['google_d'] = $shop_array_temp123[$i]['google_d'];
            }
            $this->json_array['data']['shops'] = $shop_array_temp1234;
        } else {
            $this->json_array['data']['shops'][] = "Stockist not available";
        }
        return json_encode($this->json_array);
    }

    //USER DEFINED FUNCTION FOR SORTING BY KEY 
    function array_sort($array, $on, $order = SORT_ASC) {

        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }

    function fnGetday($dayid) {
        $str = "";
        switch ($dayid) {
            case '1':
                $str = "Monday";
                break;
            case '2':
                $str = "Tuesday";
                break;
            case '3':
                $str = "Wednesday";
                break;
            case '4':
                $str = "Thursday";
                break;
            case '5':
                $str = "Friday";
                break;
            case '6':
                $str = "Saturday";
                break;
            case '7':
                $str = "Sunday";
                break;
        }
        return $str;
    }

    function fngetStatenm($id) {
        $sql = "SELECT `id`,`name` FROM tbl_state WHERE id='" . $id . "'";
        $proRow = $this->executeQuery($sql);
        $row = mysql_fetch_array($proRow);
        return $row['name'];
    }

    function fngetCitynm($id) {
        $sql = "SELECT `id`,`name` FROM tbl_city WHERE id='" . $id . "'";
        $proRow = $this->executeQuery($sql);
        $row = mysql_fetch_array($proRow);
        return $row['name'];
    }
	

    function fngetSuburbnm($id) {
        $sql = "SELECT `suburbnm` FROM  tbl_area WHERE id='" . $id . "'";
        $proRow = $this->executeQuery($sql);
        $row = mysql_fetch_array($proRow);
        return $row['suburbnm'];
    }

    function get_orderdata($userid, $pageid) {
        $npageid = $pageid * 10;
        if ($pageid == '1')
            $limit = 0;
        else {
            $limit = ($pageid - 1) * 10;
        }
        $sql = "select shops.name as shopname,OA.shop_id, OA.order_date,  
						OA.order_no, VO.product_variant_weight1,
						VO.product_variant_unit1,
						SUM(VO.p_cost_cgst_sgst) as totalcost,
						count(VO.order_id) as variantunit, 
		IF(OA.offer_provided = 0,'Offer Applied','Offer Not Applied') AS offer_provided_val, VO.cat_id

		FROM `tbl_order_details` VO
		LEFT JOIN tbl_orders OA on OA.id=VO.order_id
		LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
		WHERE 
		OA.ordered_by='" . $userid . "' AND DATE_FORMAT(OA.order_date,'%Y-%m-%d')=DATE(NOW())
		GROUP BY VO.order_id limit $limit,$npageid "; //
        $proRow = $this->executeQuery($sql);

        if ($rowcount = mysql_num_rows($proRow) > 0) {
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '9';
            while ($row = mysql_fetch_array($proRow)) {
                $tcost = $row['totalcost'];

                $sql_free_pro = "SELECT COUNT(tbl_variant_order.id) as quantity_free FROM `tbl_variant_order` INNER JOIN `tbl_order_app` ON tbl_order_app.id = tbl_variant_order.orderappid WHERE campaign_sale_type='free' AND OA.cat_id = " . $row['cat_id'];
                $result_free_pro = mysqli_query($con, $sql_free_pro);
                $row_free_pro = mysqli_fetch_assoc($result_free_pro);
                $free_quantity = 0;
                $sale_quantity = $row['variantunit'];
                if ($row_free_pro['quantity_free'] > 0) {
                    $free_quantity = $row_free_pro['quantity_free'];
                    $sale_quantity = $row['variantunit'] - $free_quantity;
                }
                $ohistory_array_temp['tcost'] = number_format($tcost, 2, '.', ''); //$rowtc['tcost'];
                $ohistory_array_temp['free_quantity'] = $free_quantity;
                $ohistory_array_temp['sale_quantity'] = $sale_quantity;
                $ohistory_array_temp['quantity'] = $row['variantunit'];
                $ohistory_array_temp['orderid'] = $row['order_no'];
                $ohistory_array_temp['order_date'] = $row['order_date'];
                $ohistory_array_temp['shopid'] = $row['shop_id'];
                $ohistory_array_temp['shopnme'] = $row['shopname']; //$rowh['shopnme'];
                $ohistory_array_temp['offer_applied'] = $row['offer_provided_val'];
                $this->json_array['data']['ohistory'][] = $ohistory_array_temp;
            }
        } else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['ohistory'][] = "No records found";
        }
        return json_encode($this->json_array);
    }

    function get_orderdata_dcp($userid, $pageid) {
        $npageid = $pageid * 10;
        if ($pageid == '1')
            $limit = 0;
        else {
            $limit = ($pageid - 1) * 10;
        }
        $sql = "SELECT od.id as odid,od.order_by,tu.firstname,od.cartons_id,od.order_id,od.order_date,od.brand_id,od.cat_id,od.product_id
                  ,od.product_variant_id,count(od.product_quantity) as product_quantity,od.product_variant_weight1,
                  od.product_variant_unit1,od.product_unit_cost,od.product_total_cost,od.product_cgst,
                  od.product_sgst,sum(od.p_cost_cgst_sgst) as p_cost_cgst_sgst,od.order_status,
                  pcatvar.category_name as cat_name,pcatvar.product_name
                 FROM `tbl_dcp_orders` od 
                 LEFT join pcatbrandvariant pcatvar on od.product_id=pcatvar.product_id 
                  and od.product_variant_id=pcatvar.product_variant_id
                 left join tbl_user tu on od.order_by=tu.id
		WHERE 
		od.order_by='" . $userid . "' AND DATE_FORMAT(od.order_date,'%Y-%m-%d')=DATE(NOW())
		GROUP BY od.order_id limit $limit,$npageid "; //
        $proRow = $this->executeQuery($sql);

        if ($rowcount = mysql_num_rows($proRow) > 0) {
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '9';
            while ($row = mysql_fetch_array($proRow)) {
                $tcost = $row['p_cost_cgst_sgst'];
                $sale_quantity = $row['product_quantity'];
                $ohistory_array_temp['tcost'] = number_format($tcost, 2, '.', ''); //$rowtc['tcost'];
                $ohistory_array_temp['free_quantity'] = '0';
                $ohistory_array_temp['sale_quantity'] = $sale_quantity;
                $ohistory_array_temp['quantity'] = $sale_quantity;
                $ohistory_array_temp['orderid'] = $row['order_id'];
                $ohistory_array_temp['order_date'] = $row['order_date'];
                $ohistory_array_temp['cartons_id'] = $row['cartons_id'];
                $ohistory_array_temp['cartons_name'] = "carton-" . $row['cartons_id']; //$rowh['shopnme'];
                $ohistory_array_temp['offer_applied'] = "no";
                $this->json_array['data']['ohistory'][] = $ohistory_array_temp;
            }
        } else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['ohistory'][] = "No records found";
        }
        return json_encode($this->json_array);
    }

    function getorderdetailsbyordernumber_dcp($ordernum) {
        //echo "<pre>";print_r($ordernum);die();
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '9';
        $sql = "SELECT od.id as odid,od.order_by,tu.firstname,od.cartons_id,od.order_id,od.order_date,od.brand_id,od.cat_id,od.product_id
                  ,od.product_variant_id,od.product_quantity,od.product_variant_weight1,
                  od.product_variant_unit1,od.product_unit_cost,od.product_total_cost,od.product_cgst,
                  od.product_sgst,od.p_cost_cgst_sgst,od.order_status,
                  pcatvar.brand_name as brand_name,pcatvar.category_name as cat_name,pcatvar.product_name
                 FROM `tbl_dcp_orders` od 
                 LEFT join pcatbrandvariant pcatvar on od.product_id=pcatvar.product_id 
                  and od.product_variant_id=pcatvar.product_variant_id
                 left join tbl_user tu on od.order_by=tu.id
		WHERE od.order_id='".$ordernum."' 
		ORDER BY od.order_date  "; //
        $proRow = $this->executeQuery($sql);
        
        //$result1 = $this->getOrders($ordernum);
        //echo "<pre>";print_r(count($result1));die();
        $ohistory_array_temp1 = array();
        $ohistory_array_temp2 = array();
        //$result1 = mysqli_query($con,$sql);
        if ($rowcount = mysql_num_rows($proRow) > 0) {
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '9';
           while ($value = mysql_fetch_array($proRow)) {
                $ohistory_array_temp['product_varient_id']=$value['product_variant_id'];
                $ohistory_array_temp['distributorid']='';
                $ohistory_array_temp['distributornm']='';
                $ohistory_array_temp['salespersonnm']=$value['firstname'];
                $ohistory_array_temp['orderid']='';
                $ohistory_array_temp['ordernum']=$value['order_id'];
                $ohistory_array_temp['variant_oder_id']='';
                //$ohistory_array_temp['productname']=$value['product_name'];
                
                 $ohistory_array_temp['productname'] = $value['product_name'] . " " . $value['product_variant_weight1'] . "-" . $value['product_variant_unit1'];
                    if (!empty($value['product_variant_weight2'])) {
                        $ohistory_array_temp['productname'] .= " " . $value['product_variant_weight2'] . "-" . $value['product_variant_unit2'];
                    }
                $ohistory_array_temp['order_date']=$value['order_date'];
                $ohistory_array_temp['unitprice']=$value['product_unit_cost'];
                $ohistory_array_temp['quantity']=$value['product_quantity'];
                $ohistory_array_temp['totalcost']=$value['p_cost_cgst_sgst'];
                $ohistory_array_temp['shop_id']=$value['cartons_id'];
                $ohistory_array_temp['shopnm']="carton-".$value['cartons_id'];
                $ohistory_array_temp['catnm']=$value['cat_name'];
                $ohistory_array_temp['brandnm']=$value['brand_name'];
               $ohistory_array_temp['totalproductcount']='';
                $ohistory_array_temp1[]=$ohistory_array_temp;
            }
            if (!empty($ohistory_array_temp1))
                $this->json_array['data']['orderdetails'] = $ohistory_array_temp1;
        }else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['ohistory'][] = "No records found";
        }
        return json_encode($this->json_array);
    }

    function get_campaigndata() {
        $sql_campaign = "SELECT `id` AS campaign_id, `campaign_name`, `campaign_description`, `campaign_start_date`, `campaign_end_date`, `campaign_type`, `status` FROM `tbl_campaign` WHERE deleted = 0 AND status =0 AND campaign_end_date >= '" . date('Y-m-d') . "'";

        $result_campaign = $this->executeQuery($sql_campaign);

        while ($row_campaign = mysql_fetch_array($result_campaign)) {
            $campaign_array_temp['campaign_id'] = $row_campaign['campaign_id'];
            $campaign_array_temp['campaign_name'] = $row_campaign['campaign_name'];
            $campaign_array_temp['campaign_description'] = $row_campaign['campaign_description'];
            $campaign_array_temp['campaign_start_date'] = $row_campaign['campaign_start_date'];
            $campaign_array_temp['campaign_end_date'] = $row_campaign['campaign_end_date'];
            $campaign_array_temp['campaign_type'] = $row_campaign['campaign_type'];

            $sql_campaign_area = "SELECT `level`, `state_id`, `city_id`, `suburb_id`, `shop_id`, subarea_id FROM `tbl_campaign_area` WHERE deleted = 0 AND campaign_id=" . $row_campaign['campaign_id'];

            $result_campaign_area = $this->executeQuery($sql_campaign_area);
            $campaign_array_temp['campaign_area'] = array();
            $campaign_array_temp['campaign_product'] = array();
            $campaign_array_temp['campaign_product_weight'] = array();
            while ($row_campaign_area = mysql_fetch_array($result_campaign_area)) {
                $campaign_array_temp['campaign_area']['level'] = $row_campaign_area['level'];

                $sql_state = "SELECT `name` FROM `tbl_state` WHERE id IN (" . $row_campaign_area['state_id'] . ")";
                $result_state = $this->executeQuery($sql_state);
                $state_name_arr = array();

                while ($row_state = mysql_fetch_array($result_state)) {
                    $state_name_arr[] = $row_state['name'];
                }
                $state_name = implode(',', $state_name_arr);
                $campaign_array_temp['campaign_area']['state_id'] = $row_campaign_area['state_id'];
                $campaign_array_temp['campaign_area']['state_name'] = $state_name;

                if ($row_campaign_area['city_id'] != '') {
                    $sql_city = "SELECT `name` FROM `tbl_city` WHERE id IN (" . $row_campaign_area['city_id'] . ")";
                    $result_city = $this->executeQuery($sql_city);
                    $city_name_arr = array();
                    while ($row_city = mysql_fetch_array($result_city)) {
                        $city_name_arr[] = $row_city['name'];
                    }
                    $city_name = implode(',', $city_name_arr);
                    $campaign_array_temp['campaign_area']['city_id'] = $row_campaign_area['city_id'];
                    $campaign_array_temp['campaign_area']['city_name'] = $city_name;
                } else {
                    $campaign_array_temp['campaign_area']['city_id'] = "";
                    $campaign_array_temp['campaign_area']['city_name'] = "";
                }
                if ($row_campaign_area['suburb_id'] != '') {
                    $sql_suburb = "SELECT `suburbnm` FROM `tbl_area` WHERE id IN (" . $row_campaign_area['suburb_id'] . ")";
                    $result_suburb = $this->executeQuery($sql_suburb);
                    $suburb_name_arr = array();
                    while ($row_suburb = mysql_fetch_array($result_suburb)) {
                        $suburb_name_arr[] = $row_suburb['suburbnm'];
                    }
                    $suburb_name = implode(',', $suburb_name_arr);
                    $campaign_array_temp['campaign_area']['suburb_id'] = $row_campaign_area['suburb_id'];
                    $campaign_array_temp['campaign_area']['suburb_name'] = $suburb_name;
                } else {
                    $campaign_array_temp['campaign_area']['suburb_id'] = "";
                    $campaign_array_temp['campaign_area']['suburb_name'] = "";
                }

                // 
                if ($row_campaign_area['subarea_id'] != '') {
                    $sql_subarea = "SELECT subarea_name,subarea_id FROM tbl_subarea WHERE subarea_id IN (" . $row_campaign_area['subarea_id'] . ")";

                    $result_subarea = $this->executeQuery($sql_subarea);
                    $subarea_name_arr = array();

                    while ($row_subarea = mysql_fetch_array($result_subarea)) {
                        $subarea_name_arr[] = $row_subarea['subarea_name'];
                    }

                    $subarea_name = implode(',', $subarea_name_arr);

                    $campaign_array_temp['campaign_area']['subarea_id'] = $row_campaign_area['subarea_id'];
                    $campaign_array_temp['campaign_area']['subarea_name'] = $subarea_name;
                } else {
                    $campaign_array_temp['campaign_area']['subarea_id'] = "";
                    $campaign_array_temp['campaign_area']['subarea_name'] = "";
                }

                if ($row_campaign_area['shop_id'] != '') {
                    $sql_shop = "SELECT `name` FROM `tbl_shops` WHERE id IN (" . $row_campaign_area['shop_id'] . ")";
                    $result_shop = $this->executeQuery($sql_shop);
                    $shop_name_arr = array();
                    while ($row_shop = mysql_fetch_array($result_shop)) {
                        $shop_name_arr[] = $row_shop['name'];
                    }
                    $shop_name = implode(',', $shop_name_arr);
                    $campaign_array_temp['campaign_area']['shop_id'] = $row_campaign_area['shop_id'];
                    $campaign_array_temp['campaign_area']['shop_name'] = $shop_name;
                } else {
                    $campaign_array_temp['campaign_area']['shop_id'] = "";
                    $campaign_array_temp['campaign_area']['shop_name'] = "";
                }
            }

            switch ($row_campaign['campaign_type']) {
                case 'discount':
                    $sql_campaign_area_price = "SELECT `product_price`, `discount_percent` FROM `tbl_campaign_area_price` WHERE deleted = 0 AND campaign_id=" . $row_campaign['campaign_id'];
                    $result_campaign_area_price = $this->executeQuery($sql_campaign_area_price);
                    $i = 1;
                    $campaign_array_temp['area_price'] = array();
                    while ($row_campaign_area_price = mysql_fetch_array($result_campaign_area_price)) {
                        $record_discount = array();
                        $record_discount['record_counter'] = $i;
                        $record_discount['product_price'] = $row_campaign_area_price['product_price'];
                        $record_discount['discount_percent'] = $row_campaign_area_price['discount_percent'];
                        $campaign_array_temp['area_price'][] = $record_discount;
                        $i++;
                    }
                    break;
                case 'free_product':

                    $sql_campaign_free_product = "SELECT `c_p_brand_id`, `c_p_category_id`, `c_product_id`, `c_p_quantity`, `c_p_measure`, `c_p_measure_id`,`c_p_quantity_measure`, `f_p_brand_id`, `f_p_category_id`, `f_product_id`, `f_p_quantity`, `f_p_measure`, `f_p_measure_id`,`f_p_quantity_measure` FROM `tbl_campaign_product` WHERE deleted = 0 AND campaign_id=" . $row_campaign['campaign_id'];

                    $result_campaign_free_product = $this->executeQuery($sql_campaign_free_product);
                    $campaign_array_temp['area_price'] = array();
                    $campaign_array_temp['campaign_product'] = array();
                    $campaign_array_temp['campaign_product_weight'] = array();

                    $j = 1;
                    while ($row_campaign_free_product = mysql_fetch_array($result_campaign_free_product)) {
                        $record_product = array();
                        $record_product['record_counter'] = $j;
                        $record_product['c_p_brand_id'] = $row_campaign_free_product['c_p_brand_id'];
                        $record_product['c_p_brand_name'] = $this->getBrand($row_campaign_free_product['c_p_brand_id']);
                        $record_product['c_p_category_id'] = $row_campaign_free_product['c_p_category_id'];
                        $record_product['c_p_category_name'] = $this->getCategory($row_campaign_free_product['c_p_category_id']);
                        $record_product['c_product_id'] = $row_campaign_free_product['c_product_id'];
                        $record_product['c_product_name'] = $this->getProduct($row_campaign_free_product['c_product_id']);
                        if ($row_campaign_free_product['c_p_measure_id'] != '') {

                            $sqlp = "SELECT * from `tbl_product_variant` where id = " . $row_campaign_free_product['c_p_measure_id'];
                            $result1p = $this->executeQuery($sqlp);
                            $i = 1;
                            while ($row = mysql_fetch_array($result1p)) {
                                //$exp_variant1 = $row['variant_1'];
                                //$imp_variant1 = split(',', $exp_variant1);
								  $variant_1 = $row['variant_1'];
								 $variant1_unit_id = $row['variant1_unit_id'];

                                if ($variant_1 != '') {
                                    $sql1p = "SELECT  unitname,id FROM `tbl_units` WHERE id=" . $variant1_unit_id;
                                    $result2p = $this->executeQuery($sql1p);
                                    $row_prd_variant1p = mysql_fetch_array($result2p);
                                    $combine1 = $row['id'] . " " . $variant_1 . " " . $row_prd_variant1p['id'];
                                    $combine1_display = "";

                                    $combine1_displayp = $row_prd_variant1p['unitname'];
                                }
                            }

                            $record_product['c_product_variant_wt_quantity'] = $row_campaign_free_product['c_p_quantity'];
                            $record_product['c_product_variant_unit'] = $combine1_displayp;
                            $record_product['c_product_variant'] = $row_campaign_free_product['c_p_quantity'] . " " . $combine1_displayp;
                            $record_product['c_product_variant_unit_id'] = $row_campaign_free_product['c_p_measure_id'];
                        } else
                            $campaign_array_temp['campaign_product']['c_product_variant'] = "";

                        $record_product['f_p_brand_id'] = $row_campaign_free_product['f_p_brand_id'];
                        $record_product['f_p_brand_name'] = $this->getBrand($row_campaign_free_product['f_p_brand_id']);
                        $record_product['f_p_category_id'] = $row_campaign_free_product['f_p_category_id'];
                        $record_product['f_p_category_name'] = $this->getCategory($row_campaign_free_product['f_p_category_id']);
                        $record_product['f_product_id'] = $row_campaign_free_product['f_product_id'];
                        $record_product['f_product_name'] = $this->getProduct($row_campaign_free_product['f_product_id']);
                        if ($row_campaign_free_product['f_p_measure_id'] != '') {
                            $sql = "SELECT * from `tbl_product_variant` where id = " . $row_campaign_free_product['f_p_measure_id'];
                            $result1 = $this->executeQuery($sql);
                            $i = 1;
                            while ($row = mysql_fetch_array($result1)) {
                                //$exp_variant1 = $row['variant_1'];
                                //$imp_variant1 = split(',', $exp_variant1);
								  $variant_1 = $row['variant_1'];
								 $variant1_unit_id = $row['variant1_unit_id'];

                                if ($variant_1 != '') {
                                    $sql1 = "SELECT  unitname,id FROM `tbl_units` WHERE id=" . $variant1_unit_id;
                                    $result2 = $this->executeQuery($sql1);
                                    $row_prd_variant1 = mysql_fetch_array($result2);
                                    $combine1 = $row['id'] . " " . $variant_1 . " " . $row_prd_variant1['id'];
                                    $combine1_display = "";

                                    $combine1_display = $row_prd_variant1['unitname'];
                                }
                            }
                            $record_product['f_product_variant_wt_quantity'] = $row_campaign_free_product['f_p_quantity'];
                            $record_product['f_product_variant_unit'] = $combine1_display;
                            $record_product['f_product_variant'] = $row_campaign_free_product['f_p_quantity'] . " " . $combine1_display;
                            $record_product['f_product_variant_unit_id'] = $row_campaign_free_product['f_p_measure_id'];
                        } else
                            $campaign_array_temp['campaign_product']['f_product_variant'] = "";

                        $campaign_array_temp['campaign_product'][] = $record_product;
                        $j++;
                    }
                    break;
                case 'by_weight':
                    $sql_campaign_product_weight = "SELECT `id`, `campaign_id`, `campaign_area_id`, `c_weight`, `c_unit`, `f_p_brand_id`, `f_p_category_id`, `f_product_id`, `f_p_quantity`, `f_p_measure`, `f_p_measure_id`, `f_p_quantity_measure` FROM `tbl_campaign_product_weight` WHERE deleted = 0 AND campaign_id=" . $row_campaign['campaign_id'];

                    $result_campaign_product_weight = $this->executeQuery($sql_campaign_product_weight);
                    $campaign_array_temp['area_price'] = array();
                    $campaign_array_temp['campaign_product'] = array();
                    $campaign_array_temp['campaign_product_weight'] = array();
                    $j = 1;
                    while ($row_campaign_product_weight = mysql_fetch_array($result_campaign_product_weight)) {
                        $record_product = array();
                        $record_product['record_counter'] = $j;
                        $record_product['c_weight'] = $row_campaign_product_weight['c_weight'];
                        $record_product['c_unit'] = $row_campaign_product_weight['c_unit'];

                        $record_product['f_p_brand_id'] = $row_campaign_product_weight['f_p_brand_id'];
                        $record_product['f_p_brand_name'] = $this->getBrand($row_campaign_product_weight['f_p_brand_id']);
                        $record_product['f_p_category_id'] = $row_campaign_product_weight['f_p_category_id'];
                        $record_product['f_p_category_name'] = $this->getCategory($row_campaign_product_weight['f_p_category_id']);
                        $record_product['f_product_id'] = $row_campaign_product_weight['f_product_id'];
                        $record_product['f_product_name'] = $this->getProduct($row_campaign_product_weight['f_product_id']);
                        if ($row_campaign_product_weight['f_p_measure_id'] != '') {
                            $record_product['f_product_variant_wt_quantity'] = $row_campaign_product_weight['f_p_quantity'];
                            $record_product['f_product_variant_unit'] = $row_campaign_product_weight['f_p_measure'];
                            $record_product['f_product_variant'] = $row_campaign_product_weight['f_p_quantity'] . " " . $row_campaign_product_weight['f_p_measure'];
                            $record_product['f_product_variant_unit_id'] = $row_campaign_product_weight['f_p_measure_id'];
                        } else
                            $campaign_array_temp['campaign_product_weight']['f_product_variant'] = "";


                        $campaign_array_temp['campaign_product_weight'][] = $record_product;
                        $j++;
                    }
                    break;
            }
            $data[] = $campaign_array_temp;
        }
        if (count($data) == 0)
            $data = [];

        return $data;
    }

    function getBrand($id) {
        $sql_brand = "SELECT `name` FROM `tbl_brand` WHERE id =" . $id;
        $result_brand = $this->executeQuery($sql_brand);
        $row_brand = mysql_fetch_array($result_brand);
        return $row_brand['name'];
    }

    function getCategory($id) {
        $sql_category = "SELECT `categorynm` FROM `tbl_category` WHERE id =" . $id;
        $result_category = $this->executeQuery($sql_category);
        $row_category = mysql_fetch_array($result_category);
        return $row_category['categorynm'];
    }

    function getProduct($id) {
        $sql_product = "SELECT `productname` FROM `tbl_product` WHERE id =" . $id;
        $result_product = $this->executeQuery($sql_product);
        $row_product = mysql_fetch_array($result_product);
        return $row_product['productname'];
    }

    function getVariantUnit($id) {
        $sql_variant = "SELECT `unitname` FROM `tbl_units` WHERE id =" . $id;
        $result_variant = $this->executeQuery($sql_variant);
        $row_variant = mysql_fetch_array($result_variant);
        return $row_variant['unitname'];
    }

    function get_subarea($suburbids) {
        $sqlcity = "SELECT subarea_id,subarea_name,suburb_id FROM tbl_subarea WHERE suburb_id in ($suburbids) AND tbl_subarea.isdeleted !='1'";
        $proRowcity = $this->executeQuery($sqlcity);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '2';
        while ($rowcity = mysql_fetch_array($proRowcity)) {
            $c_array_temp['subarea_name'] = $rowcity['subarea_name'];
            $c_array_temp['subarea_id'] = $rowcity['subarea_id'];
            $c_array_temp['suburb_id'] = $rowcity['suburb_id'];
            $this->json_array['data']['subarea'][] = $c_array_temp;
        }
        return json_encode($this->json_array);
    }

    function get_distributorlist($salespersonid) {

        $sql = "SELECT external_id 
		  FROM tbl_user WHERE id = '" . $salespersonid . "'";

        $proRow = $this->executeQuery($sql);
        $row = mysql_fetch_array($proRow); //print_r($row );
        /* $suburb_ids 	= $row["suburb_ids"];
          $subarea_ids 	= $row["subarea_ids"];
          $state 		= $row["state"]; */
        $city = $row["city"];
        $external_id = $row["external_id"];


        /* $sqlcity = "SELECT 
          `id`,
          `firstname`
          FROM
          `tbl_user`
          WHERE
          user_type = 'Distributor' AND city= '$city'";
          $proRowcity = $this->executeQuery($sqlcity);
          $this->json_array['status']['responsecode'] = '0';
          $this->json_array['status']['entity'] = '2';
          if (mysql_num_rows($proRowcity) != 0) {
          while($rowcity = mysql_fetch_array($proRowcity)) {
          $c_array_temp['id']	= $rowcity['id'];
          $c_array_temp['firstname'] 	= $rowcity['firstname'];
          $this->json_array['data']['distributor'][]=$c_array_temp;
          }
          }else{ */
        $sqlstockist = "SELECT 
			`id`, 	 
			`firstname`	 	
			FROM 
				`tbl_user` 
			WHERE 
				user_type = 'Distributor' AND id IN ($external_id) AND isdeleted!='1' ";
        $proRowstockist = $this->executeQuery($sqlstockist);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '9';
        while ($rowstockist = mysql_fetch_array($proRowstockist)) {
            $c_array_temp['id'] = $rowstockist['id'];
            $c_array_temp['firstname'] = $rowstockist['firstname'];
            $this->json_array['data']['distributor'][] = $c_array_temp;
        }
        //}
        return json_encode($this->json_array);
    }

    function fnsaveNoordertakenHistory($clientnm,$shop_id, $salesperson_id, $shop_close_reason_type, $shop_close_reason_details, $shop_visit_date_time) {

        $sql = "INSERT INTO tbl_shop_visit(shop_id,salesperson_id,shop_close_reason_type, 
		shop_close_reason_details, shop_visit_date_time)
		VALUES('" . $shop_id . "' , '" . $salesperson_id . "' , '" . 
		$shop_close_reason_type . "' , '" . $shop_close_reason_details . "' , '" . 
		$shop_visit_date_time . "')";

        $proRow = $this->executeQuery($sql);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '1';
        $this->json_array['data']['leave']['msg'] = "No order taken history has been stored successfully.";

        $salespersonname = "";
        $sql = "SELECT firstname FROM  `tbl_user` WHERE id='" . $salesperson_id . "'";
        $proRow = $this->executeQuery($sql);
        if (mysql_num_rows($proRow) != 0) {
            $row = mysql_fetch_array($proRow);
            $salespersonname = $row['firstname'];
        }

        $shopname = "";
        $sql = "SELECT name FROM  tbl_shops WHERE id='" . $shop_id . "'";
        $proRow = $this->executeQuery($sql);
        if (mysql_num_rows($proRow) != 0) {
            $row = mysql_fetch_array($proRow);
            $shopname = $row['name'];
        }

        $subject = "No Order Recieved";
        $message = "Hi Admin,<br><br> Following shop not taked order.<br>";
        $message .= "<br><b>Salesperson name:</b> " . $salespersonname;
        $message .= "<br><b>Shop name:</b> " . $shopname;
        $message .= "<br><b>Reason:</b> " . $shop_close_reason_type;
        $message .= "<br><b>Reason details:</b> " . $shop_close_reason_details;
        $message .= "<br><br>BR, </br>" . $clientnm . " Team";

        //$FROMMAILID = FROMMAILID;
        $FROMMAILID = "shivraj.dakore@ecotechservices.com";
        //$headers = 'From: '. $FROMMAILID  .'\r\n Reply-To: ' . $FROMMAILID;

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <' . $FROMMAILID . '>' . "\r\n";
        //$headers .= 'Cc: myboss@example.com' . "\r\n";

        $to = "shivraj.dakore@ecotechservices.com";
        //@mail($to, $subject, $message, $headers);
        return json_encode($this->json_array);
    }

    function save_userlocation($userid, $latt, $longg) {
        $last_id = 0;
        $sql_point_check = "select id from `tbl_user_location` where userid='$userid' and lattitude='$latt' and longitude='$longg' and DATE_FORMAT(tdate,'%Y-%m-%d')=DATE(NOW()) ORDER BY id DESC LIMIT 1";
        $proRowp = $this->executeQuery($sql_point_check);
        if (mysql_num_rows($proRowp) != 0) {
            $this->json_array['data']['Location']['id'] = $last_id;
        } else {
            if ($latt != '0.0' && $longg != '0.0') {
                $sql = "INSERT INTO tbl_user_location(userid, lattitude, longitude,tdate)VALUES('" . $userid . "','" . $latt . "','" . $longg . "',now())";
                $proRow = $this->executeQuery($sql);
                $last_id = mysql_insert_id();
                $this->json_array['data']['Location']['id'] = $last_id;
            }
        }
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '1';
        $this->json_array['data']['Location']['msg'] = "Location added successfully.";

        return json_encode($this->json_array);
    }

    //count for images TADA bill
    function get_sptravel_count($data) {
        $userid = $data['userid'];
        $sqlfordistance = "SELECT max(id) as maxid FROM  tbl_sp_tadabill WHERE userid='" . $userid . "' and date_format(date_tada,'%d-%m-%Y')= date_format(NOW(), '%d-%m-%Y')";
        $proRowdist = $this->executeQuery($sqlfordistance);
        $rowAttendance = mysql_fetch_assoc($proRowdist);
        return $rowAttendance['maxid'];
    }

    function save_customer($data) {
        //state,city,area,subarea,addr_lat,addr_long,
        $username = $data['username'];
        $customer_emailid = $data['customer_emailid'];
        $sql_point_check = "select id from `tbl_customer_new` where customer_emailid='$customer_emailid' and username='$username'  ORDER BY id DESC LIMIT 1";
        $proRowp = $this->executeQuery($sql_point_check);
        if (mysql_num_rows($proRowp) != 0) {
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['Customer']['msg'] = "User already exists!";
            $this->json_array['data']['Customer']['id'] = 0;
        } else {
            $pwd = md5($data['password']);
            $sql = "INSERT INTO tbl_customer_new
                    (customer_name,customer_emailid,username,password,customer_phone_no,
                    customer_address,customer_type,customer_shipping_address,state,city,area,subarea,ship_addr_lat,ship_addr_long)VALUES
                    ('" . $data['customer_name'] . "','" . $customer_emailid .
                    "','" . $username . "','" . $pwd .
                    "','" . $data['customer_phone_no'] . "','" . $data['customer_address'] .
                    "','" . $data['customer_type'] . "','" . $data['customer_shipping_address'] .
                     "','" . $data['state'] . "','" . $data['city'] .
                     "','" . $data['area'] . "','" . $data['subarea'] .
                    "','" . $data['ship_addr_lat'] . "','" . $data['ship_addr_long'] . "')";
            $proRow = $this->executeQuery($sql);
            $last_id = mysql_insert_id();
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['Customer']['msg'] = "Customer added successfully.";
            $this->json_array['data']['Customer']['id'] = $last_id;
        }
        return json_encode($this->json_array);
    }

    function GetDrivingDistance($lat1, $lat2, $long1, $long2) {
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $long1 . "&destinations=" . $lat2 . "," . $long2 . "&mode=driving&language=pl-PL";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
        $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
        return array('distance' => $dist, 'time' => $time);
    }

    function save_sptravel($data) {
        $userid = $data['userid'];
        $distance_covered = $data['distance_covered'];
        $mode_of_transe = $data['mode_of_transe'];
        $food = $data['food'];
        $other = $data['other'];
        $sqlfordistance = "SELECT 	id,lattitude,longitude FROM  tbl_user_location WHERE userid='" . $userid . "' and date_format(tdate,'%d-%m-%Y')= date_format(NOW(), '%d-%m-%Y')";
        $proRowdist = $this->executeQuery($sqlfordistance);
        $countpoints = mysql_num_rows($proRowdist);
        $countrec = 0;
        if (mysql_num_rows($proRowdist) > 0) {
            while ($rowc = mysql_fetch_array($proRowdist)) {
                $c_array_temp[$countrec]['id'] = $rowc['id'];
                $c_array_temp[$countrec]['lattitude'] = $rowc['lattitude'];
                $c_array_temp[$countrec]['longitude'] = $rowc['longitude'];
                $countrec++;
            }
        }
        //echo "".$countpoints."<pre>";print_r($c_array_temp);
        $google_distance = 0;
        for ($i = 0; $i < $countpoints - 1; $i++) {
            $google_d = $this->GetDrivingDistance($c_array_temp[$i]['lattitude'], $c_array_temp[$i + 1]['lattitude'], $c_array_temp[$i]['longitude'], $c_array_temp[$i + 1]['longitude']);
            //echo "<pre>";print_r($google_d);
            $google_distance = $google_distance + $google_d['distance'];
        }
        $google_distance = bcdiv($google_distance, 1000, 3);

        $sqlforrate = "SELECT rupees_per_km from tbl_mode_transe where id='$mode_of_transe'";
        $prorateRow = $this->executeQuery($sqlforrate);
        $rupees_per_km = 0;
        while ($rowc = mysql_fetch_array($prorateRow)) {
            $rupees_per_km = $rowc['rupees_per_km'];
        }
        $sql = "INSERT INTO tbl_sp_tadabill(userid, distance_covered,google_distance, mode_of_transe,food,other,date_tada,Current_rate_mot)VALUES
		('" . $userid . "','" . $distance_covered . "','" . $google_distance . "','" . $mode_of_transe . "','" . $food . "','" . $other . "',now(),'" . $rupees_per_km . "')";
        //,Current_rate_mot
        //,'".$rupees_per_km."'
        $proRow = $this->executeQuery($sql);
        $last_id = mysql_insert_id();
        $this->json_array['data']['Travel']['msg'] = "Expense bill submitted successfully.";

        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '1';
        $this->json_array['data']['Travel']['id'] = $last_id;
        return json_encode($this->json_array);
    }

    function get_userlocation($userid) {
        $sql = "select id,userid,lattitude,longitude from tbl_user_location where userid='$userid'";
        $proRow = $this->executeQuery($sql);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '2';
        $countt = 0;
        while ($rowc = mysql_fetch_array($proRow)) {

            $c_array_temp['id'] = $rowc['id'];
            $c_array_temp['userid'] = $rowc['userid'];
            $c_array_temp['lattitude'] = $rowc['lattitude'];
            $c_array_temp['longitude'] = $rowc['longitude'];
            $this->json_array['data']['location'][] = $c_array_temp;
            $countt++;
        }
        $this->json_array['data']['sdpoint'][] = $this->json_array['data']['location'][0];
        $this->json_array['data']['sdpoint'][] = $this->json_array['data']['location'][$countt - 1];
        for ($i = 0; $i < $countt; $i++) {

            $this->json_array['data']['location'][$i]['latd'] = $this->json_array['data']['location'][$i + 1]['lattitude'];
            $this->json_array['data']['location'][$i]['longgd'] = $this->json_array['data']['location'][$i + 1]['longitude'];
        }
        $this->json_array['data']['location'][$countt - 1]['latd'] = $this->json_array['data']['location'][$countt - 1]['lattitude'];
        $this->json_array['data']['location'][$countt - 1]['longgd'] = $this->json_array['data']['location'][$countt - 1]['longitude'];


        return json_encode($this->json_array);
    }

    //new service for shop edit
    function fnEditStore($salespersonid, $store_name, $contact_no, $store_Address, $lat, $lng,  $shopid) {       

        $sql = "UPDATE tbl_shops SET 
		name='".$store_name."',
		address='".$store_Address."',		
		mobile='".$contact_no."',
		shop_added_by='".$salespersonid."',
		latitude='".$lat."',                   
		longitude='".$lng."'
		where id='".$shopid."'";
        $proRow = $this->executeQuery($sql);

        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '1';
        $this->json_array['data']['Store']['msg'] = "Store updated successfully.";
        return json_encode($this->json_array);
    }

    function checkAttendance($id) {
        $sql = "SELECT count(sp_id) AS count,dayendtime 
                FROM `tbl_sp_attendance` 
                WHERE sp_id = $id AND date_format(tdate, '%d-%m-%Y') = '" . date('d-m-Y') . "'";
        $resultAttendance = $this->executeQuery($sql);
        if (mysql_num_rows($resultAttendance) != 0) {
            $rowAttendance = mysql_fetch_assoc($resultAttendance);
            return $rowAttendance;
        } else {
            return 0;
        }
    }

    function checkAttendance_forday($id, $leavedate) {
        $sql = "SELECT count(id) AS counter
                FROM `tbl_sp_attendance` 
                WHERE sp_id = '$id' AND date_format(`tdate`, '%d-%m-%Y') = date_format('" . $leavedate . "', '%d-%m-%Y')";
        $resultAttendance = $this->executeQuery($sql);
        if (mysql_num_rows($resultAttendance) != 0) {
            $rowAttendance = mysql_fetch_assoc($resultAttendance);
            return $rowAttendance;
        } else {
            return 0;
        }
    }

    function checkLeave($id) {
        $sql = "SELECT 
                    count(sp_id) AS count, 	dayendtime
                    FROM `tbl_sp_attendance` 
                    WHERE presenty !='1' and 
                    sp_id = $id AND date_format(tdate, '%d-%m-%Y') = '" . date('d-m-Y') . "'";
        $resultAttendance = $this->executeQuery($sql);
        if (mysql_num_rows($resultAttendance) != 0) {
            $rowAttendance = mysql_fetch_assoc($resultAttendance);
            return $rowAttendance;
        } else {
            return 0;
        }
    }
	function checkLeavenew($id,$leavedate) {
         $sql = "SELECT 
                    count(sp_id) AS count, 	dayendtime
                    FROM `tbl_sp_attendance` 
                    WHERE presenty !='1' and 
                    sp_id = $id AND date_format(tdate, '%Y-%m-%d') = '" . $leavedate . "'";
        $resultAttendance = $this->executeQuery($sql);
        if (mysql_num_rows($resultAttendance) != 0) {
            $rowAttendance = mysql_fetch_assoc($resultAttendance);
            return $rowAttendance;
        } else {
            return 0;
        }
    }

    function addAttendance($id) {
        $attendance_count = $this->checkAttendance($id);
        $leave_count = $this->checkLeave($id);

        if ($leave_count['count'] == 0) {
            if ($attendance_count['count'] == 0) {
                $sql = "INSERT INTO tbl_sp_attendance(sp_id, tdate,presenty) VALUES ( '" . $id . "',now(),'1' )";
                $proRow = $this->executeQuery($sql);
                $this->json_array['status']['responsecode'] = '0';
                $this->json_array['status']['entity'] = '1';
                $this->json_array['data']['msg'] = "Day Start Time added successfully.";
            } else {
                $this->json_array['status']['responsecode'] = '0';
                $this->json_array['status']['entity'] = '1';
                $this->json_array['data']['msg'] = "Day Start Time already exist.";
            }
        } else {
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['msg'] = "Cant start day, you are on leave!";
        }
        return json_encode($this->json_array);
    }

    
	function fnApplyLeavenew($id, $reasonleave, $description, $leavestartdate,$leaveenddate) {		
		$leaveenddate1 = strtotime($leaveenddate);
		$leavestartdate1 =  strtotime($leavestartdate);
		$datediff =  $leaveenddate1 - $leavestartdate1;
		$todaysdate=strtotime(date('Y-m-d'));
		$leave_id=time();
		$datediff1 = round($datediff / (60 * 60 * 24));
		$count_leave=0;
	
		if($leaveenddate1>=$todaysdate && $leavestartdate1>=$todaysdate){
			if($datediff1>=0){			
				for($i=0;$i<=$datediff1;$i++){
					$leavedt = date('Y-m-d', strtotime($leavestartdate . ' +'.$i.' day'));
					$leave_count = $this->checkLeavenew($id,$leavedt);
					if ($leave_count['count'] == 0) {
						$sql = "INSERT INTO tbl_sp_attendance(sp_id,reason,description,tdate,leave_id)VALUES('" . $id . "','" . $reasonleave . "','" . $description . "','" . $leavedt . "','".$leave_id."')";
						$proRow = $this->executeQuery($sql);
						$count_leave++;
					}		
				}
				if ($count_leave > 0) {           
					$this->json_array['status']['responsecode'] = '0';
					$this->json_array['status']['entity'] = '1';
					$this->json_array['data']['leave']['msg'] = " ".$count_leave." Days Leave application sent successfully.";
				} else {
					$this->json_array['status']['responsecode'] = '0';
					$this->json_array['status']['entity'] = '1';
					$this->json_array['data']['leave']['msg'] = "You Have Already Applied Leave!.";
				}
			}else{
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
				$this->json_array['data']['leave']['msg'] = "End Date should be greater or equals to Start Date!.";
			}
		}else{
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['leave']['msg'] = "Start date or End date should Not be past date !.";
		}
        return json_encode($this->json_array);
    }

    function addDayEndTime($id) {
        $attendance_count = $this->checkAttendance($id);
        $leave_count = $this->checkLeave($id);

        if ($leave_count['count'] == 0) {
            if ($attendance_count['count'] != 0 && $attendance_count['dayendtime'] == '') {
                $sql = "UPDATE tbl_sp_attendance SET dayendtime = now() WHERE sp_id = $id AND date_format(tdate, '%d-%m-%Y') = '" . date('d-m-Y') . "'";
                $proRow = $this->executeQuery($sql);
                $this->json_array['status']['responsecode'] = '0';
                $this->json_array['status']['entity'] = '1';
                $this->json_array['data']['msg'] = "Day End Time added successfully.";
            } else if ($attendance_count['dayendtime'] != '') {
                $this->json_array['status']['responsecode'] = '0';
                $this->json_array['status']['entity'] = '1';
                $this->json_array['data']['msg'] = "Day End Time already exist.";
            } else {
                $this->json_array['status']['responsecode'] = '0';
                $this->json_array['status']['entity'] = '1';
                $this->json_array['data']['msg'] = "Please add day start time.";
            }
        } else {
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['msg'] = "Cant end day, you are on leave!";
        }

        return json_encode($this->json_array);
    }

    public function getProductname($prodid) {
        $sql = "SELECT productname
		FROM `tbl_product` 
		WHERE id = " . $prodid;
        $proRow = $this->executeQuery($sql);
        //$prod_name_result = mysqli_query($this->local_connection,$sql);		
        return $row = mysql_fetch_assoc($proRow);
    }

    public function getOrders($order_no) {

        $where_clause_outer = '';

        if ($order_no != '') {
            $where_clause_outer .= " AND o.order_no = '" . $order_no . "' ";
        }

        $order_sql = "SELECT o.id, o.order_no, o.invoice_no, 
		o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
		o.order_date, 
		o.shop_id,  s.name AS shop_name,
		s.suburbid, (SELECT sb.suburbnm FROM tbl_area AS sb WHERE sb.id = s.suburbid) AS region_name,
		o.total_items, 
		o.total_cost, o.total_order_gst_cost, o.lat, 
		o.long, o.offer_provided
		FROM `tbl_orders` AS o 		
		LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
		WHERE 1=1 $where_clause_outer
		ORDER BY shop_name, o.order_date DESC"; //o.shop_order_status = ".$order_status."

        $orders_result = $this->executeQuery($order_sql);
        $row_orders_count = mysql_num_rows($orders_result);
        $i = 1;
        $all_orders = array();
        $row_orders_det_count = 0;
        if ($row_orders_count > 0) {
            while ($row_orders = mysql_fetch_assoc($orders_result)) {
                $all_orders[$i] = $row_orders;
                $order_details_sql = "SELECT od.id, od.order_id, 
				od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
				od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
				od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
				od.producthsn,
				od.product_variant_id, od.product_quantity, 
				od.product_variant_weight1, od.product_variant_unit1, 
				od.product_variant_weight2, od.product_variant_unit2, 
				od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
				od.campaign_applied, od.campaign_type, od.campaign_sale_type, od.order_status, 
				od.delivery_assing_date, od.delivery_assign_to, od.transport_date, 
				od.challan_no, od.vehicle_no, od.transport_mode, od.date_time_supply, od.place_of_supply, 
				od.quantity_delivered, od.delivery_date, od.amount_paid, od.payment_date,
				od.free_product_details,
				odis.discount_amount
				FROM `tbl_order_details` AS od
				LEFT JOIN tbl_order_cp_discount AS odis ON odis.order_variant_id = od.id
				WHERE od.order_id = " . $row_orders['id'] . " "; //AND od.order_status = ".$order_status." 
                $orders_det_result = $this->executeQuery($order_details_sql);
                //$row_orders_det_count = mysqli_num_rows($orders_det_result);
                if (mysql_num_rows($orders_det_result) > 0) {
                    $j = 1;
                    while ($row_orders_det = mysql_fetch_assoc($orders_det_result)) {
                        $all_orders[$i]['order_details'][$j] = $row_orders_det;
                        $j++;
                    }
                    $i++;
                } else {
                    unset($all_orders[$i]);
                }
            }
        }
        //print"<pre>";
        //print_r($all_orders);

        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }

    function getorderdetailsbyordernumber($ordernum) {
        //echo "<pre>";print_r($ordernum);die();
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '9';
        $result1 = $this->getOrders($ordernum);
        //echo "<pre>";print_r(count($result1));die();
        $ohistory_array_temp1 = array();
        $ohistory_array_temp2 = array();
        //$result1 = mysqli_query($con,$sql);
        if (count($result1) > 0) {
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '9';
            foreach ($result1 as $key => $value) {
                foreach ($value['order_details'] as $key1 => $value1) {
                    $ohistory_array_temp['product_varient_id'] = $value1['product_variant_id'];
                    $ohistory_array_temp['distributorid'] = '';
                    $ohistory_array_temp['distributornm'] = '';
                    $ohistory_array_temp['salespersonnm'] = $value['order_by_name'];

                    $ohistory_array_temp['orderid'] = $value['id'];
                    $ohistory_array_temp['ordernum'] = $value['order_no'];

                    $ohistory_array_temp['variant_oder_id'] = '';
                    $ohistory_array_temp['productname'] = $value1['product_name'] . " " . $value1['product_variant_weight1'] . "-" . $value1['product_variant_unit1'];
                    if (!empty($value1['product_variant_weight2'])) {
                        $ohistory_array_temp['productname'] .= " " . $value1['product_variant_weight2'] . "-" . $value1['product_variant_unit2'];
                    }
                    if (!empty($value1['free_product_details'])) {
                        $ohistory_array_temp2[$key1] = $value1['free_product_details'];
                    }

                    $ohistory_array_temp['order_date'] = date('d-m-Y H:i:s', strtotime($value['order_date']));
                    $ohistory_array_temp['unitprice'] = number_format($value1['product_unit_cost'], 2, '.', '');
                    $ohistory_array_temp['quantity'] = $value1['product_quantity'];
                    $total_cost = $ohistory_array_temp['unitprice'] * $ohistory_array_temp['quantity'];
                    $ohistory_array_temp['totalcost'] = number_format($value1['p_cost_cgst_sgst'], 2, '.', '');
                    //$ohistory_array_temp['p_cost_cgst_sgst']=number_format($value1['p_cost_cgst_sgst'],2, '.', '');
                    $ohistory_array_temp['shop_id'] = $value['shop_id'];
                    $ohistory_array_temp['shopnm'] = $value['shop_name'];

                    $ohistory_array_temp['catnm'] = $value1['cat_name'];
                    $ohistory_array_temp['brandnm'] = $value1['brand_name'];

                    $ohistory_array_temp['totalproductcount'] = '';
                    $ohistory_array_temp1[] = $ohistory_array_temp;
                }
                foreach ($ohistory_array_temp2 as $valfree) {
                    $free_prod_array = explode(",", $valfree);
                    $ohistory_array_temp['product_varient_id'] = $free_prod_array[6];
                    $ohistory_array_temp['distributorid'] = '';
                    $ohistory_array_temp['distributornm'] = '';
                    $ohistory_array_temp['salespersonnm'] = $value['order_by_name'];

                    $ohistory_array_temp['orderid'] = $value['id'];
                    $ohistory_array_temp['ordernum'] = $value['order_no'];

                    $ohistory_array_temp['variant_oder_id'] = '';
                    $productname = $this->getProductname($free_prod_array[4]);

                    foreach ($productname as $keyprod => $valueprod) {
                        $product_name .= $valueprod;
                    }
                    $ohistory_array_temp['productname'] = $product_name;
                    $ohistory_array_temp['productname'] .= " " . $free_prod_array[7];


                    $ohistory_array_temp['order_date'] = date('d-m-Y H:i:s', strtotime($value['order_date']));
                    $ohistory_array_temp['unitprice'] = 0;
                    $ohistory_array_temp['quantity'] = $free_prod_array[8];
                    //$total_cost=$ohistory_array_temp['unitprice']*$ohistory_array_temp['quantity'];
                    $ohistory_array_temp['totalcost'] = 0;
                    //$ohistory_array_temp['p_cost_cgst_sgst']=0;
                    $ohistory_array_temp['shop_id'] = $value['shop_id'];
                    $ohistory_array_temp['shopnm'] = $value['shop_name'];

                    $ohistory_array_temp['catnm'] = $free_prod_array[3];
                    $ohistory_array_temp['brandnm'] = $free_prod_array[1];

                    $ohistory_array_temp['totalproductcount'] = '';
                    $ohistory_array_temp1[] = $ohistory_array_temp;
                }
            }
            if (!empty($ohistory_array_temp1))
                $this->json_array['data']['orderdetails'] = $ohistory_array_temp1;
        }else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['ohistory'][] = "No records found";
        }
        return json_encode($this->json_array);
    }

    function get_distributorlist_by_ssid($ssid) {
        $sqlstockist = "SELECT 
			`id`, 	 
			`firstname`	 	
			FROM 
				`tbl_user` 
			WHERE 
				user_type = 'Distributor' AND external_id =$ssid";
        $proRowstockist = $this->executeQuery($sqlstockist);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '9';
        while ($rowstockist = mysql_fetch_array($proRowstockist)) {
            $c_array_temp['id'] = $rowstockist['id'];
            $c_array_temp['firstname'] = $rowstockist['firstname'];
            $this->json_array['data']['distributor'][] = $c_array_temp;
        }

        return json_encode($this->json_array);
    }

    function get_order_and_orderdetails($userid, $order_status) {
        $order_sql = "SELECT o.id as oid, o.order_no, o.invoice_no, 
		o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
		o.order_date, 
		o.shop_id,  s.name AS shop_name,
		s.suburbid, (SELECT sb.suburbnm FROM tbl_area AS sb WHERE sb.id = s.suburbid) AS region_name,
		o.total_items, 
		o.total_cost, o.total_order_gst_cost, o.lat, 
		o.long, o.offer_provided
		FROM `tbl_orders` AS o 		
		LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
		WHERE   o.delivery_person_id=" . $userid . "
		ORDER BY shop_name, o.order_date DESC";

        $orders_result = $this->executeQuery($order_sql);
        $row_orders_count = mysql_num_rows($orders_result);
        //$i = 1;
        $all_orders = array();
        $row_orders_det_count = 0;
        if ($row_orders_count > 0) {
            while ($row_orders = mysql_fetch_assoc($orders_result)) {
                $where_cond = "";
                if (strpos($order_status, ',') !== false) {
                    $where_cond .= "  AND od.order_status in ('3','4') ";
                } else {
                    $where_cond .= "  AND od.order_status = '" . $order_status . "' ";
                }
                //$all_orders[$i] = $row_orders;
                $order_details_sql = "SELECT od.id as odid, od.order_id, 
				od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
				od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
				od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
				od.producthsn,
				od.product_variant_id, od.product_quantity, 
				od.product_variant_weight1, od.product_variant_unit1, 
				od.product_variant_weight2, od.product_variant_unit2, 
				od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
				od.campaign_applied, od.campaign_type, od.campaign_sale_type, od.order_status, 
				od.delivery_assing_date, od.delivery_assign_to, od.transport_date, 
				od.challan_no, od.vehicle_no, od.transport_mode, od.date_time_supply, od.place_of_supply, 
				od.quantity_delivered, od.delivery_date, od.amount_paid, od.payment_date 
				FROM `tbl_order_details` AS od
				WHERE od.order_id = " . $row_orders['oid'] . " $where_cond";
                //die();

                $orders_det_result = $this->executeQuery($order_details_sql);
                $row_orders_det_count = $row_orders_det_count + mysql_num_rows($orders_det_result);
                //echo $row_orders_det_count ;
                if (mysql_num_rows($orders_det_result) > 0) {
                    //$j = 1;
                    while ($row_orders_det = mysql_fetch_assoc($orders_det_result)) {
                        $newarr[] = array_merge($row_orders_det, $row_orders);
                        //$all_orders[$i]['order_details'][] = $newarr;
                    }
                }
            }
        }
        //echo $row_orders_det_count ;
        //echo "<pre>";print_r($newarr);die();

        if ($row_orders_det_count == 0) {
            $all_orders = array();
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['orderdetails'][] = "No records";
        } else {
            foreach ($newarr as $key => $value) {
                foreach ($value as $key1 => $value1) {
                    if (is_null($value1)) {
                        $newarr[$key][$key1] = "";
                    }
                }
            }
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '9';
            $this->json_array['data']['orderdetails'] = $newarr;
        }
        return json_encode($this->json_array);
    }
	function get_shop_orderhistory($userid, $shopid) {
       
        $sql = "select shops.name as shopname,OA.shop_id, OA.order_date,  
						OA.order_no, VO.product_variant_weight1,
						VO.product_variant_unit1,
						SUM(VO.p_cost_cgst_sgst) as totalcost,
						count(VO.order_id) as variantunit, 
		IF(OA.offer_provided = 0,'Offer Applied','Offer Not Applied') AS offer_provided_val, VO.cat_id

		FROM `tbl_order_details` VO
		LEFT JOIN tbl_orders OA on OA.id=VO.order_id
		LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
		WHERE 
		OA.ordered_by='".$userid."' 
		AND OA.shop_id='".$shopid."' 
		AND OA.shop_id is not null
		AND DATE_FORMAT(OA.order_date,'%Y-%m-%d')=DATE(NOW())
		GROUP BY VO.order_id  "; //
        $proRow = $this->executeQuery($sql);

        if ($rowcount = mysql_num_rows($proRow) > 0) {
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '9';
            while ($row = mysql_fetch_array($proRow)) {
                $tcost = $row['totalcost'];

                $sql_free_pro = "SELECT COUNT(tbl_variant_order.id) as quantity_free FROM `tbl_variant_order` INNER JOIN `tbl_order_app` ON tbl_order_app.id = tbl_variant_order.orderappid WHERE campaign_sale_type='free' AND OA.cat_id = " . $row['cat_id'];
                $result_free_pro = mysqli_query($con, $sql_free_pro);
                $row_free_pro = mysqli_fetch_assoc($result_free_pro);
                $free_quantity = 0;
                $sale_quantity = $row['variantunit'];
                if ($row_free_pro['quantity_free'] > 0) {
                    $free_quantity = $row_free_pro['quantity_free'];
                    $sale_quantity = $row['variantunit'] - $free_quantity;
                }
                $ohistory_array_temp['tcost'] = number_format($tcost, 2, '.', ''); //$rowtc['tcost'];
                $ohistory_array_temp['free_quantity'] = $free_quantity;
                $ohistory_array_temp['sale_quantity'] = $sale_quantity;
                $ohistory_array_temp['quantity'] = $row['variantunit'];
                $ohistory_array_temp['orderid'] = $row['order_no'];
                $ohistory_array_temp['order_date'] = $row['order_date'];
                $ohistory_array_temp['shopid'] = $row['shop_id'];
                $ohistory_array_temp['shopnme'] = $row['shopname']; //$rowh['shopnme'];
                $ohistory_array_temp['offer_applied'] = $row['offer_provided_val'];
                $this->json_array['data']['ohistory'][] = $ohistory_array_temp;
            }
        } else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['ohistory'][] = "No records found";
        }
        return json_encode($this->json_array);
    }

    function cash_on_delivery($data) {
        $count = 0;
        $cod_percent = 0;

        foreach ($data->order_details as $key => $value) {
            $count++;
            $order_id = $value->orderid;

            $sql2 = "SELECT id as oid FROM tbl_orders where order_no='$order_id' ";
            $resultoid = $this->executeQuery($sql2);
            $row_countoid = mysql_num_rows($resultoid);
            if ($row_countoid > 0) {
                $oidasdh = mysql_fetch_assoc($resultoid);
                $oid = $oidasdh['oid'];
            }
            if ($data->cod_type == 1) {
                $sql1 = "SELECT cod_percent FROM tbl_cod_admin where status = 'active'";
                $result = $this->executeQuery($sql1);
                $row_count = mysql_num_rows($result);
                if ($row_count > 0) {
                    $cod_percents = mysql_fetch_assoc($result);
                    $cod_percent = $cod_percents['cod_percent'];
                }
                $sql = "UPDATE tbl_orders SET c_o_d='1',cod_percent='$cod_percent' where order_no='$order_id'";
                $proRow = $this->executeQuery($sql);
            }
            //tbl_order_details
            $sql = "UPDATE tbl_order_details SET order_status='4' where order_id='$oid'";
            $proRow = $this->executeQuery($sql);
        }

        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '1';
        if ($count > 0) {
            $this->json_array['data']['Delivery']['msg'] = "Cash on delivery updated successfully.";
        } else {
            $this->json_array['data']['Delivery']['msg'] = "Cash on delivery not updated.";
        }
        return json_encode($this->json_array);
    }

    function get_mode_of_transe() {
        $sqlcity = "SELECT id, van_type FROM  tbl_mode_transe";
        $proRowcity = $this->executeQuery($sqlcity);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '3';
        while ($rowcity = mysql_fetch_array($proRowcity)) {
            $c_array_temp['id'] = $rowcity['id'];
            $c_array_temp['van_type'] = $rowcity['van_type'];
            $this->json_array['data']['mot'][] = $c_array_temp;
        }
        return json_encode($this->json_array);
    }

    

//	function save_sp_todaystravel($data) {
//		$userid =$data['userid'];
//		$flag =$data['flag'];
//		$comments =$data['comments'];
//		$user_type =$data['user_type'];
//		$visit_to_id =$data['visit_to_id'];//shop/ Trasnport office
//		
//		$sql = "SELECT * from tbl_sp_attendance WHERE sp_id='".$userid ."' 
//		AND DATE_FORMAT(tdate,'%Y-%m-%d')=DATE(NOW())";
//		$proRow = $this->executeQuery($sql);
//		$c_array_temp=array();
//		while($rowc = mysql_fetch_array($proRow)) {
//			$c_array_temp['dayendtime']   = $rowc['dayendtime'];
//			$c_array_temp['daystarttime'] = $rowc['daystarttime'];			
//		}
//		if (mysql_num_rows($proRow) == 0) {
//				$this->json_array['data']['Track']['msg'] = "Day not started yet.";
//				$this->json_array['status']['responsecode'] = '0';
//				$this->json_array['status']['entity'] = '1';
//				$this->json_array['data']['Track']['id'] = '';
//				return json_encode($this->json_array);
//		}else if($c_array_temp['dayendtime']!== NULL){
//				$this->json_array['data']['Track']['msg'] = "Day already ended.";
//				$this->json_array['status']['responsecode'] = '0';
//				$this->json_array['status']['entity'] = '1';
//				$this->json_array['data']['Track']['id'] = '';
//				return json_encode($this->json_array);
//		}else{
//			if($user_type == 'SalesPerson' || $user_type == ''){
//				$sql="INSERT INTO tbl_shop_visit(salesperson_id, shop_visit_date_time, flag, comments, is_shop_location)VALUES
//				('".$userid."',now(),'".$flag."','".$comments."','1')";
//				$proRow = $this->executeQuery($sql);
//				$last_id = mysql_insert_id();
//				$this->json_array['data']['Track']['msg'] = "Location track ".$flag.".";
//				$this->json_array['status']['responsecode'] = '0';
//				$this->json_array['status']['entity'] = '1';
//				$this->json_array['data']['Track']['id'] = $last_id;
//			}else if($user_type == 'DeliveryPerson'){
//				$sql="INSERT INTO tbl_transport_off_visit(`transport_off_id`, `deliveryperson_id`, `transport_off_visit_date_time`, `flag`, `comments`)
//				VALUES
//				('".$visit_to_id."', '".$userid."',now(),'".$flag."','".$comments."','1')";
//				$proRow = $this->executeQuery($sql);
//				$last_id = mysql_insert_id();
//				$this->json_array['data']['Track']['msg'] = "Location track ".$flag.".";
//				$this->json_array['status']['responsecode'] = '0';
//				$this->json_array['status']['entity'] = '1';
//				$this->json_array['data']['Track']['id'] = $last_id;
//			}
//			return json_encode($this->json_array);
//		}
//	}
    function save_sp_todaystravel($data) {
        $userid = $data['userid'];
        $flag = $data['flag'];
        $comments = $data['comments'];
        $user_type = $data['user_type'];
        $visit_to_id = $data['visit_to_id']; //shop/ Trasnport office

        $sql = "SELECT * from tbl_sp_attendance WHERE sp_id='" . $userid . "' 
		AND DATE_FORMAT(tdate,'%Y-%m-%d')=DATE(NOW())";
        $proRow = $this->executeQuery($sql);
        $c_array_temp = array();
        while ($rowc = mysql_fetch_array($proRow)) {
            $c_array_temp['dayendtime'] = $rowc['dayendtime'];
            $c_array_temp['daystarttime'] = $rowc['daystarttime'];
            $c_array_temp['presenty'] = $rowc['presenty'];
        }
        if ($c_array_temp['presenty'] != 0) {
            if (mysql_num_rows($proRow) == 0) {
                $this->json_array['data']['Track']['msg'] = "Day not started yet.";
                $this->json_array['status']['responsecode'] = '0';
                $this->json_array['status']['entity'] = '1';
                $this->json_array['data']['Track']['id'] = '';
                return json_encode($this->json_array);
            } else if ($c_array_temp['dayendtime'] !== NULL) {
                $this->json_array['data']['Track']['msg'] = "Day already ended.";
                $this->json_array['status']['responsecode'] = '0';
                $this->json_array['status']['entity'] = '1';
                $this->json_array['data']['Track']['id'] = '';
                return json_encode($this->json_array);
            } else {
                if ($user_type == 'SalesPerson' || $user_type == '') {
                    $sql = "INSERT INTO tbl_shop_visit(salesperson_id, shop_visit_date_time, flag, comments, is_shop_location)
					VALUES
					('" . $userid . "',now(),'" . $flag . "','" . $comments . "','1')";
                    $proRow = $this->executeQuery($sql);
                    $last_id = mysql_insert_id();
                    $this->json_array['data']['Track']['msg'] = "Location track " . $flag . ".";
                    $this->json_array['status']['responsecode'] = '0';
                    $this->json_array['status']['entity'] = '1';
                    $this->json_array['data']['Track']['id'] = $last_id;
                } else if ($user_type == 'DeliveryPerson') {
                    $sql = "INSERT INTO tbl_transport_off_visit(`transport_off_id`, `deliveryperson_id`, `transport_off_visit_date_time`, `flag`, `comments`)
					VALUES
					('" . $visit_to_id . "', '" . $userid . "',now(),'" . $flag . "','" . $comments . "','1')";
                    $proRow = $this->executeQuery($sql);
                    $last_id = mysql_insert_id();
                    $this->json_array['data']['Track']['msg'] = "Location track " . $flag . ".";
                    $this->json_array['status']['responsecode'] = '0';
                    $this->json_array['status']['entity'] = '1';
                    $this->json_array['data']['Track']['id'] = $last_id;
                }
                return json_encode($this->json_array);
            }
        } else {
            $this->json_array['data']['Track']['msg'] = "Cant start day, you are on leave!";
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['Track']['id'] = '';
            return json_encode($this->json_array);
        }
    }

    function get_sp_pause_resume($userid, $user_type) {
        $sql = "SELECT * from tbl_sp_attendance WHERE sp_id='" . $userid . "' 
		AND DATE_FORMAT(tdate,'%Y-%m-%d')=DATE(NOW())";
        $proRow = $this->executeQuery($sql);
        $c_array_temp = array();
        while ($rowc = mysql_fetch_array($proRow)) {
            $c_array_temp['dayendtime'] = $rowc['dayendtime'];
            $c_array_temp['daystarttime'] = $rowc['daystarttime'];
        }
        if (mysql_num_rows($proRow) == 0 || $c_array_temp['dayendtime'] !== NULL) {
            $this->json_array['data']['flag'] = "2";
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '1';
            return json_encode($this->json_array);
        } else {
            if ($user_type == 'SalesPerson' || $user_type == '') {
                $sql = "SELECT salesperson_id, shop_visit_date_time, flag, comments, is_shop_location 
				FROM tbl_shop_visit where salesperson_id='" . $userid . "' and
				is_shop_location = '1' 
				and date_format(shop_visit_date_time, '%d-%m-%Y') = '" . date('d-m-Y') . "'";
                $proRow = $this->executeQuery($sql);
                $count_resume = mysql_num_rows($proRow);
                $newcount = $count_resume % 2;
                $this->json_array['data']['flag'] = (string) $newcount;
                $this->json_array['status']['responsecode'] = '0';
                $this->json_array['status']['entity'] = '1';
            } else if ($user_type == 'DeliveryPerson') {
                $sql = "SELECT `transport_off_id`, `deliveryperson_id`, `transport_off_visit_date_time`, `flag`
				FROM tbl_transport_off_visit where deliveryperson_id='" . $userid . "'
				and date_format(transport_off_visit_date_time, '%d-%m-%Y') = '" . date('d-m-Y') . "'";
                $proRow = $this->executeQuery($sql);
                $count_resume = mysql_num_rows($proRow);
                $newcount = $count_resume % 2;
                $this->json_array['data']['flag'] = (string) $newcount;
                $this->json_array['status']['responsecode'] = '0';
                $this->json_array['status']['entity'] = '1';
            }
            return json_encode($this->json_array);
        }
    }

    function get_invoice_pdf($orderid) {
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '9';
        $filename = SITEURL . "/templates/admin/uploadpdf/" . $orderid . ".pdf";
        $this->json_array['data']['invoice_pdf'] = SITEURL . "/templates/admin/uploadpdf/" . $orderid . ".pdf";
        return json_encode($this->json_array);
    }
    //customer order details
    function getorderdetailsbyordernumber_customer($ordernum) {
        //echo "<pre>";print_r($ordernum);die();
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '9';
      $where_clause_outer=" AND order_id='$ordernum' ";
         $order_sql = "SELECT tco.*,tu.firstname, pcatvar.category_name as cat_name,
           pcatvar.brand_name,pcatvar.product_name,pcatvar.product_variant1,pcatvar.product_variant2,
		   pcatvar.variant1_unit_name,pcatvar.variant2_unit_name,
		   pcatvar.product_price
               FROM tbl_customer_orders tco  
                left join `tbl_user` tu  on tco.distributor_id=tu.id
                LEFT join pcatbrandvariant pcatvar on tco.product_id=pcatvar.product_id 
                 and tco.product_var_id=pcatvar.product_variant_id
                WHERE 1=1 $where_clause_outer  "; //o.shop_order_status = ".$order_status."
        $proRow = $this->executeQuery($order_sql);
        
        //$result1 = $this->getOrders($ordernum);
        //echo "<pre>";print_r(count($result1));die();
        $ohistory_array_temp1 = array();
        $ohistory_array_temp2 = array();
        //$result1 = mysqli_query($con,$sql);
        if ($rowcount = mysql_num_rows($proRow) > 0) {
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '9';
           while ($value = mysql_fetch_array($proRow)) {
                $ohistory_array_temp['product_varient_id']=$value['product_var_id'];
                $ohistory_array_temp['distributorid']=$value['distributor_id'];
                $ohistory_array_temp['distributornm']=$value['firstname'];
                $ohistory_array_temp['customernm']=$value['customer_name'];
                $ohistory_array_temp['orderid']=$value['id'];
                $ohistory_array_temp['ordernum']=$value['order_id'];
                $ohistory_array_temp['variant_oder_id']='';
                //get product var details
				$var_name1="";$var_name2="";
                $weight = $value['product_variant1'];
				$weightunit = $value['variant1_unit_name']; 				
                if ($weight != '' ) {
                    $var_name1 = $weight . "-" . $weightunit . " ";
                }				
				$size = $value['product_variant2'];
				$sizeunit = $value['variant2_unit_name']; 				
                if ($size != '' ) {
                    $var_name2 = $size . "-" . $sizeunit . " ";
                }
				
                
                
                 $ohistory_array_temp['productname'] = $value['product_name'] . " " . $var_name1 ;
                    if (!empty($var_name2)) {
                        $ohistory_array_temp['productname'] .= " " . $var_name2;
                    }
                $ohistory_array_temp['order_date']=$value['order_date'];
                $ohistory_array_temp['unitprice']=$value['product_price'];
                $ohistory_array_temp['quantity']=$value['product_quantity'];
                $ohistory_array_temp['totalcost']=$value['total_cost_with_tax'];
                $ohistory_array_temp['catnm']=$value['cat_name'];
                $ohistory_array_temp['brandnm']=$value['brand_name'];
               $ohistory_array_temp['totalproductcount']='';
                $ohistory_array_temp1[]=$ohistory_array_temp;
            }
            if (!empty($ohistory_array_temp1))
                $this->json_array['data']['orderdetails'] = $ohistory_array_temp1;
        }else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['ohistory'][] = "No records found";
        }
        return json_encode($this->json_array);
    }
    function get_orderdata_customer($userid, $pageid) {
        $npageid = $pageid * 10;
        if ($pageid == '1')
            $limit = 0;
        else {
            $limit = ($pageid - 1) * 10;
        }
        $where_clause_outer="";
        $where_clause_outer=" AND customer_id='$userid' ";
         $order_sql = "SELECT tco.order_date,tco.order_id,
            count(tco.product_quantity) as product_quantity,sum(tco.total_cost_with_tax) as total_cost_with_tax          
            FROM tbl_customer_orders tco                
            WHERE 1=1 $where_clause_outer  
            GROUP BY tco.order_id limit $limit,$npageid "; //o.shop_order_status = ".$order_status."
        
       
        $proRow = $this->executeQuery($order_sql);

        if ($rowcount = mysql_num_rows($proRow) > 0) {
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '9';
            while ($row = mysql_fetch_array($proRow)) {
                $tcost = $row['total_cost_with_tax'];
                $sale_quantity = $row['product_quantity'];
                $ohistory_array_temp['tcost'] = number_format($tcost, 2, '.', ''); //$rowtc['tcost'];
                $ohistory_array_temp['free_quantity'] = '0';
                $ohistory_array_temp['sale_quantity'] = $sale_quantity;
                $ohistory_array_temp['quantity'] = $sale_quantity;
                $ohistory_array_temp['orderid'] = $row['order_id'];
                $ohistory_array_temp['order_date'] = $row['order_date'];               
                $ohistory_array_temp['offer_applied'] = "no";
                $this->json_array['data']['ohistory'][] = $ohistory_array_temp;
            }
        } else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['ohistory'][] = "No records found";
        }
        return json_encode($this->json_array);
    }
    function getorderdata_customer_delivery($dp_userid,$order_status,$client_id,$compnm) {
        //echo "<pre>";print_r($ordernum);die();
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '9';
     $where_clause_outer="";
        $where_clause_outer.=" AND assigned_to_dp='$dp_userid' ";        
        $where_clause_outer.=" AND tco.order_status ='$order_status' ";
         $order_sql = "SELECT tco.id as id,tco.customer_id,tco.distributor_id,tu.firstname as disname,tus.firstname as dpname
                    ,tco.current_lat,tco.current_long,
                    tco.order_id,tco.product_id,tco.product_var_id,tco.product_price,tco.product_quantity,
                    tco.total_cost,tco.total_cost_with_tax,tco.order_date,tco.order_status,
                    tco.assigned_to_dp,tco.assigned_datetime,tco.delivered_datetime ,
                    pcatview.brand_id,pcatview.brand_name,
                    pcatview.category_id,pcatview.category_name,pcatview.product_name,
                    pcatview.product_variant1,pcatview.product_variant2,
					 pcatview.variant1_unit_name,pcatview.variant2_unit_name
       FROM tbl_customer_orders tco  
       left join tbl_user tu on tco.distributor_id=tu.id
       left join tbl_user tus on tco.assigned_to_dp=tus.id
       left join pcatbrandvariant pcatview on tco.product_id = pcatview.product_id 
                and tco.product_var_id = pcatview.product_variant_id 
       WHERE 1=1 $where_clause_outer  ";
        $proRow = $this->executeQuery($order_sql);
        
        //$result1 = $this->getOrders($ordernum);
        //echo "<pre>";print_r(count($result1));die();
        $ohistory_array_temp1 = array();
        $ohistory_array_temp2 = array();
        //$result1 = mysqli_query($con,$sql);
        if ($rowcount = mysql_num_rows($proRow) > 0) {
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '9';
           while ($value = mysql_fetch_array($proRow)) {
               $ohistory_array_temp['client_id'] = $client_id;           
            $ohistory_array_temp['client_name'] = $compnm;
           
            $ohistory_array_temp['orderid'] = $value['order_id'];
            $ohistory_array_temp['order_date'] = $value['order_date'];               
           
            $ohistory_array_temp['customer_id'] = $value['customer_id'];               
            $ohistory_array_temp['distributor_id'] = $value['distributor_id'];   
             $ohistory_array_temp['distributor_name'] = $value['disname'];   
            
            $ohistory_array_temp['current_lat'] = $value['current_lat'];               
            $ohistory_array_temp['current_long'] = $value['current_long']; 
            $ohistory_array_temp['catnm']=$value['category_name'];
            $ohistory_array_temp['brandnm']=$value['brand_name'];
            $ohistory_array_temp['product_id'] = $value['product_id'];  
			//prod varient new code
			$var_name1="";$var_name2="";
                $weight = $value['product_variant1'];
				$weightunit = $value['variant1_unit_name']; 				
                if ($weight != '' ) {
                    $var_name1 = $weight . "-" . $weightunit . " ";
                }				
				$size = $value['product_variant2'];
				$sizeunit = $value['variant2_unit_name']; 				
                if ($size != '' ) {
                    $var_name2 = $size . "-" . $sizeunit . " ";
                }
            $ohistory_array_temp['productname'] = $value['product_name'] . " " . $var_name1 ;
            if (!empty($var_name2)) {
                $ohistory_array_temp['productname'] .= " " . $var_name2;
            }
            
            $ohistory_array_temp['product_var_id'] = $value['product_var_id'];   
            
            $ohistory_array_temp['product_price'] = $value['product_price'];               
            $ohistory_array_temp['product_quantity'] = $value['product_quantity'];  
            
            $ohistory_array_temp['total_cost'] = number_format($value['total_cost'], 2, '.', '');               
            $ohistory_array_temp['total_cost_with_tax'] = number_format($value['total_cost_with_tax'], 2, '.', ''); 
            
            $ohistory_array_temp['order_status'] = $value['order_status'];               
            $ohistory_array_temp['assigned_to_dp'] = $value['assigned_to_dp']; 
            $ohistory_array_temp['assigned_to_dp_name'] = $value['dpname']; 
            
            $ohistory_array_temp['assigned_datetime'] = $value['assigned_datetime'];
            $customer_details=array();
             $customer_details = $this->getcustomer_details($value['customer_id']);
             
            $ohistory_array_temp['customer_name'] = $customer_details['customer_name'];               
            $ohistory_array_temp['customer_phone_no'] = $customer_details['customer_phone_no']; 
            $ohistory_array_temp['customer_shipping_address'] = $customer_details['customer_shipping_address'];
            $ohistory_array_temp['ship_addr_lat']=$customer_details['ship_addr_lat'];
            $ohistory_array_temp['ship_addr_long']=$customer_details['ship_addr_long'];
            
            $ohistory_array_temp1[]=$ohistory_array_temp;
            }
            if (!empty($ohistory_array_temp1))
                $this->json_array['data']['orderdetails'] = $ohistory_array_temp1;
        }else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['ohistory'][] = "No records found";
        }
        return json_encode($this->json_array);
    }
     function getcustomer_details($id) {
       $conmain1 = mysqli_connect(SALZPOINT_DB_HOST, SALZPOINT_DB_USERNAME, SALZPOINT_DB_PASSWORD, SALZPOINT_DB_DATABASE) or die("cannot connect");
        $sql_customer = "SELECT `id`, `customer_name`, `customer_emailid`, `username`,
                         `customer_phone_no`, `customer_address`, 
                       `customer_type`, `customer_shipping_address`, `state`, `city`, `area`, `subarea`, 
                       `addr_lat`, `addr_long`, `ship_addr_lat`, `ship_addr_long`,
                        `added_on_date` FROM `tbl_customer_new` WHERE  id='" . $id . "' limit 1";
           $result = mysqli_query($conmain1, $sql_customer);
           while ($rowresult = mysqli_fetch_array($result)) {
               $arraynew['customer_name']=$rowresult['customer_name'];
               $arraynew['customer_phone_no']=$rowresult['customer_phone_no'];
               $arraynew['customer_shipping_address']=$rowresult['customer_shipping_address'];
               $arraynew['ship_addr_lat']=$rowresult['ship_addr_lat'];
               $arraynew['ship_addr_long']=$rowresult['ship_addr_long'];
           }
        return $arraynew;
    }
	//b2c common codebase to all
	function cash_on_delivery_customer($data) {		
		 foreach ($data->order_details as $key => $value) {        
				$order_id = $value->orderid; 
				$customer_order_sql = "UPDATE tbl_customer_orders SET order_status='3' where order_id='$order_id'"; 
				 $proRow = $this->executeQuery($customer_order_sql);				
				$count = $count + mysql_affected_rows();
			}
		$jsonOutput = array();
		if($count>0){
		   $jsonOutput['status']['responsecode'] = '0';
			$jsonOutput['status']['entity'] = '1';
			$jsonOutput['data']['invitation']['msg'] = "Delivery updated successfully."; 
		}else{
			$jsonOutput['status']['responsecode'] = '0';
			$jsonOutput['status']['entity'] = '1';
			$jsonOutput['data']['invitation']['msg'] = "Delivery not updated!";
		}
		return $jsonOutput;
    }
	//place orders methods
	function add_order($order_no, $order_date, $order_by_id, $lat, $long, $shop_id,$external_id) {		
		  $order_sql = "INSERT INTO `tbl_orders` 
		(`order_no`, `ordered_by`, `order_date`, `shop_id`, `order_from`,`order_to`,
		`total_items`, `total_cost`, `lat`, `long`)
		VALUES ('$order_no', '$order_by_id', '$order_date', '$shop_id', '$order_by_id','$external_id', 
		'$total_items', '$total_cost', '$lat', '$long')"; //o.shop_order_status = ".$order_status."
			
		$proRow = $this->executeQuery($order_sql);	
		return mysql_insert_id();
    }
	function add_order_details($order_product_details) {		
		$fields = "";
		$values = "";
		if(isset($order_product_details['producthsn']) && $order_product_details['producthsn'] != '')
		{			
			$fields.= ", producthsn";
			$values.= ", '".$order_product_details['producthsn']."'";			
		}
		if(isset($order_product_details['product_variant_weight2']) && $order_product_details['product_variant_weight2'] != '')
		{			
			$fields.= ", product_variant_weight2";
			$values.= ", '".$order_product_details['product_variant_weight2']."'";			
		}
		if(isset($order_product_details['product_variant_unit2']) && $order_product_details['product_variant_unit2'] != '')
		{			
			$fields.= ", product_variant_unit2";
			$values.= ", '".$order_product_details['product_variant_unit2']."'";			
		}
		if(isset($order_product_details['campaign_applied']) && $order_product_details['campaign_applied'] == 0)
		{			
			$fields.= ", campaign_applied, campaign_type, campaign_sale_type";
			$values.= ", '".$order_product_details['campaign_applied']."', '".$order_product_details['campaign_type']."', '".$order_product_details['campaign_sale_type']."'";			
		}
		if(isset($order_product_details['campaign_applied']) && $order_product_details['campaign_applied'] == 0 && $order_product_details['campaign_type'] == 'free_product')
		{			
			$fields.= ", free_product_details";
			$values.= ", '".$order_product_details['free_product_details']."'";			
		}
		
		  $order_sql = "INSERT INTO `tbl_order_details` 
		( `order_id`, `brand_id`, `cat_id`, `product_id`, `product_variant_id`, 
			`product_quantity`,`product_variant_weight1`, `product_variant_unit1`, 
			`product_unit_cost`, `product_total_cost`, `product_cgst`, 
			`product_sgst`, `p_cost_cgst_sgst`, `order_status`
		$fields) 
		VALUES ('".$order_product_details['order_record_id']."','".$order_product_details['brand_id']."',
		'".$order_product_details['cat_id']."', '".$order_product_details['product_id']."' , 
		'".$order_product_details['product_variant_id']."', 
		'".$order_product_details['product_quantity']."','".$order_product_details['product_variant_weight1']."',
		'".$order_product_details['product_variant_unit1']."', 
		'".$order_product_details['product_unit_cost']."', '".$order_product_details['product_total_cost']."',
		'".$order_product_details['product_cgst']."', 
		'".$order_product_details['product_sgst']."', '".$order_product_details['p_cost_cgst_sgst']."',
		'".$order_product_details['order_status']."' $values)";
		$proRow = $this->executeQuery($order_sql);	
		return mysql_insert_id();
	}
	
	
	function add_order_c_p_discount($campaign_id,$order_variant_id,$discount_amount,$discount_percent,$actual_amount)
	{
		  $sthsstockistsql = "INSERT INTO `tbl_order_cp_discount` 
			( `campaign_id`, `order_variant_id`,  `discount_amount` ,  `discount_percent` , `actual_amount`) 
			VALUES ('".$campaign_id."', '".$order_variant_id."', '".$discount_amount."', '".$discount_percent."' ,
			'".$actual_amount."')";
		$proRow = $this->executeQuery($sthsstockistsql);	
		return mysql_insert_id();
	}
	public function update_order($order_id, $total_items , $total_cost, $total_order_gst_cost,$offer_provided) 
	{		
         $sthsstockistsql = "UPDATE `tbl_orders` SET `total_items` = '".$total_items."' , 
		`total_cost` = '".$total_cost."', `total_order_gst_cost`= '".$total_order_gst_cost."',
		`offer_provided` = '".offer_provided."' WHERE `id` = '".$order_id."'";
		$proRow = $this->executeQuery($sthsstockistsql);	
		return mysql_insert_id();
	}
	function add_order_c_n_f_product($campaign_id,$c_p_order_variant_id,$f_p_order_variant_id)
	{		
         $sthsstockistsql = "INSERT INTO `tbl_order_c_n_f_product` 
			( `campaign_id`, `c_p_order_variant_id`,  `f_p_order_variant_id`) 
			VALUES ('".$campaign_id."', '".$c_p_order_variant_id."', '".$f_p_order_variant_id."')";
		$proRow = $this->executeQuery($sthsstockistsql);	
		return mysql_insert_id();
	}
	
	public function update_order_dcp($order_product_details) {
              // echo "<pre>";print_r($order_product_details);//die();
                $dcp_stock_sql="SELECT stock_qnty,sale_qnty 
                    FROM tbl_dcp_stock 
                    WHERE prod_id = '".$order_product_details['product_id']."' and 
                        prod_var_id='".$order_product_details['prod_var_id']."' and 
                        dcp_id='".$order_product_details['dcp_id']."' and 
                     cartons_id='".$order_product_details['cartons_id']."' ";
             $proRow = $this->executeQuery($dcp_stock_sql);	
			 $resultsname = mysql_fetch_array($proRow);
                
		$stock_qnty=$resultsname['stock_qnty'];
                $pre_sale_qnty=$resultsname['sale_qnty'];
                $temp=$stock_qnty-$order_product_details['sale_qnty'];
                $new_sale_qnty=$pre_sale_qnty+$order_product_details['sale_qnty'];
                 $dates= date('d-m-Y');           
                  $update_stock_sql="
                    UPDATE `tbl_dcp_stock` 
                     SET
                     order_no='".$order_product_details['order_no']."',
                         stock_qnty='".$temp."',sale_qnty='".$new_sale_qnty."',
                     updated_date=now(),usr_lat='".$order_product_details['usr_lat']."'
                         ,usr_lng='".$order_product_details['usr_lng']."'
                    WHERE prod_id = '".$order_product_details['product_id']."' and 
                        prod_var_id='".$order_product_details['prod_var_id']."' and 
                        dcp_id='".$order_product_details['dcp_id']."' and 
                     cartons_id='".$order_product_details['cartons_id']."' and stock_qnty>0";
                  $proRowupdate = $this->executeQuery($update_stock_sql);
                
		 $tbl_dcp_orders_sql=" INSERT INTO `tbl_dcp_orders` 
		( `order_by`,`order_id`,`cartons_id`,`order_date`, `product_id`, `product_variant_id`, 
		`product_quantity`, `product_variant_weight1`, `product_variant_unit1`, 
		`product_unit_cost`, `product_total_cost`, `product_cgst`, 
		`product_sgst`, `p_cost_cgst_sgst`,`usr_lat`,`usr_lng`) 
		VALUES ('".$order_product_details['dcp_id']."','".$order_product_details['order_no']."','".$order_product_details['cartons_id']."',now(), '".$order_product_details['product_id']."' ,
                    '".$order_product_details['prod_var_id']."', '".$order_product_details['sale_qnty']."' ,
                    '".$order_product_details['product_variant_weight1']."', '".$order_product_details['product_variant_unit1']."' ,
                    '".$order_product_details['product_unit_cost']."', '".$order_product_details['product_total_cost']."' ,
                    '".$order_product_details['product_cgst']."', '".$order_product_details['product_sgst']."' ,
                         '".$order_product_details['p_cost_cgst_sgst']."','".$order_product_details['usr_lat']."',"
                         . "'".$order_product_details['usr_lng']."') ";
		$proRowinsert = $this->executeQuery($tbl_dcp_orders_sql);	
		return $proRowupdate;
	}
	function fnplaceorder_customer($userid,$distributor_id,$current_lat, $current_long, $order_id,
                    $product_id, $product_varient_id,$product_price,$product_quantity,$total_cost
            ,$total_cost_with_tax,$order_status)
	{		
		 $customer_place_sql = "INSERT INTO `tbl_customer_orders` 
		(`customer_id`, `distributor_id`, `current_lat`,`current_long`,`order_id`
                ,`product_id`,`product_var_id`,`product_price`,`product_quantity`, `total_cost`
                ,`total_cost_with_tax`, `order_date`,`order_status`)
		VALUES ('".$userid."','".$distributor_id."','".$current_lat."','".$current_long."','".$order_id."',
                    '".$product_id."','".$product_varient_id."','".$product_price."','".$product_quantity."','".$total_cost."'
            ,'".$total_cost_with_tax."',now(),'".$order_status."')";
		$proRow = $this->executeQuery($customer_place_sql);	
            	
		return mysql_insert_id();
		
	}
	function update_lead_status($shop_id,$reminder,$visited_date,$lead_status,$notification) 
	{	
		$added_reminder = date("Y-m-d H:i:s",strtotime($reminder));
		   $sql = "INSERT INTO tbl_lead_details(shop_id,reminder,visited_date,notification,lead_status,created_on)
		 VALUES('$shop_id','$added_reminder',now(),'$notification','$lead_status',now()) ";     
			//die();
		$proRow = $this->executeQuery($sql);	
		$count= mysql_insert_id();
		//echo $lead_status;//die();
		
		if($count>0){
			 $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['lead'] = "Lead status updated successfully.";
		}else{
			 $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['lead'] = "Lead status not updated";
		}
		 return json_encode($this->json_array);
	}
	//lead
	function get_lead_terms() {
        $sql = "SELECT id, quetions,q_type FROM tbl_lead_terms ";
        $proRow = $this->executeQuery($sql);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '9';
        while ($row = mysql_fetch_array($proRow)) {          
            $s_array_temp['id'] = $row['id'];
			  $s_array_temp['quetions'] = $row['quetions'];
			  $s_array_temp['type'] = $row['q_type']; 
            $this->json_array['data']['terms'][] = $s_array_temp;
        }		
        return json_encode($this->json_array);
    }
	function save_lead_terms($lead_array) {
		$lead_id=$lead_array->lead_id;
		$lead_detail_id=$lead_array->lead_detail_id;
		$question_list=$lead_array->question_list;
		foreach($lead_array->question_list as $key=>$value){
			if($value->que_id!=2){
				$que_ids[$key] = $value->que_id; 
				$ans[$key] = $value->ans;
			}else{
				//$que_ids[$key] = $value->que_id; 
				$start_order_date = date("Y-m-d H:i:s",strtotime($value->ans));				
			}
			
		}
		$lead_term_ids = implode (",", $que_ids);
		$lead_terms_ans = implode (",", $ans);
		
         $sql_lead_terms = "UPDATE tbl_lead_details SET
		lead_term_ids='$lead_term_ids',start_order_date='$start_order_date'
		,lead_terms_ans='$lead_terms_ans',lead_status='4'
		where id='$lead_detail_id'";
		//exit;
        $proRow = $this->executeQuery($sql_lead_terms);
		
			$need_sql="INSERT INTO tbl_shops
       ( `name`, `address`, `city`, `state`, `contact_person`,
				 `mobile`, `shop_added_by`,`shop_type_id`, `lat`, `lon`, `contact_person_other`,
				 `mobile_number_other`, `gst_number`, `suburbid`, `closedday`,
				 `opentime`, `closetime`, `latitude`, `longitude`, `subarea_id`,
				 `sstockist_id`, `stockist_id`, `status`, `status_seen_log`, 
				 `isdeleted`,  `billing_name`, `bank_name`,
				 `bank_acc_name`, `bank_acc_no`,
				 `bank_b_name`, `bank_ifsc`, `shop_status`, `added_on`) 
		SELECT 
            `name`, `address`, `city`, `state`, `contact_person`,
				 `mobile`, `shop_added_by`,`shop_type_id`, `lat`, `lon`, `contact_person_other`,
				 `mobile_number_other`, `gst_number`, `suburbid`, `closedday`,
				 `opentime`, `closetime`, `latitude`, `longitude`, `subarea_id`,
				 `sstockist_id`, `stockist_id`, `status`, `status_seen_log`, 
				 `isdeleted`,  `billing_name`, `bank_name`,
				 `bank_acc_name`, `bank_acc_no`,
				 `bank_b_name`, `bank_ifsc`, `shop_status`, `added_on`
       FROM tbl_leads tl
           WHERE tl.id='$lead_id' " ;
			// $sql_lead = "select * from tbl_leads where id='".$shop_id."' limit 1";        
			$proRow_lead = $this->executeQuery($need_sql);
		
        $this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['msg'] = "Lead confirmed and converted to shop successfully.";		
        return json_encode($this->json_array);
    }
 function fnEditLead($salespersonid, $store_name, 
	 $contact_no, $store_Address, $lat, $lng,  $leadid,$lead_detail_id,$reminder,$visited_date,
	 $lead_status,$notification) {
        /* Get Stockist id */
		//$city, $state, $suburbid, $subarea_id,
       

         $sql = "UPDATE tbl_leads SET 
		name='$store_name',
		address='$store_Address',		
		mobile='$contact_no',
		shop_added_by='$salespersonid',
		latitude='$lat',                   
		longitude='$lng'		
		where id='$leadid'";
        $proRow = $this->executeQuery($sql);
		$added_reminder = date("Y-m-d H:i:s",strtotime($reminder));
		$added_visited_date = date("Y-m-d H:i:s",strtotime($visited_date));
		
		/*$sql_lead_details = "UPDATE tbl_lead_details SET 
		reminder='$added_reminder',
		visited_date='$added_visited_date',
		lead_status='$lead_status',
		notification='$notification'
		where id='$lead_detail_id'";

        $proRow_leads = $this->executeQuery($sql_lead_details);*/

        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '1';
        $this->json_array['data']['Store']['msg'] = "Lead updated successfully.";
        return json_encode($this->json_array);
    }
	function get_sp_targets($spid) {        
		$sql1="SELECT `id`,`assign_user_type`, `target_type`,`start_date`,`end_date`,
			(SELECT firstname FROM tbl_user WHERE id = tbl_target.assign_user_id) AS name,
			(SELECT unitname FROM tbl_units WHERE id = tbl_target.variant1_wt) AS unitname1,		
			`target_in_rs`,`target_in_wt` 
			FROM tbl_target 
			WHERE assign_user_id='".$spid."' 
			ORDER BY id DESC LIMIT 1";
            $proRow = $this->executeQuery($sql1);	
		
		if (mysql_num_rows($proRow) != 0) {
			while ($row = mysql_fetch_array($proRow)) { 
				$s_array_temp['id'] = $row['id'];
				$s_array_temp['target_type'] = $row['target_type'];
				
				$s_array_temp['start_date'] = $row['start_date'];
				$s_array_temp['end_date'] = $row['end_date'];
				$s_array_temp['unitname1'] = $row['unitname1']; 
				
				$where ="";
				if($spid!=''){	
					$where.=" AND o.ordered_by='".$spid."' ";
				}
				if($s_array_temp['target_type']=='spdate'){	
					$where.=" AND date_format(o.order_date, '%Y-%m-%d') BETWEEN STR_TO_DATE('" . $s_array_temp['start_date'] . "','%d-%m-%Y') 
					 AND STR_TO_DATE('" . $s_array_temp['end_date'] . "','%d-%m-%Y') ";
					
				}
				if($s_array_temp['target_type']=='daily'){
					$where.=" AND date_format(o.order_date, '%Y-%m-%d')= DATE(NOW())";
				}
				if($s_array_temp['target_type']=='monthly'||$s_array_temp['target_type']=='weekly'){
					$date_array=explode('::', $s_array_temp['start_date']);	
					$where.=" AND date_format(o.order_date, '%Y-%m-%d') BETWEEN STR_TO_DATE('" . $date_array[0] . "','%d-%m-%Y') 
					 AND STR_TO_DATE('" . $date_array[1] . "','%d-%m-%Y') ";
				}				
				  $sql_targetvalue = "SELECT o.order_date,
						od.p_cost_cgst_sgst as total_order_gst_cost,
						od.product_total_cost,
						od.product_quantity,od.product_variant_weight1,od.product_variant_unit1 
				FROM	tbl_orders o 
				LEFT JOIN tbl_order_details od ON o.id = od.order_id 
				WHERE 1=1 $where";
				 $proRow_targetvalue = $this->executeQuery($sql_targetvalue);
					$saleruppes=0;$saleweight=0;
				if (mysql_num_rows($proRow_targetvalue) != 0) {
					while ($row_targetvalue = mysql_fetch_array($proRow_targetvalue)) { 
						$saleruppes+=$row_targetvalue['total_order_gst_cost'];
						
						$order_units=$row_targetvalue['product_variant_unit1'];
						if($order_units=='kg'){
							$orderweight=$row_targetvalue['product_variant_weight1']*1000;
						}
						if($order_units=='gm'){
							$orderweight=$row_targetvalue['product_variant_weight1'];
						}
						
						$saleweight+=$row_targetvalue['product_quantity']*$orderweight;
					}
				}			
				if($s_array_temp['unitname1']=='kg'){
					$saleweight=$saleweight/1000;
				}
				$s_array_temp['start_date'] = $row['start_date'];
				$s_array_temp['name'] = $row['name'];
				$target_in_rs = $row['target_in_rs']; 
				$target_in_wt = $row['target_in_wt'];
				
				$s_array_temp['saleruppes'] = (string)$saleruppes; 
				$s_array_temp['saleweight'] = (string)$saleweight; 
				
				
				$remain_ruppes1=0;
				$remain_weight1=0;
				if(($target_in_rs-$saleruppes)>=0){
					$remain_ruppes1 = $target_in_rs-$saleruppes; 
				}
				if(($target_in_wt-$saleweight)>=0){
					$remain_weight1 = $target_in_wt-$saleweight; 
				}
				$s_array_temp['remain_ruppes'] = (string)($remain_ruppes1); 
				$s_array_temp['remain_weight'] = (string)($remain_weight1); 
				
				$s_array_temp['target_in_rs'] = $target_in_rs; 
				$s_array_temp['target_in_wt'] = $target_in_wt; 
				
				$this->json_array['data']['targets'][] = $s_array_temp;
			}	
			$this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '1';
		}else{
			$this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['leave']['msg'] = "You Have No Target Yet!.";
		}  
        return json_encode($this->json_array);
    }
	function get_sp_leaves($spid) {      
		$sql1="SELECT distinct `leave_id`
			FROM tbl_sp_attendance 
			WHERE sp_id='".$spid."' AND presenty!='1' ";
		$proRow1 = $this->executeQuery($sql1);	
		if (mysql_num_rows($proRow1) != 0) {			
			while ($row = mysql_fetch_array($proRow1)) { 
				$leave_id = $row['leave_id'];
				$sql2="SELECT max(`tdate`) as enddate,min(`tdate`) as startdate,count(`id`) as daycount, `reason`, 
				`description`,`leave_status`,`leave_id`
				FROM tbl_sp_attendance 
				WHERE sp_id='".$spid."' AND presenty!='1' AND leave_id='".$leave_id."'";
				$proRow2 = $this->executeQuery($sql2);	
				if (mysql_num_rows($proRow2) != 0) {			
					while ($row = mysql_fetch_array($proRow2)) { 
						$s_array_temp['leave_id'] = $leave_id;
						$s_array_temp['startdate'] = date("Y-m-d",strtotime($row['startdate']));
						$s_array_temp['enddate'] = date("Y-m-d",strtotime($row['enddate'])); 
						$s_array_temp['daycount'] = $row['daycount']; 
						$temparr=array("0" => "Pending", "1" => "Approved","2"=>"Rejected");
						$leavs_st=$row['leave_status']; 
						$s_array_temp['leave_status'] = $temparr[$leavs_st];
						$this->json_array['data']['leaves'][] = $s_array_temp;
					}
				}
			}
			$this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '1';
		}else{
			$this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['leave']['msg'] = "You Have No Leave Application Yet!.";
		}  
        return json_encode($this->json_array);
    }
}
