--new changes as dynamic mrp or margin 
ALTER TABLE `tbl_product_variant` ADD `mrp_or_margin_flag` INT NOT NULL COMMENT '1=mrp,2=margin' AFTER `price`;
ALTER TABLE `tbl_product_variant` CHANGE `mrp_or_margin_flag` `mrp_or_margin_flag` INT(11) NOT NULL DEFAULT '2' COMMENT '1=mrp,2=margin';

UPDATE `tbl_product_variant` SET `mrp_or_margin_flag` = '2' ;

--note start
--how much user level or user type count that much columns added in tbl_product_variant
--mrp or margin
--note end

ALTER TABLE `tbl_product_variant` 
CHANGE `price_ss` `price_level1` FLOAT(8,2) NOT NULL, 
CHANGE `price_dcp` `price_level2` FLOAT(8,2) NOT NULL, 
CHANGE `margin_ss` `price_level3` FLOAT(8,2) NOT NULL, 
CHANGE `margin_dcp` `price_level4` FLOAT(8,2) NOT NULL;

ALTER TABLE `tbl_product_variant` 
ADD `price_level5` FLOAT(8,2) NOT NULL AFTER `price_level4`, 
ADD `price_level6` FLOAT(8,2) NOT NULL AFTER `price_level5`, 
ADD `price_level7` FLOAT(8,2) NOT NULL AFTER `price_level6`, 
ADD `price_level8` FLOAT(8,2) NOT NULL AFTER `price_level7`;

-- add Shopkeeper user type in tbl_user 
ALTER TABLE `tbl_user` CHANGE `user_type` `user_type` 
ENUM('Admin','Superstockist','Distributor','SalesPerson','DeliveryPerson','Accountant',
'DeliveryChannelPerson','Shopkeeper') 
CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;


CREATE TABLE `tbl_lead_confirm_form` (
  `id` int(11) NOT NULL,
  `quetions` int(11) NOT NULL,
  `yes_no` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_lead_terms` (
  `id` int(11) NOT NULL,
  `quetions` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `tbl_lead_terms` (`id`, `quetions`) VALUES
(1, 'Are you agree with terms?'),
(2, '1.Deposit - 100000/- only?'),
(3, 'Feezer is available or not?'),
(4, 'Shop start Date?'),
(5, '1st Order ?');

ALTER TABLE `tbl_lead_terms`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tbl_lead_terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
  
ALTER TABLE `tbl_lead_details` ADD `lead_term_ids` VARCHAR(255) NOT NULL AFTER `created_on`, ADD `lead_terms_ans` VARCHAR(255) NOT NULL AFTER `lead_term_ids`;
ALTER TABLE `tbl_product` ADD `mrp_or_margin_flag` INT NOT NULL DEFAULT '2' AFTER `added_by_usertype`;

ALTER TABLE `tbl_lead_details` CHANGE `lead_status` `lead_status` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '1=Pending,2=Confirm,3=Cancel,4=Added_in_shop';

ALTER TABLE `tbl_lead_details` ADD `start_order_date` DATETIME NOT NULL AFTER `lead_term_ids`;

ALTER TABLE `tbl_shops` ADD `shop_owner_id` INT NOT NULL AFTER `added_on`;

ALTER TABLE `tbl_lead_terms` ADD `q_type` VARCHAR(255) NOT NULL AFTER `quetions`;

ALTER TABLE `tbl_order_stockist` ADD `product_unitcost` FLOAT(10,2) NOT NULL AFTER `product_sgst`;

ALTER TABLE `tbl_target` ADD `target_in_wt` FLOAT(10,2) NOT NULL AFTER `target_in_rs`, 
	ADD `variant1_wt` INT NOT NULL AFTER `target_in_wt`, 
	ADD `target_in_pcs` FLOAT(10,2) NOT NULL AFTER `variant1_wt`
	, ADD `variant1_pcs` INT NOT NULL AFTER `target_in_pcs`;
	
ALTER TABLE `tbl_target` ADD `added_by_usertype` VARCHAR(100) NOT NULL AFTER `added_by`;

ALTER TABLE `tbl_sp_attendance` ADD `leave_status` TINYINT NOT NULL DEFAULT '0' AFTER `description`;

ALTER TABLE `tbl_sp_attendance` CHANGE `leave_status` `leave_status` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '0=pending,1=approved';

ALTER TABLE `tbl_sp_attendance` ADD `leave_id` INT NOT NULL DEFAULT '0' AFTER `leave_status`;

ALTER TABLE `tbl_sp_attendance` CHANGE `leave_id` `leave_id` VARCHAR(100) NULL DEFAULT '0';

ALTER TABLE `tbl_sp_attendance` CHANGE `leave_status` `leave_status` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '0=pending,1=approved,2=Rejected';