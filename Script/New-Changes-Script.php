ALTER TABLE `tbl_brand` ADD `added_by_userrole` VARCHAR(250) NOT NULL AFTER `added_by_usertype`, ADD `createdon` DATETIME NOT NULL AFTER `added_by_userrole`;


ALTER TABLE `tbl_category` ADD `added_by_userrole` VARCHAR(250) NOT NULL AFTER `added_by_usertype`, ADD `createdon` DATETIME NOT NULL AFTER `added_by_userrole`

ALTER TABLE `tbl_units` ADD `createdon` DATETIME NOT NULL AFTER `deleted_on`;


ALTER TABLE `tbl_product` ADD `added_by_userrole` VARCHAR(250) NOT NULL AFTER `added_by_usertype`;


ALTER TABLE `tbl_orders` ADD `order_created_by` INT NOT NULL DEFAULT '0' AFTER `order_no`, ADD `order_from` INT NOT NULL AFTER `order_created_by`, ADD `order_to` INT NOT NULL AFTER `order_from`;


ALTER TABLE `tbl_order_details`
  DROP `supp_chnz_status`;


  ALTER TABLE `tbl_order_details`
  DROP `supp_chnz_status`;

  ALTER TABLE `tbl_user_tree` ADD `all_parent_id` VARCHAR(250) NOT NULL AFTER `parent_id`;


  ALTER TABLE `tbl_pages` ADD `section_icon` VARCHAR(250) NOT NULL AFTER `isdeleted`;



 ------29-03-2019  (srock Management) ----------------------

  ALTER TABLE `tbl_stock_management` ADD `product_variant1` VARCHAR(50) NOT NULL AFTER `product_variant`, ADD `variant1_unit_name` VARCHAR(50) NOT NULL AFTER `product_variant1`, ADD `variant2_unit_name` VARCHAR(50) NOT NULL AFTER `variant1_unit_name`, ADD `product_variant2` VARCHAR(50) NOT NULL AFTER `variant2_unit_name`;

  ALTER TABLE `tbl_stock_management` ADD `brand_name` VARCHAR(100) NOT NULL AFTER `createdon`;

  ALTER TABLE `tbl_stock_management` ADD `product_price` FLOAT(8,2) NOT NULL AFTER `producthsn`, ADD `product_cgst` FLOAT(8,2) NOT NULL AFTER `product_price`, ADD `product_sgst` FLOAT(8,2) NOT NULL AFTER `product_cgst`;


 ------4-034-2019  (Customer Management) ----------------------

  ALTER TABLE `tbl_customer` ADD `added_by` INT NOT NULL AFTER `cust_address`, ADD `isdeleted` INT NOT NULL DEFAULT '0' AFTER `added_by`, ADD `createdon` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `isdeleted`;


 ------15-04-2019  (Stock Management) ----------------------
  ALTER TABLE `tbl_stock_management` CHANGE `stock_status` `stock_status` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '1=Add Into Stcok, 2=Remove From Stock';