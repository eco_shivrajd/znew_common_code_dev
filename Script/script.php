http://salzpoint.net/uatnew/kaysons/templates/admin/imp-script-for-location.php








DROP TABLE `tbl_user`, `tbl_user_details`, `tbl_user_working_area`;



CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `common_user_id` int(11) NOT NULL,
  `old_id` int(11) NOT NULL,
  `external_id` varchar(255) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `username` varchar(255) NOT NULL,
  `pwd` varchar(50) NOT NULL,
  `user_type` enum('Admin','Superstockist','Distributor','SalesPerson','DeliveryPerson','Accountant','DeliveryChannelPerson') NOT NULL,
  `subarea_ids` varchar(250) NOT NULL,
  `suburb_ids` varchar(250) NOT NULL,
  `city` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `stockist_id` varchar(250) NOT NULL,
  `sstockist_id` int(11) NOT NULL DEFAULT '0',
  `office_phone_no` varchar(15) NOT NULL,
  `accname` varchar(250) NOT NULL,
  `accno` varchar(250) NOT NULL,
  `bank_name` varchar(250) NOT NULL,
  `accbrnm` varchar(250) NOT NULL,
  `accifsc` varchar(250) NOT NULL,
  `gst_number_sss` varchar(20) NOT NULL,
  `latitude` varchar(250) NOT NULL,
  `longitude` varchar(250) NOT NULL,
  `reset_key` varchar(255) DEFAULT NULL,
  `is_default_user` enum('0','1') NOT NULL DEFAULT '0',
  `isdeleted` tinyint(4) NOT NULL DEFAULT '0',
  `user_status` enum('Active','Inactive') DEFAULT 'Active',
  `is_common_user` tinyint(4) NOT NULL DEFAULT '0',
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

  ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

  ALTER TABLE `tbl_product` ADD `added_on` DATETIME NOT NULL AFTER `added_by_usertype`;

  ALTER TABLE `tbl_product_variant` ADD `variant1_unit_id` INT NOT NULL AFTER `variant_2`, ADD `variant2_unit_id` INT NOT NULL AFTER `variant1_unit_id`;

  ALTER TABLE `tbl_product_variant` ADD `carton_quantities` INT NOT NULL AFTER `sgst`;

 ALTER TABLE tbl_surb RENAME tbl_area

 ALTER TABLE `tbl_sp_attendance` ADD `daystarttime` DATETIME NOT NULL AFTER `presenty`;



INSERT INTO `tbl_user` (`id`, `common_user_id`, `old_id`, `external_id`, `firstname`, `username`, `pwd`, `user_type`, `subarea_ids`, `suburb_ids`, `city`, `state`, `address`, `mobile`, `email`, `stockist_id`, `sstockist_id`, `office_phone_no`, `accname`, `accno`, `bank_name`, `accbrnm`, `accifsc`, `gst_number_sss`, `latitude`, `longitude`, `reset_key`, `is_default_user`, `isdeleted`, `user_status`, `is_common_user`, `added_on`) VALUES
(1, 0, 1, '1', 'vishwasfoods', 'vishwasfoods@admin.com', 'e10adc3949ba59abbe56e057f20f883e', 'Admin', '0', '0', 2763, 22, 'Pune', '0000000000', 'vishwasfoods@admin.com', '0', 0, '', '', '', '', '', '', '', '', '', '', '0', 0, NULL, 0, '0000-00-00 00:00:00');




CREATE VIEW pcatbrandvariant AS
(select `b`.`id` AS `brand_id`,`c`.`id` AS `category_id`,`p`.`id` AS `product_id`,`u`.`unitname` AS `variant1_unit_name`,`u2`.`unitname` AS `variant2_unit_name`,`pv`.`id` AS `product_variant_id`,`b`.`name` AS `brand_name`,`c`.`categorynm` AS `category_name`,`c`.`categoryimage` AS `category_image`,`p`.`productname` AS `product_name`,`p`.`added_by_userid` AS `added_by_userid`,`p`.`added_by_usertype` AS `added_by_usertype`,`pv`.`productimage` AS `product_image`,`pv`.`price` AS `product_price`,`pv`.`variant_1` AS `product_variant1`,`pv`.`variant_2` AS `product_variant2`,`pv`.`variant_cnt` AS `variant_cnt`,`pv`.`producthsn` AS `product_hsn`,`pv`.`cgst` AS `product_cgst`,`pv`.`sgst` AS `product_sgst`,`pv`.`productbarcodeimage` AS `product_barcode_img`,`pv`.`carton_quantities` AS `carton_quantities` from 
(((`salzpoin_znew_uatvishwasfoods`.`tbl_brand` `b` join `salzpoin_znew_uatvishwasfoods`.`tbl_category` `c` on((`b`.`id` = `c`.`brandid`))) 
join `salzpoin_znew_uatvishwasfoods`.`tbl_product` `p` on((`c`.`id` = `p`.`catid`))) 
join `salzpoin_znew_uatvishwasfoods`.`tbl_product_variant` `pv` on((`p`.`id` = `pv`.`productid`))
join `salzpoin_znew_uatvishwasfoods`.`tbl_units` `u` on((`pv`.`variant1_unit_id` = `u`.`id`))
join `salzpoin_znew_uatvishwasfoods`.`tbl_units` `u2` on((`pv`.`variant2_unit_id` = `u2`.`id`))
)
 where ((`b`.`isdeleted` <> 1) and (`c`.`isdeleted` <> 1) and (`p`.`isdeleted` <> 1)))



 CREATE ALGORITHM=UNDEFINED DEFINER=`salzpoint`@`localhost` SQL SECURITY DEFINER VIEW `bcatproduct`  AS  (select `b`.`id` AS `brand_id`,`c`.`id` AS `category_id`,`p`.`id` AS `product_id`,`b`.`name` AS `brand_name`,`c`.`categorynm` AS `category_name`,`p`.`productname` AS `product_name` from ((`tbl_brand` `b` join `tbl_category` `c` on((`b`.`id` = `c`.`brandid`))) join `tbl_product` `p` on((`c`.`id` = `p`.`catid`)))) ;


CREATE ALGORITHM=UNDEFINED DEFINER=`salzpoint`@`localhost` SQL SECURITY DEFINER VIEW `tbl_user_view`  AS  select `u`.`id` AS `id`,`u`.`external_id` AS `external_id`,`u`.`firstname` AS `firstname`,`u`.`username` AS `username`,`u`.`pwd` AS `pwd`,`u`.`user_type` AS `user_type`,`u`.`address` AS `address`,`c`.`name` AS `cityname`,`u`.`city` AS `city_id`,`u`.`state` AS `state_id`,`s`.`name` AS `statename`,`u`.`suburb_ids` AS `suburb_ids`,`su`.`suburbnm` AS `suburbnm`,`u`.`subarea_ids` AS `subarea_ids`,`sub`.`subarea_name` AS `subarea_name`,`u`.`mobile` AS `mobile`,`u`.`email` AS `email`,`u`.`stockist_id` AS `stockist_id`,`u1`.`firstname` AS `stockist_name`,`u`.`sstockist_id` AS `sstockist_id`,`u2`.`firstname` AS `sstockist_name`,`u`.`office_phone_no` AS `office_phone_no`,`u`.`accname` AS `accname`,`u`.`accno` AS `accno`,`u`.`bank_name` AS `bank_name`,`u`.`accbrnm` AS `accbrnm`,`u`.`accifsc` AS `accifsc`,`u`.`gst_number_sss` AS `gst_number_sss`,`u`.`latitude` AS `latitude`,`u`.`longitude` AS `longitude`,`u`.`is_default_user` AS `is_default_user`,`u`.`is_common_user` AS `is_common_user`,`u`.`added_on` AS `added_on` from ((((((`tbl_user` `u` left join `tbl_state` `s` on((`u`.`state` = `s`.`id`))) left join `tbl_city` `c` on((`u`.`city` = `c`.`id`))) left join `tbl_area` `su` on((`u`.`suburb_ids` = `su`.`id`))) left join `tbl_subarea` `sub` on((`u`.`subarea_ids` = `sub`.`subarea_id`))) left join `tbl_user` `u1` on((`u1`.`id` = `u`.`stockist_id`))) left join `tbl_user` `u2` on((`u2`.`id` = `u`.`sstockist_id`))) where ((`u`.`isdeleted` <> '1') and (`u`.`is_common_user` <> '1')) order by `u`.`id` desc ;


CREATE ALGORITHM=UNDEFINED DEFINER=`salzpoint`@`localhost` SQL SECURITY DEFINER VIEW `tbl_shop_view`  AS  select `sp`.`id` AS `id`,`sp`.`status` AS `status`,`sp`.`status_seen_log` AS `status_seen_log`,`u`.`firstname` AS `sstockist`,`u1`.`firstname` AS `stockist`,`sp`.`name` AS `shop_name`,`sp`.`contact_person` AS `contact_person`,`sp`.`mobile` AS `mobile`,`c`.`name` AS `cityname`,`s`.`name` AS `statename`,`su`.`suburbnm` AS `suburbnm`,`sub`.`subarea_name` AS `subarea_name`,`sp`.`billing_name` AS `billing_name`,`sp`.`bank_name` AS `bank_name`,`sp`.`bank_acc_name` AS `bank_acc_name`,`sp`.`bank_acc_no` AS `bank_acc_no`,`sp`.`bank_b_name` AS `bank_b_name`,`sp`.`bank_ifsc` AS `bank_ifsc`,`sp`.`added_on` AS `added_on` from ((((((`tbl_shops` `sp` left join `tbl_user` `u` on((`u`.`id` = `sp`.`sstockist_id`))) left join `tbl_user` `u1` on((`u1`.`id` = `sp`.`stockist_id`))) left join `tbl_state` `s` on((`sp`.`state` = `s`.`id`))) left join `tbl_city` `c` on((`sp`.`city` = `c`.`id`))) left join `tbl_area` `su` on((`sp`.`suburbid` = `su`.`id`))) left join `tbl_subarea` `sub` on((`sp`.`subarea_id` = `sub`.`subarea_id`))) where (`sp`.`isdeleted` <> '1') order by `sp`.`id` desc ;



