

ALTER TABLE `tbl_units` ADD `variantid` INT NOT NULL AFTER `id`;

( SELECT DISTINCT(SV.shop_id), s.name AS shopname, o.lat, o.long, no_o_lat, no_o_long, 
s.latitude, s.longitude,	SV.shop_visit_date_time AS date, 
o.total_order_gst_cost, s.added_on,	SV.shop_visit_reason, 
SV.shop_close_reason_type, SV.shop_close_reason_details 
FROM tbl_shop_visit AS SV	
LEFT JOIN tbl_shops s ON s.id= SV.shop_id 
LEFT JOIN tbl_orders o ON o.shop_id= SV.shop_id AND o.order_date =SV.shop_visit_date_time 
WHERE date_format(SV.shop_visit_date_time, '%d-%m-%Y') = '04-08-2018' AND SV.salesperson_id = '9526' 
GROUP BY s.id ORDER BY SV.shop_visit_date_time ASC ) 
 ORDER BY case when date is null then 1 else 0 end, date;

SELECT `id`, `userid`, `lattitude`, `longitude`, `tdate`, `shop_id` 
FROM tbl_user_location 
WHERE date_format(tdate, '%d-%m-%Y') = '04-08-2018' AND userid = '9526' 
and lattitude > 0 and lattitude is not null 
and longitude > 0 and longitude is not null ORDER BY id DESC;

--new table tbl_userlevel
CREATE TABLE `tbl_userlevel` (
  `id` int(11) NOT NULL,
  `level` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `usertype` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `margin` decimal(8,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `tbl_userlevel`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `tbl_userlevel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

  ALTER TABLE `tbl_userlevel` ADD `ul_added_on` DATETIME NOT NULL AFTER `margin`;
ALTER TABLE `tbl_userlevel` ADD `last_updated_on` DATETIME NOT NULL AFTER `ul_added_on`;
--16-08-2018
  ALTER TABLE `tbl_order_stockist` ADD `assign_to_userid` INT NOT NULL AFTER `ordered_by_userid`;

--13-05-2019
  ALTER TABLE `tbl_customer_orders_new` ADD `pro_mnf_date` DATETIME NOT NULL COMMENT 'product manufacture date added during ordere placed or delivered or assigned time ' AFTER `delivery_date`;

--14-05-2019
  ALTER TABLE `tbl_order_details` ADD `sp_signature_image` VARCHAR(250) NOT NULL AFTER `signature_image`, ADD `comment` BLOB NOT NULL AFTER `sp_signature_image`;
--15-05-2019
  ALTER TABLE `tbl_order_details` ADD `pro_mnf_date` DATETIME NOT NULL COMMENT ' product manufacture date added during ordere placed or delivered or assigned time' AFTER `delivery_date`;
--17-05-2019
  ALTER TABLE `tbl_order_details` ADD `payment_status` VARCHAR(50) NOT NULL AFTER `payment_date`;


--29-05-2019
  INSERT INTO `tbl_userrole` (`id`, `user_role`, `userrole_margin`) VALUES ('9', 'SalesHead', '0.00');
 