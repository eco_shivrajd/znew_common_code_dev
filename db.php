<?php
include("includes/config.php");
class Connection{	
	private $host = DB_HOST;
	public $database ;
	private $username = DB_USERNAME;
	private $password = DB_PASSWORD;
	
	private $link;
	private $result;
	public $sql;
	function __construct($database){
		if (!empty($database)){ $this->database = $database; }
		$this->link = mysql_connect($this->host,$this->username,$this->password);
		mysql_select_db($this->database, $this->link);
		return $this->link;  // returns false if connection could not be made.
	}
	function query($sql){
		if (!empty($sql)){
			$this->sql = $sql;
			$this->result = mysql_query($sql);
			return $this->result;
		}else{
			return false;
		}
	}
	function __destruct(){
		//mysql_close($this->link);
	}
	function close(){
		mysql_close($this->link);
	}
}
?>