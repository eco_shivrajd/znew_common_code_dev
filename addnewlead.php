<?php
include 'db.php';
$client_id =$_POST['client_id'];
$con = new Connection($DB_NAMES_ARRAY[$client_id]);
include 'wsJSON.php';
$JSONVar = new wsJSON($con);
$lead['salespersonid']	= $_POST['userid'];
$lead['store_name']		= fnEncodeString($_POST['store_name']);
$lead['owner_name']		= fnEncodeString($_POST['owner_name']);
$lead['contact_no']		= $_POST['contact_no'];
$lead['store_Address']	= fnEncodeString($_POST['store_Address']);
$lead['lat']			= $_POST['lat'];
$lead['lng']			= $_POST['lon'];
$lead['city']			= $_POST['city'];
$lead['state']			= $_POST['state'];

$lead['distributorid']	= $_POST['distributorid'];
$lead['suburbid']		= $_POST['suburbid'];
$lead['subarea_id']		= $_POST['subareaid'];

//new fields added
$lead['billing_name']	= $_POST['billing_name'];
$lead['bank_acc_name']		= $_POST['bank_acc_name'];
$lead['bank_acc_no']		= $_POST['bank_acc_no'];
$lead['bank_name']	= $_POST['bank_name'];
$lead['bank_b_name']		= $_POST['bank_b_name'];
$lead['bank_ifsc']		= $_POST['bank_ifsc'];
$lead['gst_number'] = $_POST['gst_number'];
$lead['fssai_number'] = $_POST['fssai_number'];

$shop_type_id = $_POST['shop_type_id'];

$lead['reminder']  = $_POST['reminder'];
$lead['visited_date']  = $_POST['visited_date'];
$lead['lead_status']  = $_POST['lead_status'];
$lead['notification']  = $_POST['notification'];
$lead['parent_ids'] 	= $_POST['parent_ids'];

$lead['service_by_usertype_id']  = $_POST['service_by_usertype_id'];
$lead['service_by_user_id'] 	= $_POST['service_by_user_id'];
$lead['landline_number_other'] 	= $_POST['landline_number_other'];


$jsonOutput = $JSONVar->fnAddNewLead($lead);
echo $jsonOutput;
