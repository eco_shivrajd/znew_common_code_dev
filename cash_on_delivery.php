<?php
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);

include 'db.php';
$client_id =$_POST['client_id'];
$con = new Connection($DB_NAMES_ARRAY[$client_id]);

$json_string =$_POST['delivery_json'];
$array = json_decode($json_string);

$temp = explode(".", $_FILES["signature_image"]["name"]);
$newfilename = round(microtime(true)) . '.' . end($temp);

$target_file="uploads/cod/".$array->challan_no."/";
if(!is_dir($target_file)){
    //Directory does not exist, so lets create it.
    mkdir($target_file, 0755, true);
}
move_uploaded_file($_FILES["signature_image"]["tmp_name"], $target_file."".$newfilename);
$array->signature_image=$newfilename;

include 'wsJSON.php';
$JSONVar = new wsJSON($con);
$jsonOutput = $JSONVar->cash_on_delivery($array);
echo $jsonOutput;
