<?php
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include 'db.php';
$client_id =$_POST['client_id'];
$con = new Connection($DB_NAMES_ARRAY[$client_id]);
include 'wsJSON.php';
$JSONVar = new wsJSON($con);
$order_no  = $_POST['order_no'];
$shop_id  = $_POST['shop_id'];
$comment  = $_POST['comment'];
$payment_date=date("Y-m-d",strtotime($_POST['payment_date']));
$jsonOutput = $JSONVar->payment_delay($order_no,$shop_id,$payment_date,$comment);
echo $jsonOutput;
