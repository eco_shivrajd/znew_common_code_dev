<?php  
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include 'db.php';
$client_id =$_POST['client_id'];
//$company_name =$_POST['company_name'];
//$logo =$_POST['logo'];
$con = new Connection($DB_NAMES_ARRAY[$client_id]);

include 'wsJSON.php';
$JSONVar = new wsJSON($con);

$json_string =$_POST['orderarray'];
$array = json_decode($json_string);
$array->comment=$_POST['comment'];
$array->payment_status=$_POST['payment_status'];
//$array->payment_date=$_POST['payment_date'];
$array->payment_date=date("Y-m-d",strtotime($_POST['payment_date']));
//image Upload code start
$temp_order_image_name="";
if (isset($_FILES["signature_image"]["name"])) 
{
	$temp = explode(".", $_FILES["signature_image"]["name"]);
	$temp_order_image_name = $temp[0];
	$newfilename = round(microtime(true)) . '.' . end($temp);
	$target_file="uploads/cod/sp_signature_".$client_id."/";
	if(!is_dir($target_file)){
		//Directory does not exist, so lets create it.
		mkdir($target_file, 0755, true);
	}
	move_uploaded_file($_FILES["signature_image"]["tmp_name"], $target_file."".$newfilename);
	$array->signature_image=$newfilename;
}else{
	$array->signature_image='';
}
//image Upload code end
$order_by_id = $array->sales_person_id;

$total_products = 0;
$order_date = $array->order_date;
$lat=$array->usr_lat;
$long=$array->usr_lng;   
$total_order_cost = 0;
$total_order_gst_cost = 0;
$sale_count = 0;
$free_count = 0;
$offer_provided = 1;
//echo "string";
//exit();
//echo "<pre>";
//print_r($array);
//exit();
foreach($array->order_list as $key => $order)//order initial details shop level
{
	 $order_no = $order->order_id; 
	//exit();
	$shop_id = $order->shop_id;  
	$external_id = $order->external_id; 
	$total_items = $order->total_items;   
	//Check Order Is exist or not (Update Order) is order is exist then delete the order
    $del_record_id = $JSONVar->del_order($order_no);
    //exit();
	//$order_record_id = $db->add_order($client_id,$order_no, $order_date, $order_by_id, $lat, $long, $shop_id, $shop_order_status,$super_stockist_id,$dist_id);
	$order_record_id = $JSONVar->add_order($order_no, $order_date, $order_by_id, $lat, $long, $shop_id, $external_id);
	$shop_and_orderto_mobiles = $JSONVar->get_mobile_numbers($shop_id, $external_id);
	//echo "<pre>";print_r($shop_and_orderto_mobiles);die();
	if(isset($order->product_details))
	{
		foreach($order->product_details as $pkey => $product)//Product level details
		{	
			$order_product_details = array();
								
			$order_product_details['order_record_id'] = $order_record_id;
			$order_product_details['brand_id'] = $product->brand_id;
			$order_product_details['cat_id'] = $product->category_id;
			$order_product_details['product_id'] =  $product->product_id;
			//echo "-------";
			 $order_product_details['signature_image'] =  $array->signature_image;
             $order_product_details['payment_date'] =  $array->payment_date;
			 $order_product_details['comment'] =  $array->comment;
			 $order_product_details['payment_status'] =  $array->payment_status;
		    //	exit();
			foreach($product->varient_details as $vkey => $varient)//Product variant level details
			{			
				$order_product_details['producthsn'] 		 = $varient->product_hsn;
				$order_product_details['product_variant_id'] = $varient->product_variant_rowcnt;
				$order_product_details['product_quantity'] = $varient->quantity;
				$sale_count+=$varient->quantity;
				
				$order_product_details['product_variant_weight1'] = $varient->weightquantity;
				$order_product_details['product_variant_unit1'] = $varient->unit;
				$order_product_details['product_variant_weight2'] = $varient->product_variant_weight2;//new param for pcs like variant
				$order_product_details['product_variant_unit2'] = $varient->product_variant_unit2;//new param for pcs like variant
				$order_product_details['product_unit_cost'] = ($varient->price);/// $varient->quantity);
				
				$total_final_price = 0;
				$discount_apply = 1;
				$cgst_value = 0;
				$sgst_value = 0;
				$total_gst = 0;
				$free_product_details='';
				if($varient->campaign_applied == 'yes' && $varient->campaign_type == 'discount'){//Product Campaign level details					
					$order_product_details['campaign_applied'] = 0;
					$order_product_details['campaign_type'] = 'discount';
					$order_product_details['campaign_sale_type'] = 'discount';
					
					$discounted_amount = $varient->price - $varient->campaign_discount;
					$total_final_price =($discounted_amount * $varient->quantity);	
					$discount_apply = 0;
				}else if($varient->campaign_applied == 'yes' && $varient->campaign_type == 'free_product'){//Product Campaign level details					
					$order_product_details['campaign_applied'] = 0;
					$order_product_details['campaign_type'] = 'free_product';
					$order_product_details['campaign_sale_type'] = 'free_product';
					
					foreach($varient->campaign_free_product_details as $fkey => $fvarient) {
						if($fvarient->quantity>0){
							$free_product_details.="".$fvarient->brand_id.",".$fvarient->brand_name.","
							.$fvarient->category_id.",".$fvarient->category_name
							.",".$fvarient->product_id.",".$fvarient->product_name
							.",".$fvarient->product_variant_rowcnt.","
							.$fvarient->weight.",".$fvarient->quantity."";
						}
					}
					//free_product_details
					$order_product_details['free_product_details']=$free_product_details;
					$total_final_price = $varient->price * $varient->quantity;
				}else{
					$total_final_price = $varient->price * $varient->quantity;
				}
				$order_product_details['product_total_cost'] = $varient->price * $varient->quantity;//$total_final_price
				$order_product_details['product_cgst'] = $varient->cgst;//% value
				$order_product_details['product_sgst'] = $varient->sgst;//% value
				if($varient->cgst != 0){
					$cgst_value = (($total_final_price * $varient->cgst)/100);
				}
				
				if($varient->sgst != 0){
					$sgst_value = (($total_final_price * $varient->sgst)/100);
				}
				$total_gst = $total_final_price;
				if($cgst_value != 0)
					$total_gst = $total_gst + $cgst_value;
				if($sgst_value != 0)
					$total_gst = $total_gst + $sgst_value;
								
								
				$order_product_details['p_cost_cgst_sgst'] = $total_gst;
				$order_product_details['order_no'] = $order_no;
				$order_product_details['temp_order_image_name'] = $temp_order_image_name;
				$total_order_cost=$total_order_cost+($varient->price * $varient->quantity);//$total_final_price
				$total_order_gst_cost=$total_order_gst_cost+$total_gst;
				$order_product_details['order_status'] = 1;//order received
				$order_product_details['client_id'] = $client_id;
				//$order_variant_id = $db->add_order_details($order_product_details);
				$order_variant_id = $JSONVar->add_order_details($order_product_details);
				if($discount_apply == 0){
					//$db->add_order_c_p_discount($client_id,$varient->campaign_id,$order_variant_id,$varient->campaign_discount,$varient->campaign_percent,$varient->price);
					$JSONVar->add_order_c_p_discount($varient->campaign_id,$order_variant_id,$varient->campaign_discount,$varient->campaign_percent,$varient->price);
					$offer_provided = 0;
				}
			}
		}
		//$db->update_order($client_id,$order_record_id, $sale_count, $total_order_cost, $total_order_gst_cost,$offer_provided);
		$JSONVar->update_order($order_record_id, $sale_count, $total_order_cost, $total_order_gst_cost,$offer_provided);
	}
	
	 $ch = curl_init('https://salzpoint.net/uatnew-dev/kaysons/templates/admin/invoicedemo2_final.php?orderid='.$order_no.'&client_id='.$client_id.'');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
	$err = curl_error($ch);
	curl_close($ch);
	foreach($shop_and_orderto_mobiles as $key_mobile=>$value_mobile){
		//var_dump($value_mobile);echo "ghdjfgj-".strlen($value_mobile);
		if($value_mobile!='' && strlen($value_mobile)==10 ){
			$filename = "https://salzpoint.net/uatnew-dev/kaysons/templates/admin/uploadpdf/".$order_no.".pdf";
			$ch1 = curl_init($filename);    
			curl_setopt($ch1, CURLOPT_NOBODY, true);
			curl_exec($ch1);
			$code = curl_getinfo($ch1, CURLINFO_HTTP_CODE);
			curl_close($ch1);
			if($code == 200){
				//send whatsapp msg
				/* $curl = curl_init();
				$pdf_addrees="https://panel.apiwha.com/send_message.php?apikey=4VF5WQPL52IKQPAUVBYQ&number=918668632913&text=https://salzpoint.net/uatnew-dev/kaysons/templates/admin/uploadpdf/".$order_no.".pdf";
				curl_setopt_array($curl, array(
				  CURLOPT_URL => $pdf_addrees,
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "GET",
				));
				$response = curl_exec($curl);
				$err = curl_error($curl);
				curl_close($curl); */
			}
		}
	} 
	
}

$total_count = $sale_count + $free_count;
$jsonOutput = $JSONVar->fnplaceorder($total_order_cost,$total_order_gst_cost,$sale_count,$free_count,$total_count);
echo $jsonOutput;