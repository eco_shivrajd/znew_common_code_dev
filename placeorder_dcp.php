<?php  
ini_set('display_errors', 1); 
include 'db.php';
$client_id =$_POST['client_id'];
$con = new Connection($DB_NAMES_ARRAY[$client_id]);

include 'wsJSON.php';
$JSONVar = new wsJSON($con);

$json_string =$_POST['orderarray'];
$array = json_decode($json_string);


$order_by_id = $array->dcp_user_id;
$order_date = $array->order_date;
$lat=$array->usr_lat;
$long=$array->usr_lng;   

foreach($array->order_list as $key => $order)//order initial details shop level
{
    $order_no = $order->order_id;
    $total_items = $order->total_items; 
    $cartons_id = $order->cartons_id;
    if(isset($order->product_details))
    {
        foreach($order->product_details as $pkey => $product)//Product level details
        {	
            $order_product_details = array();	
            $order_product_details['dcp_id'] = $order_by_id;
            $order_product_details['cartons_id'] = $cartons_id;
            $order_product_details['order_no'] = $order_no;
            $order_product_details['product_id'] =  $product->product_id;
                
            $order_product_details['usr_lat'] = $lat;
            $order_product_details['usr_lng'] = $long;

            foreach($product->varient_details as $vkey => $varient)//Product variant level details
            {	
                $order_product_details['prod_var_id'] = $varient->product_variant_rowcnt;
                $order_product_details['unit_price'] = ($varient->price);	
                $order_product_details['sale_qnty'] = $varient->quantity;
                $sale_count+=$varient->quantity; 

                $order_product_details['product_variant_weight1'] = $varient->weightquantity;
                $order_product_details['product_variant_unit1'] = $varient->unit;
                $order_product_details['product_variant_weight2'] = $varient->weightquantity2;//new param for pcs like variant
                $order_product_details['product_variant_unit2'] = $varient->unit2;//new param for pcs like variant
                $order_product_details['product_unit_cost'] = ($varient->price);/// $varient->quantity);
                
                $total_final_price = 0;
				$discount_apply = 1;
				$cgst_value = 0;
				$sgst_value = 0;
				$total_gst = 0;
				$free_product_details='';
				if($varient->campaign_applied == 'yes' && $varient->campaign_type == 'discount'){//Product Campaign level details					
					$order_product_details['campaign_applied'] = 0;
					$order_product_details['campaign_type'] = 'discount';
					$order_product_details['campaign_sale_type'] = 'discount';
					
					$discounted_amount = $varient->price - $varient->campaign_discount;
					$total_final_price =($discounted_amount * $varient->quantity);	
					$discount_apply = 0;
				}else if($varient->campaign_applied == 'yes' && $varient->campaign_type == 'free_product'){//Product Campaign level details					
					$order_product_details['campaign_applied'] = 0;
					$order_product_details['campaign_type'] = 'free_product';
					$order_product_details['campaign_sale_type'] = 'free_product';
					
					foreach($varient->campaign_free_product_details as $fkey => $fvarient) {
						$free_product_details.="".$fvarient->brand_id.",".$fvarient->brand_name.","
						.$fvarient->category_id.",".$fvarient->category_name
						.",".$fvarient->product_id.",".$fvarient->product_name
						.",".$fvarient->product_variant_rowcnt.","
						.$fvarient->weight.",".$fvarient->quantity."";
					}
					//free_product_details
					$order_product_details['free_product_details']=$free_product_details;
					$total_final_price = $varient->price * $varient->quantity;
				}else{
					$total_final_price = $varient->price * $varient->quantity;
				}
				$order_product_details['product_total_cost'] = $total_final_price;
				$order_product_details['product_cgst'] = $varient->cgst;//% value
				$order_product_details['product_sgst'] = $varient->sgst;//% value
				if($varient->cgst != 0){
					$cgst_value = (($total_final_price * $varient->cgst)/100);
				}
				
				if($varient->sgst != 0){
					$sgst_value = (($total_final_price * $varient->sgst)/100);
				}
				$total_gst = $total_final_price;
				if($cgst_value != 0)
					$total_gst = $total_gst + $cgst_value;
				if($sgst_value != 0)
					$total_gst = $total_gst + $sgst_value;
								
								
				$order_product_details['p_cost_cgst_sgst'] = $total_gst;
				$total_order_cost=$total_order_cost+$total_final_price;
				$total_order_gst_cost=$total_order_gst_cost+$total_gst;
				$order_product_details['order_status'] = 1;//order received
				
                $order_product_details['product_total_cost'] = $total_final_price;
				$order_product_details['client_id'] = $client_id;
               
                $order_variant_id = $JSONVar->update_order_dcp($order_product_details);
                //$order_variant_id = $db->add_order_dcp($order_product_details);
            }
        }
    }
}
$free_count=0;
$total_count = $sale_count + $free_count;
$jsonOutput = $JSONVar->fnplaceorder($total_order_cost,'0',$sale_count,'0',$total_count);
echo $jsonOutput;