<?php
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include 'db.php';
$client_id =$_POST['client_id'];
$con = new Connection($DB_NAMES_ARRAY[$client_id]);
include 'wsJSON.php';
$JSONVar = new wsJSON($con);
$salespersonid	= $_POST['userid'];
$store_name		= fnEncodeString($_POST['store_name']);
$owner_name		= fnEncodeString($_POST['owner_name']);
$contact_no		= $_POST['contact_no'];
$store_Address	= fnEncodeString($_POST['store_Address']);
$lat			= $_POST['lat'];
$lng			= $_POST['lon'];
$city			= $_POST['city'];
$state			= $_POST['state'];

$parent_ids	= $_POST['parent_ids'];
$suburbid		= $_POST['suburbid'];
$subarea_id		= $_POST['subareaid'];

//new fields added

$billing_name	= $_POST['billing_name'];
$bank_acc_name		= $_POST['bank_acc_name'];
$bank_acc_no		= $_POST['bank_acc_no'];
$bank_name	= $_POST['bank_name'];
$bank_b_name		= $_POST['bank_b_name'];
$bank_ifsc		= $_POST['bank_ifsc'];
$gst_number = $_POST['gst_number'];
$fssai_number = $_POST['fssai_number'];
$shop_type_id = $_POST['shop_type_id'];

$service_by_usertype_id = $_POST['service_by_usertype_id'];
$service_by_user_id = $_POST['service_by_user_id'];
$landline_number_other = $_POST['landline_number_other'];

$jsonOutput 	= $JSONVar->fnAddNewStore($salespersonid,
											$store_name,$owner_name,$contact_no,
											$store_Address,$lat,$lng,$city,$state,
											$parent_ids,$suburbid,$subarea_id,
											$billing_name,$bank_acc_name,$bank_acc_no,
											$bank_name,$bank_b_name,$bank_ifsc,$gst_number,$fssai_number,
											$service_by_usertype_id,$service_by_user_id,$landline_number_other,
											$shop_type_id);
echo $jsonOutput;
