<?php

//error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 0);

class wsJSON {

    private $connection;
    private $json_array = array("status" => array("responsecode" => "1", "entity" => "1"),
        "data" => array());

    function __construct($con) {
        $this->connection = $con;
    }

    function executeQuery($query) {
        $result = $this->connection->query($query) or die(mysql_error());
        return $result;
    }

    function addslashes_to_string($str) {
        $tempStr = addslashes(trim($str));
        return str_replace("\'", "", $tempStr);
        // return str_replace("\r\n","",str_replace("\'","'",$tempStr));
        //return str_replace("\r\n","",str_replace("\'","'",$tempStr));
    }

    function GetLatitudeLongitude($geoaddress) {
        $geoaddress = urlencode($geoaddress);
        $geoaddress = str_replace("#", "", $geoaddress);
        $geoaddress = str_replace(" ", "+", $geoaddress);
        $geoaddress = str_replace("++", "+", $geoaddress);
        //echo "http://maps.googleapis.com/maps/api/geocode/json?address=".$geoaddress."&sensor=true";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://maps.googleapis.com/maps/api/geocode/json?address=" . $geoaddress . "&sensor=true");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $geocode = curl_exec($ch);
        $output2 = json_decode($geocode);

        curl_close($ch);
        $latitude = $output2->results[0]->geometry->location->lat;
        $longitude = $output2->results[0]->geometry->location->lng;

        $coordinate = array($latitude, $longitude);
        return $coordinate;
    }

    function fnSendInvitation($senderid, $receiverid) {
        //check invitation already exists:
        // $sql = "SELECT *  FROM tbl_invitation WHERE (senderid='$senderid' AND receiverid='$receiverid') OR (senderid='$receiverid' AND receiverid='$senderid')AND status!='4'";
        $sqluser = "SELECT *  FROM users WHERE uid='" . $receiverid . "'";
        $proRowuser = $this->executeQuery($sqluser);
        $rowuser = mysql_fetch_array($proRowuser);
        //contactssource
        $sql = "SELECT * from contactssource WHERE phone='" . $rowuser['phone'] . "' AND feloze_uid='" . $senderid . "'";
        $result = $this->executeQuery($sql);
        $rowcount = mysql_num_rows($result);
        if ($rowcount > 0) {
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['invitation']['msg'] = "Contact already exist in contacts.";
        } else {
            $sql = "SELECT *  FROM tbl_invitation WHERE senderid='$senderid' AND receiverid='$receiverid' AND status!='4'";
            $proRow = $this->executeQuery($sql);
            if (mysql_num_rows($proRow) != 0) {
                $this->json_array['status']['responsecode'] = '1';
                $this->json_array['status']['entity'] = '1';
                $this->json_array['data']['invitation']['msg'] = "Invitation already sent.";
            } else {
                $sql = "INSERT INTO tbl_invitation(senderid,receiverid)VALUES('$senderid', '$receiverid') ";
                $proRow = $this->executeQuery($sql);
                $user_id = mysql_insert_id();
                if ($user_id) {
                    $this->json_array['status']['responsecode'] = '0';
                    $this->json_array['status']['entity'] = '1';
                    $this->json_array['data']['invitation']['msg'] = "Invitation sent successfully.";
                } else {
                    $this->json_array['status']['responsecode'] = '0';
                    $this->json_array['status']['entity'] = '1';
                    $this->json_array['data']['invitation']['msg'] = "Unsuccessful.";
                }
            }
        }
        return json_encode($this->json_array);
    }

    function fnplaceorder() {
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '1';
        $this->json_array['data']['invitation']['msg'] = "Order Placed Successfully.";
        return json_encode($this->json_array);
    }

    function get_login_bk($uname, $password) {
        $password = md5($password);
        $sql = "SELECT *  FROM `tbl_user` WHERE username='$uname' AND pwd='$password'";
        $proRow = $this->executeQuery($sql);
        if (mysql_num_rows($proRow) != 0) {
            $row = mysql_fetch_array($proRow);
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '6';
            $this->json_array['data']['Login']['id'] = $row['id'];
            $this->json_array['data']['Login']['username'] = $row['username'];
            $this->json_array['data']['Login']['firstname'] = $row['firstname'];
            $this->json_array['data']['Login']['surname'] = $row['surname'];
            $this->json_array['data']['Login']['address'] = $row['address'];
            $this->json_array['data']['Login']['email'] = $row['email'];
        } else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['Login']['message'] = "Invalid username or password.";
        }
        return json_encode($this->json_array);
    }
    
    function getprocategory_clientid($id,$cust_lat,$cust_long) {
        $getpropath = "";
         $sql = "SELECT id,wsbasepath FROM tbl_clients_maintenance WHERE id='" . $id . "'";
        $proRow = $this->executeQuery($sql);
        $row = mysql_fetch_array($proRow);
        //echo $cust_lat."---".$cust_long;
        if (mysql_num_rows($proRow) != 0) {
            $getpropath .= $row['wsbasepath'];
            $getpropath .= "/getprocategory_customer.php?cust_lat='$cust_lat'&cust_long='$cust_long'";
           // echo "<pre>";print_r($getpropath);
            $curlSession = curl_init();
            curl_setopt($curlSession, CURLOPT_URL, $getpropath);
            curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);

            $jsonData = json_decode(curl_exec($curlSession)); 
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '9';
            $this->json_array['data'] = $jsonData;
        } else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data'][] = [];
        }
        //echo $getpropath;
        return json_encode($this->json_array);
    }

//b2c
    function get_all_client() {
        $getpropath = "";
         $sql = "SELECT `id`, `clientnm`, `wsbasepath` FROM `tbl_clients_maintenance` WHERE (id >= '30' and id < '35') or id=28 ";
        $proRow = $this->executeQuery($sql);
        $clientarray = array();
        if (mysql_num_rows($proRow) != 0) {
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '6';
            while ($row = mysql_fetch_array($proRow)) {
                $clientarray['id'] = $row['id'];
                $clientarray['clientnm'] = $row['clientnm'];
                $clientarray['wsbasepath'] = $row['wsbasepath'];
                $this->json_array['data'][] = $clientarray;
            }
        } else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data'] = "No client found";
        }
        return json_encode($this->json_array);
    }
    function get_login($uname, $password) {
        $password = md5(addslashes($password));
        $uname = addslashes($uname);
          $sql = "SELECT uid,level,company_id,user_level 
          FROM `tbl_users` 
          WHERE username='$uname' AND passwd='$password' AND isdeleted!='1'
          AND (level='SalesPerson' OR level='DeliveryPerson' OR level='SalesHead' 
          OR level='Shopkeeper')";
        // OR level='DeliveryChannelPerson' 
        $proRow = $this->executeQuery($sql);
        if (mysql_num_rows($proRow) != 0) {
            $row = mysql_fetch_array($proRow);
            $sqlcom = "SELECT tbl_clients_maintenance.*  FROM tbl_clients_maintenance WHERE id='" . $row['company_id'] . "' ";
            $proRowcom = $this->executeQuery($sqlcom);
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '6';
            $this->json_array['data']['id'] = $row['uid'];
             $this->json_array['data']['userrole'] = $row['level'];
            $this->json_array['data']['usertype'] = $row['user_level'];

            $this->json_array['data']['username'] = $uname;
            while ($rowcom = mysql_fetch_array($proRowcom)) {
                $comments_array_temp['clientnm'] = $rowcom['clientnm'];
                $comments_array_temp['logo'] = $rowcom['logo'];
                $comments_array_temp['color'] = $rowcom['color'];
                $comments_array_temp['wsbasepath'] = $rowcom['wsbasepath'];
                $comments_array_temp['companyid'] = $rowcom['id'];
                 $comments_array_temp['parent_ids'] = 'dsfs';
                $comments_array_temp['parent_names'] = 'dfsdf';
                $this->json_array['data']['Login'][] = $comments_array_temp;
            }
        } else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
         $this->json_array['data']['Login']['message'] = "Invalid username or password.";
        }
        return json_encode($this->json_array);
    }
    /*
    function get_login($uname, $password) {
        $password = md5(addslashes($password));
        $uname = addslashes($uname);
          $sql = "SELECT uid,level,company_id,user_level,is_logged_in 
          FROM `tbl_users` 
          WHERE username='$uname' AND passwd='$password' 
          AND (level='SalesPerson' OR level='DeliveryPerson' OR level='DeliveryChannelPerson' 
          OR level='Shopkeeper')";
        // 
        $proRow = $this->executeQuery($sql);
        if (mysql_num_rows($proRow) != 0) {         
            $row = mysql_fetch_array($proRow);
            if($row['is_logged_in']=='0'){
                $sqlcom = "SELECT tbl_clients_maintenance.*  FROM tbl_clients_maintenance WHERE id='" . $row['company_id'] . "' ";
                $proRowcom = $this->executeQuery($sqlcom);
                $this->json_array['status']['responsecode'] = '0';
                $this->json_array['status']['entity'] = '6';
                $this->json_array['data']['id'] = $row['uid'];
                 $this->json_array['data']['userrole'] = $row['level'];
                $this->json_array['data']['usertype'] = $row['user_level'];

                $this->json_array['data']['username'] = $uname;
                while ($rowcom = mysql_fetch_array($proRowcom)) {
                    $comments_array_temp['clientnm'] = $rowcom['clientnm'];
                    $comments_array_temp['logo'] = $rowcom['logo'];
                    $comments_array_temp['color'] = $rowcom['color'];
                    $comments_array_temp['wsbasepath'] = $rowcom['wsbasepath'];
                    $comments_array_temp['companyid'] = $rowcom['id'];
                     $comments_array_temp['parent_ids'] = 'dsfs';
                    $comments_array_temp['parent_names'] = 'dfsdf';
                    $this->json_array['data']['Login'][] = $comments_array_temp;
                }
                $sql_update = "UPDATE  tbl_users set is_logged_in='1' WHERE uid='".$row['uid']."' ";
                $proRowcom_update = $this->executeQuery($sql_update);
            }            
            else {              
                $this->json_array['status']['responsecode'] = '1';
                $this->json_array['status']['entity'] = '1';
                $this->json_array['data']['Login']['message'] = "Already logged in.";
            }
        } else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['Login']['message'] = "Invalid username or password.";
        }
        return json_encode($this->json_array);
    }*/

    function fnGetProductCat() {
        $sql = "SELECT * FROM pcatbrandvariant";
        $proRow = $this->executeQuery($sql);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '1';
        while ($row = mysql_fetch_array($proRow)) {
            $p_array_temp['brand_name'] = $row['brand_name'];
            $p_array_temp['category_name'] = $row['category_name'];
            if ($row['category_image'] != '') {
                $p_array_temp['category_image'] = "http://devphp.ecotechservices.com/live/saras/templates/admin/upload/" . $row['category_image'];
            } else {
                $p_array_temp['category_image'] = "";
            }
            $p_array_temp['product_name'] = $row['product_name'];
            $p_array_temp['product_price'] = $row['product_price'];
            $p_array_temp['brand_id'] = $row['brand_id'];
            $p_array_temp['category_id'] = $row['category_id'];
            $p_array_temp['product_id'] = $row['product_id']; //variant row
            $p_array_temp['product_variant_rowcnt'] = $row['variant_cnt'];
            $variant1arr = explode(",", $row['product_variant1']);
            $sizeunit = $this->fnGetUnit($variant1arr[1]);
            $p_array_temp['size'] = $variant1arr[0];
            $p_array_temp['sizeunit'] = $sizeunit;

            $variant2arr = explode(",", $row['product_variant2']);
            $weightunit = $this->fnGetUnit($variant2arr[1]);
            $p_array_temp['weight'] = $variant2arr[0];
            $p_array_temp['weightunit'] = $weightunit;

            $this->json_array['data']['procats'][] = $p_array_temp;
        }
        return json_encode($this->json_array);
    }

    function fnGetUnit($id) {
        $sql = "SELECT unitname FROM tbl_units WHERE id='" . $id . "'";
        $proRow = $this->executeQuery($sql);
        $row = mysql_fetch_array($proRow);
        return $row['unitname'];
    }
    function fnGet_seperate_user_id($id) {
        $sql = "SELECT tbl_user.id,tuv.parent_ids,tuv.parent_names ,tuv.external_id
        from tbl_user
        left join tbl_user_view tuv on tbl_user.id=tuv.id
        where common_user_id='" . $id . "'";
        $proRow = $this->executeQuery($sql);
        $row = mysql_fetch_assoc($proRow);
        return $row;
    }

    function fnProfileUpdate($profileid, $name, $address, $state, $city, $email, $mobile) {
        $sql = "UPDATE tbl_user SET firstname='$name',address='$address',state='$state',city='$city',email='$email',mobile='$mobile' where id='$profileid'";
        $proRow = $this->executeQuery($sql);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '7';
        $this->json_array['data']['Profile']['id'] = $profileid;
        $this->json_array['data']['Profile']['firstname'] = $name;
        $this->json_array['data']['Profile']['address'] = $address;
        $this->json_array['data']['Profile']['state'] = $state;
        $this->json_array['data']['Profile']['city'] = $city;
        $this->json_array['data']['Profile']['email'] = $email;
        $this->json_array['data']['Profile']['mobile'] = $mobile;
        return json_encode($this->json_array);
    }
    function get_statecity() {
        $sql = "SELECT id, name FROM tbl_state";
        $proRow = $this->executeQuery($sql);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '9';
        while ($row = mysql_fetch_array($proRow)) {
            $s_array_temp['name'] = $row['name'];
            $s_array_temp['id'] = $row['id'];
            $this->json_array['data']['state'][] = $s_array_temp;
        }
        $sqlcity = "SELECT id, name,state_id FROM  tbl_city";
        $proRowcity = $this->executeQuery($sqlcity);
        while ($rowcity = mysql_fetch_array($proRowcity)) {
            $c_array_temp['name'] = $rowcity['name'];
            $c_array_temp['id'] = $rowcity['id'];
            $c_array_temp['state_id'] = $rowcity['state_id'];
            $this->json_array['data']['city'][] = $c_array_temp;
        }
        return json_encode($this->json_array);
    }

    function fnAddNewStore($salespersonid, $store_name, $owner_name, $contact_no, $store_Address, $lat, $lng, $city, $state) {
        $sql = "INSERT IN TO tbl_shops(name, address, city, state, contact_person, mobile, shop_added_by,lat,lon)VALUES('" . $store_name . "','" . $store_Address . "','" . $city . "','" . $state . "','" . $owner_name . "','" . $contact_no . "','" . $salespersonid . "','" . $lat . "','" . $lon . "')";
        $proRow = $this->executeQuery($sql);
        $last_id = mysql_insert_id();
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '1';
        $this->json_array['data']['Store']['msg'] = "Store added successfully.";
        $this->json_array['data']['Store']['id'] = $last_id;
        return json_encode($this->json_array);
    }

    function get_shoplist() {
        $sql = "SELECT id, name, address, city, state, contact_person, mobile, shop_added_by, lat, lon FROM tbl_shops";
        $proRow = $this->executeQuery($sql);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '9';
        while ($row = mysql_fetch_array($proRow)) {
            $shop_array_temp['id'] = $row['id'];
            $shop_array_temp['name'] = $row['name'];
            $shop_array_temp['address'] = $row['address'];
            $shop_array_temp['contact_person'] = $uname;
            $shop_array_temp['lat'] = $row['lat'];
            $shop_array_temp['lon'] = $row['lon'];
            $shop_array_temp['mobile'] = $bday;
            $shop_array_temp['city'] = $this->fngetCitynm($row['city']);
            $shop_array_temp['state'] = $this->fngetStatenm($row['state']);
            $this->json_array['data']['shops'][] = $shop_array_temp;
        }
        return json_encode($this->json_array);
    }

    function fngetStatenm($id) {
        $sql = "SELECT `id`,`name` FROM tbl_state WHERE id='" . $id . "'";
        $proRow = $this->executeQuery($sql);
        $row = mysql_fetch_array($proRow);
        return $row['name'];
    }

    function fngetCitynm($id) {
        $sql = "SELECT `id`,`name` FROM tbl_city WHERE id='" . $id . "'";
        $proRow = $this->executeQuery($sql);
        $row = mysql_fetch_array($proRow);
        return $row['name'];
    }

    function fnForgotPass($uername) {
        if ($uername != "") {//now this is username
            $sql = "SELECT tu.*,tcm.clientnm,tcm.email as comp_email FROM tbl_users tu
            left join tbl_clients_maintenance tcm on tu.company_id=tcm.id 
            WHERE username='$uername'"; 
            
            $proRow = $this->executeQuery($sql);
            $res = mysql_num_rows($proRow);
            if ($res == "0") {
                $json_array['status']['responsecode'] = '1';
                $json_array['status']['entity'] = '1';
                $json_array['data']['PasswordResult']['message'] = "Username not exists";
                return json_encode($json_array);
            } else {
                $resarr = mysql_fetch_assoc($proRow);
                if ($resarr['emailaddress'] != '') {
                    $newPass = rand(999, 9999);
                    $newPassMD5 = md5($newPass);                    
                    $to  = $resarr['emailaddress'];
                    $client_id=$resarr['company_id'];
                    $comp_email=$resarr['comp_email'];  
                    $clientnm=$resarr['clientnm'];  
                    $fromMail  = $comp_email;                   
                    $semi_rand = md5(time());
                    $headers = "";
                    $headers .= "From: " . $fromMail . "\r\n";
                    $headers .= "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-Type: text/html;" . "\r\n";
                    $message .= "Hello " . $resarr['username'] . ",<br/><br/>";
                    $message .= "We have recently received a password request for your email address. Below are your login details: <br/>";
                    $message .= "Username: " . $resarr['username'] . "<br/>";
                    $message .= "New password: " . $newPass . "<br/>";
                    $message .= "<br/>Thanks, <br/>";
                    $message .=  $clientnm. "  Admin";
                    $subject   = "Reset Password";
                    $to = $resarr['emailaddress'];
                    $sent = @mail($to, $subject, $message, $headers);


                    $sql1 = "UPDATE  tbl_users set `passwd`='$newPassMD5' WHERE `username`='$uername'";

                    $result1 = $this->executeQuery($sql1);

                    $json_array['status']['responsecode'] = '0';
                    $json_array['status']['entity'] = '0';
                    $json_array['data']['PasswordResult']['message'] = "Password sent on your email Id.";
                    return json_encode($json_array);
                } else {
                    $json_array['status']['responsecode'] = '1';
                    $json_array['status']['entity'] = '1';
                    $json_array['data']['PasswordResult']['message'] = "Please contact to Admin";
                    return json_encode($json_array);
                }
            }
        } else {
            $json_array['status']['responsecode'] = '1';
            $json_array['status']['entity'] = '1';
            $json_array['data']['PasswordResult']['message'] = "No email address sent.";
            echo json_encode($json_array);
        }
    }
    //b2c app customer forgotpassword
    function fnForgotPass_customer($email) {
        if ($email != "") {//now this is username
            $sql = "SELECT * FROM `tbl_customer_new` WHERE customer_emailid='$email'";
            $proRow = $this->executeQuery($sql);
            $res = mysql_num_rows($proRow);
            if ($res == "0") {
                $json_array['status']['responsecode'] = '1';
                $json_array['status']['entity'] = '1';
                $json_array['data']['PasswordResult']['message'] = "Username not exists";
                return json_encode($json_array);
            } else {
                $resarr = mysql_fetch_assoc($proRow);
                if ($resarr['customer_emailid'] != '') {
                    $newPass = rand(999, 9999);
                    $newPassMD5 = md5($newPass);

                    $fromMail = "supply@salzpoint.net"; // sender
                    
                  
                    $semi_rand = md5(time());
                    $headers = "";
                    $headers .= "From: " . $fromMail . "\r\n";
                    $headers .= "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-Type: text/html;" . "\r\n";
                    $message .= "Hello " . $resarr['username'] . ",<br/><br/>";
                    $message .= "We have recently received a password request for your email address. Below are your login details: <br/>";
                    $message .= "Username: " . $resarr['username'] . "<br/>";
                    $message .= "New password: " . $newPass . "<br/>";
                    $message .= "<br/>Thanking yours, <br/>";
                    $message .= "Salzpoint  Admin";
                    $subject = "Password reset";
                    $to = $resarr['customer_emailid'];
                    $sent = @mail($to, $subject, $message, $headers);

                   
                    $sql1 = "UPDATE  tbl_customer_new set `password`='$newPassMD5' WHERE `customer_emailid`='$email'";

                    $result1 = $this->executeQuery($sql1);

                    $json_array['status']['responsecode'] = '0';
                    $json_array['status']['entity'] = '0';
                    $json_array['data']['PasswordResult']['message'] = "Password sent on your email Id.";
                    return json_encode($json_array);
                } else {
                    $json_array['status']['responsecode'] = '1';
                    $json_array['status']['entity'] = '1';
                    $json_array['data']['PasswordResult']['message'] = "Please contact to Admin";
                    return json_encode($json_array);
                }
            }
        } else {
            $json_array['status']['responsecode'] = '1';
            $json_array['status']['entity'] = '1';
            $json_array['data']['PasswordResult']['message'] = "No email address sent.";
            echo json_encode($json_array);
        }
    }

    function save_customer($data) {
        //state,city,area,subarea,addr_lat,addr_long,
        //echo "fskjd";
        $username = $data['username'];
        $customer_emailid = $data['customer_emailid'];
        $sql_point_check = "select id from `tbl_customer_new` where customer_emailid='$customer_emailid' or username='$username'  ORDER BY id DESC LIMIT 1";
        $proRowp = $this->executeQuery($sql_point_check);
        if (mysql_num_rows($proRowp) > 0) {
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['Customer']['msg'] = "Username or email already exists!";
            $this->json_array['data']['Customer']['id'] = 0;
        } else {
            $pwd = md5($data['password']);
            $sql = "INSERT INTO tbl_customer_new
                    (customer_name,customer_emailid,username,password,customer_phone_no,
                    customer_address,customer_type,customer_shipping_address,ship_addr_lat,ship_addr_long)VALUES
                    ('" . $data['customer_name'] . "','" . $customer_emailid .
                    "','" . $username . "','" . $pwd .
                    "','" . $data['customer_phone_no'] . "','" . $data['customer_address'] .
                    "','" . $data['customer_type'] . "','" . $data['customer_shipping_address'] .
                    "','" . $data['ship_addr_lat'] . "','" . $data['ship_addr_long'] . "')";
            $proRow = $this->executeQuery($sql);
            $last_id = mysql_insert_id();
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['Customer']['msg'] = "Customer added successfully.";
            $this->json_array['data']['Customer']['id'] = $last_id;
        }
        return json_encode($this->json_array);
    }

    //for only b2c
    function customer_login($uname, $password) {
        //echo "fskjd";            
        $password = md5($password);
         $sql = "SELECT *  FROM `tbl_customer_new` WHERE username='$uname' AND password='$password'";
        $proRow = $this->executeQuery($sql);
        $cust_data = array();
        if (mysql_num_rows($proRow) != 0) {
            //password,ship_addr_lat,ship_addr_long

            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '6';


            while ($row = mysql_fetch_array($proRow)) {
                $cust_data['id'] = $row['id'];
                $cust_data['username'] = $row['username'];
                $cust_data['customer_name'] = $row['customer_name'];
                $cust_data['customer_emailid'] = $row['customer_emailid'];
                $cust_data['customer_phone_no'] = $row['customer_phone_no'];
                $cust_data['customer_address'] = $row['customer_address'];
                $cust_data['customer_type'] = $row['customer_type'];

                $cust_data['customer_shipping_address'] = $row['customer_shipping_address'];
                $cust_data['state'] = $row['state'];
                $cust_data['city'] = $row['city'];
                $cust_data['area'] = $row['area'];
                $cust_data['subarea'] = $row['subarea'];
                $cust_data['addr_lat'] = $row['addr_lat'];

                $cust_data['addr_long'] = $row['addr_long'];
                $cust_data['ship_addr_lat'] = $row['ship_addr_lat'];
                $cust_data['ship_addr_long'] = $row['ship_addr_long'];
                $cust_data['added_on_date'] = $row['added_on_date'];
                 $cust_data['delivery_channel_id'] = '1';
                $cust_data['delivery_channel_name'] = 'delivery_channel_name';
            }

            $this->json_array['data']['Login'] = $cust_data;
        } else {
            $this->json_array['status']['responsecode'] = '1';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['Login']['message'] = "Invalid username or password.";
        }
        return json_encode($this->json_array);
    }

    function update_customer_data($cust_data) {        
        $customer_emailid = $cust_data['customer_emailid'];
        $sql_point_check = "select id from `tbl_customer_new` where customer_emailid='$customer_emailid'  and id!='".$cust_data['id']."' ";
        $proRowp = $this->executeQuery($sql_point_check);
        if (mysql_num_rows($proRowp) > 0) {
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '1';
            $this->json_array['data']['Customer']['msg'] = "Email already exists!";
            $this->json_array['data']['Customer']['id'] = 0;
        } else {
            
            $update_string='';            
            if($cust_data['customer_name']!=''){
                $update_string.=" , customer_name='".$cust_data['customer_name']."' ";
            } 
            if($cust_data['customer_emailid']!=''){
                $update_string.=" ,customer_emailid='".$cust_data['customer_emailid']."' ";
            }
            if($cust_data['customer_phone_no']!=''){
                $update_string.=" ,customer_name='".$cust_data['customer_phone_no']."' ";
            }
            if($cust_data['customer_address']!=''){
                $update_string.=" ,customer_address='".$cust_data['customer_address']."' ";
            }
            if($cust_data['customer_type']!=''){
                $update_string.=" ,customer_type='".$cust_data['customer_type']."' ";
            }
            if($cust_data['customer_shipping_address']!=''){
                $update_string.=" ,customer_shipping_address='".$cust_data['customer_shipping_address']."' ";
            }
            if($cust_data['state']!=''){
                $update_string.=" ,state='".$cust_data['state']."' ";
            }
            if($cust_data['city']!=''){
                $update_string.=" ,city='".$cust_data['city']."' ";
            }
            if($cust_data['area']!=''){
                $update_string.=" ,area='".$cust_data['area']."' ";
            }
            if($cust_data['subarea']!=''){
                $update_string.=" ,subarea='".$cust_data['subarea']."' ";
            }
            if($cust_data['addr_lat']!=''){
                $update_string.=" ,addr_lat='".$cust_data['addr_lat']."' ";
            }
            if($cust_data['addr_long']!=''){
                $update_string.=" ,addr_long='".$cust_data['addr_long']."' ";
            }
            if($cust_data['ship_addr_lat']!=''){
                $update_string.=" ,ship_addr_lat='".$cust_data['ship_addr_lat']."' ";
            }
            if($cust_data['ship_addr_long']!=''){
                $update_string.=" ,ship_addr_long='".$cust_data['ship_addr_long']."' ";
            }
            $sql="UPDATE tbl_customer_new SET id='".$cust_data['id']."' 
                    $update_string
                    where id='".$cust_data['id']."'";
            $proRow = $this->executeQuery($sql);    
            $this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '6';
            $this->json_array['data'] = "Record updated successfully";
        }      
        return json_encode($this->json_array);
    }
    function get_language() {
        $sqlcity = "SELECT id, name,lang_code FROM  tbl_language";
        $proRowcity = $this->executeQuery($sqlcity);
        $this->json_array['status']['responsecode'] = '0';
        $this->json_array['status']['entity'] = '3';
        while ($rowcity = mysql_fetch_array($proRowcity)) {
            $c_array_temp['id'] = $rowcity['id'];
            $c_array_temp['name'] = $rowcity['name'];
            $c_array_temp['lang_code'] = $rowcity['lang_code'];
            $this->json_array['data']['language'][] = $c_array_temp;
        }
        return json_encode($this->json_array);
    }

}
