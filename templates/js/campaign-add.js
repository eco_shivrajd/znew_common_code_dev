function CriteriaSection(section_type) {
	if(section_type == "free_product"){
		document.getElementById("free_product_div").style="display:block";
		document.getElementById("discount_div").style="display:none";
	}else if(section_type == "discount"){
		document.getElementById("discount_div").style="display:block";
		document.getElementById("free_product_div").style="display:none";
	}
}
function CallAJAX(url,assignDivName) {
	if (window.XMLHttpRequest)
	{
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById("" + assignDivName +"").innerHTML	=	xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET",url,true);
	xmlhttp.send();	
}
function setSelectNoValue(div,select_element){
	var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled>-Select-</option></select>';
	document.getElementById(div).innerHTML	=	select_selement_section;
}
function calculate_data_count(element_value){
	element_value = element_value.toString();
	var element_arr = element_value.split(',');	
	return element_arr.length;
}
function fnShowCity(id_value) {	
	var state_str = $("#state_campaign_discount").val(); 	
	var state_arr_count = calculate_data_count(state_str);
	if(state_arr_count == 1){//If single state selected then only show its related Cities
		var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city_campaign_discount&multiple=multiple";
		CallAJAX(url,"div_city_campaign_discount");
	}else{
		setSelectNoValue("div_city_campaign_discount", "city_campaign_discount");
	}	
}
function FnGetSuburbDropDown(id) {
	var city_str = $("#city_campaign_discount").val();	
	var city_arr_count = calculate_data_count(city_str);
	if(city_arr_count == 1){//If single city selected then only show its related suburb
		var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=suburb_campaign_discount&multiple=multiple";
		CallAJAX(url,"div_suburb_campaign_discount");
	}else{
		setSelectNoValue("div_suburb_campaign_discount", "suburb_campaign_discount");
	}		
}
function FnGetShopsDropdown(id) {	
	var suburb_str = $("#suburb_campaign_discount").val();		
	var suburb_arr_count = calculate_data_count(suburb_str);
	if(suburb_arr_count == 1){//If single suburb selected then only show its related Shops
		var url = "getShopDropdown.php?suburbid="+id.value+"&select_name_id=shop_campaign_discount&multiple=multiple";
		CallAJAX(url,"div_shop_campaign_discount");
	}else{
		setSelectNoValue("div_shop_campaign_discount", "shop_campaign_discount");
	}		
}
						
$("#btnAddDisc").click(function () {
	var total_elements = $("#total_element").val();
	var current_total_elements = parseInt(total_elements) + 1;
	
	var outer_div_open = '<div class="discdet" id="discount_'+current_total_elements+'">';
	var outer_div_close = '</div>';
	var price_input = '<input type="text" name="campaign_product_price_'+current_total_elements+'" id="campaign_product_price_'+current_total_elements+'" data-parsley-trigger="change" data-parsley-pattern="[0-9]*(\.?[0-9]{2}$)?" data-parsley-error-message="Please enter numeric value" class="form-control" onchange="calculate_discount('+current_total_elements+')">';
	var disccont_input = '<input type="text" name="campaign_product_discount_'+current_total_elements+'" id="campaign_product_discount_'+current_total_elements+'" data-parsley-trigger="change" data-parsley-pattern="^(?!\s)[0-9.\' ]*$" data-parsley-error-message="Please enter numeric value" class="form-control minsz" onchange="calculate_discount('+current_total_elements+')">';
	var inner_div1_open = '<div class="form-group"><label class="col-md-3">Discount Details:<span class="mandatory">*</span></label><div class="col-md-6 nopadl"><div class="col-md-4">'+price_input+'</div><label class="col-md-1 nopadl"[0-9]*(\.?[0-9]{2}$)? >Price</label><div class="col-md-2">'+disccont_input+' </div><label class="col-md-1 nopadl" style="padding-top:5px; ">%</label><div class="col-md-1"><button class="btn btn-primary btn-md" type="button" onclick="fnRemoveDiscountSection('+current_total_elements+')" title="Remove Details"><i class="fa fa-times"></i></button></div></div>';
	var inner_div1_close = '</div>';
	var inner_div2_open = '<div class="form-group"><label class="col-md-3"> </label><div class="col-md-6 nopadl"><label class="col-md-4 ">Discounted Amount:</label><label class="col-md-2" id="discount_value_'+current_total_elements+'"></label>';
	var inner_div2_close = '</div>';
	var append_section = outer_div_open+inner_div1_open+inner_div1_close+inner_div2_open+inner_div2_close+'<hr/>'+outer_div_open;
	$("#disccont").append(append_section);
	$('#campaign_product_price_'+current_total_elements).parsley(); 
	$('#campaign_product_discount_'+current_total_elements).parsley(); 
	$("#total_element").val(current_total_elements);
});
function fnRemoveDiscountSection(element_id){
	var total_elements = $("#total_element").val();
	var current_total_elements = parseInt(total_elements) - 1;
	$("#total_element").val(current_total_elements);
	$("#discount_"+element_id).remove();
	if(current_total_elements > 0){
		for(var i=element_id; i<= current_total_elements; i++){
			var current_id = parseInt(i)+1;
			//outer_div
			$("#discount_"+current_id).attr("id","discount_"+i);			
			//price_input
			$("#campaign_product_price_"+current_id).attr("id","campaign_product_price_"+i);		
			$("#campaign_product_price_"+i).attr("name","campaign_product_price_"+i);
			$("#campaign_product_price_"+i).attr("onchange","calculate_discount("+i+")");
			//disccont_input
			$("#campaign_product_discount_"+current_id).attr("id","campaign_product_discount_"+i);		
			$("#campaign_product_discount_"+i).attr("name","campaign_product_discount_"+i);	
			$("#campaign_product_discount_"+i).attr("onchange","calculate_discount("+i+")");	
			//Discounted Amount label
			$("#discount_value_"+current_id).attr("id","discount_value_"+i);		
			$("#discount_value_"+i).attr("name","discount_value_"+i);		
		}
	}
}
function calculate_discount(element_id)
{
	var campaign_product_price = document.getElementById("campaign_product_price_"+element_id).value;
	var campaign_product_discount = document.getElementById("campaign_product_discount_"+element_id).value;
	if(campaign_product_discount > 100){
		alert("Discount should not be greater than 100%");
		return false;
	}else{
		if(campaign_product_price != '' && campaign_product_discount != '')
		{
			var discount_value = ((campaign_product_price * campaign_product_discount)/100);
			//document.getElementById("discount_value").innerHTML = "(Discount Price: "+discount_value+")";;
			
			if(!isNaN(discount_value))
				document.getElementById("discount_value_"+element_id).innerHTML = discount_value;
		}	
	}
}	
function fnShowBrand(type,section_id) {
	if(type == 'c')
	{				
		var div_id = "div_brand_campaign_p_"+section_id;
		var select_name_id = "brand_campaign_p_"+section_id;
		set_select_blank('category_campaign',type,section_id,'fnShowProducts');
		set_select_blank('product_campaign',type,section_id,'fnShowProductVariant');
		set_select_blank('variant_campaign',type,section_id,'');
	}
	else if(type == 'f')
	{
		var div_id = "div_brand_free_p_"+section_id;
		var select_name_id = "brand_free_p_"+section_id;
		set_select_blank('category_free',type,section_id,'fnShowProducts');
		set_select_blank('product_free',type,section_id,'fnShowProductVariant');
		set_select_blank('variant_free',type,section_id,'');
	}
	
	var url = "getBrandDropDown.php?select_name_id="+select_name_id+"&function_param1="+type+"&function_param2="+section_id;
	CallAJAX(url,div_id);
} 
function fnShowCategories(id,type,section_id) {
	if(type == 'c')
	{
		var div_id = "div_category_campaign_p_"+section_id;
		var select_name_id = "category_campaign_p_"+section_id;
		set_select_blank('product_campaign',type,section_id,'fnShowProductVariant');
		set_select_blank('variant_campaign',type,section_id,'');
	}
	else if(type == 'f')
	{
		var div_id = "div_category_free_p_"+section_id;
		var select_name_id = "category_free_p_"+section_id;
		set_select_blank('product_free',type,section_id,'fnShowProductVariant');
		set_select_blank('variant_free',type,section_id,'');
	}
	
	var url = "getCategoryDropdown.php?brandid="+id.value+"&select_name_id="+select_name_id+"&function_param1="+type+"&function_param2="+section_id;
	CallAJAX(url,div_id);
} 
function fnShowProducts (id,type,section_id) {
	if(type == 'c')
	{
		var div_id = "div_product_campaign_p_"+section_id;
		var select_name_id = "product_campaign_p_"+section_id;
		set_select_blank('variant_campaign',type,section_id,'');
	}
	else if(type == 'f')
	{
		var div_id = "div_product_free_p_"+section_id;
		var select_name_id = "product_free_p_"+section_id;
		set_select_blank('variant_free',type,section_id,'');
	}
	
	var url = "getProductDropdown.php?cat_id="+id.value+"&select_name_id="+select_name_id+"&function_param1="+type+"&function_param2="+section_id;
	CallAJAX(url,div_id);	
}

function fnShowProductVariant (id,type,section_id) {
	if(type == 'c')
	{
		var div_id = "div_variant_campaign_p_"+section_id;
		var select_name_id = "variant_campaign_p_"+section_id;
	}
	else if(type == 'f')
	{
		var div_id = "div_variant_free_p_"+section_id;
		var select_name_id = "variant_free_p_"+section_id;
	}
	var url = "getProductVariantDropdown.php?cat_id="+id.value+"&select_name_id="+select_name_id+"&function_param1="+type+"&function_param2="+section_id;
	CallAJAX(url,div_id);
}
function set_select_blank(element,type,section,function_name){
	if(function_name != '')
		var html = '<select name="'+element+'_p_'+section+'" id="'+element+'_'+type+'_'+section+'" class="form-control" onchange="'+function_name+'(this,'+type+','+section+')">';
	else
		var html = '<select name="'+element+'_p_'+section+'" id="'+element+'_'+type+'_'+section+'" class="form-control">';
	
	html = html + '<option value="">-Select-</option></select>';
	var assign_element_div = 'div_'+element+'_p_'+section;
	document.getElementById(""+assign_element_div+"").innerHTML	=	html;
}
function select_html(element,type,section,function_name){
	if(function_name != '')
		var html = '<select name="'+element+'_p_'+section+'" id="'+element+'_p_'+section+'" class="form-control" onchange="'+function_name+'(this,\''+type+'\','+section+')">';
	else
		var html = '<select name="'+element+'_p_'+section+'" id="'+element+'_p_'+section+'" class="form-control">';
	
	html = html + '<option value="">-Select-</option></select>';
	
	return html;
}
$("#btnAddFree").click(function () {
	 
	var total_elements = $("#total_element_free").val();
	var current_total_elements = parseInt(total_elements) + 1;
	
	var outer_div_open = '<div class="free" id="free_pro_'+current_total_elements+'"><div class="row">';
	var outer_div_close = '</div><hr/></div>';
	var section_campaign_open = '<div class="col-sm-6"><h4 style="margin-top:30px;"><b>Campaign Product</b></h4><hr />';
	var section_campaign_close = '</div>';
	
	var brand_campaign_section_open = '<div class="form-group"><label class="col-md-6">Brand:</label><div class="col-md-5" id="div_brand_campaign_p_'+current_total_elements+'">';
	var brand_campaign_section = select_html('brand_campaign','c',current_total_elements,'fnShowCategories');
	var brand_campaign_section_close = '</div></div>';
	var brand_campaign_complete = brand_campaign_section_open+brand_campaign_section+brand_campaign_section_close;
	
	var category_campaign_section_open =  '<div class="form-group"><label class="col-md-6">Category:</label><div class="col-md-5" id="div_category_campaign_p_'+current_total_elements+'">';
	var category_campaign_section = select_html('category_campaign','c',current_total_elements,'fnShowProducts');
	var category_campaign_section_close = '</div></div>';
	var category_campaign_complete = category_campaign_section_open+category_campaign_section+category_campaign_section_close;
	
	var product_campaign_section_open =  '<div class="form-group"><label class="col-md-6">Product:</label><div class="col-md-5" id="div_product_campaign_p_'+current_total_elements+'">';
	var product_campaign_section = select_html('product_campaign','c',current_total_elements,'fnShowProductVariant');
	var product_campaign_section_close = '</div></div>';
	var product_campaign_complete = product_campaign_section_open+product_campaign_section+product_campaign_section_close;
	
	var variant_campaign_section_open =  '<div class="form-group"><label class="col-md-6">Variant:</label><div class="col-md-5" id="div_variant_campaign_p_'+current_total_elements+'">';
	var variant_campaign_section = select_html('variant_campaign','c',current_total_elements,'');
	var variant_campaign_section_close =  '</div></div>';
	var variant_campaign_complete = variant_campaign_section_open+variant_campaign_section+variant_campaign_section_close;
	
	var section_campaign_complete = section_campaign_open+brand_campaign_complete+category_campaign_complete+product_campaign_complete+variant_campaign_complete+section_campaign_close;
	var section_free_open = '<div class="col-sm-5"><h4 style="margin-top:30px;"><b>Free Product</b></h4><hr />';
	var section_free_close = '</div>';
	
	var brand_free_section_open =  '<div class="form-group"><label class="col-md-3">Brand:</label><div class="col-md-5" id="div_brand_free_p_'+current_total_elements+'">';
	var brand_free_section = select_html('brand_campaign','f',current_total_elements,'fnShowCategories');
	var brand_free_section_close =  '</div></div>';
	var brand_free_complete = brand_free_section_open+brand_free_section+brand_free_section_close;
	
	var category_free_section_open =  '<div class="form-group"><label class="col-md-3">Category:</label><div class="col-md-5" id="div_category_free_p_'+current_total_elements+'">';
	var category_free_section = select_html('category_free','f',current_total_elements,'fnShowProducts');
	var category_free_section_close = '</div></div>';
	var category_free_complete = category_free_section_open+category_free_section+category_free_section_close;
	
	var product_free_section_open =  '<div class="form-group"><label class="col-md-3">Product:</label><div class="col-md-5" id="div_product_free_p_'+current_total_elements+'">';
	var product_free_section = select_html('product_free','f',current_total_elements,'fnShowProductVariant');
	var product_free_section_close = '</div></div>';
	var product_free_complete = product_free_section_open+product_free_section+product_free_section_close;
	
	var variant_free_section_open =  '<div class="form-group"><label class="col-md-3">Variant:</label><div class="col-md-5" id="div_variant_free_p_'+current_total_elements+'">';
	var variant_free_section = select_html('variant_free','f',current_total_elements,'');
	var variant_free_section_close = '</div></div>';
	var variant_free_complete = variant_free_section_open+variant_free_section+variant_free_section_close;
	
	var section_free_complete = section_free_open+brand_free_complete+category_free_complete+product_free_complete+variant_free_complete+section_free_close;
	var remove_button = '<div class="col-md-1"><button class="btn btn-primary btn-md" type="button" onclick="fnRemoveFreeProductSection('+current_total_elements+')" title="Remove Campaign & Free Product"><i class="fa fa-times"></i></button></div>';
	
	var append_section = outer_div_open+section_campaign_complete+section_free_complete+remove_button+outer_div_close;
	$("#freecont").append(append_section);
	fnShowBrand('c',current_total_elements);
	setTimeout(
	  function() 
	  {
		fnShowBrand('f',current_total_elements);
	  }, 1000);
	
	$("#total_element_free").val(current_total_elements);	
});
function fnRemoveFreeProductSection(element_id){
	var total_elements = $("#total_element_free").val();
	var current_total_elements = parseInt(total_elements) - 1;
	$("#total_element_free").val(current_total_elements);
	$("#free_pro_"+element_id).remove();
	if(current_total_elements > 0){
		for(var i=element_id; i<= current_total_elements; i++){
			var current_id = parseInt(i)+1;
			//outer_div
			$("#free_pro_"+current_id).attr("id","free_pro_"+i);
			//Campaign section 
			$("#div_brand_campaign_p_"+current_id).attr("id","div_brand_campaign_p_"+i);
			$("#div_brand_campaign_p_"+i).attr("name","div_brand_campaign_p_"+i);
			
			$("#brand_campaign_p_"+current_id).attr("id","brand_campaign_p_"+i);
			$("#brand_campaign_p_"+i).attr("name","brand_campaign_p_"+i);
			$("#brand_campaign_p_"+i).attr("onchange","fnShowCategories(this+i)");//fnShowCategories(this,'c',1)	
			
			$("#div_category_campaign_p_"+current_id).attr("id","div_category_campaign_p_"+i);
			$("#div_category_campaign_p_"+i).attr("name","div_category_campaign_p_"+i);
			$("#category_campaign_p_"+current_id).attr("id","category_campaign_p_"+i);
			$("#category_campaign_p_"+i).attr("name","category_campaign_p_"+i);
			$("#category_campaign_p_"+i).attr("onchange","fnShowProducts(this,'c',"+i+")");//fnShowProducts(this,'c',1)
			
			$("#div_product_campaign_p_"+current_id).attr("id","div_product_campaign_p_"+i);
			$("#div_product_campaign_p_"+i).attr("name","div_product_campaign_p_"+i);
			$("#product_campaign_p_"+current_id).attr("id","product_campaign_p_"+i);
			$("#product_campaign_p_"+i).attr("name","product_campaign_p_"+i);
			$("#product_campaign_p_"+i).attr("onchange","fnShowProductVariant(this,'c',"+i+")");//fnShowProductVariant(this,'c',1)
			
			$("#div_variant_campaign_p_"+current_id).attr("id","div_variant_campaign_p_"+i);
			$("#div_variant_campaign_p_"+i).attr("name","div_variant_campaign_p_"+i);
			$("#variant_campaign_p_"+current_id).attr("id","variant_campaign_p_"+i);
			$("#variant_campaign_p_"+i).attr("name","variant_campaign_p_"+i);
			
			//Free section			
			$("#div_brand_free_p_"+current_id).attr("id","div_brand_free_p_"+i);
			$("#div_brand_free_p_"+i).attr("name","div_brand_free_p_"+i);			
			$("#brand_free_p_"+current_id).attr("id","brand_free_p_"+i);
			$("#brand_free_p_"+i).attr("name","brand_free_p_"+i);
			$("#brand_free_p_"+i).attr("onchange","fnShowCategories(this,'f',"+i+")");//fnShowCategories(this,'f',1)
			
			$("#div_category_free_p_"+current_id).attr("id","div_category_free_p_"+i);
			$("#div_category_free_p_"+i).attr("name","div_category_free_p_"+i);
			$("#category_free_p_"+current_id).attr("id","category_free_p_"+i);
			$("#category_free_p_"+i).attr("name","category_free_p_"+i);
			$("#category_free_p_"+i).attr("onchange","fnShowProducts(this,'f',"+i+")");//fnShowProducts(this,'f',1)
			
			$("#div_product_free_p_"+current_id).attr("id","div_product_free_p_"+i);
			$("#div_product_free_p_"+i).attr("name","div_product_free_p_"+i);
			$("#product_free_p_"+current_id).attr("id","product_free_p_"+i);
			$("#product_free_p_"+i).attr("name","product_free_p_"+i);
			$("#product_free_p_"+i).attr("onchange","fnShowProductVariant(this,'f',"+i+"))");//fnShowProductVariant(this,'f',1)
			
			$("#div_variant_free_p_"+current_id).attr("id","div_variant_free_p_"+i);
			$("#div_variant_free_p_"+i).attr("name","div_variant_free_p_"+i);
			$("#variant_free_p_"+current_id).attr("id","variant_free_p_"+i);
			$("#variant_free_p_"+i).attr("name","variant_free_p_"+i);
				
		}
	}
}

function fnQuantitySection(section){
	if(section == "packet"){		
		document.getElementById("packet_div").style="display:block";
		document.getElementById("pcs_div").style="display:none";
		document.getElementById("weight_div").style="display:none";
	}else if(section == "pcs"){		
		document.getElementById("pcs_div").style="display:block";
		document.getElementById("weight_div").style="display:none";
		document.getElementById("packet_div").style="display:none";
	}else if(section == "weight"){
		document.getElementById("weight_div").style="display:block";
		document.getElementById("packet_div").style="display:none";
		document.getElementById("pcs_div").style="display:none";
	}
	
}
function fnDiscountSection(section){
	if(section == "pcs"){		
		document.getElementById("pcs_discount_div").style="display:block";
		document.getElementById("weight_discount_div").style="display:none";
	}else if(section == "weight"){
		document.getElementById("weight_discount_div").style="display:block";
		document.getElementById("pcs_discount_div").style="display:none";
	}
	
}
function validateForm(){
	var campaign_name = $("#campaign_name").val();
	var campaign_start_date = $("#campaign_start_date").val();
	var campaign_end_date = $("#campaign_end_date").val();
	if(campaign_name == '')
	{
		alert('Please enter Campaign Name');
		$('#campaign_name').focus();
		return false;
	}
	if(campaign_start_date == '')
	{
		alert('Please enter Start Date');
		$('#campaign_start_date').focus();
		return false;
	}
	if(campaign_end_date == '')
	{
		alert('Please enter End Date');
		$('#campaign_end_date').focus();
		return false;
	}	
	if(campaign_start_date != '' && campaign_end_date != '')
	{
		var date_validate = compaire_dates(campaign_start_date,campaign_end_date);
		if(date_validate == 1)
		{
			alert("'End Date' should be greater than 'Start Date'.");
			return false;
		}
	}
	
	var criteriaType_checked = $('input[name="criteriaType"]').is(':checked');
	var criteriaType = $("input[name='criteriaType']:checked").val();
	
	if(criteriaType_checked == false)
	{
		alert('Please select Campaign Type');
		$('#criteriaType').focus();
		return false;
	}
	if(criteriaType_checked == true && criteriaType == "free_product")
	{	
		var current_total_elements = parseInt($("#total_element_free").val());
		if(current_total_elements > 0){
			for(var i=1; i<= current_total_elements; i++){
				var campaign_product = $("#product_campaign_p_"+i).val();
				var free_product = $("#product_free_p_"+i).val();
				if(campaign_product == '')
				{
					alert('Please select Campaign Product');
					$('#product_campaign_p_'+i).focus();
					return false;
				}
				if(free_product == '')
				{
					alert('Please select Free Product');
					$('#product_free_p_'+i).focus();
					return false;
				}
			}
		}
	}else if(criteriaType_checked == true && criteriaType == "discount")
	{
		var state_arr_count = 0;
		var state_str = $("#state_campaign_discount").val(); 	
		if(state_str != '')
			var state_arr_count = calculate_data_count(state_str);
		
		
		var city_arr_count = 0;
		var city_str = $("#city_campaign_discount").val();
		if(city_str != null)
			var city_arr_count = calculate_data_count(city_str);
		
		var suburb_arr_count = 0;
		var suburb_str = $("#suburb_campaign_discount").val();	
		if(suburb_str != null)		
			var suburb_arr_count = calculate_data_count(suburb_str);
	
		var shop_arr_count = 0;
		var shop_str = $("#shop_campaign_discount").val();		
		if(shop_str != null)
			var shop_arr_count = calculate_data_count(shop_str);	
		
		
		//if(state_arr_count == 0 && city_arr_count == 0 && suburb_arr_count == 0 && shop_arr_count == 0)
		if(state_arr_count == 0)
		{
			//alert('Please select State/ City/ Suburb/ Shop on which the Campaign');
			alert('Please select State on which the Campaign');
			$("#state_campaign_discount").focus();
			return false;
		}
		
		var current_total_elements = parseInt($("#total_element").val());
		
		if(current_total_elements > 0){
			for(var i=1; i<= current_total_elements; i++){
				var campaign_product_price = $("#campaign_product_price_"+i).val();
				var campaign_product_discount = $("#campaign_product_discount"+i).val();
				if(campaign_product_price == '')
				{
					alert('Please enter Price/Amount to be discounted');
					$('#campaign_product_price_'+i).focus();
					return false;
				}
				if(campaign_product_discount == '')
				{
					alert('Please Percentage(%) of discount on Price');
					$('#campaign_product_discount_'+i).focus();
					return false;
				}
			}
		}
	}
	
	
}
function compaire_dates(date1,date2){
	
	if(date1 != '')
	{
		var d1 = new Date(date1);
		var fromtime = d1.getTime();
	}	
	if(date2 != '')
	{
		var d2 = new Date(date2);
		var totime = d2.getTime();
	}
	if(date1 != '' && date2 != '')
	{
		if(fromtime > totime)			
			return 1;//date1 greater than date 2
		else
			return 0;//date1 lesser than date 2
	}
	
}

$('.date-picker1').datepicker({	
	autoclose: true
});