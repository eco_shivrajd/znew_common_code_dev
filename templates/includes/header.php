<?php
include ("../../includes/config.php");
include ("../includes/common.php");
if(!isset($_SESSION[SESSION_PREFIX."user_id"])) {	
	echo '<script>location.href="../login.php";</script>';
	exit;
}
$user_id = $_SESSION[SESSION_PREFIX."user_id"];
//$report_type_search="";
$usertype_role=$_REQUEST['report_type'];

$condnsearch="";
$selperiod=$_REQUEST['selTest'];
//$order_status=$_REQUEST['order_status'];

/*$date1 = "1998-11-24"; 
$date2 = "1997-03-26"; 
if ($date1 > $date2) {
    echo "$date1 is latest than $date2"; 
}
else {
    echo "$date1 is older than $date2"; 
}
*/
switch($selperiod){
	case 1:
		$condnsearch=" AND yearweek(order_date) = yearweek(curdate())";
	break;
	case 2:
		$condnsearch=" AND date_format(order_date, '%m')=date_format(now(), '%m')";
	break;
	case 3:
		if($_REQUEST['todate']!="")
			$todat=$_REQUEST['todate'];
		else
			$todat = date("d-m-Y");
	
		$condnsearch=" AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('".$_REQUEST['frmdate']."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
	break;
	case 4:
		$condnsearch=" AND DATE_FORMAT(order_date,'%Y-%m-%d')=DATE(NOW())";
	break;
	case 0:
		$condnsearch="";
	break;
	default:
		$condnsearch="";
	break;
}

$report_type_byrole=$_REQUEST['report_type_byrole'];
if ($report_type_byrole=='') 
{
	$report_type_byrole='SalesPerson';
}
/*if ($_SESSION[SESSION_PREFIX."user_role"] =='Admin') 
{
	$condnsearch2=" AND u.user_role='Admin'";
}
else
{
	$condnsearch2=" AND o.order_to='".$user_id."'";
}*/
switch($report_type_byrole)
{
	case "Admin":
		$condnsearch2=" AND u.user_role='Admin' ";
	break;
	case "SalesPerson":
		$condnsearch2=" AND u.user_role='SalesPerson' AND find_in_set('".$user_id."',u.parent_ids) <> 0";
	break;
	case "Distributor":
		$condnsearch2=" AND u.user_role='Distributor' AND find_in_set('".$user_id."',u.parent_ids) <> 0";
	break;
	case "Superstockist":
		$condnsearch2=" AND u.user_role='Superstockist' AND find_in_set('".$user_id."',u.parent_ids) <> 0";
	break;
}

if(isset($_SESSION[SESSION_PREFIX."firstname"]))
{
?>
<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<style>
g text:last-child {font-size :15px;}
</style>
<meta charset="utf-8"/>
<title><?=SITETITLE;?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
 <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

 <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:500,700" rel="stylesheet">

<link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css"></script>
<!-- BEGIN THEME STYLES -->
<link href="../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="../../assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon"  href="../../assets/TitleLogo/favicon.png" type="image/gif" size="16x16">

<!-- amCharts javascript sources -->
		<script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
		<script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script>
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/pie.js"></script>
		<script type="text/javascript" src="../includes/js/common.js"></script>

<style type="text/css">
    .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open>.dropdown-toggle.btn-primary {
    color: #fff;
    background-color: #a24507;
    border-color: #ccc; /*set the color you want here*/
}
</style>
		<?php
		if (strpos($_SERVER['SCRIPT_NAME'], 'index.php') !== false)
		{


			if ($_SESSION[SESSION_PREFIX."user_role"] !='Accountant') 
			{
				$order_to = $_SESSION[SESSION_PREFIX."user_id"];
			}
			else
			{
				 $sql_acc="SELECT external_id as order_to  FROM tbl_user where id ='".$_SESSION[SESSION_PREFIX.'user_id']."'";
				$result_acc 	=	mysqli_query($con,$sql_acc);
				$row_acc 		=	mysqli_fetch_assoc($result_acc);
				$order_to = $row_acc['order_to'];
			}
			

  if ($report_type_byrole!="SalesPerson")
  {
  	  $sqlproducttrend="SELECT u.user_role,od.id, od.order_id,round(SUM(od.p_cost_cgst_sgst),2) AS total_cost,
od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,od.product_quantity,od.product_unit_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst FROM `tbl_order_details` AS od
LEFT JOIN tbl_orders o ON od.order_id = o.id 
LEFT JOIN tbl_user u ON o.order_to = u.id 
WHERE  1=1 ".$condnsearch." ".$condnsearch2."
GROUP BY od.product_id";// AND o.order_to='".$order_to."'
          
      $sqlcategory="SELECT u.user_role,od.id, od.order_id,od.cat_id,round(SUM(od.p_cost_cgst_sgst),2) AS total_cost,
od.product_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS category_name,od.product_quantity,od.product_unit_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst FROM `tbl_order_details` AS od
LEFT JOIN tbl_orders o ON od.order_id = o.id 
LEFT JOIN tbl_user u ON o.order_to = u.id 
WHERE  1=1 ".$condnsearch." ".$condnsearch2." 
GROUP BY od.cat_id";//AND o.order_to='".$order_to."'

  $sqlregionwise = "SELECT o.order_no, od.p_cost_cgst_sgst,round(SUM(od.p_cost_cgst_sgst),2)  
			  AS total_cost,od.product_unit_cost, s.address,s.suburbid, ts.suburbnm FROM tbl_orders o  
 LEFT JOIN tbl_order_details od ON o.id = od.order_id 
  LEFT JOIN tbl_shops s ON o.shop_id = s.id 
  LEFT JOIN tbl_area ts ON ts.id = s.suburbid 
  LEFT JOIN tbl_user u ON o.order_to = u.id 
  WHERE 1=1 ".$condnsearch." ".$condnsearch2." AND s.suburbid!='' GROUP BY s.suburbid";//AND o.order_to='".$order_to."'
  }
  else
  {
  	   $sqlproducttrend="SELECT u.user_role,od.id, od.order_id,round(SUM(od.p_cost_cgst_sgst),2) AS total_cost,
od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,od.product_quantity,od.product_unit_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst FROM `tbl_order_details` AS od
LEFT JOIN tbl_orders o ON od.order_id = o.id 
LEFT JOIN tbl_user u ON o.order_from = u.id 
WHERE  1=1 ".$condnsearch." ".$condnsearch2."
GROUP BY od.product_id";// AND o.order_to='".$order_to."'

       $sqlcategory="SELECT u.user_role,od.id, od.order_id,od.cat_id,round(SUM(od.p_cost_cgst_sgst),2) AS total_cost,
od.product_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS category_name,od.product_quantity,od.product_unit_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst FROM `tbl_order_details` AS od
LEFT JOIN tbl_orders o ON od.order_id = o.id 
LEFT JOIN tbl_user u ON o.order_from = u.id 
WHERE  1=1 ".$condnsearch." ".$condnsearch2." 
GROUP BY od.cat_id";//AND o.order_to='".$order_to."'

 $sqlregionwise = "SELECT o.order_no, od.p_cost_cgst_sgst,round(SUM(od.p_cost_cgst_sgst),2)  
			  AS total_cost,od.product_unit_cost, s.address,s.suburbid, ts.suburbnm FROM tbl_orders o  
 LEFT JOIN tbl_order_details od ON o.id = od.order_id 
  LEFT JOIN tbl_shops s ON o.shop_id = s.id 
  LEFT JOIN tbl_area ts ON ts.id = s.suburbid 
  LEFT JOIN tbl_user u ON o.order_from = u.id 
  WHERE 1=1 ".$condnsearch." ".$condnsearch2." AND s.suburbid!='' GROUP BY s.suburbid";//AND o.order_to='".$order_to."'
  }

            



/*   if (isset($usertype_role)) 
   {
        if ($usertype_role =="Salesperson") {
        	 $sql_role ="select u.id,u.firstname as Name,u.user_role,sum(od.p_cost_cgst_sgst) as Total_Sales from tbl_user u left join tbl_orders o on u.id=o.order_from LEFT JOIN tbl_order_details od ON od.order_id =o.id where user_role='".$usertype_role."' AND od.p_cost_cgst_sgst !='' AND find_in_set('".$user_id."',u.parent_ids) <> 0  group by u.id";
        }
        else
        {
        	 $sql_role ="select u.id,u.firstname as Name,u.user_role,sum(od.p_cost_cgst_sgst) as Total_Sales from tbl_user u left join tbl_orders o on u.id=o.order_to LEFT JOIN tbl_order_details od ON od.order_id =o.id where user_role='".$usertype_role."' AND od.p_cost_cgst_sgst !='' AND find_in_set('".$user_id."',u.parent_ids) <> 0 group by u.id";
        } 
			$result_role = mysqli_query($con,$sql_role);
			$rows_role = array();
			while ($row_role = mysqli_fetch_array ($result_role)){
			$rows_role[] = array(
			"category" => $row_role['Name'],
			"column-1" => $row_role['Total_Sales']
			);
			}
			$txtcat="User Role Wise Trend";
			$rolearr= json_encode($rows_role);
   } 
   */
$resultregionwise = mysqli_query($con,$sqlregionwise);
$rowsregionwise = array();
while ($rowrw = mysqli_fetch_array ($resultregionwise))
{
     $rowsregionwise[] = array(
    "category" => $rowrw['suburbnm'],
   "column-1" => $rowrw['total_cost'] 
     );
 }
$txtnmmm="Region Wise Trend";
$regionwisetrend= json_encode($rowsregionwise);



$resultproducttrend = mysqli_query($con,$sqlproducttrend);
$rowsproduct = array();
while ($rowpr = mysqli_fetch_array ($resultproducttrend)){
     $rowsproduct[] = array(
    "category" => $rowpr['product_name'],
   "column-1" => $rowpr['total_cost'] 
  );
  }
  $txtnmm="Product Wise Trend";
  $producttrend= json_encode($rowsproduct); 

//exit();
$resultcategory = mysqli_query($con,$sqlcategory);
$rowscategory = array();
while ($rowcategory = mysqli_fetch_array ($resultcategory)){
     $rowscategory[] = array(
    "category" => $rowcategory['category_name'],
    "column-1" => $rowcategory['total_cost']
  );
  }
  $txtcat="Category Wise Trend";
  $catarr= json_encode($rowscategory);
?>
			<!-- amCharts javascript code -->
			<script type="text/javascript">
			var regionwisetrend=<?php echo $regionwisetrend;?>;		
		if(regionwisetrend.length>0){
			var chart = AmCharts.makeChart("chartdiv",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start",
						"labelRotation": 45,
						"labelsEnabled":false
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Region Wise Trend",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "₹"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": ""
						}
					],
					"dataProvider": <?php echo $regionwisetrend;?>
				}
			);		
		}			
		</script>		
		<!-- amCharts javascript code -->
		<!--<script type="text/javascript">
		var superstockistarr=<?php echo $superstockistarr;?>;		
		if(superstockistarr.length>0){
			AmCharts.makeChart("chartdiv1",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start",
						"labelRotation": 45,
						"labelsEnabled":false
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"fillColors": "#12D812",
							"id": "AmGraph-1",
							"title": "Farmer Wise Sales in ₹",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "₹"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": " "
						}
					],
					//"dataProvider": <?php //echo $stockistarr;?>
					"dataProvider": <?php echo $superstockistarr;?>
				}
			);
		}
		</script>-->
		<!-- amCharts javascript code -->
		<script type="text/javascript">
		var categorydata=<?php echo $catarr;?>;		
		if(categorydata.length>0){
			AmCharts.makeChart("chartdiv2",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start",
						"labelRotation": 45,
						"labelsEnabled":false
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "<?php echo $txtcat;?>",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "₹"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": " "
						}
					],
					"dataProvider": <?php echo $catarr;?>
				}
			);
		}
			
		</script>
	<!--	
<script type="text/javascript">
		var roledata=<?php echo $rolearr;?>;		
		if(roledata.length>0){
			AmCharts.makeChart("chartdiv22",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start",
						"labelRotation": 45,
						"labelsEnabled":false
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "<?php echo $txtcat;?>",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "₹"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": " "
						}
					],
					"dataProvider": <?php echo $rolearr;?>
				}
			);
		}
			
		</script>-->
		<script type="text/javascript">
		var productdata=<?php echo $producttrend;?>;		
		if(productdata.length>0){
			AmCharts.makeChart("chartdiv3",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start",
						"labelRotation": 45,
						"labelsEnabled":false
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Product Wise Trend",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "₹" 
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": " "
						}
					],
						"dataProvider":<?php echo $producttrend;?>
				}
			);
		}
		</script>				
		<style>
		.fa.fa-inr.fa-6 {font-size: 30px;}
		.amcharts-chart-div > a {display: none !important;}
		</style>
		<?php 
		}// index page...
		?>
</head>
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="./index.php">
			<img src="<?=PORTALLOGOPATH;?>" alt="logo" class="logo-default"/>
			</a>
			<div class="menu-toggler sidebar-toggler hide">
				<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
			</div>
			
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-user">
                   

					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                      <img src="../../assets/global/img/user_icon.png" class="img-radius" alt="User-Profile-Image" width="30px" height="30px">
					<span class="username ">
					<?php echo "Welcome "."<b>".$_SESSION[SESSION_PREFIX.'firstname']."</b>";?> </span>
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
					 
                           <li>
							<a href="profile.php">
							<i class="icon-user"></i> My Profile </a>
						</li>
						  <li>
							<a href="changepassword.php">
							<i class="icon-lock"></i>Change Password </a>
						</li>
					 
						<li>
                            <?php echo '<a href="../logout.php"><i class="icon-key"></i> Logout </a>';?>
						</li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				
				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<?php
}
else
{	
header("location:../login.php");
}
?>