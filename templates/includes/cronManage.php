<?php
/***********************************************************
 * File Name	: cronManage.php
 ************************************************************/	
class cronManage
{	
	private $local_connection   	= 	'';		
	private $common_connection   	= 	'';
	public function __construct($con123,$conmain) {
		$this->local_connection = $con123;
		$this->common_connection = $conmain;
		date_default_timezone_set('Asia/Kolkata');
	}
	

    public function get_summary_for_mail_ccb($client_id){		
		//$date = date('d-m-Y');
		$date =date('d-m-Y',strtotime("-1 days"));
		$condnsearch="  date_format(oa.order_date, '%d-%m-%Y') = '".$date."' ";
		$condnsearch11="  date_format(order_date, '%d-%m-%Y') = '".$date."' ";
					
		 $sql="SELECT count(oa.id) as Todays_orders ,sum(vo.p_cost_cgst_sgst) as Todays_Sales 
				FROM tbl_orders oa   
				LEFT JOIN tbl_order_details vo ON oa.id = vo.order_id 
				where ".$condnsearch."";	
		$result = mysqli_query($this->local_connection,$sql);
		
		 $sql112="SELECT count(id) as Todays_orders 
				FROM tbl_orders	where ".$condnsearch11."";	
		 $result112 = mysqli_query($this->local_connection,$sql112);

		$sql11="SELECT * FROM tbl_route";	
		$result15 = mysqli_query($this->local_connection,$sql11);
        $row15 = mysqli_fetch_assoc($result15);
		$records['route_name']=$row15['route_name'];

		
		 $sqltotalsale="SELECT count(oa.id) as Total_orders,sum(vo.p_cost_cgst_sgst) as Total_Sales 
				FROM tbl_order_details vo  
				LEFT JOIN tbl_orders oa ON  vo.order_id = oa.id ";	
		$result5 = mysqli_query($this->local_connection,$sqltotalsale);
		$row5 = mysqli_fetch_assoc($result5);
	//	$records['Total_orders']=$row5['Total_orders'];
		$records['Total_Sales']=$row5['Total_Sales'];
		
		$sqltotalorders="SELECT count(id) as Total_orders FROM tbl_orders";	
		$result15 = mysqli_query($this->local_connection,$sqltotalorders);
		$row15 = mysqli_fetch_assoc($result15);
		$records['Total_orders']=$row15['Total_orders'];
		
		$sql2="select database() as manufacturer";
		$result2 = mysqli_query($this->local_connection,$sql2);
		$row2 = mysqli_fetch_assoc($result2);
		$output = str_replace("salzpoin_","",$row2['manufacturer']);
		$output = str_replace("znew_uat","",$output);
		
		
		$sql3="select count(s.id) as Total_shops_added_count from tbl_shops  s where date_format(s.added_on, '%d-%m-%Y') = '".$date."' ";
		$result3 = mysqli_query($this->local_connection,$sql3);
		$row3 = mysqli_fetch_assoc($result3);
		$records['Total_shops_added_count']=$row3['Total_shops_added_count'];
		
		$sql4="select count(s.id) as Total_shops_exists from tbl_shops  s ";
		$result4 = mysqli_query($this->local_connection,$sql4);
		$row4 = mysqli_fetch_assoc($result4);
		$records['Total_shops_exists']=$row4['Total_shops_exists'];


		$sql5="SELECT * FROM tbl_clients_maintenance where id = '".$client_id."' ";
	//	$result5 = mysqli_query($conmain,$sql5);
		$result5 = mysqli_query($this->common_connection,$sql5);		
		$row5 = mysqli_fetch_assoc($result5);
		$records['client_id']=$row5['id'];
		$records['clientnm']=$row5['clientnm'];
		$records['dateofcontract']=$row5['dateofcontract'];
		
		//todays salesperson added
		$sql_todays_sp_count="select count(s.id) as todays_sp_count from tbl_user  s where date_format(s.added_on, '%d-%m-%Y') = '".$date."' and user_type='SalesPerson' ";
		$result_todays_sp_count = mysqli_query($this->local_connection,$sql_todays_sp_count);
		$row_todays_sp_count = mysqli_fetch_assoc($result_todays_sp_count);
		$records['todays_sp_count']=$row_todays_sp_count['todays_sp_count'];
		
		//total salesperson added till date
		$sql_total_sp_count="select count(s.id) as total_sp_count from tbl_user s where  user_type='SalesPerson'";
		$result_total_sp_count = mysqli_query($this->local_connection,$sql_total_sp_count);
		$row_total_sp_count = mysqli_fetch_assoc($result_total_sp_count);
		$records['total_sp_count']=$row_total_sp_count['total_sp_count'];
		
		$records['db']=$output;
		$row_count = mysqli_num_rows($result);		
		while($row = mysqli_fetch_assoc($result)){			
		//	$records['summary']['Todays_orders'] = $row['Todays_orders'];			
			$records['summary']['Todays_Sales'] = $row['Todays_Sales'];
		}
		while($row = mysqli_fetch_assoc($result112)){			
			$records['summary']['Todays_orders'] = $row['Todays_orders'];			
		//	$records['summary']['Todays_Sales'] = $row['Todays_Sales'];
		}
		return $records;
	}
	
	 public function get_summary_for_mail_ccb_dut($client_id){		
		//$date = date('d-m-Y');
		$date =date('d-m-Y',strtotime("-1 days"));
		$condnsearch="  date_format(oa.order_date, '%d-%m-%Y') = '".$date."' ";
		$condnsearch11="  date_format(order_date, '%d-%m-%Y') = '".$date."' ";
					
		 $sql="SELECT count(oa.id) as Todays_orders ,sum(vo.p_cost_cgst_sgst) as Todays_Sales 
				FROM tbl_orders oa   
				LEFT JOIN tbl_order_details vo ON oa.id = vo.order_id 
				where ".$condnsearch."";	
		$result = mysqli_query($this->local_connection,$sql);
		
		 $sql112="SELECT count(id) as Todays_orders 
				FROM tbl_orders	where ".$condnsearch11."";	
		 $result112 = mysqli_query($this->local_connection,$sql112);

		$sql11="SELECT * FROM tbl_route";	
		$result15 = mysqli_query($this->local_connection,$sql11);
        $row15 = mysqli_fetch_assoc($result15);
		$records['route_name']=$row15['route_name'];

		
		 $sqltotalsale="SELECT count(oa.id) as Total_orders,sum(vo.p_cost_cgst_sgst) as Total_Sales 
				FROM tbl_order_details vo  
				LEFT JOIN tbl_orders oa ON  vo.order_id = oa.id ";	
		$result5 = mysqli_query($this->local_connection,$sqltotalsale);
		$row5 = mysqli_fetch_assoc($result5);
	//	$records['Total_orders']=$row5['Total_orders'];
		$records['Total_Sales']=$row5['Total_Sales'];
		
		$sqltotalorders="SELECT count(id) as Total_orders FROM tbl_orders";	
		$result15 = mysqli_query($this->local_connection,$sqltotalorders);
		$row15 = mysqli_fetch_assoc($result15);
		$records['Total_orders']=$row15['Total_orders'];
		
		$sql2="select database() as manufacturer";
		$result2 = mysqli_query($this->local_connection,$sql2);
		$row2 = mysqli_fetch_assoc($result2);
		$output = str_replace("salzpoin_","",$row2['manufacturer']);
		$output = str_replace("znew_uat","",$output);
		
		
		$sql3="select count(s.id) as Total_shops_added_count from tbl_shops  s where date_format(s.added_on, '%d-%m-%Y') = '".$date."' ";
		$result3 = mysqli_query($this->local_connection,$sql3);
		$row3 = mysqli_fetch_assoc($result3);
		$records['Total_shops_added_count']=$row3['Total_shops_added_count'];
		
		$sql4="select count(s.id) as Total_shops_exists from tbl_shops  s ";
		$result4 = mysqli_query($this->local_connection,$sql4);
		$row4 = mysqli_fetch_assoc($result4);
		$records['Total_shops_exists']=$row4['Total_shops_exists'];


		$sql5="SELECT * FROM tbl_clients_maintenance where id = '".$client_id."' ";
	//	$result5 = mysqli_query($conmain,$sql5);
		$result5 = mysqli_query($this->common_connection,$sql5);		
		$row5 = mysqli_fetch_assoc($result5);
		$records['client_id']=$row5['id'];
		$records['clientnm']=$row5['clientnm'];
		$records['dateofcontract']=$row5['dateofcontract'];
		
		//todays salesperson added
		$sql_todays_sp_count="select count(s.id) as todays_sp_count from tbl_user  s where date_format(s.added_on, '%d-%m-%Y') = '".$date."' and user_role='SalesPerson' ";
		$result_todays_sp_count = mysqli_query($this->local_connection,$sql_todays_sp_count);
		$row_todays_sp_count = mysqli_fetch_assoc($result_todays_sp_count);
		$records['todays_sp_count']=$row_todays_sp_count['todays_sp_count'];

       //todays salesperson login
		$sql_todays_sp_login="select count(s.id) as todays_sp_login from tbl_sp_attendance  s where date_format(s.tdate, '%d-%m-%Y') = '".$date."'";
		$result_todays_sp_login = mysqli_query($this->local_connection,$sql_todays_sp_login);
		$row_todays_sp_login = mysqli_fetch_assoc($result_todays_sp_login);
		$records['todays_sp_login']=$row_todays_sp_login['todays_sp_login'];
		
		//total salesperson added till date
		$sql_total_sp_count="select count(s.id) as total_sp_count from tbl_user s where  user_role='SalesPerson'";
		$result_total_sp_count = mysqli_query($this->local_connection,$sql_total_sp_count);
		$row_total_sp_count = mysqli_fetch_assoc($result_total_sp_count);
		$records['total_sp_count']=$row_total_sp_count['total_sp_count'];
		
		$records['db']=$output;
		$row_count = mysqli_num_rows($result);		
		while($row = mysqli_fetch_assoc($result)){			
		//	$records['summary']['Todays_orders'] = $row['Todays_orders'];			
			$records['summary']['Todays_Sales'] = $row['Todays_Sales'];
		}
		while($row = mysqli_fetch_assoc($result112)){			
			$records['summary']['Todays_orders'] = $row['Todays_orders'];			
		//	$records['summary']['Todays_Sales'] = $row['Todays_Sales'];
		}
		return $records;
	}

	public function get_summary_for_mail_ccb_other_details($client_id){		
		//$date = date('d-m-Y');
		$date =date('d-m-Y',strtotime("-1 days"));
		$condnsearch="  date_format(oa.order_date, '%d-%m-%Y') = '".$date."' ";
					
		 $sql="SELECT count(oa.id) as Todays_orders ,sum(vo.p_cost_cgst_sgst) as Todays_Sales 
				FROM tbl_orders oa   
				LEFT JOIN tbl_order_details vo ON oa.id = vo.order_id 
				where ".$condnsearch."";	
		$result = mysqli_query($this->local_connection,$sql);

		$sql11="SELECT * FROM tbl_route";	
		$result15 = mysqli_query($this->local_connection,$sql11);
        $row15 = mysqli_fetch_assoc($result15);
		$records['route_name']=$row15['route_name'];

		
		 $sqltotalsale="SELECT count(oa.id) as Total_orders,sum(vo.p_cost_cgst_sgst) as Total_Sales 
				FROM tbl_order_details vo  
				LEFT JOIN tbl_orders oa ON  vo.order_id = oa.id";	
		$result5 = mysqli_query($this->local_connection,$sqltotalsale);
		$row5 = mysqli_fetch_assoc($result5);
		$records['Total_orders']=$row5['Total_orders'];
		$records['Total_Sales']=$row5['Total_Sales'];
		
		$sql2="select database() as manufacturer";
		$result2 = mysqli_query($this->local_connection,$sql2);
		$row2 = mysqli_fetch_assoc($result2);
		$output = str_replace("salzpoin_","",$row2['manufacturer']);
		
		
		$sql3="select count(s.id) as Total_shops_added_count from tbl_shops  s where date_format(s.added_on, '%d-%m-%Y') = '".$date."' ";
		$result3 = mysqli_query($this->local_connection,$sql3);
		$row3 = mysqli_fetch_assoc($result3);
		$records['Total_shops_added_count']=$row3['Total_shops_added_count'];
		
		$sql4="select count(s.id) as Total_shops_exists from tbl_shops  s ";
		$result4 = mysqli_query($this->local_connection,$sql4);
		$row4 = mysqli_fetch_assoc($result4);
		$records['Total_shops_exists']=$row4['Total_shops_exists'];


		$sql5="SELECT * FROM tbl_clients_maintenance where id = '".$client_id."' ";
	//	$result5 = mysqli_query($conmain,$sql5);
		$result5 = mysqli_query($this->common_connection,$sql5);		
		$row5 = mysqli_fetch_assoc($result5);
		$records['client_id']=$row5['id'];
		$records['clientnm']=$row5['clientnm'];
		$records['dateofcontract']=$row5['dateofcontract'];
		
		//todays salesperson added
		$sql_todays_sp_count="select count(s.id) as todays_sp_count from tbl_user  s where date_format(s.added_on, '%d-%m-%Y') = '".$date."' and user_type='SalesPerson' ";
		$result_todays_sp_count = mysqli_query($this->local_connection,$sql_todays_sp_count);
		$row_todays_sp_count = mysqli_fetch_assoc($result_todays_sp_count);
		$records['todays_sp_count']=$row_todays_sp_count['todays_sp_count'];		
		//total salesperson added till date
		$sql_total_sp_count="select count(s.id) as total_sp_count from tbl_user s where  user_type='SalesPerson'";
		$result_total_sp_count = mysqli_query($this->local_connection,$sql_total_sp_count);
		$row_total_sp_count = mysqli_fetch_assoc($result_total_sp_count);
		$records['total_sp_count']=$row_total_sp_count['total_sp_count'];


        //todays Superstockist added
		$sql_todays_ss_count="select count(s.id) as todays_ss_count from tbl_user  s where date_format(s.added_on, '%d-%m-%Y') = '".$date."' and user_type='Superstockist' and isdeleted!=1";
		$result_todays_ss_count = mysqli_query($this->local_connection,$sql_todays_ss_count);
		$row_todays_ss_count = mysqli_fetch_assoc($result_todays_ss_count);
		$records['todays_ss_count']=$row_todays_ss_count['todays_ss_count'];		
		//total Superstockist added till date
		$sql_total_ss_count="select count(s.id) as total_ss_count from tbl_user s where  user_type='Superstockist' and isdeleted!=1";
		$result_total_ss_count = mysqli_query($this->local_connection,$sql_total_ss_count);
		$row_total_ss_count = mysqli_fetch_assoc($result_total_ss_count);
		$records['total_ss_count']=$row_total_ss_count['total_ss_count'];


		//todays Distributor added
		$sql_todays_stockist_count="select count(s.id) as todays_stockist_count from tbl_user  s where date_format(s.added_on, '%d-%m-%Y') = '".$date."' and user_type='Distributor' and isdeleted!=1";
		$result_todays_stockist_count = mysqli_query($this->local_connection,$sql_todays_stockist_count);
		$row_todays_stockist_count = mysqli_fetch_assoc($result_todays_stockist_count);
		$records['todays_stockist_count']=$row_todays_stockist_count['todays_stockist_count'];		
		//total Distributor added till date
		$sql_total_stockist_count="select count(s.id) as total_stockist_count from tbl_user s where  user_type='Distributor' and isdeleted!=1";
		$result_total_stockist_count = mysqli_query($this->local_connection,$sql_total_stockist_count);
		$row_total_stockist_count = mysqli_fetch_assoc($result_total_stockist_count);
		$records['total_stockist_count']=$row_total_stockist_count['total_stockist_count'];

		//todays DeliveryPerson added
		$sql_todays_dp_count="select count(s.id) as todays_dp_count from tbl_user  s where date_format(s.added_on, '%d-%m-%Y') = '".$date."' and user_type='DeliveryPerson' and isdeleted!=1";
		$result_todays_dp_count = mysqli_query($this->local_connection,$sql_todays_dp_count);
		$row_todays_dp_count = mysqli_fetch_assoc($result_todays_dp_count);
		$records['todays_dp_count']=$row_todays_dp_count['todays_dp_count'];		
		//total DeliveryPerson added till date
		$sql_total_dp_count="select count(s.id) as total_dp_count from tbl_user s where  user_type='DeliveryPerson' and isdeleted!=1";
		$result_total_dp_count = mysqli_query($this->local_connection,$sql_total_dp_count);
		$row_total_dp_count = mysqli_fetch_assoc($result_total_dp_count);
		$records['total_dp_count']=$row_total_dp_count['total_dp_count'];

        //todays Product added
		$sql_todays_product_count="select count(s.id) as todays_product_count from tbl_product  s where date_format(s.added_on, '%d-%m-%Y') = '".$date."' and isdeleted!=1 ";
		$result_todays_product_count = mysqli_query($this->local_connection,$sql_todays_product_count);
		$row_todays_product_count = mysqli_fetch_assoc($result_todays_product_count);
		$records['todays_product_count']=$row_todays_product_count['todays_product_count'];		
		//total Product added till date
		$sql_total_product_count="select count(s.id) as total_product_count from tbl_product s where  isdeleted!=1";
		$result_total_product_count = mysqli_query($this->local_connection,$sql_total_product_count);
		$row_total_product_count = mysqli_fetch_assoc($result_total_product_count);
		$records['total_product_count']=$row_total_product_count['total_product_count'];


         //todays Lead added
		 $sql_todays_lead_count="select count(s.id) as todays_lead_count from tbl_leads  s where date_format(s.added_on, '%d-%m-%Y') = '".$date."' and isdeleted!=1 ";
		$result_todays_lead_count = mysqli_query($this->local_connection,$sql_todays_lead_count);
		$row_todays_lead_count = mysqli_fetch_assoc($result_todays_lead_count);
		$records['todays_lead_count']=$row_todays_lead_count['todays_lead_count'];		
		//total Lead added till date
		$sql_total_lead_count="select count(s.id) as total_lead_count from tbl_leads s where  isdeleted!=1";
		$result_total_lead_count = mysqli_query($this->local_connection,$sql_total_lead_count);
		$row_total_lead_count = mysqli_fetch_assoc($result_total_lead_count);
		$records['total_lead_count']=$row_total_lead_count['total_lead_count'];




		
		$records['db']=$output;
		$row_count = mysqli_num_rows($result);		
		while($row = mysqli_fetch_assoc($result)){			
			$records['summary']['Todays_orders'] = $row['Todays_orders'];			
			$records['summary']['Todays_Sales'] = $row['Todays_Sales'];
		}			
		return $records;
	}

    
}
?>