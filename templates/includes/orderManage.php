<?php

/* * *********************************************************
 * File Name    : orderManage.php
 * ********************************************************** */

class orderManage {

    private $local_connection = '';
    private $common_connection = '';

    public function __construct($con, $conmain) {
        $this->local_connection = $con;
        $this->common_connection = $conmain;
    }
    
    public function getReportTitle_total($user_role) {
        extract($_POST);    
        //echo "<pre>";print_r($_POST);die();
        if($reportType=='Typewise'){
            $thePostIdArray = explode(',', $_POST['report_type_usertype']);
            $report_type_usertype=$thePostIdArray[0];
            $report_type_role=$thePostIdArray[1];
        }
        $title_string='';
        if($reportType=='Rolewise'){
            $title_string=$user_role;
        }else if($reportType=='Typewise'){
            $title_string=$report_type_usertype;
        }else if($reportType=='Productwise'){
            $title_string='Product';
        }else{
            $title_string='Shop';
        }
        $report_title = '';         
        switch ($selTest) {
            case 1:
                $monday = strtotime("last sunday");
                $monday = date('w', $monday) == date('w') ? $monday + 7 * 86400 : $monday;

                $sunday = strtotime(date("d-m-Y", $monday) . " +6 days");

                $this_week_sd = date("d-m-Y", $monday);
                $this_week_ed = date("d-m-Y", $sunday);
                $date = " Weekly (" . $this_week_sd . " TO " . $this_week_ed . ") Report";
                break;
            case 2:
                $date = " Current Month (" . date('F') . ") Report";
                break;
            case 3:
                $date = " Report During ($frmdate To $todate)";
                break;
            case 4:
                $date = " Today's (" . date('d-m-Y') . ") Report";
                break;
            case 5:
                $date = "All";
                break;
        }
        $report_title = $title_string."(".$date.")" ;
        return $report_title;
    }
    public function getReportTitle($frmdate = null, $todate = null) {
        extract($_POST);
        $report_title = '';
        switch ($report_type) {
            case "superstockist":
                $report_title = 'Super Stockist';
                break;
            case "stockist":
                $report_title = 'Stockist';
                break;
            case "salesperson":
                $report_title = 'Sales Person';
                break;
            case "deliveryperson":
                $report_title = 'Delivery Person';
                break;
            case "shop":
                $report_title = 'Shops';
                break;
            case "product":
                $report_title = 'Products';
                break;
        }
        switch ($selTest) {
            case 1:
                $monday = strtotime("last sunday");
                $monday = date('w', $monday) == date('w') ? $monday + 7 * 86400 : $monday;

                $sunday = strtotime(date("d-m-Y", $monday) . " +6 days");

                $this_week_sd = date("d-m-Y", $monday);
                $this_week_ed = date("d-m-Y", $sunday);
                $date = " Weekly (" . $this_week_sd . " TO " . $this_week_ed . ") Report";
                break;
            case 2:
                $date = " Current Month (" . date('F') . ") Report";
                break;
            case 3:
                $date = " Report During ($frmdate To $todate)";
                break;
            case 4:
                $date = " Today's (" . date('d-m-Y') . ") Report";
                break;
            case 5:
                $date = "";
                break;
        }
        $report_title = $report_title . $date;
        return $report_title;
    }

    public function getReportTitle_forshopadded($frmdate = null, $todate = null) {
        extract($_POST);
        $report_title = '';

        switch ($selTest) {
            case 1:
                $monday = strtotime("last sunday");
                $monday = date('w', $monday) == date('w') ? $monday + 7 * 86400 : $monday;

                $sunday = strtotime(date("d-m-Y", $monday) . " +6 days");

                $this_week_sd = date("d-m-Y", $monday);
                $this_week_ed = date("d-m-Y", $sunday);
                $date = " Weekly (" . $this_week_sd . " TO " . $this_week_ed . ") Report";
                break;
            case 2:
                $date = " Current Month (" . date('F') . ") Report";
                break;
            case 3:
                $date = " Report During ($frmdate To $todate)";
                break;
            case 4:
                $date = " Today's (" . date('d-m-Y') . ") Report";
                break;
            case 5:
                $date = "";
                break;
        }
        $report_title = $report_title . $date;
        return $report_title;
    }

    //title for shop
    public function getReportTitleForShop($frmdate = null, $todate = null) {
        extract($_POST);
        $where = "";
        if (!empty($selshop)) {
            $where = " WHERE id= '$selshop' ";
        }
        $report_title = 'Sale Report For Shop : ';
        $sql = "SELECT id , name FROM tbl_shops  $where";
        $result1 = mysqli_query($this->local_connection, $sql);

        $i = 0;
        $row_count = mysqli_num_rows($result1);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result1)) {
                $report_title .= $row['name'];
            }
        } else {
            $report_title = 'No shop found';
        }

        return $report_title;
    }

    //new method for sp sales report productwise //ganeshfoods
    public function getAllOrders1() {

        extract($_POST);
        $selprod = $_POST['selprod'];
        $user_role = $_SESSION[SESSION_PREFIX . 'user_role'];
        $user_id = $_SESSION[SESSION_PREFIX . 'user_id'];
        $limit = '';
        $order_by = '';
        $search_name = '';
        if ($actionType == "excel") {
            $start = 0;
            if ($page == 0)
                $start = '';
            else {
                $start = ($per_page * ($page)) . ",";
            }
            $sort = explode(',', $sort_complete);
            $sort_by = '';
            if ($sort[0] == 0)
                $sort_by = ' Name ' . $sort[1];
            else if ($sort[0] == 1)
                $sort_by = ' Total_Sales ' . $sort[1];

            $order_by = ' ORDER BY ' . $sort_by;
            if ($per_page != -1)
                $limit = " LIMIT $start " . $per_page;
        }
        $condnsearch = "";
        $selperiod = $selTest;
        if ($selperiod != 0) {
            switch ($selperiod) {
                case 1:
                    $condnsearch = " AND yearweek(OA.order_date) = yearweek(curdate())";
                    //$condnsearch_sp=" AND yearweek(shop_visit_date_time) = yearweek(curdate())";
                    break;
                case 2:
                    $condnsearch = " AND date_format(OA.order_date, '%Y-%m')=date_format(now(), '%Y-%m') ";
                    //$condnsearch_sp=" AND date_format(shop_visit_date_time, '%Y-%m')=date_format(now(), '%Y-%m')";
                    break;
                case 3:
                    if ($todate != "")
                        $todat = $todate;
                    else
                        $todat = date("d-m-Y");

                    $condnsearch = " AND (date_format(OA.order_date, '%Y-%m-%d') >= STR_TO_DATE('" . $frmdate . "','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('" . $todat . "','%d-%m-%Y'))";
                    //  $condnsearch_sp=" AND (date_format(shop_visit_date_time, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(shop_visit_date_time, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
                    break;
                case 4:
                    $date = date('d-m-Y');
                    $condnsearch = " AND date_format(OA.order_date, '%d-%m-%Y') = '" . $date . "' ";
                    //  $condnsearch_sp=" AND date_format(shop_visit_date_time, '%d-%m-%Y') = '".$date."' ";
                    break;
                case 0:
                    $condnsearch = "";
                    break;
                case 5:
                    $condnsearch = "";
                    break;
                default:
                    $condnsearch = "";
                    break;
            }
        }
        $where_product = "";
        if ($selprod != '') {
            $where_product = " AND VO.product_id = $selprod ";
        }

        $where = "";
        switch ($user_role) {
            case "Admin":
                $where .= " ";
                break;
            case "Superstockist":
                $where .= " AND find_in_set('".$user_id."',parent_ids) <> 0 ";
                break;
            case "Distributor":
                $where .= " AND find_in_set('".$user_id."',parent_ids) <> 0 ";
                break;
        }


         $sql = "SELECT
                    OA.ordered_by, 
                    u.firstname,
                    sum(VO.product_quantity) as totalunit, 
                    sum(VO.p_cost_cgst_sgst) as Total_Sales
                FROM tbl_orders OA 
                LEFT JOIN tbl_user u ON  OA.ordered_by=u.id
                LEFT JOIN tbl_order_details VO ON OA.id = VO.order_id
                LEFT JOIN tbl_product TP ON VO.product_id = TP.id               
                WHERE u.user_role='SalesPerson'
                    $where $where_product $condnsearch 
                     group by u.id "; //date_format(OA.order_date, '%d-%m-%Y') = '".$frmdate."' //tbl_orders
                     //die();
        $result1 = mysqli_query($this->local_connection, $sql);

        $i = 0;
        $row_count = mysqli_num_rows($result1);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result1)) {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    }
    
    public function getSpwisesummary() {
        extract($_POST);
        $selprod = $_POST['selprod'];
        $dropdownSalesPerson = $_POST['dropdownSalesPerson'];       
        $user_role = $_SESSION[SESSION_PREFIX . 'user_role'];
        $user_id = $_SESSION[SESSION_PREFIX . 'user_id'];
        $limit = '';
        $order_by = '';
        $search_name = '';
        if ($actionType == "excel") {
            $start = 0;
            if ($page == 0)
                $start = '';
            else {
                $start = ($per_page * ($page)) . ",";
            }
            $sort = explode(',', $sort_complete);
            $sort_by = '';
            if ($sort[0] == 0)
                $sort_by = ' Name ' . $sort[1];
            else if ($sort[0] == 1)
                $sort_by = ' Total_Sales ' . $sort[1];

            $order_by = ' ORDER BY ' . $sort_by;
            if ($per_page != -1)
                $limit = " LIMIT $start " . $per_page;
        }
        $condnsearch = "";
        $selperiod = $selTest;
        if ($selperiod != 0) {
            switch ($selperiod) {
                case 1:
                    $condnsearch = " AND yearweek(OA.order_date) = yearweek(curdate())";
                    break;
                case 2:
                    $condnsearch = " AND date_format(OA.order_date, '%Y-%m')=date_format(now(), '%Y-%m')";
                    break;
                case 3:
                    if ($todate != "")
                        $todat = $todate;
                    else
                        $todat = date("d-m-Y");

                    $condnsearch = " AND (date_format(OA.order_date, '%Y-%m-%d') >= STR_TO_DATE('" . $frmdate . "','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('" . $todat . "','%d-%m-%Y'))";
                    break;
                case 4:
                    $date = date('d-m-Y');
                    $condnsearch = " AND date_format(OA.order_date, '%d-%m-%Y') = '" . $date . "' ";
                     break;
                case 0:
                    $condnsearch = "";
                    break;
                case 5:
                    $condnsearch = "";
                    break;
                default:
                    $condnsearch = "";
                    break;
            }
        }
        $sql = "SELECT
                    ts.id,ts.name as firstname,
                    sum(VO.product_quantity) as totalunit, 
                    sum(p_cost_cgst_sgst) as Total_Sales
                FROM tbl_shops ts 
                LEFT JOIN tbl_orders OA ON OA.shop_id = ts.id
                LEFT JOIN tbl_order_details VO ON OA.id = VO.order_id        
                WHERE OA.ordered_by='$dropdownSalesPerson'  $condnsearch 
                group by ts.id "; //date_format(OA.order_date, '%d-%m-%Y') = '".$frmdate."' //tbl_orders
                
        $result1 = mysqli_query($this->local_connection, $sql);

        $i = 0;
        $row_count = mysqli_num_rows($result1);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result1)) {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    }

    public function getDeliveryReport() {
        extract($_POST);
        $selprod = $_POST['selprod'];
        $user_role = $_SESSION[SESSION_PREFIX . 'user_role'];
        $user_id = $_SESSION[SESSION_PREFIX . 'user_id'];
        $limit = '';
        $order_by = '';
        $search_name = '';
        if ($actionType == "excel") {
            $start = 0;
            if ($page == 0)
                $start = '';
            else {
                $start = ($per_page * ($page)) . ",";
            }
            $sort = explode(',', $sort_complete);
            $sort_by = '';
            if ($sort[0] == 0)
                $sort_by = ' Name ' . $sort[1];
            else if ($sort[0] == 1)
                $sort_by = ' Total_Sales ' . $sort[1];

            $order_by = ' ORDER BY ' . $sort_by;
            if ($per_page != -1)
                $limit = " LIMIT $start " . $per_page;
        }
        $condnsearch = "";
        $selperiod = $selTest;
        if ($selperiod != 0) {
            switch ($selperiod) {
                case 1:
                    $condnsearch = " AND yearweek(OA.order_date) = yearweek(curdate())";
                    break;
                case 2:
                    $condnsearch = " AND date_format(OA.order_date, '%Y-%m')=date_format(now(), '%Y-%m')";
                    break;
                case 3:
                    if ($todate != ""){
                        $todat = $todate;
                    }
                    else{
                        $todat = date("d-m-Y");
                    }

                    $condnsearch = " AND (date_format(OA.order_date, '%Y-%m-%d') >= STR_TO_DATE('" . $frmdate . "','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('" . $todat . "','%d-%m-%Y'))";
                    break;
                case 4:
                    $date = date('d-m-Y');
                    $condnsearch = " AND date_format(OA.order_date, '%d-%m-%Y') = '" . $date . "' ";
                    break;
                case 0:
                    $condnsearch = "";
                    break;
                case 5:
                    $condnsearch = "";
                    break;
                default:
                    $condnsearch = "";
                    break;
            }
        }
        $where = "";
        switch ($user_role) {
            case "Admin":
                $where .= " ";
                break;
            case "Superstockist":
                $where .= " AND find_in_set('".$user_id."',parent_ids) <> 0 ";
                break;
            case "Distributor":
                $where .= " AND find_in_set('".$user_id."',parent_ids) <> 0 ";
                break;
        }

          $sql="SELECT OA.order_no,OA.order_from,OA.order_to,OA.delivery_person_id,OA.total_order_gst_cost,VO.order_status,VO.delivery_date,VO.delivery_assing_date,VO.signature_image,VO.amount_paid,VO.challan_no,
             udp.firstname as dp_fisrtname,ufrom.firstname as ufrom_firstname,
             uto.firstname as uto_firstname 
          FROM tbl_orders OA 
             LEFT JOIN tbl_order_details VO ON OA.id = VO.order_id 
             LEFT JOIN tbl_user udp ON OA.delivery_person_id = udp.id
             LEFT JOIN tbl_user ufrom ON OA.order_to = ufrom.id
             LEFT JOIN tbl_user uto ON OA.order_from = uto.id
              WHERE OA.delivery_person_id!=0 
               $where $condnsearch
              GROUP BY OA.order_no";

        $result1 = mysqli_query($this->local_connection, $sql);

        $i = 0;
        $row_count = mysqli_num_rows($result1);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result1)) {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    }
   public function get_all_shop_added_on_by() {
        extract($_POST);
        $user_type = $_SESSION[SESSION_PREFIX.'user_type'];
        $user_role = $_SESSION[SESSION_PREFIX.'user_role'];
        $seesion_user_id = $_SESSION[SESSION_PREFIX."user_id"];
        if ($user_role =='Accountant') 
        {
            $seesion_user_id =1;
        }
        $condnsearch="";
        $frmdate = date("Y-m-d", strtotime($frmdate));
        $todate = date("Y-m-d",strtotime($todate));
        $selperiod=$selTest;
        if($selperiod!=0){
            switch($selperiod){
                case 1:
                    $condnsearch=" AND yearweek(s.added_on) = yearweek(curdate())";
                break;
                case 2:
                    $condnsearch=" AND date_format(s.added_on, '%Y-%m')=date_format(now(), '%Y-%m')";
                break;
                case 3:
                    if($todate!="")
                        $todat=$todate;
                    else
                        $todat=date("Y-m-d");  
              $condnsearch=" AND date_format(s.added_on, '%Y-%m-%d') >= '".$frmdate."' AND date_format(s.added_on, '%Y-%m-%d') <= '".$todat."'";
                break;
                case 4:
                    $date = date('Y-m-d');
                    $condnsearch="AND date_format(s.added_on, '%Y-%m-%d') = '".$date."' ";
                break;
                case 0:
                    $condnsearch="";
                break;
                case 5:
                    $condnsearch="";
                break;
                default:
                    $condnsearch="";
                break;
            }
        }
        $where="";        
        if ($user_role !='Admin') 
        {
            $where.="WHERE 1=1 AND ( u.id='".$seesion_user_id."' OR find_in_set('".$seesion_user_id."',u.parent_ids) <> 0 ) ";
        }
        else
        {
             $where.="WHERE 1=1 ";   
        }
        $sql = "SELECT s.added_on AS added_date,u.id,u.firstname,u.user_type,
                    count(s.id) as totalunit,s.shop_added_by 
                FROM tbl_shops s 
                left join tbl_user u on u.id=s.shop_added_by  
                 $where $condnsearch 
                     group by  date_format(s.added_on, '%Y-%m-%d'),s.shop_added_by";
        $result1 = mysqli_query($this->local_connection,$sql);        
        $i = 0;
        $row_count = mysqli_num_rows($result1);
        if($row_count > 0){
            while($row = mysqli_fetch_assoc($result1))
            {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        }else{
            return 0;
        }
    }
    public function get_all_shop_list_added_on_by($userid,$added_date){ 
        $condnsearch=" AND date_format(s.added_on, '%Y-%m-%d') = '".$added_date."' ";
        $sql = "SELECT s.added_on AS added_date,s.name,u.id,u.firstname,u.user_type,s.contact_person,s.mobile,s.address,s.shop_added_by 
                FROM tbl_shops s 
                left join tbl_user u on u.id=s.shop_added_by  
                WHERE s.shop_added_by='$userid' $condnsearch ";
        $result1 = mysqli_query($this->local_connection,$sql);        
        $i = 0;
        $row_count = mysqli_num_rows($result1);
        if($row_count > 0){
            while($row = mysqli_fetch_assoc($result1))
            {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        }else{
            return 0;
        }
    }

    //new method for shop sales report  //ganeshfoods
    public function getAllOrders2() {
        extract($_POST);
        $selshop = $_POST['selshop'];

        $user_type = $_SESSION[SESSION_PREFIX . 'user_type'];
        $luser_id = $_SESSION[SESSION_PREFIX . 'user_id'];
        $user_role = $_SESSION[SESSION_PREFIX.'user_role'];
        if ($user_role =='Accountant') 
        {
                $luser_id =1;
        }
        $where = "";
        if ($user_type == 'Superstockist') {
            $where = " AND OA.superstockistid ='$luser_id' ";
        }
        if ($user_type == 'Distributor') {
            $where = " AND OA.distributorid='$luser_id' ";
        }

        $sql = "SELECT                  
                    TP.id as id,    
                    TP.productname as productname,
                    date_format(OA.order_date, '%M') as month,
                    VO.product_variant_weight1,
                    VO.p_cost_cgst_sgst,
                    VO.product_variant_unit1 as unit, 
                    VO.product_quantity as variantunit, 
                    VO.product_unit_cost as unitcost,
                    VO.product_variant_id
                FROM tbl_shops AS TS
                LEFT JOIN tbl_orders OA ON OA.shop_id = TS.id                           
                LEFT JOIN tbl_order_details VO ON OA.id = VO.order_id
                LEFT JOIN tbl_product TP ON VO.product_id = TP.id 
                WHERE OA.shop_id = $selshop
                $where ORDER BY `TP`.`productname`"; //date_format(OA.order_date, '%d-%m-%Y') = '".$frmdate."' //FROM tbl_product TP 

        $result1 = mysqli_query($this->local_connection, $sql);

        $i = 0;
        $row_count = mysqli_num_rows($result1);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result1)) {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    }
     
    public function getAllOrdersSummary_total() {
        // echo "".$user_role;
        extract($_POST);
        if($reportType=='Rolewise'){
            $report_type_role = $_POST['report_type_role'];
        }
        if($reportType=='Typewise'){
            $thePostIdArray = explode(',', $_POST['report_type_usertype']);
            $report_type_usertype=$thePostIdArray[0];
            $report_type_role=$thePostIdArray[1];
        }
       
        
        $user_role = $_SESSION[SESSION_PREFIX.'user_role'];
        $user_id = $_SESSION[SESSION_PREFIX.'user_id'];
        if ($user_role =='Accountant') 
        {
                $user_id =1;
        }

        $limit = '';
        $order_by = '';
        $search_name = '';
        if ($actionType == "excel") {
            $start = 0;
            if ($page == 0)
                $start = '';
            else {
                $start = ($per_page * ($page)) . ",";
            }
            $sort = explode(',', $sort_complete);
            $sort_by = '';
            if ($sort[0] == 0)
                $sort_by = ' Name ' . $sort[1];
            else if ($sort[0] == 1)
                $sort_by = ' Total_Sales ' . $sort[1];

            $order_by = ' ORDER BY ' . $sort_by;
            if ($per_page != -1)
                $limit = " LIMIT $start " . $per_page;
        }
        $condnsearch = "";
        $selperiod = $selTest;
        if ($selperiod != 0) {
            switch ($selperiod) {
                case 1:
                    $condnsearch = " AND yearweek(o.order_date) = yearweek(curdate())";
                    $condnsearch_sp = " AND yearweek(sv.shop_visit_date_time) = yearweek(curdate())";
                    break;
                case 2:
                    $condnsearch = " AND date_format(o.order_date, '%Y-%m')=date_format(now(), '%Y-%m')";
                    $condnsearch_sp = " AND date_format(sv.shop_visit_date_time, '%Y-%m')=date_format(now(), '%Y-%m')";
                    break;
                case 3:
                    if ($todate != "")
                        $todat = $todate;
                    else
                        $todat = date("d-m-Y");

                    $condnsearch = " AND (date_format(o.order_date, '%Y-%m-%d') >= STR_TO_DATE('" . $frmdate . "','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('" . $todat . "','%d-%m-%Y'))";
                    $condnsearch_sp = " AND (date_format(sv.shop_visit_date_time, '%Y-%m-%d') >= STR_TO_DATE('" . $frmdate . "','%d-%m-%Y') AND date_format(shop_visit_date_time, '%Y-%m-%d') <= STR_TO_DATE('" . $todat . "','%d-%m-%Y'))";
                    break;
                case 4:
                    $date = date('d-m-Y');
                    $condnsearch = " AND date_format(o.order_date, '%d-%m-%Y') = '" . $date . "' ";
                    $condnsearch_sp = " AND date_format(sv.shop_visit_date_time, '%d-%m-%Y') = '" . $date . "' ";
                    break;
                case 0:
                    $condnsearch = "";
                    break;
                case 5:
                    $condnsearch = "";
                    break;
                default:
                    $condnsearch = "";
                    break;
            }
        }
        
        
        $user_sp=" ";
        if($reportType=='Rolewise'||$reportType=='Typewise'){
            $user_condition="";
            $user_condition.=" AND find_in_set('".$user_id."',u.parent_ids) <> 0 ";
            if($reportType=='Rolewise'){
                $user_condition.=" AND u.user_role='".$report_type_role."' ";
            }else{
                $user_condition.=" AND u.user_type='".$report_type_usertype."' ";
            }           
            if($report_type_role=='SalesPerson'){
                $user_sp .=" ,count(DISTINCT o.shop_id) AS shop_order,
                                (select count(shop_id) 
                                from tbl_shop_visit AS sv                           
                                WHERE u.id = sv.salesperson_id $condnsearch_sp) 
                                AS shop_no_order ";
                    $sql="select u.id,u.firstname as Name,sum(od.p_cost_cgst_sgst) as Total_Sales $user_sp
                    from tbl_user u 
                    left join tbl_orders o on u.id=o.order_from   
                    LEFT JOIN tbl_order_details od ON od.order_id =o.id  
                    where 1=1 $user_condition $condnsearch
                    group by u.id";
            }else{
                $sql="select u.id,u.firstname as Name,sum(od.p_cost_cgst_sgst) as Total_Sales $user_sp
                from tbl_user u 
                left join tbl_orders o on u.id=o.order_to   
                LEFT JOIN tbl_order_details od ON od.order_id =o.id  
                where 1=1 $user_condition $condnsearch
                group by u.id"; 
            }
        }else if($reportType=='Productwise'){
                    
            $sql="select p.id,p.productname as Name,sum(od.p_cost_cgst_sgst) as Total_Sales $user_sp
                from tbl_product p 
                left join tbl_order_details od on p.id=od.product_id  
                LEFT JOIN tbl_orders o ON od.order_id =o.id  
                where 1=1   $condnsearch AND shop_id is not null 
                group by p.id"; 
        }else{//Shopwise
            
            $sql="select s.id,s.name as Name,sum(od.p_cost_cgst_sgst) as Total_Sales $user_sp
                from tbl_shops s 
                left join tbl_orders o on s.id=o.shop_id   
                LEFT JOIN tbl_order_details od ON od.order_id =o.id  
                where 1=1   $condnsearch AND shop_id is not null 
                group by s.id"; 
        }

        $result1 = mysqli_query($this->local_connection, $sql);
        $i = 1;
        $row_count = mysqli_num_rows($result1);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result1)) {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    }
    //deleted method getAllOrdersSummaryByLocation not in use 17 april order_summary_details6.php
    //and deleted two files also.
    

   public function getSPLeavestatus($sp_id2, $tdate2) {
        $sql = "SELECT tdate FROM tbl_sp_attendance where sp_id= $sp_id2 AND tdate = '$tdate2' and presenty!='1' ";
        $result1 = mysqli_query($this->local_connection, $sql);
        $row_count = mysqli_num_rows($result1);
        if ($row_count > 0) {
            return $result1;
        } else
            return $row_count;
    }

    public function getSPattendance() {
        extract($_POST);
        $dropdownSalesPerson = $_POST['dropdownSalesPerson'];
        $user_role = $_SESSION[SESSION_PREFIX . 'user_role'];
        $user_id = $_SESSION[SESSION_PREFIX . 'user_id'];
        if ($user_role =='Accountant') 
        {
                $user_id =1;
        }
        $frmdate = date("Y-m-d", strtotime($frmdate));
        $todate = date("Y-m-d",strtotime($todate));

        $limit = '';
        $order_by = '';
        $search_name = '';
        if ($actionType == "excel") {
            $start = 0;
            if ($page == 0)
                $start = '';
            else {
                $start = ($per_page * ($page)) . ",";
            }
            $sort = explode(',', $sort_complete);
            $sort_by = '';
            if ($sort[0] == 0)
                $sort_by = ' Name ' . $sort[1];
            else if ($sort[0] == 1)
                $sort_by = ' Total_Sales ' . $sort[1];

            $order_by = ' ORDER BY ' . $sort_by;
            if ($per_page != -1)
                $limit = " LIMIT $start " . $per_page;
        }
        $condnsearch = "";
        $selperiod = $selTest;
        if ($selperiod != 0) {
            switch ($selperiod) {
                case 1:
                    $condnsearch = " AND yearweek(sa.tdate) = yearweek(curdate())";
                    //$condnsearch_sp=" AND yearweek(shop_visit_date_time) = yearweek(curdate())";
                    break;
                case 2:
                    $condnsearch = " AND date_format(sa.tdate, '%Y-%m')=date_format(now(), '%Y-%m')";
                    break;
                case 3:
                    if ($todate != "")
                        $todat = $todate;
                    else
                  $todat=date("Y-m-d");  
                 $condnsearch=" AND date_format(sa.tdate, '%Y-%m-%d') >= '".$frmdate."' AND date_format(sa.tdate, '%Y-%m-%d') <= '".$todat."'";
                    //  $condnsearch_sp=" AND (date_format(shop_visit_date_time, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(shop_visit_date_time, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
                    break;
                case 4:
                    $date = date("Y-m-d");
                    $condnsearch = " AND date_format(sa.tdate, '%Y-%m-%d') = '" . $date . "'";
                    break;
                case 0:
                    $condnsearch = "";
                    break;
                case 5:
                    $condnsearch = "";
                    break;
                default:
                    $condnsearch = "";
                    break;
            }
        }
        $where = "";
         if ($user_role !='Admin') 
        {
            $where.="WHERE 1=1 AND find_in_set('".$user_id."',parent_ids) <> 0 ";
        }
        else
        {
             $where.="WHERE 1=1 ";   
        }
        if ($dropdownSalesPerson != '') {
            $where.= "AND sa.sp_id = $dropdownSalesPerson ";
        }
       
     $sql = "SELECT (UNIX_TIMESTAMP(sa.dayendtime) - UNIX_TIMESTAMP(sa.tdate)) / 60.0 / 60.0 as hours_difference,
u.id,sa.id, sa.tdate, sa.sp_id, sa.todays_travelled_distance, sa.dayendtime ,sa.presenty,u.firstname
 from tbl_sp_attendance sa
LEFT JOIN tbl_user u ON sa.sp_id = u.id $where $condnsearch ";
        $result1 = mysqli_query($this->local_connection, $sql);
        $i = 0;
        $row_count = mysqli_num_rows($result1);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result1)) {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    }

    public function getReportTitleForSP($selTest = null, $frmdate = null, $todate = null) {
        $report_title = '';
        switch ($selTest) {
            case 1:
                $monday = strtotime("last sunday");
                $monday = date('w', $monday) == date('w') ? $monday + 7 * 86400 : $monday;

                $sunday = strtotime(date("d-m-Y", $monday) . " +6 days");

                $this_week_sd = date("d-m-Y", $monday);
                $this_week_ed = date("d-m-Y", $sunday);
                $date = " Weekly (" . $this_week_sd . " TO " . $this_week_ed . ") Report";
                break;
            case 2:
                $date = " Current Month (" . date('F') . ") Report";
                break;
            case 3:
                $frmdate = date('d-m-Y', $frmdate);
                $todate = date('d-m-Y', $todate);
                $date = " Report During ($frmdate To $todate)";
                break;
            case 4:
                $date = " Today's (" . date('d-m-Y') . ") Report";
                break;
            case 5:
                $date = "";
                break;
        }
        $report_title = $report_title . $date;
        return $report_title;
    }

    function getSPShopOrdersSummary($sp_id, $filter_date, $date1 = null, $date2 = null) {
        //echo "fsgjdhfgj";//die();
        if ($filter_date != 0) {
            switch ($filter_date) {
                case 1:
                    $condnsearch = " AND yearweek(order_date) = yearweek(curdate())";
                    break;
                case 2:
                    $condnsearch = " AND date_format(order_date, '%Y-%m')=date_format(now(), '%Y-%m')";
                    break;
                case 3:
                    $frmdate = date('d-m-Y', $date1);
                    $todate = date('d-m-Y', $date2);
                    if ($todate != "")
                        $todat = $todate;
                    else
                        $todat = date("d-m-Y");

                    $condnsearch = " AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('" . $frmdate . "','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('" . $todat . "','%d-%m-%Y'))";
                    break;
                case 4:
                    $date = date('d-m-Y');
                    $condnsearch = " AND date_format(order_date, '%d-%m-%Y') = '" . $date . "' ";
                    break;
                case 0:
                    $condnsearch = "";
                    break;
                case 5:
                    $condnsearch = "";
                    break;
                default:
                    $condnsearch = "";
                    break;
            }
        }
        
          $sql = "SELECT s.name as shop_name,s.id as shop_id,sum(vo.p_cost_cgst_sgst) as Total_Sales                
                FROM tbl_orders oa                
                LEFT JOIN tbl_user u  ON oa.ordered_by = u.id 
                LEFT JOIN tbl_shops AS s ON s.id = oa.shop_id
                LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id)
                WHERE u.id = $sp_id " . $condnsearch . "
                GROUP BY s.name ORDER BY s.name";
        //echo $sql;die();
        $result1 = mysqli_query($this->local_connection, $sql);
        $i = 1;
        $row_count = mysqli_num_rows($result1);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_array($result1)) {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    }

    function getSPShopOrders($sp_id, $s_id, $filter_date, $date1 = null, $date2 = null) {
        if ($filter_date != 0) {
            switch ($filter_date) {
                case 1:
                    $condnsearch = " AND yearweek(order_date) = yearweek(curdate())";
                    break;
                case 2:
                    $condnsearch = " AND date_format(order_date, '%Y-%m')=date_format(now(), '%Y-%m')";
                    break;
                case 3:
                    $frmdate = date('d-m-Y', $date1);
                    $todate = date('d-m-Y', $date2);
                    if ($todate != "")
                        $todat = $todate;
                    else
                        $todat = date("d-m-Y");

                    $condnsearch = " AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('" . $frmdate . "','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('" . $todat . "','%d-%m-%Y'))";
                    break;
                case 4:
                    $date = date('d-m-Y');
                    $condnsearch = " AND date_format(order_date, '%d-%m-%Y') = '" . $date . "' ";
                    break;
                case 0:
                    $condnsearch = "";
                    break;
                case 5:
                    $condnsearch = "";
                    break;
                default:
                    $condnsearch = "";
                    break;
            }
        }
         $sql = "SELECT vo.product_variant_id, vo.order_id,pvr.brand_name AS brandnm,
        pvr.product_name AS product_name,
        pvr.category_name as categorynm, vo.product_quantity, vo.p_cost_cgst_sgst,
        vo.product_variant_weight1,vo.product_variant_unit1, 
        vo.product_variant_weight2,vo.product_variant_unit2, 
        vo.id as variant_oder_id, oa.order_date, vo.order_id, vo.product_id, vo.campaign_sale_type
                FROM tbl_orders oa              
                LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id)
                LEFT JOIN pcatbrandvariant1 pvr ON (vo.product_variant_id = pvr.product_variant_id)              
                WHERE oa.ordered_by = '".$sp_id."' 
                AND  oa.shop_id = '".$s_id."' " . $condnsearch . " 
                ORDER BY vo.id DESC";
        $result1 = mysqli_query($this->local_connection, $sql);
        $i = 1;
        $row_count = mysqli_num_rows($result1);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_array($result1)) { 
                $dimentionDetails=$row['product_variant_weight1']."
                ".$row['product_variant_unit1']."
                ".$row['product_variant_weight2']."
                ".$row['product_variant_unit2'];
                $row['prodname_variant'] = $row['product_name'] . $dimentionDetails;
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    }

    public function no_order_history_details($sp_id = null, $filter_date = null, $date1 = null, $date2 = null) {
        $user_role = $_SESSION[SESSION_PREFIX . 'user_role'];
        $userid = $_SESSION[SESSION_PREFIX . 'user_id'];
        $child_sql = "SELECT id FROM `tbl_user` 
        WHERE  find_in_set('".$userid."',parent_ids) <> 0"; 
        $child_result = mysqli_query($this->local_connection, $child_sql);
        $row_child_count = mysqli_num_rows($child_result);
        $all_childs = array();$i=0;
        $all_childs[$i]=$userid;$i=$i+1;
        if ($row_child_count > 0) {
            while ($row_childs = mysqli_fetch_assoc($child_result)) {               
                $all_childs[$i] = $row_childs['id'];  $i++;             
            }
        }
        $order_from_ids=implode(',',array_unique($all_childs));
        $where_clause_outer='';
        if ($order_status == 0) {
            $where_clause_outer .= " AND find_in_set(tbl_shop_visit.salesperson_id,'".$order_from_ids."') <> 0" ;
        }
       
       $sql1 = "SELECT tbl_shop_visit.id, `shop_id`, `salesperson_id`, `shop_visit_date_time`, `shop_visit_reason`, `shop_close_reason_type`, `shop_close_reason_details`
                    FROM tbl_shop_visit 
                    INNER JOIN tbl_shops as s ON s.id = shop_id 
                    LEFT JOIN tbl_user as u ON u.id = shop_added_by
                    WHERE 1=1 $where_clause_outer 
                    AND shop_id != 0 AND salesperson_id !=0 
                    ORDER BY id DESC";
               
        $result1 = mysqli_query($this->local_connection, $sql1);
        $row_count = mysqli_num_rows($result1);
        if ($row_count > 0) {
            return $result1;
        } else
            return $row_count;
    }

    function getSalesPOrders_old($date, $salespid) {
        $sql = "SELECT  DISTINCT(VO.order_id), OA.shop_id, shops.name as shopname, shops.address, shops.mobile,
                OA.order_date , OA.categorynm, oplacelat, oplacelon, VO.order_id,
                (SELECT name FROM tbl_city WHERE id = shops.city) AS cityname,
                (SELECT name FROM tbl_state WHERE id = shops.state) AS statename
                FROM tbl_orders OA 
                LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
                LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id                
                WHERE
                    date_format(OA.order_date, '%d-%m-%Y') = '" . $date . "' AND OA.ordered_by = " . $salespid . "
                GROUP BY VO.order_id,
                OA.order_date
                ORDER BY OA.order_date ASC";
//GROUP BY OA.shop_id
        $result = mysqli_query($this->local_connection, $sql);
        return $result;
    }

    function getSalesPOrders($date, $salespid) {
        echo  $sql = "(
                        SELECT  DISTINCT(VO.order_id), OA.shop_id, shops.name as shopname, 
                        shops.address, shops.mobile, OA.order_date , c.categorynm, 
                        VO.cat_id,OA.lat AS oplacelat, OA.long AS oplacelon,SV.no_o_lat,SV.no_o_long,
						sum(VO.product_quantity) as sum_product_quantity,OA.total_order_gst_cost as sum_total_order_gst_cost,
                        (SELECT name FROM tbl_city WHERE id = shops.city) AS cityname,
                        (SELECT name FROM tbl_state WHERE id = shops.state) AS statename, 
                        OA.order_date AS date,
                        SV.shop_visit_reason, SV.shop_close_reason_type, SV.shop_close_reason_details
                        FROM tbl_orders OA 
                        LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
                        LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id
                        LEFT JOIN tbl_category c ON c.id= VO.cat_id         
                        LEFT OUTER JOIN  tbl_shop_visit AS SV ON SV.salesperson_id = OA.ordered_by AND OA.order_date =SV.shop_visit_date_time
                        WHERE date_format(OA.order_date, '%d-%m-%Y') = '$date' AND OA.ordered_by = $salespid AND VO.order_id!=''
                        GROUP BY VO.order_id
                        ORDER BY OA.order_date ASC
                    ) UNION  ALL
                    (
						SELECT 'oid',SV.shop_id,
						(SELECT name FROM tbl_shops WHERE id = SV.shop_id) AS shopname, 
						shops.address, shops.mobile,SV.shop_visit_date_time AS order_date,
						'cname','catid',SV.no_o_lat AS oplacelat ,SV.no_o_long AS oplacelon,
						SV.no_o_lat,SV.no_o_long, 'sum_product_quantity','sum_total_order_gst_cost',
						(SELECT name FROM tbl_city WHERE id = shops.city) AS cityname, 
						(SELECT name FROM tbl_state WHERE id = shops.state) AS statename, 
						SV.shop_visit_date_time AS date, SV.shop_visit_reason, 
						SV.shop_close_reason_type, SV.shop_close_reason_details 
						FROM tbl_shop_visit AS SV 
						LEFT JOIN tbl_shops shops ON shops.id= SV.shop_id 
						WHERE date_format(SV.shop_visit_date_time, '%d-%m-%Y') = '$date' 
						AND SV.salesperson_id = $salespid  ORDER BY SV.shop_visit_date_time ASC
					)
                    ORDER BY date ASC";
//GROUP BY OA.shop_id
        $result = mysqli_query($this->local_connection, $sql);
        return $result;
    }

    function getSalesPOrdersProducts($date, $salespid) {
        $sql = "SELECT DISTINCT(OV.product_variant_id), b.name, c.categorynm,OV.brand_id,OV.cat_id,
        (SELECT  productname FROM `tbl_product` WHERE id = OV.product_id) AS productname
        FROM `tbl_orders` AS OA 
        LEFT JOIN `tbl_order_details` AS OV ON OV.order_id = OA.id 
        LEFT JOIN `tbl_brand` AS b ON b.id = OV.brand_id
        LEFT JOIN `tbl_category` AS c ON c.id= OV.cat_id 
        WHERE
        date_format(OA.order_date, '%d-%m-%Y') = '" . $date . "' AND OA.ordered_by = " . $salespid;

        $result = mysqli_query($this->local_connection, $sql);
        return $result;
    }

    function getSPStartLocationPoints($date, $salespid) {
        /* echo $sql = "SELECT *,MIN(id) FROM `tbl_user_location` AS OA WHERE
          date_format(OA.tdate, '%d-%m-%Y') = '".$date."' AND OA.userid = " . $salespid; */
        /*  $sql = "SELECT * FROM `tbl_user_location` AS OA WHERE date_format(OA.tdate, '%d-%m-%Y') = '".$date."' AND OA.userid = '".$salespid."' ORDER BY OA.id ASC LIMIT 0, 1"; */

         $sql = "SELECT OA.lattitude,OA.longitude,SA.tdate AS start_date_time FROM `tbl_user_location` AS OA 
LEFT JOIN `tbl_sp_attendance` AS SA ON SA.sp_id = OA.userid  
WHERE date_format(OA.tdate, '%d-%m-%Y') = '" . $date . "' AND date_format(SA.tdate, '%d-%m-%Y') = '" . $date . "' AND  OA.userid = '" . $salespid . "' ORDER BY OA.id ASC LIMIT 0, 1";

        $result = mysqli_query($this->local_connection, $sql);
        return $result;
    }

    function getSPEndLocationPoints($date, $salespid) {
        /* $sql = "SELECT *,MAX(id) FROM `tbl_user_location` AS OA WHERE
          date_format(OA.tdate, '%d-%m-%Y') = '".$date."' AND OA.userid = " . $salespid; */
        /* $sql = "SELECT * FROM `tbl_user_location` AS OA WHERE date_format(OA.tdate, '%d-%m-%Y') = '".$date."' AND OA.userid = '".$salespid."' ORDER BY OA.id DESC LIMIT 0, 1";    */
        $sql = "SELECT OA.lattitude,OA.longitude,SA.dayendtime AS end_date_time FROM `tbl_user_location` AS OA 
LEFT JOIN `tbl_sp_attendance` AS SA ON SA.sp_id = OA.userid  
WHERE date_format(OA.tdate, '%d-%m-%Y') = '" . $date . "' AND date_format(SA.dayendtime, '%d-%m-%Y') = '" . $date . "' AND  OA.userid = '" . $salespid . "' ORDER BY OA.id DESC LIMIT 0, 1";
        $result = mysqli_query($this->local_connection, $sql);
        return $result;
    }

    function getSPLocationPoints($date, $salespid) {
         $sql = "SELECT * FROM `tbl_user_location` AS OA WHERE
        date_format(OA.tdate, '%d-%m-%Y') = '" . $date . "' AND OA.userid = " . $salespid;
        $result = mysqli_query($this->local_connection, $sql);
        return $result;
    }

    function getSalesPOrdersPQuantity($shop_id, $product_variant_id, $order_date) {
        $sql = "SELECT OV.product_quantity AS quantity, OV.p_cost_cgst_sgst AS totalcost, OV.product_quantity AS variantunit FROM `tbl_orders` AS OA LEFT JOIN `tbl_order_details` AS OV ON OV.order_id = OA.id
        WHERE
        OV.product_variant_id = " . $product_variant_id . " AND OA.shop_id = " . $shop_id . " AND OA.order_date = '" . $order_date . "'";

        $result = mysqli_query($this->local_connection, $sql);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }

    function getSalesPOrdersDetails($orderRecId) {
        $sql = "SELECT  shops.name as shopname, shops.address, shops.mobile,
                OA.order_date , OA.categorynm,
                (SELECT name FROM tbl_city WHERE id = shops.city) AS cityname,
                (SELECT name FROM tbl_state WHERE id = shops.state) AS statename
                FROM tbl_orders OA 
                LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id           
                WHERE
                    date_format(OA.order_date, '%d-%m-%Y') = '" . $date . "' AND OA.ordered_by = " . $salespid . "
                ORDER BY OA.order_date ASC";

        $result = mysqli_query($this->local_connection, $sql);
        return $result;
    }

    function getSalesPOrdersStart_old($date, $salespid) {
        $sql = "SELECT  OA.order_date , VO.id, shops.name as shopname, shops.address, OA.oplacelat, OA.oplacelon
                FROM tbl_orders OA 
                LEFT JOIN tbl_order_details VO ON OA.id = VO.order_id 
                LEFT JOIN tbl_shops shops ON shops.id= VO.shopid            
                WHERE
                    date_format(OA.order_date, '%d-%m-%Y') = '" . $date . "' AND OA.ordered_by = " . $salespid . "
                ORDER BY OA.order_date ASC LIMIT 1";

        $result = mysqli_query($this->local_connection, $sql);
        $record = mysqli_fetch_assoc($result);
        return $record;
    }

    function getSalesPOrdersEnd_old($date, $salespid) {
        $sql = "SELECT  OA.order_date, VO.id, shops.name as shopname, shops.address, OA.oplacelat, OA.oplacelon
                FROM tbl_orders OA 
                LEFT JOIN tbl_order_details VO ON OA.id = VO.order_id 
                LEFT JOIN tbl_shops shops ON shops.id= VO.shopid            
                WHERE
                    date_format(OA.order_date, '%d-%m-%Y') = '" . $date . "' AND OA.ordered_by = " . $salespid . "
                ORDER BY OA.order_date DESC LIMIT 1";

        $result = mysqli_query($this->local_connection, $sql);
        $record = mysqli_fetch_assoc($result);
        return $record;
    }

    function getSalesPOrdersStart($date, $salespid) {
        $sql = "(
                        SELECT  VO.id, shops.name as shopname, shops.address, OA.lat AS oplacelat, OA.long AS oplacelon,
                        OA.order_date AS date
                        FROM tbl_orders OA 
                        LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
                        LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id        
                        LEFT OUTER JOIN  tbl_shop_visit AS SV ON SV.shop_id = OA.shop_id  AND OA.order_date =SV.shop_visit_date_time
                        WHERE date_format(OA.order_date, '%d-%m-%Y') = '$date' AND OA.ordered_by = " . $salespid . "
                        GROUP BY VO.order_id,
                        OA.order_date
                        ORDER BY OA.order_date ASC
                    )
                    UNION  ALL
                    (
                        SELECT VO.id,
                        (SELECT name FROM tbl_shops WHERE id = SV.shop_id) AS shopname, 
                        shops.address, OA.lat AS oplacelat, OA.long AS oplacelon,
                        SV.shop_visit_date_time AS date     
                        FROM tbl_shop_visit AS SV
                        LEFT OUTER JOIN  tbl_orders OA  ON OA.shop_id = SV.shop_id  AND OA.order_date =SV.shop_visit_date_time                      
                        LEFT JOIN tbl_shops shops ON shops.id= SV.shop_id
                        LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id    
                        WHERE date_format(SV.shop_visit_date_time, '%d-%m-%Y') = '$date' AND SV.salesperson_id = " . $salespid . "
                        ORDER BY SV.shop_visit_date_time ASC
                    )
                    ORDER BY date ASC LIMIT 1";

        $result = mysqli_query($this->local_connection, $sql);
        $record = mysqli_fetch_assoc($result);
        return $record;
    }

    function getSalesPOrdersEnd($date, $salespid) {
        $sql = "(
                        SELECT  OA.id AS oid, VO.id, shops.name as shopname, shops.address, OA.lat AS oplacelat, OA.long AS oplacelon,
                        OA.order_date AS date
                        FROM tbl_orders OA 
                        LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
                        LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id        
                        LEFT OUTER JOIN  tbl_shop_visit AS SV ON SV.shop_id = OA.shop_id  AND OA.order_date =SV.shop_visit_date_time
                        WHERE date_format(OA.order_date, '%d-%m-%Y') = '$date' AND OA.ordered_by = $salespid
                        GROUP BY VO.order_id,
                        OA.order_date
                        ORDER BY OA.id DESC, OA.order_date DESC
                    )
                    UNION  ALL
                    (
                        SELECT OA.id AS oid, VO.id,
                        (SELECT name FROM tbl_shops WHERE id = SV.shop_id) AS shopname, 
                        shops.address, OA.lat AS oplacelat, OA.long AS oplacelon,
                        SV.shop_visit_date_time AS date     
                        FROM tbl_shop_visit AS SV
                        LEFT OUTER JOIN  tbl_orders OA  ON OA.shop_id = SV.shop_id  AND OA.order_date =SV.shop_visit_date_time                      
                        LEFT JOIN tbl_shops shops ON shops.id= SV.shop_id
                        LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id    
                        WHERE date_format(SV.shop_visit_date_time, '%d-%m-%Y') = '$date' AND SV.salesperson_id = $salespid
                        ORDER BY SV.id DESC, SV.shop_visit_date_time DESC
                    )
                    ORDER BY date DESC, oid DESC  LIMIT 1";

        $result = mysqli_query($this->local_connection, $sql);
        $record = mysqli_fetch_assoc($result);
        return $record;
    }

    function getSalesPStartEndDay($date, $salespid) {
        $sql_day_time = "SELECT `sp_id`, `tdate`, `presenty`, `dayendtime`
                FROM tbl_sp_attendance
                WHERE
                date_format(tdate, '%d-%m-%Y') = '" . $date . "' AND sp_id = " . $salespid;

        $result = mysqli_query($this->local_connection, $sql_day_time);
        $totalRecords = mysqli_num_rows($result);
        if ($totalRecords > 0)
            return mysqli_fetch_array($result);
        else
            return $totalRecords;
    }

    function getSProductVariant($product_variant_id) {
        $sqlprd = "SELECT variant_1, variant_2 FROM `tbl_product_variant` WHERE id = '$product_variant_id' ";
        $resultprd = mysqli_query($this->local_connection, $sqlprd);
        $rowprd = mysqli_fetch_array($resultprd);
        $exp_variant1 = $rowprd['variant_1'];
        $imp_variant1 = explode(',', $exp_variant1);
        $exp_variant2 = $rowprd['variant_2'];
        $imp_variant2 = explode(',', $exp_variant2);
        $sql = "SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
        $resultunit = mysqli_query($this->local_connection, $sql);
        $rowunit = mysqli_fetch_array($resultunit);
        $variant_unit1 = $rowunit['unitname'];
        $sql = "SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant2[1]'";
        $resultunit = mysqli_query($this->local_connection, $sql);
        $rowunit = mysqli_fetch_array($resultunit);
        $variant_unit2 = $rowunit['unitname'];
        $dimentionDetails = "";
        /* if($imp_variant1[0]!="") {
          $dimentionDetails = " Pcs:  " .$imp_variant1[0] . " " . $variant_unit1;;//  $imp_variant1[0] //$variant_unit1;
          } */
        if ($variant_unit1 != "") {
            $dimentionDetails = " " . $imp_variant1[0] . " " . $variant_unit1; //  $imp_variant1[0] //$variant_unit1;
        }
        //  $dimentionDetails .= " - Weight: " .  $imp_variant2[0] . " " . $variant_unit2;
        return $dimentionDetails;
    }

    function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Km') {
        /* $latitude1 ='18.520430';
          $longitude1 ='73.856744';

          $latitude2 ='19.075984';
          $longitude2 ='72.877656'; Mumabi pune co-ordinates */

        $theta = $longitude1 - $longitude2;
        $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
        $distance = acos($distance);
        $distance = rad2deg($distance);
        $distance = $distance * 60 * 1.1515;
        switch ($unit) {
            case 'Mi': break;
            case 'Km' : $distance = $distance * 1.609344;
        }
        return (round($distance, 2));
    }

    function getDistanceBetweenPoints($lat1, $lat2, $long1, $long2) {
        //echo $lat1;
        //exit();
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $long1 . "&destinations=" . $lat2 . "," . $long2 . "&key=AIzaSyCDMpdO43txdg_zyovvZm9i1tMMFIQTKTU&mode=driving&language=pl-PL";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
        $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
        return array('distance' => $dist, 'time' => $time);
    } 
    /* function getDistanceBetweenPoints($latitudeFrom, $latitudeTo, $longitudeFrom,  $longitudeTo, $earthRadius = 6371000){
      // convert from degrees to radians
      $latFrom = deg2rad($latitudeFrom);
      $lonFrom = deg2rad($longitudeFrom);
      $latTo = deg2rad($latitudeTo);
      $lonTo = deg2rad($longitudeTo);

      $latDelta = $latTo - $latFrom;
      $lonDelta = $lonTo - $lonFrom;

      $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
        cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        $dist = $angle * $earthRadius;
         return array('distance' => $dist, 'time' => '');     
    } */

    public function getLocation($latitude, $longitude) {
        $latitude = $latitude;
        $longitude = $longitude;
        $geolocation = $latitude . "," . $longitude;
        //$geolocation = $latitude.",".$longitude;//"18.4773911,73.9025829";//
        $request = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$geolocation&key=AIzaSyCDMpdO43txdg_zyovvZm9i1tMMFIQTKTU&sensor=false";
        //echo $request;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);

        //$output = file_get_contents($request);
        $json_decode = json_decode($output);
        //print"<pre>";
       // print_R($json_decode);
        $address = "";
        if (isset($json_decode->results[0])) {
            $response = array();
            $address = $json_decode->results[0]->formatted_address;
        }
        return $address;
    }

    public function getLocationsp($latitude, $longitude) {

        $geolocation = $latitude . "," . $longitude; //"18.4773911,73.9025829";//
        //echo "<pre>";print_r($geolocation);
        $request = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$geolocation&key=AIzaSyCDMpdO43txdg_zyovvZm9i1tMMFIQTKTU&sensor=false";


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);

        //$output = file_get_contents($request);
        $json_decode = json_decode($output);
        //print"<pre>";
        //print_R($json_decode);
        $address = "";
        if (isset($json_decode->results[0])) {
            $response = array();
            $address = $json_decode->results[0]->formatted_address;
        }
        return $address;
    }

    public function getShopOrdersbyorderid($order_no) {

        /* $user_type = $_SESSION[SESSION_PREFIX.'user_type'];
          $luser_id = $_SESSION[SESSION_PREFIX.'user_id'];
          $where="";
          if($user_type=='Superstockist'){
          $where=" AND OA.superstockistid ='$luser_id' ";
          }
          if($user_type=='Distributor'){
          $where=" AND OA.distributorid='$luser_id' ";
          } */

      $sql = "SELECT                  
                    TP.id as id,    
                    TP.productname as productname,
                    date_format(o.order_date, '%M') as month,
                    date_format(o.order_date, '%d-%m-%Y') as order_date,
                    od.product_variant_weight1,
                    od.product_unit_cost,
                    od.producthsn,
                    od.product_cgst,
                    od.product_sgst,
                    o.shop_id,
                    o.c_o_d,
                    o.invoice_no,
                    o.cod_percent,
                    o.ordered_by,
                    o.order_no,
                    od.product_variant_unit1 as unit, 
                    od.product_quantity as variantunit,
                    od.pro_mnf_date as pro_mnf_date,
                    od.order_status as order_status,
                    od.free_product_details,
                    od.p_cost_cgst_sgst as unitcost,
                    od.challan_no,
                    od.vehicle_no,
                    od.transport_mode,
                    od.date_time_supply,
                    od.place_of_supply,
                    od.signature_image,
                    od.sp_signature_image,
                    date_format(od.delivery_date, '%d-%m-%Y') as delivery_date                  
                FROM  tbl_orders o              
                LEFT JOIN tbl_order_details od ON od.order_id =o.id 
                LEFT JOIN tbl_product TP ON TP.id = od.product_id
                WHERE o.order_no = '$order_no' ";


        $result1 = mysqli_query($this->local_connection, $sql);

        $i = 0;
        $row_count = mysqli_num_rows($result1);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result1)) {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    } 
    public function getShopOrdersbyorderid_arrayid($order_ids) {
            //echo $_POST;//echo $order_ids;
       $sql = "SELECT   
                    TP.product_id as product_id, TP.product_variant_id,   
                    TP.product_name as product_name,
                    od.product_variant_weight1,
                    od.product_variant_unit1 , 
                    od.product_variant_weight2,
                    od.product_variant_unit2 ,
                    od.free_product_details,
                    sum(od.product_quantity) as sum_product_quantity
                FROM  tbl_orders o              
                LEFT JOIN tbl_order_details od ON od.order_id =o.id 
                LEFT JOIN pcatbrandvariant1 TP ON od.product_id=TP.product_id  and od.product_variant_id =TP.product_variant_id 
                WHERE o.id IN (".$order_ids.") group by od.product_variant_id";
        $result1 = mysqli_query($this->local_connection, $sql);

        $i = 0;
        $row_count = mysqli_num_rows($result1);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result1)) {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    }
    public function getShopOrdersbyorderid_cust($order_no) {

        $sql = "SELECT                  
                    TP.id as id,    
                    TP.productname as productname,
                    date_format(od.order_date, '%M') as month,
                    date_format(od.order_date, '%d-%m-%Y') as order_date,
                    od.product_variant_weight1,
                    od.product_unit_cost,
                    od.producthsn,
                    od.product_cgst,
                    od.product_sgst,
                    od.order_from as ordered_by,
                    od.order_no,
                    od.product_variant_unit1 as unit, 
                    od.product_quantity as variantunit,
                    od.p_cost_cgst_sgst as unitcost,
                    od.challan_no,
                    od.vehicle_no,
                    od.transport_mode,
                    od.date_time_supply,
                    od.place_of_supply,
                    od.signature_image,
                    date_format(od.delivery_date, '%d-%m-%Y') as delivery_date      
                FROM  tbl_customer_orders_new od 
                LEFT JOIN tbl_product TP ON TP.id = od.product_id
                WHERE od.order_no = '$order_no' ";
                /*
                
                    od.free_product_details,
                od.challan_no,
                    od.vehicle_no,
                    od.transport_mode,
                    od.date_time_supply,
                    od.place_of_supply,
                    od.signature_image,
                    date_format(od.delivery_date, '%d-%m-%Y') as delivery_date          
                */


        $result1 = mysqli_query($this->local_connection, $sql);

        $i = 0;
        $row_count = mysqli_num_rows($result1);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result1)) {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    }

       public function getDirectOrdersbyorderid($order_no) {

     $sql = "SELECT os.id, os.product_id, os.product_varient_id, os.order_no, os.invoice_no, os.ordered_by_userid, os.assign_to_userid, os.ordered_by_usertype, os.order_date, os.category_name, os.product_name, os.producthsn, os.product_variant, os.product_quantity, os.product_cgst, os.product_sgst, os.total_cost, os.discounted_price, os.price_by_weight, os.p_cost_totalwith_tax, os.delivery_assign_to, os.delivery_assing_date, os.order_status_ssss, p.productname,pv.variant_1,pv.variant_2,pv.variant1_unit_id,pv.variant2_unit_id,os.product_unitcost as unit_price,pu1.unitname as unitname1,pu2.unitname as unitname2
    FROM tbl_order_stockist as os
    left join tbl_product p ON p.id = os.product_id 
    left join tbl_product_variant pv ON pv.id = os.product_varient_id
    left join tbl_units pu1 ON pu1.id = pv.variant1_unit_id
    left join tbl_units pu2 ON pu2.id = pv.variant2_unit_id
    where os.order_no ='$order_no' ";


        $result1 = mysqli_query($this->local_connection, $sql);

        $i = 0;
        $row_count = mysqli_num_rows($result1);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result1)) {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    }

    public function getOrders($order_status, $order_id = null) {
        extract($_POST);
        //var_dump($_POST);
        $supp_chnz_status = substr($order_status, 1, 1);
        $order_status = substr($order_status, 0, 1);
        //$sup_chnz_status
        if ($supp_chnz_status != "") {
            $sup_chnz_statuscond .= " AND od.supp_chnz_status = " . $supp_chnz_status;
        }
        $where_clause_outer = '';
        $where_clause_inner = '';
        if ($order_id != '') {
            $where_clause_outer .= " AND o.id = '" . $order_id . "' ";
        }
        if ($frmdate != '') {
            $where_clause_outer .= " AND date_format(o.order_date, '%d-%m-%Y') = '" . $frmdate . "' ";
        }
        if ($dropdownSalesPerson != "") {
            $where_clause_outer .= " AND o.ordered_by = " . $dropdownSalesPerson;
        }
        if ($divShopdropdown != "") {
            $where_clause_outer .= " AND o.shop_id = " . $divShopdropdown;
        }
        if ($subarea != "") {
            $where_clause_outer .= " AND s.subarea_id = " . $subarea;
        }
        if ($area != "") {
            $where_clause_outer .= " AND s.suburbid = " . $area;
        }
        if ($city != "") {
            $where_clause_outer .= " AND s.city = " . $city;
        }
        if ($dropdownState != "") {
            $where_clause_outer .= " AND s.state = " . $dropdownState;
        }
        if ($dropdownCategory != "") {
            $where_clause_inner .= " AND od.cat_id = " . $dropdownCategory;
        }
        if ($dropdownProducts != "") {
            $where_clause_inner .= " AND od.product_id = " . $dropdownProducts;
        }
        if ($distributorid != "") {
            $where_clause_outer .= " AND o.distributorid = " . $distributorid;
        }

         $order_sql = "SELECT o.id as oid, o.order_no, o.invoice_no, 
        o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
        o.order_date, 
        o.shop_id,  s.name AS shop_name,
        s.suburbid, (SELECT sb.suburbnm FROM tbl_area AS sb WHERE sb.id = s.suburbid) AS region_name,
        o.total_items, 
        o.total_cost, o.total_order_gst_cost, o.lat, 
        o.long, o.offer_provided 
        FROM `tbl_orders` AS o      
        LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
        LEFT JOIN tbl_order_details AS od ON od.order_id = o.id 
        WHERE 1=1 $where_clause_outer $sup_chnz_statuscond
        ORDER BY shop_name, o.order_date DESC"; //o.shop_order_status = ".$order_status."
        $orders_result = mysqli_query($this->local_connection, $order_sql);
        $row_orders_count = mysqli_num_rows($orders_result);
        $i = 1;
        $all_orders = array();
        $row_orders_det_count = 0;
        if ($row_orders_count > 0) {
            while ($row_orders = mysqli_fetch_assoc($orders_result)) {
                $all_orders[$i] = $row_orders;
                $order_details_sql = "SELECT od.id, od.order_id, 
                od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
                od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
                od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
                od.producthsn,
                od.product_variant_id, od.product_quantity, 
                od.product_variant_weight1, od.product_variant_unit1, 
                od.product_variant_weight2, od.product_variant_unit2, 
                od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
                od.campaign_applied, od.campaign_type, od.campaign_sale_type, od.order_status, 
                od.delivery_assing_date, od.delivery_assign_to, od.transport_date, 
                od.challan_no, od.vehicle_no, od.transport_mode, od.date_time_supply, od.place_of_supply, 
                od.quantity_delivered, od.delivery_date, od.amount_paid, od.payment_date,
                odis.discount_amount
                FROM `tbl_order_details` AS od
                LEFT JOIN tbl_order_cp_discount AS odis ON odis.order_variant_id = od.id
                WHERE od.order_id = " . $row_orders['id'] . " AND od.order_status = " . $order_status . " $where_clause_inner";
                $orders_det_result = mysqli_query($this->local_connection, $order_details_sql);
                //$row_orders_det_count = mysqli_num_rows($orders_det_result);
                if (mysqli_num_rows($orders_det_result) > 0) {
                    $j = 1;
                    while ($row_orders_det = mysqli_fetch_assoc($orders_det_result)) {
                        $all_orders[$i]['order_details'][$j] = $row_orders_det;
                        $j++;
                    }
                    $i++;
                } else {
                    unset($all_orders[$i]);
                }
            }
        }
        //print"<pre>";
        //print_r($all_orders);

        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }

    public function getOrders1($order_status, $distributorid, $usertype) {
        $supp_chnz_status = substr($order_status, 1, 1);
        $order_status = substr($order_status, 0, 1);
        //$sup_chnz_status
        if ($supp_chnz_status != "") {
            $sup_chnz_statuscond .= " AND od.supp_chnz_status = " . $supp_chnz_status;
        }
        if ($usertype == "Distributor") {
            if ($distributorid != "") {
                $where_clause_outer .= " AND o.distributorid = " . $distributorid;
            }
        } else {
            if ($distributorid != "") {
                $where_clause_outer .= " AND o.superstockistid = " . $distributorid;
            }
        }



        $order_sql = "SELECT o.id as oid, o.order_no, o.invoice_no, 
        o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
        o.order_date, 
        o.shop_id,  s.name AS shop_name,
        s.suburbid, (SELECT sb.suburbnm FROM tbl_area AS sb WHERE sb.id = s.suburbid) AS region_name,
        o.total_items, 
        o.total_cost, o.total_order_gst_cost, o.lat, 
        o.long, o.offer_provided 
        FROM `tbl_orders` AS o      
        LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
        LEFT JOIN tbl_order_details AS od ON od.order_id = o.id 
        WHERE 1=1 $where_clause_outer $sup_chnz_statuscond
        ORDER BY shop_name, o.order_date DESC"; //o.shop_order_status = ".$order_status."
        $orders_result = mysqli_query($this->local_connection, $order_sql);
        $row_orders_count = mysqli_num_rows($orders_result);
        $i = 1;
        $all_orders = array();
        $row_orders_det_count = 0;
        if ($row_orders_count > 0) {
            while ($row_orders = mysqli_fetch_assoc($orders_result)) {
                $all_orders[$i] = $row_orders;
                $order_details_sql = "SELECT od.id, od.order_id, 
                od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
                od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
                od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
                od.producthsn,
                od.product_variant_id, od.product_quantity, 
                od.product_variant_weight1, od.product_variant_unit1, 
                od.product_variant_weight2, od.product_variant_unit2, 
                od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
                od.campaign_applied, od.campaign_type, od.campaign_sale_type, od.order_status, 
                od.delivery_assing_date, od.delivery_assign_to, od.transport_date, 
                od.challan_no, od.vehicle_no, od.transport_mode, od.date_time_supply, od.place_of_supply, 
                od.quantity_delivered, od.delivery_date, od.amount_paid, od.payment_date,
                odis.discount_amount
                FROM `tbl_order_details` AS od
                LEFT JOIN tbl_order_cp_discount AS odis ON odis.order_variant_id = od.id
                WHERE od.order_id = " . $row_orders['oid'] . " "; //AND od.order_status = ".$order_status." $where_clause_inner
                $orders_det_result = mysqli_query($this->local_connection, $order_details_sql);
                //$row_orders_det_count = mysqli_num_rows($orders_det_result);
                if (mysqli_num_rows($orders_det_result) > 0) {
                    $j = 1;
                    while ($row_orders_det = mysqli_fetch_assoc($orders_det_result)) {
                        $all_orders[$i]['order_details'][$j] = $row_orders_det;
                        $j++;
                    }
                    $i++;
                } else {
                    unset($all_orders[$i]);
                }
            }
        }
        //print"<pre>";
        //print_r($all_orders);

        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }

    public function getOrderschnage($order_status, $order_id = null) { //for orders page search
        extract($_POST);
        //var_dump($_POST);
        $supp_chnz_status = substr($order_status, 1, 1);
        $order_status = substr($order_status, 0, 1);
        //$sup_chnz_status
        if ($supp_chnz_status != "") {
            $sup_chnz_statuscond .= " AND o.supp_chnz_status = " . $supp_chnz_status;
        }
        $where_clause_outer = '';
        $where_clause_inner = '';

        if ($order_id != "") {
            $where_clause_outer .= " AND od.order_id = " . $order_id;
        }
        if ($frmdate != '') {
            $where_clause_outer .= " AND date_format(o.order_date, '%d-%m-%Y') = '" . $frmdate . "' ";
        }
        if ($dropdownSalesPerson != "") {
            $where_clause_outer .= " AND o.ordered_by = " . $dropdownSalesPerson;
        }
        if ($cmbSuperStockist != "") {
            $where_clause_outer .= " AND o.superstockistid = " . $cmbSuperStockist;
        }
        if ($dropdownStockist != "") {
            $where_clause_outer .= " AND o.distributorid = " . $dropdownStockist;
        }
        if ($divShopdropdown != "") {
            $where_clause_outer .= " AND o.shop_id = " . $divShopdropdown;
        }
        if ($area != "") {
            $where_clause_outer .= " AND s.suburbid = " . $area;
        }
        if ($subarea != "") {
            $where_clause_outer .= " AND s.subarea_id = " . $subarea;
        }
        if ($city != "") {
            $where_clause_outer .= " AND s.city = " . $city;
        }
        if ($dropdownState != "") {
            $where_clause_outer .= " AND s.state = " . $dropdownState;
        }
        if ($dropdownCategory != "") {
            $where_clause_inner .= " AND od.cat_id = " . $dropdownCategory;
        }
        if ($dropdownProducts != "") {
            $where_clause_inner .= " AND od.product_id = " . $dropdownProducts;
        }


         $order_sql = "SELECT o.id as oid,od.id as odid, o.order_no, o.invoice_no, 
        o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
        o.order_date, 
        o.shop_id,  s.name AS shop_name,
        s.suburbid, (SELECT sb.suburbnm FROM tbl_area AS sb WHERE sb.id = s.suburbid) AS region_name,
        o.total_items, 
        o.total_cost, o.total_order_gst_cost, o.lat, 
        o.long, o.offer_provided,u.firstname AS stockistnm 
        FROM `tbl_orders` AS o      
        LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
        RIGHT JOIN tbl_order_details AS od ON od.order_id = o.id 
        LEFT JOIN `tbl_user` AS u ON u.id = o.distributorid
        WHERE 1=1 AND od.order_status = " . $order_status . " $where_clause_outer $where_clause_inner $sup_chnz_statuscond
        ORDER BY o.order_date DESC "; //o.shop_order_status = ".$order_status."
        $orders_result = mysqli_query($this->local_connection, $order_sql);
        $row_orders_count = mysqli_num_rows($orders_result);
        $i = 1;
        $all_orders = array();
        if ($row_orders_count > 0) {
            while ($row_orders = mysqli_fetch_assoc($orders_result)) {
                $all_orders[$i] = $row_orders;
                $i++;
            }
        }

        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }

    public function getOrderschnage1($order_status, $order_id = null) { //for orders page search
        extract($_POST);
        $usertype = $_SESSION[SESSION_PREFIX . 'user_type'];
        $userid = $_SESSION[SESSION_PREFIX . 'user_id'];

        $where_clause_outer = '';
        $where_clause_inner = '';
        $child_sql = "SELECT id FROM `tbl_user` 
        WHERE  find_in_set('".$userid."',parent_ids) <> 0"; 
        $child_result = mysqli_query($this->local_connection, $child_sql);
        $row_child_count = mysqli_num_rows($child_result);
        $all_childs = array();$i=0;
        $all_childs[$i]=$userid;$i=$i+1;
        if ($row_child_count > 0) {
            while ($row_childs = mysqli_fetch_assoc($child_result)) {               
                $all_childs[$i] = $row_childs['id'];  $i++;             
            }
        }
        $order_from_ids=implode(',',array_unique($all_childs));
        
        $select_location_clause =" (select suburb_ids from tbl_user_view tuv where tuv.id =o.ordered_by) as suburbid1, 
            (select suburbnm from tbl_user_view tuv where tuv.id =o.ordered_by ) AS region_name1 ,";
            
        if ($order_status == 0) {
            $where_clause_outer .= " AND find_in_set(o.order_from,'".$order_from_ids."') <> 0" ;
        }else if($order_status == 10){
            $where_clause_outer .= " AND  o.order_from ='".$userid."'";
        }else if($order_status == 1 || $order_status == 2 || $order_status == 4){
            $where_clause_outer .= " AND od.order_status  = '".$order_status."' AND o.order_to ='".$userid."'  AND shop_id  IS NOT NULL";
        }else if($order_status == 11 || $order_status == 22||$order_status == 44){ $order_status=$order_status%10;
            $where_clause_outer .= " AND od.order_status  = '$order_status' AND o.order_to ='".$userid."'  AND shop_id  IS  NULL";          
        }else{
            $where_clause_outer .= " AND od.order_status  = '".$order_status."' AND o.order_to ='".$userid."'";
        }
                
        if ($order_id != "") {
            $where_clause_outer .= " AND od.order_id = '".$order_id."' ";
        }
        if ($frmdate != '') {
            $where_clause_outer .= " AND date_format(o.order_date, '%d-%m-%Y') = '" . $frmdate . "' ";
        }       
        
        if ($order_from != "") {
            $where_clause_outer .= " AND o.order_from = '".$order_from."' ";
        }       
        if ($area != "") {
            $where_clause_outer .= " AND s.suburbid = '".$area."' ";
        }
        if ($subarea != "") {
            $where_clause_outer .= " AND s.subarea_id = '".$subarea."'";
        }
        if ($city != "") {
            $where_clause_outer .= " AND s.city = '".$city."' ";
        }
        if ($dropdownState != "") {
            $where_clause_outer .= " AND s.state = '".$dropdownState."' ";
        }
         if ($dropdownCategory != "") {
            $where_clause_inner .= " AND od.cat_id = " . $dropdownCategory;
        }
        if ($dropdownProducts != "") {
            $where_clause_inner .= " AND od.product_id = " . $dropdownProducts;
        }

        $order_sql = "SELECT distinct o.id as oid, o.order_no, o.invoice_no, od.order_status,
        o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
        (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.order_to) AS order_to_name,
        o.order_date, 
        o.shop_id,  s.name AS shop_name,
        s.suburbid, (SELECT sb.suburbnm FROM tbl_area AS sb WHERE sb.id = s.suburbid) AS region_name,
        s.subarea_id, (SELECT sb1.subarea_name FROM tbl_subarea AS sb1 WHERE sb1.subarea_id = s.subarea_id) AS subarea_name,
        $select_location_clause
        o.total_items, 
        o.total_cost, o.total_order_gst_cost, o.lat, 
        o.long, o.offer_provided,(SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.order_from) AS order_from_name,
        o.sended_to_production,od.payment_date,od.comment
        FROM `tbl_orders` AS o      
        LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
        LEFT JOIN tbl_order_details AS od ON od.order_id = o.id   
        WHERE 1=1  $where_clause_outer $where_clause_inner 
        ORDER BY o.order_date DESC "; //o.shop_order_status = ".$order_status."
        
        $orders_result = mysqli_query($this->local_connection, $order_sql);
        $row_orders_count = mysqli_num_rows($orders_result);
        $i = 1;
        $all_orders = array();
        if ($row_orders_count > 0) {
            while ($row_orders = mysqli_fetch_assoc($orders_result)) {
                $all_orders[$i] = $row_orders;
                $i++;
            }
        }

        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }
    public function getOrderschnage1_sp($order_status, $order_id = null) { //for orders page search
        extract($_POST);
        $usertype = $_SESSION[SESSION_PREFIX . 'user_type'];
        $userid = $_SESSION[SESSION_PREFIX . 'user_id'];

        $where_clause_outer = '';
        $where_clause_inner = '';
        $child_sql = "SELECT id FROM `tbl_user` 
        WHERE  find_in_set('".$userid."',parent_ids) <> 0 AND (user_role='Admin' or user_role='Superstockist' or user_role='Distributor') "; 
        $child_result = mysqli_query($this->local_connection, $child_sql);
        $row_child_count = mysqli_num_rows($child_result);
        $all_childs = array();$i=0;
        $all_childs[$i]=$userid;$i=$i+1;
        if ($row_child_count > 0) {
            while ($row_childs = mysqli_fetch_assoc($child_result)) {               
                $all_childs[$i] = $row_childs['id'];  $i++;             
            }
        }
        $order_from_ids=implode(',',array_unique($all_childs));
        
        $select_location_clause =" (select suburb_ids from tbl_user_view tuv where tuv.id =o.ordered_by) as suburbid1, 
            (select suburbnm from tbl_user_view tuv where tuv.id =o.ordered_by ) AS region_name1 ,";
            
        if ($order_status == 0) {
            $where_clause_outer .= " AND find_in_set(o.order_to,'".$order_from_ids."') <> 0 AND shop_id  IS NOT NULL " ;
        }else if($order_status == 1 || $order_status == 2 || $order_status == 4){
            $where_clause_outer .= " AND od.order_status  = '".$order_status."' AND o.order_to ='".$userid."'  AND shop_id  IS NOT NULL";
        }else{
            $where_clause_outer .= " AND od.order_status  = '".$order_status."' AND o.order_to ='".$userid."' AND shop_id  IS NOT NULL";
        }
                
        if ($order_id != "") {
            $where_clause_outer .= " AND od.order_id = '".$order_id."' ";
        }
        if ($frmdate != '') {
            $where_clause_outer .= " AND date_format(o.order_date, '%d-%m-%Y') = '" . $frmdate . "' ";
        }       
        
        if ($order_from != "") {
            $where_clause_outer .= " AND o.order_from = '".$order_from."' ";
        }       
        if ($area != "") {
            $where_clause_outer .= " AND s.suburbid = '".$area."' ";
        }
        if ($subarea != "") {
            $where_clause_outer .= " AND s.subarea_id = '".$subarea."'";
        }
        if ($city != "") {
            $where_clause_outer .= " AND s.city = '".$city."' ";
        }
        if ($dropdownState != "") {
            $where_clause_outer .= " AND s.state = '".$dropdownState."' ";
        }
         if ($dropdownCategory != "") {
            $where_clause_inner .= " AND od.cat_id = " . $dropdownCategory;
        }
        if ($dropdownProducts != "") {
            $where_clause_inner .= " AND od.product_id = " . $dropdownProducts;
        }

          $order_sql = "SELECT distinct o.id as oid, o.order_no, o.invoice_no, 
        o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
        (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.order_to) AS order_to_name,
        o.order_date, 
        o.shop_id,  s.name AS shop_name,
        s.suburbid, (SELECT sb.suburbnm FROM tbl_area AS sb WHERE sb.id = s.suburbid) AS region_name,
        s.subarea_id, (SELECT sb1.subarea_name FROM tbl_subarea AS sb1 WHERE sb1.subarea_id = s.subarea_id) AS subarea_name,
        $select_location_clause
        o.total_items, 
        o.total_cost, o.total_order_gst_cost, o.lat, 
        o.long, o.offer_provided,(SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.order_from) AS order_from_name,
        o.sended_to_production
        FROM `tbl_orders` AS o      
        LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
        LEFT JOIN tbl_order_details AS od ON od.order_id = o.id   
        WHERE 1=1  $where_clause_outer $where_clause_inner 
        ORDER BY o.order_date DESC "; //o.shop_order_status = ".$order_status."
        //die();
        $orders_result = mysqli_query($this->local_connection, $order_sql);
        $row_orders_count = mysqli_num_rows($orders_result);
        $i = 1;
        $all_orders = array();
        if ($row_orders_count > 0) {
            while ($row_orders = mysqli_fetch_assoc($orders_result)) {
                $all_orders[$i] = $row_orders;
                $i++;
            }
        }

        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }

        public function getMyStockRequest($order_status, $order_id = null) { //for orders page search
        extract($_POST);
        $usertype = $_SESSION[SESSION_PREFIX . 'user_type'];
        $userid = $_SESSION[SESSION_PREFIX . 'user_id'];

        $where_clause_outer = '';
        $where_clause_inner = '';

       if($order_status == 10)
       {
            $where_clause_outer .= " AND  o.order_from ='".$userid."'";
       }
        else if($order_status == 1 || $order_status == 2 || $order_status == 4)
        {
            $where_clause_outer .= " AND od.order_status  = '".$order_status."' AND o.order_from ='".$userid."'  AND shop_id  IS NOT NULL";
        }
        else if($order_status == 11 || $order_status == 22||$order_status == 44)

        { $order_status=$order_status%10;
            $where_clause_outer .= " AND od.order_status  = '$order_status' AND o.order_from ='".$userid."'  AND shop_id  IS  NULL";          
        }         
        else if($order_status == 99)
        {
           $where_clause_outer .= " AND od.order_status  IN (1,2,4,9) AND o.order_from ='".$userid."'  AND shop_id  IS  NULL";
        }
        else{
            $where_clause_outer .= " AND od.order_status  = '".$order_status."' AND o.order_from ='".$userid."'";
        }
                
        if ($order_id != "") {
            $where_clause_outer .= " AND od.order_id = '".$order_id."' ";
        }
        if ($frmdate != '') {
            $where_clause_outer .= " AND date_format(o.order_date, '%d-%m-%Y') = '" . $frmdate . "' ";
        }       
          
         if ($dropdownCategory != "") {
            $where_clause_inner .= " AND od.cat_id = " . $dropdownCategory;
        }
        if ($dropdownProducts != "") {
            $where_clause_inner .= " AND od.product_id = " . $dropdownProducts;
        }

        $order_sql = "SELECT distinct o.id as oid, o.order_no, o.invoice_no, 
        o.ordered_by,od.order_status, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
        (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.order_to) AS order_to_name,
        o.order_date, 
        o.shop_id,  s.name AS shop_name,
        s.suburbid, (SELECT sb.suburbnm FROM tbl_area AS sb WHERE sb.id = s.suburbid) AS region_name,
        s.subarea_id, (SELECT sb1.subarea_name FROM tbl_subarea AS sb1 WHERE sb1.subarea_id = s.subarea_id) AS subarea_name,
        $select_location_clause
        o.total_items, 
        o.total_cost, o.total_order_gst_cost, o.lat, 
        o.long, o.offer_provided,(SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.order_from) AS order_from_name,
        o.sended_to_production
        FROM `tbl_orders` AS o      
        LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
        LEFT JOIN tbl_order_details AS od ON od.order_id = o.id   
        WHERE 1=1  $where_clause_outer $where_clause_inner 
        ORDER BY o.order_date DESC "; //o.shop_order_status = ".$order_status."
        $orders_result = mysqli_query($this->local_connection, $order_sql);
        $row_orders_count = mysqli_num_rows($orders_result);
        $i = 1;
        $all_orders = array();
        if ($row_orders_count > 0) {
            while ($row_orders = mysqli_fetch_assoc($orders_result)) {
                $all_orders[$i] = $row_orders;
                $i++;
            }
        }

        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }
    public function getOrderschnage1_cust($order_status, $order_id = null) { //for orders page search
        extract($_POST);
        //echo "<pre>";print_r($_POST);
        $usertype = $_SESSION[SESSION_PREFIX . 'user_type'];
        $userid = $_SESSION[SESSION_PREFIX . 'user_id'];

        $where_clause_outer = '';
        $where_clause_inner = '';
        
        $child_sql = "SELECT id FROM `tbl_user` 
        WHERE  find_in_set('".$userid."',parent_ids) <> 0 AND (user_role='Admin' or user_role='Superstockist' or user_role='Distributor')"; 
        $child_result = mysqli_query($this->local_connection, $child_sql);
        $row_child_count = mysqli_num_rows($child_result);
        $all_childs = array();$i=0;
        $all_childs[$i]=$userid;$i=$i+1;
        if ($row_child_count > 0) {
            while ($row_childs = mysqli_fetch_assoc($child_result)) {               
                $all_childs[$i] = $row_childs['id'];  $i++;             
            }
        }
        $order_from_ids=implode(',',array_unique($all_childs));
            
        if ($order_status == 10) {
            $where_clause_outer .= " AND find_in_set(od.order_to,'".$order_from_ids."') <> 0" ;
        }else{
            $order_status = substr($order_status, 0, 1);
            $where_clause_outer .= " AND od.order_status  = '".$order_status."' AND od.order_to ='".$userid."'";
            if ($order_id != "") {
                $where_clause_outer .= " AND od.order_no = '".$order_id."' ";
            }
        }
        
        if ($frmdate != '') {
            $where_clause_outer .= " AND date_format(od.order_date, '%d-%m-%Y') = '" . $frmdate . "' ";
        }       
        
        if ($order_from != "") {
            $where_clause_outer .= " AND od.order_from = '".$order_from."' ";
        }       
        
        if ($dropdownCategory != "") {
            $where_clause_inner .= " AND od.cat_id = " . $dropdownCategory;
        }
        if ($dropdownProducts != "") {
            $where_clause_inner .= " AND od.product_id = " . $dropdownProducts;
        }

        $order_sql = "SELECT  `order_no` as oid, `order_from`, `order_to`, 
         (SELECT tc.cust_name FROM tbl_customer AS tc WHERE tc.cust_id = od.order_from) AS order_by_name,
         (SELECT u.firstname FROM tbl_user AS u WHERE u.id = od.order_to) AS order_to_name,
         `order_date`, `order_status`,sum(`product_quantity`) as sum_product_quantity ,
        sum( `product_unit_cost`) as sum_product_unit_cost,sum( `product_total_cost`)as sum_product_total_cost ,
        sum( `p_cost_cgst_sgst`) as sum_p_cost_cgst_sgst
         FROM `tbl_customer_orders_new` od WHERE 1=1 $where_clause_outer $where_clause_inner
         GROUP BY od.order_no 
        ORDER BY od.order_date DESC "; //o.shop_order_status = ".$order_status."
    
        $orders_result = mysqli_query($this->local_connection, $order_sql);
        $row_orders_count = mysqli_num_rows($orders_result);
        $i = 1;
        $all_orders = array();
        if ($row_orders_count > 0) {
            while ($row_orders = mysqli_fetch_assoc($orders_result)) {
                $all_orders[$i] = $row_orders;
                $i++;
            }
        }

        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }
    
    public function getOrdersDetailschnage_cust($order_no) {
        extract($_POST);

        if ($order_no != "") {
            $where_clause_outer .= " AND od.order_no = " . $order_no;
        }
          $order_details_sql = "SELECT od.id as odid, od.order_no, 
                  od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
                od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
                od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
                (SELECT p.added_by_userid FROM tbl_product AS p WHERE p.id = od.product_id) AS added_by_userid,             
                od.producthsn,
                od.product_variant_id, od.product_quantity, 
                od.product_variant_weight1, od.product_variant_unit1, 
                od.product_variant_weight2, od.product_variant_unit2, 
                od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst,
                od.order_status
                FROM `tbl_customer_orders_new` AS od    
                WHERE 1=1 $where_clause_outer";
        //,odpw.discounted_totalcost
        //LEFT JOIN tbl_campaign_price_weight AS odpw ON odpw.odid = od.id
        //AND od.order_status = ".$order_status." $where_clause_inner
        $orders_det_result = mysqli_query($this->local_connection, $order_details_sql);
        //$row_orders_det_count = mysqli_num_rows($orders_det_result);
        if (mysqli_num_rows($orders_det_result) > 0) {
            while ($row_orders_det = mysqli_fetch_assoc($orders_det_result)) {
                $all_orders['order_details'][] = $row_orders_det;
            }
        }
        return $all_orders;
    }


       public function getAllOrdersByUserid($order_status) { //for orders page search
        extract($_POST);
        $usertype = $_SESSION[SESSION_PREFIX . 'user_type'];
        $userrole = $_SESSION[SESSION_PREFIX . 'user_role'];
        $userid = $_SESSION[SESSION_PREFIX . 'user_id'];
        $where_clause_outer = '';
        $where_clause_inner = '';
        
        $child_sql = "SELECT id FROM `tbl_user` 
        WHERE  find_in_set('".$userid."',parent_ids) <> 0"; 
        $child_result = mysqli_query($this->local_connection, $child_sql);
        $row_child_count = mysqli_num_rows($child_result);
        $all_childs = array();$i=0;
        $all_childs[$i]=$userid;$i=$i+1;
        if ($row_child_count > 0) {
            while ($row_childs = mysqli_fetch_assoc($child_result)) {               
                $all_childs[$i] = $row_childs['id'];  $i++;             
            }
        }
        $order_from_ids=implode(',',array_unique($all_childs));
       
        if ($order_status == 0) {
            $where_clause_outer .= " AND find_in_set(o.order_from,'".$order_from_ids."') <> 0" ;
        }else{
            $where_clause_outer .= " AND od.order_status  = '".$order_status."' AND o.order_to ='".$userid."'";
        }
        if ($order_status == 1) {
            $where_clause_outer .= " AND shop_id  IS NOT NULL" ;
        }
        if ($order_status == 11) {
            $where_clause_outer .= " AND shop_id  IS NULL" ;
        }
        if ($frmdate != '') {
            $where_clause_outer .= " AND date_format(o.order_date, '%d-%m-%Y') = '" . $frmdate . "' ";
        }
        if ($order_from != "") {
            $where_clause_outer .= " AND o.ordered_by = '".$order_from."' ";
        }
       
        if ($divShopdropdown != "") {
            $where_clause_outer .= " AND o.shop_id = '".$divShopdropdown."' ";
        }
        if ($area != "") {
            $where_clause_outer .= " AND s.suburbid = '".$area."' ";
        }
        if ($subarea != "") {
            $where_clause_outer .= " AND s.subarea_id = " . $subarea;
        }
        if ($city != "") {
            $where_clause_outer .= " AND s.city = " . $city;
        }
        if ($dropdownState != "") {
            $where_clause_outer .= " AND s.state = " . $dropdownState;
        }
        if ($dropdownCategory != "") {
            $where_clause_inner .= " AND od.cat_id = " . $dropdownCategory;
        }
        if ($dropdownProducts != "") {
            $where_clause_inner .= " AND od.product_id = " . $dropdownProducts;
        }
        $select_location_clause =" (select suburb_ids from tbl_user_view tuv where tuv.id =o.ordered_by) as suburbid1, 
            (select suburbnm from tbl_user_view tuv where tuv.id =o.ordered_by ) AS region_name1 ,";
            
         $order_sql = "SELECT distinct o.id as oid, o.order_no, o.invoice_no, 
        o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
        (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.order_to) AS order_to_name,
        o.order_date, 
        o.shop_id,  s.name AS shop_name,
        s.suburbid, (SELECT sb.suburbnm FROM tbl_area AS sb WHERE sb.id = s.suburbid) AS region_name,
        $select_location_clause
        o.total_items, 
        o.total_cost, o.total_order_gst_cost, o.lat, 
        o.long, o.offer_provided,o.sended_to_production
        FROM `tbl_orders` AS o      
        LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
        LEFT JOIN tbl_order_details AS od ON od.order_id = o.id     
        WHERE 1=1 $where_order_from $where_clause_outer $where_clause_inner 
        AND od.order_status='".$order_status."' ORDER BY o.order_date DESC "; //o.shop_order_status = ".$order_status."
        $orders_result = mysqli_query($this->local_connection, $order_sql);
        $row_orders_count = mysqli_num_rows($orders_result);
        $i = 1;
        $all_orders = array();
        if ($row_orders_count > 0) {
            while ($row_orders = mysqli_fetch_assoc($orders_result)) {
                $all_orders[$i] = $row_orders;
                $i++;
            }
        }
        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }

     public function getAllOrdersForAcc($order_status) { //for orders page search
        extract($_POST);
        $usertype = $_SESSION[SESSION_PREFIX . 'user_type'];
        $userrole = $_SESSION[SESSION_PREFIX . 'user_role'];
        $userid = $_SESSION[SESSION_PREFIX . 'user_id'];
        $ex_sql = "SELECT id,external_id FROM `tbl_user` 
        WHERE  id= '".$userid."'"; 
        $ex_result = mysqli_query($this->local_connection, $ex_sql);
        $ex_row = mysqli_fetch_assoc($ex_result);
        $parent_id = $ex_row['external_id'];
       

        $select_location_clause =" (select suburb_ids from tbl_user_view tuv where tuv.id =o.ordered_by) as suburbid1, 
            (select suburbnm from tbl_user_view tuv where tuv.id =o.ordered_by ) AS region_name1 ,";
            
       $order_sql = "SELECT distinct o.id as oid, o.order_no, o.invoice_no, 
        o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
        (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.order_to) AS order_to_name,
        o.order_date, 
        o.shop_id,  s.name AS shop_name,
        s.suburbid, (SELECT sb.suburbnm FROM tbl_area AS sb WHERE sb.id = s.suburbid) AS region_name,
        $select_location_clause
        o.total_items, 
        o.total_cost, o.total_order_gst_cost, o.lat, 
        o.long, o.offer_provided,o.sended_to_production
        FROM `tbl_orders` AS o      
        LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
        LEFT JOIN tbl_order_details AS od ON od.order_id = o.id     
        WHERE  od.order_status='".$order_status."' AND o.order_to = '".$parent_id."' ORDER BY o.order_date DESC "; //o.shop_order_status = ".$order_status."
        $orders_result = mysqli_query($this->local_connection, $order_sql);
        $row_orders_count = mysqli_num_rows($orders_result);
        $i = 1;
        $all_orders = array();
        if ($row_orders_count > 0) {
            while ($row_orders = mysqli_fetch_assoc($orders_result)) {
                $all_orders[$i] = $row_orders;
                $i++;
            }
        }
        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }


    public function getAccOrderschnage($order_status, $order_id = null) { //for orders page search
        extract($_POST);
        $usertype = $_SESSION[SESSION_PREFIX . 'user_type'];
        $userid = $_SESSION[SESSION_PREFIX . 'user_id'];
        $where_clause_outer = '';
        $where_clause_inner = '';   
        $ex_sql = "SELECT id,external_id FROM `tbl_user` 
        WHERE  id= '".$userid."'"; 
        $ex_result = mysqli_query($this->local_connection, $ex_sql);
        $ex_row = mysqli_fetch_assoc($ex_result);
        $parent_id = $ex_row['external_id'];
        
        $select_location_clause =" (select suburb_ids from tbl_user_view tuv where tuv.id =o.ordered_by) as suburbid1, 
            (select suburbnm from tbl_user_view tuv where tuv.id =o.ordered_by ) AS region_name1 ,";
        if($order_status == 1){
            $where_clause_outer .= " AND od.order_status  = '1' AND o.order_to ='".$parent_id."' ";// AND shop_id  IS NOT NULL
        }else if($order_status == 2){
            $where_clause_outer .= " AND od.order_status  = '2' AND o.order_to ='".$parent_id."'";          
        }else{
            $where_clause_outer .= " AND od.order_status  = '".$order_status."' AND o.order_to ='".$parent_id."'";
        }
                
        if ($order_id != "") {
            $where_clause_outer .= " AND od.order_id = '".$order_id."' ";
        }
        if ($frmdate != '') {
            $where_clause_outer .= " AND date_format(o.order_date, '%d-%m-%Y') = '" . $frmdate . "' ";
        }      
        if ($area != "") {
            $where_clause_outer .= " AND s.suburbid = '".$area."' ";
        }
        if ($subarea != "") {
            $where_clause_outer .= " AND s.subarea_id = '".$subarea."'";
        }
        if ($city != "") {
            $where_clause_outer .= " AND s.city = '".$city."' ";
        }
        if ($dropdownState != "") {
            $where_clause_outer .= " AND s.state = '".$dropdownState."' ";
        }
         if ($dropdownCategory != "") {
            $where_clause_inner .= " AND od.cat_id = " . $dropdownCategory;
        }
        if ($dropdownProducts != "") {
            $where_clause_inner .= " AND od.product_id = " . $dropdownProducts;
        }

        $order_sql = "SELECT distinct o.id as oid, o.order_no, o.invoice_no, 
        o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
        (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.order_to) AS order_to_name,
        o.order_date, 
        o.shop_id,  s.name AS shop_name,
        s.suburbid, (SELECT sb.suburbnm FROM tbl_area AS sb WHERE sb.id = s.suburbid) AS region_name,
        $select_location_clause
        o.total_items, 
        o.total_cost, o.total_order_gst_cost, o.lat, 
        o.long, o.offer_provided,(SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.order_from) AS order_from_name,
        o.sended_to_production
        FROM `tbl_orders` AS o      
        LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
        LEFT JOIN tbl_order_details AS od ON od.order_id = o.id   
        WHERE 1=1  $where_clause_outer $where_clause_inner 
        ORDER BY o.order_date DESC "; //o.shop_order_status = ".$order_status."
        $orders_result = mysqli_query($this->local_connection, $order_sql);
        $row_orders_count = mysqli_num_rows($orders_result);
        $i = 1;
        $all_orders = array();
        if ($row_orders_count > 0) {
            while ($row_orders = mysqli_fetch_assoc($orders_result)) {
                $all_orders[$i] = $row_orders;
                $i++;
            }
        }

        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }





      public function getDirectOrdersFromStockist($order_status) {       //for orders page search
       // extract($_POST);
        $usertype = $_SESSION[SESSION_PREFIX . 'user_type'];
         $userid = $_SESSION[SESSION_PREFIX . 'user_id'];
       // echo $order_status;
      //  exit();
        //var_dump($_POST);  
        //$order_status = substr($order_status, 0, 1);
        $order_status = $order_status;
        $where_clause_outer = '';
        $where_clause_inner = '';
       /* if ($usertype == "Superstockist") {
            if ($userid != "") {
                $where_clause_outer .= " AND o.superstockistid = " . $userid;
            }
        }*/

        if ($order_id != "") {
            $where_clause_outer .= " AND od.order_id = " . $order_id;
        }
        if ($frmdate != '') {
            $where_clause_outer .= " AND date_format(o.order_date, '%d-%m-%Y') = '" . $frmdate . "' ";
        }
        if ($dropdownSalesPerson != "") {
            $where_clause_outer .= " AND o.ordered_by = " . $dropdownSalesPerson;
        }
        if ($cmbSuperStockist != "") {
            $where_clause_outer .= " AND o.superstockistid = " . $cmbSuperStockist;
        }
        if ($dropdownStockist != "") {
            $where_clause_outer .= " AND o.distributorid = " . $dropdownStockist;
        }
        if ($divShopdropdown != "") {
            $where_clause_outer .= " AND o.shop_id = " . $divShopdropdown;
        }
        if ($area != "") {
            $where_clause_outer .= " AND s.suburbid = " . $area;
        }
        if ($subarea != "") {
            $where_clause_outer .= " AND s.subarea_id = " . $subarea;
        }
        if ($city != "") {
            $where_clause_outer .= " AND s.city = " . $city;
        }
        if ($dropdownState != "") {
            $where_clause_outer .= " AND s.state = " . $dropdownState;
        }
        if ($dropdownCategory != "") {
            $where_clause_inner .= " AND od.cat_id = " . $dropdownCategory;
        }
        if ($dropdownProducts != "") {
            $where_clause_inner .= " AND od.product_id = " . $dropdownProducts;
        }


        $order_sql = "SELECT distinct o.order_no as oid, o.order_no, o.invoice_no, 
        o.ordered_by_userid,  o.order_date, o.product_quantity,u.firstname
        FROM `tbl_order_stockist` AS o 
        LEFT JOIN `tbl_user` AS u ON u.id = o.ordered_by_userid
        WHERE 1=1 AND o.order_status_ssss = " . $order_status . " $where_clause_outer $where_clause_inner
        ORDER BY o.order_date DESC ";

        //exit();

         //o.shop_order_status = ".$order_status."
        $orders_result = mysqli_query($this->local_connection, $order_sql);
        $row_orders_count = mysqli_num_rows($orders_result);
        $i = 1;
        $all_orders = array();
        if ($row_orders_count > 0) {
            while ($row_orders = mysqli_fetch_assoc($orders_result)) {
                $all_orders[$i] = $row_orders;
                $i++;
            }
        }

        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }
    public function getOrderschnage_wise($order_status, $order_id = null) { //for orders page search
        extract($_POST);
        $usertype = $_SESSION[SESSION_PREFIX . 'user_type'];
        $userid = $_SESSION[SESSION_PREFIX . 'user_id'];

        /* if(($usertype=='Admin'||$usertype=='Accountant') && $supp_chnz_status !="" && $order_status=='11'){
          $sup_chnz_statuscond.=" ";
          }else if(($usertype=='Admin'||$usertype=='Accountant') && $supp_chnz_status !="" && $order_status!='11'){
          $sup_chnz_statuscond .= " AND od.supp_chnz_status = " . $supp_chnz_status;
          }else if(($usertype=='Distributor'|| $usertype=='Superstockist')&& $supp_chnz_status !="" ){
          $sup_chnz_statuscond .= " AND od.supp_chnz_status = " . $supp_chnz_status;
          } */


        //$order_status = substr($order_status, 0, 1);

        $where_clause_outer = '';
        $where_clause_inner = '';
        if ($usertype == "Distributor") {
            if ($userid != "") {
                $where_clause_outer .= " AND o.distributorid = " . $userid;
            }
        }
        if ($usertype == "Superstockist") {
            if ($userid != "") {
                $where_clause_outer .= " AND o.superstockistid = " . $userid;
            }
        }

        if ($order_id != "") {
            $where_clause_outer .= " AND od.order_id = " . $order_id;
        }
        if ($frmdate != '') {
            $where_clause_outer .= " AND date_format(o.order_date, '%d-%m-%Y') = '" . $frmdate . "' ";
        }
        if ($dropdownSalesPerson != "") {
            $where_clause_outer .= " AND o.ordered_by = " . $dropdownSalesPerson;
        }
        if ($cmbSuperStockist != "") {
            $where_clause_outer .= " AND o.superstockistid = " . $cmbSuperStockist;
        }
        if ($dropdownStockist != "") {
            $where_clause_outer .= " AND o.distributorid = " . $dropdownStockist;
        }
        if ($divShopdropdown != "") {
            $where_clause_outer .= " AND o.shop_id = " . $divShopdropdown;
        }
        if ($area != "") {
            $where_clause_outer .= " AND s.suburbid = " . $area;
        }
        if ($subarea != "") {
            $where_clause_outer .= " AND s.subarea_id = " . $subarea;
        }
        if ($city != "") {
            $where_clause_outer .= " AND s.city = " . $city;
        }
        if ($dropdownState != "") {
            $where_clause_outer .= " AND s.state = " . $dropdownState;
        }
        if ($dropdownCategory != "") {
            $where_clause_inner .= " AND od.cat_id = " . $dropdownCategory;
        }
        if ($dropdownProducts != "") {
            $where_clause_inner .= " AND od.product_id = " . $dropdownProducts;
        }


         $order_sql = "SELECT distinct o.id as oid, o.order_no, o.invoice_no, 
        o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
        o.order_date, 
        o.shop_id,  s.name AS shop_name,
        s.suburbid, (SELECT sb.suburbnm FROM tbl_area AS sb WHERE sb.id = s.suburbid) AS region_name,
        o.total_items, 
        o.total_cost, o.total_order_gst_cost, o.lat, 
        o.long, o.offer_provided,u.firstname AS stockistnm 
        FROM `tbl_orders` AS o      
        LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
        LEFT JOIN tbl_order_details AS od ON od.order_id = o.id 
        LEFT JOIN `tbl_user` AS u ON u.id = o.distributorid
        WHERE 1=1 AND od.order_status = " . $order_status . " $where_clause_outer $where_clause_inner $sup_chnz_statuscond
        ORDER BY o.order_date DESC "; //o.shop_order_status = ".$order_status."
        $orders_result = mysqli_query($this->local_connection, $order_sql);
        $row_orders_count = mysqli_num_rows($orders_result);
        $i = 1;
        $all_orders = array();
        if ($row_orders_count > 0) {
            while ($row_orders = mysqli_fetch_assoc($orders_result)) {
                $all_orders[$i] = $row_orders;
                $i++;
            }
        }

        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }

    public function getOrdersDetailschnage($order_id) {
        extract($_POST);

        if ($order_id != "") {
            $where_clause_outer .= " AND od.order_id = " . $order_id;
        }
         $order_details_sql = "SELECT od.id as odid, od.order_id, 
                  od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
                od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
                od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
                (SELECT p.added_by_userid FROM tbl_product AS p WHERE p.id = od.product_id) AS added_by_userid,             
                od.producthsn,
                od.product_variant_id, od.product_quantity, 
                od.product_variant_weight1, od.product_variant_unit1, 
                od.product_variant_weight2, od.product_variant_unit2, 
                od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
                od.campaign_applied, od.campaign_type, od.campaign_sale_type, od.order_status, 
                od.delivery_assing_date, od.delivery_assign_to, od.transport_date, 
                od.challan_no, od.vehicle_no, od.transport_mode, od.date_time_supply, od.place_of_supply, 
                od.quantity_delivered, od.delivery_date, od.amount_paid, od.payment_date,
                od.free_product_details,
                od.payment_status,
                od.comment,
                odis.discount_amount               
                FROM `tbl_order_details` AS od              
                LEFT JOIN tbl_order_cp_discount AS odis ON  od.id = odis.order_variant_id
                WHERE 1=1 $where_clause_outer";
        //,odpw.discounted_totalcost
        //LEFT JOIN tbl_campaign_price_weight AS odpw ON odpw.odid = od.id
        //AND od.order_status = ".$order_status." $where_clause_inner
        $orders_det_result = mysqli_query($this->local_connection, $order_details_sql);
        //$row_orders_det_count = mysqli_num_rows($orders_det_result);
        if (mysqli_num_rows($orders_det_result) > 0) {
            while ($row_orders_det = mysqli_fetch_assoc($orders_det_result)) {
                $all_orders['order_details'][] = $row_orders_det;
            }
        }
        return $all_orders;
    }
    

    public function getOrders_shopwise($order_status, $order_id = null) {
        /* echo "hi";
          exit(); */
        extract($_POST);
        //echo $frmdate;
        /* var_dump($_POST); */
        $where_clause_outer = '';
        $where_clause_inner = '';
        if ($order_id != '') {
            $where_clause_outer .= " AND o.id = '" . $order_id . "' ";
        }
        if ($frmdate != '') {
            $where_clause_outer .= " AND date_format(o.order_date, '%d-%m-%Y') = '" . $frmdate . "' ";
        }
        if ($dropdownSalesPerson != "") {
            $where_clause_outer .= " AND o.ordered_by = " . $dropdownSalesPerson;
        }
        if ($cmbSuperStockist != "") {
            $where_clause_outer .= " AND o.superstockistid = " . $cmbSuperStockist;
        }
        if ($dropdownStockist != "") {
            $where_clause_outer .= " AND o.distributorid = " . $dropdownStockist;
        }
        if ($divShopdropdown != "") {
            $where_clause_outer .= " AND o.shop_id = " . $divShopdropdown;
        }
        if ($area != "") {
            $where_clause_outer .= " AND s.suburbid = " . $area;
        }
        if ($subarea != "") {
            $where_clause_outer .= " AND s.subarea_id = " . $subarea;
        }
        if ($city != "") {
            $where_clause_outer .= " AND s.city = " . $city;
        }
        if ($dropdownState != "") {
            $where_clause_outer .= " AND s.state = " . $dropdownState;
        }
        if ($dropdownCategory != "") {
            $where_clause_inner .= " AND od.cat_id = " . $dropdownCategory;
        }
        if ($dropdownProducts != "") {
            $where_clause_inner .= " AND od.product_id = " . $dropdownProducts;
        }

        switch ($_SESSION[SESSION_PREFIX . 'user_type']) {
            case "Admin":
                $sql = "SELECT o.order_date,o.order_no,s.name as shopnm,od.product_quantity,od.p_cost_cgst_sgst,od.product_unit_cost FROM `tbl_orders` AS o 
                 LEFT JOIN `tbl_order_details` AS od ON od.order_id = o.id
                 LEFT JOIN `tbl_shops` AS s ON s.id = o.shop_id
                 WHERE 1=1 $where_clause_outer AND od.order_status = " . $order_status . " $where_clause_inner
                 ORDER BY o.order_date,shopnm,order_no ";
                break;
            case "Superstockist":
                $sql = "SELECT o.order_date,o.order_no,s.name as shopnm,od.product_quantity,od.p_cost_cgst_sgst,od.product_unit_cost FROM `tbl_orders` AS o 
                 LEFT JOIN `tbl_order_details` AS od ON od.order_id = o.id
                 LEFT JOIN `tbl_shops` AS s ON s.id = o.shop_id
                  WHERE od.status='2' AND superstockistid='" . $_SESSION[SESSION_PREFIX . 'user_id'] . "' ORDER BY o.order_date,shopnm,order_no";
                break;
            case "Distributor":
                $sql = "SELECT o.order_date,o.order_no,s.name as shopnm,od.product_quantity,od.p_cost_cgst_sgst,od.product_unit_cost FROM `tbl_orders` AS o 
                 LEFT JOIN `tbl_order_details` AS od ON od.order_id = o.id
                 LEFT JOIN `tbl_shops` AS s ON s.id = o.shop_id
                  WHERE od.status = 1 AND distributorid='" . $_SESSION[SESSION_PREFIX . 'user_id'] . "' ORDER BY o.order_date,shopnm,order_no ";
                break;
        }

        $orders_det_result = mysqli_query($this->local_connection, $sql);
        if (mysqli_num_rows($orders_det_result) > 0) {
            return $orders_det_result;
        } else {
            unset($all_orders);
        }
        //return $row = mysqli_fetch_assoc($orders_det_result);
        //  var_dump( $orders_det_result);
        /* while($row1 = mysqli_fetch_array($orders_det_result)) 
          {
          echo "<pre>";
          print_r($row);
          }
          exit(); */
        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }

    

    //new method for supp_chnz_status maintainance
    public function getOrdersSuppChnz($supp_chnz_status) {
        $order_details_sql = "SELECT od.id, od.order_id, 
                od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
                od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
                od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
                od.producthsn,
                od.product_variant_id, od.product_quantity, 
                od.product_variant_weight1, od.product_variant_unit1, 
                od.product_variant_weight2, od.product_variant_unit2, 
                od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
                od.campaign_applied, od.campaign_type, od.campaign_sale_type, od.order_status, 
                od.delivery_assing_date, od.delivery_assign_to, od.transport_date, 
                od.challan_no, od.vehicle_no, od.transport_mode, od.date_time_supply, od.place_of_supply, 
                od.quantity_delivered, od.delivery_date, od.amount_paid, od.payment_date,
                odis.discount_amount
                FROM `tbl_order_details` AS od
                LEFT JOIN tbl_order_cp_discount AS odis ON odis.order_variant_id = od.id
                WHERE  od.supp_chnz_status = " . $supp_chnz_status . " 
                group by od.product_id ";
        $orders_det_result = mysqli_query($this->local_connection, $order_details_sql);
        return $orders_det_result;
    }

    //new method for supp_chnz_status maintainance
    public function getOrderDetailsByproductvarId($prod_var_id) {
        $sql = "SELECT o.id, o.order_no, o.invoice_no, 
        o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
        o.order_date, 
        o.shop_id,  (SELECT s.name  FROM tbl_shops AS s WHERE s.id = o.shop_id) AS shop_name,
        o.total_items, 
        o.total_cost, o.total_order_gst_cost, o.lat, 
        o.long, o.offer_provided, 
        od.id, od.order_id, 
        od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
        od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
        od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
        od.producthsn, od.product_variant_id, od.product_quantity, 
        od.product_variant_weight1, od.product_variant_unit1, 
        od.product_variant_weight2, od.product_variant_unit2, 
        od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
        od.campaign_applied, od.campaign_type, od.campaign_sale_type,
        od.order_status, od.delivery_assing_date, od.delivery_assign_to,
        od.transport_date, od.challan_no,od.vehicle_no, od.transport_mode, 
        od.date_time_supply, od.place_of_supply, od.quantity_delivered, od.delivery_date, 
        od.amount_paid, od.payment_date,
        odis.discount_amount
        FROM `tbl_orders` AS o      
        LEFT JOIN tbl_order_details AS od ON od.order_id = o.id
        LEFT JOIN tbl_order_cp_discount AS odis ON odis.order_variant_id = od.id
        WHERE od.product_variant_id = " . $prod_var_id;
        $orders_det_result = mysqli_query($this->local_connection, $sql);
        return $orders_det_result;
    }

    //new methods
    public function getProductname($prodid) {
        $sql = "SELECT productname
        FROM `tbl_product` 
        WHERE id = " . $prodid;
        $prod_name_result = mysqli_query($this->local_connection, $sql);
        return $row = mysqli_fetch_assoc($prod_name_result);
    }

    public function getOrderDetailsById($order_details_id) {
         $sql = "SELECT o.id, o.order_no, o.invoice_no, 
        o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
        o.order_date, o.margin_percentage, 
        o.shop_id,  (SELECT s.name  FROM tbl_shops AS s WHERE s.id = o.shop_id) AS shop_name,
        o.total_items, 
        o.total_cost, o.total_order_gst_cost, o.lat, 
        o.long, o.offer_provided, 
        od.id, od.order_id, 
         od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
        od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
        od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
        od.producthsn, od.product_variant_id, od.product_quantity, 
        od.product_variant_weight1, od.product_variant_unit1, 
        od.product_variant_weight2, od.product_variant_unit2, 
        od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
        od.campaign_applied, od.campaign_type, od.campaign_sale_type,
        od.order_status, od.delivery_assing_date, od.delivery_assign_to,
        od.transport_date, od.challan_no,od.vehicle_no, od.transport_mode, 
        od.date_time_supply, od.place_of_supply, od.quantity_delivered, od.delivery_date, 
        od.amount_paid, od.payment_date,
        od.free_product_details,
        odis.discount_amount        
        FROM `tbl_orders` AS o      
        LEFT JOIN tbl_order_details AS od ON od.order_id = o.id
        LEFT JOIN tbl_order_cp_discount AS odis ON odis.order_variant_id = od.id
        WHERE od.id = " . $order_details_id;
        $orders_det_result = mysqli_query($this->local_connection, $sql);
        $row_orders_det_count = mysqli_num_rows($orders_det_result);
        return $row = mysqli_fetch_assoc($orders_det_result);
    }

    
    public function get_opening_balance($shop_id) {
        $sql_pending_amount = "SELECT ROUND(SUM(p_cost_cgst_sgst),2) AS amount_to_pay
        FROM `tbl_order_details` AS od
        LEFT JOIN `tbl_orders` AS o ON o.id = od.order_id
        WHERE o.shop_id = " . $shop_id . " AND od.order_status IN (4,7,8)
        GROUP BY o.shop_id";
        $orders_payment_result = mysqli_query($this->local_connection, $sql_pending_amount);
        $row_payment_count = mysqli_num_rows($orders_payment_result);
        $row_payment = mysqli_fetch_assoc($orders_payment_result);

        $sql_paid_amount = "SELECT ROUND(SUM(paid_amount),2) AS amount_paid
        FROM `tbl_shop_payment` AS sp       
        WHERE sp.shop_id = " . $shop_id . "
        GROUP BY sp.shop_id";
        $orders_paid_result = mysqli_query($this->local_connection, $sql_paid_amount);
        $row_paid_count = mysqli_num_rows($orders_paid_result);
        $row_paid = mysqli_fetch_assoc($orders_paid_result);
        //echo $row_payment['amount_to_pay']." - ".$row_paid['amount_paid'];
        $balance = array();
        $balance['amount_to_pay'] = $row_payment['amount_to_pay'];
        $balance['amount_paid'] = $row_paid['amount_paid'];
        $balance['balance_amount'] = $row_payment['amount_to_pay'] - $row_paid['amount_paid'];
        if ($balance['balance_amount'] > 0) {
            return $balance;
        } else {
            return 0;
        }
    }

    function fnGetUnit($id) {
        $sql = "SELECT unitname FROM tbl_units WHERE id='" . $id . "'";
        $result = mysqli_query($this->local_connection, $sql);
        $row = mysql_fetch_array($result);
        return $row['unitname'];
    }
    //new methods for adding new report of salesperson todays total work 
   function fnGet_hours_worked_today($id,$datefor) {
        $where='';
        if(!empty($datefor)||$datefor!=''){
            $where = " and date_format(sa.tdate,'%d-%m-%Y') =  date_format('".$datefor."','%d-%m-%Y') " ;
        }
        $sql = "SELECT (UNIX_TIMESTAMP(sa.dayendtime) - UNIX_TIMESTAMP(sa.tdate)) / 60.0 / 60.0 as hours_difference
            , sa.todays_travelled_distance
                from tbl_sp_attendance sa
               LEFT JOIN tbl_user u ON sa.sp_id = u.id where 1=1 and 
               sa.sp_id='".$id."' $where
               ";       
        $result = mysqli_query($this->local_connection, $sql);
        $row = mysql_fetch_array($result);
         $i = 0;
        $row_count = mysqli_num_rows($result);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    }
    public function get_shop_summary()
    {

        $user_role = $_SESSION[SESSION_PREFIX.'user_role'];
        $user_id = $_SESSION[SESSION_PREFIX.'user_id'];
        if ($user_role =='Accountant') 
        {
            $user_id =1;
        }    
        $user_condition="";
        $user_condition.=" AND find_in_set('".$user_id."',u.parent_ids) <> 0 ";

        $sql="SELECT COUNT(s.id) as total_shop,u.parent_ids,s.name,u.firstname,s.shop_added_by,s.shop_status from tbl_shops s
        LEFT JOIN tbl_user u ON s.shop_added_by = u.id
        where 1=1 $user_condition
        GROUP BY s.shop_added_by HAVING s.shop_status=0"; 
        $result = mysqli_query($this->local_connection, $sql);
        $row_count = mysqli_num_rows($result);
        if ($row_count > 0) {
            return $result;
        } else {
            return 0;
        }
    }
    public function get_sale_summary()
    {
        $user_role = $_SESSION[SESSION_PREFIX.'user_role'];
        $user_id = $_SESSION[SESSION_PREFIX.'user_id'];
        if ($user_role =='Accountant') 
        {
            $user_id =1;
        }   
        $date = date('d-m-Y');
        $condnsearch = " AND date_format(o.order_date, '%d-%m-%Y') = '" . $date . "' ";   
        $user_condition="";
        $user_condition.=" AND find_in_set('".$user_id."',u.parent_ids) <> 0 ";
        $sql="select u.id,u.firstname as name,sum(od.p_cost_cgst_sgst) as total_sales from tbl_user u 
        left join tbl_orders o on u.id=o.order_to   
        LEFT JOIN tbl_order_details od ON od.order_id =o.id  
        where 1=1 $user_condition $condnsearch AND od.p_cost_cgst_sgst>0
        group by u.id";
        $result = mysqli_query($this->local_connection, $sql);
        $row_count = mysqli_num_rows($result);
        if ($row_count > 0) {
            return $result;
        } else {
            return 0;
        }
    }
    public function get_shop_count($shop_added_by)
    {
        $sql="SELECT COUNT(id) as total_shop from tbl_shops where shop_added_by =".$shop_added_by; 
        $result2 = mysqli_query($this->local_connection, $sql);
        $row_count = mysqli_num_rows($result2);
        if ($row_count > 0) {
            return $result2;
        } else {
            return 0;
        }
    }
    function fnGet_todays_expense_bill($id,$datefor) {
        $where='';
        if(!empty($datefor)||$datefor!=''){
            $where = " and date_format(tada.date_tada,'%d-%m-%Y') =  date_format('".$datefor."','%d-%m-%Y') " ;
        }
       $sql="SELECT u.firstname,u.id as spid,tada.* ,tmt.van_type
        FROM tbl_sp_tadabill tada  
        left join tbl_user  u on tada.userid=u.id
        left join tbl_mode_transe  tmt on tada.mode_of_transe=tmt.id  
        where 1=1 and tada.userid='".$id."' ".$where." order by date_tada ,firstname";
        //date_format(TUL.tdate, '%d-%m-%Y') = '".$frmdate."' ";
                
        $result = mysqli_query($this->local_connection, $sql);
        $row = mysql_fetch_array($result);
         $i = 0;
        $row_count = mysqli_num_rows($result);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    }
    
     //end new methods for adding new report of salesperson todays total work 
    //getMyOrdersStockist
    //b2c 
    function getMyOrdersCount() {
        $user_id=$_SESSION[SESSION_PREFIX . "user_id"];
         $sql = "SELECT distinct order_no  FROM tbl_order_stockist WHERE ordered_by_userid='" . $user_id . "'";
        $result = mysqli_query($this->local_connection, $sql);
       $records=array();
        $row_count = mysqli_num_rows($result);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $records[] = $row['order_no'];
            }
            return $records;
        } else {
            return 0;
        } 
    }

    function getStockistOrdersCount($order_status) {
       // $user_id=$_SESSION[SESSION_PREFIX . "user_id"];
         $sql = "SELECT distinct order_no  FROM tbl_order_stockist WHERE order_status_ssss='" . $order_status . "'";
        $result = mysqli_query($this->local_connection, $sql);
       $records=array();
        $row_count = mysqli_num_rows($result);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $records[] = $row['order_no'];
            }
            return $records;
        } else {
            return 0;
        } 
    }
    function getSuperStockistOrdersCount() {
       // $user_id=$_SESSION[SESSION_PREFIX . "user_id"];
            $order_status1 = 1;
            $order_status2 = 2;
            $order_status3 = 5;
         $sql = "SELECT distinct order_no  FROM tbl_order_stockist WHERE order_status_ssss='" . $order_status1 . "' OR order_status_ssss='" . $order_status2 . "'OR order_status_ssss='" . $order_status3 . "'";
        $result = mysqli_query($this->local_connection, $sql);
       $records=array();
        $row_count = mysqli_num_rows($result);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $records[] = $row['order_no'];
            }
            return $records;
        } else {
            return 0;
        } 
    }
    
    
    function getMyOrdersStockist($order_no) {
        $user_id=$_SESSION[SESSION_PREFIX . "user_id"];
         $sql = "SELECT * FROM tbl_order_stockist WHERE order_no='".$order_no."' and ordered_by_userid='" . $user_id . "'";
        $result = mysqli_query($this->local_connection, $sql);
        
        $i = 0;
        $row_count = mysqli_num_rows($result);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    }
    function getStockistPlacedOrders($order_no) {
        $user_id=$_SESSION[SESSION_PREFIX . "user_id"];
         $sql = "SELECT * FROM tbl_order_stockist WHERE order_no='".$order_no."' ";
        $result = mysqli_query($this->local_connection, $sql);
        
        $i = 0;
        $row_count = mysqli_num_rows($result);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    }

    public function generate_invoice_no() {
        $sql = "SELECT invoice_no FROM `tbl_orders` WHERE invoice_no != '' ORDER BY id DESC LIMIT 1 ";
        $result = mysqli_query($this->local_connection, $sql);
        $row_count = mysqli_num_rows($result);
        $row = mysqli_fetch_assoc($result);
        $part1 = 'SJFP';
        if (date("m") >= 4) {
            $part2 = date('y') . '-' . date("y", strtotime("+1 year"));
        } else {
            $part2 = date("y", strtotime("-1 year")) . '-' . date('y');
        }
        $inc_number = 5000;
        if ($row_count == 0) {
            $inc_number = 5000;
        } else {
            $invoice_part = explode('/', $row['invoice_no']);
            $inc_number = $invoice_part[2] + 1;
        }
        $part3 = $inc_number;
        $invoice_no = $part1 . '/' . $part2 . '/' . $part3;
        return $invoice_no;
    }
    public function get_summary_for_mail_newdb($client_id){     
        //$date = date('d-m-Y');
        $date =date('d-m-Y',strtotime("-1 days"));
        $condnsearch="  date_format(oa.order_date, '%d-%m-%Y') = '".$date."' ";
                    
         $sql="SELECT count(oa.id) as Todays_orders ,sum(vo.p_cost_cgst_sgst) as Todays_Sales 
                FROM tbl_orders oa   
                LEFT JOIN tbl_order_details vo ON oa.id = vo.order_id 
                where ".$condnsearch."";    
        $result = mysqli_query($this->local_connection,$sql);
        
        $sqltotalsale="SELECT count(oa.id) as Total_orders,sum(vo.p_cost_cgst_sgst) as Total_Sales 
                FROM tbl_order_details vo  
                LEFT JOIN tbl_orders oa ON  vo.order_id = oa.id";   
        $result5 = mysqli_query($this->local_connection,$sqltotalsale);
        $row5 = mysqli_fetch_assoc($result5);
        $records['Total_orders']=$row5['Total_orders'];
        $records['Total_Sales']=$row5['Total_Sales'];
        
        $sql2="select database() as manufacturer";
        $result2 = mysqli_query($this->local_connection,$sql2);
        $row2 = mysqli_fetch_assoc($result2);
        $output = str_replace("salzpoin_","",$row2['manufacturer']);
        
        
        $sql3="select count(s.id) as Total_shops_added_count from tbl_shops  s where date_format(s.added_on, '%d-%m-%Y') = '".$date."' ";
        $result3 = mysqli_query($this->local_connection,$sql3);
        $row3 = mysqli_fetch_assoc($result3);
        $records['Total_shops_added_count']=$row3['Total_shops_added_count'];
        
        $sql4="select count(s.id) as Total_shops_exists from tbl_shops  s ";
        $result4 = mysqli_query($this->local_connection,$sql4);
        $row4 = mysqli_fetch_assoc($result4);
        $records['Total_shops_exists']=$row4['Total_shops_exists'];


        $sql5="SELECT * FROM tbl_clients_maintenance where id = '".$client_id."' ";
    //  $result5 = mysqli_query($conmain,$sql5);
        $result5 = mysqli_query($this->common_connection,$sql5);        
        $row5 = mysqli_fetch_assoc($result5);
        $records['client_id']=$row5['id'];
        $records['clientnm']=$row5['clientnm'];
        $records['dateofcontract']=$row5['dateofcontract'];
        
        //todays salesperson added
        $sql_todays_sp_count="select count(s.id) as todays_sp_count from tbl_user  s where date_format(s.added_on, '%d-%m-%Y') = '".$date."' and user_type='SalesPerson' ";
        $result_todays_sp_count = mysqli_query($this->local_connection,$sql_todays_sp_count);
        $row_todays_sp_count = mysqli_fetch_assoc($result_todays_sp_count);
        $records['todays_sp_count']=$row_todays_sp_count['todays_sp_count'];
        
        //total salesperson added till date
        $sql_total_sp_count="select count(s.id) as total_sp_count from tbl_user s where  user_type='SalesPerson'";
        $result_total_sp_count = mysqli_query($this->local_connection,$sql_total_sp_count);
        $row_total_sp_count = mysqli_fetch_assoc($result_total_sp_count);
        $records['total_sp_count']=$row_total_sp_count['total_sp_count'];
        
        $records['db']=$output;
        $row_count = mysqli_num_rows($result);      
        while($row = mysqli_fetch_assoc($result)){          
            $records['summary']['Todays_orders'] = $row['Todays_orders'];           
            $records['summary']['Todays_Sales'] = $row['Todays_Sales'];
        }           
        return $records;
    }
    

}

?>