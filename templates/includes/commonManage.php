<?php
/***********************************************************
 * File Name	: commonManage.php
 ************************************************************/	
class commonManage 
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
	}
	public function log_add_record($table_name,$record_id,$comment){
		$added_on = date("Y-m-d H:i:s");
		$added_by = $_SESSION[SESSION_PREFIX.'user_id'];
		
		$sql = "INSERT INTO tbl_record_tracker (`table_name`,`record_id`,`added_on`,`added_by`, `comment`) 
		VALUES('".$table_name."','".$record_id."','".$added_on."','".$added_by."','".fnEncodeString($comment)."')";
		mysqli_query($this->local_connection,$sql);				
	}
	public function log_update_record($table_name,$record_id,$comment){
		$updated_on = date("Y-m-d H:i:s");
		$updated_by = $_SESSION[SESSION_PREFIX.'user_id'];		
		
		$sql = "INSERT INTO tbl_record_tracker (`table_name`,`record_id`,`updated_on`,`updated_by`, `comment`) 
		VALUES('".$table_name."','".$record_id."','".$updated_on."','".$updated_by."','".fnEncodeString($comment)."')";
		mysqli_query($this->local_connection,$sql);		
	}
	public function log_delete_record($table_name,$record_id,$comment){
		$deleted_on = date("Y-m-d H:i:s");
		$deleted_by = $_SESSION[SESSION_PREFIX.'user_id'];
		
		$sql = "INSERT INTO tbl_record_tracker (`table_name`,`record_id`,`deleted_on`,`deleted_by`, `comment`)  
		VALUES('".$table_name."','".$record_id."','".$deleted_on."','".$deleted_by."','".fnEncodeString($comment)."')";
		mysqli_query($this->local_connection,$sql);		
	}
	public function log_get_commonclienttype(){
		$sql = "select client_type from  tbl_clients_maintenance where id=".COMPID." limit 1 ";
		$result= mysqli_query($this->common_connection,$sql);	
		$result1 = mysqli_fetch_array($result);
		return $result1['client_type'];
	}	
	public function getPageIDforUrlAdd($php_page_name){
		 $sql1="SELECT `id` as page_id FROM tbl_pages where php_page_add='".$php_page_name."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		$resultarray=array();
		if($row_count > 0){				
			$row_url = mysqli_fetch_assoc($result1);
			return $row_url;		
		}else
			return $row_count;		
	}
	public function getURLforAdd($profile_id,$page_id_url){
		 $sql1="SELECT ischecked_add FROM tbl_action_profile where profile_id='".$profile_id."' AND  page_id='".$page_id_url."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		$resultarray=array();
		if($row_count > 0){				
			$row_url = mysqli_fetch_assoc($result1);
			return $row_url;		
		}else
			return $row_count;		
	}
	public function getPageIDforUrlEdit($php_page_name){
		  $sql1="SELECT `id` as page_id FROM tbl_pages where php_page_edit='".$php_page_name."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		$resultarray=array();
		if($row_count > 0){				
			$row_url = mysqli_fetch_assoc($result1);
			return $row_url;		
		}else
			return $row_count;		
	}
	public function getURLforEdit($profile_id,$page_id_url){
		 $sql1="SELECT ischecked_edit FROM tbl_action_profile where profile_id='".$profile_id."' AND  page_id='".$page_id_url."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		$resultarray=array();
		if($row_count > 0){				
			$row_url = mysqli_fetch_assoc($result1);
			return $row_url;		
		}else
			return $row_count;		
	}

	
}
?>