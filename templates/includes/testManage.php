<?php
/***********************************************************
 * File Name	: testManager.php
 ************************************************************/	
date_default_timezone_set('Asia/Kolkata');
class testManager
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public $parent_result='';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
		$this->parent_result = array();
		//$this->commonObj 	= 	new commonManage($this->local_connection,$this->common_connection);
	}	

	public function getTest() {		
		/* $sql_depth="SELECT depth as maxdepth,user_type FROM tbl_user order by depth desc limit 1";	*/
		$seesion_user_id=$_SESSION[SESSION_PREFIX.'user_id'];
		$seesion_user_type=$_SESSION[SESSION_PREFIX.'user_type'];
      /*  $sql_depth="SELECT depth as lowdepth,user_type FROM tbl_user where id ='".$seesion_user_id."'";	*/
          $sql_ptype="SELECT id as profile_id FROM tbl_user_tree where user_type  ='".$seesion_user_type."'";
          $result_ptype = mysqli_query($this->local_connection,$sql_ptype);
           $row_ptype = mysqli_fetch_assoc($result_ptype);
              $profile_id = $row_ptype['profile_id'];
         //exit();
        $sql1="SELECT distinct(`user_type`),`user_role`,id,depth FROM tbl_user_tree 
			where find_in_set('".$profile_id."',all_parent_id) <> 0  AND isdeleted!=1 ORDER BY user_type ASC";
         $result1 = mysqli_query($this->local_connection,$sql1);
			$row_count = mysqli_num_rows($result1);
			if($row_count > 0){	
				return $result1;		
			}else
				return $row_count;
		/*  $sql1="SELECT distinct(`user_type`),depth as maxdepth,`user_role` FROM tbl_user 
			where find_in_set('".$user_id."',parent_ids) <> 0  AND isdeleted!=1";*/
		/*$result_depth = mysqli_query($this->local_connection,$sql_depth);
		$row_count_depth = mysqli_num_rows($result_depth);
		if($row_count_depth>0){
			$row_depth=mysqli_fetch_assoc($result_depth);		
			$lowdepth=$row_depth['lowdepth'];
			$user_type=$row_depth['user_type'];			
			$sql1="SELECT * FROM tbl_user_tree where (id!=1 and depth > '".$lowdepth."' )
			 or parent_id=(select id from tbl_user_tree where user_type='".$user_type."' ) order by depth ASC";
			//exit();
			$result1 = mysqli_query($this->local_connection,$sql1);
			$row_count = mysqli_num_rows($result1);
			if($row_count > 0){	
				return $result1;		
			}else
				return $row_count;
		}else{
			return $row_count_depth;
		}	*/	
	}
	public function getAll_usertree() {
		$usertree_table=array();		
		$sql1="SELECT `id`, `user_role`, `user_type`, `parent_id`, `depth` FROM tbl_user_tree  order by id ASC";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			 while ($row = mysqli_fetch_array($result1)){
				$usertype_id=$temp['id'];
				$temp['id']=$row['id'];
				$temp['user_role']=$row['user_role'];
				$temp['user_type']=$row['user_type'];
				$temp['parent_id']=$row['parent_id'];				
				$temp['depth']=$row['depth'];
				$usertree_table[$usertype_id]=$temp;
			 }
			return $usertree_table;		
		}else
			return $row_count;
	}
	public function checkParentIds($id, $data = array()) {		
		 $sql_parent="SELECT parent_id,user_type FROM tbl_user_tree WHERE id = '$id'";
		//echo "<br>";
		$parent = mysqli_query($this->local_connection,$sql_parent);
		$parent_query = mysqli_fetch_assoc($parent);		
		if ($parent_query['parent_id'] > 0) {
			$temp['parent_id']= $parent_query['parent_id'];
			$temp['user_type']= $parent_query['user_type'];
			$data[] = $temp;			
			$this->checkParentIds($parent_query['parent_id'], $data);
		} else {
			 $this->parent_result = $data;	
		}
		return $this->parent_result;
	}



	public function getChields($user_type) {		
		//$parentdepth=$depth-1;		
		//$seesion_user_id=$_SESSION[SESSION_PREFIX.'user_id'];
	    $sql1="SELECT * FROM tbl_user where user_type='".$user_type."'   AND isdeleted!=1 ORDER BY firstname ASC";		
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;			
	}
	public function getSessionUser() {	
		$user_details=array();
	     $sql1="SELECT id,user_role,firstname,user_type,parent_ids,parent_names,parent_usertypes,depth
		FROM tbl_user_view 
		where id='".$_SESSION[SESSION_PREFIX.'user_id']."'  ORDER BY firstname ASC";		
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			while ($row_user_details = mysqli_fetch_assoc($result1)){ 
				$user_details[]=$row_user_details;
			} 
			return $user_details;		
		}else
			return $row_count;			
	}
	public function onchange_getrole($usertype_id) {		
		//$parentdepth=$depth-1;		
	    $sql1="SELECT * FROM tbl_user_tree where id='".$usertype_id."' AND isdeleted!=1 limit 1";		
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;			
	}
	public function getChields_byuserid($user_id,$user_type) {	
	    $sql1="SELECT * FROM tbl_user where external_id='".$user_id."' AND user_type='".$user_type."' AND isdeleted!=1 and (user_role='Superstockist' or user_role='Distributor' )  ";		
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;			
	}
	public function getUsertype_underme_byuserid($user_id) {
		$user_role = $_SESSION[SESSION_PREFIX.'user_role'];
		if ($user_role =='Accountant') 
		{
				$user_id =1;
		}
	    $sql1="SELECT distinct(`user_type`),`user_role` FROM tbl_user 
			where find_in_set('".$user_id."',parent_ids) <> 0  AND isdeleted!=1 order by user_type ASC";		
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;			
	}
	public function getUsertype_underme_byuseridrole($user_id) {
		$user_role = $_SESSION[SESSION_PREFIX.'user_role'];
		if ($user_role =='Accountant') 
		{
				$user_id =1;
		}
	    $sql1="SELECT distinct(`user_type`),`user_role` FROM tbl_user 
			where find_in_set('".$user_id."',parent_ids) <> 0  AND isdeleted!=1 AND user_role NOT IN ('Accountant', 'DeliveryChannelPerson','DeliveryPerson') order by user_type ASC";		
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;			
	}
	public function get_customer_details($user_id) {
		$user_role = $_SESSION[SESSION_PREFIX.'user_role'];
		if ($user_role =='Accountant') 
		{
				$user_id =1;
		}
	    $sql1="SELECT `cust_id`, `cust_name`, `cust_email`, `cust_mobile`, `cust_address` FROM tbl_customer  ";//"where AND isdeleted!=1 ";		
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;			
	}
	public function getUserrole_underme_byuseridrole($user_id) {
		$user_role = $_SESSION[SESSION_PREFIX.'user_role'];
		if ($user_role =='Accountant') 
		{
				$user_id =1;
		}
	    $sql1="SELECT distinct(`user_role`) FROM tbl_user 
			where find_in_set('".$user_id."',parent_ids) <> 0  AND isdeleted!=1 AND user_role NOT IN ('Accountant', 'DeliveryChannelPerson','DeliveryPerson') order by user_type ASC";		
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;			
	}
	public function get_location_dropdown_option($querystring,$parent_id) {
			$sql='';
			$sql.=$querystring."".$parent_id;
		$result1 = mysqli_query($this->local_connection,$sql);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;			
	}
	public function onchange_get_serviceby_userid($usertype,$userrole) {		
		//$parentdepth=$depth-1;	
		$seesion_user_id=$_SESSION[SESSION_PREFIX.'user_id'];
		$seesion_user_type=$_SESSION[SESSION_PREFIX.'user_type'];
		$where_cond="";
		if($usertype==$seesion_user_type){
			$where_cond=" OR id='".$seesion_user_id."' ";
		}
	    $sql1="SELECT * FROM tbl_user where user_type='".$usertype."' AND user_role='".$userrole."' 
		AND find_in_set('".$seesion_user_id."',parent_ids) <> 0 $where_cond
		AND isdeleted!=1 ";		
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;			
	}
	public function getUsertype_underme_forshop_byuserid($user_id) {	
	     $sql1="SELECT distinct(`user_type`),`user_role`,`profile_id`			
			FROM tbl_user 
			where (find_in_set('".$user_id."',parent_ids) <> 0 OR id='".$user_id."' ) AND isdeleted!=1
			AND (user_role='Admin' OR user_role='Superstockist' OR user_role='Distributor')";		
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;			
	}
	function cron_sp_dayend_auto() {		
		$sql = "UPDATE tbl_sp_attendance SET dayendtime = now() 
		WHERE dayendtime IS NULL AND date_format(tdate, '%d-%m-%Y') = '" . date('d-m-Y') . "'";
		$result1 = mysqli_query($this->local_connection,$sql);        
    }
	function cron_order_delay_auto() {		
		$sql = "UPDATE tbl_order_details SET payment_date = DATE_ADD(payment_date,INTERVAL 3 DAY)
		WHERE payment_status='delay' AND date_format(payment_date, '%d-%m-%Y') = '" . date('d-m-Y') . "'";
		$result1 = mysqli_query($this->local_connection,$sql);        
    }
	function cron_sp_campaign_active_inactive_auto() {	
		$tomorrow = date("Y-m-d", time() + 86400);
		$sql_inactive = "UPDATE tbl_campaign SET status = '1' 
		WHERE  campaign_end_date < '" . $tomorrow . "' ";
		//"OR campaign_start_date > '" . $tomorrow . "'
		$result1 = mysqli_query($this->local_connection,$sql_inactive);

$sql2 = "DELETE e FROM tbl_sp_campaign_assign e 
					LEFT JOIN tbl_campaign j ON j.id = e.campaign_id 
					WHERE j.status = 1";
$result2 = mysqli_query($this->local_connection,$sql2);
      /* $sql1="SELECT id as compaign_id	FROM tbl_campaign 
			where status = '1' ";		
	  $result1 = mysqli_query($this->local_connection,$sql1);
      $row_count = mysqli_num_rows($result1);
      if($row_count > 0){	
			 while($row = mysqli_fetch_array($result1))
			 {
			 	 $compaign_id = $row['compaign_id'];
			 	  $sql_delete_assigned_user = "DELETE FROM tbl_sp_campaign_assign
							WHERE campaign_id = '".$compaign_id;
				  $result2 = mysqli_query($this->local_connection,$sql_delete_assigned_user);
			 }
		}*/
		/* $sql_active = "UPDATE tbl_campaign SET status = '0' 
		WHERE  campaign_end_date >= '" . $tomorrow . "' AND  
				campaign_start_date <= '" . $tomorrow . "'";
		$result1 = mysqli_query($this->local_connection,$sql_active); 



		 $sql_delete_assigned_inactive = "DELETE e FROM tbl_sp_campaign_assign e 
					LEFT JOIN tbl_campaign j ON j.id = e.campaign_id 
					WHERE j.status = 1 ";
		$result1 = mysqli_query($this->local_connection,$sql_delete_assigned_inactive); */
    }
}
?>