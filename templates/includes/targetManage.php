<?php
/***********************************************************
 * File Name	: shopManage.php
 ************************************************************/	

class targetManager
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
		$this->commonObj 	= 	new commonManage($this->local_connection,$this->common_connection);
	}	
	
	public function addTarget($user_type,$id) 
	{
		//print_r($_POST);
		//exit();	
		extract ($_POST);
		$added_by = $_SESSION[SESSION_PREFIX."user_id"];
		$assign_user_id = $id;

		if($user_type != '')
		{
			$fields.= ",`assign_user_type`";
			$values.= ",'".$user_type."'";
		}
		if($target_in_rs != '')
		{
			$fields.= ",`target_in_rs`";
			$values.= ",'".$target_in_rs."'";
		}
		if($targetType == 'daily')
		{
			$fields.= ",`start_date`";
			$values.= ",now()";
			$fields.= ",`end_date`";
			$values.= ",now()";
		}
		if($targetType == 'weekly')
		{
			$fields.= ",`start_date`";
			$values.= ",'".$drpWeeklyOption."'";
		}
		if($targetType == 'monthly')
		{
			$fields.= ",`start_date`";
			$values.= ",'".$drpMonthlyOption."'";
		}
		if($targetType == 'spdate')
		{
			$fields.= ",`start_date`";
			$values.= ",'".$frmdate."'";
			$fields.= ",`end_date`";
			$values.= ",'".$todate."'";
		}	
		if($target_in_wt != '')
		{
			$fields.= ",`target_in_wt`";
			$values.= ",'".$target_in_wt."'";
		}		
		if($variant1_wt != '')
		{
			$fields.= ",`variant1_wt`";
			$values.= ",'".$variant1_wt."'";
		}
		
				

		$added_on = date('Y-m-d H:i:s');
		$target_sql = "INSERT INTO tbl_target (`added_by`, `assign_user_id`, `target_type`, `createdon` $fields) 
		VALUES('".$added_by."','".$assign_user_id."','".$targetType."','".$added_on."' $values)";
		//exit();		
		mysqli_query($this->local_connection,$target_sql);
		//return $userid=mysqli_insert_id($this->common_connection); 
	}	
	public function getTargets() {
	   $sql1="SELECT tt.id,tt.assign_user_type,tt.assign_user_id, target_type,tt.start_date,tt.end_date,
			tu.firstname as name,tu.parent_ids as parent_ids,
		 (SELECT unitname FROM tbl_units WHERE id = tt.variant1_wt) AS unitname1,		
		 tt.target_in_rs,tt.target_in_wt,tt.createdon
		 FROM tbl_target tt
		 LEFT JOIN tbl_user tu on tt.assign_user_id=tu.id 
		 where find_in_set('".$_SESSION[SESSION_PREFIX.'user_id']."',parent_ids) <> 0 
		 ORDER BY id DESC";
		// exit();
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			//return $row = mysqli_fetch_assoc($result1);
			return $result1;		
		}else
			return $row_count;		
	}
	function get_sp_targets($spid) {     //all required for target   
		 $sql1="SELECT `id`,`assign_user_type`, `target_type`,`start_date`,`end_date`,
			(SELECT firstname FROM tbl_user WHERE id = tbl_target.assign_user_id) AS name,
			(SELECT unitname FROM tbl_units WHERE id = tbl_target.variant1_wt) AS unitname1,		
			`target_in_rs`,`target_in_wt` 
			FROM tbl_target 
			WHERE assign_user_id='".$spid."' 
			ORDER BY id DESC LIMIT 1";
           $result1 = mysqli_query($this->local_connection,$sql1);
		$s_array_temp1=array();
		if (mysqli_num_rows($result1) != 0) {
			while ($row = mysqli_fetch_array($result1)) { 
				$s_array_temp['id'] = $row['id'];
				$s_array_temp['target_type'] = $row['target_type'];
				
				$s_array_temp['start_date'] = $row['start_date'];
				$s_array_temp['end_date'] = $row['end_date'];
				$s_array_temp['unitname1'] = $row['unitname1']; 
				
				$where ="";
				if($spid!=''){	
					$where.=" AND o.ordered_by='".$spid."' ";
				}
				if($s_array_temp['target_type']=='spdate'){	
					$where.=" AND date_format(o.order_date, '%Y-%m-%d') BETWEEN STR_TO_DATE('" . $s_array_temp['start_date'] . "','%d-%m-%Y') 
					 AND STR_TO_DATE('" . $s_array_temp['end_date'] . "','%d-%m-%Y') ";
					
				}
				if($s_array_temp['target_type']=='daily'){
					$where.=" AND date_format(o.order_date, '%Y-%m-%d')= DATE(NOW())";
				}
				if($s_array_temp['target_type']=='monthly'||$s_array_temp['target_type']=='weekly'){
					$date_array=explode('::', $s_array_temp['start_date']);	
					$where.=" AND date_format(o.order_date, '%Y-%m-%d') BETWEEN STR_TO_DATE('" . $date_array[0] . "','%d-%m-%Y') 
					 AND STR_TO_DATE('" . $date_array[1] . "','%d-%m-%Y') ";
				}				
				  $sql_targetvalue = "SELECT o.order_date,
						od.p_cost_cgst_sgst as total_order_gst_cost,
						od.product_total_cost,
						od.product_quantity,od.product_variant_weight1,od.product_variant_unit1 
				FROM	tbl_orders o 
				LEFT JOIN tbl_order_details od ON o.id = od.order_id 
				WHERE 1=1 $where";
				 $proRow_targetvalue = mysqli_query($this->local_connection,$sql_targetvalue);
				
					$saleruppes=0;$saleweight=0;
				if (mysqli_num_rows($proRow_targetvalue) != 0) {
					while ($row_targetvalue = mysqli_fetch_array($proRow_targetvalue)) { 
						$saleruppes+=$row_targetvalue['total_order_gst_cost'];
						
						$order_units=$row_targetvalue['product_variant_unit1'];
						if($order_units=='kg'){
							$orderweight=$row_targetvalue['product_variant_weight1']*1000;
						}
						if($order_units=='gm'){
							$orderweight=$row_targetvalue['product_variant_weight1'];
						}
						
						$saleweight+=$row_targetvalue['product_quantity']*$orderweight;
					}
				}			
				if($s_array_temp['unitname1']=='kg'){
					$saleweight=$saleweight/1000;
				}
				$s_array_temp['start_date'] = $row['start_date'];
				$s_array_temp['name'] = $row['name'];
				$s_array_temp['target_in_rs'] = $row['target_in_rs']; 
				$s_array_temp['target_in_wt'] = $row['target_in_wt'];
				
				$s_array_temp['saleruppes'] = $saleruppes; 
				$s_array_temp['saleweight'] = $saleweight; 
				
				$s_array_temp['remain_ruppes']=0;
				$s_array_temp['remain_weight']=0;
				if(($s_array_temp['target_in_rs']-$saleruppes)>=0){
					$s_array_temp['remain_ruppes'] = $s_array_temp['target_in_rs']-$saleruppes; 
				}
				if(($s_array_temp['target_in_wt']-$saleweight)>=0){
					$s_array_temp['remain_weight'] = $s_array_temp['target_in_wt']-$saleweight; 
				}
				//$s_array_temp['remain_ruppes'] = $s_array_temp['target_in_rs']-$saleruppes; 
				//$s_array_temp['remain_weight'] = $s_array_temp['target_in_wt']-$saleweight; 
				
				$s_array_temp1[] = $s_array_temp;
			}
			 return $s_array_temp1;
		}else{
			$s_array_temp2=0;return $s_array_temp2;
		}  
    }
	public function getTargets_byspid($spid) {
	  $sql1="SELECT `id`,`assign_user_type`,`assign_user_id`, `target_type`,`start_date`,`end_date`,
		 (SELECT firstname FROM tbl_user WHERE id = tbl_target.assign_user_id) AS name,
		 (SELECT unitname FROM tbl_units WHERE id = tbl_target.variant1_wt) AS unitname1,		
		 `target_in_rs`,`target_in_wt` FROM tbl_target
		 WHERE assign_user_id='".$spid."' ORDER BY id DESC LIMIT 1";
		// exit();
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			//return $row = mysqli_fetch_assoc($result1);
			return $result1;		
		}else
			return $row_count;		
	}
	public function getTargetsAchivement($target_type,$start_date,$end_date) {
		$where ="";
		if($target_type=='spdate'){	
			$where.=" AND date_format(o.order_date, '%Y-%m-%d') BETWEEN STR_TO_DATE('" . $start_date . "','%d-%m-%Y') 
			 AND STR_TO_DATE('" . $end_date . "','%d-%m-%Y') ";
			
		}
		if($target_type=='daily'){
			$where.=" AND date_format(o.order_date, '%d-%m-%Y')= DATE(NOW())";
		}
		if($target_type=='monthly'||$target_type=='weekly'){
			$date_array=explode('::', $start_date);	
			$where.=" AND date_format(o.order_date, '%Y-%m-%d') BETWEEN STR_TO_DATE('" . $date_array[0] . "','%d-%m-%Y') 
			 AND STR_TO_DATE('" . $date_array[1] . "','%d-%m-%Y') ";
		}				
		 $sql_targetvalue = "SELECT o.order_date,
				od.p_cost_cgst_sgst as total_order_gst_cost,
				od.product_total_cost,
				od.product_quantity,od.product_variant_weight1,od.product_variant_unit1 
		FROM	tbl_orders o 
		LEFT JOIN tbl_order_details od ON o.id = od.order_id 
		WHERE 1=1 $where";
		$result1 = mysqli_query($this->local_connection,$sql_targetvalue);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			//return $row = mysqli_fetch_assoc($result1);
			return $result1;		
		}else
			return $row_count;		
	}
	public function getLeaves() {
		$seesion_user_id = $_SESSION[SESSION_PREFIX."user_id"];
		$user_role = $_SESSION[SESSION_PREFIX . 'user_role'];
        if ($user_role =='Accountant') 
        {
                $seesion_user_id =1;
        }		
	 /* $sql1="SELECT distinct `leave_id`
			FROM tbl_sp_attendance 
			WHERE presenty!='1' ";*/
		$sql1="SELECT distinct `leave_id`
			FROM tbl_leave_view 
			WHERE (find_in_set('".$seesion_user_id."',parent_ids) <> 0 ) AND presenty!='1' ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		$result_array=array();
		if($row_count > 0){	
			while($row = mysqli_fetch_array($result1)){
				$result_array[]=$row;
			}
			return $result_array;	
		}else{
			return $row_count;	
		}	
	}
	public function getLeaveDetails($leaveid) {	  
		$sql2="SELECT max(`tdate`) as enddate,min(`tdate`) as startdate,count(`id`) as daycount, `reason`, 
		`description`,`leave_status`,`leave_id`,
		 (SELECT firstname FROM tbl_user WHERE id = tbl_sp_attendance.sp_id) AS name
		FROM tbl_sp_attendance 
		WHERE  presenty!='1' AND leave_id='".$leaveid."'";
		$result2 = mysqli_query($this->local_connection,$sql2);
		$row_count2 = mysqli_num_rows($result2);
		if($row_count2 > 0){	
			while($row2 = mysqli_fetch_array($result2)){
				$result_array[]=$row2;
			}
			return $result_array;	
		}else{
			return $row_count2;	
		}	
	}
	public function getAllUnits($tuid) {	
		$where="";
		if($tuid!=0){
			$where .=" AND  tu.id='".$tuid."'";
		}		
		$sql1="SELECT tu.id tuid, tu.variantid, tu.unitname,tv.name as vname 
					FROM  `tbl_units` tu
				LEFT JOIN tbl_variant tv on tu.variantid=tv.id
				WHERE isdeleted!=1 $where order by tuid asc";
		$result2 = mysqli_query($this->local_connection,$sql1);
		$row_count2 = mysqli_num_rows($result2);
		if($row_count2 > 0){	
			while($row2 = mysqli_fetch_assoc($result2)){
				$result_array[]=$row2;
			}
			return $result_array;	
		}else{
			return $row_count2;	
		}	
	}
	public function getAllVariant($tvid) {
		$where="";
		if($tvid!=0){
			$where .=" AND  tv.id='".$tvid."' ";
		}
		$sql1="SELECT tv.id as tvid,tv.name as tvname FROM  `tbl_variant` tv 
		WHERE 1=1 $where ";
		$result2 = mysqli_query($this->local_connection,$sql1);
		$row_count2 = mysqli_num_rows($result2);
		if($row_count2 > 0){	
			while($row2 = mysqli_fetch_assoc($result2)){
				$result_array[]=$row2;
			}
			return $result_array;	
		}else{
			return $row_count2;	
		}	
	}
	
}
?>