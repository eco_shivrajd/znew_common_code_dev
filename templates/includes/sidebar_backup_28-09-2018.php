<?php
    $currentFile = $_SERVER["PHP_SELF"];
    $parts = Explode('/', $currentFile);
    $php_page_name = $parts[count($parts) - 1];

    $user_id = $_SESSION[SESSION_PREFIX.'user_id'];
    $profile_id = $_SESSION[SESSION_PREFIX.'profile_id'];
   /* $sql3="SELECT `id`,`profile_id` FROM tbl_user where id=$user_id";
	$result3 = mysqli_query($con,$sql3);
	$row3 = mysqli_fetch_assoc($result3);
    $profile_id = $row3['profile_id'];*/

	$sql4="SELECT `id` as page_id FROM tbl_pages where php_page_name='".$php_page_name."' ";
	$result4 = mysqli_query($con,$sql4);
	$row4 = mysqli_fetch_assoc($result4);
	$page_id = $row4['page_id'];

	$sql5="SELECT ischecked_add,ischecked_edit,	ischecked_delete FROM tbl_action_profile where profile_id='".$profile_id."' AND  page_id='".$page_id."' ";
	$result5 = mysqli_query($con,$sql5);
	$row5 = mysqli_fetch_assoc($result5);
	$ischecked_add = $row5['ischecked_add'];
	$ischecked_edit = $row5['ischecked_edit'];
	$ischecked_delete = $row5['ischecked_delete']; 



    $chk_menu = array();
    $j=0;
	$sql6="SELECT ap.ischecked_view,ap.page_id,ap.profile_id,p.page_name,p.php_page_name FROM tbl_action_profile ap 
	LEFT JOIN tbl_pages p ON p.id = ap.page_id
	where ap.profile_id='".$profile_id."' ";
	$result6 = mysqli_query($con,$sql6);
	while ($row6 = mysqli_fetch_array($result6)) 
	{
		$chk_menu[$j]['ischecked_view']=$row6['ischecked_view'];
		$chk_menu[$j]['page_id'] = $row6['page_id'];
		$chk_menu[$j]['profile_id'] = $row6['profile_id'];
		//echo "<pre>";
		//print_r($row6);
		$j++;
	}
	//echo "<pre>";print_r($chk_menu);
	//die();

?>

<div class="page-sidebar-wrapper">	 
	<div class="page-sidebar navbar-collapse collapse">
		<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
		
			<li class="sidebar-toggler-wrapper">
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<div class="sidebar-toggler"></div>
				<!-- END SIDEBAR TOGGLER BUTTON -->
			</li>			
			<br/>			
			<li class="<?php if($activeMainMenu=="Dashboard"){ echo 'active open'; } ?>">
				<a href="index.php">
				<i class="icon-home"></i>
				<span class="title">Dashboard</span>
				<span class="selected"></span>
				</a>
			</li>
			<?php if($_SESSION[SESSION_PREFIX.'user_type']!="Accountant") {?>
			<li class="<?php if($activeMainMenu=="ManageSupplyChain"){ echo 'active open'; } ?>">
				<a href="javascript:;">
				<i class="fa fa-share-alt"></i>
				<span class="title">Manage Supply Chain</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
					<?php if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") {?>
				    <li class="<?php if($activeMenu=="Taluka"){ echo 'active open'; } ?>">
						<a href="suburb.php">
						 Taluka</a>
					</li>
					<li class="<?php if($activeMenu=="Subarea"){ echo 'active open'; } ?>">
						<a href="subarea.php">
						 Subarea</a>
					</li>
				<!-- 	<li class="<?php if($activeMenu=="Superstockist"){ echo 'active open'; } ?>">
						<a href="superstockist.php">
						 Superstockist </a>
					</li>	 -->				
					<?php } ?>
					<?php if($_SESSION[SESSION_PREFIX.'user_type']!="Distributor") { ?>
					<!-- <li class="<?php if($activeMenu=="Stockist"){ echo 'active open'; } ?>">
						<a href="distributors.php">
						 Stockist</a>
					</li> -->
					<?php } ?>
					<!-- <li class="<?php if($activeMenu=="SalesPerson"){ echo 'active open'; } ?>">
						<a href="sales.php">
						 Sales Person </a>
					</li>
					
					<li class="<?php if($activeMenu=="DeliveryPerson"){ echo 'active open'; } ?>">
						<a href="delivery-persons.php">
						 Delivery Person</a>
					</li>
					<li class="<?php if($activeMenu=="Accountant"){ echo 'active open'; } ?>">
						<a href="accountant.php">
						 Accountant</a>
					</li>
                     <li class="<?php if($activeMenu=="DeliveryChannel"){ echo 'active open'; } ?>">
						<a href="dcpusers.php">
						 Delivery Channel</a>
					</li> -->
					<li class="<?php if($activeMenu=="Shops"){ echo 'active open'; } ?>">
						<a href="shops.php">Shops</a>
					</li>
					<!-- <li class="<?php if($activeMenu=="User List"){ echo 'active open'; } ?>">
						<a href="user_list.php">
						 User List</a>
					</li> -->
					<li class="<?php if($activeMenu=="User List"){ echo 'active open'; } ?>">
						<a href="user_list_config.php">
						 User List Config</a>
					</li>
					<li class="<?php if($activeMenu=="Leads"){ echo 'active open'; } ?>">
						<a href="leads.php">Leads</a>
					</li>
					<li class="<?php if($activeMenu=="Targets"){ echo 'active open'; } ?>">
						<a href="target_list.php">Targets</a>
					</li>

					<?php if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { ?>
					<li class="<?php if($activeMenu=="routes"){ echo 'active open'; } ?>">
						<a href="geolocation-add-route.php">Route</a>
					</li>

					<li class="<?php if($activeMenu=="UserType"){ echo 'active open'; } ?>">
						<!-- <a href="user_type_list.php">User Type</a> -->
						<a href="user-type-add.php">User Type</a>
					</li>

					<!-- <li class="<?php if($activeMenu=="UserLevel"){ echo 'active open'; } ?>">
						<a href="userlevel_list.php">User Level Margin</a>
					</li> -->
					<?php } ?> 
					<?php if($_SESSION[SESSION_PREFIX.'company_id']=='8') { ?>
					<li class="<?php if($activeMenu=="Common User"){ echo 'active open'; } ?>">
						<a href="common-user.php">
						 Common User</a>
					</li>
					<?php } ?>
				</ul>
			</li>
			<?php } ?>
			
			<?php if($_SESSION[SESSION_PREFIX.'user_type']!="Accountant") {?>
			<li class="<?php if($activeMainMenu=="ManageProducts"){ echo 'active open'; } ?>">
				<a href="javascript:;">
				<i class="fa fa-share-alt"></i>
				<span class="title">Manage Products</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
				   <li class="<?php if($activeMenu=="Brands"){ echo 'active open'; } ?>">
						<a href="brands.php">
						 Brands</a>
					</li>
					<li class="<?php if($activeMenu=="Categories"){ echo 'active open'; } ?>">
						<a href="categories.php">
						 Categories </a>
					</li>
                                        <?php if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { ?>
					<li class="<?php if($activeMenu=="Variant"){ echo 'active open'; } ?>">
						<a href="variant.php">
						 Variant</a>
					</li>
					<li class="<?php if($activeMenu=="Unit"){ echo 'active open'; } ?>">
						<a href="units.php">
						 Unit</a>
					</li>
                                        <?php } ?>
					<li class="<?php if($activeMenu=="Product"){ echo 'active open'; } ?>">
						<a href="product.php">
						Product</a>
					</li>
                                        <?php if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { ?>
					<li class="<?php if($activeMenu=="Campaign"){ echo 'active open'; } ?>">
						<a href="campaign.php">
						Campaign</a>
					</li> 
                                        <?php } ?>
                                        <?php if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { ?>
					<li class="<?php if($activeMenu=="Cartons"){ echo 'active open'; } ?>">
						<a href="cartons-list.php">
						Cartons</a>
					</li> 
                                        <?php } ?>
				</ul>
			</li>
			<?php } ?>
			<?php if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { ?>
			 <li class="<?php if($activeMainMenu=="ManageTransport"){ echo 'active open'; } ?>">
				<a href="javascript:;">
				<i class="fa fa-share-alt"></i>
				<span class="title">Manage Transport</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
				   <li class="<?php if($activeMenu=="modeoftranse"){ echo 'active open'; } ?>">
						<a href="motlist.php">
						 Mode Of Transport</a>
					</li>	
					<li class="<?php if($activeMenu=="Vehicles"){ echo 'active open'; } ?>">
						<a href="van.php">
						 Vehicles</a>
					</li>	
				</ul>
			</li> 
			<?php } ?>
					
			
			<?php if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") {?>			
			<li class="<?php if($activeMainMenu=="Orders"){ echo 'active open'; } ?>">
				<a href="javascript:;">
				<i class="fa fa-share-alt"></i>
				<span class="title">Orders</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
					<li class="<?php if($activeMenu=="Ordersall"){ echo 'active open'; } ?>">
					<a href="orders-allinone.php">Retailer Orders</a>
					</li>
					
					<!--<li class="<?php if($activeMenu=="Orders"){ echo 'active open'; } ?>">
					<a href="Orders.php">Received Orders </a>
					</li>
					
					
					<li class="<?php if($activeMenu=="OrderVisibility"){ echo 'active open'; } ?>">
					<a href="Orders.php?recieved=admin">
					Order Visibility</a>
					</li>-->
					 <li class="<?php if($activeMenu=="CUSTOrders"){ echo 'active open'; } ?>">
					<a href="customer_orders.php">Customer Orders</a>
					</li>
					<!-- <li class="<?php if($activeMenu=="ShopOrders"){ echo 'active open'; } ?>">
					<a href="Ordersshop.php">Shopwise Orders </a>
					</li>					
					<li class="<?php if($activeMenu=="StockistOrders"){ echo 'active open'; } ?>">
					<a href="Ordersstockist.php">Stockistwise Orders </a>
					</li> -->

					<li class="<?php if($activeMenu=="DirectOrders"){ echo 'active open'; } ?>">
					<a href="direct_orders_from_superstockist.php">Direct Orders </a>
					</li>
				</ul>
			<li>
		
			<?php } else { ?>
			
				<li class="<?php if($activeMainMenu=="Orders"){ echo 'active open'; } ?>">
				<a href="javascript:;">
				<i class="fa fa-share-alt"></i>
				<span class="title">Orders</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
					
					<li class="<?php if($activeMenu=="Ordersall"){ echo 'active open'; } ?>">
					<a href="Orders_stockist_placed.php">
					Retailer Orders</a>
					</li>

					<?php if($_SESSION[SESSION_PREFIX.'user_type']=="Superstockist") {?>
                                            <li class="<?php if($activeMenu=="DirectOrdersFromstockist"){ echo 'active open'; } ?>">
                                            <a href="direct_orders_from_stockist.php">
                                            Direct Orders From Stockist</a>
                                            </li>
                                       <?php } ?> 

                                        <?php if($_SESSION[SESSION_PREFIX.'user_type']=="Distributor"||$_SESSION[SESSION_PREFIX.'user_type']=="Superstockist") {?>
                                            <li class="<?php if($activeMenu=="Ordersnewstockist"){ echo 'active open'; } ?>">
                                            <a href="new_order_from_stockist.php">
                                            Create New Order</a>
                                            </li>
                                            <li class="<?php if($activeMenu=="Ordersmystockist"){ echo 'active open'; } ?>">
                                            <a href="my_order_from_stockist.php">
                                            My Orders</a>
                                            </li>
                                       <?php } ?> 
                                        
				</ul>
				<li>			
			<?php } ?>


			<li class="<?php if($activeMainMenu=="Stock"){ echo 'active open'; } ?>">
				<a href="javascript:;">
					<i class="fa fa-share-alt"></i>
					<span class="title">Stock Management</span>
					<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
					<li class="<?php if($activeMenu=="stock_dashboard"){ echo 'active open'; } ?>">
					 <a href="stock_dashboard.php">
                       Stock Dashboard</a>
					</li>
				    <li class="<?php if($activeMenu=="stock_add"){ echo 'active open'; } ?>">
					 <a href="stock_add.php">
                                            Add Stock</a>
					</li>					
				</ul>
			</li>



			<?php if($_SESSION[SESSION_PREFIX.'user_type']!="Accountant") { ?>
			<li class="<?php if($activeMainMenu=="Reports"){ echo 'active open'; } ?>">
				<a href="javascript:;">
					<i class="fa fa-share-alt"></i>
					<span class="title">Reports</span>
					<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
				    <li class="<?php if($activeMenu=="OrderSummary"){ echo 'active open'; } ?>">
					<a href="order_summary.php">Summary</a>
					</li>
					<li class="<?php if($activeMenu=="SalesReport"){ echo 'active open'; } ?>">
						<a href="sales_report.php">Sales Report</a>
					</li> 
					
					<li class="<?php if($activeMenu=="OrderSummary1"){ echo 'active open'; } ?>">
					<a href="order_summary1.php">SP Summary</a>
					</li>
						<li class="<?php if($activeMenu=="OrderSummary2"){ echo 'active open'; } ?>">
					<a href="order_summary2.php">Shop Summary</a>
					</li>
					<li class="<?php if($activeMenu=="OrderSummary4"){ echo 'active open'; } ?>">
					<a href="order_summary4.php">Shop Added Summary</a>
					</li>
					<li class="<?php if($activeMenu=="DailyStatusReport"){ echo 'active open'; } ?>">
						<a href="daily_status_report.php">Daily Status Report</a>
					</li> 
					<li class="<?php if($activeMenu=="geolocation"){ echo 'active open'; } ?>">
						<a href="geolocation-track1.php">Sales Person Location</a>
					</li> 
					<li class="<?php if($activeMenu=="geolocation1"){ echo 'active open'; } ?>">
						<a href="geolocation-track-shop.php">Shop Locations</a>
					</li> 
					<li class="<?php if($activeMenu=="tadareport"){ echo 'active open'; } ?>">
						<a href="tadareport.php">Expense Report</a>
					</li> 
                                        <li class="<?php if($activeMenu=="tadareportmonthly"){ echo 'active open'; } ?>">
						<a href="tadareportmonthly.php">Expense Report Dated</a>
					</li> 
					<li class="<?php if($activeMenu=="Sales Person Attendance"){ echo 'active open'; } ?>">		<a href="sales_person_attendance.php">Sales Person Attendance</a>
					</li> 
					 <li class="<?php if($activeMenu=="dcpreport"){ echo 'active open'; } ?>">
					<a href="dcp_report.php">DCP Report</a>
					</li>

				<li class="<?php if($activeMenu=="Targetsview"){ echo 'active open'; } ?>">
					<a href="target_view.php">Target Report</a>
				</li>

				<li class="<?php if($activeMenu=="LeaveReport"){ echo 'active open'; } ?>">
						<a href="leave_report.php">Leave Report</a>
				</li>
				</ul>
			</li>
			<?php } ?>	



			<?php if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { ?>
				
			<li class="<?php if($activeMainMenu=="Noorder"){ echo 'active open'; } ?>">
				<a href="no_order_history.php">
				<i class="fa fa-share-alt"></i>
				<span class="title">No order accept history</span>
				</a>
			</li>
			<?php } ?>	


</br></br>



<?php

    $sql7="SELECT section_name,section_id,section_active_status,php_page_name,php_section_name FROM tbl_pages where `isdeleted`!=1 GROUP BY section_id ";
	$result7 = mysqli_query($con,$sql7);
	//$row7 = mysqli_fetch_array($result7);
	//$page_id = $row4['page_id'];
       foreach ($result7 as $key => $value) 
       {
                $section_id = $value['section_id']; 
                $section_active_status = $value['section_active_status'];
                $section_php_page_name = $value['php_section_name'];
                $section_name = $value['section_name'];  
                // echo  $section_php_page_name."--";  

                $sql9="SELECT profile_id,section_id,SUM(ischecked_view) as total_view FROM tbl_action_profile  where profile_id= '".$profile_id."' AND section_id= '".$section_id."'"; 
                  $result9 = mysqli_query($con,$sql9);
                  $row9 = mysqli_fetch_assoc($result9);
                  $total_view_count = $row9['total_view'];
                //exit();
       ?>

            
        <?php 
        if($total_view_count > 0)
        {
        ?>
                       
                 <li class="<?php if($activeMainMenu==$section_active_status){ echo 'active open'; } ?>">

						<a href="<?=$section_php_page_name;?>">
							<i class="fa fa-share-alt"></i>
							<span class="title"> <?=$section_name;?></span>
							<span class="arrow "></span>
						</a>
                       <?php
                       if ($section_php_page_name=='') 
                       {
                       	?>

                                <ul class="sub-menu">
							<?php
							$sql8="SELECT id,page_name,php_page_name,page_active_status FROM tbl_pages where section_id ='".$section_id."' AND isdeleted!='1' ";
								$result8 = mysqli_query($con,$sql8);
								foreach ($result8 as $key => $value1) 
								{ 
								 $page_active_status = $value1['page_active_status'];
			                     $page_id = $value1['id'];
								
								//$t = 9;
								$search = ['page_id' => $page_id, 'ischecked_view' => 1];
								$keys = array_keys(
								array_filter(
								$chk_menu,
								function ($v) use ($search) { return $v['page_id'] == $search['page_id'] && $v['ischecked_view'] == $search['ischecked_view']; }
								)
								);
								$key = $keys[0];
								if (isset($key)) 
								{
								//echo "set";
									if($activeMenu==$page_active_status)
									{
				                     echo "<li class='active open'>";
				                    }
				                    else
				                    {
				                     echo "<li class=''>";
				                    }
				                     echo "<a href='".$value1['php_page_name']."'>".$value1['page_name']."</a>";
				                     echo "</li>"; 
								}
								/*else
								{
								echo "notset";
								}*/
								?>						
							<?php 
							}
							?>				
						</ul>

                       <?php	
                       }
                       ?>
                 	 </li>
        <?php	
        }
        ?>  
                



               
			
		<?php }
?>			 
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>