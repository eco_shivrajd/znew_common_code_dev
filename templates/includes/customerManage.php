<?php
/***********************************************************
 * File Name	: custManage.php
 ************************************************************/	
include "../includes/commonManage.php";	
class customerManager
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
		$this->commonObj 	= 	new commonManage($this->local_connection,$this->common_connection);
		}	
	public function getAllCustomerDetails()
	{		
		$sql1="SELECT *	FROM tbl_customer where isdeleted!='1'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}	
	public function getCustomerDetailsById($id)
	{		
		$sql1="SELECT *	FROM tbl_customer where cust_id='".$id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}		
	
    public function addCustomerDetails()
    {
    	extract ($_POST);
    	$cust_name	=	fnEncodeString($cust_name);
    	$added_by = $_SESSION[SESSION_PREFIX . 'user_id'];
    	if($cust_name != '')
		{
			$fields.= "`cust_name`";
			$values.= "'".$cust_name."'";
		}
		if($cust_email != '')
		{
			$fields.= ",`cust_email`";
			$values.= ",'".$cust_email."'";
		}
		if($cust_mobile != '')
		{
			$fields.= ",`cust_mobile`";
			$values.= ",'".$cust_mobile."'";
		}
		if($cust_address != '')
		{
			$fields.= ",`cust_address`";
			$values.= ",'".$cust_address."'";
		}
		if($added_by != '')
		{
			$fields.= ",`added_by`";
			$values.= ",'".$added_by."'";
		}

		$cust_sql = "INSERT INTO tbl_customer ($fields) VALUES($values)";	
		//exit();		
		$result1 = mysqli_query($this->local_connection,$cust_sql);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;
		//$id=mysqli_insert_id($this->local_connection);
		//$this->commonObj->log_add_record('tbl_customer',$id,$cust_sql);
    }
   
    public function updateCustomerDetails($cust_id)
    {
    	extract ($_POST);
    
    	if($cust_name != '')
		{
			$values.= "`cust_name`= '".$cust_name."'";				
		}else{$values.= "`cust_name`= ''";}
		if($cust_mobile != '')
		{
			$values.= ", `cust_mobile`= '".$cust_mobile."'";				
		}else{$values.= ", `cust_mobile`= ''";}
		if($cust_address != '')
		{
			$values.= ", `cust_address`= '".$cust_address."'";				
		}else{$values.= ", `cust_address`= ''";}
		if($cust_email != '')
		{
			$values.= ", `cust_email`= '".$cust_email."'";				
		}else{$values.= ", `cust_email`= ''";}

        $update_cust_sql = "UPDATE tbl_customer SET $values WHERE cust_id='$cust_id'";
	  //exit();
		mysqli_query($this->local_connection,$update_cust_sql);
	//	$this->commonObj->log_update_record('tbl_customer',$user_id,$update_cust_sql);

    }

}
?>