<?php
/***********************************************************
 * File Name	: reportManage.php
 ************************************************************/	
class reportManage
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
	}
	public function get_all_sp_leave() 
	{		
		$today = date('Y-m-d');
		if($today <= date('Y-m-15')){
			$frmdate = date("Y-m-01");
			$todate  = date("Y-m-15");
		}else{
			$frmdate = date("Y-m-16");
			$todate  =  date("Y-m-d", strtotime("last day of this month"));
		}
		
		 $sql = "SELECT tsl.tdate, tsl.sp_id,u.firstname,tsl.reason FROM tbl_sp_attendance  tsl 
		left join tbl_user u on tsl.sp_id=u.id 
		where presenty !='1' and find_in_set('".$_SESSION[SESSION_PREFIX."user_id"]."',u.parent_ids) <> 0 AND 		
		(date_format(tdate, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%Y-%m-%d') 
		AND date_format(tdate, '%Y-%m-%d') <= STR_TO_DATE('".$todate."','%Y-%m-%d'))";
		$result1 = mysqli_query($this->local_connection,$sql);		
		$i = 0;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_assoc($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}else{
			return 0;
		}
	}
  public function get_all_sp_current_location() 
	{		
            $today = date('Y-m-d');

             $sql = "SELECT tsl.id, tsl.userid, tsl.lattitude, tsl.longitude, tsl.tdate, tsl.shop_id,u.firstname 
			FROM tbl_user_location  tsl 
            left join tbl_user u on tsl.userid=u.id 
            where tsl.id in ( select max(tsl1.id) as idnew from tbl_user_location tsl1 
								where date_format(tsl1.tdate, '%Y-%m-%d') = STR_TO_DATE('".$today."','%Y-%m-%d') 
								group by tsl1.userid ) 
			and find_in_set('".$_SESSION[SESSION_PREFIX."user_id"]."',u.parent_ids) <> 0
            and u.user_role ='SalesPerson'";
            $result1 = mysqli_query($this->local_connection,$sql);		
            $i = 0;
            $row_count = mysqli_num_rows($result1);
            if($row_count > 0){
                    while($row = mysqli_fetch_assoc($result1))
                    {
                            $records[$i] = $row;
                            $i++;
                    }
                    return $records;
            }else{
                    return 0;
            }
	}
        public function getCartonDetails($cartonids) {
       
        $carton_details_sql = "SELECT tdc.id,tdc.cartons_id,tdc.brand_id,tdc.cat_id,tdc.prod_id,tdc.prod_var_id,tdc.qnty,
                                                tdc.carton_added_date,tc.brand_name,tc.category_name,tc.product_name,tc.product_price,
                                                tc.product_variant1,tc.product_variant2,
												tc.variant1_unit_name,tc.variant2_unit_name
                                                FROM tbl_dcp_cartons tdc 
                                                left join pcatbrandvariant tc on tdc.prod_id=tc.product_id 
                                                and tdc.prod_var_id=tc.product_variant_id where cartons_id ='$cartonids' and tdc.qnty > 0 ";
        $carton_det_result = mysqli_query($this->local_connection, $carton_details_sql);
        //$row_orders_det_count = mysqli_num_rows($orders_det_result);
        if (mysqli_num_rows($carton_det_result) > 0) {
            while ($row_carton_det = mysqli_fetch_assoc($carton_det_result)) {
                $all_cartons['carton_details'][] = $row_carton_det;
            }
        }
        return $all_cartons;
    }
    public function assignCartons($data) { 
        $cartons_id=$data['input_carton_id'];        
         $carton_details_sql = "SELECT id,cartons_id,company_id,brand_id,cat_id,prod_id,prod_var_id,qnty
                                                FROM tbl_dcp_cartons where cartons_id ='$cartons_id'";
        $carton_det_result = mysqli_query($this->local_connection, $carton_details_sql);
        if (mysqli_num_rows($carton_det_result) > 0) {
            while ($row_carton_det = mysqli_fetch_assoc($carton_det_result)) {
                $total_qnty=$row_carton_det['qnty']*$data['no_of_catons'];
                if($total_qnty>0){
                     $insert_user_cartons = "INSERT INTO `tbl_dcp_stock` 
                    (`dcp_id`, `cartons_id`,`prod_id`, `prod_var_id`,`assigned_qnty`,`stock_qnty`, `added_date`)
                    VALUES ('".$data['dcp_id']."','".$cartons_id."','".$row_carton_det['prod_id']."'
                        ,'".$row_carton_det['prod_var_id']."','".$total_qnty."',
                             '".$total_qnty."',now())";   
                     mysqli_query($this->local_connection, $insert_user_cartons);
                }
            }
        }        
        $update_user_sql = "INSERT INTO tbl_dcp_cartons_assign (`dcp_id`,`cartons_id`,`no_of_carton`,`added_date`) "
             . "VALUES('".$data['dcp_id']."','".$cartons_id."','".$data['no_of_catons']."',now())";  
         return mysqli_query($this->local_connection, $update_user_sql);
        //$this->commonObj->log_update_record('tbl_dcp_cartons_assign', $data['dcp_id'], $update_user_sql);
    }
	 public function assignCampaign($data) { 
		$campaign_ids=$data['input_campaign_ids']; 
		$sp_ids=$data['sp_ids_selected']; 
		$insert_count=0;
		
		$campaign_ids_Array = explode(',', $campaign_ids);
		$sp_ids_Array = explode(',', $sp_ids);
		       
		foreach($campaign_ids_Array as $key => $campaign_id){
			foreach($sp_ids_Array as $key1 => $sp_id){
				if($sp_id!=''&& $sp_id!=0){
					$insert_user_campaign = "INSERT INTO `tbl_sp_campaign_assign` 
					(`sp_id`, `campaign_id`, `added_date`)
					VALUES ('".$sp_id."','".$campaign_id."',now())";   
					mysqli_query($this->local_connection, $insert_user_campaign);
					if(mysqli_affected_rows($this->local_connection)>0){
						$insert_count++;
					}
				}
			}
		}        
		return $insert_count;
    }
	 public function reassignCampaign($data) { 
		//echo "<pre>";print_r($data);//exit();
		$campaign_ids=$data['campaign_ids']; 
		$sp_id=$data['sp_id']; 
		$insert_count=0;		
		$campaign_ids_Array = explode(',', $campaign_ids);
		$tbl_sp_campaign_assign = "DELETE FROM  tbl_sp_campaign_assign  WHERE sp_id='$sp_id'";
		mysqli_query($this->local_connection,$tbl_sp_campaign_assign);		
		
		foreach($campaign_ids_Array as $key => $campaign_id){			
			if($sp_id!=''&& $sp_id!=0){
				 $insert_user_campaign = "INSERT INTO `tbl_sp_campaign_assign` 
				(`sp_id`, `campaign_id`, `added_date`)
				VALUES ('".$sp_id."','".$campaign_id."',now())";   
				mysqli_query($this->local_connection, $insert_user_campaign);
				if(mysqli_affected_rows($this->local_connection)>0){
					$insert_count++;
				}
			}			
		}        
		return $insert_count;
    }
	function get_assigned_campaign_to_sp($user_id){
			$sql_campaign="select tu.id,tu.firstname, 
			(SELECT GROUP_CONCAT(tsca.campaign_id) 
			FROM tbl_sp_campaign_assign tsca WHERE sp_id=tu.id) AS campaign_ids,
			(SELECT GROUP_CONCAT(tc.campaign_name) 
			 FROM tbl_sp_campaign_assign tsca1 
			 LEFT JOIN tbl_campaign tc on tc.id=tsca1.campaign_id 
			 WHERE sp_id=tu.id) AS campaign_names,
			 (SELECT GROUP_CONCAT(tc1.status) 
			 FROM tbl_sp_campaign_assign tsca2 
			 LEFT JOIN tbl_campaign tc1 on tc1.id=tsca2.campaign_id 
			 WHERE sp_id=tu.id) AS campaign_status 
			from tbl_user tu 
			where tu.user_role='SalesPerson' AND tu.isdeleted!='1' 
			AND tu.external_id='".$user_id."'";
	
        $result_campaign = mysqli_query($this->local_connection, $sql_campaign);
       $camp_details=array();
        if (mysqli_num_rows($result_campaign) > 0) {
            while ($row_orders_det = mysqli_fetch_assoc($result_campaign)) {
                $camp_details[] = $row_orders_det;
            }
        }
        return $camp_details;
    }
     public function getOrderschnage1() { //for orders page search
        extract($_POST);
        $where_clause_outer = '';
        $where_clause_inner = '';
        if ($order_id != "") {
            $where_clause_outer .= " AND od.order_id = " . $order_id;
        }
        if ($frmdate != '') {
            $where_clause_outer .= " AND date_format(o.order_date, '%d-%m-%Y') = '" . $frmdate . "' ";
        }
        if ($dropdownSalesPerson != "") {
            $where_clause_outer .= " AND o.ordered_by = " . $dropdownSalesPerson;
        }
        if ($area != "") {
            $where_clause_outer .= " AND s.suburbid = " . $area;
        }
        if ($subarea != "") {
            $where_clause_outer .= " AND s.subarea_id = " . $subarea;
        }
        if ($city != "") {
            $where_clause_outer .= " AND s.city = " . $city;
        }
        if ($dropdownState != "") {
            $where_clause_outer .= " AND s.state = " . $dropdownState;
        }
        if ($dropdownCategory != "") {
            $where_clause_inner .= " AND od.cat_id = " . $dropdownCategory;
        }
        if ($dropdownProducts != "") {
            $where_clause_inner .= " AND od.product_id = " . $dropdownProducts;
        }
        $order_sql = "SELECT distinct(order_id) as oid FROM `tbl_dcp_orders` WHERE 1"; //o.shop_order_status = ".$order_status."
        $orders_result = mysqli_query($this->local_connection, $order_sql);
        $row_orders_count = mysqli_num_rows($orders_result);
        $i = 0;
        $all_orders = array();
        if ($row_orders_count > 0) {
            while ($row_orders = mysqli_fetch_assoc($orders_result)) {
                $all_orders[$i] = $row_orders;
                $i++;
            }
        }

        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }
    public function getOrdersDetailschnage($order_id) {
        extract($_POST);
        $where_clause_outer='';
        if ($order_id != "") {
            $where_clause_outer .= " AND od.order_id = '" . $order_id."' ";
        }
          $order_sql = "SELECT od.id as odid,od.order_by,tu.firstname,od.cartons_id,od.order_id,od.order_date,od.brand_id,od.cat_id,od.product_id
                    ,od.product_variant_id,od.product_quantity,od.product_variant_weight1,
                    od.product_variant_unit1,od.product_unit_cost,od.product_total_cost,od.product_cgst,
                    od.product_sgst,od.p_cost_cgst_sgst,od.order_status,
                    pcatvar.category_name as cat_name,pcatvar.product_name,
                    tuma.suburb_ids, (SELECT sb.suburbnm FROM tbl_surb AS sb WHERE sb.id = tuma.suburb_ids) AS region_name
                 FROM `tbl_dcp_orders` od 
                 LEFT join pcatbrandvariant pcatvar on od.product_id=pcatvar.product_id 
                  and od.product_variant_id=pcatvar.product_variant_id
                 left join tbl_user tu on od.order_by=tu.id
                 left join tbl_user_working_area tuma on tu.id=tuma.user_id 
                 WHERE 1=1 $where_clause_outer"; //o.shop_order_status = ".$order_status."
       
        $orders_det_result = mysqli_query($this->local_connection, $order_sql);
       $all_orders=array();
        if (mysqli_num_rows($orders_det_result) > 0) {
            while ($row_orders_det = mysqli_fetch_assoc($orders_det_result)) {
                $all_orders['order_details'][] = $row_orders_det;
            }
        }
        return $all_orders;
    }
    public function getOrdersDetailsbyvarient($order_id,$prod_id,$prod_var_id) {
        extract($_POST);
        $where_clause_outer='';
        if ($order_id != "") {
            $where_clause_outer .= " AND od.order_id = '" . $order_id."' ";
        }
        if ($prod_id != "") {
            $where_clause_outer .= " AND od.product_id = '" . $prod_id."' ";
        }
        if ($prod_var_id != "") {
            $where_clause_outer .= " AND od.product_variant_id = '" . $prod_var_id."' ";
        }
           $order_sql = "SELECT od.id as odid,od.order_by,tu.firstname,od.cartons_id,od.order_id,od.order_date,od.brand_id,od.cat_id,od.product_id
                    ,od.product_variant_id,od.product_quantity,od.product_variant_weight1,
                    od.product_variant_unit1,od.product_unit_cost,od.product_total_cost,od.product_cgst,
                    od.product_sgst,od.p_cost_cgst_sgst,od.order_status,
                    pcatvar.category_name as cat_name,pcatvar.product_name,
                    tuma.suburb_ids, (SELECT sb.suburbnm FROM tbl_surb AS sb WHERE sb.id = tuma.suburb_ids) AS region_name
                 FROM `tbl_dcp_orders` od 
                 LEFT join pcatbrandvariant pcatvar on od.product_id=pcatvar.product_id 
                  and od.product_variant_id=pcatvar.product_variant_id
                 left join tbl_user tu on od.order_by=tu.id
                 left join tbl_user_working_area tuma on tu.id=tuma.user_id 
                 WHERE 1=1 $where_clause_outer limit 1"; //o.shop_order_status = ".$order_status."
       
        $orders_det_result = mysqli_query($this->local_connection, $order_sql);
       $all_orders=array();
        if (mysqli_num_rows($orders_det_result) > 0) {
            while ($row_orders_det = mysqli_fetch_assoc($orders_det_result)) {
                $all_orders['order_details'] = $row_orders_det;
            }
        }
        return $all_orders;
    }
    public function getDCPStock() {
        extract($_POST);
        $where_clause_outer='';
        if ($dropdownSalesPerson != "") {
            $where_clause_outer .= " AND od.dcp_id = '" . $dropdownSalesPerson."' ";
        }
       
          /* $order_sql = "SELECT od.dcp_id,od.cartons_id,od.prod_id,
               od.prod_var_id,sum(od.assigned_qnty) as assigned_qnty,sum(od.stock_qnty) as stock_qnty,sum(od.sale_qnty) as sale_qnty,
               tu.firstname,
                pcatvar.category_name as cat_name,pcatvar.product_name,pcatvar.product_variant1,pcatvar.product_variant2
                 FROM tbl_dcp_stock od  
                 left join `tbl_user` tu  on od.dcp_id=tu.id
                 LEFT join pcatbrandvariant pcatvar on od.prod_id=pcatvar.product_id 
                  and od.prod_var_id=pcatvar.product_variant_id
                 WHERE 1=1 $where_clause_outer group by od.dcp_id "; */
$seesion_user_id = $_SESSION[SESSION_PREFIX."user_id"]; 
$user_role = $_SESSION[SESSION_PREFIX . 'user_role'];
        if ($user_role =='Accountant') 
        {
                $seesion_user_id =1;
        }
$order_sql = "SELECT od.dcp_id,od.cartons_id,od.prod_id,
               od.prod_var_id,sum(od.assigned_qnty) as assigned_qnty,sum(od.stock_qnty) as stock_qnty,sum(od.sale_qnty) as sale_qnty,
               tu.firstname,
                pcatvar.category_name as cat_name,pcatvar.product_name,pcatvar.product_variant1,pcatvar.product_variant2,tuv.parent_ids
                 FROM tbl_dcp_stock od  
                 left join `tbl_user` tu  on od.dcp_id=tu.id
                 LEFT JOIN `tbl_user_view` tuv on od.dcp_id=tuv.id 
                 LEFT join pcatbrandvariant pcatvar on od.prod_id=pcatvar.product_id 
                  and od.prod_var_id=pcatvar.product_variant_id
                 WHERE 1=1 AND (find_in_set('".$seesion_user_id."',tuv.parent_ids) <> 0 ) $where_clause_outer group by od.dcp_id "; 

                 //o.shop_order_status = ".$order_status."
       
        $orders_det_result = mysqli_query($this->local_connection, $order_sql);
       $all_orders=array();
        if (mysqli_num_rows($orders_det_result) > 0) {
            while ($row_orders_det = mysqli_fetch_assoc($orders_det_result)) {
                $all_orders[] = $row_orders_det;
            }
        }
        return $all_orders;
    }
    public function getDCPStockDetails($cart_id,$dcp_id) {       
        $where_clause_outer='';
        if ($cart_id != "") {
            $where_clause_outer .= " AND od.cartons_id = '" . $cart_id."' ";
        }
        if ($dcp_id != "") {
            $where_clause_outer .= " AND od.dcp_id = '" . $dcp_id."' ";
        }
       
           $order_sql = "SELECT od.dcp_id,od.cartons_id,od.prod_id,
               od.prod_var_id,od.assigned_qnty,od.stock_qnty,od.sale_qnty,
               tu.firstname,
                pcatvar.category_name as cat_name,pcatvar.product_name,pcatvar.product_variant1,pcatvar.product_variant2
                 FROM tbl_dcp_stock od  
                 left join `tbl_user` tu  on od.dcp_id=tu.id
                 LEFT join pcatbrandvariant pcatvar on od.prod_id=pcatvar.product_id 
                  and od.prod_var_id=pcatvar.product_variant_id
                 WHERE 1=1 $where_clause_outer  "; //o.shop_order_status = ".$order_status."
       
        $orders_det_result = mysqli_query($this->local_connection, $order_sql);
       $all_orders=array();
        if (mysqli_num_rows($orders_det_result) > 0) {
            while ($row_orders_det = mysqli_fetch_assoc($orders_det_result)) {
                $all_orders[] = $row_orders_det;
            }
        }
        return $all_orders;
    }
    public function get_stockist_own_orders_by_status($orderstatus) { //for orders page search
        extract($_POST);
        $where_clause_outer="";
        $user_id=$_SESSION[SESSION_PREFIX.'user_id'];
        $user_type=$_SESSION[SESSION_PREFIX.'user_type'];
        if($user_type=='Distributor'){
            $where_clause_outer .= " AND tbl_order_stockist.distributor_id = '" . $user_id . "' ";
        }
       
        if ($frmdate != '') {
            $where_clause_outer .= " AND date_format(tbl_order_stockist.order_date, '%d-%m-%Y') = '" . $frmdate . "' ";
        }
        
        $order_sql = "SELECT distinct(order_id) as oid,order_status  FROM `tbl_order_stockist` WHERE order_status='$orderstatus'  $where_clause_outer"; //o.shop_order_status = ".$order_status."
        $orders_result = mysqli_query($this->local_connection, $order_sql);
        $row_orders_count = mysqli_num_rows($orders_result);
        $i = 0;
        $all_orders = array();
        if ($row_orders_count > 0) {
            while ($row_orders = mysqli_fetch_assoc($orders_result)) {
                $all_orders[$i] = $row_orders;
                $i++;
            }
        }
        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }
    public function get_stockist_own_order_details($order_id) { //for orders page search
        extract($_POST);
        $where_clause_outer=" AND order_id='$order_id' ";
         $order_sql = "SELECT tco.*,tu.firstname, pcatvar.category_name as cat_name,
            pcatvar.product_name,pcatvar.product_variant1,pcatvar.product_variant2
                FROM tbl_order_stockist tco  
                 left join `tbl_user` tu  on tco.distributor_id=tu.id
                 LEFT join pcatbrandvariant pcatvar on tco.product_id=pcatvar.product_id 
                  and tco.product_var_id=pcatvar.product_variant_id
                 WHERE 1=1 $where_clause_outer  "; //o.shop_order_status = ".$order_status."
       
        $orders_det_result = mysqli_query($this->local_connection, $order_sql);
       $all_orders=array();
        if (mysqli_num_rows($orders_det_result) > 0) {
            while ($row_orders_det = mysqli_fetch_assoc($orders_det_result)) {
                $row_orders_det['customer_details']=$this->getcustomer_details($row_orders_det['customer_id']);
                $row_orders_det['var_details1']=$this->getvarient_details($row_orders_det['product_variant1']);
                $row_orders_det['var_details2']=$this->getvarient_details($row_orders_det['product_variant2']);
                $all_orders[] = $row_orders_det;
            }
        }
        return $all_orders;
    }
    //customer section
     public function get_customer_orders_by_status($orderstatus) { //for orders page search
        extract($_POST);
        $where_clause_outer="";
        $user_id=$_SESSION[SESSION_PREFIX.'user_id'];
        $user_type=$_SESSION[SESSION_PREFIX.'user_type'];
        if($user_type=='Distributor'){
            $where_clause_outer .= " AND tbl_customer_orders.distributor_id = '" . $user_id . "' ";
        }
       
        if ($frmdate != '') {
            $where_clause_outer .= " AND date_format(tbl_customer_orders.order_date, '%d-%m-%Y') = '" . $frmdate . "' ";
        }
        
        $order_sql = "SELECT distinct(order_id) as oid,order_status  FROM `tbl_customer_orders` WHERE order_status='$orderstatus'  $where_clause_outer"; //o.shop_order_status = ".$order_status."
        $orders_result = mysqli_query($this->local_connection, $order_sql);
        $row_orders_count = mysqli_num_rows($orders_result);
        $i = 0;
        $all_orders = array();
        if ($row_orders_count > 0) {
            while ($row_orders = mysqli_fetch_assoc($orders_result)) {
                $all_orders[$i] = $row_orders;
                $i++;
            }
        }
        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }
    public function get_customer_orders() { //for orders page search
        extract($_POST);
        $where_clause_outer='';
        $user_id=$_SESSION[SESSION_PREFIX.'user_id'];
        $user_type=$_SESSION[SESSION_PREFIX.'user_type'];
        if($user_type=='Distributor'){
            $where_clause_outer .= " AND tbl_customer_orders.distributor_id = '" . $user_id . "' ";
        }
        $order_sql = "SELECT distinct(order_id) as oid,order_status  FROM `tbl_customer_orders` WHERE 1=1 $where_clause_outer"; //o.shop_order_status = ".$order_status."
        $orders_result = mysqli_query($this->local_connection, $order_sql);
        $row_orders_count = mysqli_num_rows($orders_result);
        $i = 0;
        $all_orders = array();
        if ($row_orders_count > 0) {
            while ($row_orders = mysqli_fetch_assoc($orders_result)) {
                $all_orders[$i] = $row_orders;
                $i++;
            }
        }
        if (count($all_orders) == 0) {
            $all_orders = array();
            return $all_orders;
        } else
            return $all_orders;
    }
    public function get_customer_order_details($order_id) { //for orders page search
        extract($_POST);
        $where_clause_outer=" AND order_id='$order_id' ";
         $order_sql = "SELECT tco.*,tu.firstname, pcatvar.category_name as cat_name,
            pcatvar.product_name,pcatvar.product_variant1,pcatvar.product_variant2
                FROM tbl_customer_orders tco  
                 left join `tbl_user` tu  on tco.distributor_id=tu.id
                 LEFT join pcatbrandvariant pcatvar on tco.product_id=pcatvar.product_id 
                  and tco.product_var_id=pcatvar.product_variant_id
                 WHERE 1=1 $where_clause_outer  "; //o.shop_order_status = ".$order_status."
       
        $orders_det_result = mysqli_query($this->local_connection, $order_sql);
       $all_orders=array();
        if (mysqli_num_rows($orders_det_result) > 0) {
            while ($row_orders_det = mysqli_fetch_assoc($orders_det_result)) {
                $row_orders_det['customer_details']=$this->getcustomer_details($row_orders_det['customer_id']);
                $row_orders_det['var_details1']=$this->getvarient_details($row_orders_det['product_variant1']);
                $row_orders_det['var_details2']=$this->getvarient_details($row_orders_det['product_variant2']);
                $all_orders[] = $row_orders_det;
            }
        }
        return $all_orders;
    }
    public function get_customer_order_varient_details($order_id,$prod_id,$prod_var_id) { //for orders page search
        extract($_POST);
        $where_clause_outer="";
        if(!empty($order_id)){
            $where_clause_outer.=" AND tco.order_id='$order_id' ";
        }
        if(!empty($prod_id)){
            $where_clause_outer.=" AND tco.product_id='$prod_id' ";
        }
        if(!empty($prod_var_id)){
            $where_clause_outer.=" AND tco.product_var_id='$prod_var_id' ";
        }
        $order_sql = "SELECT tco.*,tu.firstname, pcatvar.category_name as cat_name,
            pcatvar.product_name,pcatvar.product_variant1,pcatvar.product_variant2
            FROM tbl_customer_orders tco  
            left join `tbl_user` tu  on tco.distributor_id=tu.id
            LEFT join pcatbrandvariant pcatvar on tco.product_id=pcatvar.product_id 
            and tco.product_var_id=pcatvar.product_variant_id
            WHERE 1=1 $where_clause_outer  "; //o.shop_order_status = ".$order_status."
       
        $orders_det_result = mysqli_query($this->local_connection, $order_sql);
        $all_orders=array();
        if (mysqli_num_rows($orders_det_result) > 0) {
            while ($row_orders_det = mysqli_fetch_assoc($orders_det_result)) {
                $row_orders_det['customer_details']=$this->getcustomer_details($row_orders_det['customer_id']);
                $row_orders_det['var_details1']=$this->getvarient_details($row_orders_det['product_variant1']);
                $row_orders_det['var_details2']=$this->getvarient_details($row_orders_det['product_variant2']);
                $all_orders[] = $row_orders_det;
            }
        }
        return $all_orders;
    }
    function getvarient_details($product_variant1){
        $var_name1='';
        $imp_variant1 = explode(',', $product_variant1);
        $var_unit_id = trim($product_variant1, ",");
        if($imp_variant1[1]!=''){
            $sqlvarunit = "SELECT unitname FROM tbl_units  where id='" . $imp_variant1[1] . "'";
            $resultvarunit = mysqli_query($this->local_connection, $sqlvarunit);
            while ($rowvarunit = mysqli_fetch_array($resultvarunit)) {
                if ($imp_variant1[0] != '') {
                    $var_name1 .= $imp_variant1[0] . "-" . $rowvarunit['unitname'] . " ";
                }
            }
        }
        return $var_name1;
    }
    function getcustomer_details($id){       
        $sql_customer = "SELECT `id`, `customer_name`, `customer_emailid`, `username`,"
                . "  `customer_phone_no`, `customer_address`, "
                . "`customer_type`, `customer_shipping_address`, `state`, `city`, `area`, `subarea`, "
                . "`addr_lat`, `addr_long`, `ship_addr_lat`, `ship_addr_long`,"
                . " `added_on_date` FROM `tbl_customer_new` WHERE  id='" . $id . "' limit 1";
        $result_customer = mysqli_query($this->common_connection, $sql_customer);
       $cust_details=array();
        if (mysqli_num_rows($result_customer) > 0) {
            while ($row_orders_det = mysqli_fetch_assoc($result_customer)) {
                $cust_details = $row_orders_det;
            }
        }
        return $cust_details;
    }
}
?>