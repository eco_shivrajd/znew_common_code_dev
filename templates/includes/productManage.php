<?php
/***********************************************************
 * File Name	: productManage.php
 ************************************************************/	
require_once "../includes/commonManage.php";	
class productManage
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
		$this->commonObj 	= 	new commonManage($this->local_connection,$this->common_connection);		
	}
	public function deleteBrand($brand_id) {
		$deleted_on = date("Y-m-d H:i:s");			
		$update_sql="UPDATE  tbl_brand SET isdeleted=1,deleted_on='".$deleted_on."' where id='$brand_id'";		
		mysqli_query($this->local_connection,$update_sql);	
		$this->commonObj->log_delete_record('tbl_brand',$brand_id,$update_sql);
		$this->deleteCategory($brand_id);
		$record['brand_id'] = $brand_id;
		//$this->deleteCampaign($record);
	}
	public function deleteUnit($unit_id) {	
		$deleted_on = date("Y-m-d H:i:s");			
		$update_sql="UPDATE  tbl_units SET isdeleted=1,deleted_on='".$deleted_on."' where id='$unit_id'";		
		
		mysqli_query($this->local_connection,$update_sql);	
		$this->commonObj->log_delete_record('tbl_units',$unit_id,$update_sql);
	}
	public function deleteCategory($brand_id, $cat_id=null) {
		$deleted_on = date("Y-m-d H:i:s");
		$where_clause = "";
		if($cat_id != ''){
			$where_clause = " AND id = ".$cat_id;
		}			
		$update_sql="UPDATE tbl_category SET isdeleted=1,deleted_on='".$deleted_on."' where brandid='$brand_id'".$where_clause;		
		mysqli_query($this->local_connection,$update_sql);
		$this->commonObj->log_delete_record('tbl_category',$brand_id,$update_sql);
		if($cat_id != ''){
			$this->deleteProduct($cat_id);
			$record['cat_id'] = $cat_id;			
			//$this->deleteCampaign($record);
		}else{
			$sql_cat = "SELECT GROUP_CONCAT(id) AS cat_ids FROM tbl_category WHERE brandid='$brand_id'";
			$result = mysqli_query($this->local_connection,$sql_cat);
			$row_count = mysqli_num_rows($result);
			if($row_count > 0){					
				$row = mysqli_fetch_assoc($result);	
				
				if(isset($row['cat_ids']) && $row['cat_ids'] !=''){
					$this->deleteProduct($row['cat_ids']);
					$record['cat_id'] = $cat_id;
					//$this->deleteCampaign($record);
				}
			}
		}
	}
	public function deleteProduct($cat_id, $product_id=null) {
		$deleted_on = date("Y-m-d H:i:s");
		$where_clause = "";
		if($product_id != ''){
			$where_clause = " AND id = ".$product_id;
		}			
		$update_sql="UPDATE tbl_product SET isdeleted=1,deleted_on='".$deleted_on."' where catid IN ($cat_id) ".$where_clause;
		mysqli_query($this->local_connection,$update_sql);
		//$delete_sql="UPDATE tbl_product_variant SET isdeleted=1,deleted_on='".$deleted_on."' where catid IN ($cat_id) ".$where_clause;	
		//mysqli_query($this->local_connection,$delete_sql);
		
		
		$this->commonObj->log_delete_record('tbl_product',$cat_id,$update_sql);
		$record['cat_id'] = $cat_id;
		//$this->deleteCampaign($record);
	}
	public function deleteCampaign($record) {
		$deleted_on = date("Y-m-d H:i:s");
		if($record['brand_id']!=''){
			$sql_camp = "SELECT GROUP_CONCAT(campaign_id) AS campaign_id FROM tbl_campaign_product WHERE  (c_p_brand_id='".$record['brand_id']."' OR f_p_brand_id='".$record['brand_id']."' )";
			$result = mysqli_query($this->local_connection,$sql_camp);
			$row_count = mysqli_num_rows($result);
			if($row_count > 0){					
				$row = mysqli_fetch_assoc($result);	
				$id = $row['campaign_id'];
			}	
				
		}elseif($record['cat_id']!=''){
			$sql_camp = "SELECT GROUP_CONCAT(campaign_id) AS campaign_id FROM tbl_campaign_product WHERE  (c_p_category_id IN (".$record['cat_id'].") OR f_p_category_id IN (".$record['cat_id'].") )";
			$result = mysqli_query($this->local_connection,$sql_camp);
			$row_count = mysqli_num_rows($result);
			if($row_count > 0){					
				$row = mysqli_fetch_assoc($result);	
				$id = $row['campaign_id'];
			}
		}
		if(isset($id) && $id != ''){
			$update_sql="UPDATE tbl_campaign SET isdeleted=1,deleted_on='".$deleted_on."' where id IN ($id) ";
			mysqli_query($this->local_connection,$update_sql);
			$this->commonObj->log_delete_record('tbl_campaign',$id,$update_sql);
		}
	}
	public function getAllBrands(){
		$sql1="SELECT `id`, `name`, `description`, `type`, `isdeleted`, `deleted_on` 
		FROM `tbl_brand`
		ORDER BY name ASC";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}
	public function getAllCategory(){
		$sql1="SELECT `id`, `brandid`, `categorynm`, `categoryimage`, `isdeleted`, `deleted_on` 
		FROM `tbl_category` 
		ORDER BY categorynm ASC";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}
	public function getAllProducts(){
		$sql1="SELECT `id`, `catid`, `productname`, `isdeleted`, `deleted_on`
		FROM tbl_product 
		ORDER BY productname ASC";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}	
	public function check_product_available(){
		$sql = "SELECT count(p.id) AS pcount, 
		GROUP_CONCAT(p.catid) AS cat_id, 
		GROUP_CONCAT(b.id) AS brand_id 
		FROM tbl_product AS p, tbl_category AS c, tbl_brand AS b 
		WHERE p.catid = c.id AND c.brandid = b.id 
		AND p.isdeleted != 1 AND c.isdeleted != 1 AND b.isdeleted != 1 ";
		$result = mysqli_query($this->local_connection,$sql);
		$row = mysqli_fetch_assoc($result);	//print_r($row);
		if($row['pcount'] == 1){
			echo $row['pcount']."####".$row['cat_id']."####".$row['brand_id'];
		}else if($row['pcount'] > 1){
			echo "more_than_one_product";
		}else{
			echo "no_product";
		}
	}
	public function getSingleProduct($id){
		 $sql1="SELECT  `id`, `catid`, `productname`, `added_on`, `isdeleted`, `deleted_on`, 
		`added_by_userid`, `added_by_usertype`
		FROM tbl_product 
		WHERE id='$id' limit 1";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			 return $row = mysqli_fetch_assoc($result1);			
		}else
			return $row_count;	
	}
	public function get_brand_category(){
	   $sql = "SELECT c.id as id,b.name, c.categorynm 
				FROM `tbl_category` c,
				tbl_brand b 
				WHERE b.id=c.brandid AND c.isdeleted != 1 
				AND  b.isdeleted != 1";
		$result1 = mysqli_query($this->local_connection,$sql);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			$p_array_temp=array();$counter=0;
			while ($row = mysqli_fetch_array($result1)) {
				$p_array_temp[$counter]['id'] = $row['id'];
				$p_array_temp[$counter]['name'] = $row['name'];
				$p_array_temp[$counter]['categorynm'] = $row['categorynm'];
				$counter++;
			}
			 return $p_array_temp;			
		}else
			return $row_count;	
	}
	public function get_single_product_varients($prod_id){
	   $sql = "SELECT * from `tbl_product_variant` where productid = '$product_id' ";
		$result1 = mysqli_query($this->local_connection,$sql);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			$p_array_temp=array();$counter=0;
			while ($row = mysql_fetch_array($result1)) {
				$p_array_temp[$counter] = $row;
				$counter++;
			}
			 return $p_array_temp;			
		}else
			return $row_count;	
	}
	public function get_varient_units($varient_id){
	   $sql = "SELECT * from `tbl_units` where variantid = '$varient_id' and isdeleted!=1 ";
		$result1 = mysqli_query($this->local_connection,$sql);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			$p_array_temp=array();$counter=0;
			while ($row = mysqli_fetch_assoc($result1)) {
				$p_array_temp[$counter] = $row;
				$counter++;
			}
			return $p_array_temp;			
		}else
			return $row_count;	
	}

	public function deleteRoute1($route_id) {
		 $update_sql="UPDATE tbl_route SET isdeleted=1 where id='$route_id'";
		mysqli_query($this->local_connection,$update_sql);	
		$this->commonObj->log_delete_record('tbl_route',$route_id,$update_sql);
		//$this->deleteCampaign($record);
	}
	//delete location area and subarea,shop,lead
	public function deleteShopbyid($id){
		$tbl_shops = "UPDATE  tbl_shops SET isdeleted='1'   WHERE id='$id'";
		mysqli_query($this->local_connection,$tbl_shops);		
	}
	public function inactiveShopbyid($id){
		$tbl_shops = "UPDATE  tbl_shops SET shop_status='1'   WHERE id='$id'";
		mysqli_query($this->local_connection,$tbl_shops);		
	}
	public function activeShopbyid($id){
		$tbl_shops = "UPDATE  tbl_shops SET shop_status='0'   WHERE id='$id'";
		mysqli_query($this->local_connection,$tbl_shops);		
	}
    public function deleteLeadbyid($id){
		$tbl_shops = "UPDATE  tbl_leads SET isdeleted='1'   WHERE id='$id'";
		mysqli_query($this->local_connection,$tbl_shops);		
	}
	public function deleteCustomerbyid($cust_id){
		$tbl_customer = "UPDATE  tbl_customer SET isdeleted='1'   WHERE cust_id='$cust_id'";		
		mysqli_query($this->local_connection,$tbl_customer);		
	}
	
	public function deleteAreabyid($id){		
		$tbl_shops = "UPDATE  tbl_area SET isdeleted='1'   WHERE id='$id'";
		mysqli_query($this->local_connection,$tbl_shops);
		
		//suburb_id
		$tbl_shops = "UPDATE  tbl_subarea SET isdeleted='1'   WHERE suburb_id='$id'";
		mysqli_query($this->local_connection,$tbl_shops);
	}
	
	public function deleteSubareabyid($id){
		$tbl_shops = "UPDATE  tbl_subarea SET isdeleted='1'   WHERE subarea_id='$id'";
		mysqli_query($this->local_connection,$tbl_shops);
	}
	
	public function deleteAssignedCampaign($sp_id){
		$tbl_sp_campaign_assign = "DELETE FROM  tbl_sp_campaign_assign  WHERE sp_id='$sp_id'";
		mysqli_query($this->local_connection,$tbl_sp_campaign_assign);		
	}
	//delete order from sp .25-04-2019
	public function delete_order($order_id){
		$tbl_orders_sql = "DELETE FROM  tbl_orders  WHERE id='$order_id'";
		mysqli_query($this->local_connection,$tbl_orders_sql);	
		$tbl_order_details_sql = "DELETE FROM  tbl_order_details  WHERE order_id='$order_id'";
		mysqli_query($this->local_connection,$tbl_order_details_sql);		
	}
	//delete order from customer.25-04-2019
	public function delete_order_cust($order_no){
		$tbl_customer_orders_new_sql = "DELETE FROM  tbl_customer_orders_new  WHERE order_no='$order_no'";
		mysqli_query($this->local_connection,$tbl_customer_orders_new_sql);		
	}
		
	
}
?>