<?php
/***********************************************************
 * File Name	: userManage.php
 ************************************************************/	
include "../includes/commonManage.php";	
class userConfigManager 
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
		$this->commonObj 	= 	new commonManage($this->local_connection,$this->common_connection);
	}
	public function getAllLocalUserDetails($user_type,$external_id=''){
		$where_clause = ''; 
		if($user_type == 'SalesPerson' && $_SESSION[SESSION_PREFIX.'user_type'] == "Superstockist" && $external_id != ''){
			$where_clause = " AND (sstockist_id = ". $external_id.")";
		}else if($external_id != '')
		{			
			$where_clause = " AND (external_id IN (". $external_id.") OR external_id LIKE ('%,". $external_id."%'))";
		}
		$sql1="SELECT `id`, `external_id`, `firstname`, `username`, 
		`pwd`, `user_type`, `address`, `city`, `state`, `mobile`, `email`, 
		`reset_key`, `is_default_user`,`is_common_user`, `sstockist_id`
		FROM tbl_user where user_type = '".$user_type."' AND tbl_user.isdeleted!='1' $where_clause order by is_default_user desc, firstname asc";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}
	public function getCommonUserDetails($user_id) {
		$sql1="SELECT `uid`, `emailaddress`, `passwd`, `username`, `level`, `reset_key`, `id`, `address`, `state`, `city`, `mobile`, `firstname`, `suburbid` 
		FROM tbl_users where id = '".$user_id."'";
		$result1 = mysqli_query($this->common_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	public function get_alluserole(){
		 $sql1="SELECT `id` as roleid, `user_role` as rolename,userrole_margin 
		FROM tbl_userrole where id NOT IN (1,7);";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		$resultarray=array();
		if($row_count > 0){	
			while($row = mysqli_fetch_assoc($result1)){
				$resultarray[]=$row;
			}
			return $resultarray;		
		}else
			return $row_count;		
	}
	public function get_allparentuserole($parent_id){
		 $sql1="SELECT `id`, `user_role`, `user_type`,`depth`
		FROM tbl_user_tree  WHERE parent_id='$parent_id' ORDER BY `parent_id` ASC ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		$resultarray=array();
		if($row_count > 0){	
			while($row = mysqli_fetch_assoc($result1)){
				$resultarray[]=$row;
			}
			return $resultarray;		
		}else
			return $row_count;		
	}
	public function get_allparentchield(){
		 $sql1="SELECT tut.id, tut.user_role,tut.user_type,tut.usertype_margin,tut.parent_id,
		 (select user_type from tbl_user_tree where id=tut.parent_id) as parent_user_type		 
		FROM tbl_user_tree tut";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		$resultarray=array();
		if($row_count > 0){	
			while($row = mysqli_fetch_assoc($result1)){
				$resultarray[]=$row;
			}
			return $resultarray;		
		}else
			return $row_count;		
	}
	public function get_action_profile_checkbox($profile_id1){
		  $sql1="SELECT `id`, `page_id`, `profile_id`, `ischecked_add`, `ischecked_view`, `ischecked_edit`, `ischecked_delete`
		 FROM `tbl_action_profile` WHERE profile_id='".$profile_id1."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		$resultarray=array();
		if($row_count > 0){	
			while($row = mysqli_fetch_assoc($result1)){				
				$keystring=$row['profile_id'].''.$row['page_id'];
				$resultarray[$keystring]=$row;
			}
			return $resultarray;		
		}else
			return $row_count;		
	}

	public function getUserTypeConfig($user_type,$external_id='') {
		$where_clause = '';
		//echo "string";
	//	exit();
		if($external_id != '')
		{
			$where_clause = " AND external_id = ". $external_id;
		}
		$sql1="SELECT `id`, `external_id`, `firstname`, `username`, 
		`pwd`, `user_type`, `address`, `city`, `state`, `mobile`, `email`, 
		`reset_key`, `is_default_user`, `sstockist_id`, `stockist_id`
		FROM tbl_user where user_type = '".$user_type."' AND tbl_user.isdeleted!='1' $where_clause order by is_default_user desc, firstname asc";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}
	public function addUserType() {
		extract ($_POST);	
		//print_r($_POST);
	   // exit();	
		$ul_added_on = date("Y-m-d H:i:s");		
		$fields = '';
		$values = ''; 
		if($user_role != '')
		{
			$fields.= ",`user_role`";			
			$values.= ",'".$user_role."'";
		}		
		if($user_type != '')
		{	$user_type	=	fnEncodeString($user_type);
			$fields.= ",`user_type`";			
			$values.= ",'".$user_type."'";
		}
		if($usertype_margin != '')
		{
			$fields.= ",`usertype_margin`";			
			$values.= ",'".$usertype_margin."'";
		}
		if($parent_id != '')
		{
			$fields.= ",`parent_id`";			
			$values.= ",'".$parent_id."'";
		}
	    $result11 =	$this->checkAllParentIds($parent_id);
	   // $result12 = sort($result11);
	    foreach ($result11 as $key => $value) 
	    {
	   // echo "string---";
	    $all_parent_id.=$value['self_id'].",";
	    }
       $all_parent_id .= '1';
	   $all_parent_id1 = explode(',', $all_parent_id);
	   asort($all_parent_id1);
	   $all_parent_id_asc = implode(',', $all_parent_id1);
	   // print_r($all_parent_id1);
	   // exit();
       if($all_parent_id_asc != '')
		{
			$fields.= ",`all_parent_id`";			
			$values.= ",'".$all_parent_id_asc."'";
		}
		

        $sql3="SELECT `id`,`depth` FROM tbl_user_tree where id=$parent_id";
		$result3 = mysqli_query($this->local_connection,$sql3);
		$row3 = mysqli_fetch_assoc($result3);
          $p_depth = $row3['depth'];
          $depth=$p_depth+1;
          //exit(); 
	    $sql = "INSERT INTO tbl_user_tree (`ul_added_on`,`depth`,`level` $fields) 
		VALUES('".$ul_added_on."','".$depth."','level".$depth."' $values)";
		//exit();
		mysqli_query($this->local_connection,$sql);
		$id=mysqli_insert_id($this->local_connection); 
        $profile_id = $id;
        
		//$sql_update = "UPDATE  tbl_user_tree SET margin='".$margin."' where depth='".$depth."'";
		//exit();
		mysqli_query($this->local_connection,$sql_update);       

        if ($user_role=='Superstockist' || $user_role=='Distributor' || $user_role=='Accountant' || $user_role=='Admin' ) 
        {
			/*$sql1="SELECT `id` as page_id,section_id
			FROM tbl_pages where isdeleted!='1' order by page_id asc";
			$result1 = mysqli_query($this->local_connection,$sql1);
			$row_count = mysqli_num_rows($result1);        
			while ($row = mysqli_fetch_array($result1))
			{
				$page_id = $row['page_id'];
				$section_id = $row['section_id'];
				$sql2 = "INSERT INTO tbl_action_profile (`page_id`,`section_id`,`profile_id`,`ischecked_add`,`ischecked_view`,`ischecked_edit`,`ischecked_delete`) 
				VALUES('".$page_id."','".$section_id."','".$profile_id."','0','0','0','0')";
				mysqli_query($this->local_connection,$sql2);
			}*/

			$sql1="SELECT *
			FROM tbl_profile_default_settings where profile_role = '".$user_role."' AND isdeleted!='1' order by page_id asc";
			$result1 = mysqli_query($this->local_connection,$sql1);
			$row_count = mysqli_num_rows($result1);        
			while ($row = mysqli_fetch_array($result1))
			{
				$page_id = $row['page_id'];
				$section_id = $row['section_id'];
                $ischecked_add = $row['ischecked_add'];
				$ischecked_view = $row['ischecked_view'];
				$ischecked_edit = $row['ischecked_edit'];
				$ischecked_delete = $row['ischecked_delete'];

				$sql2 = "INSERT INTO tbl_action_profile (`page_id`,`section_id`,`profile_id`,`ischecked_add`,`ischecked_view`,`ischecked_edit`,`ischecked_delete`) 
				VALUES('".$page_id."','".$section_id."','".$profile_id."','".$ischecked_add."','".$ischecked_view."','".$ischecked_edit."','".$ischecked_delete."')";
				mysqli_query($this->local_connection,$sql2);
			}

        }  
        

		$this->commonObj->log_add_record('tbl_user_tree',$id,$sql);			
	}



	public function checkAllParentIds($id, $data = array()) {		
		 $sql_parent="SELECT parent_id,id,user_type FROM tbl_user_tree WHERE id = '$id'";
		//echo "<br>";
		$parent = mysqli_query($this->local_connection,$sql_parent);
		$parent_query = mysqli_fetch_assoc($parent);		
		if ($parent_query['parent_id'] > 0) {
			$temp['parent_id']= $parent_query['parent_id'];
			$temp['self_id']= $parent_query['id'];
			$temp['user_type']= $parent_query['user_type'];
			$data[] = $temp;			
			$this->checkAllParentIds($parent_query['parent_id'], $data);
		} else {
			 $this->parent_result = $data;	
		}
		return $this->parent_result;
	}
	//copied from userManage
	public function addCommonUserDetails($user_type) {
		extract ($_POST);		
		$thePostIdArray = explode('_', $user_type);
		$user_tree_data = $this->getusertreedatabyid($thePostIdArray[0]);
		if($user_tree_data!=0){
			$new_user_type=$user_tree_data['user_type'];
			$new_user_role=$user_tree_data['user_role'];
		}else{
			$new_user_type='';
			$new_user_role='';
		}
		
          //echo "<pre>";print_r($new_user_type);
		  
		//print_r($_POST);
	    //exit();
		$email		=	fnEncodeString($email);
		$username	=	fnEncodeString($username);
		$firstname		=	fnEncodeString(trim($firstname));
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$fields = '';
		$values = ''; 
		if($state != '')
		{
			$fields.= ",`state`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city`";
			$values.= ",'".$city."'";
		}
		if($mobile != '')
		{
			$fields.= ",`mobile`";
			$values.= ",'".$mobile."'";
		}
		if($address != '')
		{
			$fields.= ",`address`";
			$values.= ",'".$address."'";
		}
		if($firstname != '')
		{
			$fields.= ",`firstname`";
			$values.= ",'".$firstname."'";
		}
		$added_on = date('Y-m-d H:i:s');
		$user_sql = "INSERT INTO tbl_users (`company_id`, `emailaddress`, `passwd`, `username`, `added_on`, `level`,`user_level` $fields) 
		VALUES('".COMPID."','".$email."','".$password."','".$username."','".$added_on."','".$new_user_role."','".$new_user_type."' $values)";
		
		mysqli_query($this->common_connection,$user_sql);	
		return $userid=mysqli_insert_id($this->common_connection); 		
	}
	public function addAllLocalUserDetails($user_type,$common_user_id) {
		extract ($_POST);	
		$thePostIdArray = explode('_', $user_type);
		$profile_id=$thePostIdArray[0];
		$user_tree_data = $this->getusertreedatabyid($thePostIdArray[0]);
		if($user_tree_data!=0){
			$new_user_type=$user_tree_data['user_type'];
			$user_role=$user_tree_data['user_role'];
		}else{
			$new_user_type='';$user_role='';
		}
		
          //echo "<pre>";print_r($new_user_type);
		$parents_array=array();
       // $external_id =	$_SESSION[SESSION_PREFIX.'user_id'];
		foreach($_POST as $key=>$value){
		  if("user_depth_" == substr($key,0,11)){
			$theparentIdArray = explode('_', $value[0]);
			$parents_array[] =$theparentIdArray[0];
		  }
		}
		if(!empty($parents_array)){
			$parent_ids = implode (",", $parents_array);
			$parent_ids = "1,".$parent_ids;
			$countarr_length=count($parents_array)-1;
			$external_id =	$parents_array[$countarr_length];
			$depth=count($parents_array)+2;			
		}else{
			$parent_ids = "1";
			$external_id =	1;
			$depth=2;
		}
		
		//echo "<pre>";print_r($parents_array);
		//echo "<pre>";print_r($parent_ids);
		//echo "<pre>";print_r($external_id);
	   
		//print"<pre>";print_r($_POST);
        //exit();
		
		$surname	=	"";
		$name		=	fnEncodeString(trim($firstname));
		$username	=	fnEncodeString($username);
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$email		=	fnEncodeString($email);
		$mobile		= 	fnEncodeString($mobile);		
		$address	= 	fnEncodeString($address);		
		
		$fields = '';
		$values = ''; 
		
		if($parent_ids != '')
		{			
			$fields.= ",`parent_ids`";			
			$values.= ",'".$parent_ids."'";
		}
		if($user_role != '')
		{
			$fields.= ",`user_role`";
			$values.= ",'".$user_role."'";
		}
		if($profile_id != '')
		{			
			$fields.= ",`profile_id`";			
			$values.= ",'".$profile_id."'";
		}
		if($depth != '')
		{
			$fields.= ",`depth`";
			$values.= ",'".$depth."'";
		}
		if($email != '')
		{
			$fields.= ",`email`";
			$values.= ",'".$email."'";
		}
		if($mobile != '')
		{
			$fields.= ",`mobile`";
			$values.= ",'".$mobile."'";
		}
		if($address != '')
		{
			$fields.= ",`address`";
			$values.= ",'".$address."'";
		}
		if($state != '')
		{
			$fields.= ",`state`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city`";
			$values.= ",'".$city."'";
		}
		if($area != '')
		{
			$fields.= ",`suburb_ids`";
			$suburb_ids = implode(',',$area);
			$values.= ",'".$suburb_ids."'";
		}
		if($subarea != '')
		{
			$fields.= ",`subarea_ids`";
			$subarea_ids = implode(',',$subarea);
			$values.= ",'".$subarea_ids."'";
		}
		if($gstnumber != '')
		{
			$fields.= ",`gst_number_sss`";
			$values.= ",'".$gstnumber."'";
		}
		if($phone_no != '')
		{
			$fields.= ",`office_phone_no`";
			$values.= ",'".$phone_no."'";
		}
		if($accname != '')
		{
			$fields.= ",`accname`";			
			$values.= ",'".$accname."'";
		}
		if($accno != '')
		{
			$fields.= ",`accno`";			
			$values.= ",'".$accno."'";
		}
		if($bank_name != '')
		{
			$fields.= ",`bank_name`";			
			$values.= ",'".$bank_name."'";
		}
		if($accbrnm != '')
		{
			$fields.= ",`accbrnm`";			
			$values.= ",'".$accbrnm."'";
		}
		if($accifsc != '')
		{
			$fields.= ",`accifsc`";			
			$values.= ",'".$accifsc."'";
		}
		if($latitude != '')
		{
			$fields.= ",`latitude`";
			$values.= ",'".fnEncodeString($latitude)."'";
		}
		if($longitude != '')
		{
			$fields.= ",`longitude`";
			$values.= ",'".fnEncodeString($longitude)."'";
		}
		if($marginType != '')
		{			
			$fields.= ",`marginType`";			
			$values.= ",'".$marginType."'";
		}
		if($userid_margin != '')
		{			
			$fields.= ",`userid_margin`";			
			$values.= ",'".$userid_margin."'";
		}
		$added_on = date('Y-m-d H:i:s');
		
	   $user_sql = "INSERT INTO tbl_user(`common_user_id`,`added_on`,`external_id`,
	   `firstname`,`username`,`pwd`,`user_type` $fields) 
		VALUES('".$common_user_id."','".$added_on."','".$external_id."','".$name."','".$username."','".$password."','".$new_user_type."' $values)";
		//die();
		mysqli_query($this->local_connection,$user_sql);		
		 $local_userid=mysqli_insert_id($this->local_connection);
		if($user_role == 'Shopkeeper')
		{
			if(count($assignshopids>0)){
				foreach($assignshopids as $key=>$shopid){
					$shopkeeper_shops_sql = "UPDATE tbl_shops 
					SET shop_owner_id='$local_userid' 
					WHERE id='$shopid'";			
					mysqli_query($this->local_connection,$shopkeeper_shops_sql);
				}
			}
		}
		//return $userid;
	}
	public function sendUserCreationEmail() {
		extract ($_POST);
		$username	=	fnEncodeString($username);
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$subject   = "Account Created";
		$fromMail  = FROMMAILID;		
		$headers = "";
		$headers .= "From: ".$fromMail."\r\n";
		$headers .= "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-Type: text/html;" . "\r\n";
		$message = "";
		$message .= "Hi, <br/>";
		$message .= "Your account created successfully<br/>";
		$message .= "Please find the login details below<br/>";
		$message .= "Username: $username<br/>";
		$message .= "Password: $rand<br/>";
		$message .= "<a href='".SITEURL."templates/login.php'>Click Here</a> to Login<br/>";
		$message .= "<br/><br/>";
		$message .= "Thanks,<br/>";
		$message .= "Admin.";
		if($username != ""){
			$sent = @mail($username,$subject,$message,$headers);
		}
	}
	public function getusertreedatabyid($tree_id) {	
		$result_array=array();
	   echo $sql1="SELECT user_type,user_role FROM tbl_user_tree where id='".$tree_id."' limit 1 ";		
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row_user=mysqli_fetch_assoc($result1)){
				$result_array['user_type']=$row_user['user_type'];
				$result_array['user_role']=$row_user['user_role'];
			}
			return $result_array;		
		}else
			return $row_count;			
	}
	public function getAllusertreedata() {	
		$result_array=array();
	    $sql1="SELECT id,user_type,user_role FROM tbl_user_tree ";		
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row_user=mysqli_fetch_assoc($result1)){
				$result_array[$row_user['id']]['user_type_id']=$row_user['id'];
				$result_array[$row_user['id']]['user_type']=$row_user['user_type'];
				$result_array[$row_user['id']]['user_role']=$row_user['user_role'];
			}
			return $result_array;		
		}else
			return $row_count;			
	}
	public function getAllUsersView() 
	{
        extract($_POST); 
		$user_type_info=$_POST['user_type'];
		$thePostIdArray = explode(',', $user_type_info);
		$user_type=$thePostIdArray[0];
		$user_role=$thePostIdArray[1];		
        $sess_user_type  = $_SESSION[SESSION_PREFIX.'user_type'];
        $sess_user_id  = $_SESSION[SESSION_PREFIX.'user_id'];       
        $sql="SELECT id,firstname,user_type,address,cityname,suburb_ids,subarea_ids,
		parent_names,statename,mobile,email,is_common_user,parent_ids 
		FROM tbl_user_view 
		where user_type ='".$user_type."' 
		AND find_in_set('".$sess_user_id."',parent_ids) <> 0"; 
        //echo $sql;echo "222 ".$sql2;
        $result1 = mysqli_query($this->local_connection, $sql);
       $row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;  
    }

   public function getAllUsersViewAllDetails($user_details_id) 
  {
        extract($_POST);
        $limit = '';
        $order_by = '';
        $search_name = '';
	      $sql="SELECT id,external_id,firstname,username,pwd,user_type,user_role,address,
		 subarea_name,suburb_ids,subarea_ids,parent_names,parent_usertypes,suburbnm,cityname,
		 statename,mobile,email,is_default_user,is_common_user,parent_ids,
		 accname,accno,accbrnm,bank_name,accifsc,	
		 gst_number_sss,office_phone_no 
		 FROM tbl_user_view where id ='".$user_details_id."'"; 
	     $users_det_result = mysqli_query($this->local_connection, $sql);
         return $row = mysqli_fetch_assoc($users_det_result);
        //echo $sql;echo "222 ".$sql2;       
    }
	public function updateAllLocalUserDetails($user_type,$user_role, $user_id) {

		extract ($_POST);	
		//print_r($_POST);
		//exit();
		$address= fnEncodeString($address);	
		$email	= fnEncodeString($email);
		$mobile	= fnEncodeString($mobile);			
		$values = '';
		
		$values.= " `id`= '".$user_id."'";
		
		//echo "<pre>";echo $parent_usertypes_test;die();
		$parent_usertypes_test1 = str_replace(' ', ',', $parent_usertypes_test);
		$parent_usertypes=explode(',',$parent_usertypes_test1);
		//print_r($parent_usertypes);
		if ((!empty($parent_usertypes))&&(count($parent_usertypes)>1)) {		
			foreach($parent_usertypes as $key=>$value){
				if($key==0){
					$values.= ", `parent_ids`= '1,";
				}else if($key==count($parent_usertypes)-1){
					$values.= "".$_POST[$value]."'";//echo $value;
					$values.= ", `external_id`= '".$_POST[$value]."'";
				}else{
					$values.= "".$_POST[$value].",";//echo $value;
				}				
			}			
		}else{
			$values.= ", `parent_ids`= '1' ";
			$values.= ", `external_id`= '1' ";
		}
		//echo $values;die();
		if($address != '')
		{
			$values.= ", `address`= '".$address."'";				
		}else{$values.= ", `address`= ''";}
		if($state != '')
		{
			$values.= ", `state`= '".$state."'";
		}else{
			$values.= ", `state`= ''";
		}
		if($city != '')
		{	
			$values.= ",`city`= '".$city."'";
		}else{$values.= ",`city`= ''";}
		if($email != '')
		{
			$values.= ", `email`= '".$email."'";				
		}else{$values.= ", `email`= ''";}
		if($mobile != '')
		{
			$values.= ", `mobile`= '".$mobile."'";				
		}else{$values.= ", `mobile`= ''";}
		if($phone_no != '')
		{
			$values.= ", `office_phone_no` = '".$phone_no."'";
		}else{$values.= ", `office_phone_no` = ''";}
		if($accname != '')
		{
			$values.= ", `accname` = '".$accname."'";
		}else{$values.= ", `accname` = ''";}
		if($accno != '')
		{
			$values.= ", `accno` = '".$accno."'";
		}else{$values.= ", `accno` = ''";}
		if($accbrnm != '')
		{
			$values.= ", `accbrnm` = '".$accbrnm."'";
		}else{$values.= ", `accbrnm` = ''";}
		if($accifsc != '')
		{
			$values.= ", `accifsc` = '".$accifsc."'";
		}else{$values.= ", `accifsc` = ''";}
		if($bank_name != '')
		{
			$values.= ", `bank_name` = '".$bank_name."'";
		}else{$values.= ", `bank_name` = ''";}
		if($gst_number_sss != '')
		{
			$values.= ", `gst_number_sss` = '".$gst_number_sss."'";
		}else{$values.= ", `gst_number_sss` = ''";}
		if($area != '')
		{
			$suburb_ids = implode(',',$area);
			$values.= ", `suburb_ids` = '".$suburb_ids."'";
		}else{$values.= ", `suburb_ids` = ''";}
		if($subarea != '')
		{
			$subarea_ids = implode(',',$subarea);
			$values.= ", `subarea_ids` = '".$subarea_ids."'";
		}else{$values.= ", `subarea_ids` = ''";}
		if($latitude != '')
		{
			$values.= ", `latitude`='".fnEncodeString($latitude)."'";
		}else{$values.= ", `latitude`=''";}
		if($longitude != '')
		{			
			$values.= ", `longitude`='".fnEncodeString($longitude)."'";
		}else{$values.= ", `longitude`=''";}
		if($marginType != '')
		{			
			$values.= ", `marginType`='".$marginType."'";
			if($marginType!=2){
				$values.= ", `userid_margin`='0.00'";
			}else{
				if($userid_margin != '')
				{			
					$values.= ", `userid_margin`='".$userid_margin."'";
				}else{$values.= ", `userid_margin`='0.0'";}
			}
		}else{$values.= ", `marginType`='0'";}
		
		
		
		//if($_SESSION[SESSION_PREFIX.'user_type'] == 'Superstockist' OR $_SESSION[SESSION_PREFIX.'user_type'] == 'Distributor')
		//{
			if($firstname!="") {
				$values.= ", `firstname`= '".fnEncodeString(trim($firstname))."'";	
			}
			if($username!="") {
				$values.= ", `username`= '".fnEncodeString($username)."'";					
			}
			
		//}
		if($_SESSION[SESSION_PREFIX.'user_type'] == 'Admin'){
			if($cod_percent!="") {
				$update_cod_sql = "update  tbl_cod_admin SET status='inactive'";
				mysqli_query($this->local_connection,$update_cod_sql);
				
				$insert_cod_sql = "insert  tbl_cod_admin SET cod_percent='$cod_percent',status='active'";
				mysqli_query($this->local_connection,$insert_cod_sql);
			}
		}	
			//assignshopids
		if($user_role == 'Shopkeeper')
		{			
			if(count($assignshopids>0)){
				$remove_assigned_sql = "update  tbl_shops SET shop_owner_id=0 where shop_owner_id='$user_id'";
				mysqli_query($this->local_connection,$remove_assigned_sql);				
				foreach($assignshopids as $key=>$shopid){
					$shopkeeper_shops_sql = "UPDATE tbl_shops 
					SET shop_owner_id='$user_id' 
					WHERE id='$shopid'";			
					mysqli_query($this->local_connection,$shopkeeper_shops_sql);
				}
			}
		}
     $update_user_sql = "UPDATE tbl_user SET $values WHERE id='$user_id'";
	  //exit();
		mysqli_query($this->local_connection,$update_user_sql);
		$this->commonObj->log_update_record('tbl_user',$user_id,$update_user_sql);
	}
	
	public function updateCommonUserDetails($common_user_id) {		
		extract ($_POST);	
		if($firstname != '')
		{
			$values.= "`firstname`= '".fnEncodeString(trim($firstname))."'";
		}
		if($username != '')
		{
			$values.= ", `username`= '".fnEncodeString($username)."'";
		}
		if($email != '')
		{
			$values.= ", `emailaddress`= '".fnEncodeString($email)."'";
		}
		if($mobile != '')
		{
			$values.= ", `mobile`= '".$mobile."'";				
		}
		if($address != '')
		{
			$values.= ", `address`= '".fnEncodeString($address)."'";
		}
		if($state != '')
		{
			$values.= ", `state`= '".$state."'";
		}
		if($city != '')
		{	
			$values.= ",`city`= '".$city."'";
		}

		$update_user_sql = "UPDATE tbl_users SET $values WHERE uid='$common_user_id'";
		mysqli_query($this->common_connection,$update_user_sql);		
	}
	public function getCommonUserID($id) {
		$sql1="SELECT `common_user_id`	FROM tbl_user where id = '".$id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;			
	}
	public function getLocalUserDetailsByUserType($user_type,$external_id='') {
		$where_clause = '';
		if($external_id != '')
		{
			$where_clause = " AND external_id = ". $external_id;
		}
		$sql1="SELECT `id`, `external_id`, `firstname`, `username`, 
		`pwd`, `user_type`, `address`, `city`, `state`, `mobile`, `email`, 
		`reset_key`, `is_default_user`, `sstockist_id`, `stockist_id`,`parent_ids` 
		FROM tbl_user where user_type = '".$user_type."' AND tbl_user.isdeleted!='1' $where_clause order by is_default_user desc, firstname asc";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}
	public function getshop_assigned_idsfor_shopkeeper($shopkeeper_id){		
		$assigned_shop_sql="SELECT id FROM tbl_shops where shop_owner_id='".$shopkeeper_id."' order by name";
		$assigned_shop_result = mysqli_query($this->local_connection,$assigned_shop_sql);
		$row_count = mysqli_num_rows($assigned_shop_result);
		if($row_count > 0){	
			$assigned_shop_array=array();
			while($assigned_row = mysqli_fetch_assoc($assigned_shop_result)){
				$assigned_shop_array[]=$assigned_row['id'];
			}
			return $assigned_shop_array;		
		}else
			return $row_count;	
	}
	public function getshop_update_shopkeeper($shopkeeper_id){		
		$assigned_shop_sql="SELECT id,name FROM tbl_shops where isdeleted!='1' and ( shop_owner_id=0 or shop_owner_id='".$shopkeeper_id."' ) order by name";
		$assigned_shop_result = mysqli_query($this->local_connection,$assigned_shop_sql);
		$row_count = mysqli_num_rows($assigned_shop_result);
		if($row_count > 0){	
			$assigned_shop_array=array();
			$counter=0;
			while($assigned_row = mysqli_fetch_assoc($assigned_shop_result)){
				$assigned_shop_array[$counter]['id']=$assigned_row['id'];
				$assigned_shop_array[$counter]['name']=$assigned_row['name'];
				$counter++;
			}
			return $assigned_shop_array;		
		}else
			return $row_count;	
	}
	public function getAllLocalUserDetailss($user_id) {
		 $sql1="SELECT `id`, `external_id`, `firstname`, `username`, `pwd`, `user_type`,`user_role`, `address`,
		 `subarea_ids`, `suburb_ids`,`city_id` as `city`, `state_id` as `state`, `mobile`,`office_phone_no`,
		 `user_status`, `accname`,`accno`,`bank_name`,`accbrnm`,`accifsc`,`latitude`,`longitude`,
			`marginType`,`userid_margin`, `email`,
		 `reset_key`, `is_default_user`, `parent_ids`,`parent_names`,`parent_usertypes`, `gst_number_sss` 
		 FROM `tbl_user_view` where `id`  = '".$user_id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	public function getAllLocalMarginDetailsByUsertype() {
		$margin_percentage = array();
		$margin_by_usertype = "SELECT tut.user_role,tut.user_type,tut.usertype_margin,turole.userrole_margin 
		FROM tbl_user_tree tut 
		LEFT JOIN tbl_userrole turole on turole.user_role=tut.user_role  ";
		$margin_by_usertype_result = mysqli_query($this->local_connection, $margin_by_usertype);
		while ($row = mysqli_fetch_assoc($margin_by_usertype_result)) {
			$user_type = $row['user_type'];
			$margin_percentage['user_type'][$user_type] = $row['usertype_margin'];
			$margin_percentage['user_role'][$user_type] = $row['userrole_margin'];
		}
		/* $margin_by_userid = "SELECT id as userid,userid_margin FROM `tbl_user` ";
		$margin_by_userid_result = mysqli_query($this->local_connection, $margin_by_userid);
		while ($row = mysqli_fetch_assoc($margin_by_userid_result)) {
			$margin_userid = $row['userid'];
			$margin_percentage[$margin_userid] = $row['userid_margin'];
		} */
		return $margin_percentage;		
	}

	public function deleteSorSSById($user_id,$user_id1){
		//echo $user_id."--".$user_id1;
		//exit();
		$tbl_user = "UPDATE  tbl_user SET isdeleted='1' WHERE id='$user_id'";
		mysqli_query($this->local_connection,$tbl_user);
		
		$tbl_shops = "UPDATE  tbl_shops SET service_by_user_id='$user_id1' WHERE service_by_user_id='$user_id'";
		mysqli_query($this->local_connection,$tbl_shops);
		
		$tbl_shops = "UPDATE  tbl_leads SET service_by_user_id='$user_id1' WHERE service_by_user_id='$user_id'";
		mysqli_query($this->local_connection,$tbl_shops);	
		
		$sql1="SELECT `common_user_id`	FROM tbl_user where id = '".$user_id."'";
		$result11 = mysqli_query($this->local_connection,$sql1);
		$row11 = mysqli_fetch_assoc($result11);
		$common_user_id = $row11['common_user_id'];		
		$tbl_users = "UPDATE  tbl_users SET isdeleted='1' WHERE uid='$common_user_id'";
		mysqli_query($this->common_connection,$tbl_users);

		$tbl_user_new_parent2 = "UPDATE tbl_user SET external_id='$user_id1' WHERE external_id='$user_id'";
		mysqli_query($this->local_connection,$tbl_user_new_parent2);
			
		$tbl_order_new_parent2 = "UPDATE tbl_orders o 
		JOIN tbl_order_details od ON o.id = od.order_id
		SET o.order_to='$user_id1' WHERE o.order_to='$user_id' 
		AND (od.order_status='1' or od.order_status='2') ";
		mysqli_query($this->local_connection,$tbl_order_new_parent2);

		$tbl_find = "SELECT id,external_id,parent_ids FROM tbl_user where FIND_IN_SET('$user_id', parent_ids)";
        $result = mysqli_query($this->local_connection,$tbl_find);
        while ($row = mysqli_fetch_array($result)) {
        	$id = $row['id'];
        	$external_id = $row['external_id'];
        	$parent_ids = $row['parent_ids'];
        	$parent_ids1 = explode(',', $parent_ids);
        	//$last_parent_id = end($parent_ids1);
        	$new_parent_ids = str_replace($user_id, $user_id1, $parent_ids1);
        	$new_parent_ids_str = implode(',', $new_parent_ids);
        	 // print_r($parent_ids1);
		   // print_r($new_parent_ids);
        	//echo $new_parent_ids_str;
			 $tbl_user_new_parent = "UPDATE tbl_user SET parent_ids='$new_parent_ids_str' WHERE id='$id'";
			mysqli_query($this->local_connection,$tbl_user_new_parent);				  
        }
		//exit();
	}

		public function getLocalUserDetails($user_id) {
		$sql1 ="SELECT `id`, `external_id`, `firstname`, `username`, `pwd`, `user_type`, `address`,`subarea_ids`,`suburb_ids`, `city`, `state`, `mobile`,`office_phone_no`,`user_status`, `email`, `reset_key`, `is_default_user`, `sstockist_id`, `gst_number_sss` from tbl_user where id = '".$user_id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}

	public function deleteSalespersonById($user_id){
		//echo $user_id;
		//exit();
		$user_data = $this->getLocalUserDetails($user_id);
		$external_id=$user_data['external_id'];
		$tbl_user = "UPDATE  tbl_user SET isdeleted='1' WHERE id='$user_id'";
		mysqli_query($this->local_connection,$tbl_user);

		$sql1="SELECT `common_user_id`	FROM tbl_user where id = '".$user_id."'";
		$result11 = mysqli_query($this->local_connection,$sql1);
		$row11 = mysqli_fetch_assoc($result11);
		$common_user_id = $row11['common_user_id'];		
		$tbl_users = "UPDATE  tbl_users SET isdeleted='1' WHERE uid='$common_user_id'";
		mysqli_query($this->common_connection,$tbl_users);

		$tbl_shops = "UPDATE  tbl_shops SET shop_added_by='$external_id' WHERE shop_added_by='$user_id'";
		mysqli_query($this->local_connection,$tbl_shops);		
	}
	public function deleteShopkeeperById($user_id){
		//echo $user_id;
		//exit();
		$user_data = $this->getLocalUserDetails($user_id);
		$external_id=$user_data['external_id'];
		$tbl_user = "UPDATE  tbl_user SET isdeleted='1' WHERE id='$user_id'";
		mysqli_query($this->local_connection,$tbl_user);

		$sql1="SELECT `common_user_id`	FROM tbl_user where id = '".$user_id."'";
		$result11 = mysqli_query($this->local_connection,$sql1);
		$row11 = mysqli_fetch_assoc($result11);
		$common_user_id = $row11['common_user_id'];		
		$tbl_users = "UPDATE  tbl_users SET isdeleted='1' WHERE uid='$common_user_id'";
		mysqli_query($this->common_connection,$tbl_users);

		$tbl_shops = "UPDATE  tbl_shops SET shop_added_by='$external_id' WHERE shop_added_by='$user_id'";
		mysqli_query($this->local_connection,$tbl_shops);		
	}
    public function deleteDeliveryPersonById($user_id){
		$user_data = $this->getLocalUserDetails($user_id);
		$external_id=$user_data['external_id'];
		$tbl_user = "UPDATE  tbl_user SET isdeleted='1' WHERE id='$user_id'";
		mysqli_query($this->local_connection,$tbl_user);

		$sql1="SELECT `common_user_id`	FROM tbl_user where id = '".$user_id."'";
		$result11 = mysqli_query($this->local_connection,$sql1);
		$row11 = mysqli_fetch_assoc($result11);
		$common_user_id = $row11['common_user_id'];		
		$tbl_users = "UPDATE  tbl_users SET isdeleted='1' WHERE uid='$common_user_id'";
		mysqli_query($this->common_connection,$tbl_users);	
	}
	public function deletedcpById($user_id){
		$user_data = $this->getLocalUserDetails($user_id);
		$external_id=$user_data['external_id'];
		$tbl_user = "UPDATE  tbl_user SET isdeleted='1' WHERE id='$user_id'";
		mysqli_query($this->local_connection,$tbl_user);

		$sql1="SELECT `common_user_id`	FROM tbl_user where id = '".$user_id."'";
		$result11 = mysqli_query($this->local_connection,$sql1);
		$row11 = mysqli_fetch_assoc($result11);
		$common_user_id = $row11['common_user_id'];		
		$tbl_users = "UPDATE  tbl_users SET isdeleted='1' WHERE uid='$common_user_id'";
		mysqli_query($this->common_connection,$tbl_users);	
	}
	public function deleteAccountantById($user_id){
		$user_data = $this->getLocalUserDetails($user_id);
		$external_id=$user_data['external_id'];
		$tbl_user = "UPDATE  tbl_user SET isdeleted='1' WHERE id='$user_id'";
		mysqli_query($this->local_connection,$tbl_user);

		$sql1="SELECT `common_user_id`	FROM tbl_user where id = '".$user_id."'";
		$result11 = mysqli_query($this->local_connection,$sql1);
		$row11 = mysqli_fetch_assoc($result11);
		$common_user_id = $row11['common_user_id'];		
		$tbl_users = "UPDATE  tbl_users SET isdeleted='1' WHERE uid='$common_user_id'";
		mysqli_query($this->common_connection,$tbl_users);	
	}
}
?>