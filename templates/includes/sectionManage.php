<?php
/***********************************************************
 * File Name	: sectionManager.php
 ************************************************************/	

class sectionManager
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
		//$this->commonObj 	= 	new commonManage($this->local_connection,$this->common_connection);
	}	

	public function addPage()
	{		
		//print_r($_POST);
		//exit();	
		extract ($_POST);

					$fields = '';
					$values = ''; 
					if($section_id != '')
					{
					$fields.= "`section_id`";
					$values.= "'".$section_id."'";
					}
					if($page_name != '')
					{
					$fields.= ",`page_name`";
					$values.= ",'".$page_name."'";
					}
                    if($page_active_status != '')
					{
					$fields.= ",`page_active_status`";
					$values.= ",'".$page_active_status."'";
					}
					if($php_page_name != '')
					{
					$fields.= ",`php_page_name`";
					$values.= ",'".$php_page_name."'";
					}

                   $sql1="SELECT section_name,php_section_name,section_active_status FROM tbl_section WHERE id = '".$section_id."'";
				   $result1 = mysqli_query($this->local_connection,$sql1);
				   $row1 = mysqli_fetch_assoc($result1);
				   $section_name = $row1['section_name'];
				  $php_section_name = $row1['php_section_name'];
                  $section_active_status = $row1['section_active_status'];
                  // exit();

				   $sql2 = "INSERT INTO `tbl_pages` (`section_name`,`php_section_name`,`section_active_status`,$fields) VALUES('".$section_name."','".$php_section_name."','".$section_active_status."',$values)";
                  //  exit();
					$sql2=mysqli_query($this->local_connection,$sql2);	
					$page_id = mysqli_insert_id($this->local_connection); 

                   $sql3="SELECT profile_id FROM tbl_action_profile GROUP BY profile_id";
				   $result3 = mysqli_query($this->local_connection,$sql3);
				   while ($row3 = mysqli_fetch_array($result3)) 
				   {
		         	   $profile_id = $row3['profile_id'];
						$sql33 = "INSERT INTO tbl_action_profile (`page_id`,`section_id`,`profile_id`,`ischecked_add`,`ischecked_view`,`ischecked_edit`,`ischecked_delete`) 
						VALUES('".$page_id."','".$section_id."','".$profile_id."','1','1','1','1')";
						mysqli_query($this->local_connection,$sql33);	
				   } 

				    $sql4="SELECT profile_id,profile_role FROM tbl_profile_default_settings GROUP BY profile_id";
				   $result4 = mysqli_query($this->local_connection,$sql4);
				   while ($row4 = mysqli_fetch_array($result4)) 
				   {
		         	   $profile_id = $row4['profile_id'];
		         	   $profile_role = $row4['profile_role'];
						$sql44 = "INSERT INTO tbl_profile_default_settings (`page_id`,`section_id`,`profile_id`,`profile_role`,`ischecked_add`,`ischecked_view`,`ischecked_edit`,`ischecked_delete`) 
			     	VALUES('".$page_id."','".$section_id."','".$profile_id."','".$profile_role."','1','1','1','1')";
						mysqli_query($this->local_connection,$sql44);	
				   }                  

				$commonObj 	= 	new commonManage($this->local_connection,$conmain);
				$commonObj->log_add_record('tbl_pages',$page_id,$sql1);		
	}
	public function addSection()
	{		
					//print_r($_POST);
					//exit();	
		            extract ($_POST);
					$fields = '';
					$values = '';
					if($section_name != '')
					{
					$fields.= "`section_name`";
					$values.= "'".$section_name."'";
					}
                    if($section_active_status != '')
					{
					$fields.= ",`section_active_status`";
					$values.= ",'".$section_active_status."'";
					}
					if($php_section_name != '')
					{
					$fields.= ",`php_section_name`";
					$values.= ",'".$php_section_name."'";
					}                  
				    $sql2 = "INSERT INTO `tbl_section` ($fields) VALUES($values)";
					$sql2=mysqli_query($this->local_connection,$sql2);	
					$section_id = mysqli_insert_id($this->local_connection); 
				    $commonObj 	= 	new commonManage($this->local_connection,$conmain);
				    $commonObj->log_add_record('tbl_pages',$section_id,$sql2);		
	}
    public function updatePage()
	{	
		extract ($_POST);
		if($section_id != '')
		{
			$values.= "`section_id` = '".$section_id."'";
		}	
        if($page_name != '')
		{
			$values.= ", `page_name` = '".$page_name."'";
		}
		if($page_active_status != '')
		{
			$values.= ", `page_active_status` = '".$page_active_status."'";
		}
		if($php_page_name != '')
		{
			$values.= ", `php_page_name` = '".$php_page_name."'";
		}
			$sql1 = "UPDATE tbl_pages SET $values WHERE id='$page_id'";
			$result1 = mysqli_query($this->local_connection,$sql1);
			$commonObj 	= 	new commonManage($this->local_connection,$conmain);
			$commonObj->log_add_record('tbl_pages',$page_id,$sql1);		
	}
     public function updateSection()
	{	
		extract ($_POST);
		//print_r($_POST);
		//exit();
			
        if($section_name != '')
		{
			$values.= "`section_name` = '".$section_name."'";
		}
		if($section_active_status != '')
		{
			$values.= ", `section_active_status` = '".$section_active_status."'";
		}
		if($php_section_name != '')
		{
			$values.= ", `php_section_name` = '".$php_section_name."'";
		}


		$sql1 = "UPDATE tbl_pages SET $values WHERE section_id='$section_id'";
		$result1 = mysqli_query($this->local_connection,$sql1);

		$sql2 = "UPDATE tbl_section SET $values WHERE id='$section_id'";
		$result2 = mysqli_query($this->local_connection,$sql2);

			$commonObj 	= 	new commonManage($this->local_connection,$conmain);
			$commonObj->log_add_record('tbl_pages',$page_id,$sql1);		
	}
	public function getAllSection()
	{
	               $sql="SELECT id,section_name FROM tbl_section WHERE isdeleted != 1";
				   $result = mysqli_query($this->local_connection,$sql);
				   $row_count = mysqli_num_rows($result);
				   if ($row_count > 0) {
				   	return $result;
				   }
				   else
				   {
				   	 	return $row_count;
				   }
	}

	public function getAllPagesDetails()
	{
		           $sql="SELECT * FROM tbl_pages WHERE isdeleted != 1";
				   $result = mysqli_query($this->local_connection,$sql);
				   $row_count = mysqli_num_rows($result);
				   if ($row_count > 0) {
				   	return $result;
				   }
				   else
				   {
				   	 	return $row_count;
				   }
	}
	public function getPagesDetailsByID($page_id)
	{
		          $sql="SELECT * FROM tbl_pages WHERE id ='". $page_id."'";
				   $result = mysqli_query($this->local_connection,$sql);
				   $row_count = mysqli_num_rows($result);
				   if ($row_count > 0) {
				   	return $result;
				   }
				   else
				   {
				   	 	return $row_count;
				   }
	}
	public function getSectionDetailsByID($section_id)
	{
		          $sql="SELECT * FROM tbl_section WHERE id ='". $section_id."'";
				   $result = mysqli_query($this->local_connection,$sql);
				   $row_count = mysqli_num_rows($result);
				   if ($row_count > 0) {
				   	return $result;
				   }
				   else
				   {
				   	 	return $row_count;
				   }
	}
    public function getAllSectionDetails()
	{
		           $sql="SELECT * FROM tbl_section WHERE isdeleted != 1 order by id desc";
				   $result = mysqli_query($this->local_connection,$sql);
				   $row_count = mysqli_num_rows($result);
				   if ($row_count > 0) {
				   	return $result;
				   }
				   else
				   {
				   	 	return $row_count;
				   }
	}
	public function deletePageById($del_page_id){
		$tbl_pages = "UPDATE tbl_pages SET isdeleted='1' WHERE id='$del_page_id'";
		mysqli_query($this->local_connection,$tbl_pages);
          
        $tbl_action_profile = "UPDATE tbl_action_profile SET isdeleted='1' WHERE page_id='$del_page_id'";
		mysqli_query($this->local_connection,$tbl_action_profile);

		$tbl_profile_default_settings = "UPDATE tbl_profile_default_settings SET isdeleted='1' WHERE page_id='$del_page_id'";
		mysqli_query($this->local_connection,$tbl_profile_default_settings);

		$commonObj 	= 	new commonManage($this->local_connection,$conmain);
		$commonObj->log_add_record('tbl_pages',$del_page_id,$tbl_pages);
	}
	public function deleteSectionById($del_section_id){

		$tbl_section = "UPDATE tbl_section SET isdeleted='1' WHERE id='$del_section_id'";
		mysqli_query($this->local_connection,$tbl_section);
		
		$tbl_pages = "UPDATE tbl_pages SET isdeleted='1' WHERE section_id='$del_section_id'";
		mysqli_query($this->local_connection,$tbl_pages);
          
        $tbl_action_profile = "UPDATE tbl_action_profile SET isdeleted='1' WHERE section_id='$del_section_id'";
		mysqli_query($this->local_connection,$tbl_action_profile);

		$tbl_profile_default_settings = "UPDATE tbl_profile_default_settings SET isdeleted='1' WHERE section_id='$del_section_id'";
		mysqli_query($this->local_connection,$tbl_profile_default_settings);

		$commonObj 	= 	new commonManage($this->local_connection,$conmain);
		$commonObj->log_add_record('tbl_section',$del_page_id,$tbl_section);
	}

}
?>