<?php
/***********************************************************
 * File Name	: shopManage.php
 ************************************************************/	
//include "../includes/commonManage.php";	
class shopManager
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
		$this->commonObj 	= 	new commonManage($this->local_connection,$this->common_connection);
	}	
	public function checkShopAccess($id) {
		$where_clause = "";
		if($_SESSION[SESSION_PREFIX.'user_type'] == 'Superstockist')
		{
			$user_id		= $_SESSION[SESSION_PREFIX.'user_id'];	
			$where_clause = " AND (sstockist_id =".$user_id." OR stockist_id IN (SELECT id FROM tbl_user WHERE external_id = ".$user_id." ))";
		}
		else if($_SESSION[SESSION_PREFIX.'user_type'] == 'Distributor')
		{
			$user_id		= $_SESSION[SESSION_PREFIX.'user_id'];	
			$where_clause = " AND stockist_id =".$user_id;
		}
		$sql1="SELECT `id`	FROM tbl_shops WHERE id=".$id.$where_clause;
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return 1;		
		}else
			return $row_count;		
	}	
	public function getAllShopsByUser() {
		$where_clause = "";
		if($_SESSION[SESSION_PREFIX.'user_type'] == 'Superstockist')
		{
			$user_id		= $_SESSION[SESSION_PREFIX.'user_id'];	
			$where_clause = " and (sstockist_id =".$user_id." OR stockist_id IN (SELECT id FROM tbl_user WHERE external_id = ".$user_id." ))";
		}
		else if($_SESSION[SESSION_PREFIX.'user_type'] == 'Distributor')
		{
			$user_id		= $_SESSION[SESSION_PREFIX.'user_id'];	
			$where_clause = " and  stockist_id =".$user_id;
		}
		$sql1="SELECT `id`, `name`, `address`, `city`, `state`, `contact_person`, `mobile`, `shop_added_by`, `lat`, 
		`lon`, `contact_person_other`, `mobile_number_other`, `gst_number`, `suburbid`, `closedday`, `opentime`, 
		`closetime`, `latitude`, `longitude`, `subarea_id`,`sstockist_id`,`stockist_id`, `status`
		FROM tbl_shops  WHERE isdeleted!='1' ".$where_clause;
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}	
	public function getAllShopsByfilter() {
		$where_clause = "";
		if($_SESSION[SESSION_PREFIX.'user_type'] == 'Superstockist')
		{
			$user_id		= $_SESSION[SESSION_PREFIX.'user_id'];	
			$where_clause = " WHERE (sstockist_id =".$user_id." OR stockist_id IN (SELECT id FROM tbl_user WHERE external_id = ".$user_id." ))";
		}
		else if($_SESSION[SESSION_PREFIX.'user_type'] == 'Distributor')
		{
			$user_id		= $_SESSION[SESSION_PREFIX.'user_id'];	
			$where_clause = " WHERE stockist_id =".$user_id;
		}
		if(!empty($_GET['state'])){
			$where_clause .=" and state=".$_GET['state'].' ';
			if($_GET['city'] != ""){
				$where_clause .=" and city=".$_GET['city'].' ';
				if($_GET['area'] != ""){
					$where_clause .=" and suburbid = ".$_GET['area'].' ';
					if($_GET['subarea'] != ""){
						$where_clause .=" and subarea_id = ".$_GET['subarea'].' ';
					}
				}
			}
		}
		 $sql1="SELECT `id`, `name`, `address`, `city`, `state`, `contact_person`, `mobile`, `shop_added_by`, `lat`, 
		`lon`, `contact_person_other`, `mobile_number_other`, `gst_number`, `suburbid`, `closedday`, `opentime`, 
		`closetime`, `latitude`, `longitude`, `subarea_id`,`sstockist_id`,`stockist_id`, `status`
		FROM tbl_shops ".$where_clause. " and isdeleted!='1' order by name";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}
	public function getShopDetails($id) {		
		 $sql1="SELECT `id`, `name`, `address`, `city`,
		(SELECT name FROM tbl_city WHERE id = tbl_shops.city) AS city_name,
		service_by_usertype_id ,
		(SELECT user_type FROM tbl_user_tree WHERE id = tbl_shops.service_by_usertype_id) AS service_by_usertype_name,
		service_by_user_id,
		(SELECT firstname FROM tbl_user WHERE id = tbl_shops.service_by_user_id) AS service_by_user_name,
		`state`, 
		(SELECT name FROM tbl_state WHERE id = tbl_shops.state) AS state_name,`contact_person`, `mobile`, `shop_added_by`, `lat`, 
		`lon`, `contact_person_other`, `mobile_number_other`,`landline_number_other`, `gst_number`, `suburbid`, `closedday`, `opentime`, 
		`closetime`, `latitude`, `longitude`,`shop_margin`, `subarea_id`,`sstockist_id`,`stockist_id`, 
         `billing_name`,`bank_acc_name`, `bank_acc_no`, `bank_b_name`,`bank_name`,`bank_ifsc`, `pan_number`,`fssai_number`,`shop_type_id`,`shop_status`,
		`status`
		FROM tbl_shops WHERE id = '$id' ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	public function getSessionUserDetails($id) {		
		$sql1="SELECT `id`, `firstname` as `name`, `address`, `city`,(SELECT name FROM tbl_city WHERE id = tbl_user.city) AS city_name, `state`, 
		(SELECT name FROM tbl_state WHERE id = tbl_user.state) AS state_name,`gst_number_sss` as `gst_number`,
        '' as `billing_name`,'accname' as `bank_acc_name`,'accno' as `bank_acc_no`, 'accbrnm' as `bank_b_name`,'bank_name' as `bank_name`,'accifsc' as `bank_ifsc`
		FROM tbl_user WHERE id = '$id' ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	public function getLeadDetails($id) {		
		 $sql1="SELECT `id`, `name`, `address`, `city`,
		(SELECT name FROM tbl_city WHERE id = tbl_leads.city) AS city_name,
		service_by_usertype_id ,
		(SELECT user_type FROM tbl_user_tree WHERE id = tbl_leads.service_by_usertype_id) AS service_by_usertype_name,
		service_by_user_id,
		(SELECT firstname FROM tbl_user WHERE id = tbl_leads.service_by_user_id) AS service_by_user_name,
		(SELECT name FROM tbl_city WHERE id = tbl_leads.city) AS city_name, `state`, 
		(SELECT name FROM tbl_state WHERE id = tbl_leads.state) AS state_name,`contact_person`, `mobile`, `shop_added_by`, `lat`, 
		`lon`, `contact_person_other`, `mobile_number_other`,`landline_number_other`, `gst_number`, `suburbid`, `closedday`, `opentime`, 
		`closetime`, `latitude`, `longitude`, `subarea_id`,`sstockist_id`,`stockist_id`, 
         `billing_name`,`bank_acc_name`, `bank_acc_no`, `bank_b_name`,`bank_name`,`bank_ifsc`, 
		`status`
		FROM tbl_leads WHERE id = '$id' ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}

	public function getShopLeads() {
		$seesion_user_id=$_SESSION[SESSION_PREFIX.'user_id'];
		 $sql1="SELECT tl.id,tl.name,tl.address,tl.city,
		(SELECT name FROM tbl_city WHERE id = tl.city) AS city_name, 
		tl.state,
		 (SELECT name FROM tbl_state WHERE id = tl.state) AS state_name,
		 (SELECT suburbnm FROM tbl_area WHERE id = tl.suburbid) AS area_name,
		 tu.firstname as salesperson_name,tu.parent_ids as parent_ids,		 
		 (SELECT firstname FROM tbl_user tu1 WHERE tu1.id = tl.sstockist_id) AS sstockist_name,
		 (SELECT firstname FROM tbl_user tu2 WHERE tu2.id = tl.stockist_id) AS stockist_name,
		 tl.contact_person,tl.mobile,tl.shop_added_by,tl.lat,tl.lon,tl.
		 contact_person_other,tl.mobile_number_other,tl.gst_number,tl.
		 suburbid,tl.closedday,tl.opentime,tl.closetime,tl.latitude,tl.longitude,tl.
		 subarea_id,tl.sstockist_id,tl.stockist_id,tl.billing_name,tl.bank_acc_name,tl.
		 bank_acc_no,tl.bank_b_name,tl.bank_name,tl.bank_ifsc,tl.status
		 FROM tbl_leads tl
		 left join tbl_user tu on tl.shop_added_by=tu.id 
		 where tl.isdeleted!=1 AND ( find_in_set('".$seesion_user_id."',tu.parent_ids) <> 0 
		 OR tl.shop_added_by='$seesion_user_id' OR tl.service_by_user_id='$seesion_user_id' ) ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			//return $row = mysqli_fetch_assoc($result1);
			return $result1;		
		}else
			return $row_count;		
	}



	public function getShopAssignToUsers($id) {		
		$sql1="SELECT `stockist_id`
		FROM tbl_shops WHERE id = '$id' ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	public function getLeadAssignToUsers($id) {		
		$sql1="SELECT `stockist_id`
		FROM tbl_leads WHERE id = '$id' ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}	
	public function addShopDetails() {
		extract ($_POST);		$added_on = date("Y-m-d H:i:s");
		$name=fnEncodeString($name);
		$address=fnEncodeString($address);		
		$contact_person= fnEncodeString($contact_person);
		$shop_added_by=$_SESSION[SESSION_PREFIX.'user_id'];
		$user_role=$_SESSION[SESSION_PREFIX.'user_role'];
		if ($user_role!='Admin') {
			$sql_selectparents = "SELECT parent_ids from tbl_users where id='".$shop_added_by."' limit 1";
					$result1 = mysqli_query($this->local_connection,$sql_selectparents);
					$row_count = mysqli_num_rows($result1);
					if($row_count > 0){	
						 while ($row = mysqli_fetch_assoc($result1)) {
				            $parent_ids = $row['parent_ids'];                
				        }
					}

		}
		if ($user_role=='Admin') {
			$parent_ids = 1;  
		}
				
		$state 	= $state;
		$city 	= $city;		
		
		$fields = '';
		$values = ''; 
		
		if($parent_ids != '')
		{
			$fields.= ",`user_parent_ids`";			
			$values.= ",'".$parent_ids."'";
		}
		if($pan_number != '')
		{
			$fields.= ",`pan_number`";			
			$values.= ",'".$pan_number."'";
		}
		//service by new field
		if($service_by_user_type != '')
		{
			$array = explode(',', $service_by_user_type); 
			if($array[2]!=''){
				$fields.= ",`service_by_usertype_id`";			
				$values.= ",'".$array[2]."'";
			}			
		}
		if($service_by_user_id != '')
		{
			$fields.= ",`service_by_user_id`";			
			$values.= ",'".$service_by_user_id."'";
		}
		if($fssai_number != '')
		{
			$fields.= ",`fssai_number`";			
			$values.= ",'".$fssai_number."'";
		}
		if($shop_type != '')
		{
			$fields.= ",`shop_type_id`";			
			$values.= ",'".$shop_type."'";
		}
		if($area != '')
		{
			$fields.= ",`suburbid`";			
			$values.= ",'".$area."'";
		}
		if($subarea != '')
		{
			$fields.= ",`subarea_id`";			
			$values.= ",'".$subarea."'";
		}
		if($contact_person_other != '')
		{
			$fields.= ",`contact_person_other`";
			$values.= ",'".fnEncodeString($contact_person_other)."'";
		}
		if($mobile_number_other != '')
		{
			$fields.= ",`mobile_number_other`";
			$values.= ",'".fnEncodeString($mobile_number_other)."'";
		}
		if($landline_number_other != '')
		{
			$fields.= ",`landline_number_other`";
			$values.= ",'".fnEncodeString($landline_number_other)."'";
		}
		if($gst_number != '')
		{
			$fields.= ",`gst_number`";
			$values.= ",'".fnEncodeString($gst_number)."'";
		}
		if($closedday != '')
		{
			$fields.= ",`closedday`";
			$values.= ",'".$closedday."'";
		}
		if($opentime != '')
		{
			$fields.= ",`opentime`";
			$values.= ",'".$opentime."'";
		}
		if($closetime != '')
		{
			$fields.= ",`closetime`";
			$values.= ",'".$closetime."'";
		}
		if($latitude != '')
		{
			$fields.= ",`latitude`";
			$values.= ",'".fnEncodeString($latitude)."'";
		}
		if($longitude != '')
		{
			$fields.= ",`longitude`";
			$values.= ",'".fnEncodeString($longitude)."'";
		}
		if($shop_margin != '')
		{
			$fields.= ",`shop_margin`";
			$values.= ",'".fnEncodeString($shop_margin)."'";
		}
		if($billing_name != '')
		{
			$fields.= ",`billing_name`";
			$values.= ",'".fnEncodeString($billing_name)."'";
		}
		if($bank_acc_name != '')
		{
			$fields.= ",`bank_acc_name`";
			$values.= ",'".fnEncodeString($bank_acc_name)."'";
		}
		if($bank_acc_no != '')
		{
			$fields.= ",`bank_acc_no`";
			$values.= ",'".fnEncodeString($bank_acc_no)."'";
		}
		if($bank_b_name != '')
		{
			$fields.= ",`bank_b_name`";
			$values.= ",'".fnEncodeString($bank_b_name)."'";
		}
		if($bank_ifsc != '')
		{
			$fields.= ",`bank_ifsc`";
			$values.= ",'".fnEncodeString($bank_ifsc)."'";
		}
		if($bank_name != '')
		{
			$fields.= ",`bank_name`";
			$values.= ",'".fnEncodeString($bank_name)."'";
		}
		if($shop_status != '')
		{
			$fields.= ",`shop_status`";
			$values.= ",'".fnEncodeString($shop_status)."'";
		}
		$status = 1;//shop added through web
		$sql = "INSERT INTO tbl_shops (`name`,`address`,`city`,`state`,`contact_person`,`mobile`,`shop_added_by`,`status`, `added_on` $fields) 
		VALUES('".$name."','".$address."','".$city."','".$state."','".$contact_person."','".$mobile."','".$shop_added_by."',$status, '".$added_on."' $values)";

		mysqli_query($this->local_connection,$sql);
		$shopid=mysqli_insert_id($this->local_connection); 
		$this->commonObj->log_add_record('tbl_shops',$shopid,$sql);			
	}
	
	public function updateShopDetails($id) {
		extract ($_POST);	
			//echo "<pre>";print_r($_POST);
		$name=fnEncodeString($name);
		$address=fnEncodeString($address);		
		$contact_person= fnEncodeString($contact_person);
		$shop_added_by=$_SESSION[SESSION_PREFIX.'user_id'];
		$state 	= $state;
		$city 	= $city;
		$values = ''; 
	
		if($area != '')
		{		
			$values.= ", `suburbid`='".$area."'";
		}
		if($subarea != '')
		{
			$values.= ",`subarea_id`='".$subarea."'";
		}
		else
		{
			$values.= ",`subarea_id`=''";
		}
		if($contact_person_other != '')
		{
			$values.= ", `contact_person_other`='".fnEncodeString($contact_person_other)."'";
		}else{
			$values.= ", `contact_person_other`=''";
		}
		if($mobile_number_other != '')
		{
			$values.= ", `mobile_number_other`='".fnEncodeString($mobile_number_other)."'";
		}else{
			$values.= ", `mobile_number_other`=''";
		}
		if($landline_number_other != '')
		{
			$values.= ", `landline_number_other`='".fnEncodeString($landline_number_other)."'";
		}else{
			$values.= ", `landline_number_other`=''";
		}
		if($gst_number != '')
		{
			$values.= ", `gst_number`='".fnEncodeString($gst_number)."'";
		}else
		{
			$values.= ", `gst_number`=''";
		}
        if($pan_number != '')
		{
			$values.= ", `pan_number`='".fnEncodeString($pan_number)."'";
		}else
		{
			$values.= ", `pan_number`=''";
		}
		if($fssai_number != '')
		{
			$values.= ", `fssai_number`='".fnEncodeString($fssai_number)."'";
		}else
		{
			$values.= ", `fssai_number`=''";
		}
		if($shop_type_id != '')
		{
			$values.= ", `shop_type_id`='".fnEncodeString($shop_type_id)."'";
		}else
		{
			$values.= ", `shop_type_id`=''";
		}
		if($closedday != '')
		{
			$values.= ", `closedday`='".$closedday."'";
		}else
		{
			$values.= ", `closedday`=''";
		}
		if($opentime != '')
		{
			$values.= ", `opentime`='".$opentime."'";
		}
		if($closetime != '')
		{
			$values.= ", `closetime`='".$closetime."'";
		}
		if($latitude != '')
		{
			$values.= ", `latitude`='".fnEncodeString($latitude)."'";
		}
		else
		{
			$values.= ", `latitude`=''";
		}
		if($longitude != '')
		{			
			$values.= ", `longitude`='".fnEncodeString($longitude)."'";
		}
		else
		{			
			$values.= ", `longitude`=''";
		}
		if($shop_margin != '')
		{			
			$values.= ", `shop_margin`='".fnEncodeString($shop_margin)."'";
		}
		else
		{			
			$values.= ", `shop_margin`=''";
		}
		if($billing_name != '')
		{
			$values.= ", `billing_name` = '".fnEncodeString($billing_name)."'";
		}
		if($bank_acc_name != '')
		{
			$values.= ", `bank_acc_name` = '".fnEncodeString($bank_acc_name)."'";
		}
		if($bank_acc_no != '')
		{
			$values.= ", `bank_acc_no` = '".fnEncodeString($bank_acc_no)."'";
		}
		if($bank_b_name != '')
		{
			$values.= ", `bank_b_name` = '".fnEncodeString($bank_b_name)."'";
		}
		if($bank_name != '')
		{
			$values.= ", `bank_name` = '".fnEncodeString($bank_name)."'";
		}
		if($bank_ifsc != '')
		{
			$values.= ", `bank_ifsc` = '".fnEncodeString($bank_ifsc)."'";
		}
		if($shop_status != '')
		{
			$values.= ", `shop_status` = '".fnEncodeString($shop_status)."'";
		}
		//new service by field
		if($service_by_user_type != '')
		{
			$array = explode(',', $service_by_user_type); 
			if($array[2]!=''){				
				$values.= ", `service_by_usertype_id` = '".$array[2]."'";
			}			
		}
		if($service_by_user_id != '')
		{
			$values.= ", `service_by_user_id` = '".$service_by_user_id."'";
		}		
		$update_sql="UPDATE tbl_shops SET name='$name',address='$address',state='$state',city='$city',contact_person='$contact_person',mobile='$mobile' $values where id='$id'";		
		mysqli_query($this->local_connection,$update_sql);
		$this->commonObj->log_update_record('tbl_shops',$id,$update_sql);
		
	}
	public function updateLeadDetails($id) {
		extract ($_POST);
		//echo "<pre>";print_r($_POST);exit();
		$name=fnEncodeString($name);
		$address=fnEncodeString($address);		
		$contact_person= fnEncodeString($contact_person);
		$shop_added_by=$_SESSION[SESSION_PREFIX.'user_id'];
		$state 	= $state;
		$city 	= $city;
		$values = ''; 
		
		if($area != '')
		{		
			$values.= ", `suburbid`='".$area."'";
		}
		if($subarea != '')
		{
			$values.= ",`subarea_id`='".$subarea."'";
		}
		else
		{
			$values.= ",`subarea_id`=''";
		}
		if($contact_person_other != '')
		{
			$values.= ", `contact_person_other`='".fnEncodeString($contact_person_other)."'";
		}else{
			$values.= ", `contact_person_other`=''";
		}
		if($mobile_number_other != '')
		{
			$values.= ", `mobile_number_other`='".fnEncodeString($mobile_number_other)."'";
		}else{
			$values.= ", `mobile_number_other`=''";
		}
		if($landline_number_other != '')
		{
			$values.= ", `landline_number_other`='".fnEncodeString($landline_number_other)."'";
		}else{
			$values.= ", `landline_number_other`=''";
		}
		if($gst_number != '')
		{
			$values.= ", `gst_number`='".fnEncodeString($gst_number)."'";
		}else
		{
			$values.= ", `gst_number`=''";
		}
		if($closedday != '')
		{
			$values.= ", `closedday`='".$closedday."'";
		}else
		{
			$values.= ", `closedday`=''";
		}
		if($opentime != '')
		{
			$values.= ", `opentime`='".$opentime."'";
		}
		if($closetime != '')
		{
			$values.= ", `closetime`='".$closetime."'";
		}
		if($latitude != '')
		{
			$values.= ", `latitude`='".fnEncodeString($latitude)."'";
		}
		else
		{
			$values.= ", `latitude`=''";
		}
		if($longitude != '')
		{			
			$values.= ", `longitude`='".fnEncodeString($longitude)."'";
		}
		else
		{			
			$values.= ", `longitude`=''";
		}		
		$values.= ", `billing_name` = '".fnEncodeString($billing_name)."'";
		$values.= ", `bank_acc_name` = '".fnEncodeString($bank_acc_name)."'";
		$values.= ", `bank_acc_no` = '".fnEncodeString($bank_acc_no)."'";
		$values.= ", `bank_b_name` = '".fnEncodeString($bank_b_name)."'";
		$values.= ", `bank_name` = '".fnEncodeString($bank_name)."'";		
		$values.= ", `bank_ifsc` = '".fnEncodeString($bank_ifsc)."'";
		
		//new service by field
		if($service_by_user_type != '')
		{
			$array = explode(',', $service_by_user_type); 
			if($array[2]!=''){				
				$values.= ", `service_by_usertype_id` = '".$array[2]."'";
			}			
		}
		if($service_by_user_id != '')
		{
			$values.= ", `service_by_user_id` = '".$service_by_user_id."'";
		}
		$update_sql="UPDATE tbl_leads SET name='$name',address='$address',state='$state',city='$city',contact_person='$contact_person',mobile='$mobile' $values where id='$id'";
		mysqli_query($this->local_connection,$update_sql);
		$this->commonObj->log_update_record('tbl_shops',$id,$update_sql);
		
	}
	public function getShopStatusNLog($id) {		
		$sql1="SELECT `status`, `status_seen_log` 
		FROM tbl_shops WHERE id = '$id' ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	public function updateViewStatus($id) {
		$shop_data = $this->getShopStatusNLog($id);
		$current_log = $shop_data['status_seen_log'];//json format
		$current_log_array = json_decode($current_log);
		$current_log_array = (array)$current_log_array;
		$log_array = array();
		$status = '';
		if($shop_data['status'] != 0 && $shop_data['status'] != NULL){//Shop added by Sales Person
			switch($_SESSION[SESSION_PREFIX.'user_type']){
				case "Admin":											
					if($shop_data['status'] != 4)//shop details not seen by Admin
					{
						$status = 4;	
						$log_array['date'] = date('d-m-Y');
						$log_array['by'] = $_SESSION[SESSION_PREFIX.'user_type']."_4";
						$log_array['by_id'] = $_SESSION[SESSION_PREFIX.'user_id'];
					}
				break;
				case "Superstockist":											
					if($shop_data['status'] != 3)//shop details not seen by Superstockist
					{	
						$status = 3;
						$log_array['date'] = date('d-m-Y');
						$log_array['by'] = $_SESSION[SESSION_PREFIX.'user_type']."_3";
						$log_array['by_id'] = $_SESSION[SESSION_PREFIX.'user_id'];
					}
				break;
				case "Distributor":
					if($shop_data['status'] != 2)//shop details not seen by Distributor
					{							
						$status = 2;	
						$log_array['date'] = date('d-m-Y');
						$log_array['by'] = $_SESSION[SESSION_PREFIX.'user_type']."_2";
						$log_array['by_id'] = $_SESSION[SESSION_PREFIX.'user_id'];
					}
				break;
			}
			if($status != '')
			{
				if($shop_data['status_seen_log'] != '' && !in_array($status,array_values($current_log_array['status']))){
					$current_log_array['status'] = (array)$current_log_array['status'];
					$current_log_array['log'] = (array)$current_log_array['log'];
					$current_log_array['status'][] = $status;
					$current_log_array['log'][] = $log_array;
					$update_json = json_encode($current_log_array);
					$update_sql="UPDATE tbl_shops SET status='$status',status_seen_log='$update_json' where id='$id'";	
					mysqli_query($this->local_connection,$update_sql);	
				}else{
					$new_log_array['status'][] = $status;
					$new_log_array['log'][] = $log_array;
					$update_json = json_encode($new_log_array);
					$update_sql="UPDATE tbl_shops SET status='$status',status_seen_log='$update_json' where id='$id'";		
					mysqli_query($this->local_connection,$update_sql);	
				}				
			}
		}		
	}
	public function getAllShops() {
		$where_clause = "";		
		if(!empty($_GET['state'])){
			$where_clause .=" and state=".$_GET['state'].' ';
			if($_GET['city'] != ""){
				$where_clause .=" and city=".$_GET['city'].' ';
				if($_GET['area'] != ""){
					$where_clause .=" and suburbid = ".$_GET['area'].' ';
					if($_GET['subarea'] != ""){
						$where_clause .=" and subarea_id = ".$_GET['subarea'].' ';
					}
				}
			}
		}
		if($where_clause != '')
			$where_clause = ' WHERE '.$where_clause;
		
		$sql1="SELECT `id`, `name`, `address`, `city`, `state`, `contact_person`, `mobile`, `shop_added_by`, `lat`, 
		`lon`, `contact_person_other`, `mobile_number_other`, `gst_number`, `suburbid`, `closedday`, `opentime`, 
		`closetime`, `latitude`, `longitude`
		FROM tbl_shops ".$where_clause. " order by name";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}
   function getRouteCount() {
       // $user_id=$_SESSION[SESSION_PREFIX . "user_id"];          
       $sql = "SELECT route_name,id  FROM tbl_route WHERE isdeleted!=1 ";
        $result = mysqli_query($this->local_connection, $sql);
       $route=array();
        $row_count = mysqli_num_rows($result);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $route[] = $row['id'];
            }
            return $route;
        } else {
            return 0;
        } 
    }
      function getRouteCountDetails($route_id) {
		 $sql = "SELECT r.route_name,r.id as route_id,s.name as state_name,c.name as city_name,a.suburbnm as area_name,sa.subarea_name
		FROM tbl_route_details rd 
		LEFT JOIN tbl_state s ON s.id = rd.state_id 
		LEFT JOIN tbl_city c ON c.id = rd.city_id
		LEFT JOIN tbl_area a ON a.id = rd.area_id 
		LEFT JOIN tbl_subarea sa ON sa.subarea_id = rd.subarea_id
		LEFT JOIN tbl_route r ON r.id = rd.route_id 
		WHERE rd.route_id='".$route_id."' ORDER BY rd.id ASC";
        $result = mysqli_query($this->local_connection, $sql);
        
        $i = 0;
        $row_count = mysqli_num_rows($result);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    }

     function getRoutePopup($route_id) {

		$sql = "SELECT rd.lattitude,rd.longitude,r.route_name,r.id as route_id,s.name as state_name,c.name as city_name,a.suburbnm as area_name,sa.subarea_name
		FROM tbl_route_details rd 
		LEFT JOIN tbl_state s ON s.id = rd.state_id 
		LEFT JOIN tbl_city c ON c.id = rd.city_id
		LEFT JOIN tbl_area a ON a.id = rd.area_id 
		LEFT JOIN tbl_subarea sa ON sa.subarea_id = rd.subarea_id
		LEFT JOIN tbl_route r ON r.id = rd.route_id 
		WHERE rd.route_id='".$route_id."' ORDER BY rd.id ASC";
        $result = mysqli_query($this->local_connection, $sql);
       // $row_orders_det_count = mysqli_num_rows($orders_det_result);
       // return $row = mysqli_fetch_array($orders_det_result);
        
       
    }











}
?>