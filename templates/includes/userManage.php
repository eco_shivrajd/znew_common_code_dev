<?php
/***********************************************************
 * File Name	: userManage.php
 ************************************************************/	
include "../includes/commonManage.php";	
class userManager 
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
		$this->commonObj 	= 	new commonManage($this->local_connection,$this->common_connection);
	}
	public function getAllLocalUserDetails($user_type,$external_id=''){
		$where_clause = ''; 
		if($user_type == 'SalesPerson' && $_SESSION[SESSION_PREFIX.'user_type'] == "Superstockist" && $external_id != ''){
			$where_clause = " AND (sstockist_id = ". $external_id.")";
		}else if($external_id != '')
		{			
			$where_clause = " AND (external_id IN (". $external_id.") OR external_id LIKE ('%,". $external_id."%'))";
		}
		$sql1="SELECT `id`, `external_id`, `firstname`, `username`, 
		`pwd`, `user_type`, `address`, `city`, `state`, `mobile`, `email`, 
		`reset_key`, `is_default_user`,`is_common_user`, `sstockist_id`
		FROM tbl_user where user_type = '".$user_type."' AND tbl_user.isdeleted!='1' $where_clause order by is_default_user desc, firstname asc";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}


	 public function getUserTitle($frmdate = null, $todate = null) {
        extract($_POST);
        $user_type_title = '';
        switch ($user_type) {
            case "superstockist":
                $user_type_title = 'Super Stockist';
                break;
            case "Distributor":
                $user_type_title = 'Stockist';
                break;
            case "salesperson":
                $user_type_title = 'Sales Person';
                break;
            case "DeliveryPerson":
                $user_type_title = 'Delivery Person';
                break;
            case "Accountant":
                $user_type_title = 'Accountant';
                break;
            case "DeliveryChannelPerson":
                $user_type_title = 'Delivery Channel Person';
                break;
        }       
        $user_type_title = $user_type_title . $date;
        return $user_type_title;
    }
	 public function getAllUsers() {
        extract($_POST);
        $limit = '';
        $order_by = '';
        $search_name = '';

			$sql="SELECT u.id, u.external_id, u.firstname, u.username, u.pwd, u.user_type, u.address, c.name as cityname, s.name as statename, u.mobile, u.email, u.is_default_user,u.is_common_user, u.sstockist_id 
			FROM tbl_user u 
			LEFT JOIN tbl_state s ON u.state = s.id 
			LEFT JOIN tbl_city c ON u.city = c.id where u.user_type = '".$user_type."' AND u.isdeleted!='1' order by u.is_default_user desc, u.firstname asc"; 
        //echo $sql;echo "222 ".$sql2;
        $result1 = mysqli_query($this->local_connection, $sql);
        $i = 1;
        $row_count = mysqli_num_rows($result1);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result1)) {
                $records[$i] = $row;
                $i++;
            }
            if ($sql2 != '') {
                $result2 = mysqli_query($this->local_connection, $sql2);
                $j = 1;
                $row_count2 = mysqli_num_rows($result2);
                if ($row_count2 > 0) {
                    $name = array_column($records, 'Name');

                    while ($row2 = mysqli_fetch_assoc($result2)) {
                        if (!in_array($row2['Name'], $name)) {
                            $records2[$j] = $row2;
                            $j++;
                        }
                    }
                } else {
                    $records2 = 0;
                }
            }
            if ($records2 != 0) {
                $records = array_merge($records, $records2);
            }
            return $records;
        } else {
            return 0;
        }
    }

  public function getAllUsersView() 
  {
        extract($_POST);
        $limit = '';
        $order_by = '';
        $search_name = '';

        $sess_user_type  = $_SESSION[SESSION_PREFIX.'user_type'];
        $sess_user_id  = $_SESSION[SESSION_PREFIX.'user_id'];
        $cond ="";
        if($sess_user_type=='Superstockist') 
        {
        	$cond = "AND sstockist_id = $sess_user_id";
        	$sql="SELECT id,firstname,user_type,address,cityname,suburb_ids,subarea_ids,stockist_name,sstockist_name,statename,mobile,email,is_common_user,sstockist_id FROM tbl_user_view where user_type ='".$user_type."' ".$cond." "; 
        }
        if($sess_user_type=='Distributor') 
        {
        	$sess_user_id1 = ",".$sess_user_id;
        	$sess_user_id2 = $sess_user_id.",";

        	if ($user_type == 'salesperson' || $user_type == 'DeliveryPerson') 
        	{
        		$sql="SELECT id,firstname,user_type,address,cityname,suburb_ids,subarea_ids,stockist_name,sstockist_name,statename,mobile,email,is_common_user,sstockist_id,stockist_id FROM tbl_user_view where user_type ='".$user_type."' AND (stockist_id LIKE '$sess_user_id1%' OR stockist_id LIKE '$sess_user_id2%' OR stockist_id = $sess_user_id)"; 
        	}
        	else
        	{
        		$sql="SELECT id,firstname,user_type,address,cityname,suburb_ids,subarea_ids,stockist_name,sstockist_name,statename,mobile,email,is_common_user,sstockist_id,stockist_id FROM tbl_user_view where user_type ='".$user_type."' ";
        	}
 
        }

       if($sess_user_type=='Admin') 
        {
	     $sql="SELECT id,firstname,user_type,address,cityname,suburb_ids,subarea_ids,stockist_name,sstockist_name,statename,mobile,email,is_common_user,sstockist_id FROM tbl_user_view where user_type ='".$user_type."' ".$cond." "; 
	   }
        //echo $sql;echo "222 ".$sql2;
        $result1 = mysqli_query($this->local_connection, $sql);
       $row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;  
    }

  public function getAllUsersViewAllDetails($user_details_id) 
  {
        extract($_POST);
        $limit = '';
        $order_by = '';
        $search_name = '';
	     $sql="SELECT id,external_id,firstname,username,pwd,user_type,address,subarea_name,suburb_ids,subarea_ids,sstockist_name,suburbnm,cityname,statename,mobile,email,is_default_user,is_common_user,sstockist_id,stockist_id,accname,accno,accbrnm,bank_name,accifsc,	gst_number_sss,office_phone_no FROM tbl_user_view where id ='".$user_details_id."'"; 
	     $users_det_result = mysqli_query($this->local_connection, $sql);
         return $row = mysqli_fetch_assoc($users_det_result);
        //echo $sql;echo "222 ".$sql2;
       
  }


	public function getLocalUserDetailsByUserType($user_type,$external_id='') {
		$where_clause = '';
		if($external_id != '')
		{
			$where_clause = " AND external_id = ". $external_id;
		}
		$sql1="SELECT `id`, `external_id`, `firstname`, `username`, 
		`pwd`, `user_type`, `address`, `city`, `state`, `mobile`, `email`, 
		`reset_key`, `is_default_user`, `sstockist_id`, `stockist_id`
		FROM tbl_user where user_type = '".$user_type."' AND tbl_user.isdeleted!='1' $where_clause order by is_default_user desc, firstname asc";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}
	public function getUserTypeConfig($user_role,$external_id='') {
		$where_clause = '';
		//echo "string";
	//	exit();
		if($external_id != '')
		{
			$where_clause = " AND external_id = ". $external_id;
		}
		$sql1="SELECT `id`, `external_id`, `firstname`, `username`, 
		`pwd`, `user_role`, `address`, `city`, `state`, `mobile`, `email`, 
		`reset_key`, `is_default_user`, `sstockist_id`, `stockist_id`
		FROM tbl_user where user_role = '".$user_role."' AND tbl_user.isdeleted!='1' $where_clause order by is_default_user desc, firstname asc";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}
	public function getLocalUserDetails($user_id) {
		$sql1 ="SELECT `id`, `external_id`, `firstname`, `username`, `pwd`, `user_type`, `address`,`subarea_ids`,`suburb_ids`, `city`, `state`, `mobile`,`office_phone_no`,`user_status`, `email`, `reset_key`, `is_default_user`, `sstockist_id`, `gst_number_sss` from tbl_user
where id = '".$user_id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	public function getLocalUserArea($suburb_ids2) {
		$sql1 ="SELECT `suburbnm` from tbl_area
          where id = '".$suburb_ids2."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	public function getAllLocalUserDetailss($user_id) {
		$sql1="SELECT `id`, `external_id`, `firstname`, `username`, 
		`pwd`, `user_type`, `address`, `subarea_ids`, `suburb_ids`,`city`, `state`, `mobile`,`office_phone_no`,`user_status`, `accname`,`accno`,`bank_name`,`accbrnm`,`accifsc`,`latitude`,`longitude`,
		`email`, `reset_key`, `is_default_user`, `sstockist_id`,`stockist_id`, `gst_number_sss`
		FROM tbl_user where id = '".$user_id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	
	public function get_cod_percent() {
		$sql1="SELECT cod_percent FROM tbl_cod_admin where status = 'active'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	public function updateLocalUserOtherDetails($user_id) {
		extract ($_POST);		
		$values = ''; 
		
		if($phone_no != '')
		{
			$values.= ", `phone_no` = '".$phone_no."'";
		}
		if($tollfree_no != '')
		{
			$values.= ", `tollfree_no` = '".$tollfree_no."'";
		}
		if($website != '')
		{
			$values.= ", `website` = '".$website."'";
		}
		if($declaration != '')
		{
			$values.= ", `declaration` = '".$declaration."'";
		}
		if($accname != '')
		{
			$values.= ", `accname` = '".$accname."'";
		}
		if($accno != '')
		{
			$values.= ", `accno` = '".$accno."'";
		}
		if($accbrnm != '')
		{
			$values.= ", `accbrnm` = '".$accbrnm."'";
		}
		if($accifsc != '')
		{
			$values.= ", `accifsc` = '".$accifsc."'";
		}
		if($bank_name != '')
		{
			$values.= ", `bank_name` = '".$bank_name."'";
		}
		if($gst_number_sss != '')
		{
			$values.= ", `gst_number_sss` = '".$gst_number_sss."'";
		}	
		$user_other_details = "UPDATE tbl_user_details SET userid='$user_id' $values WHERE userid='$user_id'";				
			
		mysqli_query($this->local_connection,$user_other_details);
		$this->commonObj->log_update_record('tbl_user_details',$user_id, $user_other_details);
	}
	public function addLocalUserOtherDetails($user_id) {
		extract ($_POST);		
		$values = ''; 
		$fields = '';		
		if($phone_no != '')
		{
			$fields.= ",`phone_no`";
			$values.= ",'".$phone_no."'";
		}
		if($accname != '')
		{
			$fields.= ",`accname`";			
			$values.= ",'".$accname."'";
		}
		if($accno != '')
		{
			$fields.= ",`accno`";			
			$values.= ",'".$accno."'";
		}
		if($bank_name != '')
		{
			$fields.= ",`bank_name`";			
			$values.= ",'".$bank_name."'";
		}
		if($accbrnm != '')
		{
			$fields.= ",`accbrnm`";			
			$values.= ",'".$accbrnm."'";
		}
		if($accifsc != '')
		{
			$fields.= ",`accifsc`";			
			$values.= ",'".$accifsc."'";
		}
		 $user_other_details = "INSERT INTO tbl_user_details (`userid` $fields) 
		VALUES( '".$user_id."' $values)";
		mysqli_query($this->local_connection,$user_other_details);
		$this->commonObj->log_update_record('tbl_user_details',$user_id, $user_other_details);
	}
	public function assignRegion() {	
		extract ($_POST);	
		$region_id = $assign;			
		if(count($assign) > 1){
			$region_id = implode(',',$assign);
		}else{
			$region_id = $assign[0];
		}				
		
		$values.= " `suburb_ids`= '".$region_id."'";
		$update_user_sql = "UPDATE tbl_user_working_area SET $values WHERE user_id='$sales_p_id'";
		return mysqli_query($this->local_connection,$update_user_sql);
		$this->commonObj->log_update_record('tbl_user_working_area',$sales_p_id,$update_user_sql);
	}
	public function getLocalUserWorkingAreaDetails($user_id){
		$sql1="SELECT `state_ids`, `city_ids`, `suburb_ids`, `subarea_ids` FROM tbl_user_working_area where user_id = '".$user_id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;	
	}
	public function getCommonUserDetails($user_id) {
		$sql1="SELECT `uid`, `emailaddress`, `passwd`, `username`, `level`, `reset_key`, `id`, `address`, `state`, `city`, `mobile`, `firstname`, `suburbid` 
		FROM tbl_users where id = '".$user_id."'";
		$result1 = mysqli_query($this->common_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}

	public function getAdminDetails($user_id) {	
		 $sql1="SELECT UD.userid,U.gst_number_sss, UD.accname,  UD.accno, UD.accbrnm, UD.accifsc, UD.phone_no, UD.tollfree_no, UD.bank_name,U.gst_number_sss,UD.website, UD.declaration
		FROM tbl_user_details UD
        LEFT JOIN tbl_user U ON U.id = UD.userid
		where UD.userid = '".$user_id."' AND UD.isdeleted!='1' ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);	
		}else
			return $row_count;		
	}
	public function getLocalUserOtherDetails($user_id,$user_type='') {	
		$sql1="SELECT `userid`, `accname`,  `accno`, `accbrnm`, `accifsc`, `phone_no`, `tollfree_no`, `bank_name`,`gst_number_sss`,`website`, `declaration`
		FROM tbl_user_details where userid = '".$user_id."' AND isdeleted!='1' ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);	
		}else
			return $row_count;		
	}

		public function getLocalUserWorkingRegionDetails($user_id) {
		$sql1="SELECT `user_id`, `state_ids`, `city_ids`, `suburb_ids`, `subarea_ids`,
		(SELECT name FROM tbl_state WHERE id = tbl_user_working_area.state_ids) AS state_name,
		(SELECT name FROM tbl_city WHERE id = tbl_user_working_area.city_ids) AS city_name,
		(SELECT  suburbnm FROM tbl_area WHERE id = tbl_user_working_area.suburb_ids) AS region_name
		FROM `tbl_user_working_area`
		where user_id = '".$user_id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	  }
	public function getCommonUserDetailsByUsername($username) {
		$sql1="SELECT `uid`, `emailaddress`, `passwd`, `username`, `level`, `reset_key`, `id`, `address`, `state`, `city`, `mobile`, `firstname`, `suburbid` 
		FROM tbl_users where username='".$username."'";
		$result1 = mysqli_query($this->common_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		} else
			return $row_count;			
	}
	public function getCommonUserCompanyDetails($user_id) {
		$sql1="SELECT `id`, `userid`, `companyid` FROM tbl_user_company where userid='".$user_id."' AND companyid='".COMPID."'";
		$result1 = mysqli_query($this->common_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;
	}
	public function addLocalUserDetails($user_type,$common_user_id) {
		extract ($_POST);
		$external_id =	$_SESSION[SESSION_PREFIX.'user_id'];	
		
		if(($user_type == 'Distributor' OR $user_type == 'SalesPerson' OR $user_type == 'Shopkeeper') && $assign != "")
		{
			$external_id = $assign;
			if($user_type == 'SalesPerson'){
				if(count($assign) > 1){
					$external_id = implode(',',$assign);
				}else{
					$external_id = $assign[0];
				}
				if(gettype($_POST['assign']) == string)
					$external_id = $assign;
			}
		}
		
		$surname	=	"";
		$name		=	fnEncodeString(trim($firstname));
		$username	=	fnEncodeString($username);
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$email		=	fnEncodeString($email);
		$mobile		= 	fnEncodeString($mobile);		
		$address	= 	fnEncodeString($address);		
		
		$fields = '';
		$values = ''; 
		if($user_type == 'SalesPerson'){			
			$fields.= ",`sstockist_id`";
			$values.= ",".$cmbSuperStockist;
		}
		if($email != '')
		{
			$fields.= ",`email`";
			$values.= ",'".$email."'";
		}
		if($mobile != '')
		{
			$fields.= ",`mobile`";
			$values.= ",'".$mobile."'";
		}
		if($address != '')
		{
			$fields.= ",`address`";
			$values.= ",'".$address."'";
		}
		if($state != '')
		{
			$fields.= ",`state`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city`";
			$values.= ",'".$city."'";
		}
		if($gstnumber != '')
		{
			$fields.= ",`gst_number_sss`";
			$values.= ",'".$gstnumber."'";
		}
		$added_on = date('Y-m-d H:i:s');
		
		$user_sql = "INSERT INTO tbl_user (`common_user_id`,`added_on`,`external_id`,`firstname`,`username`,`pwd`,`user_type` $fields) 
		VALUES('".$common_user_id."','".$added_on."','".$external_id."','".$surname."','".$name."','".$username."','".$password."','".$user_type."' $values)";
		mysqli_query($this->local_connection,$user_sql);		
		$userid=mysqli_insert_id($this->local_connection); 
		$this->commonObj->log_add_record('tbl_user',$userid,$user_sql);	
		return $userid;
	}

		public function addAllLocalUserDetails($user_type,$common_user_id) {
		extract ($_POST);		
       // $external_id =	$_SESSION[SESSION_PREFIX.'user_id'];
		//print"<pre>";print_r($_POST);
      //  exit();
		if ($user_type == 'Superstockist') 
		{
				$external_id =	1;
		}
		if ($user_type == 'Accountant') 
		{
				$external_id =	1;
		}
		if ($user_type == 'Distributor') 
		{
				$external_id =	$cmbSuperStockist;
		}
		if ($user_type == 'DeliveryChannelPerson') 
		{
				$external_id =	$cmbSuperStockist;
		}	
		
		if(($user_type == 'SalesPerson' OR $user_type == 'DeliveryPerson' OR $user_type == 'Shopkeeper') && $assign != "")
		{
			$external_id = $assign;
			if($user_type == 'SalesPerson' || $user_type == 'DeliveryPerson' || $user_type == 'Shopkeeper'){
				if(count($assign) > 1){
					$external_id = implode(',',$assign);
				}else{
					$external_id = $assign[0];
				}
				if(gettype($_POST['assign']) == string)
					$external_id = $assign;
			}
		}		
			$stockist_external_id = $assign;
			if($user_type == 'SalesPerson' || $user_type == 'DeliveryPerson' || $user_type == 'Shopkeeper')
			{
				if(count($assign) > 1){
					$stockist_external_id = implode(',',$assign);
				}else{
					$stockist_external_id = $assign[0];
				}
				if(gettype($_POST['assign']) == string)
					$stockist_external_id = $assign;
			}
		
		$surname	=	"";
		$name		=	fnEncodeString(trim($firstname));
		$username	=	fnEncodeString($username);
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$email		=	fnEncodeString($email);
		$mobile		= 	fnEncodeString($mobile);		
		$address	= 	fnEncodeString($address);		
		
		$fields = '';
		$values = ''; 
		if($user_type != 'Superstockist' && $user_type != 'Accountant')
		{			
			$fields.= ",`sstockist_id`";
			//$values.= ",".$cmbSuperStockist;
			$values.= ",'".$cmbSuperStockist."'";
		}
		if($stockist_external_id != '')
		{			
			$fields.= ",`stockist_id`";
			//$values.= ",".$stockist_external_id;
			$values.= ",'".$stockist_external_id."'";
		}

		if($email != '')
		{
			$fields.= ",`email`";
			$values.= ",'".$email."'";
		}
		if($mobile != '')
		{
			$fields.= ",`mobile`";
			$values.= ",'".$mobile."'";
		}
		if($address != '')
		{
			$fields.= ",`address`";
			$values.= ",'".$address."'";
		}
		if($state != '')
		{
			$fields.= ",`state`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city`";
			$values.= ",'".$city."'";
		}
		if($area != '')
		{
			$fields.= ",`suburb_ids`";
			$suburb_ids = implode(',',$area);
			$values.= ",'".$suburb_ids."'";
		}
		if($subarea != '')
		{
			$fields.= ",`subarea_ids`";
			$subarea_ids = implode(',',$subarea);
			$values.= ",'".$subarea_ids."'";
		}
		if($gstnumber != '')
		{
			$fields.= ",`gst_number_sss`";
			$values.= ",'".$gstnumber."'";
		}
		if($phone_no != '')
		{
			$fields.= ",`office_phone_no`";
			$values.= ",'".$phone_no."'";
		}
		if($accname != '')
		{
			$fields.= ",`accname`";			
			$values.= ",'".$accname."'";
		}
		if($accno != '')
		{
			$fields.= ",`accno`";			
			$values.= ",'".$accno."'";
		}
		if($bank_name != '')
		{
			$fields.= ",`bank_name`";			
			$values.= ",'".$bank_name."'";
		}
		if($accbrnm != '')
		{
			$fields.= ",`accbrnm`";			
			$values.= ",'".$accbrnm."'";
		}
		if($accifsc != '')
		{
			$fields.= ",`accifsc`";			
			$values.= ",'".$accifsc."'";
		}
		if($latitude != '')
		{
			$fields.= ",`latitude`";
			$values.= ",'".fnEncodeString($latitude)."'";
		}
		if($longitude != '')
		{
			$fields.= ",`longitude`";
			$values.= ",'".fnEncodeString($longitude)."'";
		}
		$added_on = date('Y-m-d H:i:s');
		
	   $user_sql = "INSERT INTO tbl_user (`common_user_id`,`added_on`,`external_id`,
	   `firstname`,`username`,`pwd`,`user_type` $fields) 
		VALUES('".$common_user_id."','".$added_on."','".$external_id."','".$name."','".$username."','".$password."','".$user_type."' $values)";
		
		mysqli_query($this->local_connection,$user_sql);		
		 $local_userid=mysqli_insert_id($this->local_connection);
		if($user_type == 'Shopkeeper')
		{
			if(count($assignshopids>0)){
				foreach($assignshopids as $key=>$shopid){
					$shopkeeper_shops_sql = "UPDATE tbl_shops 
					SET shop_owner_id='$local_userid' 
					WHERE id='$shopid'";			
					mysqli_query($this->local_connection,$shopkeeper_shops_sql);
				}
			}
		}
		$this->commonObj->log_add_record('tbl_user',$userid,$user_sql);	
		//return $userid;
	}
	public function addLocalUserWorkingAreaDetails($user_id) {
		extract ($_POST);
		$fields = '';
		$values = ''; 
		if($state != '')
		{
			$fields.= ",`state_ids`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city_ids`";
			$values.= ",'".$city."'";
		}
		if($area != '')
		{
			$fields.= ",`suburb_ids`";
			$suburb_ids = implode(',',$area);
			$values.= ",'".$suburb_ids."'";
		}
		if($subarea != '')
		{
			$fields.= ",`subarea_ids`";
			$subarea_ids = implode(',',$subarea);
			$values.= ",'".$subarea_ids."'";
		}
		
		$user_working_area = "INSERT INTO tbl_user_working_area (`user_id` $fields) 
		VALUES('".$user_id."' $values)";
		mysqli_query($this->local_connection,$user_working_area);		
		$this->commonObj->log_add_record('tbl_user_working_area',$user_id,$user_working_area);	
	}
	public function addCommonUserDetails($user_type) {
		extract ($_POST);
         /* echo "string";
		print_r($_POST);
	    exit();*/
		$email		=	fnEncodeString($email);
		$username	=	fnEncodeString($username);
		$firstname		=	fnEncodeString(trim($firstname));
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$fields = '';
		$values = ''; 
		if($state != '')
		{
			$fields.= ",`state`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city`";
			$values.= ",'".$city."'";
		}
		if($mobile != '')
		{
			$fields.= ",`mobile`";
			$values.= ",'".$mobile."'";
		}
		if($address != '')
		{
			$fields.= ",`address`";
			$values.= ",'".$address."'";
		}
		if($firstname != '')
		{
			$fields.= ",`firstname`";
			$values.= ",'".$firstname."'";
		}
		$added_on = date('Y-m-d H:i:s');
		$user_sql = "INSERT INTO tbl_users (`company_id`, `emailaddress`, `passwd`, `username`, `added_on`, `level` $fields) 
		VALUES('".COMPID."','".$email."','".$password."','".$username."','".$added_on."','".$user_type."' $values)";
		
		mysqli_query($this->common_connection,$user_sql);	
		return $userid=mysqli_insert_id($this->common_connection); 		
	}


	
	public function getCommonUserID($id) {
		$sql1="SELECT `common_user_id`	FROM tbl_user where id = '".$id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;			
	}
	public function addCommonUserCompanyDetails($user_id) {		
		$company_sql = "INSERT INTO tbl_user_company(userid,companyid)  VALUES('".$user_id."','".COMPID."')";
		mysqli_query($this->common_connection,$company_sql);
	}
	public function sendUserCreationEmail() {
		extract ($_POST);
		$username	=	fnEncodeString($username);
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$subject   = "Account Created";
		$fromMail  = FROMMAILID;		
		$headers = "";
		$headers .= "From: ".$fromMail."\r\n";
		$headers .= "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-Type: text/html;" . "\r\n";
		$message = "";
		$message .= "Hi, <br/>";
		$message .= "Your account created successfully<br/>";
		$message .= "Please find the login details below<br/>";
		$message .= "Username: $username<br/>";
		$message .= "Password: $rand<br/>";
		$message .= "<a href='".SITEURL."templates/login.php'>Click Here</a> to Login<br/>";
		$message .= "<br/><br/>";
		$message .= "Thanks,<br/>";
		$message .= "Admin.";
		if($username != ""){
			$sent = @mail($username,$subject,$message,$headers);
		}
	}
	public function updateLocalUserDetails($user_type, $user_id) {

		extract ($_POST);	
		$address= fnEncodeString($address);	
		$email	= fnEncodeString($email);
		$mobile	= fnEncodeString($mobile);			
		//$values = '';
		
		$values.= "`address`= '".$address."'";				
		$values.= ", `state`= '".$state."'";
		$values.= ",`city`= '".$city."'";
		
		if($area != '')
		{
			$suburb_ids = implode(',',$area);
			$values.= ", `suburb_ids`= '".$suburb_ids."'";
		}else{
			$values.= ", `suburb_ids`= ''";
		}
		if($subarea != '')
		{			
			$subarea_ids = implode(',',$subarea);
			$values.= ",`subarea_ids`= '".$subarea_ids."'";
		}else{
			$values.= ",`subarea_ids`= ''";
		}
		$values.= ", `email`= '".$email."'";				
		$values.= ", `mobile`= '".$mobile."'";				
		
		if($phone_no != '')
		{
			$values.= ", `phone_no`= '".$phone_no."'";				
		}
		if(($user_type == 'Distributor' OR $user_type == 'SalesPerson') && $assign != "")
		{
			$external_id = $assign;
			if($user_type == 'SalesPerson'){
				if(count($assign) > 1){
					$external_id = implode(',',$assign);
				}else{
					$external_id = $assign[0];
				}	
				
			}
			$values.= ", `external_id`= '".$external_id."'";
		}else if($user_type == 'SalesPerson' && $assign == ""){
			$values.= ", `external_id`= ''";
		}
		if($user_type == 'SalesPerson' || $user_type == 'DeliveryChannelPerson' || $user_type == 'Accountant' || $user_type == 'DeliveryPerson'){
			$values.= ", `sstockist_id`= ".$cmbSuperStockist;
		}
		if($user_type == 'Superstockist'||$user_type =='Distributor'){
			$values.= ", `gst_number_sss`= '".$gstnumber."'";
		}
		//if($_SESSION[SESSION_PREFIX.'user_type'] == 'Superstockist' OR $_SESSION[SESSION_PREFIX.'user_type'] == 'Distributor')
		//{
			if($firstname!="") {
				$values.= ", `firstname`= '".fnEncodeString(trim($firstname))."'";	
			}
			if($username!="") {
				$values.= ", `username`= '".fnEncodeString($username)."'";					
			}
			
		//}
		if($_SESSION[SESSION_PREFIX.'user_type'] == 'Admin'){
			if($cod_percent!="") {
				$update_cod_sql = "update  tbl_cod_admin SET status='inactive'";
				mysqli_query($this->local_connection,$update_cod_sql);
				
				$insert_cod_sql = "insert  tbl_cod_admin SET cod_percent='$cod_percent',status='active'";
				mysqli_query($this->local_connection,$insert_cod_sql);
			}
		}
		
	    $update_user_sql = "UPDATE tbl_user SET $values WHERE id='$user_id'";
		//exit();
		mysqli_query($this->local_connection,$update_user_sql);
		$this->commonObj->log_update_record('tbl_user',$user_id,$update_user_sql);
	}

	public function updateAllLocalUserDetails($user_type, $user_id) {

		extract ($_POST);	
		//print_r($_POST);
		//exit();
		$address= fnEncodeString($address);	
		$email	= fnEncodeString($email);
		$mobile	= fnEncodeString($mobile);			
		$values = '';
		
		$values.= " `id`= '".$user_id."'";
			
		if($address != '')
		{
			$values.= ", `address`= '".$address."'";				
		}
		if($state != '')
		{
			$values.= ", `state`= '".$state."'";
		}
		if($city != '')
		{	
			$values.= ",`city`= '".$city."'";
		}
		if($email != '')
		{
			$values.= ", `email`= '".$email."'";				
		}
		if($mobile != '')
		{
			$values.= ", `mobile`= '".$mobile."'";				
		}
		if($phone_no != '')
		{
			$values.= ", `office_phone_no` = '".$phone_no."'";
		}
		if($accname != '')
		{
			$values.= ", `accname` = '".$accname."'";
		}
		if($accno != '')
		{
			$values.= ", `accno` = '".$accno."'";
		}
		if($accbrnm != '')
		{
			$values.= ", `accbrnm` = '".$accbrnm."'";
		}
		if($accifsc != '')
		{
			$values.= ", `accifsc` = '".$accifsc."'";
		}
		if($bank_name != '')
		{
			$values.= ", `bank_name` = '".$bank_name."'";
		}
		if($gst_number_sss != '')
		{
			$values.= ", `gst_number_sss` = '".$gst_number_sss."'";
		}
		if($area != '')
		{
			$suburb_ids = implode(',',$area);
			$values.= ", `suburb_ids` = '".$suburb_ids."'";
		}
		if($subarea != '')
		{
			$subarea_ids = implode(',',$subarea);
			$values.= ", `subarea_ids` = '".$subarea_ids."'";
		}
		if($latitude != '')
		{
			$values.= ", `latitude`='".fnEncodeString($latitude)."'";
		}
		if($longitude != '')
		{			
			$values.= ", `longitude`='".fnEncodeString($longitude)."'";
		}
		if($cmbSuperStockist != '')
		{
			$values.= ", `sstockist_id`= ".$cmbSuperStockist;
		}


        if ($user_type == 'Superstockist') 
		{
				$external_id =	1;
				$values.= ", `external_id` = '".$external_id."'";
		}
		if ($user_type == 'Accountant') 
		{
				$external_id =	1;
				$values.= ", `external_id` = '".$external_id."'";
		}
		if ($user_type == 'Distributor') 
		{
				$external_id =	$cmbSuperStockist;
				$values.= ", `external_id` = '".$external_id."'";
		}
		if ($user_type == 'DeliveryChannelPerson') 
		{
				$external_id =	$cmbSuperStockist;
				$values.= ", `external_id` = '".$external_id."'";
		}
/*
		if($user_type == 'SalesPerson' || $user_type == 'DeliveryPerson')
		{
			$external_id = $assign;
							if(count($assign) > 1)
							{
					$external_id = implode(',',$assign);
					$values.= ", `external_id` = '".$external_id."'";
				}else{
					$external_id = $assign[0];
				}
				if(gettype($_POST['assign']) == string)
					$external_id = $assign;
				    //$values.= ", `external_id`= ".$external_id;
			
		}	*/

			$stockist_external_id = $assign;
			if($user_type == 'SalesPerson' || $user_type == 'DeliveryPerson' ||$user_type == 'Shopkeeper')
			{
				if(count($assign) > 1)
				{
					$stockist_external_id = implode(',',$assign);
					$stockist_external_id1 = implode(',',$assign);
				}else
				{
					$stockist_external_id = $assign[0];
					$stockist_external_id1 = $assign[0];
				}
				if(gettype($_POST['assign']) == string)
					$stockist_external_id = $assign;
				    $stockist_external_id1 = $assign;
			}

		if($stockist_external_id != '')
		{
			$values.= ", `stockist_id` = '".$stockist_external_id."'";
		}
		if($stockist_external_id1 != '')
		{
			$values.= ", `external_id` = '".$stockist_external_id."'";
		}
		//if($_SESSION[SESSION_PREFIX.'user_type'] == 'Superstockist' OR $_SESSION[SESSION_PREFIX.'user_type'] == 'Distributor')
		//{
			if($firstname!="") {
				$values.= ", `firstname`= '".fnEncodeString(trim($firstname))."'";	
			}
			if($username!="") {
				$values.= ", `username`= '".fnEncodeString($username)."'";					
			}
			
		//}
		if($_SESSION[SESSION_PREFIX.'user_type'] == 'Admin'){
			if($cod_percent!="") {
				$update_cod_sql = "update  tbl_cod_admin SET status='inactive'";
				mysqli_query($this->local_connection,$update_cod_sql);
				
				$insert_cod_sql = "insert  tbl_cod_admin SET cod_percent='$cod_percent',status='active'";
				mysqli_query($this->local_connection,$insert_cod_sql);
			}
		}	
			//assignshopids
		if($user_type == 'Shopkeeper')
		{			
			if(count($assignshopids>0)){
				$remove_assigned_sql = "update  tbl_shops SET shop_owner_id=0 where shop_owner_id='$user_id'";
				mysqli_query($this->local_connection,$remove_assigned_sql);				
				foreach($assignshopids as $key=>$shopid){
					$shopkeeper_shops_sql = "UPDATE tbl_shops 
					SET shop_owner_id='$user_id' 
					WHERE id='$shopid'";			
					mysqli_query($this->local_connection,$shopkeeper_shops_sql);
				}
			}
		}
      $update_user_sql = "UPDATE tbl_user SET $values WHERE id='$user_id'";
	  // exit();
		mysqli_query($this->local_connection,$update_user_sql);
		$this->commonObj->log_update_record('tbl_user',$user_id,$update_user_sql);
	}
	
	public function updateCommonUserDetails($common_user_id) {		
		extract ($_POST);	
		if($firstname != '')
		{
			$values.= "`firstname`= '".fnEncodeString(trim($firstname))."'";
		}
		if($username != '')
		{
			$values.= ", `username`= '".fnEncodeString($username)."'";
		}
		if($email != '')
		{
			$values.= ", `emailaddress`= '".fnEncodeString($email)."'";
		}
		if($mobile != '')
		{
			$values.= ", `mobile`= '".$mobile."'";				
		}
		if($address != '')
		{
			$values.= ", `address`= '".fnEncodeString($address)."'";
		}
		if($state != '')
		{
			$values.= ", `state`= '".$state."'";
		}
		if($city != '')
		{	
			$values.= ",`city`= '".$city."'";
		}

		$update_user_sql = "UPDATE tbl_users SET $values WHERE uid='$common_user_id'";
		mysqli_query($this->common_connection,$update_user_sql);		
	}
	
	public function updateLocalUserWorkingAreaDetails($user_id) {
		extract ($_POST);		
		$values = ''; 
		if($state != '')
		{
			$values.= ", `state_ids`= '".$state."'";
		}
		if($city != '')
		{	
			$values.= ",`city_ids`= '".$city."'";
		}
		if($area != '')
		{
			$suburb_ids = implode(',',$area);
			$values.= ", `suburb_ids`= '".$suburb_ids."'";
		}else{
			$values.= ", `suburb_ids`= ''";
		}
		if($subarea != '')
		{			
			$subarea_ids = implode(',',$subarea);
			$values.= ",`subarea_ids`= '".$subarea_ids."'";
		}else{
			$values.= ",`subarea_ids`= ''";
		}
			
		$user_working_area = "UPDATE tbl_user_working_area SET user_id='$user_id' $values WHERE user_id='$user_id'";				
			
		mysqli_query($this->local_connection,$user_working_area);
		$this->commonObj->log_update_record('tbl_user_working_area',$user_id, $user_working_area);
	}
	function checkUserBelongTo($user_id,$external_id){
		if($external_id == $_SESSION[SESSION_PREFIX.'user_id'])
			return 0;
		else{
			$user_data = $this->getLocalUserDetails($external_id);
			if($user_data['external_id'] == $_SESSION[SESSION_PREFIX.'user_id'])
				return 0;
			else
				return 1;
		}
	}
	public function assignStockist() {	
		extract ($_POST);	
		$external_id = $assign;			
		if(count($assign) > 1){
			$external_id = implode(',',$assign);
		}else{
			$external_id = $assign[0];
		}				
		
		$values.= " `external_id`= '".$external_id."'";
		$update_user_sql = "UPDATE tbl_user SET $values WHERE id='$sales_p_id'";
		return mysqli_query($this->local_connection,$update_user_sql);
		$this->commonObj->log_update_record('tbl_user',$sales_p_id,$update_user_sql);
	}
	public function getDefaultUserDetails($user_type){
		$sql1="SELECT tbl_user_working_area.state_ids, tbl_user_working_area.city_ids, 
		tbl_user_working_area.suburb_ids, tbl_user_working_area.subarea_ids
		FROM tbl_user 
		LEFT JOIN  tbl_user_working_area ON  tbl_user_working_area.user_id = tbl_user.id
		WHERE user_type = '".$user_type."'  AND is_default_user = '1'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return mysqli_fetch_assoc($result1);		
		}else
			return $row_count;	
	}
	public function migration_sp_sstockist(){
		echo $sql1="SELECT `id`, `external_id`, `sstockist_id`
		FROM tbl_user where user_type = 'SalesPerson'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			while($row_sp = mysqli_fetch_array($result1))
			{//print"<pre>";print_R($row_sp);
				if($row_sp['sstockist_id'] == 0){
					echo "<br>#id==".$row_sp['id']."#external_id".$row_sp['external_id'];
					$stockist_array = explode(',',$row_sp['external_id']);
					$first_stockist = $stockist_array[0];
					
					echo "<br>".$sql2="SELECT `external_id`
					FROM tbl_user where id = ".$first_stockist;
					$result2 = mysqli_query($this->local_connection,$sql2);
					$row_count2 = mysqli_num_rows($result2);
					$sstockist_array = mysqli_fetch_assoc($result2);	
					
					echo "<br>".$sql3="UPDATE tbl_user SET `sstockist_id` = ".$sstockist_array['external_id']."
					where id = ".$row_sp['id'];
					$result3 = mysqli_query($this->local_connection,$sql3);
				}
			}	
		}else
			return $row_count;	
	}
	//Folowing methods for delete supply chain users,shop,area,subarea 

	public function deleteSuperstockistById($user_id,$user_id1){

		/*echo $user_id."--".$user_id1;
		exit();*/
		$tbl_user = "UPDATE  tbl_user SET isdeleted='1' WHERE id='$user_id'";
		mysqli_query($this->local_connection,$tbl_user);
		
		$sql1="SELECT `common_user_id`	FROM tbl_user where id = '".$user_id."'";
		$result11 = mysqli_query($this->local_connection,$sql1);
		$row11 = mysqli_fetch_assoc($result11);
		$common_user_id = $row11['common_user_id'];		
		$tbl_users = "UPDATE  tbl_users SET isdeleted='1' WHERE uid='$common_user_id'";
		mysqli_query($this->common_connection,$tbl_users);

          $tbl_user_new_parent2 = "UPDATE tbl_user SET external_id='$user_id1' WHERE external_id='$user_id'";
			mysqli_query($this->local_connection,$tbl_user_new_parent2);

		$tbl_find = "SELECT id,external_id,parent_ids FROM tbl_user where FIND_IN_SET('$user_id', parent_ids)";
        $result = mysqli_query($this->local_connection,$tbl_find);
        while ($row = mysqli_fetch_array($result)) {
        	$id = $row['id'];
        	$external_id = $row['external_id'];
        	$parent_ids = $row['parent_ids'];
        	$parent_ids1 = explode(',', $parent_ids);
        	//$last_parent_id = end($parent_ids1);
        	$new_parent_ids = str_replace($user_id, $user_id1, $parent_ids1);
        	$new_parent_ids_str = implode(',', $new_parent_ids);
        	 // print_r($parent_ids1);
		   // print_r($new_parent_ids);
        	//echo $new_parent_ids_str;
			 $tbl_user_new_parent = "UPDATE tbl_user SET parent_ids='$new_parent_ids_str' WHERE id='$id'";
			mysqli_query($this->local_connection,$tbl_user_new_parent);				  
        }
		//exit();
	}
	public function deleteStockistById($user_id,$user_id1){
		
		//echo $user_id."--".$user_id1;
		//exit();
		$tbl_user = "UPDATE  tbl_user SET isdeleted='1' WHERE id='$user_id'";
		mysqli_query($this->local_connection,$tbl_user);
		
		$sql1="SELECT `common_user_id`	FROM tbl_user where id = '".$user_id."'";
		$result11 = mysqli_query($this->local_connection,$sql1);
		$row11 = mysqli_fetch_assoc($result11);
		$common_user_id = $row11['common_user_id'];		
		$tbl_users = "UPDATE  tbl_users SET isdeleted='1' WHERE uid='$common_user_id'";
		mysqli_query($this->common_connection,$tbl_users);


		 $tbl_user_new_parent2 = "UPDATE tbl_user SET external_id='$user_id1' WHERE external_id='$user_id'";
		 mysqli_query($this->local_connection,$tbl_user_new_parent2);

		$tbl_find = "SELECT id,external_id,parent_ids FROM tbl_user where FIND_IN_SET('$user_id', parent_ids)";
        $result = mysqli_query($this->local_connection,$tbl_find);
        while ($row = mysqli_fetch_array($result)) {
        	$id = $row['id'];
        	$external_id = $row['external_id'];
        	$parent_ids = $row['parent_ids'];
        	$parent_ids1 = explode(',', $parent_ids);
        	$new_parent_ids = str_replace($user_id, $user_id1, $parent_ids1);
        	$new_parent_ids_str = implode(',', $new_parent_ids);
        	 // print_r($parent_ids1);
		   // print_r($new_parent_ids);
        	//echo $new_parent_ids_str;
			$tbl_user_new_parent = "UPDATE tbl_user SET parent_ids='$new_parent_ids_str' WHERE id='$id'";
			mysqli_query($this->local_connection,$tbl_user_new_parent);		  
        }
		//exit();
		
	}
	public function deleteSalespersonById($user_id){
		$user_data = $this->getLocalUserDetails($user_id);
		$external_id=$user_data['external_id'];
		$tbl_user = "UPDATE  tbl_user SET isdeleted='1' WHERE id='$user_id'";
		mysqli_query($this->local_connection,$tbl_user);

		$sql1="SELECT `common_user_id`	FROM tbl_user where id = '".$user_id."'";
		$result11 = mysqli_query($this->local_connection,$sql1);
		$row11 = mysqli_fetch_assoc($result11);
		$common_user_id = $row11['common_user_id'];		
		$tbl_users = "UPDATE  tbl_users SET isdeleted='1' WHERE uid='$common_user_id'";
		mysqli_query($this->common_connection,$tbl_users);

		$tbl_shops = "UPDATE  tbl_shops SET shop_added_by='$external_id' WHERE shop_added_by='$user_id'";
		mysqli_query($this->local_connection,$tbl_shops);		
	}
    public function deleteDeliveryPersonById($user_id){
		$user_data = $this->getLocalUserDetails($user_id);
		$external_id=$user_data['external_id'];
		$tbl_user = "UPDATE  tbl_user SET isdeleted='1' WHERE id='$user_id'";
		mysqli_query($this->local_connection,$tbl_user);

		$sql1="SELECT `common_user_id`	FROM tbl_user where id = '".$user_id."'";
		$result11 = mysqli_query($this->local_connection,$sql1);
		$row11 = mysqli_fetch_assoc($result11);
		$common_user_id = $row11['common_user_id'];		
		$tbl_users = "UPDATE  tbl_users SET isdeleted='1' WHERE uid='$common_user_id'";
		mysqli_query($this->common_connection,$tbl_users);	
	}

	public function deleteUserType($id){
		$tbl_userlevel = "UPDATE  tbl_user_tree SET isdeleted='1'   WHERE id='$id'";
		mysqli_query($this->local_connection,$tbl_userlevel);		
	}
	
	
	public function getAllLocalUser($user_type){
		$where="";
		switch($_SESSION[SESSION_PREFIX.'user_type']){
		case "Admin":				
				$where.=" ";
			break;
			case "Superstockist":			
				$where.=" AND tbl_user.sstockist_id='".$_SESSION[SESSION_PREFIX."user_id"]."' ";
			break;
			case "Distributor":
				$where.=" AND tbl_user.external_id='".$_SESSION[SESSION_PREFIX."user_id"]."' ";				
			break;
		}
		 $sql1="SELECT `id`, `firstname`, `username`, 
		`pwd`, `user_type`, `address`, `city`, `state`, `mobile`, `email`, 
		`reset_key`, `is_default_user`
		FROM tbl_user where user_type = '".$user_type."' 
		$where
		order by is_default_user desc, firstname asc";//, `user_status`
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}
	public function getAllSP($user_role){
		$where="";
		switch($_SESSION[SESSION_PREFIX.'user_role']){
		case "Admin":				
				$where.=" ";
			break;
			case "Superstockist":			
				$where.=" AND find_in_set('".$_SESSION[SESSION_PREFIX."user_id"]."',parent_ids) <> 0 ";
			break;
			case "Distributor":
				$where.=" AND find_in_set('".$_SESSION[SESSION_PREFIX."user_id"]."',parent_ids) <> 0 ";				
			break;
		}
		$sql1="SELECT `id`, `firstname`, `username`, 
		`pwd`, `user_role`, `address`, `city`, `state`, `mobile`, `email`, 
		`reset_key`, `is_default_user`
		FROM tbl_user where user_role = '".$user_role."' 
		$where AND isdeleted!=1
		order by is_default_user desc, firstname asc";//, `user_status`
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}
	public function getAllLocalUserNew($user_role){
		$where="";
		$where.=" AND tbl_user.external_id='".$_SESSION[SESSION_PREFIX."user_id"]."' ";
		
		 $sql1="SELECT `id`, `firstname`, `username`, 
		`pwd`, `user_type`, `address`, `city`, `state`, `mobile`, `email`, 
		`reset_key`, `is_default_user`
		FROM tbl_user where user_role = '".$user_role."' 
		$where
		order by is_default_user desc, firstname asc";//, `user_status`
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}

	public function updateSPattendance($frmdate1,$dropdownSalesPerson1,$google_distance1) 
	{		
		 $user_other_details = "UPDATE tbl_sp_attendance SET todays_travelled_distance='$google_distance1' WHERE `sp_id`='$dropdownSalesPerson1' AND (date_format(tdate, '%Y-%m-%d'))='$frmdate1'";			
		 mysqli_query($this->local_connection,$user_other_details);
		$this->commonObj->log_update_record('tbl_sp_attendance',$dropdownSalesPerson1, $user_other_details);
	}

    
	public function getAllLocalUserLevels(){		
		 $sql1="SELECT distinct(`user_role`) as user_type,`id`, `level` ,`usertype_margin`
		FROM tbl_user_tree group by user_role  order by id  asc";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}	
	
	public function getshop_assigned_idsfor_shopkeeper($shopkeeper_id){		
		$assigned_shop_sql="SELECT id FROM tbl_shops where shop_owner_id='".$shopkeeper_id."' order by name";
		$assigned_shop_result = mysqli_query($this->local_connection,$assigned_shop_sql);
		$row_count = mysqli_num_rows($assigned_shop_result);
		if($row_count > 0){	
			$assigned_shop_array=array();
			while($assigned_row = mysqli_fetch_assoc($assigned_shop_result)){
				$assigned_shop_array[]=$assigned_row['id'];
			}
			return $assigned_shop_array;		
		}else
			return $row_count;	
	}
	public function getshop_update_shopkeeper($shopkeeper_id){		
		$assigned_shop_sql="SELECT id,name FROM tbl_shops where shop_owner_id=0 or shop_owner_id='".$shopkeeper_id."' order by name";
		$assigned_shop_result = mysqli_query($this->local_connection,$assigned_shop_sql);
		$row_count = mysqli_num_rows($assigned_shop_result);
		if($row_count > 0){	
			$assigned_shop_array=array();
			$counter=0;
			while($assigned_row = mysqli_fetch_assoc($assigned_shop_result)){
				$assigned_shop_array[$counter]['id']=$assigned_row['id'];
				$assigned_shop_array[$counter]['name']=$assigned_row['name'];
				$counter++;
			}
			return $assigned_shop_array;		
		}else
			return $row_count;	
	}
	public function saveRoutePoints($points) {
		$i=0;	
		$t=time();$str_route="Route".$t."";
		$insert_route_sql = "INSERT INTO tbl_route_by_map(`name`) VALUES('$str_route') ";		
		mysqli_query($this->local_connection, $insert_route_sql);
		 $route_id=mysqli_insert_id($this->local_connection);
		foreach($points as $key=>$value){
			$latt1=$value['latt1'];
			$longg1=$value['longg1'];
			$address1=$value['address1'];
			$insert_sql = "INSERT INTO tbl_route_details_by_map(`route_id`, `address`, `lattitude`, `longitude`)
			 VALUES('$route_id', '$address1','$latt1','$longg1') ";		
			mysqli_query($this->local_connection, $insert_sql);
			$i++;
		}
		return $i;	
    }
	function getLeadDetails($lead_detail_id){		 
	     $lead_detail_sql="SELECT `id`, `shop_id`, `reminder`, `visited_date`, `notification`,
		 `lead_status`, `created_on`, `lead_term_ids`, `start_order_date`, `lead_terms_ans`
		 FROM tbl_lead_details where id ='".$lead_detail_id."'"; 
	     $lead_det_result = mysqli_query($this->local_connection, $lead_detail_sql);
		 $count_row=mysqli_num_rows($lead_det_result);
		 if($count_row>0){return $row = mysqli_fetch_assoc($lead_det_result);}
         else{ return $count_row;}
	}
	function getLeadQuestions(){		 
		$lead_detail_sql="SELECT `id`, `quetions`, `q_type` FROM tbl_lead_terms "; 
		$lead_det_result = mysqli_query($this->local_connection, $lead_detail_sql);
		$count_row=mysqli_num_rows($lead_det_result);
		$array=array();
		if($count_row>0){
			 while($row = mysqli_fetch_assoc($lead_det_result)){
				$array[]=$row;
			 }return $array;
		}else{
			return $count_row;
		}
	}
	

}
?>