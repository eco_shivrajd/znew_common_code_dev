
function CriteriaSection(section_type) {

	//alert(section_type);
	if(section_type == "free_product"){
		$("#enabled_div").val("free_product_div");
		$("#discount_div").hide();
		$("#by_weight_div").hide();
		$("#free_product_div").show();		
		$("#free_product_div :input").attr("disabled", false);	
		$("#discount_div :input").attr("disabled", true);
		$("#by_weight_div :input").attr("disabled", true);			
		$("[id^='campaign_product_price_']").removeAttr("data-parsley-required");
		$("[id^='campaign_product_discount_']").removeAttr("data-parsley-required");
		$("[id^='campaign_product_price_']").removeAttr("data-parsley-required-message");
		$("[id^='campaign_product_discount_']").removeAttr("data-parsley-required-message"); 
		$("[id^='campaign_product_price_']").removeAttr("data-parsley-error-message");
		$("[id^='campaign_product_discount_']").removeAttr("data-parsley-error-message"); 
		
		$("[id^='campaign_product_wt_qty_']").removeAttr("data-parsley-errors-container");	
		$("[id^='campaign_product_wt_qty_']").removeAttr("data-parsley-required");
		$("[id^='campaign_product_wt_qty_']").removeAttr("data-parsley-pattern");
		$("[id^='campaign_product_wt_qty_']").removeAttr("data-parsley-required-message");
		$("[id^='campaign_product_wt_qty_']").removeAttr("data-parsley-error-message");		
	} 
}					
function fnAddDisc(){
	$("[id^='remove_discount_']").show();
	var total_elements = $("#total_element").val();
	var current_total_elements = parseInt(total_elements) + 1;
	
	var outer_div_open = '<div class="discdet" id="discount_'+current_total_elements+'">';
	var outer_div_close = '<hr/></div>';
	var price_input = '<input type="text" name="campaign_product_price_'+current_total_elements+'" id="campaign_product_price_'+current_total_elements+'" data-parsley-trigger="change" data-parsley-pattern="[0-9]*(\.?[0-9]{1,2}$)?" data-parsley-error-message="Please enter Price (positive numeric, Decimal 2 value)" class="form-control" onchange="calculate_discount('+current_total_elements+')">';
	var disccont_input = '<input type="text" name="campaign_product_discount_'+current_total_elements+'" id="campaign_product_discount_'+current_total_elements+'" data-parsley-trigger="change" data-parsley-pattern="[0-9]*(\.?[0-9]{1,2}$)?" data-parsley-error-message="Please enter Discount (positive numeric, Decimal 2 value)" class="form-control minsz" onchange="calculate_discount('+current_total_elements+')">';
	var inner_div1_open = '<div class="form-group"><label class="col-md-3">Discount Details:<span class="mandatory">*</span></label><div class="col-md-6 nopadl"><div class="col-md-4">'+price_input+'</div><div class="col-md-1" id="nopad1"><label class="nopadl" style="padding-top:5px">Price</label></div><div class="col-md-2" id="camdis">'+disccont_input+' </div><div class="col-md-1" id="nopad1"><label class="nopadl" style="padding-top:5px; ">%</label></div><div class="col-md-1"><button class="btn btn-primary btn-md" type="button" id="remove_discount_'+current_total_elements+'" name="remove_discount_'+current_total_elements+'" onclick="fnRemoveDiscountSection('+current_total_elements+')" title="Remove Details"><i class="fa fa-times"></i></button></div></div>';
	var inner_div1_close = '</div>';
	//var inner_div2_open = '<div class="form-group"><label class="col-md-3"> </label><div class="col-md-6 nopadl"><label class="col-md-4 ">Discounted Amount:</label><label class="col-md-2" id="discount_value_'+current_total_elements+'"></label></div>';
	//var inner_div2_close = '</div>';
	var append_section = outer_div_open+inner_div1_open+inner_div1_close+outer_div_close;
	$("#disccont").append(append_section);
	$("#total_element").val(current_total_elements);
	$('#campaign_product_price_'+current_total_elements).parsley(); 
	$('#campaign_product_discount_'+current_total_elements).parsley(); 	
}
function fnRemoveDiscountSection(element_id){
	var total_elements = $("#total_element").val();
	if(total_elements > 1){
		$("[id^='remove_discount_']").show();
		var current_total_elements = parseInt(total_elements) - 1;
		$("#total_element").val(current_total_elements);
		$("#discount_"+element_id).remove();
	
		for(var i=element_id; i<= current_total_elements; i++){
			var current_id = parseInt(i)+1;
			//outer_div
			$("#discount_"+current_id).attr("id","discount_"+i);			
			//price_input
			$("#campaign_product_price_"+current_id).attr("id","campaign_product_price_"+i);		
			$("#campaign_product_price_"+i).attr("name","campaign_product_price_"+i);
			$("#campaign_product_price_"+i).attr("onchange","calculate_discount("+i+")");
			//disccont_input
			$("#campaign_product_discount_"+current_id).attr("id","campaign_product_discount_"+i);		
			$("#campaign_product_discount_"+i).attr("name","campaign_product_discount_"+i);	
			$("#campaign_product_discount_"+i).attr("onchange","calculate_discount("+i+")");	
			//Discounted Amount label
			$("#discount_value_"+current_id).attr("id","discount_value_"+i);		
			$("#discount_value_"+i).attr("name","discount_value_"+i);	
			//Remove button		
			$("#remove_discount_"+current_id).attr("id","remove_discount_"+i);
			$("#remove_discount_"+i).attr("name","remove_discount_"+i);				
			$("#remove_discount_"+i).attr("onclick","fnRemoveDiscountSection("+i+")");	
		}
		if(current_total_elements == 1){
			$("[id^='remove_discount_']").hide();
		}
	}
}
	
function setSelectNoValue(div,select_element){
	var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled>-Select-</option></select>';
	$("#"+div).html(select_selement_section);
}
function fnShowBrand(type,section_id,call) {
	var enabled_div = $("#enabled_div").val();	
	var inner_div = " > div > div > div > div > div > ";
	if(type == 'c')
	{				
		var div_id = enabled_div+inner_div+"#div_state_p_"+section_id;
		var select_name_id = "state_p_"+section_id;
		if(call != 'add_more')
		{
			if(enabled_div != 'by_weight_div'){
				setSelectNoValue(enabled_div+inner_div+"#div_city_p_"+section_id, "city");
				setSelectNoValue(enabled_div+inner_div+"#div_area_p_"+section_id, "area");
				setSelectNoValue(enabled_div+inner_div+"#div_subarea_p_"+section_id, "subarea");
				/*set_select_blank(enabled_div+inner_div+'#city','p',section_id,'fnShowArea');
				set_select_blank(enabled_div+inner_div+'#area','p',section_id,'fnShowSubArea');
				set_select_blank(enabled_div+inner_div+'#subarea','p',section_id,'');*/
			}
		}
	}
	
	var url = "getStateDropDown1.php?select_name_id="+select_name_id+"&function_param1="+type+"&function_param2="+section_id;
	CallAJAX(url,div_id);
} 
function fnShowCity(id,type,section_id) {
	var enabled_div = $("#enabled_div").val();
	var inner_div = " > div > div > div > div > div > ";
	if(type == 'c')
	{
		var div_id = enabled_div+inner_div+"#div_city_p_"+section_id;
		var select_name_id = "city_p_"+section_id;
		if(enabled_div != 'by_weight_div'){			
			/*set_select_blank(enabled_div+inner_div+'#area','p',section_id,'fnShowSubArea');
			set_select_blank(enabled_div+inner_div+'#subarea','p',section_id,'');*/
			setSelectNoValue(enabled_div+inner_div+"#div_area_p_"+section_id, "area");
			setSelectNoValue(enabled_div+inner_div+"#div_subarea_p_"+section_id, "subarea");
		}
	}
	
	var url = "getCityDropDown1.php?state_id="+id.value+"&select_name_id="+select_name_id+"&function_param1="+type+"&function_param2="+section_id;
	CallAJAX(url,div_id);
} 
function fnShowArea (id,type,section_id) {
	var enabled_div = $("#enabled_div").val();
	var inner_div = " > div > div > div > div > div > ";
	if(type == 'c')
	{
		var div_id = enabled_div+inner_div+"#div_area_p_"+section_id;
		var select_name_id = "area_p_"+section_id;			
		if(enabled_div != 'by_weight_div'){
			//set_select_blank(enabled_div+inner_div+'#subarea','p',section_id,'');		
			setSelectNoValue(enabled_div+inner_div+"#div_subarea_p_"+section_id, "subarea");
		}
	}
	
	var url = "getAreaDropdown1.php?area_id="+id.value+"&select_name_id="+select_name_id+"&function_param1="+type+"&function_param2="+section_id;	
	CallAJAX(url,div_id);	
}

function fnShowSubArea (id,type,section_id) {
	var select_option = "";
	var enabled_div = $("#enabled_div").val();
	var inner_div = " > div > div > div > div > div > ";
	if(type == 'c')
	{
		var div_id = enabled_div+inner_div+"#div_subarea_p_"+section_id;
		var select_name_id = "subarea_p_"+section_id;
		select_option = "&function_name=select_all_or_none&multiple=multiple";
	}
	var url = "getSubAreaDropdown1.php?area_id="+id.value+"&select_name_id="+select_name_id+"&function_param1="+type+"&function_param2="+section_id+select_option;
	CallAJAX(url,div_id);
}
function set_select_blank(element,type,section,function_name){
	if(function_name != '')
		var html = '<select name="'+element+'_p_'+section+'" id="'+element+'_'+type+'_'+section+'" class="form-control" onchange="'+function_name+'(this,'+type+','+section+')">';
	else
		var html = '<select name="'+element+'_p_'+section+'" id="'+element+'_'+type+'_'+section+'" class="form-control">';
	
	html = html + '<option value="">-Select-</option></select>';
	var assign_element_div = 'div_'+element+'_p_'+section;
	document.getElementById(""+assign_element_div+"").innerHTML	=	html;
}
function select_html(element,type,section,function_name){
	if(function_name != '')
		var html = '<select name="'+element+'_p_'+section+'" id="'+element+'_p_'+section+'" class="form-control" onchange="'+function_name+'(this,\''+type+'\','+section+')">';
	else
		var html = '<select name="'+element+'_p_'+section+'" id="'+element+'_p_'+section+'" class="form-control">';
	
	html = html + '<option value="">-Select-</option></select>';
	
	return html;
}
function select_all_or_none(obj){	
	var id = obj.id;
	var value = obj.value;
	if(value == 'select_all')
		$("#"+id+" option").prop("selected",true);
	else{
		$("#"+id+" option[value='select_all']").prop("selected",false);
	}
}
function fnAddFree(){
	$("[id^='remove_free_product_']").show();
	
	var total_elements = $("#total_element_free").val();	
	var current_total_elements = parseInt(total_elements) + 1;
	
	var outer_div_open = '<div class="free" id="free_pro_'+current_total_elements+'"><div class="row">';
	var outer_div_close = '</div><hr/></div>';
	var section_campaign_open = '<div class="col-sm-6"><h4 style="margin-top:30px;"><b>Add Route</b>&nbsp;&nbsp;</h4><hr />';
	var section_campaign_close = '</div>';
	
	var state_section_open = '<div class="form-group" id="brand_div"><label class="col-md-6">State:<span class="mandatory">*</span></label><div class="col-md-5" id="div_state_p_'+current_total_elements+'">';
	var state_section = select_html('state','c',current_total_elements,'fnShowCity');
	var state_section_close = '</div></div>';
	var state_complete = state_section_open+state_section+state_section_close;
	
	var city_section_open =  '<div class="form-group" id="category_div"><label class="col-md-6">City:<span class="mandatory">*</span></label><div class="col-md-5" id="div_city_p_'+current_total_elements+'">';
	var city_section = select_html('city','c',current_total_elements,'fnShowArea');
	var city_section_close = '</div></div>';
	var city_complete = city_section_open+city_section+city_section_close;
	
	var area_section_open =  '<div class="form-group" id="product_div"><label class="col-md-6">Area:<span class="mandatory">*</span></label><div class="col-md-5" id="div_area_p_'+current_total_elements+'">';
	var area_section = select_html('area','c',current_total_elements,'fnShowSubArea');
	var area_section_close = '</div></div>';
	var area_complete = area_section_open+area_section+area_section_close;
	
	var subarea_section_open =  '<div class="form-group" id="variant_div"><label class="col-md-6">Subarea:<span class="mandatory">*</span></label><div class="col-md-5" id="div_subarea_p_'+current_total_elements+'">';
	var subarea_section = select_html('subarea','c',current_total_elements,'');
	var subarea_section_close =  '</div></div>';
	var subarea_complete = subarea_section_open+subarea_section+subarea_section_close;
	
	var brand_all_div = '<div class="form-group" id="brand_all_msg" style="display:none;"><label class="col-md-6">This campaign is applicable to all Brands.</label></div>';
	var section_campaign_complete = section_campaign_open+state_complete+city_complete+area_complete+subarea_complete+brand_all_div+section_campaign_close;
	var section_free_open = '<div class="col-sm-5"><h4 style="margin-top:30px;"><b>Free Product</b></h4><hr />';
	var section_free_close = '</div>';

	//var section_free_complete = section_free_open+brand_free_complete+category_free_complete+product_free_complete+variant_free_complete+section_free_close;
	var remove_button = '<div class="col-md-1"><button class="btn btn-primary btn-md" type="button" id="remove_free_product_'+current_total_elements+'" name="remove_free_product_'+current_total_elements+'" onclick="fnRemoveFreeProductSection('+current_total_elements+')" title="Remove Campaign & Free Product"><i class="fa fa-times"></i></button></div>';
	
	var append_section = outer_div_open+section_campaign_complete+remove_button+outer_div_close;
	$("#freecont").append(append_section);
	$("#free_pro_"+current_total_elements).loader('show');
	setTimeout(
	  function() 
	  {
		setTimeout(
		function() 
		{
			fnShowBrand('c',current_total_elements,'add_more');
		}, 2000);
			fnShowBrand('f',current_total_elements,'add_more');
			$("#free_pro_"+current_total_elements).loader('hide');
	  }, 2500);
	$("#select_all_brand_span").hide();	
	$("#total_element_free").val(current_total_elements);	
}
function fnRemoveFreeProductSection(element_id){
	$("[id^='remove_free_product_']").show();
	var total_elements = $("#total_element_free").val();
	if(total_elements > 1){
	var current_total_elements = parseInt(total_elements) - 1;
	$("#total_element_free").val(current_total_elements);
	$("#free_pro_"+element_id).remove();
	
		for(var i=element_id; i<= current_total_elements; i++){
			var current_id = parseInt(i)+1;
			//outer_div
			$("#free_pro_"+current_id).attr("id","free_pro_"+i);
			//Campaign section 
			$("#div_state_p_"+current_id).attr("id","div_state_p_"+i);
			$("#div_state_p_"+i).attr("name","div_state_p_"+i);
			
			$("#state_p_"+current_id).attr("id","state_p_"+i);
			$("#state_p_"+i).attr("name","state_p_"+i);
			$("#state_p_"+i).attr("onchange","fnShowCity(this+i)");//fnShowCity(this,'c',1)	
			
			$("#div_city_p_"+current_id).attr("id","div_city_p_"+i);
			$("#div_city_p_"+i).attr("name","div_city_p_"+i);
			$("#city_p_"+current_id).attr("id","city_p_"+i);
			$("#city_p_"+i).attr("name","city_p_"+i);
			$("#city_p_"+i).attr("onchange","fnShowArea(this,'c',"+i+")");//fnShowArea(this,'c',1)
			
			$("#div_area_p_"+current_id).attr("id","div_area_p_"+i);
			$("#div_area_p_"+i).attr("name","div_area_p_"+i);
			$("#area_p_"+current_id).attr("id","area_p_"+i);
			$("#area_p_"+i).attr("name","area_p_"+i);
			$("#area_p_"+i).attr("onchange","fnShowSubArea(this,'c',"+i+")");//fnShowSubArea(this,'c',1)
			
			$("#div_subarea_p_"+current_id).attr("id","div_subarea_p_"+i);
			$("#div_subarea_p_"+i).attr("name","div_subarea_p_"+i);
			$("#subarea_p_"+current_id).attr("id","subarea_p_"+i);
			$("#subarea_p_"+i).attr("name","subarea_p_"+i);
			
			//Free section			
			$("#div_brand_free_p_"+current_id).attr("id","div_brand_free_p_"+i);
			$("#div_brand_free_p_"+i).attr("name","div_brand_free_p_"+i);			
			$("#brand_free_p_"+current_id).attr("id","brand_free_p_"+i);
			$("#brand_free_p_"+i).attr("name","brand_free_p_"+i);
			$("#brand_free_p_"+i).attr("onchange","fnShowCity(this,'f',"+i+")");//fnShowCity(this,'f',1)
			
			$("#div_category_free_p_"+current_id).attr("id","div_category_free_p_"+i);
			$("#div_category_free_p_"+i).attr("name","div_category_free_p_"+i);
			$("#category_free_p_"+current_id).attr("id","category_free_p_"+i);
			$("#category_free_p_"+i).attr("name","category_free_p_"+i);
			$("#category_free_p_"+i).attr("onchange","fnShowArea(this,'f',"+i+")");//fnShowArea(this,'f',1)
			
			$("#div_product_free_p_"+current_id).attr("id","div_product_free_p_"+i);
			$("#div_product_free_p_"+i).attr("name","div_product_free_p_"+i);
			$("#product_free_p_"+current_id).attr("id","product_free_p_"+i);
			$("#product_free_p_"+i).attr("name","product_free_p_"+i);
			$("#product_free_p_"+i).attr("onchange","fnShowSubArea(this,'f',"+i+"))");//fnShowSubArea(this,'f',1)
			
			$("#div_variant_free_p_"+current_id).attr("id","div_variant_free_p_"+i);
			$("#div_variant_free_p_"+i).attr("name","div_variant_free_p_"+i);
			$("#variant_free_p_"+current_id).attr("id","variant_free_p_"+i);
			$("#variant_free_p_"+i).attr("name","variant_free_p_"+i);
						
			$("#remove_free_product_"+current_id).attr("id","remove_free_product_"+i);
			$("#remove_free_product_"+i).attr("name","remove_free_product_"+i);	
			$("#remove_free_product_"+i).attr("onclick","fnRemoveFreeProductSection("+i+")");
				
		}
		if(current_total_elements == 1){
			$("[id^='remove_free_product_']").hide();
			$("#select_all_brand_span").show();
		}
	}
}

function fnQuantitySection(section){
	if(section == "packet"){		
		document.getElementById("packet_div").style="display:block";
		document.getElementById("pcs_div").style="display:none";
		document.getElementById("weight_div").style="display:none";
	}else if(section == "pcs"){		
		document.getElementById("pcs_div").style="display:block";
		document.getElementById("weight_div").style="display:none";
		document.getElementById("packet_div").style="display:none";
	}else if(section == "weight"){
		document.getElementById("weight_div").style="display:block";
		document.getElementById("packet_div").style="display:none";
		document.getElementById("pcs_div").style="display:none";
	}
	
}
function fnDiscountSection(section){
	if(section == "pcs"){		
		document.getElementById("pcs_discount_div").style="display:block";
		document.getElementById("weight_discount_div").style="display:none";
	}else if(section == "weight"){
		document.getElementById("weight_discount_div").style="display:block";
		document.getElementById("pcs_discount_div").style="display:none";
	}
	
}





function fnSelectAllBrand(obj){
	if($(obj).is(':checked') == true)
	{		
		$("#brand_div").hide();
		$("#category_div").hide();
		$("#product_div").hide();
		$("#variant_div").hide();
		$("#btnAddFree").hide();	
		$("#brand_all_msg").show();	
	}else{		
		$("#brand_div").show();
		$("#category_div").show();
		$("#product_div").show();
		$("#variant_div").show();
		$("#btnAddFree").show();
		$("#brand_all_msg").hide();	
	}	
}