function CriteriaSection(section_type) {

	//alert(section_type);
	if(section_type == "free_product"){
		$("#enabled_div").val("free_product_div");
		$("#discount_div").hide();
		$("#by_weight_div").hide();
		$("#free_product_div").show();		
		$("#free_product_div :input").attr("disabled", false);	
		$("#discount_div :input").attr("disabled", true);
		$("#by_weight_div :input").attr("disabled", true);			
		$("[id^='campaign_product_price_']").removeAttr("data-parsley-required");
		$("[id^='campaign_product_discount_']").removeAttr("data-parsley-required");
		$("[id^='campaign_product_price_']").removeAttr("data-parsley-required-message");
		$("[id^='campaign_product_discount_']").removeAttr("data-parsley-required-message"); 
		$("[id^='campaign_product_price_']").removeAttr("data-parsley-error-message");
		$("[id^='campaign_product_discount_']").removeAttr("data-parsley-error-message"); 
		
		$("[id^='campaign_product_wt_qty_']").removeAttr("data-parsley-errors-container");	
		$("[id^='campaign_product_wt_qty_']").removeAttr("data-parsley-required");
		$("[id^='campaign_product_wt_qty_']").removeAttr("data-parsley-pattern");
		$("[id^='campaign_product_wt_qty_']").removeAttr("data-parsley-required-message");
		$("[id^='campaign_product_wt_qty_']").removeAttr("data-parsley-error-message");		
	} else if(section_type == "discount"){
		$("#enabled_div").val("discount_div");
		$("#by_weight_div").hide();
		$("#free_product_div").hide();
		$("#discount_div").show();
		$("#discount_div :input").attr("disabled", false);	
		$("#free_product_div :input").attr("disabled", true);	
		$("#by_weight_div :input").attr("disabled", true);	
		$("[id^='campaign_product_price_']").attr("data-parsley-required","#true");
		$("[id^='campaign_product_discount_']").attr("data-parsley-required","#true");
		$("[id^='campaign_product_price_']").attr("data-parsley-error-message","Please enter Price (positive numeric, Decimal 2 value)");
		$("[id^='campaign_product_discount_']").attr("data-parsley-error-message","Please enter Discount (positive numeric, Decimal 2 value)");
		$("[id^='campaign_product_price_']").attr("data-parsley-required-message","Please enter Price (positive numeric, Decimal 2 value)");
		$("[id^='campaign_product_discount_']").attr("data-parsley-required-message","Please enter Discount (positive numeric, Decimal 2 value)");	

		$("[id^='campaign_product_wt_qty_']").removeAttr("data-parsley-errors-container");	
		$("[id^='campaign_product_wt_qty_']").removeAttr("data-parsley-required");
		$("[id^='campaign_product_wt_qty_']").removeAttr("data-parsley-pattern");
		$("[id^='campaign_product_wt_qty_']").removeAttr("data-parsley-required-message");
		$("[id^='campaign_product_wt_qty_']").removeAttr("data-parsley-error-message");	
	} else if(section_type == "by_weight"){		
		$("#enabled_div").val("by_weight_div");
		$("#free_product_div").hide();
		$("#discount_div").hide();
		$("#by_weight_div").show();
		$("#by_weight_div :input").attr("disabled", false);	
		$("#discount_div :input").attr("disabled", true);	
		$("#free_product_div :input").attr("disabled", true);
		$("[id^='campaign_product_wt_qty_']").attr("data-parsley-errors-container",".nameError");	
		$("[id^='campaign_product_wt_qty_']").attr("data-parsley-required","#true");		
		$("[id^='campaign_product_wt_qty_']").attr("data-parsley-required-message","Please enter Weight (positive numeric, Decimal 2 value)");
		$("[id^='campaign_product_wt_qty_']").attr("data-parsley-error-message","Please enter Weight (positive numeric, Decimal 2 value)");		
	}
}					
function fnAddDisc(){
	$("[id^='remove_discount_']").show();
	var total_elements = $("#total_element").val();
	var current_total_elements = parseInt(total_elements) + 1;
	
	var outer_div_open = '<div class="discdet" id="discount_'+current_total_elements+'">';
	var outer_div_close = '<hr/></div>';
	var price_input = '<input type="text" name="campaign_product_price_'+current_total_elements+'" id="campaign_product_price_'+current_total_elements+'" data-parsley-trigger="change" data-parsley-pattern="[0-9]*(\.?[0-9]{1,2}$)?" data-parsley-error-message="Please enter Price (positive numeric, Decimal 2 value)" class="form-control" onchange="calculate_discount('+current_total_elements+')">';
	var disccont_input = '<input type="text" name="campaign_product_discount_'+current_total_elements+'" id="campaign_product_discount_'+current_total_elements+'" data-parsley-trigger="change" data-parsley-pattern="[0-9]*(\.?[0-9]{1,2}$)?" data-parsley-error-message="Please enter Discount (positive numeric, Decimal 2 value)" class="form-control minsz" onchange="calculate_discount('+current_total_elements+')">';
	var inner_div1_open = '<div class="form-group"><label class="col-md-3">Discount Details:<span class="mandatory">*</span></label><div class="col-md-6 nopadl"><div class="col-md-4">'+price_input+'</div><div class="col-md-1" id="nopad1"><label class="nopadl" style="padding-top:5px">Price</label></div><div class="col-md-2" id="camdis">'+disccont_input+' </div><div class="col-md-1" id="nopad1"><label class="nopadl" style="padding-top:5px; ">%</label></div><div class="col-md-1"><button class="btn btn-primary btn-md" type="button" id="remove_discount_'+current_total_elements+'" name="remove_discount_'+current_total_elements+'" onclick="fnRemoveDiscountSection('+current_total_elements+')" title="Remove Details"><i class="fa fa-times"></i></button></div></div>';
	var inner_div1_close = '</div>';
	//var inner_div2_open = '<div class="form-group"><label class="col-md-3"> </label><div class="col-md-6 nopadl"><label class="col-md-4 ">Discounted Amount:</label><label class="col-md-2" id="discount_value_'+current_total_elements+'"></label></div>';
	//var inner_div2_close = '</div>';
	var append_section = outer_div_open+inner_div1_open+inner_div1_close+outer_div_close;
	$("#disccont").append(append_section);
	$("#total_element").val(current_total_elements);
	$('#campaign_product_price_'+current_total_elements).parsley(); 
	$('#campaign_product_discount_'+current_total_elements).parsley(); 	
}
function fnRemoveDiscountSection(element_id){
	var total_elements = $("#total_element").val();
	if(total_elements > 1){
		$("[id^='remove_discount_']").show();
		var current_total_elements = parseInt(total_elements) - 1;
		$("#total_element").val(current_total_elements);
		$("#discount_"+element_id).remove();
	
		for(var i=element_id; i<= current_total_elements; i++){
			var current_id = parseInt(i)+1;
			//outer_div
			$("#discount_"+current_id).attr("id","discount_"+i);			
			//price_input
			$("#campaign_product_price_"+current_id).attr("id","campaign_product_price_"+i);		
			$("#campaign_product_price_"+i).attr("name","campaign_product_price_"+i);
			$("#campaign_product_price_"+i).attr("onchange","calculate_discount("+i+")");
			//disccont_input
			$("#campaign_product_discount_"+current_id).attr("id","campaign_product_discount_"+i);		
			$("#campaign_product_discount_"+i).attr("name","campaign_product_discount_"+i);	
			$("#campaign_product_discount_"+i).attr("onchange","calculate_discount("+i+")");	
			//Discounted Amount label
			$("#discount_value_"+current_id).attr("id","discount_value_"+i);		
			$("#discount_value_"+i).attr("name","discount_value_"+i);	
			//Remove button		
			$("#remove_discount_"+current_id).attr("id","remove_discount_"+i);
			$("#remove_discount_"+i).attr("name","remove_discount_"+i);				
			$("#remove_discount_"+i).attr("onclick","fnRemoveDiscountSection("+i+")");	
		}
		if(current_total_elements == 1){
			$("[id^='remove_discount_']").hide();
		}
	}
}
function calculate_discount(element_id)
{
	var campaign_product_price = document.getElementById("campaign_product_price_"+element_id).value;
	var campaign_product_discount = document.getElementById("campaign_product_discount_"+element_id).value;
	if(campaign_product_discount > 100){
		alert("Discount should not be greater than 100%");
		return false;
	}else{
		if(campaign_product_price != '' && campaign_product_discount != '')
		{
			var discount_value = ((campaign_product_price * campaign_product_discount)/100);
			//document.getElementById("discount_value").innerHTML = "(Discount Price: "+discount_value+")";;
			
			if(!isNaN(discount_value))
				$("#discount_value_"+element_id).html(discount_value);
		}else	
		{
			$("#discount_value_"+element_id).html('');
		}
	}
}	
function setSelectNoValue(div,select_element){
	var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled>-Select-</option></select>';
	$("#"+div).html(select_selement_section);
}
function fnShowBrand(type,section_id,call) {
	var enabled_div = $("#enabled_div").val();	
	var inner_div = " > div > div > div > div > div > ";
	if(type == 'c')
	{				
		var div_id = enabled_div+inner_div+"#div_brand_campaign_p_"+section_id;
		var select_name_id = "brand_campaign_p_"+section_id;
		if(call != 'add_more')
		{
			if(enabled_div != 'by_weight_div'){
				setSelectNoValue(enabled_div+inner_div+"#div_category_campaign_p_"+section_id, "category_campaign");
				setSelectNoValue(enabled_div+inner_div+"#div_product_campaign_p_"+section_id, "product_campaign");
				setSelectNoValue(enabled_div+inner_div+"#div_variant_campaign_p_"+section_id, "variant_campaign");
				/*set_select_blank(enabled_div+inner_div+'#category_campaign','p',section_id,'fnShowProductss');
				set_select_blank(enabled_div+inner_div+'#product_campaign','p',section_id,'fnShowProductVariant');
				set_select_blank(enabled_div+inner_div+'#variant_campaign','p',section_id,'');*/
			}
		}
	}
	else if(type == 'f')
	{
		var div_id = enabled_div+inner_div+"#div_brand_free_p_"+section_id;
		var select_name_id = "brand_free_p_"+section_id;
		if(call != 'add_more')
		{
			/*set_select_blank(enabled_div+inner_div+'#category_free','p',section_id,'fnShowProductss');
			set_select_blank(enabled_div+inner_div+'#product_free','p',section_id,'fnShowProductVariant');
			set_select_blank(enabled_div+inner_div+'#variant_free','p',section_id,'');*/
			setSelectNoValue(enabled_div+inner_div+"#div_category_free_p_"+section_id, "category_campaign");
			setSelectNoValue(enabled_div+inner_div+"#div_product_free_p_"+section_id, "product_campaign");
			setSelectNoValue(enabled_div+inner_div+"#div_variant_free_p_"+section_id, "variant_campaign");
		}
	}
	
	var url = "getBrandDropDown_campaign.php?select_name_id="+select_name_id+"&function_param1="+type+"&function_param2="+section_id;
	CallAJAX(url,div_id);
} 
function fnShowCategoriess(id,type,section_id) {
	//alert('dfsd');
	var enabled_div = $("#enabled_div").val();
	var inner_div = " > div > div > div > div > div > ";
	if(type == 'c')
	{
		var div_id = enabled_div+inner_div+"#div_category_campaign_p_"+section_id;
		var select_name_id = "category_campaign_p_"+section_id;
		if(enabled_div != 'by_weight_div'){			
			/*set_select_blank(enabled_div+inner_div+'#product_campaign','p',section_id,'fnShowProductVariant');
			set_select_blank(enabled_div+inner_div+'#variant_campaign','p',section_id,'');*/
			setSelectNoValue(enabled_div+inner_div+"#div_product_campaign_p_"+section_id, "product_campaign");
			setSelectNoValue(enabled_div+inner_div+"#div_variant_campaign_p_"+section_id, "variant_campaign");
		}
	}
	else if(type == 'f')
	{
		var div_id = enabled_div+inner_div+"#div_category_free_p_"+section_id;
		var select_name_id = "category_free_p_"+section_id;
		
			/*set_select_blank(enabled_div+inner_div+'#product_free','p',section_id,'fnShowProductVariant');
			set_select_blank(enabled_div+inner_div+'#variant_free','p',section_id,'');*/
			setSelectNoValue(enabled_div+inner_div+"#div_product_free_p_"+section_id, "product_campaign");
			setSelectNoValue(enabled_div+inner_div+"#div_variant_free_p_"+section_id, "variant_campaign");
		
	}
	
	var url = "getCategoryDropdown_campaign.php?brandid="+id.value+"&select_name_id="+select_name_id+"&function_param1="+type+"&function_param2="+section_id;
	CallAJAX(url,div_id);
} 
function fnShowProductss (id,type,section_id) {
	//alert('fnShowProductss');
	var enabled_div = $("#enabled_div").val();
	var inner_div = " > div > div > div > div > div > ";
	if(type == 'c')
	{
		var div_id = enabled_div+inner_div+"#div_product_campaign_p_"+section_id;
		var select_name_id = "product_campaign_p_"+section_id;			
		if(enabled_div != 'by_weight_div'){
			//set_select_blank(enabled_div+inner_div+'#variant_campaign','p',section_id,'');		
			setSelectNoValue(enabled_div+inner_div+"#div_variant_campaign_p_"+section_id, "variant_campaign");
		}
	}
	else if(type == 'f')
	{
		var div_id = enabled_div+inner_div+"#div_product_free_p_"+section_id;
		var select_name_id = "product_free_p_"+section_id;		
		//set_select_blank(enabled_div+inner_div+'#variant_free','p',section_id,'');
		setSelectNoValue(enabled_div+inner_div+"#div_variant_free_p_"+section_id, "variant_campaign");
	}
	
	var url = "getProductDropdown_campaign.php?cat_id="+id.value+"&select_name_id="+select_name_id+"&function_param1="+type+"&function_param2="+section_id;	
	CallAJAX(url,div_id);	
}

function fnShowProductVariant (id,type,section_id) {
	var select_option = "";
	var enabled_div = $("#enabled_div").val();
	var inner_div = " > div > div > div > div > div > ";
	if(type == 'c')
	{
		var div_id = enabled_div+inner_div+"#div_variant_campaign_p_"+section_id;
		var select_name_id = "variant_campaign_p_"+section_id;
		select_option = "&function_name=select_all_or_none&multiple=multiple";
	}
	else if(type == 'f')
	{
		var div_id = enabled_div+inner_div+"#div_variant_free_p_"+section_id;
		var select_name_id = "variant_free_p_"+section_id;
	}
	var url = "getProductVariantDropdown.php?cat_id="+id.value+"&select_name_id="+select_name_id+"&function_param1="+type+"&function_param2="+section_id+select_option;
	CallAJAX(url,div_id);
}
function set_select_blank(element,type,section,function_name){
	if(function_name != '')
		var html = '<select name="'+element+'_p_'+section+'" id="'+element+'_'+type+'_'+section+'" class="form-control" onchange="'+function_name+'(this,'+type+','+section+')">';
	else
		var html = '<select name="'+element+'_p_'+section+'" id="'+element+'_'+type+'_'+section+'" class="form-control">';
	
	html = html + '<option value="">-Select-</option></select>';
	var assign_element_div = 'div_'+element+'_p_'+section;
	document.getElementById(""+assign_element_div+"").innerHTML	=	html;
}
function select_html(element,type,section,function_name){
	if(function_name != '')
		var html = '<select name="'+element+'_p_'+section+'" id="'+element+'_p_'+section+'" class="form-control" onchange="'+function_name+'(this,\''+type+'\','+section+')">';
	else
		var html = '<select name="'+element+'_p_'+section+'" id="'+element+'_p_'+section+'" class="form-control">';
	
	html = html + '<option value="">-Select-</option></select>';
	
	return html;
}
function select_html_for_quantity(element,type,section,function_name){
	if(function_name != '')
		var html = '<select name="'+element+'_p_'+section+'" id="'+element+'_p_'+section+'" class="form-control" onchange="'+function_name+'(this,\''+type+'\','+section+')">';
	else
		var html = '<select name="'+element+'_p_'+section+'" id="'+element+'_p_'+section+'" class="form-control">';
	
	for (i = 1; i < 101; i++) { 
		html += '<option value="'+i+'">'+i+'</option>';
	}
	html = html + '</select>';
	return html;
}
function select_all_or_none(obj){	
	var id = obj.id;
	var value = obj.value;
	if(value == 'select_all')
		$("#"+id+" option").prop("selected",true);
	else{
		$("#"+id+" option[value='select_all']").prop("selected",false);
	}
}
function fnAddFree(){//ghfghffhgj
	//alert('tt');
	$("[id^='remove_free_product_']").show();
	
	var total_elements = $("#total_element_free").val();	
	var current_total_elements = parseInt(total_elements) + 1;
	
	var outer_div_open = '<div class="free" id="free_pro_'+current_total_elements+'"><div class="row">';
	var outer_div_close = '</div><hr/></div>';
	var section_campaign_open = '<div class="col-sm-6"><h4 style="margin-top:30px;"><b>Campaign Product</b>&nbsp;&nbsp;<span style="font-size:12px; display:none;" id="select_all_brand_span"><input type="checkbox" name="select_all_brand" id="select_all_brand" value="" onchange="javascript:fnSelectAllBrand(this)">&nbsp;(Select all Brands)</span></h4><hr />';
	var section_campaign_close = '</div>';
	
	var brand_campaign_section_open = '<div class="form-group" id="brand_div"><label class="col-md-6">Brand:<span class="mandatory">*</span></label><div class="col-md-5" id="div_brand_campaign_p_'+current_total_elements+'">';
	var brand_campaign_section = select_html('brand_campaign','c',current_total_elements,'fnShowCategoriess');
	var brand_campaign_section_close = '</div></div>';
	var brand_campaign_complete = brand_campaign_section_open+brand_campaign_section+brand_campaign_section_close;
	
	var category_campaign_section_open =  '<div class="form-group" id="category_div"><label class="col-md-6">Category:<span class="mandatory">*</span></label><div class="col-md-5" id="div_category_campaign_p_'+current_total_elements+'">';
	var category_campaign_section = select_html('category_campaign','c',current_total_elements,'fnShowProductss');
	var category_campaign_section_close = '</div></div>';
	var category_campaign_complete = category_campaign_section_open+category_campaign_section+category_campaign_section_close;
	
	var product_campaign_section_open =  '<div class="form-group" id="product_div"><label class="col-md-6">Product:<span class="mandatory">*</span></label><div class="col-md-5" id="div_product_campaign_p_'+current_total_elements+'">';
	var product_campaign_section = select_html('product_campaign','c',current_total_elements,'fnShowProductVariant');
	var product_campaign_section_close = '</div></div>';
	var product_campaign_complete = product_campaign_section_open+product_campaign_section+product_campaign_section_close;
	
	var variant_campaign_section_open =  '<div class="form-group" id="variant_div"><label class="col-md-6">Variant:<span class="mandatory">*</span></label><div class="col-md-5" id="div_variant_campaign_p_'+current_total_elements+'">';
	var variant_campaign_section = select_html('variant_campaign','c',current_total_elements,'');
	var variant_campaign_section_close =  '</div></div>';
	var variant_campaign_complete = variant_campaign_section_open+variant_campaign_section+variant_campaign_section_close;
	
	//left_count
	var quantity_campaign_section_open =  '<div class="form-group" id="quantity_div"><label class="col-md-6">No Of Products:<span class="mandatory">*</span></label><div class="col-md-5" id="div_left_count_p_'+current_total_elements+'">';
	var quantity_campaign_section = select_html_for_quantity('left_count','c',current_total_elements,'');
	var quantity_campaign_section_close =  '</div></div>';
	var quantity_campaign_complete = quantity_campaign_section_open+quantity_campaign_section+quantity_campaign_section_close;
	//alert(quantity_campaign_complete);
	
	var brand_all_div = '<div class="form-group" id="brand_all_msg" style="display:none;"><label class="col-md-6">This campaign is applicable to all Brands.</label></div>';
	var section_campaign_complete = section_campaign_open+brand_campaign_complete+category_campaign_complete+product_campaign_complete+variant_campaign_complete+quantity_campaign_complete+brand_all_div+section_campaign_close;
	var section_free_open = '<div class="col-sm-5"><h4 style="margin-top:30px;"><b>Free Product</b></h4><hr />';
	var section_free_close = '</div>';
	
	var brand_free_section_open =  '<div class="form-group"><label class="col-md-3">Brand:<span class="mandatory">*</span></label><div class="col-md-5" id="div_brand_free_p_'+current_total_elements+'">';
	var brand_free_section = select_html('brand_campaign','f',current_total_elements,'fnShowCategoriess');
	var brand_free_section_close =  '</div></div>';
	var brand_free_complete = brand_free_section_open+brand_free_section+brand_free_section_close;
	
	var category_free_section_open =  '<div class="form-group"><label class="col-md-3">Category:<span class="mandatory">*</span></label><div class="col-md-5" id="div_category_free_p_'+current_total_elements+'">';
	var category_free_section = select_html('category_free','f',current_total_elements,'fnShowProductss');
	var category_free_section_close = '</div></div>';
	var category_free_complete = category_free_section_open+category_free_section+category_free_section_close;
	
	var product_free_section_open =  '<div class="form-group"><label class="col-md-3">Product:<span class="mandatory">*</span></label><div class="col-md-5" id="div_product_free_p_'+current_total_elements+'">';
	var product_free_section = select_html('product_free','f',current_total_elements,'fnShowProductVariant');
	var product_free_section_close = '</div></div>';
	var product_free_complete = product_free_section_open+product_free_section+product_free_section_close;
	
	var variant_free_section_open =  '<div class="form-group"><label class="col-md-3">Variant:<span class="mandatory">*</span></label><div class="col-md-5" id="div_variant_free_p_'+current_total_elements+'">';
	var variant_free_section = select_html('variant_free','f',current_total_elements,'');
	var variant_free_section_close = '</div></div>';
	var variant_free_complete = variant_free_section_open+variant_free_section+variant_free_section_close;
	
	//right_count
	var quantity_free_section_open =  '<div class="form-group"><label class="col-md-3">No Of Products:<span class="mandatory">*</span></label><div class="col-md-5" id="div_right_count_p_'+current_total_elements+'">';
	var quantity_free_section = select_html_for_quantity('right_count','f',current_total_elements,'');
	var quantity_free_section_close = '</div></div>';
	var quantity_free_complete = quantity_free_section_open+quantity_free_section+quantity_free_section_close;
	
	
	var section_free_complete = section_free_open+brand_free_complete+category_free_complete+product_free_complete+variant_free_complete+quantity_free_complete+section_free_close;
	var remove_button = '<div class="col-md-1"><button class="btn btn-primary btn-md" type="button" id="remove_free_product_'+current_total_elements+'" name="remove_free_product_'+current_total_elements+'" onclick="fnRemoveFreeProductSection('+current_total_elements+')" title="Remove Campaign & Free Product"><i class="fa fa-times"></i></button></div>';
	
	var append_section = outer_div_open+section_campaign_complete+section_free_complete+remove_button+outer_div_close;
	$("#freecont").append(append_section);
	$("#free_pro_"+current_total_elements).loader('show');
	setTimeout(
	  function() 
	  {
		setTimeout(
		function() 
		{
			fnShowBrand('c',current_total_elements,'add_more');
		}, 2000);
			fnShowBrand('f',current_total_elements,'add_more');
			$("#free_pro_"+current_total_elements).loader('hide');
	  }, 2500);
	$("#select_all_brand_span").hide();	
	$("#total_element_free").val(current_total_elements);	
}
function fnRemoveFreeProductSection(element_id){
	$("[id^='remove_free_product_']").show();
	var total_elements = $("#total_element_free").val();
	if(total_elements > 1){
	var current_total_elements = parseInt(total_elements) - 1;
	$("#total_element_free").val(current_total_elements);
	$("#free_pro_"+element_id).remove();
	
		for(var i=element_id; i<= current_total_elements; i++){
			var current_id = parseInt(i)+1;
			//outer_div
			$("#free_pro_"+current_id).attr("id","free_pro_"+i);
			//Campaign section 
			$("#div_brand_campaign_p_"+current_id).attr("id","div_brand_campaign_p_"+i);
			$("#div_brand_campaign_p_"+i).attr("name","div_brand_campaign_p_"+i);
			
			$("#brand_campaign_p_"+current_id).attr("id","brand_campaign_p_"+i);
			$("#brand_campaign_p_"+i).attr("name","brand_campaign_p_"+i);
			$("#brand_campaign_p_"+i).attr("onchange","fnShowCategoriess(this+i)");//fnShowCategories(this,'c',1)	
			
			$("#div_category_campaign_p_"+current_id).attr("id","div_category_campaign_p_"+i);
			$("#div_category_campaign_p_"+i).attr("name","div_category_campaign_p_"+i);
			$("#category_campaign_p_"+current_id).attr("id","category_campaign_p_"+i);
			$("#category_campaign_p_"+i).attr("name","category_campaign_p_"+i);
			$("#category_campaign_p_"+i).attr("onchange","fnShowProductss(this,'c',"+i+")");//fnShowProductss(this,'c',1)
			
			$("#div_product_campaign_p_"+current_id).attr("id","div_product_campaign_p_"+i);
			$("#div_product_campaign_p_"+i).attr("name","div_product_campaign_p_"+i);
			$("#product_campaign_p_"+current_id).attr("id","product_campaign_p_"+i);
			$("#product_campaign_p_"+i).attr("name","product_campaign_p_"+i);
			$("#product_campaign_p_"+i).attr("onchange","fnShowProductVariant(this,'c',"+i+")");//fnShowProductVariant(this,'c',1)
			
			$("#div_variant_campaign_p_"+current_id).attr("id","div_variant_campaign_p_"+i);
			$("#div_variant_campaign_p_"+i).attr("name","div_variant_campaign_p_"+i);
			$("#variant_campaign_p_"+current_id).attr("id","variant_campaign_p_"+i);
			$("#variant_campaign_p_"+i).attr("name","variant_campaign_p_"+i);
			
			//Free section			
			$("#div_brand_free_p_"+current_id).attr("id","div_brand_free_p_"+i);
			$("#div_brand_free_p_"+i).attr("name","div_brand_free_p_"+i);			
			$("#brand_free_p_"+current_id).attr("id","brand_free_p_"+i);
			$("#brand_free_p_"+i).attr("name","brand_free_p_"+i);
			$("#brand_free_p_"+i).attr("onchange","fnShowCategoriess(this,'f',"+i+")");//fnShowCategories(this,'f',1)
			
			$("#div_category_free_p_"+current_id).attr("id","div_category_free_p_"+i);
			$("#div_category_free_p_"+i).attr("name","div_category_free_p_"+i);
			$("#category_free_p_"+current_id).attr("id","category_free_p_"+i);
			$("#category_free_p_"+i).attr("name","category_free_p_"+i);
			$("#category_free_p_"+i).attr("onchange","fnShowProductss(this,'f',"+i+")");//fnShowProductss(this,'f',1)
			
			$("#div_product_free_p_"+current_id).attr("id","div_product_free_p_"+i);
			$("#div_product_free_p_"+i).attr("name","div_product_free_p_"+i);
			$("#product_free_p_"+current_id).attr("id","product_free_p_"+i);
			$("#product_free_p_"+i).attr("name","product_free_p_"+i);
			$("#product_free_p_"+i).attr("onchange","fnShowProductVariant(this,'f',"+i+"))");//fnShowProductVariant(this,'f',1)
			
			$("#div_variant_free_p_"+current_id).attr("id","div_variant_free_p_"+i);
			$("#div_variant_free_p_"+i).attr("name","div_variant_free_p_"+i);
			$("#variant_free_p_"+current_id).attr("id","variant_free_p_"+i);
			$("#variant_free_p_"+i).attr("name","variant_free_p_"+i);
						
			$("#remove_free_product_"+current_id).attr("id","remove_free_product_"+i);
			$("#remove_free_product_"+i).attr("name","remove_free_product_"+i);	
			$("#remove_free_product_"+i).attr("onclick","fnRemoveFreeProductSection("+i+")");
				
		}
		if(current_total_elements == 1){
			$("[id^='remove_free_product_']").hide();
			$("#select_all_brand_span").show();
		}
	}
}
function fnAddFreeWeight(){
	 $("[id^='remove_free_product_weight_']").show();
	var total_elements = $("#total_element_free_wt").val();	
	var current_total_elements = parseInt(total_elements) + 1;
	
	var outer_div_open = '<div id="weight_free_pro_'+current_total_elements+'"><div class="row free">';
	var outer_div_close = '</div><hr/></div>';
	var section_campaign_open = '<div class="col-sm-6"><h4 style="margin-top:30px;"><b>Campaign Product</b></h4><hr />';
	var section_campaign_close = '</div>';
	
	
	var variant_campaign_section_open =  '<div class="form-group"><label class="col-md-6">Weight:<span class="mandatory">*</span></label><div class="col-md-5">\
											<div id="nopad1" style="float:left;">\
											<input class="nopad1" style="width:92px;" type="text" name="campaign_product_wt_qty_'+current_total_elements+'" id="campaign_product_wt_qty_'+current_total_elements+'" data-parsley-trigger="change" data-parsley-pattern="[0-9]*(\.?[0-9]{1,2}$)?"  class="form-control">\
											&nbsp;&nbsp;</div>\
											<div style="float:left;"  id="div_variant_unit_campaign_p_'+current_total_elements+'">\
											Kg</div>\
											<div class="clearfix"></div> \
											</div>';
	
	var variant_campaign_section_close =  '</div>';
	var variant_campaign_complete = variant_campaign_section_open+variant_campaign_section_close;
	
	var section_campaign_complete = section_campaign_open+variant_campaign_complete+section_campaign_close;
	var section_free_open = '<div class="col-sm-5"><h4 style="margin-top:30px;"><b>Free Product</b></h4><hr />';
	var section_free_close = '</div>';
	
	var brand_free_section_open =  '<div class="form-group"><label class="col-md-3">Brand:<span class="mandatory">*</span></label><div class="col-md-6" id="div_brand_free_p_'+current_total_elements+'">';
	var brand_free_section = select_html('brand_campaign','f',current_total_elements,'fnShowCategoriess');
	var brand_free_section_close =  '</div></div>';
	var brand_free_complete = brand_free_section_open+brand_free_section+brand_free_section_close;
	
	var category_free_section_open =  '<div class="form-group"><label class="col-md-3">Category:<span class="mandatory">*</span></label><div class="col-md-6" id="div_category_free_p_'+current_total_elements+'">';
	var category_free_section = select_html('category_free','f',current_total_elements,'fnShowProductss');
	var category_free_section_close = '</div></div>';
	var category_free_complete = category_free_section_open+category_free_section+category_free_section_close;
	
	var product_free_section_open =  '<div class="form-group"><label class="col-md-3">Product:<span class="mandatory">*</span></label><div class="col-md-6" id="div_product_free_p_'+current_total_elements+'">';
	var product_free_section = select_html('product_free','f',current_total_elements,'fnShowProductVariant');
	var product_free_section_close = '</div></div>';
	var product_free_complete = product_free_section_open+product_free_section+product_free_section_close;
	
	var variant_free_section_open =  '<div class="form-group"><label class="col-md-3">Variant:<span class="mandatory">*</span></label><div class="col-md-6" id="div_variant_free_p_'+current_total_elements+'">';
	var variant_free_section = select_html('variant_free','f',current_total_elements,'');
	var variant_free_section_close = '</div></div>';
	var variant_free_complete = variant_free_section_open+variant_free_section+variant_free_section_close;	
	
	var section_free_complete = section_free_open+brand_free_complete+category_free_complete+product_free_complete+variant_free_complete+section_free_close;
	var remove_button = '<div class="col-md-1"><button class="btn btn-primary btn-md" type="button" id="remove_free_product_weight_'+current_total_elements+'" name="remove_free_product_weight_'+current_total_elements+'" onclick="fnRemoveFreePWtSection('+current_total_elements+')" title="Remove Campaign & Free Product"><i class="fa fa-times"></i></button></div>';
	
	var append_section = outer_div_open+section_campaign_complete+section_free_complete+remove_button+outer_div_close;
	$("#weight_freecont").append(append_section);
	
	
	$("#weight_free_pro_"+current_total_elements).loader('show');
	setTimeout(
	  function() 
	  {
		  fnShowBrand('f',current_total_elements,'add_more');
			$("#weight_free_pro_"+current_total_elements).loader('hide');
	  }, 2500);
	
	$("#total_element_free_wt").val(current_total_elements);	
}
function fnRemoveFreePWtSection(element_id){
	$("[id^='remove_free_product_weight_']").show();
	var total_elements = $("#total_element_free_wt").val();
	if(total_elements > 1){
	var current_total_elements = parseInt(total_elements) - 1;
	$("#total_element_free_wt").val(current_total_elements);
	$("#weight_free_pro_"+element_id).remove();
	
		for(var i=element_id; i<= current_total_elements; i++){
			var current_id = parseInt(i)+1;
			//outer_div
			$("#weight_free_pro_"+current_id).attr("id","weight_free_pro_"+i);
			//Campaign section			
			$("#campaign_product_wt_qty_"+current_id).attr("id","campaign_product_wt_qty_"+i);
			$("#campaign_product_wt_qty_"+i).attr("name","campaign_product_wt_qty_"+i);			
			
			//Free section			
			$("#div_brand_free_p_"+current_id).attr("id","div_brand_free_p_"+i);
			$("#div_brand_free_p_"+i).attr("name","div_brand_free_p_"+i);			
			$("#brand_free_p_"+current_id).attr("id","brand_free_p_"+i);
			$("#brand_free_p_"+i).attr("name","brand_free_p_"+i);
			$("#brand_free_p_"+i).attr("onchange","fnShowCategoriess(this,'f',"+i+")");//fnShowCategories(this,'f',1)
			
			$("#div_category_free_p_"+current_id).attr("id","div_category_free_p_"+i);
			$("#div_category_free_p_"+i).attr("name","div_category_free_p_"+i);
			$("#category_free_p_"+current_id).attr("id","category_free_p_"+i);
			$("#category_free_p_"+i).attr("name","category_free_p_"+i);
			$("#category_free_p_"+i).attr("onchange","fnShowProductss(this,'f',"+i+")");//fnShowProductss(this,'f',1)
			
			$("#div_product_free_p_"+current_id).attr("id","div_product_free_p_"+i);
			$("#div_product_free_p_"+i).attr("name","div_product_free_p_"+i);
			$("#product_free_p_"+current_id).attr("id","product_free_p_"+i);
			$("#product_free_p_"+i).attr("name","product_free_p_"+i);
			$("#product_free_p_"+i).attr("onchange","fnShowProductVariant(this,'f',"+i+"))");//fnShowProductVariant(this,'f',1)
			
			$("#div_variant_free_p_"+current_id).attr("id","div_variant_free_p_"+i);
			$("#div_variant_free_p_"+i).attr("name","div_variant_free_p_"+i);
			$("#variant_free_p_"+current_id).attr("id","variant_free_p_"+i);
			$("#variant_free_p_"+i).attr("name","variant_free_p_"+i);
						
			$("#remove_free_product_"+current_id).attr("id","remove_free_product_"+i);
			$("#remove_free_product_"+i).attr("name","remove_free_product_"+i);	
			$("#remove_free_product_"+i).attr("onclick","fnRemoveFreeProductSection("+i+")");
				
		}
		if(current_total_elements == 1){
			$("[id^='remove_free_product_weight_']").hide();
		}
	}
}
function fnQuantitySection(section){
	if(section == "packet"){		
		document.getElementById("packet_div").style="display:block";
		document.getElementById("pcs_div").style="display:none";
		document.getElementById("weight_div").style="display:none";
	}else if(section == "pcs"){		
		document.getElementById("pcs_div").style="display:block";
		document.getElementById("weight_div").style="display:none";
		document.getElementById("packet_div").style="display:none";
	}else if(section == "weight"){
		document.getElementById("weight_div").style="display:block";
		document.getElementById("packet_div").style="display:none";
		document.getElementById("pcs_div").style="display:none";
	}
	
}
function fnDiscountSection(section){
	if(section == "pcs"){		
		document.getElementById("pcs_discount_div").style="display:block";
		document.getElementById("weight_discount_div").style="display:none";
	}else if(section == "weight"){
		document.getElementById("weight_discount_div").style="display:block";
		document.getElementById("pcs_discount_div").style="display:none";
	}
	
}
function validateForm(){
	$('#campaign_add').parsley().validate();
	var campaign_name = $("#campaign_name").val();
	var campaign_start_date = $("#campaign_start_date").val();
	var campaign_end_date = $("#campaign_end_date").val();
	var validate = 0;
	if(campaign_name == '')
	{
		alert('Please enter Campaign Name');
		$('#campaign_name').focus();
		return false;
	}else{
		if(campaign_name != '')
		{
			jQuery.ajax({
				url: "../includes/checkAvailable.php",
				data:'module=campaign&campaign_name='+campaign_name,
				type: "POST",
				async:false,
				success:function(data){	
					if(data=="exist") {
						alert('Campaign Name already exists');				
						validate = 1;
						return false;
					} else{
						validate = 0;
					}
				},
				error:function (){}
			});
		}
	}
	setTimeout(function () {	
		if(campaign_start_date == '')
		{
			alert('Please enter Start Date');
			$('#campaign_start_date').focus();
			return false;
		}else{
			var today = today_date();		
			var date_validate = compaire_dates(today,campaign_start_date);
			if(date_validate == 1)
			{
				alert("'Start Date' should be greater than or equal to current date.");
				return false;
			}
		}
		if(campaign_end_date == '')
		{
			alert('Please enter End Date');
			$('#campaign_end_date').focus();
			return false;
		}	
		if(campaign_start_date != '' && campaign_end_date != '')
		{
			var date_validate = compaire_dates(campaign_start_date,campaign_end_date);
			if(date_validate == 1)
			{
				alert("'End Date' should be greater than 'Start Date'.");
				return false;
			}
		}
		
		var criteriaType_checked = $('input[name="criteriaType"]').is(':checked');
		var criteriaType = $("input[name='criteriaType']:checked").val();
		
		if(criteriaType_checked == false)
		{
			alert('Please select Campaign Type');
			$('#criteriaType').focus();
			return false;
		}
		else{
			switch(criteriaType){
				case "free_product":
					var state_arr_count = 0;
					var state_str = $("#state_campaign_discount:enabled").val(); 	
					if(state_str != null)
						var state_arr_count = calculate_data_count(state_str);
					
					
					var city_arr_count = 0;
					var city_str = $("#city_campaign_discount:enabled").val();
					if(city_str != null)
						var city_arr_count = calculate_data_count(city_str);
					
					var suburb_arr_count = 0;
					var suburb_str = $("#suburb_campaign_discount:enabled").val();	
					if(suburb_str != null)		
						var suburb_arr_count = calculate_data_count(suburb_str);
				
					var shop_arr_count = 0;
					var shop_str = $("#shop_campaign_discount:enabled").val();		
					if(shop_str != null)
						var shop_arr_count = calculate_data_count(shop_str);	
					
					if(state_arr_count == 0)
					{
						alert('Please select State for this Campaign');
						$("#state_campaign_discount:enabled").focus();
						return false;
					}
					var current_total_elements = parseInt($("#total_element_free").val());
					if(current_total_elements > 0){				
							var selected_product_variant = [];  
							//var selected_product_variant = new Array();  
						for(var i=1; i<= current_total_elements; i++){
							var selected_product_variant_str = '';
							if($("#select_all_brand").is(':checked') == false){
								var campaign_brand = $("#brand_campaign_p_"+i).val();	
								if(campaign_brand == '')
								{
									alert('Please select Brand for Campaign Product ('+i+')');
									$('#brand_campaign_p_'+i).focus();
									return false;
								}
								var campaign_category = $("#category_campaign_p_"+i).val();
								if(campaign_category == '')
								{
									alert('Please select Category for Campaign Product ('+i+')');
									$('#category_campaign_p_'+i).focus();
									return false;
								}
								var campaign_product = $("#product_campaign_p_"+i).val();
								if(campaign_product == '')
								{
									alert('Please select Campaign Product ('+i+')');
									$('#product_campaign_p_'+i).focus();
									return false;
								}
								var campaign_variant = $("#variant_campaign_p_"+i).val();
								if(campaign_variant == null)
								{
									alert('Please select Campaign Product Variant ('+i+')');
									$('#variant_campaign_p_'+i).focus();
									return false;
								}else{
									if(campaign_product != '')
										selected_product_variant_str = campaign_product+'#'+campaign_variant;
								}
							}
							
							var free_brand = $("#brand_free_p_"+i+":enabled").val();
							if(free_brand == '')
							{
								alert('Please select Brand for Free Product ('+i+')');
								$('#brand_free_p_'+i+':enabled').focus();
								return false;
							}
							
							
							var free_category = $("#category_free_p_"+i+":enabled").val();				
							if(free_category == '')
							{
								alert('Please select Category for Free Product ('+i+')');
								$('#category_free_p_'+i+':enabled').focus();
								return false;
							} 
							
							
							var free_product = $("#product_free_p_"+i+":enabled").val();	
							if(free_product == '')
							{
								alert('Please select Free Product ('+i+')');
								$('#product_free_p_'+i+':enabled').focus();
								return false;
							}
							
							
							var free_variant = $("#variant_free_p_"+i+":enabled").val();					
							if(free_variant == '')
							{
								alert('Please select Free Product Variant ('+i+')');
								$('#variant_free_p_'+i+':enabled').focus();
								return false;
							}
							if($("#select_all_brand").is(':checked') == false){
								if(i != 1 && $.inArray(selected_product_variant_str, selected_product_variant) !== -1)
								{
									alert('Product already selected');
									$('#product_campaign_p_'+i).focus();
									return false;
								}
								
								selected_product_variant[i] = selected_product_variant_str; 
							}
							/*for(var j = 1; j<= selected_product_variant.length; j++)
							{
								alert("val  "+selected_product_variant[j]);
							}*/
						}
					}
				break;
				case "discount": 
					var state_arr_count = 0;
					var state_str = $("#state_campaign_discount:enabled").val(); 
					if(state_str != null)
						var state_arr_count = calculate_data_count(state_str);
					
					
					var city_arr_count = 0;
					var city_str = $("#city_campaign_discount:enabled").val();
					if(city_str != null)
						var city_arr_count = calculate_data_count(city_str);
					
					var suburb_arr_count = 0;
					var suburb_str = $("#suburb_campaign_discount:enabled").val();	
					if(suburb_str != null)		
						var suburb_arr_count = calculate_data_count(suburb_str);
				
					var shop_arr_count = 0;
					var shop_str = $("#shop_campaign_discount:enabled").val();		
					if(shop_str != null)
						var shop_arr_count = calculate_data_count(shop_str);	
					
					
				
					if(state_arr_count == 0)
					{
						alert('Please select State for this Campaign');
						$("#state_campaign_discount:enabled").focus();
						return false;
					}
					
					var current_total_elements = parseInt($("#total_element").val());
					
					if(current_total_elements > 0){
						for(var i=1; i<= current_total_elements; i++){
							var campaign_product_price = $("#campaign_product_price_"+i).val();
							var campaign_product_discount = $("#campaign_product_discount_"+i).val();
							if(campaign_product_price == '')
							{
								alert('Please enter Price/Amount to be discounted');
								$('#campaign_product_price_'+i).focus();
								return false;
							}
							if(campaign_product_price == 0)
							{
								alert('Campaign Product Price should not be zero(0)');
								$('#campaign_product_price_'+i).focus();
								return false;
							}
							if(campaign_product_discount == '')
							{
								alert('Please Percentage(%) of discount on Price');
								$('#campaign_product_discount_'+i).focus();
								return false;
							}
							if(campaign_product_discount == 0)
							{
								alert('Campaign Product Discount should not be zero(0)');
								$('#campaign_product_discount_'+i).focus();
								return false;
							}
						}
					}
				break;
				case "by_weight":
					var state_arr_count = 0;
					var state_str = $("#state_campaign_discount:enabled").val(); 	
					if(state_str != null)
						var state_arr_count = calculate_data_count(state_str);
								
					
					if(state_arr_count == 0)
					{
						alert('Please select State for this Campaign');
						$("#state_campaign_discount:enabled").focus();
						return false;
					}
					var current_total_elements = parseInt($("#total_element_free_wt").val());
					if(current_total_elements > 0){				
						var selected_product_variant = [];  
						
						for(var i=1; i<= current_total_elements; i++){
							var selected_product_variant_str = '';
							var campaign_weight = $("#campaign_product_wt_qty_"+i+":enabled").val();				
							if(campaign_weight == '')
							{
								alert('Please enter Campaign Product Weight');
								$('#campaign_product_wt_qty_'+i+":enabled").focus();
								return false;
							}
							if(campaign_weight == 0)
							{
								alert('Campaign Product Weight should not be zero(0)');
								$('#campaign_product_wt_qty_'+i+":enabled").focus();
								return false;
							}
							
							var free_brand = $("#brand_free_p_"+i+":enabled").val();	
							if(free_brand == '')
							{
								alert('Please select Brand for Free Product ');
								$('#brand_free_p_'+i+":enabled").focus();
								return false;
							}
							
							var free_category = $("#category_free_p_"+i+":enabled").val();				
							if(free_category == '')
							{
								alert('Please select Category for Free Product');
								$('#category_free_p_'+i+":enabled").focus();
								return false;
							} 
							
							var free_product = $("#product_free_p_"+i+":enabled").val();	
							if(free_product == '')
							{
								alert('Please select Free Product');
								$('#product_free_p_'+i+":enabled").focus();
								return false;
							}
							
							var free_variant = $("#variant_free_p_"+i+":enabled").val();					
							if(free_variant == '')
							{
								alert('Please select Free Product Variant');
								$('#variant_free_p_'+i+':enabled').focus();
								return false;
							}
						}
					}
				break;
			}
		}
	
		if(validate == 0){
			$('#campaign_add').submit();
		}
	}, 200);	
}
function today_date(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!

	var yyyy = today.getFullYear();
	return dd+'-'+mm+'-'+yyyy;
}
function compaire_dates(date1,date2){
	
	if(date1 != '')
	{
		var parts1 = date1.split("-");
		var d1 =new Date(parts1[2], parts1[1] - 1, parts1[0]);
		//var d1 = new Date(date1);
		var fromtime = d1.getTime();
	}	
	if(date2 != '')
	{
		var parts2 = date2.split("-");
		var d2 =new Date(parts2[2], parts2[1] - 1, parts2[0]);
		//var d2 = new Date(date2);
		var totime = d2.getTime();
	}
	if(date1 != '' && date2 != '')
	{
		if(fromtime > totime)			
			return 1;//date1 greater than date 2
		else
			return 0;//date1 lesser than date 2
	}
	
}
function fnSelectAllBrand(obj){
	if($(obj).is(':checked') == true)
	{		
		$("#brand_div").hide();
		$("#category_div").hide();
		$("#product_div").hide();
		$("#variant_div").hide();
		$("#btnAddFree").hide();	
		$("#brand_all_msg").show();	
	}else{		
		$("#brand_div").show();
		$("#category_div").show();
		$("#product_div").show();
		$("#variant_div").show();
		$("#btnAddFree").show();
		$("#brand_all_msg").hide();	
	}	
}