<?php
/***********************************************************
 * File Name	: reportManage.php
 ************************************************************/	
class routeManage
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
	}
	
	 function getRouteCount() {
       // $user_id=$_SESSION[SESSION_PREFIX . "user_id"];          
       $sql = "SELECT name as route_name,id as route_id  FROM tbl_route_by_map ORDER BY id DESC";
        $result = mysqli_query($this->local_connection, $sql);
       $route=array();
        $row_count = mysqli_num_rows($result);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $route[] = $row['route_id'];
            }
            return $route;
        } else {
            return 0;
        } 
    }
      function getRouteCountDetails($route_id) {
		 $sql = "SELECT rd.id as id, `route_id`, `address`, `lattitude`, `longitude`,trbm.name as route_name
		FROM tbl_route_details_by_map  rd
		left join tbl_route_by_map trbm on rd.route_id=trbm.id
		WHERE rd.route_id='".$route_id."' ORDER BY rd.id ASC";
        $result = mysqli_query($this->local_connection, $sql);
        
        $i = 0;
        $row_count = mysqli_num_rows($result);
        if ($row_count > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $records[$i] = $row;
                $i++;
            }
            return $records;
        } else {
            return 0;
        }
    }

     function getRoutePopup($route_id) {
		 $sql = "SELECT rd.id as id, `route_id`, `address`, `lattitude`, `longitude`,trbm.name as route_name
		FROM tbl_route_details_by_map  rd
		left join tbl_route_by_map trbm on rd.route_id=trbm.id
		WHERE rd.route_id='".$route_id."' ORDER BY rd.id ASC";
        $result = mysqli_query($this->local_connection, $sql);
       // $row_orders_det_count = mysqli_num_rows($orders_det_result);
       // return $row = mysqli_fetch_array($orders_det_result);
        
       
    }
       
}
?>