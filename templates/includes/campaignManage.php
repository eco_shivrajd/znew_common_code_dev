<?php
/***********************************************************
 * File Name	: testManager.php
 ************************************************************/	

class campaignManager
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;		
	}		
	
	function get_campaigndata() {
        $sql_campaign = "SELECT `id` AS campaign_id, `campaign_name`, `campaign_description`, 
		`campaign_start_date`, `campaign_end_date`, `campaign_type`, `status` 
		FROM `tbl_campaign` WHERE deleted = 0 AND status =0 AND campaign_end_date >= '" . date('Y-m-d') . "'";
		$result_campaign = mysqli_query($this->local_connection,$sql_campaign);
        //$result_campaign = $this->executeQuery($sql_campaign);

        while ($row_campaign = mysqli_fetch_array($result_campaign)) {
            $campaign_array_temp['campaign_id'] = $row_campaign['campaign_id'];
            $campaign_array_temp['campaign_name'] = $row_campaign['campaign_name'];
            $campaign_array_temp['campaign_description'] = $row_campaign['campaign_description'];
            $campaign_array_temp['campaign_start_date'] = $row_campaign['campaign_start_date'];
            $campaign_array_temp['campaign_end_date'] = $row_campaign['campaign_end_date'];
            $campaign_array_temp['campaign_type'] = $row_campaign['campaign_type'];

            $sql_campaign_area = "SELECT `level`, `state_id`, `city_id`, `suburb_id`, `shop_id`, subarea_id 
			FROM `tbl_campaign_area` 
			WHERE deleted = 0 AND level!='shop' AND campaign_id=" . $row_campaign['campaign_id'];
			$result_campaign_area = mysqli_query($this->local_connection,$sql_campaign_area);
			
            //$result_campaign_area = $this->executeQuery($sql_campaign_area);
            $campaign_array_temp['campaign_area'] = array();
            $campaign_array_temp['campaign_product'] = array();
            $campaign_array_temp['campaign_product_weight'] = array();
            while ($row_campaign_area = mysqli_fetch_array($result_campaign_area)) {
                $campaign_array_temp['campaign_area']['level'] = $row_campaign_area['level'];

                $sql_state = "SELECT `name` FROM `tbl_state` WHERE id IN (" . $row_campaign_area['state_id'] . ")";
				$result_state = mysqli_query($this->local_connection,$sql_state);
               // $result_state = $this->executeQuery($sql_state);
                $state_name_arr = array();

                while ($row_state = mysqli_fetch_array($result_state)) {
                    $state_name_arr[] = $row_state['name'];
                }
                $state_name = implode(',', $state_name_arr);
                $campaign_array_temp['campaign_area']['state_id'] = $row_campaign_area['state_id'];
                $campaign_array_temp['campaign_area']['state_name'] = $state_name;

                if ($row_campaign_area['city_id'] != '') {
                    $sql_city = "SELECT `name` FROM `tbl_city` WHERE id IN (" . $row_campaign_area['city_id'] . ")";
					$result_city = mysqli_query($this->local_connection,$sql_city);
                    //$result_city = $this->executeQuery($sql_city);
                    $city_name_arr = array();
                    while ($row_city = mysqli_fetch_array($result_city)) {
                        $city_name_arr[] = $row_city['name'];
                    }
                    $city_name = implode(',', $city_name_arr);
                    $campaign_array_temp['campaign_area']['city_id'] = $row_campaign_area['city_id'];
                    $campaign_array_temp['campaign_area']['city_name'] = $city_name;
                } else {
                    $campaign_array_temp['campaign_area']['city_id'] = "";
                    $campaign_array_temp['campaign_area']['city_name'] = "";
                }
                if ($row_campaign_area['suburb_id'] != '') {
                    $sql_suburb = "SELECT `suburbnm` FROM `tbl_area` WHERE id IN (" . $row_campaign_area['suburb_id'] . ")";
					$result_suburb = mysqli_query($this->local_connection,$sql_suburb);
                    //$result_suburb = $this->executeQuery($sql_suburb);
                    $suburb_name_arr = array();
                    while ($row_suburb = mysqli_fetch_array($result_suburb)) {
                        $suburb_name_arr[] = $row_suburb['suburbnm'];
                    }
                    $suburb_name = implode(',', $suburb_name_arr);
                    $campaign_array_temp['campaign_area']['suburb_id'] = $row_campaign_area['suburb_id'];
                    $campaign_array_temp['campaign_area']['suburb_name'] = $suburb_name;
                } else {
                    $campaign_array_temp['campaign_area']['suburb_id'] = "";
                    $campaign_array_temp['campaign_area']['suburb_name'] = "";
                }

                // 
                if ($row_campaign_area['subarea_id'] != '') {
                    $sql_subarea = "SELECT subarea_name,subarea_id FROM tbl_subarea WHERE subarea_id IN (" . $row_campaign_area['subarea_id'] . ")";
					$result_subarea = mysqli_query($this->local_connection,$sql_subarea);
                    //$result_subarea = $this->executeQuery($sql_subarea);
                    $subarea_name_arr = array();

                    while ($row_subarea = mysqli_fetch_array($result_subarea)) {
                        $subarea_name_arr[] = $row_subarea['subarea_name'];
                    }

                    $subarea_name = implode(',', $subarea_name_arr);

                    $campaign_array_temp['campaign_area']['subarea_id'] = $row_campaign_area['subarea_id'];
                    $campaign_array_temp['campaign_area']['subarea_name'] = $subarea_name;
                } else {
                    $campaign_array_temp['campaign_area']['subarea_id'] = "";
                    $campaign_array_temp['campaign_area']['subarea_name'] = "";
                }

                if ($row_campaign_area['shop_id'] != '') {
                    $sql_shop = "SELECT `name` FROM `tbl_shops` WHERE id IN (" . $row_campaign_area['shop_id'] . ")";
					$result_shop = mysqli_query($this->local_connection,$sql_shop);
                    //$result_shop = $this->executeQuery($sql_shop);
                    $shop_name_arr = array();
                    while ($row_shop = mysqli_fetch_array($result_shop)) {
                        $shop_name_arr[] = $row_shop['name'];
                    }
                    $shop_name = implode(',', $shop_name_arr);
                    $campaign_array_temp['campaign_area']['shop_id'] = $row_campaign_area['shop_id'];
                    $campaign_array_temp['campaign_area']['shop_name'] = $shop_name;
                } else {
                    $campaign_array_temp['campaign_area']['shop_id'] = "";
                    $campaign_array_temp['campaign_area']['shop_name'] = "";
                }
            }

            switch ($row_campaign['campaign_type']) {
                case 'discount':
                    $sql_campaign_area_price = "SELECT `product_price`, `discount_percent` FROM `tbl_campaign_area_price` WHERE deleted = 0 AND campaign_id=" . $row_campaign['campaign_id'];
                    $result_campaign_area_price = mysqli_query($this->local_connection,$sql_campaign_area_price);
					//$result_campaign_area_price = $this->executeQuery($sql_campaign_area_price);
                    $i = 1;
                    $campaign_array_temp['area_price'] = array();
                    while ($row_campaign_area_price = mysqli_fetch_array($result_campaign_area_price)) {
                        $record_discount = array();
                        $record_discount['record_counter'] = $i;
                        $record_discount['product_price'] = $row_campaign_area_price['product_price'];
                        $record_discount['discount_percent'] = $row_campaign_area_price['discount_percent'];
                        $campaign_array_temp['area_price'][] = $record_discount;
                        $i++;
                    }
                    break;
                case 'free_product':

                   echo $sql_campaign_free_product = "SELECT `c_p_brand_id`, `c_p_category_id`, `c_product_id`, `c_p_quantity`,
					`c_p_measure`, `c_p_measure_id`,`c_p_quantity_measure`, `f_p_brand_id`, `f_p_category_id`, `f_product_id`, 
					`f_p_quantity`, `f_p_measure`, `f_p_measure_id`,`f_p_quantity_measure` 
					FROM `tbl_campaign_product` 
					WHERE deleted = 0 AND campaign_id=" . $row_campaign['campaign_id'];
					$result_campaign_free_product = mysqli_query($this->local_connection,$sql_campaign_free_product);
                    //$result_campaign_free_product = $this->executeQuery($sql_campaign_free_product);
                    $campaign_array_temp['area_price'] = array();
                    $campaign_array_temp['campaign_product'] = array();
                    $campaign_array_temp['campaign_product_weight'] = array();

                    $j = 1;
                    while ($row_campaign_free_product = mysqli_fetch_array($result_campaign_free_product)) {
                        $record_product = array();
                        $record_product['record_counter'] = $j;
                        $record_product['c_p_brand_id'] = $row_campaign_free_product['c_p_brand_id'];
                        $record_product['c_p_brand_name'] = $this->getBrand($row_campaign_free_product['c_p_brand_id']);
                        $record_product['c_p_category_id'] = $row_campaign_free_product['c_p_category_id'];
                        $record_product['c_p_category_name'] = $this->getCategory($row_campaign_free_product['c_p_category_id']);
                        $record_product['c_product_id'] = $row_campaign_free_product['c_product_id'];
                        $record_product['c_product_name'] = $this->getProduct($row_campaign_free_product['c_product_id']);
                        if ($row_campaign_free_product['c_p_measure_id'] != '') {

                            $sqlp = "SELECT `id`, `productid`, `units_variant_id`, `numunits`, `variantrownumber`, `price`, 
							`productvarid_margin`, `variant_1`, `variant_2`, `variant1_unit_id`, `variant2_unit_id`,
							`variant_cnt`, `productimage`, `productbarcodeimage`, `producthsn`, `cgst`, `sgst`, 
							`carton_quantities` 
							from `tbl_product_variant` where id = " . $row_campaign_free_product['c_p_measure_id'];
                            $result1p = mysqli_query($this->local_connection,$sqlp);
							//$result1p = $this->executeQuery($sqlp);
                            $i = 1;
                            while ($row = mysqli_fetch_array($result1p)) {                               
								  $variant_1 = $row['variant_1'];
								 $variant1_unit_id = $row['variant1_unit_id'];
                               // $imp_variant1 = split(',', $exp_variant1);

                                if ($variant_1 != '') {
                                    $sql1p = "SELECT  unitname,id FROM `tbl_units` WHERE id=" . $variant1_unit_id;
									$result2p = mysqli_query($this->local_connection,$sql1p);
                                    //$result2p = $this->executeQuery($sql1p);
                                    $row_prd_variant1p = mysqli_fetch_array($result2p);
                                    $combine1 = $row['id'] . " " . $variant_1 . " " . $row_prd_variant1p['id'];
                                    $combine1_display = "";

                                    $combine1_displayp = $row_prd_variant1p['unitname'];
                                }
                            }

                            $record_product['c_product_variant_wt_quantity'] = $row_campaign_free_product['c_p_quantity'];
                            $record_product['c_product_variant_unit'] = $combine1_displayp;
                            $record_product['c_product_variant'] = $row_campaign_free_product['c_p_quantity'] . " " . $combine1_displayp;
                            $record_product['c_product_variant_unit_id'] = $row_campaign_free_product['c_p_measure_id'];
                        } else
                            $campaign_array_temp['campaign_product']['c_product_variant'] = "";

                        $record_product['f_p_brand_id'] = $row_campaign_free_product['f_p_brand_id'];
                        $record_product['f_p_brand_name'] = $this->getBrand($row_campaign_free_product['f_p_brand_id']);
                        $record_product['f_p_category_id'] = $row_campaign_free_product['f_p_category_id'];
                        $record_product['f_p_category_name'] = $this->getCategory($row_campaign_free_product['f_p_category_id']);
                        $record_product['f_product_id'] = $row_campaign_free_product['f_product_id'];
                        $record_product['f_product_name'] = $this->getProduct($row_campaign_free_product['f_product_id']);
                        if ($row_campaign_free_product['f_p_measure_id'] != '') {
                            $sql = "SELECT * from `tbl_product_variant` where id = " . $row_campaign_free_product['f_p_measure_id'];
                            $result1 = mysqli_query($this->local_connection,$sql);
							//$result1 = $this->executeQuery($sql);
                            $i = 1;
                            while ($row = mysqli_fetch_array($result1)) {
                                $variant_1 = $row['variant_1'];
								 $variant1_unit_id = $row['variant1_unit_id'];
                                //$imp_variant1 = split(',', $exp_variant1);

                                if ($variant_1 != '') {
                                    $sql1 = "SELECT  unitname,id FROM `tbl_units` WHERE id=" . $variant1_unit_id;
									$result2 = mysqli_query($this->local_connection,$sql1);
                                    //$result2 = $this->executeQuery($sql1);
                                    $row_prd_variant1 = mysqli_fetch_array($result2);
                                    $combine1 = $row['id'] . " " . $variant_1 . " " . $row_prd_variant1['id'];
                                    $combine1_display = "";

                                    $combine1_display = $row_prd_variant1['unitname'];
                                }
                            }
                            $record_product['f_product_variant_wt_quantity'] = $row_campaign_free_product['f_p_quantity'];
                            $record_product['f_product_variant_unit'] = $combine1_display;
                            $record_product['f_product_variant'] = $row_campaign_free_product['f_p_quantity'] . " " . $combine1_display;
                            $record_product['f_product_variant_unit_id'] = $row_campaign_free_product['f_p_measure_id'];
                        } else
                            $campaign_array_temp['campaign_product']['f_product_variant'] = "";

                        $campaign_array_temp['campaign_product'][] = $record_product;
                        $j++;
                    }
                    break;
                case 'by_weight':
                    $sql_campaign_product_weight = "SELECT `id`, `campaign_id`, `campaign_area_id`, `c_weight`, `c_unit`, `f_p_brand_id`, `f_p_category_id`, `f_product_id`, `f_p_quantity`, `f_p_measure`, `f_p_measure_id`, `f_p_quantity_measure` FROM `tbl_campaign_product_weight` WHERE deleted = 0 AND campaign_id=" . $row_campaign['campaign_id'];
					$result_campaign_product_weight = mysqli_query($this->local_connection,$sql_campaign_product_weight);
                    //$result_campaign_product_weight = $this->executeQuery($sql_campaign_product_weight);
                    $campaign_array_temp['area_price'] = array();
                    $campaign_array_temp['campaign_product'] = array();
                    $campaign_array_temp['campaign_product_weight'] = array();
                    $j = 1;
                    while ($row_campaign_product_weight = mysqli_fetch_array($result_campaign_product_weight)) {
                        $record_product = array();
                        $record_product['record_counter'] = $j;
                        $record_product['c_weight'] = $row_campaign_product_weight['c_weight'];
                        $record_product['c_unit'] = $row_campaign_product_weight['c_unit'];

                        $record_product['f_p_brand_id'] = $row_campaign_product_weight['f_p_brand_id'];
                        $record_product['f_p_brand_name'] = $this->getBrand($row_campaign_product_weight['f_p_brand_id']);
                        $record_product['f_p_category_id'] = $row_campaign_product_weight['f_p_category_id'];
                        $record_product['f_p_category_name'] = $this->getCategory($row_campaign_product_weight['f_p_category_id']);
                        $record_product['f_product_id'] = $row_campaign_product_weight['f_product_id'];
                        $record_product['f_product_name'] = $this->getProduct($row_campaign_product_weight['f_product_id']);
                        if ($row_campaign_product_weight['f_p_measure_id'] != '') {
                            $record_product['f_product_variant_wt_quantity'] = $row_campaign_product_weight['f_p_quantity'];
                            $record_product['f_product_variant_unit'] = $row_campaign_product_weight['f_p_measure'];
                            $record_product['f_product_variant'] = $row_campaign_product_weight['f_p_quantity'] . " " . $row_campaign_product_weight['f_p_measure'];
                            $record_product['f_product_variant_unit_id'] = $row_campaign_product_weight['f_p_measure_id'];
                        } else
                            $campaign_array_temp['campaign_product_weight']['f_product_variant'] = "";


                        $campaign_array_temp['campaign_product_weight'][] = $record_product;
                        $j++;
                    }
                    break;
            }
            $data[] = $campaign_array_temp;
        }
        if (count($data) == 0)
            $data = [];

        return $data;
    }
	function getBrand($id) {
        $sql_brand = "SELECT `name` FROM `tbl_brand` WHERE id =" . $id;
		$result_brand = mysqli_query($this->local_connection,$sql_brand);
       // $result_brand = $this->executeQuery($sql_brand);
        $row_brand = mysqli_fetch_array($result_brand);
        return $row_brand['name'];
    }

    function getCategory($id) {
        $sql_category = "SELECT `categorynm` FROM `tbl_category` WHERE id =" . $id;
		$result_category = mysqli_query($this->local_connection,$sql_category);
        //$result_category = $this->executeQuery($sql_category);
        $row_category = mysqli_fetch_array($result_category);
        return $row_category['categorynm'];
    }

    function getProduct($id) {
        $sql_product = "SELECT `productname` FROM `tbl_product` WHERE id =" . $id;
		$result_product = mysqli_query($this->local_connection,$sql_product);
        //$result_product = $this->executeQuery($sql_product);
        $row_product = mysqli_fetch_array($result_product);
        return $row_product['productname'];
    }

    function getVariantUnit($id) {
        $sql_variant = "SELECT `unitname` FROM `tbl_units` WHERE id =" . $id;
		$result_variant = mysqli_query($this->local_connection,$sql_variant);
        //$result_variant = $this->executeQuery($sql_variant);
        $row_variant = mysqli_fetch_array($result_variant);
        return $row_variant['unitname'];
    }

    
	
	
}
?>