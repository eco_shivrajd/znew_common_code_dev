<?php
/***********************************************************
 * File Name	: stockManage.php
 ************************************************************/	
//include "../includes/commonManage.php";	
class stockManager
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
		//$this->commonObj 	= 	new commonManage($this->local_connection,$this->common_connection);
	}	
	
	public function getStockHistory() {	
	    $user_id = $_SESSION[SESSION_PREFIX.'user_id'];	
	    $user_role = $_SESSION[SESSION_PREFIX.'user_role'];
		//$sql1="SELECT  createdon, brand_name, category_name, product_name,product_price, product_variant1, variant1_unit_name, product_variant2,  variant2_unit_name, product_quantity FROM tbl_stock_management where added_by_userid='".$user_id."'";

  $sql1="(SELECT 'myself' as dummy_column,stock_status as dummy_column2,createdon, brand_name, category_name, product_name,product_price, product_variant1, variant1_unit_name, product_variant2, variant2_unit_name, product_quantity  FROM tbl_stock_management where added_by_userid='".$user_id."' )
UNION  
(SELECT 'my_chield_order' as dummy_column,'no' as dummy_column2,o.order_date as createdon,tb.name as brand_name,tc.categorynm as category_name,tp.productname as product_name, od.product_unit_cost as product_price, od.product_variant_weight1 as product_variant1, od.product_variant_unit1 as variant1_unit_name, od.product_variant_weight2 as product_variant2, od.product_variant_unit2 as variant2_unit_name, od.product_quantity FROM tbl_orders o LEFT JOIN tbl_order_details od ON o.id=od.order_id LEFT JOIN tbl_brand tb ON od.brand_id=tb.id LEFT JOIN tbl_category tc ON od.cat_id=tc.id LEFT JOIN tbl_product tp ON od.product_id=tp.id where o.order_to='".$user_id."' AND (od.order_status=4 OR od.order_status=2))
UNION
(SELECT 'my_cust_order' as dummy_column,'no' as dummy_column2,co.order_date as createdon,tb.name as brand_name,tc.categorynm as category_name,tp.productname as product_name, co.product_unit_cost as product_price, co.product_variant_weight1 as product_variant1, co.product_variant_unit1 as variant1_unit_name, co.product_variant_weight2 as product_variant2, co.product_variant_unit2 as variant2_unit_name, co.product_quantity FROM tbl_customer_orders_new co 
 LEFT JOIN tbl_brand tb ON co.brand_id=tb.id 
 LEFT JOIN tbl_category tc ON co.cat_id=tc.id 
 LEFT JOIN tbl_product tp ON co.product_id=tp.id 
 where co.order_to='".$user_id."' AND (co.order_status=4 OR co.order_status=2))
UNION  
(SELECT 'my_order' as dummy_column,'no1' as dummy_column2,o.order_date as createdon,tb.name as brand_name,tc.categorynm as category_name,tp.productname as product_name, od.product_unit_cost as product_price, od.product_variant_weight1 as product_variant1, od.product_variant_unit1 as variant1_unit_name, od.product_variant_weight2 as product_variant2, od.product_variant_unit2 as variant2_unit_name, od.product_quantity FROM tbl_orders o LEFT JOIN tbl_order_details od ON o.id=od.order_id LEFT JOIN tbl_brand tb ON od.brand_id=tb.id LEFT JOIN tbl_category tc ON od.cat_id=tc.id LEFT JOIN tbl_product tp ON od.product_id=tp.id where o.order_from='".$user_id."' AND (od.order_status=4 OR od.order_status=2))  ORDER BY createdon DESC";			

		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}	

    public function TotalStockInOutproduct()
    {
			$user_role = $_SESSION[SESSION_PREFIX.'user_role'];
			$user_id = $_SESSION[SESSION_PREFIX.'user_id'];
            $total_stock_in_quantity='';
            $total_stock_out_quantity='';
            $total_stock_assign_quantity='';
			switch($_SESSION[SESSION_PREFIX.'user_role'])
			{
			case "Admin":	
			//stock in product quantity	
			$total_stockin_product_bytsm="SELECT SUM(product_quantity) AS total_stock_in_quantity  FROM `tbl_stock_management` where  added_by_userid ='".$user_id."' ";
			$total_stockin_product_sql_bytsm  = mysqli_query($this->local_connection,$total_stockin_product_bytsm);
			$row_total_stockin_product_bytsm  = mysqli_fetch_assoc($total_stockin_product_sql_bytsm);
			$total_stock_in_quantity =  $row_total_stockin_product_bytsm['total_stock_in_quantity'];

             //stock Delivered product quantity	
			$total_stockout_product="SELECT o.id,sum(od.product_quantity) as total_stock_out_quantity FROM tbl_orders AS o LEFT JOIN tbl_order_details AS od ON od.order_id = o.id where o.order_to='".$user_id."' AND od.order_status =4";
			$total_stockout_product_sql 	=	mysqli_query($this->local_connection,$total_stockout_product);
			$row_total_stockout_product 	=	mysqli_fetch_assoc($total_stockout_product_sql);
			$total_stock_out_quantity_byot =  $row_total_stockout_product['total_stock_out_quantity'];
			$total_stockout_product_tocust="SELECT id,sum(product_quantity) as total_stock_out_quantity FROM tbl_customer_orders_new  where order_to='".$user_id."' AND (order_status =4)";// order_status =2 OR 
			$total_stockout_product_sql_tocust   = mysqli_query($this->local_connection,$total_stockout_product_tocust);
			$row_total_stockout_product_tocust   = mysqli_fetch_assoc($total_stockout_product_sql_tocust);
			$total_stock_out_quantity_tocust =  $row_total_stockout_product_tocust['total_stock_out_quantity'];
			$total_stock_out_quantity = $total_stock_out_quantity_byot + $total_stock_out_quantity_tocust;	

            //stock assigned for delivary product quantity
               $total_stockassign_product="SELECT o.id,sum(od.product_quantity) as total_stock_assign_quantity FROM tbl_orders AS o LEFT JOIN tbl_order_details AS od ON od.order_id = o.id where o.order_to='".$user_id."' AND od.order_status =2";
			$total_stockassign_product_sql 	=	mysqli_query($this->local_connection,$total_stockassign_product);
			$row_total_stockassign_product 	=	mysqli_fetch_assoc($total_stockassign_product_sql);
			$total_stock_assign_quantity_byot =  $row_total_stockassign_product['total_stock_assign_quantity'];
			$total_stockassign_product_tocust="SELECT id,sum(product_quantity) as total_stock_assign_quantity FROM tbl_customer_orders_new  where order_to='".$user_id."' AND ( order_status =2 )";
			$total_stockassign_product_sql_tocust   = mysqli_query($this->local_connection,$total_stockassign_product_tocust);
			$row_total_stockassign_product_tocust   = mysqli_fetch_assoc($total_stockassign_product_sql_tocust);
			$total_stock_assign_quantity_tocust =  $row_total_stockassign_product_tocust['total_stock_assign_quantity'];
			$total_stock_assign_quantity = $total_stock_assign_quantity_byot + $total_stock_assign_quantity_tocust;
			break;
			case "Distributor":
			case "Superstockist":
           //stock in product quantity	
			$total_stockin_product_bytsm="SELECT SUM(product_quantity) AS total_stock_in_quantity  FROM `tbl_stock_management` where  added_by_userid ='".$user_id."' ";
			$total_stockin_product_sql_bytsm  = mysqli_query($this->local_connection,$total_stockin_product_bytsm);
			$row_total_stockin_product_bytsm  = mysqli_fetch_assoc($total_stockin_product_sql_bytsm);
			$total_stock_in_quantity_bytsm =  $row_total_stockin_product_bytsm['total_stock_in_quantity'];
			$total_stockin_product="SELECT o.id,sum(od.product_quantity) as total_stock_in_quantity FROM tbl_orders AS o LEFT JOIN tbl_order_details AS od ON od.order_id = o.id where o.order_from='".$user_id."' AND ( od.order_status =4)";//od.order_status =2 OR
			$total_stockin_product_sql 	=	mysqli_query($this->local_connection,$total_stockin_product);
			$row_total_stockin_product 	=	mysqli_fetch_assoc($total_stockin_product_sql);
			$total_stock_in_quantity_byot =  $row_total_stockin_product['total_stock_in_quantity'];

			$total_stock_in_quantity =  $total_stock_in_quantity_bytsm + $total_stock_in_quantity_byot;

            //stock Delivered product quantity	
			$total_stockout_product="SELECT o.id,sum(od.product_quantity) as total_stock_out_quantity FROM tbl_orders AS o LEFT JOIN tbl_order_details AS od ON od.order_id = o.id where o.order_to='".$user_id."' AND ( od.order_status =4)";// od.order_status =2 OR
			$total_stockout_product_sql 	=	mysqli_query($this->local_connection,$total_stockout_product);
			$row_total_stockout_product 	=	mysqli_fetch_assoc($total_stockout_product_sql);
			$total_stock_out_quantity_byot =  $row_total_stockout_product['total_stock_out_quantity'];

			$total_stockout_product_tocust="SELECT id,sum(product_quantity) as total_stock_out_quantity FROM tbl_customer_orders_new  where order_to='".$user_id."' AND ( order_status =4)";//order_status =2 OR 
			$total_stockout_product_sql_tocust   = mysqli_query($this->local_connection,$total_stockout_product_tocust);
			$row_total_stockout_product_tocust   = mysqli_fetch_assoc($total_stockout_product_sql_tocust);
			$total_stock_out_quantity_tocust =  $row_total_stockout_product_tocust['total_stock_out_quantity'];

			$total_stock_out_quantity = $total_stock_out_quantity_byot + $total_stock_out_quantity_tocust;


			//stock assigned for delivary product quantity	
			$total_stockassign_product="SELECT o.id,sum(od.product_quantity) as total_stock_assign_quantity FROM tbl_orders AS o LEFT JOIN tbl_order_details AS od ON od.order_id = o.id where o.order_to='".$user_id."' AND ( od.order_status =2)";// od.order_status =2 OR
			$total_stockassign_product_sql 	=	mysqli_query($this->local_connection,$total_stockassign_product);
			$row_total_stockassign_product 	=	mysqli_fetch_assoc($total_stockassign_product_sql);
			$total_stock_assign_quantity_byot =  $row_total_stockassign_product['total_stock_assign_quantity'];

			$total_stockassign_product_tocust="SELECT id,sum(product_quantity) as total_stock_assign_quantity FROM tbl_customer_orders_new  where order_to='".$user_id."' AND ( order_status =2)";//order_status =2 OR 
			$total_stockassign_product_sql_tocust   = mysqli_query($this->local_connection,$total_stockassign_product_tocust);
			$row_total_stockassign_product_tocust   = mysqli_fetch_assoc($total_stockassign_product_sql_tocust);
			$total_stock_assign_quantity_tocust =  $row_total_stockassign_product_tocust['total_stock_assign_quantity'];

			$total_stock_assign_quantity = $total_stock_assign_quantity_byot + $total_stock_assign_quantity_tocust;

			break;
			}
           $records['total_stock_assign_quantity'] = $total_stock_assign_quantity;
           $records['total_stock_out_quantity'] = $total_stock_out_quantity;
           $records['total_stock_in_quantity'] =$total_stock_in_quantity;
		   return $records;
		 //  exit();
    }
	public function TotalStockInOutproduct_stock_report(){
		//extract($_POST);
		$total_stock_out_quantity_byot=array();
		$total_stock_out_quantity_tocust=array();
		$user_role = $_SESSION[SESSION_PREFIX.'user_role'];
		$user_id = $_SESSION[SESSION_PREFIX.'user_id'];
		
		$total_stock_in_quantity='';
		$total_stock_out_quantity='';
		
		$sql = "SELECT distinct id as userid,firstname,user_type,user_role FROM `tbl_user` 
				where  find_in_set('$user_id',parent_ids) <> 0
				and id!='1' 
				and (user_role = 'Superstockist' OR user_role = 'Distributor') 
				order by user_type";
		$result1 = mysqli_query($this->local_connection, $sql);
			
		while ($row = mysqli_fetch_array($result1)){ 
			$userids[]=$row['userid'];
			$usernames[$row['userid']]=$row['firstname'];
			$usertypes[$row['userid']]=$row['user_type'];
		}
		$total_stockin_product_bytsm="SELECT added_by_userid as userid,
		SUM(product_quantity) AS total_stock_in_quantity  
		FROM `tbl_stock_management` where  added_by_userid  IN (".implode(',',$userids).") group by added_by_userid";
		
		$total_stockin_product_sql_bytsm  = mysqli_query($this->local_connection,$total_stockin_product_bytsm);
		$total_stock_in_quantity_bytsm=array();
		$total_stock_in_quantity_byot=array();
		while ($row_bytsm = mysqli_fetch_array($total_stockin_product_sql_bytsm)){ 
			$userids_key=$row_bytsm['userid'];
			$total_stock_in_quantity_bytsm[$userids_key]['total_stock_in_quantity'] =  $row_bytsm['total_stock_in_quantity'];
		}
		
		$total_stockin_product="SELECT o.order_from,
		sum(od.product_quantity) as total_stock_in_quantity 
		FROM tbl_orders AS o LEFT JOIN tbl_order_details AS od ON od.order_id = o.id 
		where o.order_from IN (".implode(',',$userids).") AND ( od.order_status =2 OR od.order_status =4) group by order_from";
		
		$total_stockin_product_sql  = mysqli_query($this->local_connection,$total_stockin_product);
		while ($row_quantity_byot = mysqli_fetch_array($total_stockin_product_sql)){ 
			$userids_key=$row_quantity_byot['order_from'];
			$total_stock_in_quantity_byot[$userids_key]['total_stock_in_quantity'] =  $row_quantity_byot['total_stock_in_quantity'];
		}

		//stock OUT product quantity	
		$total_stockout_product="SELECT o.order_to,
		sum(od.product_quantity) as total_stock_out_quantity 
		FROM tbl_orders AS o LEFT JOIN tbl_order_details AS od ON od.order_id = o.id 
		where o.order_to  IN (".implode(',',$userids).") AND ( od.order_status =2 OR od.order_status =4) group by o.order_to";
		
		$total_stockout_product_sql  = mysqli_query($this->local_connection,$total_stockout_product);
		while ($row_quantity_byot = mysqli_fetch_array($total_stockout_product_sql)){ 
			$userids_key=$row_quantity_byot['order_to'];
			$total_stock_out_quantity_byot[$userids_key]['total_stock_out_quantity'] =  $row_quantity_byot['total_stock_out_quantity'];
		}

		$total_stockout_product_tocust="SELECT order_to,
		sum(product_quantity) as total_stock_out_quantity 
		FROM tbl_customer_orders_new  
		where order_to IN (".implode(',',$userids).") AND ( order_status =2 OR order_status =4) group by order_to ";
		
		$total_stock_out_quantity_tocust_sql  = mysqli_query($this->local_connection,$total_stockout_product_tocust);
		while ($row_quantity_tocust = mysqli_fetch_array($total_stock_out_quantity_tocust_sql)){ 
			$userids_key=$row_quantity_tocust['order_to'];
			$total_stock_out_quantity_tocust[$userids_key]['total_stock_out_quantity'] =  $row_quantity_tocust['total_stock_out_quantity'];
		}
		foreach($userids as $key=>$value){
			if (array_key_exists($value,$total_stock_in_quantity_bytsm)){
				$total_stock_in_quantity_bytsm_qty=$total_stock_in_quantity_bytsm[$value]['total_stock_in_quantity'];
			}else{$total_stock_in_quantity_bytsm_qty=0;}
			if (array_key_exists($value,$total_stock_in_quantity_byot)){
				$total_stock_in_quantity_byot_qty=$total_stock_in_quantity_byot[$value]['total_stock_in_quantity'];
			}else{$total_stock_in_quantity_byot_qty=0;}
			$total_stock_quantity[$value]['in'] =  $total_stock_in_quantity_bytsm_qty+ $total_stock_in_quantity_byot_qty;
			
			
			if (array_key_exists($value,$total_stock_out_quantity_byot)){
				$total_stock_out_quantity_byot_djgfjs=$total_stock_out_quantity_byot[$value]['total_stock_out_quantity'];
			}else{$total_stock_out_quantity_byot_djgfjs=0;}
			if (array_key_exists($value,$total_stock_out_quantity_tocust)){
				$total_stock_out_quantity_tocust_djgfjs=$total_stock_out_quantity_tocust[$value]['total_stock_out_quantity'];
			}else{$total_stock_out_quantity_tocust_djgfjs=0;}
			$total_stock_quantity[$value]['out'] =  $total_stock_out_quantity_byot_djgfjs+ $total_stock_out_quantity_tocust_djgfjs;
			$total_stock_quantity[$value]['username'] =  $usernames[$value];
			$total_stock_quantity[$value]['user_type'] =  $usertypes[$value];
		}

		$records = $total_stock_quantity;
		return $records;
    }
	//get available stock of user
	public function get_available_stock_of_user(){
		//extract($_POST);
		$user_role = $_SESSION[SESSION_PREFIX.'user_role'];
		$user_id = $_SESSION[SESSION_PREFIX.'user_id'];
		//$total_stock_in_quantity=0;
		//this code is used for stock in qty
		$sqlvarunit7 = "SELECT sum(product_quantity) as total_stock_in_quantity,product_varient_id 
		FROM tbl_stock_management  
		where  added_by_userid='".$user_id."' group by product_varient_id";
		$resultvarunit7= mysqli_query($this->local_connection, $sqlvarunit7);
		$total_stock_by_user=array();
		$total_stock_by_parent=array();
		while ($rowvarunit7 = mysqli_fetch_array($resultvarunit7))  {
			$total_stock_by_user[$rowvarunit7['product_varient_id']] = $rowvarunit7['total_stock_in_quantity']>0?$rowvarunit7['total_stock_in_quantity']:0;
		}
		//echo "total_stock_by_user<pre>";print_r($total_stock_by_user);echo "<br>";
		if ($user_role!='Admin'){	  
		  $sqlvarunit5 = "SELECT sum(od.product_quantity) as total_stock_in_quantity ,od.product_variant_id
		  FROM tbl_orders AS o 
		  LEFT JOIN tbl_order_details AS od ON od.order_id = o.id 
		  where o.order_from='".$user_id."'  AND  od.order_status =4 group by od.product_variant_id";
		  $resultvarunit5= mysqli_query($this->local_connection, $sqlvarunit5);
		  while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) {
			$total_stock_by_parent[$rowvarunit5['product_variant_id']] = $rowvarunit5['total_stock_in_quantity']>0?$rowvarunit5['total_stock_in_quantity']:0;
		  }
		}
		//echo "total_stock_by_parent<pre>";print_r($total_stock_by_parent);echo "<br>";
		$total_stock_in_all_prodvar = array();
		foreach (array_keys($total_stock_by_user + $total_stock_by_parent) as $key) {
			$total_stock_in_all_prodvar[$key] = (isset($total_stock_by_user[$key]) ? $total_stock_by_user[$key] : 0) + (isset($total_stock_by_parent[$key]) ? $total_stock_by_parent[$key] : 0);
		}
		//echo "total_stock_in_all_prodvar<pre>";print_r($total_stock_in_all_prodvar);echo "<br>";
		//total stock out 
		
		//$total_stock_out_quantity=0;
		$sqlvarunit5 = "SELECT sum(od.product_quantity) as total_stock_out_quantity,od.product_variant_id 
						FROM tbl_orders AS o 
						LEFT JOIN tbl_order_details AS od ON od.order_id = o.id 
						where o.order_to='".$user_id."'  
						AND ( od.order_status =2 OR od.order_status =4) group by od.product_variant_id ";
		$resultvarunit5= mysqli_query($this->local_connection, $sqlvarunit5);
		$total_stock_out_quantity_tochild=array();
		$total_stock_out_quantity_tocust=array();
		while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) 
		{
		   $total_stock_out_quantity_tochild[$rowvarunit5['product_variant_id']] = $rowvarunit5['total_stock_out_quantity'];
		}
		//echo "total_stock_out_quantity_tochild<pre>";print_r($total_stock_out_quantity_tochild);echo "<br>";
		$sqlvarunit5 = "SELECT sum(product_quantity) as total_stock_out_quantity,product_variant_id 
							FROM tbl_customer_orders_new  
							where order_to='".$user_id."' 
							AND ( order_status =2 OR order_status =4) group by product_variant_id ";
		$resultvarunit5= mysqli_query($this->local_connection, $sqlvarunit5);
		while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) 
		{
		   $total_stock_out_quantity_tocust[$rowvarunit5['product_variant_id']] = $rowvarunit5['total_stock_out_quantity'];
		}
		//echo "total_stock_out_quantity_tocust<pre>";print_r($total_stock_out_quantity_tocust);echo "<br>";
//total_stock_out_quantity
		//$total_stock_out_quantity = $total_stock_out_quantity_tochild + $total_stock_out_quantity_tocust;
		$total_stock_out_all_prodvar = array();
		foreach (array_keys($total_stock_out_quantity_tochild + $total_stock_out_quantity_tocust) as $key) {
			$total_stock_out_all_prodvar[$key] = (isset($total_stock_out_quantity_tochild[$key]) ? $total_stock_out_quantity_tochild[$key] : 0) + (isset($total_stock_out_quantity_tocust[$key]) ? $total_stock_out_quantity_tocust[$key] : 0);
			//print_r($total_stock_out_all_prodvar[$key]);
		}
		//echo "total_stock_out_all_prodvar<pre>";print_r($total_stock_out_all_prodvar);echo "<br>";
		$total_available_stock_all_prodvar = array();
		foreach (array_keys($total_stock_in_all_prodvar + $total_stock_out_all_prodvar) as $key) {
			$total_available_stock_all_prodvar[$key] = (isset($total_stock_in_all_prodvar[$key]) ? $total_stock_in_all_prodvar[$key] : 0) - (isset($total_stock_out_all_prodvar[$key]) ? $total_stock_out_all_prodvar[$key] : 0);
			//print_r($total_available_stock_all_prodvar[$key]);
		}
		//echo "total_available_stock_all_prodvar<pre>";print_r($total_available_stock_all_prodvar);echo "<br>";
		return $total_available_stock_all_prodvar;
	}
}
?>