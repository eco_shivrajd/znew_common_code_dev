<?php
include ("../../includes/config.php");
	$where="";
	switch($_SESSION[SESSION_PREFIX.'user_role']){
	    case "Admin":				
			//$where.=" ";
	        $where.=" where find_in_set('1',u.parent_ids) <> 0 ";
		break;
		case "Superstockist":			
			$where.=" where find_in_set('".$_SESSION[SESSION_PREFIX."user_id"]."',u.parent_ids) <> 0 ";
		break;
		case "Distributor":
			$where.=" where find_in_set('".$_SESSION[SESSION_PREFIX."user_id"]."',u.parent_ids) <> 0 ";				
		break;
	}
extract($_POST);
$condnsearch="";		
   switch($selTest)
	{
		case 1:
			$condnsearch.="AND  yearweek(tada.date_tada) = yearweek(curdate())";
			break;
		case 2:
			$condnsearch.="AND  date_format(tada.date_tada, '%m')=date_format(now(), '%m')";
		break;
		case 3:
			if($todate!="")
				$todat=$todate;
			else
				$todat=date("d-m-Y");
			$condnsearch.="AND  (date_format(tada.date_tada, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(tada.date_tada, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
		break;
		case 4:
			$date = date('d-m-Y');
			$condnsearch.="AND  date_format(tada.date_tada, '%d-%m-%Y') = '".$date."' ";
		break;
		default:
			$condnsearch.="";
		break;
	}	
      $sql="SELECT u.firstname,u.id as spid,tada.* ,tmt.van_type
		FROM tbl_sp_tadabill tada  
		left join tbl_user  u on tada.userid=u.id
		left join tbl_mode_transe  tmt on tada.mode_of_transe=tmt.id ".$where." ".$condnsearch."  order by date_tada ,firstname";
		//date_format(TUL.tdate, '%d-%m-%Y') = '".$frmdate."' ";
				$result = mysqli_query($con,$sql);
				$record_count=mysqli_num_rows($result);
	
				
?>
<? if($_GET["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<? } ?>
<div id="loader_div1"></div>

	
	<div class="portlet-body">
		<div class="table-responsive" id="dvtblResonsive">
			<table class="table table-bordered" id="report_table">
				<?php  if($record_count > 0) { ?>
				<thead>
					<tr>
						<th  valign="top" style="text-align:center" rowspan='2'><b>Date</b></th>
						<th valign="top" style="text-align:center" rowspan='2' ><b>Name</b></th>
						<th valign="top" style="text-align:center" rowspan='2'><b>Mode Of Transport/<br>Rate Per Km</b></th>
						<th  style="text-align:center" ><b>Actual </b></th>
						<th  style="text-align:center"><b>Google </b></th>
						<th  style="text-align:center" ><b>Food</b></th>
						<th  style="text-align:center"><b>Other</b></th>
						<th  style="text-align:center" colspan='2'><b>Total ( In Rs )</b></th>
						<th  valign="top" style="text-align:center" rowspan='2'><b>Expense Bill</b></th>
					</tr>
					<tr>
						
						<th  style="text-align:center" colspan='2'><b>Distance Travelled ( In Km )</b></th>
					
						<th  style="text-align:center" colspan='2'><b>Expenses ( In Rs )</b></th>
						
						<th  style="text-align:center"><b>Actual </b></th>
						<th  style="text-align:center"><b>Google </b></th>
						
					</tr>
				</thead>
				<tbody>
				<?php 
				$total_distance_covered=0;
				$total_google_distance=0;
				$total_food=0;
				$total_other=0;
				$total_actual_row=0;
				$total_google_row=0;
				
				while($row = mysqli_fetch_array($result)){ 
				$total_distance_covered=$total_distance_covered+$row['distance_covered'];
				$total_google_distance=$total_google_distance+$row['google_distance'];
				$total_food=$total_food+$row['food'];
				$total_other=$total_other+$row['other'];
				$total_actual_row=$total_actual_row+($row['Current_rate_mot']*$row['distance_covered'])+$row['food']+$row['other'];
				$total_google_row=$total_google_row+($row['Current_rate_mot']*$row['google_distance'])+$row['food']+$row['other'];
				?>
					<tr>
					
						<td><?php echo date('d-m-Y',strtotime($row["date_tada"]));?></td>
						<td><?php echo $row['firstname'];?></td>
						<td><?php echo $row['van_type']."/".$row['Current_rate_mot'];?></td>		
						<td align="right"><?php echo $row['distance_covered'];?></td>	
						<td align="right"><?php echo $row['google_distance'];?></td>
						<td align="right"><?php echo $row['food'];?></td>	
						<td align="right"><?php echo $row['other'];?></td>
						<td align="right"><?php $total1=($row['Current_rate_mot']*$row['distance_covered'])+$row['food']+$row['other'];
						echo $total1;?></td>
						<td align="right"><?php $total2=($row['Current_rate_mot']*$row['google_distance'])+$row['food']+$row['other'];
						echo $total2;?></td>
						<td>
							<?php
							$dir="../../uploads/".date('d-m-Y',strtotime($row["date_tada"]))."/".$row['spid']."/".$row["id"]."/";
						
								if (is_dir($dir)) {	
							$images = glob($dir."*.png");
							foreach($images as $key=>$image) {								
								echo '<img id="myImg'.date('dmY',strtotime($row["date_tada"])).''.$row['spid'].''.$key.'" onclick="changeItnew(this.id)" src="'.$image.'"  width="50px"/> ';
								
							} } else {  } ?>
						</td>
					</tr>
                                <?php	} ?>
					<tr>
						<td align="right" colspan="3"><b>Total</b></td>						
						<td align="right"><b><?php echo $total_distance_covered;?></b></td>	
						<td align="right"><b><?php echo $total_google_distance;?></b></td>
						<td align="right"><b><?php echo $total_food;?></b></td>	
						<td align="right"><b><?php echo $total_other;?></b></td>
						<td align="right"><b><?php echo $total_actual_row;?></b></td>
						<td align="right"><b><?php echo $total_google_row; ?></b></td>
						<td>
							
						</td>
					</tr>
								<?php }else{
					echo "<tr><td>No Record available.</td></tr>";
				}?>
				</tbody>
				
			</table>
		</div>
	</div>
