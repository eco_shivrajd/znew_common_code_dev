<?php 
include ("../../includes/config.php");
include "../includes/userManage.php";
include "../includes/shopManage.php";
$userObj = new userManager($con, $conmain);
$shopObj = new shopManager($con, $conmain);
$user_type = $_SESSION[SESSION_PREFIX . 'user_type'];
$route_id = $_POST['route_id'];
//$route_details = $shopObj->getRoutePopup($route_id);
$route ="SELECT rd.lattitude,rd.longitude,r.route_name,r.id as route_id,s.name as state_name,c.name as city_name,a.suburbnm as area_name,sa.subarea_name
FROM tbl_route_details rd 
LEFT JOIN tbl_state s ON s.id = rd.state_id 
LEFT JOIN tbl_city c ON c.id = rd.city_id
LEFT JOIN tbl_area a ON a.id = rd.area_id 
LEFT JOIN tbl_subarea sa ON sa.subarea_id = rd.subarea_id
LEFT JOIN tbl_route r ON r.id = rd.route_id 
WHERE rd.route_id='".$route_id."' AND rd.lattitude > 0 ORDER BY rd.id ASC";
$route_details = mysqli_query($con,$route);
    $array_route = array();
	$j=0;
	while($row_route = mysqli_fetch_array($route_details)) 
	{ 
		//var_dump($row_route);
			$array_route[$j]['lat']=$row_route["lattitude"];
		    $array_route[$j]['lng']=$row_route["longitude"];
		    $array_route[$j]['title']=$row_route["city_name"];	
            $route_name=$row_route["route_name"];	
		$j++;
	}
//var_dump($array_route);
//echo "<pre>";print_r($array_route);die();
//exit();
?>
<div class="modal-header">



<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel">    
    Route : <?php echo $route_name;?>
</h4>	   
</div>
<div class="modal-body" style="padding-bottom: 5px !important;" id="divOrderPrintArea">
<div class="row">
<div class="col-md-12">   
	<div class="portlet box blue-steel">
		<div class="portlet-title ">
			<div class="caption printHeading">
			
			</div>                          
		</div>
		<div class="portlet-body" style="height:500px;">
		<? 
		$total_count_route=count($array_route);

		//var_dump($array_route);
	//	exit();
		if($total_count_route==0)
		{
		?>
		<div class="form-group">
			<div class="col-md-12">				
				<div  id="dvMap" style="min-height:480px">
				No Record available.
				</div>
			</div>
		</div>
		<?php exit; }else{ ?>
		<div class="form-group">
			<div class="col-md-12">
				
				 	<div id="dvMap" style="min-height:480px"></div>
			</div>
		</div>
		</div>
		<?php } ?>
</div>
</div>
</div>
</div>
</div>

<script>
	var markers=<?php echo json_encode($array_route); ?>;
	console.log(markers);

	function initMap() {
		drawingRoute();		
      }
	  function drawingRoute(){
      // window.onload = function () {
       	//console.log('hidfg');
            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 4,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
            var infoWindow = new google.maps.InfoWindow();
            var lat_lng = new Array();
            var latlngbounds = new google.maps.LatLngBounds();
            for (i = 0; i < markers.length; i++) {
                var data = markers[i];
               // console.log('hi');
               // console.log(data);
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                lat_lng.push(myLatlng);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: data.title
                });
                latlngbounds.extend(marker.position);
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent(data.description);
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }
            map.setCenter(latlngbounds.getCenter());
            map.fitBounds(latlngbounds);

            //***********ROUTING****************//

            //Intialize the Path Array
            var path = new google.maps.MVCArray();

            //Intialize the Direction Service
            var service = new google.maps.DirectionsService();

            //Set the Path Stroke Color
            var poly = new google.maps.Polyline({ map: map, strokeColor: '#4986E7' });

            //Loop and Draw Path Route between the Points on MAP
            for (var i = 0; i < lat_lng.length; i++) {
                if ((i + 1) < lat_lng.length) {
                    var src = lat_lng[i];
                    var des = lat_lng[i + 1];
                    path.push(src);
                    poly.setPath(path);
                    service.route({
                        origin: src,
                        destination: des,
                        travelMode: google.maps.DirectionsTravelMode.DRIVING
                    }, function (result, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                                path.push(result.routes[0].overview_path[i]);
                            }
                        }
                    });
                }
            }


        }
    </script>


 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDMpdO43txdg_zyovvZm9i1tMMFIQTKTU&callback=initMap"
        async defer></script>