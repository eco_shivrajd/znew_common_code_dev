<!-- BEGIN HEADER -->
<?php 
include "../includes/header.php";
error_reporting(E_ALL);
if($_SESSION[SESSION_PREFIX.'user_type']=="Distributor") {
	header("location:../logout.php");
}  
include "../includes/commonuserManage.php";	
$commonuserObj 	= 	new commonuserManager($con,$conmain);

if(isset($_POST['hidbtnsubmit']))
{   
   //print"<pre>";print_r($_POST);
  // exit();
    $user_type=$_POST['user_type'];    
    /* Local user section */
    $common_user_id = $commonuserObj->addCommonUserForInstances($user_type);
    //$common_user_id = 14;
    if(isset($common_user_id) && $common_user_id!='')
    {
        $userid_local = $commonuserObj->addAllLocalUserForInstances($user_type,$common_user_id);          
    }
    else
    {
        echo '<script>alert("Something Wrong..!!");</script>';
    //  $userObj->addCommonUserCompanyDetails($userid_local);   
    }
    /*$email      =   fnEncodeString($_POST['email']);
    if($email != ''){
        $userObj->sendUserCreationEmail();
    }*/
    
    echo '<script>alert("User added successfully.");location.href="common-user.php";</script>';
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	 $activeMainMenu = "ManageSupplyChain"; $activeMenu = "Common User";
    include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			common User
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="distributors.php">common User</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New common User</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New common User
							</div>
							
						</div>
						<div class="portlet-body">
						 <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>                       
						                       
                          
						<form name="addform" id="addform" class="form-horizontal" role="form" data-parsley-validate="" method="post" action="">
						<? if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { ?>

                         <div class="form-group">
						  <label class="col-md-3">User Type:<span class="mandatory">*</span></label>

						  <div class="col-md-4">
							<select name="user_type" class="form-control"> 
							<option value="Superstockist">Superstockist</option>
							<option value="Distributor">Stockist</option>
							 <option value="SalesPerson">Sales Person</option>
                                <option value="DeliveryPerson">DeliveryPerson</option>
                                <option value="DeliveryChannelPerson">Delivery Channel Person</option>
							</select>
						  </div>
						</div>  
						

                        <div class="form-group" >
						  <label class="col-md-3">Manufacturer:<span class="mandatory">*</span></label>

						  <div class="col-md-4">
							<select name="manufacturer_id[]" class="form-control" multiple> 
							<?php							
							$result1 = $commonuserObj->getManufacturer();						
							while($row = mysqli_fetch_array($result1))
							{
								$manufacturer_id = $row['id'];
								echo "<option value='$manufacturer_id'>" . fnStringToHTML($row['clientnm']) . "</option>";
							}
							?>
							</select>
						  </div>
						</div>  


						<? } else { ?>
							<input type="hidden" name="assign" id="assign" value="<?=$_SESSION[SESSION_PREFIX.'user_id']?>">
						<? }?>				 
						<?php $page_to_add = 'stockist'; //include "userAddCommEle.php";	//form common element file with javascript validation ?> 




						 <div class="form-group">
              <label class="col-md-3">Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text"
				placeholder="Enter Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"				
				name="firstname" class="form-control">
              </div>
            </div>
             
			<div class="form-group">
              <label class="col-md-3">Username:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" id="username"
				placeholder="Enter Username"
				data-parsley-minlength="6"
				data-parsley-minlength-message="Username should be minimum 6 characters without blank spaces"          
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"          
				data-parsley-type-message="Please enter Username, blank spaces are not allowed"		   
				data-parsley-required-message="Please enter Username, blank spaces are not allowed"
				data-parsley-trigger="change"
				data-parsley-required="true"
				data-parsley-pattern="/^\S*$/" 
				data-parsley-error-message="Username should be 6-50 characters without blank spaces"
				name="username" class="form-control"><span id="user-availability-status"></span>
              </div>
            </div>
			<div class="form-group">
              <label class="col-md-3">Password:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="password" id="password"
				placeholder="Enter Password"			   
				data-parsley-minlength="6"
				data-parsley-minlength-message="Password should be minimum 6 characters without blank spaces"   
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"   
				data-parsley-type-message="Password should be minimum 6 characters without blank spaces"		   
				data-parsley-required-message="Please enter Password"
				data-parsley-trigger="change"
				data-parsley-required="true"
				data-parsley-pattern="/^\S*$/" 
				data-parsley-error-message="Password should be 6-50 characters without blank spaces"
				name="password" class="form-control placeholder-no-fix">
              </div>
            </div>
           <div class="form-group">
              <label class="col-md-3">Confirm Password:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="password" id="c_password"
				placeholder="Enter Confirm Password"
				data-parsley-trigger="change"	
				data-parsley-equalto="#password"
				data-parsley-equalto-message="Password does not match with confirm password"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter confirm password"		
				name="c_password" class="form-control"><span id="user-availability-status"></span>
              </div>
            </div>
			<div class="form-group">
				<label class="col-md-3">Address:</label>
				<div class="col-md-4">
					<textarea name="address"
					placeholder="Enter Address"
					data-parsley-trigger="change"
					data-parsley-maxlength="200"
					data-parsley-maxlength-message="Only 200 characters are allowed"							
					rows="4" class="form-control" ></textarea>
				</div>
			</div>
			<?php if($page_to_add != 'sales_person'){ ?>
			<div id="working_area_top">
			<h4>Assign <?php if($page_to_add == 'stockist'){?>Taluka<?php }else{ ?>District<?php }?></h4>
			</div>
			<div class="form-group">
				<label class="col-md-3">State:</label>
				<div class="col-md-4">
				<select name="state"  id="state"
				class="form-control" onChange="fnShowCity(this.value)">
				<option selected disabled>-Select-</option>
				<?php
				$sql="SELECT id,name FROM tbl_state where country_id=101 order by name";
				$result = mysqli_query($con,$sql);
				while($row = mysqli_fetch_array($result))
				{
					$cat_id=$row['id'];
					$selected_state = '';
					if($default_state == $row['id'])
						$selected_state = 'selected';
					echo "<option value='$cat_id' $selected_state>" . $row['name'] . "</option>";
				} ?>
				</select>
				</div>
			</div>			
			<div class="form-group" id="city_div" <?php if($page_to_add != 'stockist'){echo 'style="display:none;"';} ?>>
			  <label class="col-md-3">District:</label>
			  <div class="col-md-4" id="div_select_city">
			  <?php if($page_to_add == 'stockist'){ ?>
				 <select name="city" id="city" 				
				class="form-control" onchange="FnGetSuburbDropDown(this)">
				<?php
				$sql="SELECT id,name FROM tbl_city where state_id=".$default_state." order by name";
				$result = mysqli_query($con,$sql);
				while($row = mysqli_fetch_array($result))
				{
					$cat_id=$row['id'];
					$selected_city = '';
					if($default_city == $row['id'])
						$selected_city = 'selected';
					echo "<option value='$cat_id' $selected_city>" . $row['name'] . "</option>";
				} ?>
				</select>
			  <?php }else{ ?>
			  <select name="city" id="city" 				
				class="form-control">
				<option selected value="">-Select-</option>										
				</select>
				 <?php } ?>
			  </div>
			</div>
            <div class="form-group" id="area_div" <?php if($page_to_add != 'stockist'){echo 'style="display:none;"';} ?>>
			  <label class="col-md-3">Taluka:</label>
			  <div class="col-md-4" id="div_select_area">
			  <?php if($page_to_add == 'stockist'){ ?>
				 <select name="area" id="area" data-parsley-trigger="change" class="form-control">
				<?php
				$sql="SELECT id,suburbnm FROM tbl_surb where cityid=$default_city order by suburbnm";
				$result = mysqli_query($con,$sql);
				$row_count = mysqli_num_rows($result);
				if($row_count > 0){
					while($row = mysqli_fetch_array($result))
					{
						$cat_id=$row['id'];
						$selected_area = '';
						if($default_area == $row['id'])
							$selected_area = 'selected';
						echo "<option value='$cat_id' $selected_area>" . $row['suburbnm'] . "</option>";
					}
				}else{
					echo "<option selected value=''>-Select-</option>";
				} ?>
				</select>
			  <?php }else{ ?>
			  <select name="area" id="area" data-parsley-trigger="change" class="form-control">
				<option selected value="">-Select-</option>									
				</select>
			  <?php } ?>
			  </div>
			</div>					
			<div class="form-group" id="subarea_div" style="display:none;">
			  <label class="col-md-3">Subarea:</label>
			  <div class="col-md-4" id="div_select_subarea">
			  <select name="subarea" id="subarea" data-parsley-trigger="change" class="form-control">
				<option selected value="">-Select-</option>									
				</select>
			  </div>
			</div>
			<div id="working_area_bottom">				
			</div>
			<?php } ?>
			 <div class="form-group">
              <label class="col-md-3">Email:</label>

              <div class="col-md-4">
                <input type="text" id="email"
				placeholder="Enter E-mail"
				data-parsley-maxlength="100"
				data-parsley-maxlength-message="Only 100 characters are allowed"
				data-parsley-type="email"
				data-parsley-type-message="Please enter valid e-mail" 
				data-parsley-trigger="change"
				name="email" class="form-control"><span id="user-availability-status"></span>
              </div>
            </div>
			<div class="form-group">
				  <label class="col-md-3">Mobile Number:</label>

				  <div class="col-md-4">
					<input type="text"  name="mobile"
					placeholder="Enter Mobile Number"
					data-parsley-trigger="change"					
					data-parsley-minlength="10"
					data-parsley-maxlength="15"
					data-parsley-maxlength-message="Only 15 characters are allowed"
					data-parsley-pattern="^(?!\s)[0-9]*$"
					data-parsley-pattern-message="Please enter numbers only"
					class="form-control" value="">
				  </div>
				</div>
				
				<div class="form-group">
              <label class="col-md-3">GST Number:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text"
				placeholder="Enter GST Number"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter GST Number"
				data-parsley-maxlength="15"
				data-parsley-maxlength-message="Only 15 characters are allowed"	
				data-parsley-minlength="15"
				data-parsley-minlength-message="Not Valid GST Number"	
				name="gstnumber" class="form-control">
              </div> 
            </div>
				
				<input type="hidden" name="page_to_add" id="page_to_add" value="<?=$page_to_add;?>">   
					   
						<div class="form-group">
						  <div class="col-md-4 col-md-offset-3">							
							<input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
							<input type="hidden" name="hidAction" id="hidAction" value="common-user-add.php">
							<button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button>
							<a href="distributors.php" class="btn btn-primary">Cancel</a>
						  </div>
						</div><!-- /.form-group -->
					  </form>                              
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->



<script>
function showStateCity(obj){
	var ss_id = obj.value;
	jQuery.ajax({
			url: "../includes/ajax_getUserAssLocation.php",
			data:'ss_id='+ss_id,
			type: "POST",
			async:false,
			success:function(data){		
				var user_record = JSON.parse(data);
				if(user_record!=0) {
					var state = user_record.state;
					var city = user_record.city;
					$("#state").val(state);
					setTimeout(
					function() 
					{	
						fnShowCity(state);
						setTimeout(
						function() 
						{
							$("#city").val(city);	
						}, 1800);
					}, 600);
					setTimeout(function() 
					{
						FnGetSuburbDropDown($("#city").get(0));						
					}, 2800);
					
				} 
			},
			error:function (){}
		});
}
</script>
<script type="text/javascript">
	function showDiv(elem)
	{
      if(elem.value == 'Distributor')
      {
      	document.getElementById('hidden_div').style.display = "block";
      }
       if(elem.value == 'Superstockist')
      {
      	document.getElementById('hidden_div').style.display = "none";
      }
      
    }
</script>
<script>
function calculate_data_count(element_value){
	element_value = element_value.toString();
	var element_arr = element_value.split(',');	
	return element_arr.length;
}
function setSelectNoValue(div,select_element){
	var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled value="">-Select-</option></select>';
	document.getElementById(div).innerHTML	=	select_selement_section;
}
function fnShowCity(id_value) {	
	$("#city_div").show();	
	$("#area").html('<option value="">-Select-</option>');	
	$("#subarea").html('<option value="">-Select-</option>');	
	var page_to_add = $("#page_to_add").val();
	var param = '';
	if(page_to_add == 'superstockist')
		param = "&nofunction=nofunction";
	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city&mandatory=mandatory"+param;
	CallAJAX(url,"div_select_city");	
}
function FnGetSuburbDropDown(id) {
	$("#area_div").show();	
	$("#subarea").html('<option value="">-Select-</option>');	
	var page_to_add = $("#page_to_add").val();
	var param = '';
	if(page_to_add == 'stockist')
		param = "&nofunction=nofunction";
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&multiple=multiple&function_name=FnGetSubareaDropDown"+param;
	CallAJAX(url,"div_select_area");
}
function FnGetSubareaDropDown(id) {	
	var suburb_str = $("#area").val();	
	var suburb_arr_count = calculate_data_count(suburb_str);
	if(suburb_arr_count == 1){//If single city selected then only show its related subarea	
		$("#subarea_div").show();	
		var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea&multiple=multiple";
		CallAJAX(url,"div_select_subarea");
	}else if(suburb_arr_count > 1){
		$("#subarea_div").show();	
		var multiple_id = suburb_str.join(", ");
		var url = "getSubareaDropdown.php?multiple_id="+multiple_id+"&select_name_id=subarea&multiple=multiple";
		CallAJAX(url,"div_select_subarea");
	}else{
		setSelectNoValue("div_select_subarea", "subarea");
	}		
}

function checkAvailability() {
	$('#addform').parsley().validate();
	var email = $("#email").val();
	var username = $("#username").val();
	var validate = 1;
	if(username != '')
	{
		validate = 1;
		jQuery.ajax({
			url: "../includes/checkUserAvailable.php",
			data:'validation_field=username&username='+username,
			type: "POST",
			async:false,
			success:function(data){		
				if(data=="exist") {
					alert('Username already exists.');
					validate = 1;
				} else {
					validate = 0;										
				}
			},
			error:function (){}
		});
	}
	
	if(email != '')
	{
		validate = 1;
		jQuery.ajax({
			url: "../includes/checkUserAvailable.php",
			data:'validation_field=email&email='+email,
			type: "POST",
			async:false,
			success:function(data){			
				if(data=="exist") {
					alert('E-mail already exists.');
					validate = 1;
					return false;
				} else {
					validate = 0;
				}
			},
			error:function (){}
		});
	}else if(username != '' && validate == 0)
		validate = 0;
	else
		validate = 1;
	
	if(validate == 0)
	{
		var action = $('#hidAction').val();
		$('#addform').attr('action', action);					
		$('#hidbtnsubmit').val("submit");
		$('#addform').submit();
	}	
}
/*$("#toggle-password").click(function() {
$(this).toggleClass("fa-eye fa-eye-slash");
	var input = $($(this).attr("toggle"));
	if (input.attr("type") == "password") {
		input.attr("type", "text");
	} else {
		input.attr("type", "password");
	}
});*/
</script> 
<style>
.field-icon {
  float: right;
  margin-right:10px;
  margin-top: -30px;
  position: relative;
  z-index: 2;
}
</style>
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>