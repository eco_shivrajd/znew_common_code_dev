<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php"?>
<!-- END HEADER -->
<?php echo $ischecked_add;?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
		$activeMainMenu = "ManageProducts"; $activeMenu = "Campaign";
		include "../includes/sidebar.php"?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Campaign
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					
					<li>
					<i class="fa fa-home"></i>
						<a href="#">Campaign</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
            <div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Campaign Listing
							</div>	
							<?php
                                if ($ischecked_add==1) 
                                {
                                ?>					
                              <a href="campaign_web-add.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Campaign
                              </a>
                              <?php } ?>

                              
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								
								<th>
									 Campaign Name
								</th>
								<th>
									 Campaign Type
								</th>
                                <th>
									 Start Date
								</th>
								<th>
									 End Date
								</th>
								<th>
									 Status
								</th>
							</tr>
							</thead>
							<tbody>
								<?php								
								  $sql="SELECT id AS campaign_id, campaign_name,campaign_type, campaign_start_date,campaign_end_date, status 
								  FROM tbl_campaign_web 
								  WHERE deleted = 0 ORDER BY id DESC";//c.status = 0  AND 
								$result = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result))
								{
										if($row['status'] == 0) $row['status'] ='Active'; else $row['status'] ='Inactive';
										if($row['campaign_type'] == 'discount') $row['campaign_type'] = 'Price Discount';
										if($row['campaign_type'] == 'free_product') $row['campaign_type'] = 'Free Product';
										if($row['campaign_type'] == 'by_weight') $row['campaign_type'] = 'By Weight';
										echo '<tr class="odd gradeX">									 
											   <td>';
                                             if($ischecked_edit==1){ 
											  echo '<a href="campaign-edit.php?id='.$row['campaign_id'].'">'.fnStringToHTML($row['campaign_name']).'</a>';
                                               }else{
                                                echo '' . fnStringToHTML($row['campaign_name']) . '';
                                                }
											  echo' </td>
											   <td>
											   '.$row['campaign_type'].'
											   </td>
												 <td>
											   '.date('d-m-Y',strtotime($row['campaign_start_date'])).'
											   </td>
												<td>
											   '.date('d-m-Y',strtotime($row['campaign_end_date'])).'
											   </td>
											   <td>
											   '.$row['status'].'
											   </td>';
								}							
								?>							
							</tbody>
							</table>
						</div>
					</div>
            
				
                    
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>