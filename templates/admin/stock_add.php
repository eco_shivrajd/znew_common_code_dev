<?php
include "../includes/grid_header.php";
include "../includes/commonManage.php";
$commonObj = new commonManage($con, $conmain);
$commonObjctype = $commonObj->log_get_commonclienttype($con, $conmain);
$user_type = $_SESSION[SESSION_PREFIX . 'user_type'];
$user_id = $_SESSION[SESSION_PREFIX . 'user_id'];
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "Stock";
         $activeMenu = "stock_dashboard";
        include "../includes/sidebar.php";
        $commonObj     =   new commonManage($con,$conmain);
        $row_url=$commonObj->getPageIDforUrlAdd($php_page_name);
        $page_id_url = $row_url['page_id'];
        $row_url_add=$commonObj->getURLforAdd($profile_id,$page_id_url);
        $ischecked_add_url = $row_url_add['ischecked_add'];
        if ($ischecked_add_url == 0 && $ischecked_add_url!='') 
        {
        session_set_cookie_params(0);
        session_start();
        session_destroy();
        echo '<script>location.href="../login.php";</script>';
        exit;
        }
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

                <!-- /.modal -->

                <h3 class="page-title">
                    Add Stock
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="stock_dashboard.php">Stock Dashboard</a>
                             <i class="fa fa-angle-right"></i>
                        </li>
                            <li>
                            <a href="#">Add Stock</a>
                            </li>
                    </ul>

                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">

                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">Review Updates </div>	
                                <a id="btnEmpty" class="btn btn-sm btn-default pull-right mt5" onClick="cartActionEmpty();">Empty List</a>
                                <div class="clearfix"></div>
                            </div>						
                            <div class="portlet-body">	
                                 <div id="empty_cart_div_id" align="center">Add Stock cart is empty.</div>
								 <?php
									$margin_percentage=0;
									$sql_all_get="SELECT `tbl_user`.`id`,`tbl_user`.`user_type`, `tbl_user`.`user_role`, 
									 `tbl_user`.`marginType`,`tbl_user`.`parent_ids`, `userid_margin` ,`userrole_margin`,`usertype_margin`
									FROM `tbl_user` 
									left join tbl_userrole on `tbl_user`.`user_role`=tbl_userrole.user_role
									left join tbl_user_tree on `tbl_user`.`user_type`=tbl_user_tree.user_type
									WHERE `tbl_user`.`isdeleted`!='1' 
									AND ( `tbl_user`.`user_role`='Admin' OR `tbl_user`.`user_role`='Superstockist' 
									OR `tbl_user`.`user_role`='Distributor' ) ";
									$get_all_user_result = mysqli_query($con, $sql_all_get);
									while($row_all_users = mysqli_fetch_array($get_all_user_result)){
										$userids=$row_all_users['id'];
										$margin[0]=$row_all_users['userrole_margin'];
										$margin[1]=$row_all_users['usertype_margin'];
										$margin[2]=$row_all_users['userid_margin'];
										$marginType = $row_all_users['marginType'];
										$margin_percentage1[$userids] = $margin[$marginType];
										$parent_ids1[$userids] = $row_all_users['parent_ids'];
									}
                                    $margin_percentage=$margin_percentage1[$user_id];
                                    $parent_ids=$parent_ids1[$user_id];
                                ?> 
                                    <table class="table table-striped table-bordered table-hover" id="myTable" style="display:none">
                                        <thead>
                                            <tr>					
                                                <th> Category </th>
                                                <th> Product Name</th> 
												<th>Product Price</th>
												<th>Price With </br>Margin (<span id="margin_per_span1"><?php echo $margin_percentage;?></span> %) And Tax </th>
                                                <th>Product Variant</th>
                                                <th> Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody >

                                        </tbody>
                                    </table>
                                 <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" >
                                     <div class="form-group" id="place_order_div" style="display:none">
                                        <div class="col-md-4 col-md-offset-5">
                                            <button type="button" name="place_order_btnsubmit" id="place_order_btnsubmit" class="btn btn-primary" onclick="placeStock();">Add To Stock</button>
                                        </div>
                                    </div><!-- /.form-group -->
                                 </form>
                            </div>
                            <!-- END PAGE CONTENT-->
                        </div>
                        <div class="clearfix"></div>   


                        <div class="portlet box blue-steel">
                            <div class="portlet-title">

                                <div class="caption">Product Listing</div>
                                <a id="btnEmpty" class="btn btn-sm btn-default pull-right mt5" onclick="stockAdd();">Add To Cart</a>
                                <div class="clearfix"></div>

                            </div>

                            <div class="portlet-body">

                                <form id="frmCart">
                                  <table class="table table-striped table-bordered table-hover" id="sample_7">
                                        <thead>
                                            <tr>					
                                                <th> Category </th>
                                                <th>Product Name</th>
                                                <th>Product Price</th>
												 <th>  Price With </br>Margin (<span id="margin_per_span2"><?php echo $margin_percentage;?></span> %) And Tax   </th>
                                                <th>Product Variant </th>
                                                <th> Quantity</th>
                                            </tr>
                                        </thead>
    <tbody>
        <?php 
          $sql1 = "SELECT parent_ids FROM tbl_user where id='$user_id'";
          $result1 = mysqli_query($con, $sql1);
          $row1 = mysqli_fetch_assoc($result1);
          $parent_ids = $row1['parent_ids'];
         $sql = "SELECT a.categorynm,b.productname,b.id,pv.producthsn,pv.price as price,pv.variant_1,pv.variant_2,pv.variant1_unit_id,pv.variant2_unit_id,pv.id as pvid, "
                . " pv.productimage,pv.cgst,pv.sgst,b.added_by_userid,b.added_by_usertype, a.id AS cat_id  "
                . " FROM tbl_category a,tbl_product b "
                . " left join tbl_product_variant pv on b.id = pv.productid "
                . " where a.id=b.catid AND b.isdeleted != 1 "
                . " and (b.added_by_userid='".$user_id."' OR b.added_by_userid IN (".$parent_ids."))";//((b.added_by_userid=0) OR (b.added_by_userid=1) OR (b.added_by_userid='$user_id'))
        $result = mysqli_query($con, $sql);
        $row_count = mysqli_num_rows($result);
        while ($row = mysqli_fetch_array($result)) 
        { 
            $prodcode = $row['id'] . '_' . $row['pvid'];
			$price = $row['price'];
			$cgst = $row['cgst'];
			$sgst = $row['sgst'];
			
			//new code for price update onchange 
			$prod_price_update_array[$prodcode]['price']=$price;
			$prod_price_update_array[$prodcode]['cgst']=$cgst;
			$prod_price_update_array[$prodcode]['sgst']=$sgst;
			//new code for price update onchange 
			
            $price_with_margin = $price - (($margin_percentage/100)*$price);
			$tax_cgst = $price_with_margin*$cgst/100;
			$tax_sgst = $price_with_margin*$sgst/100;
			$price_with_margin_cgst_sgst = $price_with_margin+$tax_cgst+$tax_sgst;
            ?>

            <tr class="odd gradeX" id="<?php echo $prodcode; ?>">
                <td id="num1<?php echo $prodcode; ?>">
                    <?php echo fnStringToHTML($row['categorynm']); ?>
                </td>
                <td id="num2<?php echo $prodcode; ?>">
                    <?php echo fnStringToHTML($row['productname']); ?>
                </td>
                <td align="right" id="num3<?php echo $prodcode; ?>">
                    <?php echo fnStringToHTML($row['price']); ?>
                </td>
				<td align="right" id="num4<?php echo $prodcode; ?>">
                    <?php echo fnStringToHTML(number_format((float)$price_with_margin_cgst_sgst, 2, '.', '')); ?>					
                </td>
                <td id="num5<?php echo $prodcode; ?>"> 
                     <?php
                    $sqlvarunit3 = "SELECT unitname FROM tbl_units  where id='" . $row['variant1_unit_id'] . "'";
                    $resultvarunit3 = mysqli_query($con, $sqlvarunit3);
                    while ($rowvarunit3 = mysqli_fetch_array($resultvarunit3)) 
                    {
					   $unitname3 = $rowvarunit3['unitname'];
                    }

                    $sqlvarunit4 = "SELECT unitname FROM tbl_units  where id='" . $row['variant2_unit_id'] . "'";
                    $resultvarunit4 = mysqli_query($con, $sqlvarunit4);
                    while ($rowvarunit4 = mysqli_fetch_array($resultvarunit4)) 
                    {
					   $unitname4 = $rowvarunit4['unitname'];
                    }
                    echo $row['variant_1']."-".$unitname3." , ".$row['variant_2']."-".$unitname4;
                    echo "<br>";
                      ?>

                </td>
                <td align="right" id="num6<?php echo $prodcode; ?>">
                    <input type="number" min="0" max="100000" name="prodqnty" id="qty_<?php echo $prodcode; ?>" oninput="myFunction(this)" value="0" size="2" style="width: 55px; margin: 5px 5px 5px 5px;text-align: right;"> 
                </td>
            </tr>
        <?php } ?>							
    </tbody>
</table>
                                </form>
                            </div>
                        </div> 

                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <?php include "../includes/grid_footer.php" ?>
    <!-- END FOOTER -->

    <script>
function myFunction(varl) {
    if (varl.value != "") {   
        var your_string=varl.value.toString();  
        var lengthofvalue=varl.value.toString().length;
        if (lengthofvalue > 5) { 
            varl.value = varl.value.substring(0, 5);
        }
        if (your_string.indexOf('.') > -1){
            varl.value = varl.value.substring(0, lengthofvalue-1);
        }       
    }
}
 $( document ).ready(function() {  
        $("#place_order_btnsubmit").on('click', function (event) {
           event.preventDefault();
           var el = $(this);
           el.prop('disabled', true);
           setTimeout(function(){el.prop('disabled', false); }, 3000);
     });
 
        var table = $('#sample_7').dataTable({destroy: true,stateSave: false,paging:   false});
             $('input[type=search]').on("input", function() {
                table.fnFilter('');
            });

    });  
        function placeStock(){ 
            var orderarray=[];
            $('#myTable tbody tr').each(function () {  
                var void1 = [];
                var newenergy = this.id.replace('mytabletr', '');
                var tempstrip=$('#myqty_' + newenergy + '').html();                            
                void1.push(tempstrip.replace(/ +$/, "")); 
                void1.push(newenergy); 
                 orderarray.push(void1); 
            });
          
            if (orderarray.length > 0) {
                var url = 'stock_update.php';               
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {data123 : orderarray},
                    error: function (data) {
                        console.log("error:" + data)
                    },
                    success: function (data) {
                        console.log(data);
                        if (data > 0) {
                            alert("Stock Added Successfully");
                           // location.reload();
                           window.location.href = "stock_dashboard.php";
                        } else {
                            alert("Not updated");
                        }
                    }
                });
            } else {
                alert("Please add some product!");
                return false;
            }
        }
        function cartActionEmpty(){
            $("#myTable tbody").empty();
            $('#place_order_div').hide();
            $('#myTable').hide();
            $('#empty_cart_div_id').show();
            total_price_all = 0.00;
            total_price_all_gst=0.00;
        }
        function cartActionEmptyItem(trid){
            $("#mytabletr"+trid+"").remove();
            var rowCount = $('#myTable tbody tr').length;  
            if(rowCount==0){
                $('#place_order_div').hide();
                $('#myTable').hide();
                 $('#empty_cart_div_id').show();
            }   
        }

        function stockAdd() {
            var void1 = [];
            var void2 = [];
            $('input[name="prodqnty"]').each(function () {
                if (this.value > 0) {
                    void1.push(this.value);
                    void2.push(this.id);
                }
            });
            var energy = void1.join();
            
            if (energy != '') {
                Object.keys(void1).forEach(function (key) {
                    var newenergy = void2[key].replace('qty_', '#');
                    var newenergy1 = newenergy.replace('#', '');
                    if ($('#mytabletr' + newenergy1 + '').length) {
                        var sumvalue = +$('#myqty_' + newenergy1 + '').html() + +void1[key];
                        console.log(sumvalue);
                        $('#myqty_' + newenergy1 + '').html('' + sumvalue + '');
                    } else {
                       
                        $("#myTable").append("<tr class='odd gradeX' id='mytabletr" + newenergy1 + "'><td>"+
                           $('#num1' + newenergy1 + '').html() + "</td><td>" + 
						   $('#num2' + newenergy1 + '').html() + "</td><td>" + 
						   $('#num3' + newenergy1 + '').html() + "</td><td>" + 
						   $('#num4' + newenergy1 + '').html() + "</td><td>" + 
						   $('#num5' + newenergy1 + '').html() + "</td><td >"+
						   "<button type='button' class='btn btn-primary' onClick='cartActionEmptyItem(\"" + newenergy1 + "\");'>Remove "+
						   "<span id='myqty_" + newenergy1 + "' class='badge'>"+$('#qty_' + newenergy1 + '').val()+"</span></button></td>"+
                        "</tr>");
                    }

                });
                if ($('#myTable tr').length>0) {                    
                    $('#place_order_div').show();
                    $('#myTable').show();
                    $('#empty_cart_div_id').hide();
                }
                $('input[name="prodqnty"]').each(function () {
                    if (this.value > 0) {
                        $('input[name="prodqnty"]').val('0');
                    }
                });
            } else {
                alert("Please add some quantity!");
            }
        }
    </script>


</body>
<!-- END BODY -->
</html>