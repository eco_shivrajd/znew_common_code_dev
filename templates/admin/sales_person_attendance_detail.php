<?php
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/orderManage.php";
include "../includes/userManage.php";
$orderObj = new orderManage($con, $conmain);
$userObj 	= 	new userManager($con,$conmain);
$report_title = $orderObj->getReportTitle();
$row = $orderObj->getSPattendance();
$record_count = mysqli_num_rows($row);
//echo "sdfsd<pre>";print_r($row);
$colspan = "9";
?>
<?php if ($_POST["actionType"] == "excel") { ?>
    <style>table { border-collapse: collapse; } 
        table, th, td {  border: 1px solid black; } 
        body { font-family: "Open Sans", sans-serif; 
               background-color:#fff;
               font-size: 11px;
               direction: ltr;}
        </style>
    <?php }
    ?>

    <table 
        class="table table-striped table-bordered table-hover table-highlight table-checkable" 
    data-provide="datatable" 
    data-display-rows="10"
    data-info="true"
    data-search="true"
    data-length-change="true"
    data-paginate="true"
    id="sample_5">
    <thead>
        <tr>
            <td colspan="<?= $colspan; ?>" align="canter" class="gradeX even" style="text-align:center; font-weight:600;"><h4><b><?php if (!empty($report_title))
        echo $report_title;
    else
        echo "SP Attendance Report All";
    ?></b></h4></td>              
        </tr>
        <tr>
            <th data-filterable="false" data-sortable="true" data-direction="desc">SR NO.</th>
            <th data-filterable="false" data-sortable="true" data-direction="desc">Name</th>
            <th data-filterable="false" data-sortable="false" data-direction="desc">Date</th>
            <th data-filterable="false" data-sortable="false" data-direction="desc">Start Day</th>	
            <th data-filterable="false" data-sortable="true" data-direction="desc">End Day </th>   
            <th data-filterable="false" data-sortable="false" data-direction="desc">Working Hours</th>	
            <th data-filterable="false" data-sortable="false" data-direction="desc">Distance Travelled</th>
            <!-- <th data-filterable="false" data-sortable="true" data-direction="desc">Present Status</th>	 -->
            <th data-filterable="false" data-sortable="true" data-direction="desc">Attendance</th>
            <th data-filterable="false" data-sortable="true" data-direction="desc">Remarks</th>              
        </tr>
    </thead>
    <tbody>					
        <?php
        if (!empty($row)) {
            foreach ($row as $key => $value) {
				 //$tdate = date_format($value['tdate'],);
				 $tdate = date("d-m-Y", strtotime($value['tdate']));
				 // echo "<pre>";print_r($tdate);
				 $sp_id = $value['sp_id'];
				 $resultsplocation = $orderObj->getSPLocationPoints($tdate,$sp_id);
				// echo "<pre>";print_r($resultsplocation);
				 $record_count_sp = mysqli_num_rows($resultsplocation);
				 $countrec=0;$c_array_temp=array();
					if($record_count_sp > 0){						
						while($record = mysqli_fetch_array($resultsplocation)){							
							$frmdate = $record['tdate'];
							$sp_id = $record['userid'];
							$c_array_temp[$countrec]['id'] = $record['id'];
							$c_array_temp[$countrec]['lattitude'] = $record['lattitude'];
							$c_array_temp[$countrec]['longitude'] = $record['longitude'];
							$countrec++;
						}
					}
					
					$google_distance=0;
					for($i=0;$i<count($c_array_temp)-1;$i++)
					{
						$google_d= $orderObj->getDistanceBetweenPoints($c_array_temp[$i]['lattitude'],$c_array_temp[$i+1]['lattitude'],$c_array_temp[$i]['longitude'],$c_array_temp[$i+1]['longitude']);
						$google_distance=$google_distance+$google_d['distance'];
					} 
					$google_distance=bcdiv($google_distance, 1000, 3);	
					$SPattendance = $userObj->updateSPattendance($frmdate,$sp_id,$google_distance);
					
                $leavedt = $value['leavedt']; ?>
                <tr class="odd gradeX">				
                    <td align='right'><?= $key + 1; ?></td>
                    <td align='Left'><?= $value['firstname']; ?></td>
                    <td align='right'><?= date('d-m-Y', strtotime($value['tdate'])); ?></td>
                    <td align='right'><?= date('H:i:s', strtotime($value['tdate'])); ?></td>
                    <td align='right'><?= date('H:i:s', strtotime($value['dayendtime'])); ?></td>
                    <td align='right'><!-- <?= $value['hours_difference'] ?> --> <?= number_format((float) $value['hours_difference'], 0, '.', '') ?> </td>
                    <td align='right'><?= number_format((float) $google_distance, 3, '.', '') ?></td><!--$value['todays_travelled_distance']-->
                    <td><?php if ($value['presenty'] == '1') { ?><b>P</b> <?php } else { ?> <b>A</b> <?php  } ?></td> 
					<td align='right'> -</td>
				</tr>
                <?php } 
            }
            if ($_POST["actionType"] == "excel" && $row == 0) {
                echo "<tr><td>No matching records found</td></tr>";
            }
            ?>	

        </tbody>	
    </table>



    <script>
        jQuery(document).ready(function () {

            ComponentsPickers.init();
            TableManaged.init();
        });


        $(document).ready(function () {
            var table = $('#sample_5').dataTable();
            // Perform a filter
            table.fnFilter('');
            // Remove all filtering
            //table.fnFilterClear();

        });
    </script> 
    <!-- END JAVASCRIPTS -->
    <?php
    if ($_POST["actionType"] == "excel") {
        if ($row != 0) {
            header("Content-Type: application/vnd.ms-excel");
            header("Content-disposition: attachment; filename=SP_Summary_Report.xls");
        }
    }
    ?>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- END JAVASCRIPTS -->