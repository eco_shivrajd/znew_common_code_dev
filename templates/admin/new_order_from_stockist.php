<?php
//
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include "../includes/grid_header.php";
include "../includes/commonManage.php";
include "../includes/campaignManage.php";
$commonObj = new commonManage($con, $conmain);
$campaignObj = new campaignManager($con, $conmain);
$user_type = $_SESSION[SESSION_PREFIX . 'user_type'];
$user_role = $_SESSION[SESSION_PREFIX . 'user_role'];
$user_id = $_SESSION[SESSION_PREFIX . 'user_id'];
$prod_prodvar_array=array();
$order_id_array=array();
if(isset($_POST['select_all_send']))
{
	foreach($_POST['select_all_send'] as $key=>$order_id){
		$new_order_id=str_replace('ordersubsend_','',$order_id);
		$order_id_array[]=$new_order_id;
		 $order_sql="SELECT o.id,od.product_quantity,pv.productid as productid,od.product_variant_id as pvid 
		FROM `tbl_orders` o 
		LEFT JOIN tbl_order_details AS od ON od.order_id = o.id
		LEFT JOIN tbl_product_variant pv on od.product_variant_id = pv.id
		where o.id='".$new_order_id."'";
		
        $result = mysqli_query($con, $order_sql);
        $row_count = mysqli_num_rows($result);
        while ($row = mysqli_fetch_array($result)) 
        {          
            $prodcode = $row['productid'] . '_' . $row['pvid'];
			if (array_key_exists($prodcode,$prod_prodvar_array)){
				$prod_prodvar_array[$prodcode]=$prod_prodvar_array[$prodcode]+$row['product_quantity'];
			} else {
				$prod_prodvar_array[$prodcode]=$row['product_quantity'];
			}
		}
	}
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "Orders";
        $activeMenu = "Ordersnewstockist";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

                <!-- /.modal -->

                <h3 class="page-title">
                    New Orders
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="#">New Orders</a>
                        </li>
                    </ul>

                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">

                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">Shopping Cart </div>	
                                <a id="btnEmpty" class="btn btn-sm btn-default pull-right mt5" onClick="cartActionEmpty();">Empty Cart</a>
                                <div class="clearfix"></div>
                            </div>						
                            <div class="portlet-body">	
                               <?php
                                    $sqlgetuser = "SELECT marginType,parent_ids FROM `tbl_user` where `id`='".$user_id."' and `isdeleted` !=1";
                                    $get_user_result = mysqli_query($con, $sqlgetuser);
                                    while($row1 = mysqli_fetch_array($get_user_result))
                                    {
                                    $marginType = $row1['marginType'];
									 $parent_ids = $row1['parent_ids'];
                                    }
                                    if (isset($marginType)) 
                                    {
                                    switch ($marginType) {
                                    case '0':
                                    $margin_by_userrole = "SELECT userrole_margin FROM `tbl_userrole` where `user_role`='$user_role'";
                                    $margin_by_userrole_result = mysqli_query($con, $margin_by_userrole);
                                    $result = mysqli_fetch_assoc($margin_by_userrole_result);
                                    $margin_percentage = $result['userrole_margin'];
                                    break;
                                    case '1':
                                    $margin_by_usertype = "SELECT usertype_margin FROM `tbl_user_tree` where `user_type`='$user_type'";
                                    $margin_by_usertype_result = mysqli_query($con, $margin_by_usertype);
                                    $result = mysqli_fetch_assoc($margin_by_usertype_result);
                                    $margin_percentage = $result['usertype_margin'];
                                    break;
                                    case '2':
                                    $margin_by_userid = "SELECT userid_margin FROM `tbl_user` where `id`='$user_id'";
                                    $margin_by_userid_result = mysqli_query($con, $margin_by_userid);
                                    $result = mysqli_fetch_assoc($margin_by_userid_result);
                                    $margin_percentage = $result['userid_margin'];
                                    break;              
                                    default:
                                    $margin_percentage =0;
                                    break;
                                    }
                                    }
                                ?>                
                                    <table class="table table-striped table-bordered table-hover" id="myTable" >
                                        <thead>
                                            <tr>					
                                                <th>
                                                    Category
                                                </th>
                                                <th>
                                                    Product Name
                                                </th>
                                                <th>
                                                    Product HSN
                                                </th>
                                                <th>
                                                    Product Price
                                                </th>
                                                <th>
                                                    Price With </br>Margin (<?php echo $margin_percentage;?> %) And Tax
                                                </th>
                                                <th>
                                                    Product Variant
                                                </th>
                                                 
                                                <th>
                                                    Product Image
                                                </th>                                               
                                                <th>
                                                    Quantity
                                                </th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody >

                                        </tbody>
                                    </table>
                                 <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" >

                                     <div class="form-group" id="place_order_div" style="display:none">
                                    <?php 
                                        $unique_order_number = '0001231111'.$user_id.''. round(microtime(true));                  
                                       ?>
                                       <p style="text-align: center;"><b>ORDER ID : <?=$unique_order_number;?></b></p>
									   <p style="text-align: center;"><b>Total Items : <span id="total_items"></span></b></p>
									   <p style="text-align: center;"><b>Total Cost : <span id="order_total_cost"></span></b></p>
									   <p style="text-align: center;"><b>Total Cost After GST : <span id="order_total_cost_gst"></span></b></p>
                                       <input type="hidden" name="unique_order_number" value="<?=$unique_order_number;?>">
                                        <div class="col-md-4 col-md-offset-5">
                                            <button type="button" name="place_order_btnsubmit" id="place_order_btnsubmit" class="btn btn-primary" onclick="placeOrderStockist();">Place Order</button>
                                        </div>
                                    </div><!-- /.form-group -->
                                 </form>
                            </div>
                            <!-- END PAGE CONTENT-->
                        </div>
                        <div class="clearfix"></div>   


                        <div class="portlet box blue-steel">
                            <div class="portlet-title">

                                <div class="caption">Products Listing</div>
                                <a id="btnEmpty" class="btn btn-sm btn-default pull-right mt5" onclick="cartActionAdd();">Add Order</a>
                                <div class="clearfix"></div>

                            </div>

                            <div class="portlet-body">
                                <form id="frmCart">

                                    <table class="table table-striped table-bordered table-hover" id="sample_7">
                                        <thead>
                                            <tr>					
                                                <th>
                                                    Category
                                                </th>
                                                <th>
                                                    Product Name
                                                </th>
                                                <th>
                                                    Product HSN
                                                </th>
                                                <th>
                                                    Product Price
                                                </th>
                                                 <th>
                                                     Price With </br>Margin (<?php echo $margin_percentage;?> %) And Tax
                                                </th>
                                                <th>
                                                    Product Variant
                                                </th>
                                                <th>
                                                    Product Image
                                                </th>
                                                <th>
                                                    Quantity
                                                </th>
                                               
                                            </tr>
                                        </thead>
    <tbody>
        <?php
        $sql = "SELECT a.categorynm,b.productname,b.id,pv.producthsn,pv.price as price,pv.variant_1,pv.variant_2,pv.variant1_unit_id,pv.variant2_unit_id,pv.id as pvid, "
                . " pv.productimage,pv.cgst,pv.sgst,b.added_by_userid,b.added_by_usertype, a.id AS cat_id  "
                . " FROM tbl_category a,tbl_product b "
                . " left join tbl_product_variant pv on b.id = pv.productid "
                . " where a.id=b.catid AND b.isdeleted != 1 "
                . " and (find_in_set(b.added_by_userid,'".$parent_ids."') <> 0)";
        $result = mysqli_query($con, $sql);
        $row_count = mysqli_num_rows($result);
        while ($row = mysqli_fetch_array($result)) 
        { 
            $prodcode = $row['id'] . '_' . $row['pvid'];
            $price = $row['price'];
			$cgst = $row['cgst'];
			$sgst = $row['sgst'];
            $price_with_margin = $price - (($margin_percentage/100)*$price);
			$tax_cgst = $price_with_margin*$cgst/100;
			$tax_sgst = $price_with_margin*$sgst/100;
			$price_with_margin_cgst_sgst = $price_with_margin+$tax_cgst+$tax_sgst;
			//$price_with_margin_cgst = $price_with_margin + ($price*$cgst/100);
			//$price_with_margin_cgst_sgst = $price_with_margin_cgst + ($price*$sgst/100);
            ?>

            <tr class="odd gradeX" id="<?php echo $prodcode; ?>">
                <td id="num1<?php echo $prodcode; ?>">
                    <?php echo fnStringToHTML($row['categorynm']); ?>
                </td>
                <td id="num2<?php echo $prodcode; ?>">
                    <?php echo fnStringToHTML($row['productname']); ?>
                </td>
                <td id="num3<?php echo $prodcode; ?>" >
                    <?php echo fnStringToHTML($row['producthsn']); ?>
                </td>
                <td align="right" id="num4<?php echo $prodcode; ?>">
                    <?php echo fnStringToHTML($row['price']); ?>
                </td>
                <td align="right" id="num5<?php echo $prodcode; ?>">
                    <?php echo fnStringToHTML(number_format((float)$price_with_margin_cgst_sgst, 2, '.', '')); ?>					
                </td>										
                <td id="num6<?php echo $prodcode; ?>">
                    

                     <?php
                    $sqlvarunit3 = "SELECT unitname FROM tbl_units  where id='" . $row['variant1_unit_id'] . "'";
                    $resultvarunit3 = mysqli_query($con, $sqlvarunit3);
                    while ($rowvarunit3 = mysqli_fetch_array($resultvarunit3)) 
                    {
                               $unitname3 = $rowvarunit3['unitname'];
                    }

                    $sqlvarunit4 = "SELECT unitname FROM tbl_units  where id='" . $row['variant2_unit_id'] . "'";
                    $resultvarunit4 = mysqli_query($con, $sqlvarunit4);
                    while ($rowvarunit4 = mysqli_fetch_array($resultvarunit4)) 
                    {
                               $unitname4 = $rowvarunit4['unitname'];
                    }
                    if(!empty($row['variant_1'])){ echo $row['variant_1']."-".$unitname3." ";}
					if(!empty($row['variant_2'])){ echo $row['variant_2']."-".$unitname4; }
                    echo "<br>";
                      ?>

                </td>
                <td id="num7<?php echo $prodcode; ?>">
                    <?php if (!empty($row['productimage'])) { ?>
                        <img src="upload/<?php echo COMPANYNM; ?>_upload/<?php echo $row['productimage']; ?>" 
                             alt=<?php echo $row['productimage']; ?> 
                             width="100px" />
                         <?php } ?>										 
                </td>
                <td id="num8<?php echo $prodcode; ?>" align="right">
                    <input type="number" min="0" max="100000" oninput="myFunction(this)"  
					name="prodqnty" id="qty_<?php echo $prodcode; ?>" 
                           value="0" size="2"  style="width: 65px; margin: 5px 5px 5px 5px;text-align: right;"> 
                </td>
            </tr>
        <?php } ?>							
    </tbody>
</table>
                                </form>
                            </div>
                        </div> 

                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <?php include "../includes/grid_footer.php" ?>
    <!-- END FOOTER -->

    <script>
	var total_price_all = 0.00;
	var total_price_all_gst=0.00;
	function myFunction(varl) {		
    if (varl.value != "") {		
		var lengthofvalue=varl.value.toString().length;
		if (lengthofvalue > 5) {
			varl.value = varl.value.substring(0, 5);//varl.value.toString().length - 1			
		}
    }
}
	$( document ).ready(function() {		
		var myorderqnty=<?php echo json_encode($prod_prodvar_array); ?>;		
		if(!jQuery.isEmptyObject(myorderqnty)){
			$.each(myorderqnty, function( index, value ) {			
				$('#qty_'+index+'').val(value);
			});
			alert("All selected orders product quantity are added in the following table respectively except my own product.");
		}
	
       var table = $('#sample_7').dataTable({destroy: true,stateSave: false,paging:   false});
	     $('input[type=search]').on("input", function() {
			table.fnFilter('');
		});

	});
	
	
	
        function placeOrderStockist(){
			var sended_to_production=0;
			var myorderqnty=<?php echo json_encode($prod_prodvar_array); ?>;	
			var order_id_array=<?php echo json_encode($order_id_array); ?>;	
            var unique_order_number=<?php echo json_encode($unique_order_number); ?>; 
            //alert(unique_order_number);
			//console.log(typeof(order_id_array));
			//alert(order_id_array);console.log(order_id_array);debugger;
			if(!jQuery.isEmptyObject(myorderqnty)){
				sended_to_production=1;
			}
            var orderarray=[];
            $('#myTable tbody tr').each(function () {  
                var void1 = [];
                var newenergy = this.id.replace('mytabletr', '');
                var tempstrip=$('#myqty_' + newenergy + '').html();                            
                void1.push(tempstrip.replace(/ +$/, "")); 
                void1.push(newenergy); 
                 orderarray.push(void1); 
            });
          
            if (orderarray.length > 0) {
                var url = 'updateOrderFromStockist.php';               
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {data123 : orderarray,sended_to_production:sended_to_production,order_id_array:order_id_array,unique_order_number:unique_order_number},
                    error: function (data) {
                        console.log("error:" + data)
                    },
                    success: function (data) {
                        console.log(data);
                        if (data > 0) {
                            alert("Order Placed Successfully.");
							window.location.href = "orders_from_to.php";
                            //location.reload();
                        } else {
                            alert("Not updated.");
                        }
                    }
                });
            } else {
                alert("Please add some product.");
                return false;
            }
        }
        function cartActionEmpty(){
            $("#myTable tbody").empty();
			$('#place_order_div').hide();
			total_price_all = 0.00;
			total_price_all_gst=0.00;
        }
        function cartActionEmptyItem(trid){
			console.log(trid);
			
			var nthtotal_cost=parseFloat($("#mytabletr"+trid+" td:nth-child(4)").text());
			var nthtotal_cost_gst=parseFloat($("#mytabletr"+trid+" td:nth-child(5)").text());
			//debugger;
			var total_qnty=parseFloat($("#myqty_"+trid+"").text());
			console.log(total_qnty);	
			//debugger;
			var total_cost = parseFloat($('#order_total_cost').text()) - parseFloat(nthtotal_cost*total_qnty);
			var total_cost_gst = parseFloat($('#order_total_cost_gst').text()) - parseFloat(nthtotal_cost_gst*total_qnty);
			
			$('#order_total_cost').html(parseFloat(total_cost).toFixed(2));
			$('#order_total_cost_gst').html(parseFloat(total_cost_gst).toFixed(2));
			total_price_all=parseFloat(total_cost).toFixed(2);
			total_price_all_gst=parseFloat(total_cost_gst).toFixed(2);
			var rowCount = $('#myTable tbody tr').length;			
			var total_items=rowCount-1;
            $("#mytabletr"+trid+"").remove();
			$('#total_items').html("<b>"+total_items+"</b>");
			if(rowCount==1){
				$('#place_order_div').hide();
			}			
        }
		
        function cartActionAdd() {
            var void1 = [];
            var void2 = [];
            $('input[name="prodqnty"]').each(function () {
                if (this.value > 0) {
                    void1.push(this.value);
                    void2.push(this.id);
                }
            });
            var energy = void1.join();
          //  var no = new Date().getTime();
          //  var unique_order_number = '0001231'+<?php echo $user_id;?>+no;
          //  alert(unique_order_number);            
            if (energy != '') {
                

                Object.keys(void1).forEach(function (key) { 
                //alert('dfgdfgdf');   
					
                    var newenergy = void2[key].replace('qty_', '#');
                    var newenergy1 = newenergy.replace('#', '');
					
					var pro_qty = $('#qty_' + newenergy1 + '').val();
					var pro_price = parseFloat($('#num4' + newenergy1 + '').html());
					var pro_price_gst = parseFloat($('#num5' + newenergy1 + '').html());
					var total_price = pro_qty*pro_price;
					var total_price_gst = pro_qty*pro_price_gst;
					total_price_all=parseFloat(total_price_all)+parseFloat(total_price);
					total_price_all_gst=parseFloat(total_price_all_gst)+parseFloat(total_price_gst);
					
                    if ($('#mytabletr' + newenergy1 + '').length) {						
                        var sumvalue = +$('#myqty_' + newenergy1 + '').html() + +void1[key];
                        console.log(sumvalue);						
                        $('#myqty_' + newenergy1 + '').html('' + sumvalue + '');
						$('#order_total_cost').html("<b>"+parseFloat(total_price_all).toFixed(2)+"</b>");
						$('#order_total_cost_gst').html("<b>"+parseFloat(total_price_all_gst).toFixed(2)+"</b>");
                    } else {   
                        $("#myTable").append("<tr class='odd gradeX' id='mytabletr" + newenergy1 + "'>\
                            <td>" + $('#num1' + newenergy1 + '').html() + "</td>\
                            <td>" + $('#num2' + newenergy1 + '').html() + "</td>\
                            <td>" + $('#num3' + newenergy1 + '').html() + "</td>\
                            <td align='right'>" + $('#num4' + newenergy1 + '').html() + "</td>\
                            <td align='right'>" + $('#num5' + newenergy1 + '').html() + "</td>\
                            <td>" + $('#num6' + newenergy1 + '').html() + "</td>\
                            <td>" + $('#num7' + newenergy1 + '').html() + "</td>\
                            <td ><button type='button' class='btn btn-primary' onClick='cartActionEmptyItem(\"" + newenergy1 + "\");'>Remove <span id='myqty_" + newenergy1 + "' class='badge'>"+$('#qty_' + newenergy1 + '').val()+"</span></button></td>\
                        </tr>");
						$('#order_total_cost').html("<b>"+parseFloat(total_price_all).toFixed(2)+"</b>");
						$('#order_total_cost_gst').html("<b>"+parseFloat(total_price_all_gst).toFixed(2)+"</b>");
                    }
                });
  


                if ($('#myTable tbody tr').length>0) { 
					var total_items =$('#myTable tbody tr').length;
                    $('#place_order_div').show();
					$('#total_items').html("<b>"+total_items+"</b>");
                }
                $('input[name="prodqnty"]').each(function () {
                    if (this.value > 0) {
                        $('input[name="prodqnty"]').val('0');
                    }
                });
            } else {
                alert("Please add some quantity.");
            }
        }
    </script>


</body>
<!-- END BODY -->
</html>