<?php 
include ("../../includes/config.php");
include "../includes/routeManage.php";
$routeObj = new routeManage($con, $conmain);
$route_id = $_POST['route_id'];
$route_details=$routeObj->getRouteCountDetails($route_id);
//echo "<pre>";print_r($route_details);
    $array_route = array();
	$j=0;
	foreach($route_details as $key_row=>$value_row) 
	{ 
		//var_dump($row_route);
			$array_route[$j]['lat']=$value_row["lattitude"];
		    $array_route[$j]['lng']=$value_row["longitude"];
		    $array_route[$j]['title']=$value_row["address"];	
            $route_name=$value_row["route_name"];	
		$j++;
	}
//var_dump($array_route);
//echo "<pre>";print_r($array_route);die();
//exit();
?>
<div class="modal-header">



<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel">    
    Route : <?php echo $route_name;?>
</h4>	   
</div>
<div class="modal-body" style="padding-bottom: 5px !important;" id="divOrderPrintArea">
<div class="row">
<div class="col-md-12">   
	<div class="portlet box blue-steel">
		<div class="portlet-title ">
			<div class="caption printHeading">
			
			</div>                          
		</div>
		<div class="portlet-body" style="height:500px;">
		<? 
		$total_count_route=count($array_route);

		//var_dump($array_route);
	//	exit();
		if($total_count_route==0)
		{
		?>
		<div class="form-group">
			<div class="col-md-12">				
				<div   style="min-height:480px">
				No Record available.
				</div>
			</div>
		</div>
		<?php exit; }else{ ?>
		<div class="form-group">
			<div class="col-md-12">
				
				 	<div id="map" style="min-height:480px"></div>
			</div>
		</div>
		</div>
		<?php } ?>
</div>
</div>
</div>
</div>
</div>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDMpdO43txdg_zyovvZm9i1tMMFIQTKTU&callback=initialize"
        ></script>
<script>
	var markers=<?php echo json_encode($array_route); ?>;
	console.log(markers);
		  var geocoder;
			var map;
			var directionsDisplay;
			var directionsService = new google.maps.DirectionsService();
			// var locations = markers;

			function initialize() {
			  directionsDisplay = new google.maps.DirectionsRenderer();

			  var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 10,
				center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
				mapTypeId: google.maps.MapTypeId.ROADMAP
			  });
			  directionsDisplay.setMap(map);
			  var infowindow = new google.maps.InfoWindow();

			  var marker, i;
			  /*
			  var request = {
				travelMode: google.maps.TravelMode.DRIVING
			  };
			  */
			  
			  for (i = 0; i < markers.length; i++) {
				marker = new google.maps.Marker({
				  position: new google.maps.LatLng(markers[i].lat, markers[i].lng),
				  map: map
				});

				google.maps.event.addListener(marker, 'click', (function(marker, i) {
				  return function() {
					infowindow.setContent(markers[i].title);
					infowindow.open(map, marker);
				  }
				})(marker, i));
				/*
				if (i == 0) request.origin = marker.getPosition();
				else if (i == markers.length - 1) request.destination = marker.getPosition();
				else {
				  if (!request.waypoints) request.waypoints = [];
				  request.waypoints.push({
					location: marker.getPosition(),
					stopover: true
				  });
				}
				*/
				
				
			  }	
			   var service = new google.maps.DirectionsService();
			  for (i = 0; i < markers.length; i++) {
				  if ((i + 1) < markers.length) {
				   var src = new google.maps.LatLng(markers[i].lat, markers[i].lng);
				   var des = new google.maps.LatLng(markers[i+1].lat, markers[i+1].lng);					
					//alert(src);
					service.route({
						origin: src,
						destination: des,
						travelMode: google.maps.DirectionsTravelMode.DRIVING
					}, function (result, status) {
						if (status == google.maps.DirectionsStatus.OK) {
							//Initialize the Path Array
							var path = new google.maps.MVCArray();

							//Set the Path Stroke Color
							var poly = new google.maps.Polyline({
								map: map,
								strokeColor: '#000080'
							});
							poly.setPath(path);

							for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
								path.push(result.routes[0].overview_path[i]);
							}
						}
					});
				}
			  }
			 
			}
			google.maps.event.addDomListener(window, "load", initialize);
    </script>

