<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/reportManage.php";
$reportObj = new reportManage($con, $conmain);
?>
<!-- END HEADER -->
<?php echo $ischecked_add;?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
		$activeMainMenu = "ManageProducts"; $activeMenu = "Campaign";
		include "../includes/sidebar.php"?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Campaigns
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					
					<li>
					<i class="fa fa-home"></i>
						<a href="#">Campaigns</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Campaigns Listing
							</div>	
							<?php
                                if ($ischecked_add==1) 
                                {
                                ?>					
                              <a href="campaign-add.php" class="btn btn-sm btn-default pull-right mt5" style="margin-left:5px;">
                                Add Campaign
                              </a>
							  
                              <?php } ?>
				   <button title="Assign To Sales Persons" id="assign_sp_link"
					 onclick="javascript: assign_sp(this)" 
					 class="btn btn-sm btn-default pull-right mt5" style="margin-left:5px;">
					 <span class="glyphicon glyphicon-share"></span> Assign Campaign </button>
					 
					 <button title="Assigned Sales Persons Listing" id="assigned_sp_listing_link"
					 onclick="javascript: assigned_sp_listing(this)" 
					 class="btn btn-sm btn-default pull-right mt5" style="margin-left:5px;">
					 <span class="glyphicon glyphicon-list-alt"></span> Assigned User </button>
							
                              
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2dgsd">
							<thead>
							<tr>	
								<th id="select_th">				
									<input type="checkbox" name="select_all[]" id="select_all"  
									value="" onchange="javascript:checktotalchecked(this)">
								</th>
								<th>
									 Campaign Name
								</th>
								<th>
									 Campaign Type
								</th>
                                <th>
									 Start Date
								</th>
								<th>
									 End Date
								</th>
								<th>
									 Status
								</th>
							</tr>
							</thead>
							<tbody>
								<?php								
								  $sql="SELECT id AS campaign_id, campaign_name,campaign_type, campaign_start_date,campaign_end_date, status 
								  FROM tbl_campaign 
								  WHERE deleted = 0 ORDER BY id DESC";//c.status = 0  AND 
								$result = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result))
								{
										if($row['status'] == 0) $row['status'] ='Active'; else $row['status'] ='Inactive';
										if($row['campaign_type'] == 'discount') $row['campaign_type'] = 'Price Discount';
										if($row['campaign_type'] == 'free_product') $row['campaign_type'] = 'Free Product';
										if($row['campaign_type'] == 'by_weight') $row['campaign_type'] = 'By Weight'; ?>
										<tr class="odd gradeX">	
											<td>
												<?php if($row['campaign_type']=='Free Product'){ ?>
												 <span style="color:red" class="glyphicon glyphicon-ban-circle"></span>												
												<?php } else{ ?>
												 <input type="checkbox" name="select_all[]" id="select_all" 
												 value="<?=$row['campaign_id'];?>" onchange="javascript:checktotalchecked(this)">
												<?php } ?>
											</td>
											<td>
											<?php if($ischecked_edit==1){ 
											echo '<a href="campaign-edit.php?id='.$row['campaign_id'].'">'.fnStringToHTML($row['campaign_name']).'</a>';
											}else{ echo '' . fnStringToHTML($row['campaign_name']) . '';} ?>
											</td>
											<td><?php echo $row['campaign_type'];?></td>
											<td> <?php echo date('d-m-Y',strtotime($row['campaign_start_date']));?> </td>
											<td> <?php echo date('d-m-Y',strtotime($row['campaign_end_date']));?></td>
											<td><?php echo $row['status'];?>
											</td>
								<?php }	?>							
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
	<div class="modal fade" id="assignSPModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Assign Sales Person To:</h4>	   
                </div>		
                <form role="form" class="form-horizontal" 
					onsubmit="return false;" 
					action="campaign_listing_new.php" data-parsley-validate="" 
					name="assign_sp_form" 
					id="assign_sp_form">
                    <div class="modal-body" >	  
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-md-4">Select Sales Person:</label>
                            <div class="col-md-6" id="show_dcp_dropdown">
                               <select name="sp_ids" id="sp_ids" class="form-control" multiple
                                    data-parsley-trigger="change"
                                    data-parsley-required="#true" 				
                                    data-parsley-required-message="Please select Sales Person">
                                   <option value='0'>-Select-</option>                                   
                                <?php
                                    $sqlsp = "SELECT tu.id,tu.firstname 
                                        FROM tbl_user tu 
                                        where user_role='SalesPerson' AND  external_id='".$user_id."'";
                                          // . "group by tds.sp_ids ";
                                           //. "having ((sum(tds.stock_qnty) <=0) or(sum(tds.stock_qnty)  is null))";
                                    $resultsp = mysqli_query($con, $sqlsp);
                                   
									$sqlcampaigns = "SELECT distinct tsca.sp_id
                                        FROM tbl_sp_campaign_assign tsca
                                        group by tsca.sp_id ";
                                    $result = mysqli_query($con, $sqlcampaigns);
                                    while ($row2 = mysqli_fetch_array($result)){
                                        $sp_id_assigneds[]=$row2['sp_id'];            
                                    } 
                                            
                                    while ($row_sp = mysqli_fetch_array($resultsp)) {
                                        $userid=$row_sp['id'];
                                        if (in_array($userid,$sp_id_assigneds)){
											?>
                                            <option value='<?php echo $row_sp['id'];?>' disabled><?php echo fnStringToHTML($row_sp['firstname']);?></option>-->
                                <?php   }else{ ?>
                                           <option value='<?php echo $row_sp['id'];?>'><?php echo fnStringToHTML($row_sp['firstname']);?></option>
                                     <?php  }
                                       } ?>
                                </select> 
                            </div>
                        </div><!-- /.form-group --> 
                        
                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-3">					
                                <button type="submit"  name="btnsubmit"  class="btn btn-primary">Submit</button>
                                <a href="campaign_listing_new.php" class="btn btn-primary">Cancel</a>
                            </div>
                        </div><!-- /.form-group -->
						<input type="hidden" name="sp_ids_selected" id="sp_ids_selected" value="0"/>
                        <input type="hidden" name="input_campaign_ids" id="input_campaign_ids" value="0"/>
                        <input type="hidden" name="action" id="action" value="assign_campaigns"/>
                    </div><!-- /.form-group --> 				
            </div>
            </form>	   	  
        </div>
    </div>
	
	<!--start-->
	<div class="modal fade" id="assignedSPModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Assigned Sales Person Listing:</h4>	   
                </div>		
                <form role="form" class="form-horizontal" 
					onsubmit="return false;" 
					action="campaign_listing_new.php" data-parsley-validate="" 
					name="assign_sp_form" 
					id="assign_sp_form">
                    <div class="modal-body" >	  
                        <div class="clearfix"></div>
                        <div id="campaign_assigned_to_sp_table">
						<?php 
						 $campaigndetails = $reportObj->get_assigned_campaign_to_sp($user_id);
						// echo "<pre>";print_r($campaigndetails);
						/* $sqlsp = "SELECT tu.id,tu.firstname
                                        FROM tbl_user tu 
                                        where user_role='SalesPerson' AND  external_id='".$user_id."'";
						$resultsp = mysqli_query($con, $sqlsp); */
						?>
						<table class="table table-striped table-bordered table-hover" id="sample_sp_listing">
							<thead>
							<tr>
								<th>
									 Sales Person
								</th>
								<th>
									  Campaign Names
								</th>
                                <th>
									 Action
								</th>
							</tr>
							</thead>
							<tbody>	
								<?php foreach($campaigndetails as $sps=>$campaigns ){ 
								if(!empty($campaigns['campaign_names'])){
								?>
								<tr class="odd gradeX">	
									<td><?php echo $campaigns['firstname'];?></td>
									<td><?php echo $campaigns['campaign_names'];?> </td>
									<td> <a title="Delete" 
									onclick="javascript: deleteAssignedCampaign(<?=$campaigns['id'];?>)"  
									class="btn btn-xs btn-danger">
								  <span class="glyphicon glyphicon-trash"></span>
								  </a></td>
								</tr>	
								<?php } } ?>
							</tbody>
							</table>
						</div>
                    </div><!-- /.form-group --> 				
            </div>
            </form>	   	  
        </div>
    </div>
	<!--end-->
	
	
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
	jQuery(function () {
		$('input:checkbox').click(function () {
			var numberOfChecked = $('input:checkbox:checked').length;
				//alert(numberOfChecked);
			if(numberOfChecked>0){ 				
				$("#assigned_sp_listing_link").attr("disabled", true);					
			}else{				
				$("#assigned_sp_listing_link").attr("disabled", false);		
			}
		})
	});
	$(document).ready(function() {
		var table = $('#sample_2dgsd').dataTable({"bPaginate": false,destroy: true,stateSave: true});
		$('input[type=search]').on("input", function() {
			table.fnFilter('');
		});
		//$('#select_all').tooltip();
		
	} );
	
	function checktotalchecked(obj) {
		$("#select_th").removeAttr("class");
		if ($(obj).is(':checked') == true)
		{			
			var checkbox_count = ($('input:checkbox').length) - 1;
			var check_count = ($('input:checkbox:checked').length);
			if (obj.value == '') {				
				$('input:checkbox').attr('checked', true);
			} else if (checkbox_count == check_count)				
				$('input:checkbox').attr('checked', true);			
		} else {
			$('#select_all').attr('checked', false);
			if (obj.value == '')
				$('input:checkbox').attr('checked', false);
		}			
	}
	
	function assigned_sp_listing() {
		var rowCount = $('#sample_2dgsd tr').length;
		console.log(rowCount);
		if (rowCount > 0) {
			$('#assignedSPModal').modal('show');
			//$('#campaign_assigned_to_sp_table').html($('.portlet-body').html()); 
		} else {
			alert("Campaign not assigned to sales person.");
			//location.reload();
		}
    }
    function assign_sp() {
		var void1 = [];
		$('input[name="select_all[]"]:checked').each(function () {
			if(this.value!=''){void1.push(this.value);}
		});
		var campaign_ids = void1.join();
		if (campaign_ids != '') {
			$('#assignSPModal').modal('show');
			$('#input_campaign_ids').val(campaign_ids); 
		} else {
			alert("Please select minimum one campaign.");
			//location.reload();
		}
    }
    $('form#assign_sp_form').submit(function () {		
		var sp_ids=$('#sp_ids').val();
		$('#sp_ids_selected').val(sp_ids);
         var formData = new FormData($(this)[0]);		
        if(sp_ids !== null && sp_ids !== '' && sp_ids != 0) {
            $.ajax({
                url: "assign_campaign.php",
                type: 'POST',
				data: formData,
                success: function (data) {
                    console.log(data);
                     if (data > 0) {
                        alert('Campaign assigned to Sales Person successfully.');
                        $('#assignSPModal').modal('hide');
                        document.forms.assign_sp_form.reset();
                        window.location.reload(true);
                    }  else {
                        alert('Unable to assign to Sales Person.');
                        return false;
                    } 
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }else{
			alert('Please Select Sales Person.');               
			return false;
        } 
    });
</script>
<script>
	function deleteAssignedCampaign(sp_id) {
		if (confirm('Are you sure you want to unassigned campaign(s) ?')) {
			CallAJAX('ajax_product_manage.php?action=delete_assigned_campaign&sp_id=' + sp_id );
		}
	}
//delete_assigned_campaign
	function CallAJAX(url) {
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				alert('Assigned campaign unassigned successfully.');
				location.reload();
			}
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}
</script>
</body>
<!-- END BODY -->
</html>