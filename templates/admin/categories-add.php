<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/commonManage.php";	
$user_type = $_SESSION[SESSION_PREFIX.'user_type'];
$user_role = $_SESSION[SESSION_PREFIX.'user_role'];
$user_id = $_SESSION[SESSION_PREFIX.'user_id'];
?>
<!-- END HEADER -->
<?php
if(isset($_POST['submit']))
{
	$brand_id=$_POST['brandid'];
	$categorynm=fnEncodeString($_POST['categorynm']);
    $createdon = date('Y-m-d');
    $file=$_FILES['categoryimage']['name'];
	//$folder = "upload/";
	 //create folder for upload product image
            $directoryName = 'upload/'.COMPANYNM.'_upload/'; 
            //Check if the directory already exists.
            if(!is_dir($directoryName)){
            //Directory does not exist, so lets create it.
            mkdir($directoryName, 0755, true);
            }
    $folder = "upload/".COMPANYNM."_upload/";  
        
	$upload_image = $folder . basename($_FILES["categoryimage"]["name"]);
	$str=move_uploaded_file($_FILES["categoryimage"]["tmp_name"], $upload_image);
	$sql = "INSERT INTO `tbl_category` (brandid,categorynm,categoryimage,added_by_userid,added_by_usertype,added_by_userrole,createdon) VALUES('$brand_id','$categorynm','$file','$user_id','$user_type','$user_role','$createdon')";
	$category_sql=mysqli_query($con,$sql);	
	$record_id = mysqli_insert_id($con); 
	$commonObj 	= 	new commonManage($con,$conmain);
	$commonObj->log_add_record('tbl_category',$record_id,$sql);	
	echo '<script>alert("Category added successfully.");location.href="categories.php";</script>';
}
?>

<body class="page-header-fixed page-quick-sidebar-over-content ">

<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageProducts"; $activeMenu = "Categories";
	include "../includes/sidebar.php";
	 $commonObj 	= 	new commonManage($con,$conmain);
	$row_url=$commonObj->getPageIDforUrlAdd($php_page_name);
	$page_id_url = $row_url['page_id'];
	$row_url_add=$commonObj->getURLforAdd($profile_id,$page_id_url);
	$ischecked_add_url = $row_url_add['ischecked_add'];
    if ($ischecked_add_url == 0 && $ischecked_add_url!='') 
	{
		session_set_cookie_params(0);
		session_start();
		session_destroy();
		echo '<script>location.href="../login.php";</script>';
	    exit;
	}
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Categories
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="categories.php">Categories</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Category</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Category
							</div>
							
						</div>
						<div class="portlet-body">
                       <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
                          
  <form class="form-horizontal" data-parsley-validate="" id="form" enctype="multipart/form-data" role="form" method="post" action="categories-add.php">       
            <div class="form-group">
              <label class="col-md-3">Brand:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <select name="brandid"
                data-parsley-trigger="change"				
                data-parsley-required="#true" 
                data-parsley-required-message="Please select brand"
				class="form-control">
                      <option selected disabled>Select</option>
					<?php
					$sql="SELECT * FROM tbl_brand WHERE isdeleted != 1";
					$result = mysqli_query($con,$sql);
					while($row = mysqli_fetch_array($result))
					{
					$brand_id=$row['id'];
					echo "<option value='$brand_id'>" . fnStringToHTML($row['name']) . "</option>";
					}
					?>
                </select>
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <label class="col-md-3">Category Name:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="categorynm" 
				placeholder="Enter Category Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter category name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				class="form-control">
              </div>
            </div><!-- /.form-group -->
            
            
             <div class="form-group">
              <label class="col-md-3">Category Image:</label>

              <div class="col-md-4">
                <input type="file" id="categoryimage" 
				data-parsley-trigger="change"				
				data-parsley-fileextension='png,jpeg,jpg' 
				data-parsley-max-file-size="1000"  
				name="categoryimage">
              </div>
            </div><!-- /.form-group -->
            
			
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                <a href="categories.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group -->
            
            

          </form>  
                            
                            
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->

<script>
$(document).ready(function() {
    window.ParsleyValidator
        .addValidator('fileextension', function (value, requirement) {
            // the value contains the file path, so we can pop the extension
			var arrRequirement = requirement.split(',');			
            var fileExtension = value.split('.').pop();
			var result = jQuery.inArray( fileExtension, arrRequirement );
			 
			if(result==-1)
            return false;
			else 
			return true;
        }, 32)
        .addMessage('en', 'fileextension', 'Only png or jpg image are allowed');	
window.Parsley.addValidator('maxFileSize', {
  validateString: function(_value, maxSize, parsleyInstance) {
    var files = parsleyInstance.$element[0].files;
    return files.length != 1  || files[0].size <= maxSize * 1024;
  },
  requirementType: 'integer',
  messages: {
    en: 'Image should not be larger than %s Kb',
  }
});
});
</script>

</body>
<!-- END BODY -->
</html>