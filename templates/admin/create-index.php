CREATE UNIQUE INDEX USER_INDEX
ON tbl_user (id);


SHOW INDEXES FROM tbl_orders;



EXPLAIN SELECT 
    employeeNumber, 
    lastName, 
    firstName
FROM
    tbl_orders
WHERE
    jobTitle = 'Sales Rep';


    CREATE INDEX order_status ON tbl_order_details(order_status);
    CREATE INDEX order_to ON tbl_orders(order_to);
    CREATE INDEX order_from ON tbl_orders(order_from);
    CREATE INDEX order_id ON tbl_order_details(order_id);


     CREATE INDEX id ON tbl_user(id);

DROP INDEX index_name
  ON table_name;


     SHOW INDEXES FROM tbl_order_details;

     CREATE INDEX index_name ON table_name (column_list)

     CREATE TABLE t(
   c1 INT PRIMARY KEY,
   c2 INT NOT NULL,
   c3 INT NOT NULL,
   c4 VARCHAR(10),
   INDEX (c2,c3) 
);




	
SELECT ischecked_add,ischecked_edit,	ischecked_delete FROM tbl_action_profile where profile_id='1' AND  page_id='3'


SELECT ap.ischecked_view,ap.page_id,ap.profile_id,p.page_name,p.php_page_name FROM tbl_action_profile ap 
LEFT JOIN tbl_pages p ON p.id = ap.page_id
where ap.profile_id='1' 


CREATE INDEX profile_id ON tbl_action_profile(profile_id);
CREATE INDEX page_id ON tbl_action_profile(page_id);

tbl_user_location
CREATE INDEX userid ON tbl_user_location(userid);
CREATE INDEX tdate ON tbl_user_location(tdate);



CREATE INDEX user_type ON tbl_user(user_type);
CREATE INDEX external_id ON tbl_user(external_id);














CREATE INDEX user_type ON tbl_user(user_type);
CREATE INDEX external_id ON tbl_user(external_id);
CREATE INDEX userid ON tbl_user_location(userid);
CREATE INDEX tdate ON tbl_user_location(tdate);
CREATE INDEX profile_id ON tbl_action_profile(profile_id);
CREATE INDEX page_id ON tbl_action_profile(page_id);
CREATE INDEX order_status ON tbl_order_details(order_status);
CREATE INDEX order_to ON tbl_orders(order_to);
CREATE INDEX order_from ON tbl_orders(order_from);
CREATE INDEX order_id ON tbl_order_details(order_id);
CREATE INDEX section_id ON tbl_pages(section_id)
 CREATE INDEX common_user_id ON tbl_user(common_user_id);


// new indexing
CREATE INDEX shop_added_by ON tbl_shops(shop_added_by);
CREATE INDEX id ON tbl_shops(id);
CREATE INDEX subarea_id ON tbl_subarea(subarea_id);
CREATE INDEX suburb_id ON tbl_subarea(suburb_id);
CREATE INDEX id ON tbl_city(id);
CREATE INDEX state_id ON tbl_city(state_id);
CREATE INDEX id ON tbl_area(id);
CREATE INDEX id ON tbl_state(id);

CREATE INDEX id ON tbl_product(id);
CREATE INDEX catid ON tbl_product(catid);
CREATE INDEX added_by_userid ON tbl_product(added_by_userid);

CREATE INDEX id ON tbl_category(id);
CREATE INDEX brandid ON tbl_category(brandid);
CREATE INDEX added_by_userid ON tbl_category(added_by_userid);
CREATE INDEX isdeleted ON tbl_category(isdeleted);

 CREATE INDEX `product_id` ON tbl_stock_management(`product_id`);


  //below query for common database 

   CREATE INDEX username ON tbl_users(username);   
   CREATE INDEX passwd ON tbl_users(passwd);
    CREATE INDEX isdeleted ON tbl_user(isdeleted);

ALTER TABLE tbl_users
  DROP INDEX passwd;


DELIMITER $$
 
CREATE PROCEDURE Get_tbl_product()
BEGIN
 SELECT productname, added_on
 FROM tbl_product;
    END$$

  /*  $get_product_sql = 'CALL GetAllProduct()';
                                           $result_product = mysqli_query($con, $get_product_sql);*/


SELECT distinct o.id as oid, o.order_no, o.invoice_no, o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.order_to) AS order_to_name, o.order_date, o.shop_id, s.name AS shop_name, s.suburbid, (SELECT sb.suburbnm FROM tbl_area AS sb WHERE sb.id = s.suburbid) AS region_name, (select suburb_ids from tbl_user_view tuv where tuv.id =o.ordered_by) as suburbid1, (select suburbnm from tbl_user_view tuv where tuv.id =o.ordered_by ) AS region_name1 , o.total_items, o.total_cost, o.total_order_gst_cost, o.lat, o.long, o.offer_provided,(SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.order_from) AS order_from_name, o.sended_to_production FROM `tbl_orders` AS o LEFT JOIN tbl_shops AS s ON s.id = o.shop_id LEFT JOIN tbl_order_details AS od ON od.order_id = o.id WHERE 1=1 AND find_in_set(o.order_from,'1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49') <> 0 ORDER BY o.order_date DESC


EXPLAIN SELECT distinct o.id as oid, o.order_no, o.invoice_no, o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.order_to) AS order_to_name, o.order_date, o.shop_id, s.name AS shop_name, s.suburbid, (SELECT sb.suburbnm FROM tbl_area AS sb WHERE sb.id = s.suburbid) AS region_name, (select suburb_ids from tbl_user_view tuv where tuv.id =o.ordered_by) as suburbid1, (select suburbnm from tbl_user_view tuv where tuv.id =o.ordered_by ) AS region_name1 , o.total_items, o.total_cost, o.total_order_gst_cost, o.lat, o.long, o.offer_provided,(SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.order_from) AS order_from_name, o.sended_to_production FROM `tbl_orders` AS o LEFT JOIN tbl_shops AS s ON s.id = o.shop_id LEFT JOIN tbl_order_details AS od ON od.order_id = o.id WHERE 1=1 AND od.order_status = '1' AND o.order_to ='1' AND shop_id IS NOT NULL ORDER BY o.order_date DESC


SELECT id,status,shop_status,status_seen_log, shop_added_by_name, shop_name,contact_person,mobile,cityname,statename,suburbnm FROM `tbl_shop_view` where find_in_set('1',parent_ids) <> 0 OR shop_added_by=1