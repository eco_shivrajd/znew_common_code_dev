<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/commonManage.php";	
include "../includes/sectionManage.php";
$sectionObj = new sectionManager($con, $conmain);

$user_type = $_SESSION[SESSION_PREFIX.'user_type'];
$user_id = $_SESSION[SESSION_PREFIX.'user_id'];
?>
<!-- END HEADER -->
<?php
if(isset($_POST['submit']))
{	
	$sectionObj->addPage();	
	echo '<script>alert("page added successfully.");location.href="page-add.php";</script>';
}
?>

<body class="page-header-fixed page-quick-sidebar-over-content ">

<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageProducts"; $activeMenu = "Page";
	include "../includes/sidebar.php";
	$commonObj 	= 	new commonManage($con,$conmain);
	$row_url=$commonObj->getPageIDforUrlAdd($php_page_name);
	$page_id_url = $row_url['page_id'];
	$row_url_add=$commonObj->getURLforAdd($profile_id,$page_id_url);
	$ischecked_add_url = $row_url_add['ischecked_add'];
    if ($ischecked_add_url == 0 && $ischecked_add_url!='') 
	{
		session_set_cookie_params(0);
		session_start();
		session_destroy();
		echo '<script>location.href="../login.php";</script>';
	    exit;
	}
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Page
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="page-list.php">Page</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Page</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Page
							</div>
							
						</div>
						<div class="portlet-body">
                       <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
                          
  <form class="form-horizontal" data-parsley-validate="" id="form" enctype="multipart/form-data" role="form" method="post" action="page-add.php">       
            <div class="form-group">
              <label class="col-md-3">Section Name:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <select name="section_id"
                data-parsley-trigger="change"				
                data-parsley-required="#true" 
                data-parsley-required-message="Please select brand"
				class="form-control">
                      <option selected disabled>Select</option>
					<?php
					$result = $sectionObj->getAllSection();
					while($row = mysqli_fetch_array($result))
					{
					$section_id=$row['id'];
					echo "<option value='$section_id'>" . fnStringToHTML($row['section_name']) . "</option>";
					}
					?>
                </select>
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <label class="col-md-3">Page Name:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="page_name" 
				placeholder="Enter page Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter page name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				class="form-control">
              </div>
            </div><!-- /.form-group -->            
            

             <div class="form-group">
              <label class="col-md-3">Page Active Status:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="page_active_status" 
				placeholder="Enter Page Active Status"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please Page Active Status"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				class="form-control">
              </div>
            </div><!-- /.form-group --> 

             <div class="form-group">
              <label class="col-md-3">PHP Page Name:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="php_page_name" 
				placeholder="E.g. index.php"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter page name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				class="form-control">
              </div>
            </div><!-- /.form-group -->   
			
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                <a href="page-list.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group -->
            
            

          </form>  
                            
                            
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->

</body>
<!-- END BODY -->
</html>