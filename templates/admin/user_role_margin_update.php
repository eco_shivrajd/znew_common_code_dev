<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/commonManage.php";
if($_SESSION[SESSION_PREFIX.'user_type']!="Admin") 
{
	header("location:../logout.php");
}
$id=base64_decode($_GET['id']);
$u_role=base64_decode($_GET['u_role']);
if(isset($_POST['submit']))
{
    $userrole_margin=fnEncodeString($_POST['userrole_margin']);
	$sql = "UPDATE tbl_userrole SET userrole_margin='$userrole_margin' where id='$id'";
	$update_sql=mysqli_query($con,$sql);
	$commonObj 	= 	new commonManage($con,$conmain);
	$commonObj->log_update_record('tbl_userrole',$id,$sql);
	echo '<script>alert("User Role Margin Updated successfully.");location.href="user_role_margin.php";</script>';	
}
$sql1="SELECT userrole_margin  FROM tbl_userrole where id = '$id' ";
$result1 = mysqli_query($con,$sql1);
$row1 = mysqli_fetch_array($result1);
$userrole_margin = fnStringToHTML($row1['userrole_margin']);
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php 
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "User-Role-Margin";
	include "../includes/sidebar.php";

	$commonObj 	= 	new commonManage($con,$conmain);
	$row_url=$commonObj->getPageIDforUrlEdit($php_page_name);
	$page_id_url = $row_url['page_id'];
	$row_url_edit=$commonObj->getURLforEdit($profile_id,$page_id_url);
	$ischecked_edit_url = $row_url_edit['ischecked_edit'];
    if ($ischecked_edit_url == 0 && $ischecked_edit_url!='') 
	{
		session_set_cookie_params(0);
		session_start();
		session_destroy();
		echo '<script>location.href="../login.php";</script>';
	    exit;
	}
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->			
			<h3 class="page-title">User Role Margin</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					
					<li>
						<i class="fa fa-home"></i>
						<a href="user_role_margin.php">User Role Margin</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit User Role Margin</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit User Role Margin
							</div>							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						  
						<form class="form-horizontal" data-parsley-validate="" role="form" method="post"> 

			            <div class="form-group">
						  <label class="col-md-3">User Role:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text" disabled="" 
							placeholder="Enter User Role"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter Taluka name"
							data-parsley-maxlength="50"
							data-parsley-maxlength-message="Only 50 characters are allowed"
							name="u_role" value="<?php echo $u_role;?>"class="form-control">
						  </div>
						</div><!-- /.form-group -->				
						
						<div class="form-group">
						<label class="col-md-3">Margin(%):<span class="mandatory">*</span></label>
						<div class="col-md-4">
						<input type="text" name="userrole_margin" class="form-control" onkeyup="myFunction(this)"
						placeholder="Enter Margin"
						min="0" max="100" step="0.01" 
						data-parsley-trigger="change"                         
						data-parsley-pattern="^[0-9]+?[\.0-9]*$" value="<?php echo $userrole_margin;?>" required >                
						<!--data-parsley-pattern="^(?!\s)[0-9-!@#$%^&*+_=><,./:' ]*$" -->
						</div>
						</div>
						<div class="form-group">
						  <div class="col-md-4 col-md-offset-3">
						   <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
							<a href="user_role_margin.php" class="btn btn-primary">Cancel</a>
						  </div>
						</div><!-- /.form-group -->
					  </form>                                       
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<style type="text/css">
	#pageloader
{
  background: rgba( 255, 255, 255, 0.8 );
  display: none;
  height: 100%;
  position: fixed;
  width: 100%;
  z-index: 9999;
}

#pageloader img
{
  left: 50%;
  margin-left: -32px;
  margin-top: -32px;
  position: absolute;
  top: 50%;
}
</style>

<div id="pageloader">
   <img src="http://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" alt="processing..." />
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->

<style>
.form-horizontal{
font-weight:normal;
}
</style>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

<script type="text/javascript">
	$(document).ready(function(){
  $("#myform").on("submit", function(){
    $("#pageloader").fadeIn();
  });//submit
});//document ready
</script>
<script type="text/javascript">
function myFunction(varl) {
if (varl.value != "") {
var arrtmp = varl.value.split(".");
if (arrtmp.length > 1) {
	var strTmp = arrtmp[1];
	if (strTmp.length > 2) {
					varl.value = varl.value.substring(0, varl.value.length - 1);
	}
}
}
}
</script>
