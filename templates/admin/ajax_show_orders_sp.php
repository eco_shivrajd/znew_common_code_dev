<?php 
include ("../../includes/config.php");
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);

$order_status = $_POST['order_status'];
$order_id = $_POST['order_id'];
$orders = $orderObj->getOrderschnage1_sp($order_status,$order_id);
$order_count = count($orders);
$user_type=$_SESSION[SESSION_PREFIX . "user_type"];
$session_user_id=$_SESSION[SESSION_PREFIX . "user_id"];

?>
<div class="clearfix"></div>
<table class="table table-striped table-bordered table-hover" id="sample_2">
	<thead>
		<tr id="main_th">
			<?php if(($order_status == '1' ||$order_status == '11')&& $order_count != 0 ){ ?>
			<th id="select_th">
				<input type="checkbox" name="select_all[]" id="select_all" onchange="javascript:checktotalchecked(this)" ><!-- -->
			</th>
			<?php  } ?>
			<th>Region</th>
			<th>Order From</th>
			<th>Order To</th>
			<th>Order Id<br>Order Date </th>		
			<th>Quantity</th>
			<th>Total Price (₹)</th>
			<th>Price(₹)<br>(GST+Discount)</th>
			<?php if($order_status == '0'||$order_status == '10'){  ?>
				<th>Order Status</th>
			<?php } ?>
			<th>Payment Status</th>
			<th>Action  </th>
		</tr>
		</thead>
		<tbody>
		<?php				
		foreach($orders as $key=>$value)
		{
			 $order_totalcost = 0;
			 $order_totalgstcost = 0;
			$prod_qnty_total=0;
			$orderdetails = $orderObj->getOrdersDetailschnage($value['oid']);	
			$oid = $value['oid'];
			$product_name='';
			$prod_qnty='';$totalcost='';$gstcost='';
			if(count($orderdetails['order_details'])==1){
				$order_status_myorders=$orderdetails['order_details'][0]['order_status'];
				$payment_status=$orderdetails['order_details'][0]['payment_status'];
				$payment_date=$orderdetails['order_details'][0]['payment_date'];
				$payment_comment=$orderdetails['order_details'][0]['comment'];
				if($session_user_id==$orderdetails['order_details'][0]['added_by_userid']&& $_SESSION[SESSION_PREFIX . "user_role"]!='Admin'){
					$product_name ="<span id='tooltipid' title='This product can not be sent to production.' style='color:#6a6e75;'>".$orderdetails['order_details'][0]['cat_name']."-".$orderdetails['order_details'][0]['product_name']."</span><br>";
					//$product_name=$orderdetails['order_details'][0]['cat_name']."-".$orderdetails['order_details'][0]['product_name']."<br>my";
				}else{
					$product_name=$orderdetails['order_details'][0]['cat_name']."-".$orderdetails['order_details'][0]['product_name']."<br>";
				}
				
				
				$prod_qnty='<a onclick="showOrderDetails(\''.$orderdetails['order_details'][0]['odid'].'\',\'Order Product Details\')" title="Order Product Details">'.$orderdetails['order_details'][0]['product_quantity'];
				if(!empty($orderdetails['order_details'][0]['product_variant_weight1'])){
					$prod_qnty.="(".$orderdetails['order_details'][0]['product_variant_weight1']."-".$orderdetails['order_details'][0]['product_variant_unit1'].")";
				}
				if(!empty($orderdetails['order_details'][0]['product_variant_weight2'])){
					$prod_qnty.="(".$orderdetails['order_details'][0]['product_variant_weight2']."-".$orderdetails['order_details'][0]['product_variant_unit2'].")";
				}
				$prod_qnty.="</a>";	
				$campaign_applied=$orderdetails['order_details'][0]['campaign_applied'];
				$campaign_type=$orderdetails['order_details'][0]['campaign_type'];
				if($campaign_applied == 0 && $campaign_type=='free_product'){
					$prod_qnty.="<span style='float: left'><img src='../../assets/global/img/free-icon.png' title='Free Product'></span>";	
				}
				$prod_qnty.="<br>";
				$prod_qnty_total=$orderdetails['order_details'][0]['product_quantity'];
				
				$totalcost=$orderdetails['order_details'][0]['product_total_cost'];
				
				if($orderdetails['order_details'][0]['discounted_totalcost']!=0 && $orderdetails['order_details'][0]['discounted_totalcost']!=''){
					$totalcost.="(<font color='green'>".$orderdetails['order_details'][0]['discounted_totalcost']."</font>)";
				}
				$totalcost.="<br>";
				 $order_totalcost= $orderdetails['order_details'][0]['product_total_cost'];
				
				$gstcost=$orderdetails['order_details'][0]['p_cost_cgst_sgst']."<br>";
				$order_totalgstcost = $orderdetails['order_details'][0]['p_cost_cgst_sgst'];
			}else{
				foreach ($orderdetails['order_details'] as $key1=>$value1){
					$order_status_myorders=$orderdetails['order_details'][0]['order_status'];
					$payment_status=$orderdetails['order_details'][0]['payment_status'];
					$payment_date=$orderdetails['order_details'][0]['payment_date'];
					$payment_comment=$orderdetails['order_details'][0]['comment'];
					if($session_user_id==$value1['added_by_userid']&& $_SESSION[SESSION_PREFIX . "user_role"]!='Admin'){						
						$product_name.="<span id='tooltipid' title='This product can not be sent to production.' style='color:#6a6e75;'>".$value1['cat_name']."-".$value1['product_name']."</span><br>";
						//$product_name.=$value1['cat_name']."-".$value1['product_name']."<br>";
					}else{
						$product_name.=$value1['cat_name']."-".$value1['product_name']."<br>";
					}
					
					$prod_qnty.='<a onclick="showOrderDetails(\''.$value1['odid'].'\',\'Order Product Details\')" title="Order Product Details">'.$value1['product_quantity'];
					if(!empty($value1['product_variant_weight1'])){
						$prod_qnty.="(".$value1['product_variant_weight1']."-".$value1['product_variant_unit1'].")";
					}
					if(!empty($value1['product_variant_weight2'])){
						$prod_qnty.="(".$value1['product_variant_weight2']."-".$value1['product_variant_unit2'].")";
					}
					$prod_qnty.="</a>";
					$campaign_applied=$value1['campaign_applied'];
					$campaign_type=$value1['campaign_type'];
					if($campaign_applied == 0 && $campaign_type=='free_product'){
						$prod_qnty.="<span style='float: left'><img src='../../assets/global/img/free-icon.png' title='Free Product'></span>";	
					}
					$prod_qnty.="<br>";
					$totalcost.=$value1['product_total_cost'];
					
					$prod_qnty_total=$prod_qnty_total+$value1['product_quantity'];
					 $order_totalcost = $order_totalcost + $value1['product_total_cost'];
					 
					if($value1['discounted_totalcost']!=0 && $value1['discounted_totalcost']!=''){
						$totalcost.="(<font color='green'>".$value1['discounted_totalcost']."</font>)";
					}
					$totalcost.="<br>";
					$gstcost.=$value1['p_cost_cgst_sgst']."<br>";
					$order_totalgstcost = $order_totalgstcost + $value1['p_cost_cgst_sgst'];
				}
			}
		?>
		<tr class="odd gradeX">
		<?php if(($order_status == '1' ||$order_status == '11')&&  $order_count != 0 ){  ?>
			<td>
				<input type="checkbox" class="mycheckbox" name="select_all[]" id="select_all" onchange="javascript:checktotalchecked(this)" value="<?php echo $value["oid"];?>">
			</td>
		<?php } ?>
			<td ><?php if(!empty($value["region_name"])){echo $value["region_name"];}else{echo $value["region_name1"];} ?></td>
			<td ><?php echo $value["order_by_name"];if(!empty($value["shop_name"])){echo "-".$value["shop_name"];}?></td>					
			<td ><?php echo $value["order_to_name"];?></td>
			<td ><font size="1.4"> <?php echo $value["order_no"]?></font><br><?php echo date('d-m-Y h:i:s A',strtotime($value["order_date"]));?></td>										
			
			<td  align="right" >
				<?php 
					echo '<a onclick="showOrderDetails(\''.$order_status.'\',\''.$value['oid'].'\')" title="Order Details">'.$prod_qnty_total.'<a>';?>
			</td>
			<td align="right"><b><?php echo number_format((float)$order_totalcost, 2, '.', '');?></b></td>
			<td align="right"><b><?php echo number_format((float)$order_totalgstcost, 2, '.', '');?></b> </td>
		<?php if($order_status == '0'||$order_status == '10'){  ?>
			<td><b><?php $order_status_switch_array=array('1'=>'Pending','2'=>'Delivery Assigned','4'=>'Delivered');
			echo $order_status_switch_array[$order_status_myorders];?></b></td>
		<?php } ?>
			<td align="left">
				<?php $payment_comment=$orderdetails['order_details'][0]['comment'];
				$payment_date=date('d-m-Y',strtotime($payment_date));
				if($payment_status=='confirmed'){?>
					<a title="Payment Received <?php if($payment_comment!=''){echo ":".$payment_comment;}?>."  
					class="btn btn-xs btn-success" style="background-color:#0e8836" disabled>
					<span class="glyphicon glyphicon-ok"></span> Received 
					</a><br><?php echo $payment_date;?>
				<?php }else if($payment_status=='delay'){?>
					<a title="<?php if($payment_comment!=''){echo "Comment:".$payment_comment;}else{echo "No Comment";}?>."  
					class="btn btn-xs btn-danger" style="background-color:#e2383f" disabled>
					<span class="glyphicon glyphicon-time"></span> Pending
					</a><br><?php echo $payment_date;?>
				<?php } else{echo "NA";} ?>
			</td>
			<td><!--<a onclick='showInvoice(1,<?=json_encode($oid);?>)'  title="View Invoice">View Invoice</a>-->
				<a title="View Invoice" onclick='showInvoice(1,<?=json_encode($oid);?>)' class="btn btn-xs btn-success ">
					<span class="glyphicon glyphicon-eye-open"></span>
				</a>
				<!--<?php if($order_status==1 || $order_status==11){ ?>
				<a 	title="Delete" onclick='javascript: deleteOrder(<?=json_encode($oid);?>)'  
					class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span>
				</a><?php } ?>-->
			</td>
		</tr>
		<?php } ?>
		
		 </tbody>
	</table>
<script>
$(document).ready(function() {
	 $("#sample_2").dataTable().fnDestroy();			
    $('#sample_2').dataTable( {
	order: [],
	columnDefs: [ { orderable: false, targets: [0] } ]
	});
});

</script>
