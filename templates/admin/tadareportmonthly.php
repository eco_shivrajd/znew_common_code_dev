<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);

$today = date('d-m-Y');
if($today <= date('15-m-Y')){
	$frmdate = date("01-m-Y");
	$todate  = date("d-m-Y");
}else{
	$frmdate = date("16-m-Y");
	$todate  =  date("d-m-Y", strtotime("last day of this month"));
}
?>
<!-- END HEADER -->
 <link href="../../assets/global/css/jquery.loader.css" rel="stylesheet" />
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Reports"; $activeMenu = "tadareportmonthly";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Expense Report
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Expense Report</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->			
			<div class="row">
				<div class="col-md-12"> 				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">Search Criteria</div>
						</div>
						<div class="portlet-body">
						<form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">
													
							
							<div class="form-group">
								<label class="col-md-3">Date Options:</label>
								<div class="col-sm-4">						
									<select class="form-control" name="selTest" id="selTest" onChange="fnSelectionBoxTest()">
									<option value='3' <?php if($_REQUEST['selTest']=="3")echo 'selected';?>>From specific date</option>
									</select>
								</div>
							</div>
							<div class="form-group" id="date-show" >
								<label class="col-md-3">Date:</label>
								<div class="col-md-8 nopadl">
									<div class="col-md-1" class="nopadl" style="margin-right:-17px;">
										<label class="nopadl" style="padding-top:5px;">From:</label>
									</div>
									<div class="col-md-3">										
										<div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
											<input type="text" class="form-control" name="frmdate" id="frmdate" value="<?php echo $frmdate;?>" readonly >
											<span class="input-group-btn">
											<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>			
									<div class="col-md-1" class="nopadl" style="margin-right:-35px;margin-left:-8px;">
										<label class="nopadl" style="padding-top:5px">To:</label>
									</div>
									<div class="col-md-3">
										<div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
											<input type="text" class="form-control" name="todate" id="todate" value="<?php echo $todate;?>" readonly >
											<span class="input-group-btn">
											<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>						
								</div>                          
							</div>  
						
							<div class="form-group">
								<div class="col-md-4 col-md-offset-3">
									<button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="$('#loader_div').loader('show'); ShowReport();">Search</button>
									
									<button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnChangeReportType();">Reset</button>
								</div>
							</div><!-- /.form-group -->
						</form>	
						</div>
					   <div class="clearfix"></div>
					</div>
					<div id="loader_div"></div>
					<div id="divReportHTML"></div>
		</div>			
	</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div id="print_div"  style="display:none;"></div>
<form action="../includes/exportToExcel.php" method="post" name="export_excel" id="export_excel">
	<input type="hidden" name="export_data" id="export_data">
	<input type="hidden" name="file_name" id="file_name">
</form>
<?php include "../includes/footer.php"?>

<script>
function CallAJAX(url,assignDivName) {
	if (window.XMLHttpRequest)
	{
		var xmlhttp=new XMLHttpRequest();
	} else {
		var xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById("" + assignDivName +"").innerHTML	=	xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET",url,true);
	xmlhttp.send();	
};
$(document).ready(function() {
	ShowReport();
});
function fnChangeReportType(){
	 location.reload();
}
function fnSelectionBoxTest()
{
	if($('#selTest').val() == '3')
	{
	   $('#date-show').show();
	}
	else
	{
		$('#date-show').hide();
		ShowReport();
	}
}
function ShowReport() {
	$("#loader_div").loader('show');
	var url = "tadareportmonthlyajax.php";	
	var data = $('#frmsearch').serialize();
	console.log(data);
	//var date = $('#frmdate').val();
	/* var salespersonid = $('#dropdownSalesPerson').val();
	
	if(salespersonid == ''){
		alert('Please select Sales Person.');
		$('#dropdownSalesPerson').focus();
		$("#loader_div").loader('hide');
		return false;
	} */
	var selTest = $('#selTest').val();	
	if(selTest == 3)
	{
		var frmdate = $('#frmdate').val();		
		var todate = $('#todate').val();		
		var validation = compaire_dates(frmdate,todate);
	}

	if(selTest == 3 && frmdate != '' && todate != '' && validation == 1)
	{
		alert("'From' date should not be greater than 'To' date.");
		$("#loader_div").loader('hide');
		return false;
	}
	
    jQuery.ajax({
		url: url,
		method: 'POST',
		data: data,
		async: false
	}).done(function (response) {
		console.log(response);
		$("#divReportHTML").show();
		$('#divReportHTML').html(response);
		$("#loader_div").loader('hide');
	}).fail(function () {$("#loader_div").loader('hide'); });
	
	return false;
}

function ExportToExcel() {
	var td_rec = $("#report_table td:last").html();
	if(td_rec != 'No matching records found')
	{
		var divContents = $("#dvtblResonsive").html();
		$("#print_div").html('<table id="print_table" style="text-decoration:none;">'+divContents+'</table>');	
		
		divContents =  $("#print_div").html();
		divContents = divContents.replace(/₹/g,'Rs');
		var file_name = "Report_"+$("#salespname").val()+"_"+$("#date").val()+".xls";
		$("#file_name").val(file_name);
		$("#export_data").val(divContents);
		document.forms.export_excel.submit();
	}else{
		alert("No matching records found");
	}
}

$('.date-picker1').datepicker({
	rtl: Metronic.isRTL(),
	orientation: "left",
	endDate: "<?php echo date('d-m-Y');?>",
	autoclose: true
});
function takeprint() {
	var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
	var divContents = '<style type="text/css">@page{size: landscape;}\	table { border-collapse: collapse; }\
	table, th, td {  border: 1px solid black; }\
	body { font-family: "Open Sans", sans-serif;\
	background-color:#fff;\
	font-size: 11px;\
	direction: ltr;}</style>' + $("#dvtblResonsive").html();
	
	
	if(isIE == true){
		var printWindow = window.open('', '', '');
		printWindow.document.write(divContents);
		printWindow.focus();
		printWindow.document.execCommand("print", false, null);
	}else{
		$('<iframe>', {
			name: 'myiframe',
			class: 'printFrame'
		}).appendTo('body').contents().find('body').html(divContents);
		window.frames['myiframe'].focus();
		window.frames['myiframe'].print();		
		setTimeout(
		function() 
		{
			$(".printFrame").remove();
		}, 1000);
	}
	
};
function view_expense_report(){	
	$("#loader_div").loader('show');
	$('#view_expense_report').modal('show');
	$("#loader_div").loader('hide');
	/*
	var url = "single_tada_report.php";
	jQuery.ajax({
		url: url,
		method: 'POST',
		//data: data,
		async: false
	}).done(function (response) {
		
		$('#view_expense_report').modal('show');
		$('#model_content').html(response);
		$("#loader_div").loader('hide');
	}).fail(function () { });*/
	
}
</script>


<script type="text/javascript" src="../../assets/global/scripts/jquery.loader.js"></script>
<div id="loader_div"></div>

<div class="modal fade" id="view_expense_report" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content" id="model_content">
         <div class="modal-header">
            <button type="button" name="btnPrint" id="btnPrint" onclick="takeprint()" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
            <? if($oplacelat!="" && $oplacelon!="" && $oplacelat!="0.0" && $oplacelon!="0.0") { ?>
            &nbsp;&nbsp;
            <button type="button" name="btnPrint" id="btnPrint" onclick="showGoogleMap('<?=$oplacelat;?>','<?=$oplacelon;?>')" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Location</button>
            <? } ?>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"></h4>
         </div>
         <div class="modal-body" style="padding-bottom: 5px !important;" id="divPrintARea">
            <div class="row">
               <div class="col-md-12">
                  <div class="portlet box blue-steel">
                     <div class="portlet-title ">
                        <div class="caption printHeading">
                           Expense Bill
                        </div>
                     </div>
                     <div class="portlet-body">
							<?php 
								$dir="../../uploads/01-02-2018/6523";
								if (is_dir($dir)) {									
									if(file_exists("../../uploads/01-02-2018/6523/bill_1.png")){
										echo '<img src="../../uploads/01-02-2018/6523/bill_1.png" />';
									}
									if(file_exists("../../uploads/01-02-2018/6523/bill_2.png")){
										echo '<img src="../../uploads/01-02-2018/6523/bill_2.png" />';
									}
									if(file_exists("../../uploads/01-02-2018/6523/bill_3.png")){
										echo '<img src="../../uploads/01-02-2018/6523/bill_3.png" />';
									}
								}
									
							
							?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>