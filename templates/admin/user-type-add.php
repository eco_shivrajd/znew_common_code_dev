<!-- BEGIN HEADER -->
<?php 
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);

include "../includes/header.php"; 
include "../includes/userConfigManage.php";
$userConfObj    =   new userConfigManager($con,$conmain);
//get all user role
$useroles=$userConfObj->get_alluserole();
$allusertreedata=$userConfObj->getAllusertreedata();
//echo "<pre>";print_r($allusertreedata);
$parentchield=$userConfObj->get_allparentchield();
//echo "<pre>";print_r($useroles);
?>
<?php
if(isset($_POST['submit'])){
	$parent_id = fnEncodeString($_POST['parent_id']);
	$user_role=fnEncodeString($_POST['user_role']);
	$user_type = fnEncodeString($_POST['user_type']);
	$usertype_margin = $_POST['usertype_margin'];
	$sql_user_check = mysqli_query($con, "select id from `tbl_user_tree` where user_type='$user_type' ");//and parent_id='$parent_id'
	if ($rowcount = mysqli_num_rows($sql_user_check) > 0){
		echo '<script>alert("User Type already exist.");location.href="user-type-add.php";</script>';
	}else{
		//$userObj->addUserType();
		$userConfObj->addUserType();
		//die();
		echo '<script>alert("User Type added successfully.");location.href="user-type-add.php";</script>';
	}
}
?> 
<!-- END HEADER -->

  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" />
  
   <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "UserType";
	include "../includes/sidebar.php";

	$commonObj 	= 	new commonManage($con,$conmain);
	$row_url=$commonObj->getPageIDforUrlAdd($php_page_name);
	$page_id_url = $row_url['page_id'];
	$row_url_add=$commonObj->getURLforAdd($profile_id,$page_id_url);
	$ischecked_add_url = $row_url_add['ischecked_add'];
    if ($ischecked_add_url == 0 && $ischecked_add_url!='') 
	{
		session_set_cookie_params(0);
		session_start();
		session_destroy();
		echo '<script>location.href="../login.php";</script>';
	    exit;
	}
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			User Type Add
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					 
					<li>
						<i class="fa fa-home"></i>
						<a href="user_type_list.php">User Type List</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New User Type</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New User Type
							</div>
							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						 
						<form id="myform" class="form-horizontal" data-parsley-validate="" role="form" method="post" action="user-type-add.php">         
									<div class="form-group">
										<label class="col-md-3">Select Parent:<span class="mandatory">*</span></label>
										<div class="col-md-4">
											<select name="parent_id" id="mySelect" class="form-control" onChange="fnShowUserRole(this.value)">
											</select>
											
										</div>
									</div><!-- /.form-group -->	
									<div class="form-group">
										<label class="col-md-3">Select User Role:<span class="mandatory">*</span></label>
										<div class="col-md-4">
											<select name="user_role" id="user_role" class="form-control" 
											data-parsley-required="#true" onChange="fnShowMargin(this.value)">
											<option value="">--- Select ----</option>					 
											<?php
											foreach($useroles as $key=>$value){
												$cat_id=$value['roleid'];
												echo "<option value='".$value['rolename']."' >" . fnStringToHTML($value['rolename']) . "</option>";												
											}
											?>
											</select>

										</div>
									</div><!-- /.form-group -->	
									
									
									<div class="form-group">
									  <label class="col-md-3">User Type :<span class="mandatory">*</span></label>
									  <div class="col-md-4">
										<input type="text" 
										placeholder="Enter User Type "
										data-parsley-trigger="change"				
										data-parsley-required="#true" 
										data-parsley-required-message="Please enter User Type "
										data-parsley-maxlength="50"
										data-parsley-maxlength-message="Only 50 characters are allowed"
										data-parsley-pattern="/^[A-Za-z0-9]+(?:[_-][A-Za-z0-9]+)*$/"
										data-parsley-pattern-message="User type should be alphanumeric characters & underscore, hyphen are allowed in between."
										name="user_type"class="form-control">
									  </div>
									</div><!-- /.form-group -->	
									<div class="form-group" id="margin_usertypediv" >
									  <label class="col-md-3">Margin(%):</label>
									  <div class="col-md-4">
										<input type="text" name="usertype_margin" class="form-control" onkeyup="myFunction(this)"
										placeholder="Enter Margin"
										min="0" max="100" step="0.01" 
										data-parsley-trigger="change"
										data-parsley-pattern="^[0-9]+?[\.0-9]*$" >
									  </div>
									</div>

									<div class="form-group" id="margin_div" style="display:none;">
				  <label class="col-md-3">Margin(%) Rolewise:</label>
				  <div class="col-md-4" id="div_select_city">
				 <!--  <select name="city" id="city" data-parsley-trigger="change" class="form-control">									
					</select> -->
					<input type="text" class="form-control" name="userrole_margin" value="" disabled>
				  </div>
				</div><!-- /.form-group -->									
									<div class="form-group">
									  <div class="col-md-4 col-md-offset-3">			  
									   <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>										
									  </div>
									</div><!-- /.form-group -->
								  </form>                                       
						</div>
					</div>
					
					
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<script type="text/javascript">

 function myFunction(varl) {
	if (varl.value != "") {
        var arrtmp = varl.value.split(".");
        if (arrtmp.length > 1) {
			var strTmp = arrtmp[1];
			if (strTmp.length > 2) {
				varl.value = varl.value.substring(0, varl.value.length - 1);
			}
        }
	}
}
</script>
<script type="text/javascript" charset="utf8" 
src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-treeview/1.2.0/bootstrap-treeview.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.full.min.js"></script>
<!-- END FOOTER -->
<script>
function fnShowUserRole(id_value){
	 var allusertreedata = <?php echo json_encode($allusertreedata); ?>;
	console.log(allusertreedata[id_value]['user_role']);//
	if(allusertreedata[id_value]['user_role']=='Distributor'){
		$("#user_role option[value='Superstockist']").hide();
		$("#user_role option[value='SalesHead']").hide();
		$("#user_role").val("");
	} else if(allusertreedata[id_value]['user_role']=='Superstockist'){
        $("#user_role option[value='SalesHead']").hide();
        $("#user_role").value("");
	}else{
		$("#user_role option[value='Superstockist']").show();
		$("#user_role option[value='SalesHead']").show().
		$("#user_role").val("");
	}
	
}
function fnShowMargin(id_value) {	
	//alert(id_value);
	$("#margin_div").show();
	if(id_value=="DeliveryChannelPerson"||id_value=="Superstockist"||id_value=="Distributor"){
		$("#margin_usertypediv").show();
	}else{
		$('input[name="usertype_margin"]').val('0.00');
		$('input[name="usertype_margin"]').prop('disabled', true);
	}	
	var url = "getMarginDropDown.php?cat_id="+id_value+"&select_name_id=userrole_margin&mandatory=mandatory";
	CallAJAX(url,"div_select_city");	
}

$(document).ready(function(){		
 	var url = 'response_usertree.php';
			 $.ajax({
			 url: url,
			 datatype:"JSON",
			contentType: "application/json",
			error : function(data){console.log("error:" + data)
			},
			
			success: function(data){				
				 $('#treeview').treeview({data: data});				
			}
		 });
		 var url1 = 'response_usertree_select.php';
		  $.ajax({
			 url: url1,
			 datatype:"JSON",
			contentType: "application/json",
			error : function(data){console.log("error:" + data)
			},
			
			success: function(data){
				 
				obj = JSON.parse(data);
				console.log(typeof(obj));
				console.log(obj);
				$("#mySelect").select2({
					placeholder: 'Select an option',    
					data: obj,
					formatSelection: function(item) {
					  return item.text
					},
					formatResult: function(item) {
					  return item.text
					},
					templateResult: formatResult,
				  });
			}
		 });
		 
}); 
 function formatResult(node) {
    var $result = $('<span style="padding-left:' + (20 * node.level) + 'px;">' + node.text + '</span>');
    return $result;
  };

  
</script>

<style>
.form-horizontal{
font-weight:normal;
}
</style>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
         
