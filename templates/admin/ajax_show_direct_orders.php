<?php 
include ("../../includes/config.php");
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
$order_status = $_POST['order_status'];
$orders = $orderObj->getSuperStockistOrdersChange($order_status);
$user_type = $_SESSION[SESSION_PREFIX.'user_type'];
$order_count = count($orders);
?>
<div class="clearfix"></div>
			  <table class="table table-striped table-bordered table-hover" id="sample_2">
                                            <thead>
<tr id="main_th">                        
<?php 	
			
		if(($user_type=='Admin' && $order_status == '2') && $order_count != 0 ){ ?>
		<th id="select_th">
			<input type="checkbox" name="select_all[]" id="select_all" disabled onchange="javascript:checktotalchecked(this)">
		</th>
		<?php 
		 } 
		?>                  
                                                    <th>
                                                        Order Id<br><?php echo str_repeat("&nbsp;", 8); ?>Order Date 
                                                    </th> 
                                                    <th>
                                                        Product Name            
                                                    </th>          
                                                    <th>
                                                        Quantity
                                                    </th>
                                                    <th>
                                                        Total Price (₹)
                                                    </th>
                                                    <th>
                                                        Price(₹)<br>(GST+Discount)
                                                    </th>
                                                    <th>
                                                        Action  
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
            <?php   
            foreach($orders as $key => $value) {
                $orderdetails=array();
                $orderdetails = $orderObj->getStockistPlacedOrders($value);
                $order_no=$orderdetails[0]['order_no'];
                $order_date=$orderdetails[0]['order_date'];
                 $product_name='';
                  $product_variant='';
                   $prod_qnty='';
                    $totalcost='';
                     $gstcost='';               
                    foreach ($orderdetails as $key1 => $value1) {    
                        $product_name.=$value1['product_name']."(".$value1['product_variant'].")<br>";
                        $product_variant.=$value1['product_variant']."<br>";                                                            
                        $prod_qnty.="".$value1['product_quantity']."<br>";
                        $totalcost.="".$value1['total_cost']."<br>";
                        $gstcost.="".$value1['p_cost_totalwith_tax']."<br>";
                    }
              
                ?>
                <tr class="odd gradeX">
                    <?php if(($user_type=='Admin' && $order_status == '2') && $order_count != 0 ){  ?>
					<td><input type="checkbox" name="select_all[]" id="select_all" value="<?php echo $order_no;?>"  onchange="javascript:checktotalchecked(this)">
				<?php } ?>
				  </td>


                    <td ><font size="1.4"><?php echo $order_no; ?></font><br><?php echo date('d-m-Y H:i:s', strtotime($order_date)); ?></td>                                        
                    <td><?php echo $product_name; ?></td>                                                     
                    <td ><?php echo $prod_qnty; ?></td>
                    <td align="right"><?php echo $totalcost; ?></td>
                    <td align="right"><?php echo $gstcost; ?></td>
                    <td>
                        <a onclick='showInvoice(1,<?=json_encode($order_no);?>)' title="View Invoice">View Invoice</a>
                    </td>
                </tr>
            <?php } ?>

                                            </tbody>
                                        </table>

<script>
$(document).ready(function() {
	 $("#sample_2").dataTable().fnDestroy()

    $('#sample_2').dataTable( {
	order: [],
	columnDefs: [ { orderable: false, targets: [0] } ]
	});
});
$("#select_all").click(function(){
    $('input:checkbox').prop('checked', this.checked);
});
</script>