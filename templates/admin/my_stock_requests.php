<!-- BEGIN HEADER -->
<?php
//phpinfo();die();
include "../includes/grid_header.php";
include "../includes/userManage.php";

include "../includes/productManage.php";
include "../includes/orderManage.php";
$userObj = new userManager($con, $conmain);

$prodObj = new productManage($con, $conmain);
$orderObj = new orderManage($con, $conmain);


?>
<!-- END HEADER -->
<style>
.form-horizontal .control-label {
text-align: left;
}
small, .small {
 font-size: 77%;
} 
</style>
	<!--style for on//off switch-->
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">


</head>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLEAPIKEY; ?>&callback=initMap" type="text/javascript"></script>
<!-- END HEAD -->
<body class="page-header-fixed page-quick-sidebar-over-content ">

    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "Stock";
        $activeMenu = "my_stock_requests";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->

        <div class="page-content-wrapper">
            <div class="page-content">			
                <h3 class="page-title">
                    My Stock Requests 
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.php">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>					
                        <li>
                            <a href="#">My Stock Requests </a>
                        </li>
                    </ul>				
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">
                                    My Stock Requests 
                                </div>
								
                                <a  href="stock_request.php" class="btn btn-sm btn-default pull-right mt5">
                                  <span class="glyphicon glyphicon-arrow-up"></span> Request Stock
                                </a>
								<div class="clearfix"></div>	
                            </div>						
                            <div class="portlet-body">
								<!-- Nav tabs -->
							
						
							<!-- Tab panes -->							
							<div id="orders_form_table">
                                <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">
									
                                    <div class="form-group" id="order_status_tab_result">
                                        <label class="col-md-3">Requests Status:</label>
                                        <div class="col-md-4">
                                            <select name="order_status" id="order_status" autocomplete="off" data-parsley-trigger="change" class="form-control" ><!--onchange="fnShowUser(this.value)"-->
												<option value="11" selected >Pending</option><!-- To Me(<small class="muted text-muted">By Other User</small>)-->
												<option value="44">Approved</option>
												<option value="9">Rejected</option>	
                                                <option value="99">All</option>	
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->	
									
                                    <div class="form-group" id="divDaily">
                                        <label class="col-md-3">Select Request Date:</label>
                                        <div class="col-md-4">
                                            <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                                                <input type="text" class="form-control" data-date="<?php echo date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years" name="frmdate" id="frmdate" value="" autocomplete="off"> 
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                            <!-- /input-group -->								 
                                        </div>
                                    </div><!-- /.form-group -->	
								<?php $session_user_id=$_SESSION[SESSION_PREFIX . "user_id"];?>
                                   

                                   
                                 		
                                    <div class="form-group" id="city_div" style="display:none;">
                                        <label class="col-md-3">City:</label>
                                        <div class="col-md-4" id="div_select_city">
                                            <select name="dropdownCity" id="dropdownCity" data-parsley-trigger="change" class="form-control">
                                                <option selected value="">-Select-</option>										
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group" id="area_div" style="display:none;">
                                        <label class="col-md-3">Region:</label>
                                        <div class="col-md-4" id="div_select_area">
                                            <select name="area" id="area" data-parsley-trigger="change" class="form-control">
                                                <option selected value="">-Select-</option>									
                                            </select>
                                        </div>
                                    </div>									
                                    
                                    <div class="form-group">
                                        <label class="col-md-3">Category:</label>
                                        <div class="col-md-4" id="divCategoryDropDown">
                                            <?php $cat_result = $prodObj->getAllCategory(); ?>
                                            <select name="dropdownCategory" id="dropdownCategory" class="form-control" onchange="fnShowProducts(this)">
                                                <option value="">-Select-</option>
                                                <?php while ($row_cat = mysqli_fetch_assoc($cat_result)) {
                                                    ?>									
                                                    <option value="<?= $row_cat['id']; ?>"><?= $row_cat['categorynm']; ?></option>
                                                <?php } ?>								
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label class="col-md-3">Product:</label>
                                        <div class="col-md-4" id="divProductdropdown">
                                            <?php $prod_result = $prodObj->getAllProducts(); ?>
                                            <select name="dropdownProducts" id="dropdownProducts" class="form-control">
                                                <option value="">-Select-</option>
                                                <?php while ($row_prod = mysqli_fetch_assoc($prod_result)) {
                                                    ?>									
                                                    <option value="<?= $row_prod['id']; ?>"><?= $row_prod['productname']; ?></option>
                                                <?php } ?>	
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-3">											
                                            <button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>									
                                            <button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnReset();">Reset</button>
                                        </div>
                                    </div><!-- /.form-group -->
                                </form>	
                               									
                                    <div id="order_list"> </div>
							</div>
                        </div>
						<!--portable body-->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>

    <!-- END CONTAINER -->
    <div class="modal fade" id="view_invoice" role="dialog">
        <div class="modal-dialog" style="width: 980px !important;">    
            <!-- Modal content-->
            <div class="modal-content" id="view_invoice_content">      
            </div>      
        </div>
    </div>
    <div class="modal fade " id="order_details" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 880px !important;">
            <!-- Modal content-->
            <div class="modal-content" id="order_details_content">
            </div>
        </div>
    </div>

    <div class="modal fade" id="googleMapPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="model_content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                </div>
                <div class="modal-body" style="padding-bottom: 5px !important;"> 
                    <div id="map" style="width: 100%; height: 500px;"></div> 
                </div>
            </div>
        </div>
    </div>
<!---->

   
	
    <!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php" ?>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- END FOOTER -->

<script>			
	$(document).ready(function () { 		
		/* $("#tooltipid").tooltip({
			items: "span",
			content: "This product can not be sent to production."
		}).tooltip("open"); */
		$("#select_th").removeAttr("class");
		$("#main_th th").removeAttr("class");
		if ($.fn.dataTable.isDataTable('#sample_2')) {
			table = $('#sample_2').DataTable();
			table.destroy();
			table = $('#sample_2').DataTable({
				"aaSorting": [],
			});
		}	
		ShowReport();
	});

	$('.date-picker1').datepicker({
		rtl: Metronic.isRTL(),
		orientation: "auto",
		endDate: "<?php echo date('d-m-Y'); ?>",
		autoclose: true
	});
	
	 $('.date-picker2').datepicker({
			rtl: Metronic.isRTL(),
			orientation: "auto",
		   format: "dd-mm-yyyy",
			todayHighlight: true,
			startDate: "<?php echo date('d-m-Y'); ?>",
			endDate: "<?php echo date('d-m-Y'); ?>",
			setDate: "<?php echo date('d-m-Y'); ?>",
			autoclose: true
		});
	$('.date-picker3').datepicker({
		rtl: Metronic.isRTL(),
		orientation: "auto",
		startDate: 'd',
		endDate: '+3m',
		autoclose: true
	});
	function fnReset() {
		location.reload();
	}

 
	function fnShowProducts(id) {
		var url = "getProductDropdown.php?cat_id=" + id.value;
		CallAJAX(url, "divProductdropdown");
	}
  
	function ShowReport() {
	  var order_status = $('#order_status').val();	
		var user_type='<?=$user_type?>';
		var url = "ajax_my_request_stock_list.php";
		var data = $('#frmsearch').serialize();
		jQuery.ajax({
			url: url,
			method: 'POST',
			data: data,
			async: false
		}).done(function (response) {
			$('#order_list').html(response);
			var table = $('#sample_2').dataTable();
			table.fnFilter('');
			$("#select_th").removeAttr("class");
		}).fail(function () { });
		return false;
	}
	function showInvoice(order_status, id) {
		var url = "invoice2.php";
		jQuery.ajax({
			url: url,
			method: 'POST',
			data: 'order_status=' + order_status + '&order_id=' + id,
			async: false
		}).done(function (response) {
			$('#view_invoice_content').html(response);
			$('#view_invoice').modal('show');
		}).fail(function () { });
		return false;
	}
  
	function showApproveORReject(order_stock_id) {
		$('#update_stock_request_status').modal('show');
		$('#order_stock_id').val(order_stock_id); 
	}
	$('#btn_stock_approve_reject').click(function(){	
		var order_stock_id = $('#order_stock_id').val(); 
		var order_status_approve = $('#order_status_approve').val();
		var order_status_approve_text=$("#order_status_approve option:selected").text();
		var url = 'updateOrderDelivery.php?flag=approve_or_reject&order_stock_id=' + order_stock_id + '&order_status_approve=' + order_status_approve;
		$.ajax({
			url: url,
			datatype: "JSON",
			contentType: "application/json",
			error: function (data) {
				console.log("error:" + data)
			},success: function (data) {
				console.log(data);
				if (data > 0) {
					alert("The stock request is "+order_status_approve_text+".");
					window.location.reload();
				} else {
					alert("Not updated.");
				}
			}
		});
	});
	
	function showOrderDetails(order_status,oid) {
		var url = "stock_requests_popup.php";
		jQuery.ajax({
			url: url,
			method: 'POST',
			data: 'order_id=' + oid + '&order_status=' + order_status,
			async: false
		}).done(function (response) {
			$('#order_details_content').html(response);
			$('#order_details').modal('show');
		}).fail(function () { });
		return false;
	}    

</script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
</html>