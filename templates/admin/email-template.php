<!-- #######  THIS IS A COMMENT - Visible only in the source editor #########-->
<style>
			table {
			    border-collapse: collapse;
			}
td, th {
    font-family: Verdana,Arial,Helvetica,sans-serif;
    font-size: 14px;
}.editorDemoTable td {
    border: 1px solid #777;
    margin: 0 !important;
    padding: 2px 3px;
}
			td, th {
    display: table-cell;
    vertical-align: inherit;
}
			table {
				display: table;
				border-collapse: separate;
				border-spacing: 2px;
				border-color: grey;
			}
			.editorDemoTable {
    background-color: #fff8c9;
    border-spacing: 0;
}
			</style> 
<h2>&nbsp;</h2>
<h4>Todays Order Summary for all instance:</h4>
<p style="font-size: 1.5em;">&nbsp;</p>
<table class="editorDemoTable" style="width: 564px;">
<tbody>
<tr style="height: 18px;">
<td style="height: 18px; width: 179px;"><strong>Name Of Manufacturer</strong></td>
<td style="height: 18px; width: 96px;"><strong>Orders Received&nbsp;</strong></td>
<td style="height: 18px; width: 98px;"><strong>Orders Received From Shops&nbsp;</strong></td>
<td style="height: 18px; width: 191px;"><strong>Total Sale Value</strong></td>
</tr>
<tr style="height: 18px;">
<td style="height: 18px; width: 179px;">VPH</td>
<td style="height: 18px; width: 96px;">12</td>
<td style="height: 18px; width: 98px;">5</td>
<td style="height: 18px; width: 191px;">12000</td>
</tr>
<tr style="height: 18px;">
<td style="height: 18px; width: 179px;">Saras</td>
<td style="height: 18px; width: 96px;">no order</td>
<td style="height: 18px; width: 98px;">0</td>
<td style="height: 18px; width: 191px;">0</td>
</tr>
<tr style="height: 25.4219px;">
<td style="height: 25.4219px; width: 179px;">Supplychainz</td>
<td style="height: 25.4219px; width: 96px;">5</td>
<td style="height: 25.4219px; width: 98px;">1</td>
<td style="height: 25.4219px; width: 191px;">2000</td>
</tr>
</tbody>
</table>
<p>&nbsp; &nbsp; &nbsp; &nbsp;This is for all brand all categories per manufacturer.&nbsp;&nbsp;</p>
<p><strong>Thanks,</strong><br /><strong> Admin.</strong></p>