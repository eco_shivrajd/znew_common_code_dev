<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
if($_SESSION[SESSION_PREFIX.'user_type']!="Admin") 
{
	header("location:../logout.php");
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">

	<!-- BEGIN SIDEBAR -->
	<?php 
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Subarea";
	include "../includes/sidebar.php";
	
	?>	
	<!-- END SIDEBAR -->
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Subarea
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Subarea</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
            <div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Subarea Listing
							</div>
							<?php if ($ischecked_add==1) { ?>
                            <a href="subarea-add.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Subarea
                              </a>
                             <?php } ?>
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_5">
							<thead>
							<tr>
								 
								<th>
									Subarea Name
								</th>
								<th>
									Taluka Name
								</th>
								<th>
									District
								</th>
								<th>
									State
								</th>
								 
								<th>
                                  Action
                                </th>
								
							</tr>
							</thead>
							<tbody>
							<?php
							$sql="SELECT subarea.subarea_name,st.name as statename,
								ct.name as cityname,surb.suburbnm,subarea.subarea_id
							FROM 
								tbl_subarea subarea 
								left join tbl_state st on st.id = subarea.state_id
								left join tbl_city ct on ct.id = subarea.city_id
								left join tbl_area surb on surb.id = subarea.suburb_id  
								where subarea.isdeleted!='1' ";
							$result1 = mysqli_query($con,$sql);
						 if(mysqli_num_rows($result1)>0){
							while($row = mysqli_fetch_array($result1))
							{	
								 echo '<tr class="odd gradeX"><td>';
                                 if($ischecked_edit==1){
								 echo '<a href="subarea-edit.php?id='.base64_encode($row['subarea_id']).'">'.fnStringToHTML($row['subarea_name']).'</a>';
                                 } else {
                                                echo '' . fnStringToHTML($row['subarea_name']) . '';}
								echo '</td>';

								echo '<td>'.fnStringToHTML($row['suburbnm']).'</td>
								<td>'.fnStringToHTML($row['cityname']).'</td>
								<td>'.fnStringToHTML($row['statename']).'</td>';


									echo '<td>';
										if ($ischecked_edit==1) 
                                            { ?>
                                               <a title="Edit" href="subarea-edit.php?id=<?php echo base64_encode($row['subarea_id']);?>" class="btn btn-xs btn-primary ">
											   <span class="glyphicon glyphicon-edit"></span></a>
                                            <?php } else {
                                               echo '<a title="Can Not Edit This Record" href="#" class="btn btn-xs btn-primary" disabled><span class="glyphicon glyphicon-edit"></span></a>';
                                            }
                                            if ($ischecked_delete==1) 
                                            { ?>
												<a title="Delete" onclick="javascript: deleteProduct(<?php echo $row['subarea_id'];?>)"  class="btn btn-xs btn-danger">
												<span class="glyphicon glyphicon-trash"></span></a>
                                           <?php } else {
                                                echo '<a title="Can Not Delete This Record" onclick="#" class="btn btn-xs btn-danger" disabled><span class="glyphicon glyphicon-trash"></span></a>';
                                            } 
							    echo '</td>';
								echo '</tr>';
						 } } ?>
							</tbody>
							</table>
							<br>
						</div>
					</div>
            
				
                    
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>

<script>
                function deleteProduct(subarea_id) {
                    if (confirm('Are you sure you want to delete this  subarea?')) {
                        CallAJAX('ajax_product_manage.php?action=delete_subarea&subarea_id=' + subarea_id );
                    }
                }

                function CallAJAX(url) {
                    if (window.XMLHttpRequest) {
                        xmlhttp = new XMLHttpRequest();
                    } else {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            alert('Subarea deleted successfully.');
                            location.reload();
                        }
                    }
                    xmlhttp.open("GET", url, true);
                    xmlhttp.send();
                }
            </script>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>