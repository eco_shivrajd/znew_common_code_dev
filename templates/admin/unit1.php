<?php 
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include "../includes/header.php";
include "../includes/commonManage.php";
include "../includes/targetManage.php";
$targetObj 	= 	new targetManager($con,$conmain);
$result_units = $targetObj->getAllUnits($tuid=base64_decode($_GET['id']));
$result_variant = $targetObj->getAllVariant($tvid=0);

if(isset($_POST['submit']))
{ 
	//print_r($_POST);
	//die();
	//$id=$_GET['id'];
	$id=base64_decode($_GET['id']);
	$unitname=fnEncodeString($_POST['unitname']);
	$variantname=fnEncodeString($_POST['variantname']);
	$sql_unit_check = mysqli_query($con, "select id from `tbl_units` where unitname='$unitname' and id!='$id'");
    if ($rowcount = mysqli_num_rows($sql_unit_check) > 0){
		echo '<script>alert("Unit already exist.");location.href="unit-add.php";</script>';
    } else {
		 $sql_update_unit="UPDATE `tbl_units` SET `unitname`='$unitname',
		variantid='$variantname'
		WHERE id='$id'";
		//die();
		$sql_update1=mysqli_query($con,$sql_update_unit);
	}
	echo '<script>alert("unit updated successfully.");location.href="units.php";</script>';
}
?>
<!-- BEGIN HEADER -->

<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">

<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->	
    <?php
	$activeMainMenu = "ManageProducts"; $activeMenu = "Unit";
	include "../includes/sidebar.php";
	$commonObj 	= 	new commonManage($con,$conmain);
	$row_url=$commonObj->getPageIDforUrlEdit($php_page_name);
	$page_id_url = $row_url['page_id'];
	$row_url_edit=$commonObj->getURLforEdit($profile_id,$page_id_url);
	$ischecked_edit_url = $row_url_edit['ischecked_edit'];
    if ($ischecked_edit_url == 0 && $ischecked_edit_url!='') 
	{
		session_set_cookie_params(0);
		session_start();
		session_destroy();
		echo '<script>location.href="../login.php";</script>';
	    exit;
	}
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<!-- /.modal -->			
			<h3 class="page-title">Unit</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="units.php">Unit</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Unit</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">Edit Unit</div>
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
					
                          
                    <form class="form-horizontal" role="form" action="" data-parsley-validate="" method="post">
						<?php if(count($result_units)>0){ 
						foreach($result_units as $key=>$units){ ?>	
						<div class="form-group">						
							<label class="col-md-3">Unit Name:<span class="mandatory">*</span></label>
							<div class="col-md-4">
							<input type="text" name="unitname" class="form-control"
							placeholder="Enter Unit Name"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter unit name"
							data-parsley-maxlength="25"
							data-parsley-maxlength-message="Only 25 characters are allowed"
							data-parsley-pattern="^(?!\s)[a-zA-Z ]*$"
							data-parsley-pattern-message="Please enter alphabets only"
							value="<?php echo $units['unitname']?>">
							</div>							
						</div><!-- /.form-group --> 
						
						<div class="form-group">
							<label class="col-md-3">Unit Variant:<span class="mandatory">*</span></label>
							<div class="col-md-4">
							<select name="variantname" id="variantname" class="form-control">
							  <option disabled selected>-Select-</option>	
								<?php  foreach($result_variant as $key2=>$variants){ ?>
								<option value='<?php echo $variants["tvid"];?>' 
								<?php if($variants['tvid']==$units["variantid"]){ ?>
								selected <?php } ?>>
								<?php echo fnStringToHTML($variants["tvname"]);?></option>
								<?php } ?>
							</select>
							</div>
						</div>
						<?php } } ?> 
						<div class="form-group">
						  <div class="col-md-4 col-md-offset-3">
							<button type="submit" name="submit" class="btn btn-primary">Submit</button>
							<a href="units.php" class="btn btn-primary">Cancel</a>
						  </div>
						</div><!-- /.form-group -->
						       
					  </form> 
					                   
					</div>
				</div>
				<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->

</body>
<!-- END BODY -->
</html>