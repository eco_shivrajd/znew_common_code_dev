<?php 
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
//echo "<pre>";print_r($_POST);die();
$order_id = $_POST['order_id'];
$order_type = 'Order Details';//Orderwise_Details   Order Product Details
$order_status = $_POST['order_status'];
$order_details1 = $orderObj->getOrderschnage1_cust($order_status,$order_id);
$order_details=$order_details1[1];
$order_details_new = $orderObj->getShopOrdersbyorderid_cust($order_details['oid']);
?>
<div class="modal-header">
<button type="button" name="btnPrint" id="btnPrint" onclick="OrderDetailsPrint()" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
<?php if($order_details['lat']!="" && $order_details['long']!="" && $order_details['lat']!="0.0" && $order_details['long']!="0.0") { ?>
&nbsp;&nbsp;
<button type="button" name="btnPrint" id="btnPrint" onclick="showGoogleMap('<?=$order_details['lat'];?>','<?=$order_details['long'];?>')" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Location</button>
<?php } ?>

<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel"></h4>	   
</div>
<div class="modal-body" style="padding-bottom: 5px !important;" id="divOrderPrintArea">
<div class="row">
<div class="col-md-12">   
	<div class="portlet box blue-steel">
		<div class="portlet-title ">
			<div class="caption printHeading">
				<?=$order_type;?>
			</div>                          
		</div>
		<div class="portlet-body">
			<table class="table table-striped table-bordered table-hover" id="sample_2" width="100%">
			<tr>
				<td>Order From</td>
				<td><?=$order_details['order_by_name'];?></td>				
			</tr>
			<tr>
				<td>Order To</td>
				<td><?=$order_details['order_to_name'];?></td>				
			</tr>	
			<tr>
				<td>Order Id</td>
				<td><?=$order_details['oid'];?></td>				
			</tr>
			<tr>
				<td>Order Date</td>
				<td><?php echo date('d-m-Y h:i:s A', strtotime($order_details['order_date']));?></td>				
			</tr>
			<tr>				
				<td colspan="2">				
					<table class="table table-striped table-bordered table-hover"  width="100%">
						<tr>
							<th>Product</th>
							<th>Quantity</th>
							<th>Unit Price (₹)</th>
							<th>Price (₹)</th>
							<th>GST (₹)</th>							
						</tr>
						<?php $g_total_unitcost=0;$g_tax_total_cost=0;
						foreach($order_details_new as $key_od=>$value_od){ 
									$total_unitcost=($value_od['variantunit']*$value_od['product_unit_cost']);
									$g_total_unitcost=$g_total_unitcost+$total_unitcost;
									$g_tax_total_cost=$g_tax_total_cost+$value_od['unitcost'];
						?>
							<tr>							
								<td><?=$value_od['productname'].' '.$product_variant;?></td>	
								<td><?=$value_od['variantunit'].' ('.$value_od['product_variant_weight1'].''.$value_od['unit'].') ';?></td>
								<td align="right"><?php echo number_format((float)$value_od['product_unit_cost'], 2, '.', '');?></td>
								<td align="right"><?php echo number_format((float)$total_unitcost, 2, '.', '');?></td>
								<td align="right"><?php echo number_format((float)$value_od['unitcost'], 2, '.', '');?></td>					
							</tr>	
						
						<?php } ?>
							<tr>							
								<td colspan="3"><b>Total</b></td>									
								<td align="right"><span ><b><?php echo number_format((float)$g_total_unitcost, 2, '.', '');?></b></span></td>
								<td align="right"><span ><b><?php echo number_format((float)$g_tax_total_cost, 2, '.', '');?></b></span></td>															
							</tr>
					</table>
				</td>				
			</tr>
			</table>
</div>
</div>
</div>
</div>
</div>