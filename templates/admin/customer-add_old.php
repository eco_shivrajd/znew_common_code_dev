<!-- BEGIN HEADER -->
<?php 
include "../includes/header.php";
//include "../../includes/config.php";

if($_SESSION[SESSION_PREFIX.'user_type']!="Admin") {
	header("location:../logout.php");
} 

if(isset($_POST['hidbtnsubmit']))
{
	include "../includes/customerManage.php";

	$customerObj 	= 	new customerManager($con,$conmain);
	$user_type	=	"Customer";
	$username	=	fnEncodeString($_POST['username']);
	$password	=	fnEncodeString($_POST['password']);
	$row_count = $customerObj->getCustomerDetailsByUsername($username,$password);	
	if($row_count != 0)
	{		
	  $userid_local = $customerObj->addCustomerDetails($user_type);	
	  $email		=	fnEncodeString($_POST['email']);
				if($email != '')
				{
					$customerObj->sendUserCreationEmail();
				}
		echo '<script>alert("Customer added successfully.");location.href="customer-add.php";</script>';	
	}
	else
	{	   	
		echo '<script>alert("Username And Password Allready exit.");return false";</script>';
	}	
	
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "customer_list";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->			
			<h3 class="page-title">Customer</h3>
			
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="customer.php">Customer</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Customer</a>
					</li>
				</ul>				
			</div>
			
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Customer
							</div>
							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						
       <form class="form-horizontal" role="form" data-parsley-validate="" method="post" name="addform" id="addform">       
           <?php $page_to_add = 'superstockist';include "userAddCommEle.php";	//form common element file with javascript validation ?>   

           	<div class="form-group">
							<label class="col-md-3"><b>Bank Details</b></label>
						</div>						
						 <div class="form-group">
							<label class="col-md-3">Account Name <span class="mandatory">*</span></label>
							<div class="col-md-4">
								<input type="text" 
								placeholder="Account Name"
								data-parsley-trigger="change"				
								data-parsley-required="#true" 
								data-parsley-required-message="Please enter accouont number"
								data-parsley-maxlength="50"
								data-parsley-maxlength-message="Only 50 characters are allowed"
								name="accname" 
								
								class="form-control">
							</div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Account Number:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Account Number"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter accouont number"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"						
							name="accno" class="form-control" >
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Name:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Bank Name"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter Bank Name"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"							
							name="bank_name" class="form-control" >
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Branch Name:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Branch Name"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter branch Name"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"							
							name="accbrnm" class="form-control" >
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">IFSC Code:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter IFSC Code"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter ifsc Code"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"							
							name="accifsc" class="form-control">
						  </div>
						</div> 
      
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
				<input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
				<input type="hidden" name="hidAction" id="hidAction" value="customer-add.php">
                <button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button>
                <a href="customer.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group -->
          </form>     
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>
