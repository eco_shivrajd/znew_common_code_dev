<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);
?>
<!-- END HEADER -->
 <link href="../../assets/global/css/jquery.loader.css" rel="stylesheet" />
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Reports"; $activeMenu = "DailyStatusReport";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Daily Status Report
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Report</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->			
			<div class="row">
				<div class="col-md-12"> 				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">Search Criteria</div>
						</div>
						<div class="portlet-body">
						<form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">
							<div class="form-group">
								<label class="col-md-3">Select date:</label>
								<div class="col-md-4">
									<div  class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
										<input type="text" class="form-control" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years" name="frmdate" id="frmdate" value="<?php echo date('d-m-Y');?>" autocomplete="off">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<!-- /input-group -->								 
								</div>
							</div><!-- /.form-group -->							
							<div class="form-group">
								<label class="col-md-3">Sales Person:</label>
								<div class="col-md-4" id="divsalespersonDropdown">
								<?php $user_result = $userObj->getAllSP('SalesPerson'); ?>	
									<select name="dropdownSalesPerson" id="dropdownSalesPerson" class="form-control">
										<option value="">-Select-</option>
										<?php while($row_user = mysqli_fetch_assoc($user_result))
										{ ?>									
										<option value="<?=$row_user['id'];?>"><?=$row_user['firstname'];?></option>
										<?php } ?>
									</select>
								</div>
							</div><!-- /.form-group -->
							<div class="form-group">
								<div class="col-md-4 col-md-offset-3">
									<button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="$('#loader_div').loader('show'); ShowReport();">Search</button>
									
									<button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnChangeReportType('daily');">Reset</button>
								</div>
							</div><!-- /.form-group -->
						</form>	
						</div>
					   <div class="clearfix"></div>
					</div>
					<div id="loader_div"></div>
					<div id="divReportHTML"></div>
		</div>			
	</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div id="print_div"  style="display:none;"></div>
<form action="../includes/exportToExcel.php" method="post" name="export_excel" id="export_excel">
	<input type="hidden" name="export_data" id="export_data">
	<input type="hidden" name="file_name" id="file_name">
</form>
<?php include "../includes/footer.php"?>

<script>
function CallAJAX(url,assignDivName) {
	if (window.XMLHttpRequest)
	{
		var xmlhttp=new XMLHttpRequest();
	} else {
		var xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById("" + assignDivName +"").innerHTML	=	xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET",url,true);
	xmlhttp.send();	
};
function ShowReport() {
	$("#loader_div").loader('show');
	var url = "dailyStatusReport.php";	
	var data = $('#frmsearch').serialize();
	var date = $('#frmdate').val();
	var salespersonid = $('#dropdownSalesPerson').val();
	if(date == ''){
		alert('Please select date.');
		$('#frmdate').focus();
		$("#loader_div").loader('hide');
		return false;
	}
	if(salespersonid == ''){
		alert('Please select Sales Person.');
		$('#dropdownSalesPerson').focus();
		$("#loader_div").loader('hide');
		return false;
	}
	
    jQuery.ajax({
		url: url,
		method: 'POST',
		data: data,
		async: false
	}).done(function (response) {
		console.log(response);
		$("#divReportHTML").show();
		$('#divReportHTML').html(response);
		$("#loader_div").loader('hide');
	}).fail(function () { });
	
	return false;
}

function ExportToExcel() {
	var td_rec = $("#report_table td:last").html();
	if(td_rec != 'No matching records found')
	{
		var divContents = $("#dvtblResonsive").html();
		$("#print_div").html('<table id="print_table" style="text-decoration:none;">'+divContents+'</table>');	
		
		divContents =  $("#print_div").html();
		divContents = divContents.replace(/₹/g,'Rs');
		var file_name = "Report_"+$("#salespname").val()+"_"+$("#date").val()+".xls";
		$("#file_name").val(file_name);
		$("#export_data").val(divContents);
		document.forms.export_excel.submit();
	}else{
		alert("No matching records found");
	}
}

$('.date-picker1').datepicker({
	rtl: Metronic.isRTL(),
	orientation: "left",
	endDate: "<?php echo date('d-m-Y');?>",
	autoclose: true
});
function takeprint() {
	var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
	var divContents = '<style type="text/css">@page{size: landscape;}\	table { border-collapse: collapse; }\
	table, th, td {  border: 1px solid black; }\
	body { font-family: "Open Sans", sans-serif;\
	background-color:#fff;\
	font-size: 11px;\
	direction: ltr;}</style>' + $("#dvtblResonsive").html();
	
	
	if(isIE == true){
		var printWindow = window.open('', '', '');
		printWindow.document.write(divContents);
		printWindow.focus();
		printWindow.document.execCommand("print", false, null);
	}else{
		$('<iframe>', {
			name: 'myiframe',
			class: 'printFrame'
		}).appendTo('body').contents().find('body').html(divContents);
		window.frames['myiframe'].focus();
		window.frames['myiframe'].print();		
		setTimeout(
		function() 
		{
			$(".printFrame").remove();
		}, 1000);
	}
	
};
</script>
<script type="text/javascript" src="../../assets/global/scripts/jquery.loader.js"></script>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>