<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/orderManage.php";
include "../includes/userManage.php";
include "../includes/shopManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
$userObj 	= 	new userManager($con,$conmain);
$shopObj 	= 	new shopManager($con,$conmain);
$sp_id = $_GET['id'];
if($sp_id == '')
{
	header("location:order_summary.php");
}
$s_id = $_GET['s_id'];
$user_data = $userObj->getLocalUserDetails($sp_id);
if($_SESSION[SESSION_PREFIX.'user_type'] != 'Admin')
{
	/*$belong = $userObj->checkUserBelongTo($sp_id,$user_data['external_id']);
	if($belong == 1)
	{
		header("location:order_summary.php");
	}*/
}
$shop_data = $shopObj->getShopDetails($s_id);
$filter_date = $_GET['param1'];
$date1 = null;
$date2 = null;
if(isset($_GET['param2']))
	$date1=$_GET['param2'];
if(isset($_GET['param3']))
	$date2=$_GET['param3'];

$param = "?id=".$sp_id."&s_id=".$s_id."&param1=".$filter_date;
if($filter_date == 3)
{
	$from_date = $_GET['param2'];
	$to_date = $_GET['param3'];
	$param.="&param2=".$from_date;
	$param.="&param3=".$to_date;
}

$row = $orderObj->getSPShopOrders($sp_id, $s_id, $filter_date,$date1,$date2);
$report_title = $orderObj->getReportTitleForSP($filter_date,$date1,$date2);
?>
<!-- END HEADER -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLEAPIKEY;?>&callback=initMap" type="text/javascript"></script>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Reports"; $activeMenu = "OrderSummary";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<h3 class="page-title">
			Summary 
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="shop_orders_summary.php<?=$param;?>">Sales Person: <?=$user_data['firstname'];?></a>	
					</li>
				</ul>				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->	            
			<div class="row">
				<div class="col-md-12"> 
					<div class="clearfix"></div>   
						<div class="portlet box blue-steel">
							<div class="portlet-title">
								<div class="caption"><i class="icon-puzzle"></i>Shop: <?=$shop_data['name'];?> <?=$report_title;?></div>
								<button type="button" name="btnExcel" id="btnExcel" onclick="report_download_shop();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
								&nbsp;
								<button type="button" name="btnPrint_shop" id="btnPrint_shop" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
							</div>	
							<div class="portlet-body">	
								<table class="table table-striped table-bordered table-hover table-highlight table-checkable" id="sample_2">
								<thead>							
								  <tr>
									<th>Brand</th>
									<th>Category</th>
									<th>Product Name</th>
									<th>Product Variant</th>
									<th>Order Date</th>
									<th>Quantity</th>
									<th>Total Sales <i aria-hidden='true' class='fa fa-inr'></i></th>              
									<th>Action</th>
								  </tr>
								</thead>
									<tbody>
									<?php 
										if($row != 0){
											foreach($row as $key => $val) {
											$total_cost =  $val['p_cost_cgst_sgst'];
											$weightquantity =$val["product_variant_weight1"].'-'.$val["product_variant_unit1"];
											$display_icon = '';
											if($val['campaign_sale_type'] == 'free')
											$display_icon = '<span style="float: left"><img src="'.SITEURL.'/assets/global/img/free-icon.png" title="Free Product"></span>';
										?>
											<tr class="odd gradeX">
												<td><?=$val['brandnm'];?></td>
												<td><?=$val['categorynm'];?></td>
												<td><?=$val['product_name'];?></td>	
												<td align="right"><?=$weightquantity;?></td>	
												<td><?=date('d-m-Y H:i:s',strtotime($val['order_date']));?></td>
													
												<td> <?=$val['product_quantity'];?></td>
													
												<td align=""><?=$display_icon;?><span style="float: right"><?=number_format($total_cost,2, '.', '');?></span></td>
												<td><a onclick="javascript: view_product_order(<?=$val['variant_oder_id'];?>)">view</a></td>
											</tr>			
									<?php 	}
										}
									?>	
									</tbody>	
								</table>
							</div>
						</div>
					</div>	
				</div>	
			</div>
		</div>
	</div>
<div id="table_heading"  style="display:none;"><?=$user_data['firstname'];?>'s "<?=$shop_data['name'];?>" shop's <?=$report_title;?></div>
<div id="print_div"  style="display:none;"></div>
<div id="loader_div"></div>
<div class="modal fade" id="view_product_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
    <div class="modal-content" id="model_content">
      <div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>	   
      </div>
		<div class="modal-body" style="padding-bottom: 5px !important;">
	  </div>
	</div>
</div>
</div>
<div class="modal fade" id="googleMapPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
    <div class="modal-content" id="model_content">
      <div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
		<div class="modal-body" style="padding-bottom: 5px !important;"> 
		<div id="map" style="width: 100%; height: 500px;"></div> 
		</div>
	</div>
</div>
</div>

<form action="../includes/exportToExcel.php" method="post" name="export_excel" id="export_excel">
	<input type="hidden" name="export_data" id="export_data">
</form>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
function view_product_order(order_variant_id){	
	$("#loader_div").loader('show');
	var url = "single_product_order1.php?order_variant_id="+order_variant_id;
	if (window.XMLHttpRequest)
	{
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			$('#view_product_details').modal('show');
			document.getElementById("model_content").innerHTML	=	xmlhttp.responseText;
			$("#loader_div").loader('hide');
		}
	}
	xmlhttp.open("GET",url,true);
	xmlhttp.send();	
}
function report_download_shop() {
	var td_rec = $("#sample_2 td:last").html();
	if(td_rec != 'No matching records found')
	{
		var divContents = $(".table-striped").html(); 
		$("#print_div").html('<table id="print_table">'+divContents+'</table>');
		$('#print_table tr').find('th:last-child, td:last-child').remove()
		var heading = $("#table_heading").html();
		$('<tr><th colspan="6" align="center">'+heading+'</th></tr>').insertBefore('#print_table tr:first');
		$("#print_table tr th i").html("RS");	
		$("#print_div tr").removeAttr("class");
		$("#print_div tr").removeAttr("role");
		$("#print_div tr th").removeAttr("class");
		$("#print_div tr th").removeAttr("rowspan");
		//$("#print_div tr th").removeAttr("colspan");
		$("#print_div tr th").removeAttr("style");
		$("#print_div tr th").removeAttr("aria-controls");
		$("#print_div tr th").removeAttr("tabindex");
		$("#print_div tr th").removeAttr("aria-label"); 
		$("#print_div tr th").removeAttr("aria-sort");
		$("#print_div tr td").removeAttr("class");	
		$("#print_div tr td").removeAttr("align");	
		
		divContents =  $("#print_div").html().replace(/\s/g, ' ');
		//divContents =  $("#print_div").html().replace(/<tbody>\s*<tr>/g, '<tbody><tr>');
		$("#export_data").val(divContents);//alert(divContents);
		document.forms.export_excel.submit();
	}else{
		alert("No matching records found");
	}
}
$("#btnPrint_shop").live("click", function () {
	var td_rec = $("#sample_2 td:last").html();
	if(td_rec != 'No matching records found')
	{
		var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
		
		var divContents = $(".table-striped").html();
		$("#print_div").html('<table id="print_table">'+divContents+'</table>');
		$('#print_table tr').find('th:last-child, td:last-child').remove()
		var heading = $("#table_heading").html();
		$("#print_table tr th i").html("₹");	
		$('<tr><th colspan="6" align="center">'+heading+'</th></tr>').insertBefore('#print_table tr:first');
		divContents =  $("#print_div").html();
		
		var printWindow = window.open('', '', '');
		printWindow.document.write('<html><head><title>Report</title>');
		printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
		printWindow.document.write('</head><body >');
		printWindow.document.write(divContents);
		printWindow.document.write('</body></html>');	
		printWindow.focus();	
		if( navigator.userAgent.toLowerCase().indexOf('chrome') > -1 ){		
			setTimeout(function () {
				printWindow.print();
				//printWindow.close();
			}, 500);
		}else if(isIE == true){			
			printWindow.document.execCommand("print", false, null);
			//printWindow.close();
		}
		else{
			setTimeout(function () {				
				printWindow.print();
				//printWindow.close();
			}, 100);
		}
	}else{
		alert("No matching records found");
	}
});
var lat=0;
var lng=0;

$('#googleMapPopup').on('shown.bs.modal', function (e) {
  
    var latlng = new google.maps.LatLng(lat,lng);
    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 17
    });
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: false,
      //anchorPoint: new google.maps.Point(0, -29)
   });
});
function showGoogleMap(getlat,getlng) {
   
  lat = getlat; 
  lng = getlng;
   $('#googleMapPopup').modal('show');
    /*var infowindow = new google.maps.InfoWindow();   
    google.maps.event.addListener(marker, 'click', function() {
      //var iwContent = '<div id="iw_container">' + '<div class="iw_title"><b>Location</b> : Noida</div></div>';
      // including content to the infowindow
      // infowindow.setContent(iwContent);
      // opening the infowindow in the current map and at the current marker location
      infowindow.open(map, marker);
    }); */
	/*var myLatlng = new google.maps.LatLng(lat,lng);
     var myOptions = {
         zoom: 4,
         center: myLatlng,
         mapTypeId: google.maps.MapTypeId.ROADMAP
         }
      map = new google.maps.Map(document.getElementById("map"), myOptions);
      var marker = new google.maps.Marker({
          position: myLatlng, 
          map: map,
      title:"Fast marker"
     });*/
}
function takeprint() {
	var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
	var divContents = '<style>\body {\
		font-size: 12px;}\
	th {text-align: left;}\
	.printHeading { line-height: 18px;  padding: 10px 0;  font-size: 18px; }\
	table { border-collapse: collapse;  \
		font-size: 12px; }\
	table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
	body { font-family: "Open Sans", sans-serif;\
	background-color:#fff;\
	font-size: 15px;\
	direction: ltr;}</style>' + $("#divPrintARea").html();
	if(isIE == true){
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write(divContents);
		printWindow.focus();
		printWindow.document.execCommand("print", false, null);
	}else{
		$('<iframe>', {
			name: 'myiframe',
			class: 'printFrame'
		}).appendTo('body').contents().find('body').html(divContents);
		window.frames['myiframe'].focus();
		window.frames['myiframe'].print();
		setTimeout(
		function() 
		{
			$(".printFrame").remove();
		}, 1000);
	}
}
</script>
<script type="text/javascript" src="../../assets/global/scripts/jquery.loader.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- END JAVASCRIPTS -->