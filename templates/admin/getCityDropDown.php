<?php
include ("../../includes/config.php");
$id=$_GET["cat_id"]; 
$select_name = "dropdownCity";
$select_id = "dropdownCity";
if(isset($_GET['select_name_id'])){
	$select_name = $_GET['select_name_id'];
	$select_id = $_GET['select_name_id'];
}


$selectedval = array();
if(isset($_GET['selectedval']))
{
	$selectedval = json_decode($_GET['selectedval']);
}
$mandatory = '';
if(isset($_GET['mandatory']))
{
	$mandatory = 'data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please select City"';
}
if(isset($_GET['campaign'])){
	$sql="SELECT name,id FROM tbl_city WHERE state_id IN (".$id.") order by name";
}else{
	$sql="SELECT name,id FROM tbl_city WHERE state_id ='".$id."' order by name";
}	

$result1 = mysqli_query($con,$sql);
$rowcount = mysqli_num_rows($result1);


$multiple = "";
if(isset($_GET['multiple']) && $rowcount!=0)
	$multiple = $_GET['multiple'];

if($multiple != '')
{
	$select_name = $_GET['select_name_id']."[]";
	$select_id = $_GET['select_name_id'];
}
$function = "onchange=\"FnGetSuburbDropDown(this)\"";
if($_GET['nofunction'] == 'nofunction')
	$function = "";

echo " <select name=\"$select_name\" id=\"$select_id\" class=\"form-control\"  $function $multiple $mandatory>";
if($multiple == '' OR $rowcount == 0)
	echo "<option value=''>-Select-</option>";

while($row = mysqli_fetch_array($result1))
{
	$selected = "";
	if(in_array($row["id"],$selectedval))
		$selected = "selected";
	$assign_id=$row['id']; 
	echo "<option value='$assign_id' $selected>" . fnStringToHTML($row['name']) . "</option>";
}
echo "</select>";
mysqli_close($con);
?>