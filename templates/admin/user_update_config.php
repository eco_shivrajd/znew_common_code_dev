<!-- BEGIN HEADER -->
<?php 
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include "../includes/header.php";
include "../includes/userConfigManage.php";
include "../includes/testManage.php";
$userconfObj 	= 	new userConfigManager($con,$conmain);
$testObj 	= 	new testManager($con,$conmain);
$margin_percentage_update = $userconfObj->getAllLocalMarginDetailsByUsertype();
$result_session_user_details = $testObj->getSessionUser();
$sess_parent_usertypes=explode(',',$result_session_user_details[0]['parent_usertypes']);
$sess_parent_names=explode(',',$result_session_user_details[0]['parent_names']);
$sess_parent_ids=explode(',',$result_session_user_details[0]['parent_ids']);
 $seesion_user_id=$_SESSION[SESSION_PREFIX.'user_id'];
//echo "<pre>";print_r($result_session_user_details);
?>
<!-- END HEADER -->
<?php
if(isset($_POST['hidbtnsubmit']))
{
	//print_r($_POST);
	//exit();
	$id=$_POST['id'];
	$user_type=$_POST['user_type']; 
	$user_role=$_POST['user_role']; 
	$userconfObj->updateAllLocalUserDetails($user_type,$user_role, $id);
	//die();
	$row = $userconfObj->getCommonUserID($id);
	$common_user_id = $row['common_user_id'];
	$userconfObj->updateCommonUserDetails($common_user_id);	
	echo '<script>alert("User Updated Successfully.");location.href="user_list_config.php";</script>';
}

?>

<link rel="stylesheet" href="../../assets/multipal-select/build.min.css">
<script src="../../assets/multipal-select/build.min.js"></script>
<link rel="stylesheet" href="../../assets/multipal-select/fastselect.min.css">
<script src="../../assets/multipal-select/fastselect.standalone.js"></script>

<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "User List";
	include "../includes/sidebar.php";

	$commonObj 	= 	new commonManage($con,$conmain);
	$row_url=$commonObj->getPageIDforUrlEdit($php_page_name);
	$page_id_url = $row_url['page_id'];
	$row_url_edit=$commonObj->getURLforEdit($profile_id,$page_id_url);
	$ischecked_edit_url = $row_url_edit['ischecked_edit'];
    if ($ischecked_edit_url == 0 && $ischecked_edit_url!='') 
	{
		session_set_cookie_params(0);
		session_start();
		session_destroy();
		echo '<script>location.href="../login.php";</script>';
	    exit;
	}
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- /.modal -->
			<h3 class="page-title">User Update</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="user_list_config.php">User List</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">User Update</a> 
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								User Update
							</div>
							 <a href="updatepassword.php?id=<?php echo $_GET['id'];?>" class="btn btn-sm btn-default pull-right mt5">							
                                Change Password
                              </a>
						</div>
						<div class="portlet-body">
						
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						
					    <?php
						$id	= base64_decode($_GET['id']);						
						$row1 = $userconfObj->getAllLocalUserDetailss($id);
						//echo "<pre>";print_r($row1);die();
						$user_role=$row1['user_role'];
						$user_type1=$row1['user_type'];
						$marginType=$row1['marginType'];
						$userid_margin_percentage=$row1['userid_margin'];

						 if (isset($marginType)){
							switch ($marginType) {
								case '0':                                   
									$userrole_margin_percentage = $margin_percentage_update['user_role'][$user_type1];
									break;
								case '1':                                   
									$usertype_margin_percentage = $margin_percentage_update['user_type'][$user_type1];
									break; 
								case '2':
									$userid_margin_percentage = $userid_margin_percentage;
									break;           
								default:
									$userid_margin_percentage =0;
									break;
							}
						}
						?>                       

					<form  name="updateform" id="updateform" class="form-horizontal" role="form" data-parsley-validate="" action="" method="post">

						<div class="form-group">
						<label class="col-md-3">User Type:<span class="mandatory">*</span></label>
						<div class="col-md-4">
						<input type="text" name="user_type1" class="form-control" value="<?php echo fnStringToHTML($row1['user_type']);?>" disabled>
						<input type="hidden" name="user_type" class="form-control" value="<?php echo $row1['user_type'];?>" >
						<input type="hidden" name="user_role" class="form-control" value="<?php echo $row1['user_role'];?>" >
						</div>
						</div>
						<?php 
						$parent_usertypes=explode(',',$row1['parent_usertypes']);
					    $parent_names=explode(',',$row1['parent_names']);
						$parent_ids=explode(',',$row1['parent_ids']);
						?>
						  <input type="hidden" name="parent_usertypes_test" value="<?php echo $row1['parent_usertypes'];?>">
						<?php 						
						for($i=1;$i<count($parent_usertypes);$i++){ ?>
						<div class="form-group">
						<label class="col-md-3"><?php echo $parent_usertypes[$i];?>:<span class="mandatory">*</span></label>
						<div class="col-md-4">
							<select name="<?php echo $parent_usertypes[$i];?>" 
							id="<?php echo $parent_usertypes[$i];?>" <?php if($i<(count($parent_usertypes)-1)){ ?> onchange="fngetchilds(this)"  <?php } ?> 
							class="form-control" <?php if($_SESSION[SESSION_PREFIX.'user_type']=="Superstockist") { echo "readonly";  } ?> required>
								<option value='' >-- Select --</option>
								<?php 
								$user_type=$parent_usertypes[$i];
								$result1 = $userconfObj->getLocalUserDetailsByUserType($user_type);		
								while($row = mysqli_fetch_array($result1)){	
								if(($row['user_type']==$_SESSION[SESSION_PREFIX.'user_type']||in_array($row['user_type'],$sess_parent_usertypes))&&($_SESSION[SESSION_PREFIX.'user_role']=='Superstockist'||$_SESSION[SESSION_PREFIX.'user_role']=='Distributor')){ 
									if($row['user_type']==$_SESSION[SESSION_PREFIX.'user_type']){
										if($row['id'] == $parent_ids[$i]){
											$disabled[]='disabled';
											$select_id_for_disable[]=$parent_usertypes[$i];
											$select_id_for_disable_id[]=$row['id'];
											 ?>
										 <option value="<?php echo $row['id'];?>" <?php if($row['id'] == $parent_ids[$i]) echo "selected"; ?> >
										 <?php echo fnStringToHTML($row['firstname']); ?>
										 </option>
									 <?php } }else if(in_array($row['user_type'],$sess_parent_usertypes)){
												$disabled[]='disabled';
												$select_id_for_disable[]=$parent_usertypes[$i];
												$select_id_for_disable_id[]=$row['id'];
												 if($row['id'] == $parent_ids[$i]){?>
										  <option value="<?php echo $row['id'];?>" <?php if($row['id'] == $parent_ids[$i]) echo "selected"; ?> >
										 <?php echo fnStringToHTML($row['firstname']); ?>
										 </option>
												 <?php  } }	}else{ 
												 if(in_array($seesion_user_id,  explode(',',$row['parent_ids']))){
												 ?>
										 <option value="<?php echo $row['id'];?>" <?php if($row['id'] == $parent_ids[$i]) echo "selected"; ?> >
										 <?php echo fnStringToHTML($row['firstname']); ?>
										 </option>
												 <?php } } } ?>
							</select>
						</div>
					</div> 
					<div id="hiddenfields_for_user"></div>
						<?php } ?>
						 <div class="form-group">
              <label class="col-md-3">Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text"
				placeholder="Enter Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"				
				name="firstname" class="form-control" value="<?php echo fnStringToHTML($row1['firstname'])?>">
              </div>
            </div><!-- /.form-group -->
             
			<div class="form-group">
              <label class="col-md-3">Username:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" id="username"
				placeholder="Enter Username"
				data-parsley-minlength="6"
				data-parsley-minlength-message="Username should be minimum 6 characters without blank spaces"          
				data-parsley-maxlength="25"
				data-parsley-maxlength-message="Only 25 characters are allowed"          
				data-parsley-type-message="Please enter Username, blank spaces are not allowed"		   
				data-parsley-required-message="Please enter Username, blank spaces are not allowed"
				data-parsley-trigger="change"
				data-parsley-required="true"
				data-parsley-pattern="/^[A-Za-z0-9]+(?:[_-][A-Za-z0-9]+)*$/"
                data-parsley-pattern-message="Username should be 6-25 alphanumeric characters & underscore, hyphen are allowed in between."
				name="username" class="form-control" value="<?php echo fnStringToHTML($row1['username'])?>"><span id="user-availability-status"></span>
              </div>
            </div><!--data-parsley-pattern="/^\S*$/" 
				data-parsley-error-message="Username should be 6-25 characters without blank spaces"-->
 <div class="form-group">
			  <label class="col-md-3">Address:<span class="mandatory">*</span></label>

			  <div class="col-md-4">
				<textarea name="address"
				 placeholder="Enter Address"
				data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter address"
				data-parsley-maxlength="200"
				data-parsley-maxlength-message="Only 200 characters are allowed"							
				rows="4" class="form-control"><?php echo fnStringToHTML($row1['address'])?></textarea>
			  </div>
			</div><!-- /.form-group -->
			<?php if($page_to_update != 'sales_person'){ ?>
			<div id="working_area_top">		
			<h4>User Details </h4>		
			</div>
		<?php if ($user_role == 'Shopkeeper' ){ 			
			$assigned_shop_array=$userconfObj->getshop_assigned_idsfor_shopkeeper($id);					
		?>
			<div class="form-group" id="hidden_div_shops" >
				<label class="col-md-3">Shops:</label>
				<div class="col-md-4" id="div_select_shops"> 
						<select name="assignshopids[]"  id="assignshopids"
							class="form-control" multiple >
							<option  disabled>-Select-</option>
							<?php	
							$all_shop_result=$userconfObj->getshop_update_shopkeeper($id);	
							foreach($all_shop_result as $key=>$value)
							{
								$All_shop_ids=$value['id'];
								$All_shop_names=$value['name'];
								$selected='';
								if($assigned_shop_array!=0)
								if (in_array($All_shop_ids, $assigned_shop_array)){$selected='selected';} ?>
								<option class="" value='<?php echo $All_shop_ids;?>' 
								<?php echo $selected;?>><?php echo $All_shop_names ;?></option>
							<?php } ?>
						</select>						
				</div>
			</div>
			<?php } ?>
			
			<div class="form-group">
			  <label class="col-md-3">State:<!-- <span class="mandatory">*</span> --></label>
			  <div class="col-md-4">
				<?php
					$sql_state="SELECT * FROM tbl_state where country_id=101";
					
					$result_state = mysqli_query($con,$sql_state);		
				?>
				<select name="state" id="state" class="form-control" 
				data-parsley-trigger="change"				
				data-parsley-required-message="Please select State"
				parsley-required="true"
				data-parsley-required-message="Please select State"
				 onChange="fnShowCity(this.value)">
					<?php
						if($row1['state'] == 0)
							echo "<option value=''>-Select-</option>";
						while($row_state = mysqli_fetch_array($result_state))
						{
							$selected = "";
							if($row_state['id'] == $row1['state'])
								$selected = "selected";				
						
							echo "<option value='".$row_state['id']."' $selected>" . fnStringToHTML($row_state['name']) . "</option>";
						}
					?>
				</select>
			  </div>
			</div><!-- /.form-group -->

			<div class="form-group" id="city_div">
			  <label class="col-md-3">District:<!-- <span class="mandatory">*</span> --></label>
			  <div class="col-md-4" id="div_select_city">
				<?php
					$option = "<option value=''>-Select-</option>";
					if($row1['city'] != '')
					{
						$sql_city="SELECT * FROM tbl_city where state_id='".$row1['state']."' ORDER BY name";
						
						$result_city = mysqli_query($con,$sql_city);
						while($row_city = mysqli_fetch_array($result_city)){
							$cat_id=$row_city['id'];
							$selected = "";
							if($row_city['id'] == $row1['city'])
								$selected = "selected";				
							
							$option.= "<option value='$cat_id' $selected>".$row_city['name']."</option>";
						}
					}												
				?>
			<!-- 	<select name="city" id="city" class="form-control"  
				data-parsley-trigger="change"
				data-parsley-required-message="Please select District"
				><?=$option;?></select> -->

<select name="city" id="city" class="form-control" 
onchange="FnGetSuburbDropDown(this)" 
data-parsley-trigger="change" >
<?=$option;?></select>
	

			  </div>
			</div><!-- /.form-group -->


			 
			<?php if($page_to_update != 'superstockist'){ ?>
			<div class="form-group">
			  <label class="col-md-3">Taluka:</label>
			<?php
				if($row1['suburb_ids'] != '' && $row1['suburb_ids'] != '0'){
					 //$sql_area="SELECT `id`, `cityid`, `stateid`, `suburbnm` FROM tbl_area WHERE id IN (".$row1['suburb_ids'].") and isdeleted!='1' ";

					 	$sql_area="SELECT `id`, `cityid`, `stateid`, `suburbnm` FROM tbl_area WHERE cityid = ".$row1['city']." and isdeleted!='1'";	
					
				}
				else{
					$sql_area="SELECT `id`, `cityid`, `stateid`, `suburbnm` FROM tbl_area WHERE cityid = '".$row1['city'] . "' and isdeleted!='1'";
				}
				
				$result_area = mysqli_query($con,$sql_area);
				$suburb_array = explode(',', $row1['suburb_ids']);
				//var_dump($suburb_array);
				$multiple = '';
				if(mysqli_num_rows($result_area) > 1)
				{
					$multiple = 'multiple';
				}
				
				$selected = "";
			?>
			  <div class="col-md-4"  id="div_select_area">
			  <select name="area[]" id="area" class="form-control" <?=$multiple;?> onchange="FnGetSubareaDropDown(this)">	
				<option value="">-Select-</option>
				<?php
				while($row_area = mysqli_fetch_array($result_area))
				{				
					if(count($suburb_array) > 0)
					{
                       //echo $row_area['id'];
                       $assign_id=$row_area['id'];
						$suburb_ids = explode(',', $row1['suburb_ids']);
						if(in_array($assign_id, $suburb_ids))
						{
							$selected = "selected";
						}
						else
						{
							 $selected = "";
						}
						
					}
					echo "<option value='".$row_area['id']."' $selected>" . fnStringToHTML($row_area['suburbnm']) . "</option>";
				}
				?>
				</select>				
			  </div>
			</div>
			<input type="hidden" id="selected_taluka" name="selected_taluka" value="<?=$suburb_array[0];?>">
			<!-- /.form-group -->
			
			<div class="form-group" id="subarea_div">
			  <label class="col-md-3">Subarea:</label>
			  <?php
				$rows_subarea = 0;
				if(isset($row1['subarea_ids']) && $row1['subarea_ids'] != '')
				{
					$sql_subarea="SELECT `subarea_id`, `state_id`, `city_id`, `suburb_id`, `subarea_name` 
					FROM tbl_subarea WHERE subarea_id IN (".$row1['subarea_ids'].")";				
					
					$result_subarea = mysqli_query($con,$sql_subarea);
					if($result_subarea != '')
						$rows_subarea = mysqli_num_rows($result_subarea);
					$subarea_array = explode(',', $row1['subarea_ids']);
					$multiple = '';
					if(count($subarea_array) > 1)
					{
						$multiple = 'multiple';
					}
					
					$selected = '';
				}
			  ?>
			  <div class="col-md-4" id="div_select_subarea">
			  <select name="subarea[]" id="subarea" data-parsley-trigger="change" class="form-control" <?=$multiple;?>>					
					<?php		
					if($rows_subarea == 0)
					{ 
						echo '<option value="">-Select-</option>';
					}
					else
					{
						while($row_subarea = mysqli_fetch_array($result_subarea))
						{		
							if(count($subarea_array) > 0)
							{
								if(in_array($row_subarea['subarea_id'], $subarea_array))
									$selected = "selected";
								
							}
							echo "<option value='".$row_subarea['subarea_id']."' $selected>" . fnStringToHTML($row_subarea['subarea_name']) . "</option>";
						}
					}
					?>
				</select>
			  </div>
			</div><!-- /.form-group --> 
			<?php }  ?>
<div id="working_area_bottom">				
</div>
			<?php } ?>
<div class="form-group">
			  <label class="col-md-3">Email:</label>

			  <div class="col-md-4">
				<input type="text" name="email" id="email" 
				placeholder="Enter E-mail"
				data-parsley-maxlength="100"
				data-parsley-maxlength-message="Only 100 characters are allowed"
				data-parsley-type="email"
				data-parsley-type-message="Please enter valid e-mail"		   
				
				data-parsley-trigger="change"
				
				class="form-control" value="<?php echo fnStringToHTML($row1['email'])?>">
			  </div>
			</div><!-- /.form-group -->


			<div class="form-group">
			  <label class="col-md-3">Mobile Number:<!-- <span class="mandatory">*</span> --></label>

			  <div class="col-md-4">
				<input type="text"  name="mobile"
				placeholder="Enter Mobile Number"
				data-parsley-required-message="Please enter mobile number"
				data-parsley-minlength="10"
				data-parsley-minlength-message="Mobile number should be of minimum 10 digits" 
				data-parsley-maxlength="15"				    
				data-parsley-maxlength-message="Only 15 digits are allowed"
				data-parsley-pattern="^(?!\s)[0-9]*$"
				data-parsley-pattern-message="Please enter numbers only"
				class="form-control" value="<?php echo fnStringToHTML($row1['mobile'])?>">
			  </div>
			</div><!-- /.form-group -->
			
				<div class="form-group">
              <label class="col-md-3">GST Number:<!-- <span class="mandatory">*</span> --></label>
              <div class="col-md-4">
                <input type="text"
				placeholder="Enter GST Number"
                data-parsley-trigger="change"
				data-parsley-required-message="Please enter GST Number"
				data-parsley-maxlength="15"
				data-parsley-maxlength-message="Only 15 characters are allowed"	
				data-parsley-minlength="15"
				data-parsley-minlength-message="Not Valid GST Number"	
				name="gst_number_sss" class="form-control" value="<?php echo fnStringToHTML($row1['gst_number_sss'])?>">
              </div>
            </div><!-- /.form-group -->	
						<!--new code-->
						<div class="form-group">
						  <label class="col-md-3">Office Phone No.:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Office Phone No."
							data-parsley-trigger="change"					
							data-parsley-minlength="10"
							data-parsley-maxlength="15"
							data-parsley-maxlength-message="Only 15 characters are allowed"
							data-parsley-pattern="^(?!\s)[0-9]*$"
							data-parsley-pattern-message="Please enter numbers only"
							name="phone_no" value="<?=$row1['office_phone_no'];?>" 
							class="form-control">
						  </div>
						</div>   

						 <div class="form-group">
              <label class="col-md-3">Latitude:</label>
              <div class="col-md-4">
                <input type="text" name="latitude" value="<?php echo $row1['latitude']?>"
                placeholder="Enter Latitude" 
				min="-180" max="180" step="0.0000000000000001" 
                            data-parsley-trigger="change"                                       
                            
				class="form-control"
				<?php if($_SESSION[SESSION_PREFIX."user_type"]!="Admin")  { ?> readonly  <?php } ?>>
              </div>
            </div><!-- /.form-group -->			
            <div class="form-group">
              <label class="col-md-3">Longitude:</label>
              <div class="col-md-4">
                <input type="text" name="longitude" value="<?php echo $row1['longitude']?>"
                placeholder="Enter Longitude" 
				min="-180" max="180" step="0.0000000000000001" 
                            data-parsley-trigger="change"                                       
                            
				class="form-control"
				<?php if($_SESSION[SESSION_PREFIX."user_type"]!="Admin")  { ?> readonly  <?php } ?>>
              </div>
            </div><!-- /.form-group -->	
			<?php if($row1['user_role']=='Superstockist' || $row1['user_role']=='Distributor'){ ?>
			<div class="form-group" >
				<label class="col-md-3">Margin Type:</label>
				<div class="col-md-4">
					<input type="radio" name="marginType" id="marginType_rolewise" value="0" <?php if($row1['marginType']=='0') echo "checked";?> onclick="fnChangeMarginType(this);"> Role Wise 
					&nbsp;&nbsp;
					<input type="radio" name="marginType" id="marginType_usertypewise" value="1" <?php if($row1['marginType']=='1') echo "checked";?> onclick="fnChangeMarginType(this);"> User Type Wise 
					&nbsp;&nbsp;
					<input type="radio" name="marginType" id="marginType_userid" value="2" <?php if($row1['marginType']=='2') echo "checked";?> onclick="fnChangeMarginType(this);"> User ID Wise
				</div>
			</div><!-- /.form-group -->
			<div class="form-group" id="userid_margindiv" <?php if($row1['marginType']!='2'){ ?>style="display: none;" <?php } ?>>
			  <label class="col-md-3">Margin(%):</label>
			  <div class="col-md-4">
				<input type="text"   id="userid_margin" name="userid_margin" value="<?php echo $userid_margin_percentage;?>" class="form-control" onkeyup="myFunction(this)"
				placeholder="Enter Margin"
				min="0" max="100" step="0.01" 
				data-parsley-trigger="change"										
				data-parsley-pattern="^[0-9]+?[\.0-9]*$" <?php if($row1['marginType']!='2') echo "disabled";?>  >																
				<!--data-parsley-pattern="^(?!\s)[0-9-!@#$%^&*+_=><,./:' ]*$" -->
			  </div>
			</div>
			<div class="form-group" id="userrole_margindiv" <?php if($row1['marginType']!='0'){ ?>style="display: none;" <?php } ?>>
			  <label class="col-md-3">Margin(%):</label>
			  <div class="col-md-4">
				<input type="text"   id="userrole_margin" name="userrole_margin" value="<?php echo $userrole_margin_percentage;?>" class="form-control" onkeyup="myFunction(this)"
				placeholder="Enter User Role Margin" 
				min="0" max="100" step="0.01" 
				data-parsley-trigger="change"                                       
				data-parsley-pattern="^[0-9]+?[\.0-9]*$" disabled >
			  </div>
			</div>
			<div class="form-group" id="usertype_margindiv" <?php if($row1['marginType']!='1'){ ?>style="display: none;" <?php } ?>>
			  <label class="col-md-3">Margin(%):</label>
			  <div class="col-md-4">
				<input type="text"   id="usertype_margin" name="usertype_margin" value="<?php echo $usertype_margin_percentage;?>" class="form-control" onkeyup="myFunction(this)"
				placeholder="Enter User Type Margin"
				min="0" max="100" step="0.01" 
				data-parsley-trigger="change"                                       
				data-parsley-pattern="^[0-9]+?[\.0-9]*$" disabled >
			  </div>
			</div>
			<?php } ?>	
						<div class="form-group">
							<label class="col-md-3"><b>Bank Details</b></label>
						</div>						
						 <div class="form-group">
							<label class="col-md-3">Account Name <!-- <span class="mandatory">*</span> --></label>
							<div class="col-md-4">
								<input type="text" 
								placeholder="Account Name"
								data-parsley-trigger="change"
								data-parsley-required-message="Please enter accouont number"
								data-parsley-maxlength="50"
								data-parsley-maxlength-message="Only 50 characters are allowed"
								name="accname" 
								value="<?=$row1['accname'];?>" 
								class="form-control">
							</div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Account Number:<!-- <span class="mandatory">*</span> --></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Account Number"
							data-parsley-trigger="change"
							data-parsley-required-message="Please enter accouont number"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"						
							name="accno" class="form-control" value="<?=$row1['accno'];?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Name:<!-- <span class="mandatory">*</span> --></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Bank Name"
							data-parsley-trigger="change"
							data-parsley-required-message="Please enter Bank Name"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"							
							name="bank_name" class="form-control" value="<?=$row1['bank_name'];?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Branch Name:<!-- <span class="mandatory">*</span> --></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Branch Name"
							data-parsley-trigger="change"
							data-parsley-required-message="Please enter branch Name"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"							
							name="accbrnm" class="form-control" value="<?=$row1['accbrnm'];?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">IFSC Code:<!-- <span class="mandatory">*</span> --></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter IFSC Code"
							data-parsley-trigger="change"
							data-parsley-required-message="Please enter ifsc Code"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"							
							name="accifsc" class="form-control" value="<?=$row1['accifsc'];?>">
						  </div>
						</div>
						
						<!-- <div class="form-group">
						  <label class="col-md-3">Status:</label>
						  <div class="col-md-4">
								<select name="user_status" id="user_status" class="form-control">
									<option value="Active" <?php if($row1['user_status'] == 'Active') echo "selected";?>>Active</option>
									<option value="Inactive" <?php if($row1['user_status'] == 'Inactive') echo "selected";?>>Inactive</option>
								</select>
						  </div>
						</div> -->
						<!-- /.form-group -->	
						<!--new code end-->
						
						<div class="form-group">
							<div class="col-md-4 col-md-offset-3">
								<input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
								<input type="hidden" name="hidAction" id="hidAction" value="user_update_config.php">
								<input type="hidden" name="id" id="id" value="<?=$row1['id'];?>">
								<? if($_SESSION[SESSION_PREFIX."user_type"]!="Distributor") { ?>
								<button name="getlatlong" id="getlatlong" type="button"  class="btn btn-primary">Get Lat Long</button>
								<button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button>
								<? } ?>
								<a href="user_list_config.php" class="btn btn-primary">Cancel</a>
							</div>
						</div><!-- /.form-group -->
					</form>  
				   <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
					<div class="modal-dialog" style="width:300px;">
						<div class="modal-content">
							<div class="modal-body">
								<p>
								<h4 style="color:red; text-align:center;">Do you want to delete this record ?</h4>
								</p>                     
							  <center><a href="user_update_config.php?id=<?php echo $row1['id']?>" ><button type="button" class="btn btn-success">Yes</button></a>
							  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
							  </center>
							</div>    
						</div>
					</div>
					</div>  
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<script>
$('.multipleSelect').fastselect();
</script>
<?php include "../includes/footer.php";?>
<script type="text/javascript">
 function myFunction(varl) {
	if (varl.value != "") {
        var arrtmp = varl.value.split(".");
        if (arrtmp.length > 1) {
			var strTmp = arrtmp[1];
			if (strTmp.length > 2) {
				varl.value = varl.value.substring(0, varl.value.length - 1);
			}
        }
	}
}
function fnChangeMarginType(margintype) {
	var margin_percentage_update=<?php echo json_encode($margin_percentage_update); ?>;
	var user_typej = $("input[name=user_type]").val();
	if(margintype.value==0){		
		 $("#userrole_margindiv").show();
		$("#usertype_margindiv").hide();
		$("#userid_margindiv").hide(); 
		$("#userid_margin").val('0.00');
		$("#userrole_margin").prop('disabled', true);
		$("#userrole_margin").val(margin_percentage_update['user_role'][user_typej]);
	}
	if(margintype.value==1){
		$("#usertype_margindiv").show();
		$("#userrole_margindiv").hide();
		$("#userid_margindiv").hide();	
		$("#userid_margin").val('0.00');
		$("#usertype_margin").prop('disabled', true);
		$("#usertype_margin").val(margin_percentage_update['user_type'][user_typej]);		
	}
	if(margintype.value==2){		
		$("#userid_margindiv").show();
		var userid_margin_percentage = <?php echo $userid_margin_percentage;?>;		
		$("#userid_margin").val(userid_margin_percentage.toFixed(2));
		$("#usertype_margindiv").hide();
		$("#userrole_margindiv").hide();
		$("#userid_margin").prop('disabled', false);
	}
}
</script>
<!-- END FOOTER -->
<!-- END PAGE LEVEL SCRIPTS -->
<script>
function fnShowCity(id_value) {	
	$("#city_div").show();	
	$("#area").html('<option value="">-Select-</option>');	
	$("#subarea").html('<option value="">-Select-</option>');
	var page_to_update = $("#page_to_update").val();
	var param = '';
	if(page_to_update == 'superstockist')
		param = "&nofunction=nofunction";
	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city"+param;
	CallAJAX(url,"div_select_city");	
}
function FnGetSuburbDropDown(id) {
	$("#area_div").show();	
	$("#subarea").html('<option value="">-Select-</option>');
	var page_to_update = $("#page_to_update").val();
	var param = '';
	if(page_to_update == 'stockist')
		param = "&nofunction=nofunction";	
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&multiple=multiple&function_name=FnGetSubareaDropDown"+param;
	CallAJAX(url,"div_select_area");
}

 

function calculate_data_count(element_value){
	element_value = element_value.toString();
	var element_arr = element_value.split(',');	
	return element_arr.length;
}
function setSelectNoValue(div,select_element){
	var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled value="">-Select-</option></select>';
	document.getElementById(div).innerHTML	=	select_selement_section;
}
function FnGetSubareaDropDown(id) {
	var suburb_str = $("#area").val();	
	$("#subarea").html('<option value="">-Select-</option>');	
	if(suburb_str != null)	{
		var suburb_arr_count = calculate_data_count(suburb_str);
		if(suburb_arr_count == 1){//If single city selected then only show its related subarea	
			$("#subarea_div").show();	
			var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea&multiple=multiple";
			CallAJAX(url,"div_select_subarea");
		}else if(suburb_arr_count > 1){
			$("#subarea_div").show();	
			var multiple_id = suburb_str.join(", ");
			var url = "getSubareaDropdown.php?multiple_id="+multiple_id+"&select_name_id=subarea&multiple=multiple";
			CallAJAX(url,"div_select_subarea");
		}else{
			setSelectNoValue("div_select_subarea", "subarea");
		}	
	}else{
			setSelectNoValue("div_select_subarea", "subarea");
		}	
}

function checkAvailability() {
	$('#updateform').parsley().validate();
	var email = $("#email").val();
	var username = $("#username").val();
	var id = $("#id").val();
	if(username != '')
	{
		jQuery.ajax({
			url: "../includes/checkUserAvailable.php",
			data:'validation_field=username&username='+username+'&id='+id,
			type: "POST",
			async:false,
			success:function(data){		
				if(data=="exist") {
					alert('Username already exists.');					
					return false;
				} else {
					if(email != '')
					{
						jQuery.ajax({
							url: "../includes/checkUserAvailable.php",
							data:'validation_field=email&email='+email+'&id='+id,
							type: "POST",
							async:false,
							success:function(data){			
								if(data=="exist") {
									alert('E-mail already exists.');									
									return false;
								} else {	
									submitFrm();
								}
							},
							error:function (){}
						});
					} else {
						submitFrm();
					}									
				}
			},
			error:function (){}
		});
	}
}
function submitFrm(){
	var action = $('#hidAction').val();
	$('#updateform').attr('action', action);					
	$('#hidbtnsubmit').val("submit");
	$('#updateform').submit();
}
</script> 
<script>
$( document ).ready(function() {
	//debugger;
	 var select_id_for_disable = <?php echo json_encode($select_id_for_disable); ?>;
	var select_id_for_disable_id = <?php echo json_encode($select_id_for_disable_id); ?>;
	var sess_parent_ids = <?php echo json_encode($sess_parent_ids); ?>;
	var seesion_user_id=<?php echo json_encode($seesion_user_id); ?>;
	 var disabled=<?php echo json_encode($disabled); ?>;
		//debugger;
	console.log(sess_parent_ids);
	 $.each(select_id_for_disable, function(key, value) {
		 if(disabled[key]=='disabled'){
			 //debugger;
			if((sess_parent_ids.includes(select_id_for_disable_id[key]))||seesion_user_id==select_id_for_disable_id[key]){
				//debugger;
				$('#'+value+'').attr('disabled',true);	
				$("#hiddenfields_for_user").append(" <input type='hidden' name='"+value+"' value='"+select_id_for_disable_id[key]+"' /> ");
			}
		 }
	});	
});
function fngetchilds(UserID1) {	
	var parent_usertypes = <?php echo json_encode($parent_usertypes); ?>;
	var parentid_value =UserID1.value;
	var parentid =UserID1.id;
	
	console.log(parent_usertypes);console.log(parentid_value);	console.log(parentid);
	
	var nextid=0;
	$.each(parent_usertypes, function(key, value) {
		 // alert(key);alert(value);
		  if(parentid==value){nextid=key+1;}
	});
	console.log(nextid);console.log(parent_usertypes[nextid]);
	var nextid_selector=parent_usertypes[nextid];
		
	var child_options;
	var url = "conf_get_childs.php"; 
	jQuery.ajax({
		url: url,
		method: 'POST',
		data: 'user_id='+parentid_value+'&puser_type='+parent_usertypes[nextid]+'&getflag=onchange_getchild_edit',
		async: false
	}).done(function (response) {
		console.log(response);
		child_options=response;				
	}).fail(function () { });
					
	if(child_options!='no_parent'){
		$('#'+nextid_selector+'')
			.find('option')
			.remove()
			.end()
			.append(""+child_options+"");		
		for(jtemp=nextid+1;jtemp<parent_usertypes.length;jtemp++){	
			nextid_selector=parent_usertypes[jtemp];
			$('#'+nextid_selector+'')
			.find('option')
			.remove()
			.end()
			.append("");
		}
	}else{
		alert("You can not add this user type as there is no child exist!");
		for(jtemp=nextid;jtemp<parent_usertypes.length;jtemp++){
			nextid_selector=parent_usertypes[jtemp];
			$('#'+nextid_selector+'')
			.find('option')
			.remove()
			.end()
			.append("<option value='' selected>-- Select --</option>");
		}
	}
	
}

</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLEAPIKEY;?>" type="text/javascript"></script>
<script type="text/javascript">
$( "#getlatlong" ).click(function() {
  //alert( "Handler for .click() called." );
  //$(this).find("option:selected").text();
  var curaddress=$('[name="address"]').val();
  var subarea=$('#subarea').val();
  if(subarea!=''){subarea=", "+$('#subarea').find("option:selected").text();}else{subarea='';}
   var area=$('#area').val();
  if(area!=''){area=", "+$('#area').find("option:selected").text();}else{area='';}
   var city=$('[name="city"]').val();
  if(city!=''){city=", "+$('[name="city"]').find("option:selected").text();}else{city='';}
   var state=$('[name="state"]').val();
  if(state!=''){state=", "+$('[name="state"]').find("option:selected").text();}else{state='';}
	var address = curaddress+''+subarea+''+ area+''+city+''+state;	
					//alert(address);
	//var address123 = address.replace("/^[a-zA-Z\s](\d)?$/","");
	//alert(address123);
					
  var geocoder = new google.maps.Geocoder();
	//var address = "new york";

	geocoder.geocode( { 'address': address}, function(results, status) {

	  if (status == google.maps.GeocoderStatus.OK) {
		var latitude = results[0].geometry.location.lat();
		var longitude = results[0].geometry.location.lng();		
		$('[name="latitude"]').val(latitude);
		$('[name="longitude"]').val(longitude);
	  } else{
		  alert("Not getting proper latitude,longitude!");
	  }
	}); 
});

</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>