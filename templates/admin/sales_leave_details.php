<?php
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/reportManage.php";
$reportObj = new reportManage($con, $conmain);

$today = date('Y-m-d');
if($today <= date('Y-m-15')){
	$frmdate = date("01-m-Y");
	$todate  = date("15-m-Y");
}else{
	$frmdate = date("16-m-Y");
	$todate  =  date("d-m-Y", strtotime("last day of this month"));
}
$report_title = "Leave Dates <br> From ".$frmdate." To ".$todate;
$row = $reportObj->get_all_sp_leave();
//echo "<pre>";print_r($row);

$colspan = "3";
?>
<? if($_POST["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
    table, th, td {  border: 1px solid black; } 
    body { font-family: "Open Sans", sans-serif; 
           background-color:#fff;
           font-size: 11px;
           direction: ltr;}
    </style>
    <? } ?>

 <table 
        class="table table-striped table-bordered table-hover table-highlight table-checkable" 
    data-provide="datatable" 
    data-display-rows="10"
    data-info="true"
    data-search="true"
    data-length-change="true"
    data-paginate="true"
    id="sample_2232">

    <thead >
        <tr>
            <td colspan="<?= $colspan; ?>" align="canter" class="gradeX even" style="text-align:center; font-weight:600;">
            <h4>
            <b><?php if (!empty($report_title)) echo $report_title;?></b></h4></td>              
        </tr>
        <tr style=" display:table;
    width: 100%;
    table-layout:fixed;">
            <th data-filterable="false" data-sortable="true" data-direction="desc">Added Date</th>
            <th data-filterable="false" data-sortable="false" data-direction="de">Sales Person Name</th>
            <th data-filterable="false" data-sortable="false" data-direction="de">Reason</th>
        </tr>
    </thead>
    <tbody style="overflow: auto;
    height: 150px;
    display: block;
    width:100%;">                   
        <?php
        if (!empty($row)) {
           foreach($row as $key=>$value){
                ?>
                <tr class="odd gradeX" style="display:table;
    width:100%;
    table-layout:fixed;">
                    <td  ><?php echo date('d-m-Y',strtotime($value['tdate']));?></td>
                    <td  ><?= $value['firstname']; ?></td>
                    <td  ><?= $value['reason']; ?></td>
                </tr>
            <?php } 
        } else{
          echo "<tr class='odd gradeX' style='display:table;
    width:100%;
    table-layout:fixed;'><td colspan='3' align='center'>No matching records found</td></tr>";
          } 
        ?>
    </tbody>    
</table>
<!--<script>
    jQuery(document).ready(function () {
        ComponentsPickers.init();
    });

    jQuery(document).ready(function () {
        TableManaged.init();
    });
    $(document).ready(function () {
        var table = $('#sample_2').dataTable();        
        table.fnFilter('');
    });
</script>-->

<!-- END JAVA SCRIPTS -->
<?php
if ($_POST["actionType"] == "excel") {
    if ($row != 0) {
        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=SP_leave_Report.xls");
    }
}
?>
 