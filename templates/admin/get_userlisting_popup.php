<?php
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/userManage.php";   
$userObj    =   new userManager($con,$conmain);
$id=$_GET["cat_id"];
 $sql="SELECT u.firstname,u.id,u.email,u.mobile,u.city,
(select name from tbl_city where id= u.city) as cityname,u.state,
(select name from tbl_state where id= u.state) as statename
 FROM `tbl_user` u where   external_id='".$id."' and user_type='SalesPerson' and u.isdeleted!='1'  order by u.id desc";
$result1 = mysqli_query($con,$sql);
$countrows=mysqli_num_rows($result1);
?>
<? if($_POST["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<? } ?>
<table 
	class="table table-striped table-bordered table-hover table-highlight table-checkable" 
	data-provide="datatable" 
	data-display-rows="10"
	data-info="true"
	data-search="true"
	data-length-change="true"
	data-paginate="true"
	id="sample_2">

<thead>

  <tr>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Name</th>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Email</th>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Mobile No.</th>	
	<th data-filterable="false" data-sortable="true" data-direction="desc">City</th>
	<th data-filterable="false" data-sortable="true" data-direction="desc">State</th>
	       
  </tr>
</thead>
<tbody>					
	<?php 
	if($countrows>0){
		while($val = mysqli_fetch_array($result1))
{ ?>
				<tr class="odd gradeX">
					<td><?=$val['firstname'];?></td>
					<td><?=$val['email'];?></td>
					<td><?=$val['mobile'];?></td>
					<td><?=$val['cityname'];?></td>
					<td><?=$val['statename'];?></td>	
				</tr>	
			<?php }
	}
		if($_POST["actionType"]=="excel" &&  $val == 0) {
			echo "<tr><td>No matching records found</td></tr>";
		}
	?>				
</tbody>	
</table>
<!-- END JAVASCRIPTS -->
<?
if($_POST["actionType"]=="excel") {
	if($row != 0){
		header("Content-Type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=Report_summary.xls");
		exit;
	}
} ?>
 