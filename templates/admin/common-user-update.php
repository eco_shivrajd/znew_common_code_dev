<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/commonuserManage.php";	
$commonuserObj 	= 	new commonuserManager($con,$conmain);
?>
<!-- END HEADER -->
<?php
$u_type=$_GET['u_type'];
$common_user_id=$_GET['id'];

if(isset($_POST['hidbtnsubmit'])) 
{
	$common_user_id=$_POST['common_user_id'];
	//print_r($_POST);
	//exit();
	
	//$user_type = "Distributor";
			$commonuserObj->updateInstanceCommonUserDetails($common_user_id);
			$commonuserObj->updateAllInstanceCommonUser($common_user_id);

			echo '<script>alert("User Updated successfully.");location.href="common-user.php";</script>';	
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	 $activeMainMenu = "ManageSupplyChain"; $activeMenu = "Common User";
    include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- /.modal -->
			<h3 class="page-title">User Update</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="sales.php">User Update</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#"><? if($_SESSION[SESSION_PREFIX.'user_type']=="Distributor") { echo "Sales Person Details"; } else { echo "Edit User Update"; } ?></a> 
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<? if($_SESSION[SESSION_PREFIX.'user_type']=="Distributor") { echo "Sales Person Details"; } else { echo "Edit Sales Person"; } ?>
							</div>
							 <a href="updatepassword.php?id=<?php echo base64_encode($_GET['id']);?>" class="btn btn-sm btn-default pull-right mt5">							
                                Change Password
                              </a>
						</div>
						<div class="portlet-body">
						<? if($_SESSION[SESSION_PREFIX."user_type"]!="Distributor") { ?>
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						<? } ?>
					    <?php
						$common_user_id			= $_GET['id'];
						$row1 = $commonuserObj->getAllCommonUserDetails($common_user_id);	
                       // $row1['state_ids'] = $all_user_details['state'];
						//$row1['city_ids'] = $all_user_details['city'];
						?>                       

					<form  name="updateform" id="updateform" class="form-horizontal" role="form" data-parsley-validate="" action="" method="post">

						<div class="form-group">
						<label class="col-md-3">User Type:<span class="mandatory">*</span></label>
						<div class="col-md-4">
						<input type="text"		
						name="u_type" class="form-control" value="<?php echo fnStringToHTML($row1['user_type'])?>" disabled>

						<input type="hidden" name="user_type1" value="<?php echo fnStringToHTML($row1['user_type'])?>">
						</div>
						</div>


						<div class="form-group" >
						  <label class="col-md-3">Manufacturer:<span class="mandatory">*</span></label>

                           <?php							
							$result1 = $commonuserObj->getCommonUserManufacturerId($common_user_id);						
							while($row = mysqli_fetch_array($result1))
							{
								$old_manufacturer_id1 = $row['common_user_manufacturers'];
								$old_manufacturer_id = explode(',', $old_manufacturer_id1);
								// var_dump($old_manufacturer_id);
							}
							?>
                            
						  <div class="col-md-4">
							<select name="manufacturer_id[]" class="form-control" multiple> 
							<?php							
							$result1 = $commonuserObj->getManufacturer();						
							while($row = mysqli_fetch_array($result1))
							{
								$manufacturer_id = $row['id'];								

								if (in_array($manufacturer_id, $old_manufacturer_id))
								{        
									echo "<option value='$manufacturer_id' selected>" . fnStringToHTML($row['clientnm']) . "</option>";
          
								}
								else
								{

									echo "<option value='$manufacturer_id' >" . fnStringToHTML($row['clientnm']) . "</option>";
								}
							}
							?>
							</select>
							<input type="hidden" name="old_manufacturer_id" value="<?=$old_manufacturer_id1;?>">
						  </div>
						</div> 	
			
					  
			<div class="form-group">
              <label class="col-md-3">Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text"
				placeholder="Enter Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"				
				name="firstname" class="form-control" value="<?php echo fnStringToHTML($row1['firstname'])?>">
              </div>
            </div>
             
			<div class="form-group">
              <label class="col-md-3">Username:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text" id="username"
				placeholder="Enter Username"
				data-parsley-minlength="6"
				data-parsley-minlength-message="Username should be minimum 6 characters without blank spaces"          
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"          
				data-parsley-type-message="Please enter Username, blank spaces are not allowed"		   
				data-parsley-required-message="Please enter Username, blank spaces are not allowed"
				data-parsley-trigger="change"
				data-parsley-required="true"
				data-parsley-pattern="/^\S*$/" 
				data-parsley-error-message="Username should be 6-50 characters without blank spaces"
				name="username" class="form-control" value="<?php echo fnStringToHTML($row1['username'])?>"><span id="user-availability-status"></span>
              </div>
            </div>

 <div class="form-group">
			  <label class="col-md-3">Address:<span class="mandatory">*</span></label>
			  <div class="col-md-4">
				<textarea name="address"
				 placeholder="Enter Address"
				data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter address"
				data-parsley-maxlength="200"
				data-parsley-maxlength-message="Only 200 characters are allowed"
				rows="4" class="form-control"><?php echo fnStringToHTML($row1['address'])?></textarea>
			  </div>
			</div>
			<?php if($page_to_update != 'sales_person'){ ?>
			<div id="working_area_top">		
			<h4>Assign <?php if($page_to_update == 'stockist'){?>Taluka<?php }else{ ?>District<?php }?></h4>		
			</div>
			<div class="form-group">
			  <label class="col-md-3">State:<span class="mandatory">*</span></label>
			  <div class="col-md-4">
				<?php
					$sql_state="SELECT * FROM tbl_state where country_id=101";
					
					$result_state = mysqli_query($con,$sql_state);		
				?>
				<select name="state" id="state" class="form-control" 
				data-parsley-trigger="change"
				data-parsley-required="#true" 				
				data-parsley-required-message="Please select State"
				parsley-required="true"
				data-parsley-required-message="Please select State"
				 onChange="fnShowCity(this.value)">
					<?php
						if($row1['state'] == 0)
							echo "<option value=''>-Select-</option>";
						while($row_state = mysqli_fetch_array($result_state))
						{
							$selected = "";
							if($row_state['id'] == $row1['state'])
								$selected = "selected";				
						
							echo "<option value='".$row_state['id']."' $selected>" . fnStringToHTML($row_state['name']) . "</option>";
						}
					?>
				</select>
			  </div>
			</div>

			<div class="form-group" id="city_div">
			  <label class="col-md-3">District:<span class="mandatory">*</span></label>
			  <div class="col-md-4" id="div_select_city">
				<?php
					$option = "<option value=''>-Select-</option>";
					if($row1['city'] != '')
					{
						$sql_city="SELECT * FROM tbl_city where state_id='".$row1['state']."' ORDER BY name";
						
						$result_city = mysqli_query($con,$sql_city);
						while($row_city = mysqli_fetch_array($result_city)){
							$cat_id=$row_city['id'];
							$selected = "";
							if($row_city['id'] == $row1['city'])
								$selected = "selected";				
							
							$option.= "<option value='$cat_id' $selected>".$row_city['name']."</option>";
						}
					}												
				?>
				<select name="city" id="city" class="form-control"  
				data-parsley-trigger="change"
				data-parsley-required="#true"	
				data-parsley-required-message="Please select District"
				><?=$option;?></select>
			  </div>
			</div>
			 
			<?php if($page_to_update != 'superstockist'){ ?>
			<div class="form-group">
			  <label class="col-md-3">Taluka:</label>
			<?php
				if($row1['suburb_ids'] != '' && $row1['suburb_ids'] != '0'){
					 	$sql_area="SELECT `id`, `cityid`, `stateid`, `suburbnm` FROM tbl_area WHERE cityid = ".$row1['city']." and isdeleted!='1'";						
				}
				else{
					$sql_area="SELECT `id`, `cityid`, `stateid`, `suburbnm` FROM tbl_area WHERE cityid = '".$row1['city'] . "' and isdeleted!='1'";
				}				
				$result_area = mysqli_query($con,$sql_area);
				$suburb_array = explode(',', $row1['suburb_ids']);
				$multiple = '';
				if(mysqli_num_rows($result_area) > 1)
				{
					$multiple = 'multiple';
				}
				
				$selected = "";
			?>
			  <div class="col-md-4"  id="div_select_area">
			  <select name="area[]" id="area" class="form-control" <?=$multiple;?> onchange="FnGetSubareaDropDown(this)">	
				<option value="">-Select-</option>
				<?php
				while($row_area = mysqli_fetch_array($result_area))
				{				
					if(count($suburb_array) > 0)
					{
                       //echo $row_area['id'];
                       $assign_id=$row_area['id'];
						$suburb_ids = explode(',', $row1['suburb_ids']);
						if(in_array($assign_id, $suburb_ids))
						{
							$selected = "selected";
						}
						else
						{
							 $selected = "";
						}
						
					}
					echo "<option value='".$row_area['id']."' $selected>" . fnStringToHTML($row_area['suburbnm']) . "</option>";
				}
				?>
				</select>				
			  </div>
			</div>
			<input type="hidden" id="selected_taluka" name="selected_taluka" value="<?=$suburb_array[0];?>">
			<div class="form-group" id="subarea_div">
			  <label class="col-md-3">Subarea:</label>
			  <?php
				$rows_subarea = 0;
				if(isset($row1['subarea_ids']) && $row1['subarea_ids'] != '')
				{
					$sql_subarea="SELECT `subarea_id`, `state_id`, `city_id`, `suburb_id`, `subarea_name` 
					FROM tbl_subarea WHERE subarea_id IN (".$row1['subarea_ids'].")";				
					
					$result_subarea = mysqli_query($con,$sql_subarea);
					if($result_subarea != '')
						$rows_subarea = mysqli_num_rows($result_subarea);
					$subarea_array = explode(',', $row1['subarea_ids']);
					$multiple = '';
					if(count($subarea_array) > 1)
					{
						$multiple = 'multiple';
					}
					
					$selected = '';
				}
			  ?>
			  <div class="col-md-4" id="div_select_subarea">
			  <select name="subarea[]" id="subarea" data-parsley-trigger="change" class="form-control" <?=$multiple;?>>					
					<?php		
					if($rows_subarea == 0)
					{ 
						echo '<option value="">-Select-</option>';
					}
					else
					{
						while($row_subarea = mysqli_fetch_array($result_subarea))
						{		
							if(count($subarea_array) > 0)
							{
								if(in_array($row_subarea['subarea_id'], $subarea_array))
									$selected = "selected";
								
							}
							echo "<option value='".$row_subarea['subarea_id']."' $selected>" . fnStringToHTML($row_subarea['subarea_name']) . "</option>";
						}
					}
					?>
				</select>
			  </div>
			</div>
			<?php }  ?>
<div id="working_area_bottom">				
</div>
			<?php } ?>
<div class="form-group">
			  <label class="col-md-3">Email:</label>

			  <div class="col-md-4">
				<input type="text" name="email" id="email" 
				placeholder="Enter E-mail"
				data-parsley-maxlength="100"
				data-parsley-maxlength-message="Only 100 characters are allowed"
				data-parsley-type="email"
				data-parsley-type-message="Please enter valid e-mail"		   
				
				data-parsley-trigger="change"
				
				class="form-control" value="<?php echo fnStringToHTML($row1['email'])?>">
			  </div>
			</div>

			<div class="form-group">
			  <label class="col-md-3">Mobile Number:<span class="mandatory">*</span></label>
			  <div class="col-md-4">
				<input type="text"  name="mobile"
				placeholder="Enter Mobile Number"
				data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter mobile number"
				data-parsley-minlength="10"
				data-parsley-minlength-message="Mobile number should be of minimum 10 digits" 
				data-parsley-maxlength="15"				    
				data-parsley-maxlength-message="Only 15 digits are allowed"
				data-parsley-pattern="^(?!\s)[0-9]*$"
				data-parsley-pattern-message="Please enter numbers only"
				class="form-control" value="<?php echo fnStringToHTML($row1['mobile'])?>">
			  </div>
			</div>
			
				<div class="form-group">
              <label class="col-md-3">GST Number:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text"
				placeholder="Enter GST Number"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter GST Number"
				data-parsley-maxlength="15"
				data-parsley-maxlength-message="Only 15 characters are allowed"	
				data-parsley-minlength="15"
				data-parsley-minlength-message="Not Valid GST Number"	
				name="gst_number_sss" class="form-control" value="<?php echo fnStringToHTML($row1['gst_number_sss'])?>">
              </div>
            </div>
						<div class="form-group">
						  <label class="col-md-3">Office Phone No.:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Office Phone No."
							data-parsley-trigger="change"					
							data-parsley-minlength="10"
							data-parsley-maxlength="15"
							data-parsley-maxlength-message="Only 15 characters are allowed"
							data-parsley-pattern="^(?!\s)[0-9]*$"
							data-parsley-pattern-message="Please enter numbers only"
							name="phone_no" value="<?=$row1['office_phone_no'];?>" 
							class="form-control">
						  </div>
						</div>   

						 <div class="form-group">
              <label class="col-md-3">Latitude:</label>
              <div class="col-md-4">
                <input type="text" name="latitude" value="<?php echo $row1['latitude']?>"
                placeholder="Enter Latitude" min="-180" max="180" step="0.0000000000000001" 
                            data-parsley-trigger="change"   
				class="form-control"
				<?php if($_SESSION[SESSION_PREFIX."user_type"]!="Admin")  { ?> readonly disabled <?php } ?>>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Longitude:</label>
              <div class="col-md-4">
                <input type="text" name="longitude" value="<?php echo $row1['longitude']?>"
                placeholder="Enter Longitude" data-parsley-pattern="^(?!\s)[a-zA-Z0-9!@#$%^&*+_=><,./:' ]*$" data-parsley-trigger="change"
				class="form-control"
				<?php if($_SESSION[SESSION_PREFIX."user_type"]!="Admin")  { ?> readonly disabled <?php } ?>>
              </div>
            </div>
						
						<!-- <div class="form-group">
							<label class="col-md-3"><b>Bank Details</b></label>
						</div>						
						 <div class="form-group">
							<label class="col-md-3">Account Name <span class="mandatory">*</span></label>
							<div class="col-md-4">
								<input type="text" 
								placeholder="Account Name"
								data-parsley-trigger="change"				
								data-parsley-required="#true" 
								data-parsley-required-message="Please enter accouont number"
								data-parsley-maxlength="50"
								data-parsley-maxlength-message="Only 50 characters are allowed"
								name="accname" 
								value="<?=$row1['accname'];?>" 
								class="form-control">
							</div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Account Number:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Account Number"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter accouont number"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"						
							name="accno" class="form-control" value="<?=$row1['accno'];?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Name:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Bank Name"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter Bank Name"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"							
							name="bank_name" class="form-control" value="<?=$row1['bank_name'];?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Branch Name:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Branch Name"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter branch Name"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"							
							name="accbrnm" class="form-control" value="<?=$row1['accbrnm'];?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">IFSC Code:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter IFSC Code"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter ifsc Code"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"							
							name="accifsc" class="form-control" value="<?=$row1['accifsc'];?>">
						  </div>
						</div>
						
						<div class="form-group">
						  <label class="col-md-3">Status:</label>
						  <div class="col-md-4">
								<select name="user_status" id="user_status" class="form-control">
									<option value="Active" <?php if($row1['user_status'] == 'Active') echo "selected";?>>Active</option>
									<option value="Inactive" <?php if($row1['user_status'] == 'Inactive') echo "selected";?>>Inactive</option>
								</select>
						  </div>
						</div>  -->
						
						<div class="form-group">
							<div class="col-md-4 col-md-offset-3">
								<input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
								<input type="hidden" name="hidAction" id="hidAction" value="common-user-update.php">
								<input type="hidden" name="common_user_id" id="common_user_id" value="<?=$row1['common_user_id'];?>">
								<? if($_SESSION[SESSION_PREFIX."user_type"]!="Distributor") { ?>
								<button name="getlatlong" id="getlatlong" type="button"  class="btn btn-primary">Getlatlong</button>
								<button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button>
								<? } ?>
								<a href="sales.php" class="btn btn-primary">Cancel</a>
							</div>
						</div>
					</form>  
				   <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
					<div class="modal-dialog" style="width:300px;">
						<div class="modal-content">
							<div class="modal-body">
								<p>
								<h4 style="color:red; text-align:center;">Do you want to delete this record ?</h4>
								</p>                     
							  <center><a href="user_update.php?id=<?php echo $row1['id']?>" ><button type="button" class="btn btn-success">Yes</button></a>
							  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
							  </center>
							</div>    
						</div>
					</div>
					</div>  
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php";?>
<!-- END FOOTER -->
<!-- END PAGE LEVEL SCRIPTS -->
<script>
function fnShowCity(id_value) {	
	$("#city_div").show();	
	$("#area").html('<option value="">-Select-</option>');	
	$("#subarea").html('<option value="">-Select-</option>');
	var page_to_update = $("#page_to_update").val();
	var param = '';
	if(page_to_update == 'superstockist')
		param = "&nofunction=nofunction";
	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city&mandatory=mandatory"+param;
	CallAJAX(url,"div_select_city");	
}
function FnGetSuburbDropDown(id) {
	$("#area_div").show();	
	$("#subarea").html('<option value="">-Select-</option>');
	var page_to_update = $("#page_to_update").val();
	var param = '';
	if(page_to_update == 'stockist')
		param = "&nofunction=nofunction";	
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&multiple=multiple&function_name=FnGetSubareaDropDown"+param;
	CallAJAX(url,"div_select_area");
}

 

function calculate_data_count(element_value){
	element_value = element_value.toString();
	var element_arr = element_value.split(',');	
	return element_arr.length;
}
function setSelectNoValue(div,select_element){
	var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled value="">-Select-</option></select>';
	document.getElementById(div).innerHTML	=	select_selement_section;
}
function FnGetSubareaDropDown(id) {
	var suburb_str = $("#area").val();	
	$("#subarea").html('<option value="">-Select-</option>');	
	if(suburb_str != null)	{
		var suburb_arr_count = calculate_data_count(suburb_str);
		if(suburb_arr_count == 1){//If single city selected then only show its related subarea	
			$("#subarea_div").show();	
			var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea&multiple=multiple";
			CallAJAX(url,"div_select_subarea");
		}else if(suburb_arr_count > 1){
			$("#subarea_div").show();	
			var multiple_id = suburb_str.join(", ");
			var url = "getSubareaDropdown.php?multiple_id="+multiple_id+"&select_name_id=subarea&multiple=multiple";
			CallAJAX(url,"div_select_subarea");
		}else{
			setSelectNoValue("div_select_subarea", "subarea");
		}	
	}else{
			setSelectNoValue("div_select_subarea", "subarea");
		}	
}

function checkAvailability() {
	$('#updateform').parsley().validate();
	var email = $("#email").val();
	var username = $("#username").val();
	var id = $("#id").val();
	if(username != '')
	{
		jQuery.ajax({
			url: "../includes/checkUserAvailable.php",
			data:'validation_field=username&username='+username+'&id='+id,
			type: "POST",
			async:false,
			success:function(data){		
				if(data=="exist") {
					alert('Username already exists.');					
					return false;
				} else {
					if(email != '')
					{
						jQuery.ajax({
							url: "../includes/checkUserAvailable.php",
							data:'validation_field=email&email='+email+'&id='+id,
							type: "POST",
							async:false,
							success:function(data){			
								if(data=="exist") {
									alert('E-mail already exists.');									
									return false;
								} else {	
									submitFrm();
								}
							},
							error:function (){}
						});
					} else {
						submitFrm();
					}									
				}
			},
			error:function (){}
		});
	}
}
function submitFrm(){
	var action = $('#hidAction').val();
	$('#updateform').attr('action', action);					
	$('#hidbtnsubmit').val("submit");
	$('#updateform').submit();
}
</script> 
<script>
function fnShowStockist(id){
if (window.XMLHttpRequest)
{
xmlhttp=new XMLHttpRequest();
}
else
{
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("Subcategory").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("GET","fetchstockist.php?cat_id="+id.value+"&multiple=multiple&user_type_manage=sales",true);
xmlhttp.send();
 }
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLEAPIKEY;?>" type="text/javascript"></script>
<script type="text/javascript">
$( "#getlatlong" ).click(function() {
  //alert( "Handler for .click() called." );
  //$(this).find("option:selected").text();
  var curaddress=$('[name="address"]').val();
  var subarea=$('#subarea').val();
  if(subarea!=''){subarea=", "+$('#subarea').find("option:selected").text();}else{subarea='';}
   var area=$('#area').val();
  if(area!=''){area=", "+$('#area').find("option:selected").text();}else{area='';}
   var city=$('[name="city"]').val();
  if(city!=''){city=", "+$('[name="city"]').find("option:selected").text();}else{city='';}
   var state=$('[name="state"]').val();
  if(state!=''){state=", "+$('[name="state"]').find("option:selected").text();}else{state='';}
	var address = curaddress+''+subarea+''+ area+''+city+''+state;	
					//alert(address);
	//var address123 = address.replace("/^[a-zA-Z\s](\d)?$/","");
	//alert(address123);
					
  var geocoder = new google.maps.Geocoder();
	//var address = "new york";

	geocoder.geocode( { 'address': address}, function(results, status) {

	  if (status == google.maps.GeocoderStatus.OK) {
		var latitude = results[0].geometry.location.lat();
		var longitude = results[0].geometry.location.lng();		
		$('[name="latitude"]').val(latitude);
		$('[name="longitude"]').val(longitude);
	  } else{
		  alert("Not getting proper latitude,longitude!");
	  }
	}); 
});

</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>