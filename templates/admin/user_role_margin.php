<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
if($_SESSION[SESSION_PREFIX.'user_type']!="Admin") 
{
	header("location:../logout.php");
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">

	<!-- BEGIN SIDEBAR -->
	<?php 
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "User-Role-Margin";
	include "../includes/sidebar.php"
	?>	
	<!-- END SIDEBAR -->
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			User Role Margin
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">User Role Margin</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
            <div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								User Role Margin Listing
							</div>                             
						</div>
						 <div class="clearfix"></div>
						<div class="portlet-body">
							<div class="clearfix"></div>
							<table 
							class="table table-striped table-bordered table-hover table-highlight table-checkable" 
							data-provide="datatable" 
							data-display-rows="10"
							data-info="true"
							data-search="true"
							data-length-change="true"
							data-paginate="true"
							id="sample_2">
							<!--<table class="table table-striped table-bordered table-hover" id="sample_2">-->
							<thead>
							<tr>
								<th>
									 User Role 
								</th>
								<th>
									Margin
								</th>
								<th>
                                  Action
                                </th>
							</tr>
							</thead>
							<tbody>
							<?php
							$sql="SELECT * FROM `tbl_userrole` where user_role='Distributor' OR user_role='Superstockist' OR user_role='DeliveryChannelPerson'";
							$result1 = mysqli_query($con,$sql);
							 if(mysqli_num_rows($result1)>0){
							while($row = mysqli_fetch_array($result1))
							{						
								echo '<tr class="odd gradeX"><td>';
                                if($ischecked_edit==1){
								echo '<a href="user_role_margin_update.php?id='.base64_encode($row['id']).'&u_role='.base64_encode($row['user_role']).'">'.fnStringToHTML($row['user_role']).'</a>';
                                 }else{
                                                echo '' . fnStringToHTML($row['user_role']) . '';}
								echo '</td>';
                                echo '<td align="right">';
                                echo $row['userrole_margin'];
								echo '</td>';								
								echo '<td>';
								          if ($ischecked_edit==1) 
                                            {
                                               echo '<a title="Edit" href="user_role_margin_update.php?id='.base64_encode($row['id']).'&u_role='.base64_encode($row['user_role']).'" class="btn btn-xs btn-primary "><span class="glyphicon glyphicon-edit"></span></a>'; 
                                            } else {
                                                echo '<a title="Can Not Edit This Record" href="#" class="btn btn-xs btn-primary" disabled><span class="glyphicon glyphicon-edit"></span></a>';
                                            } 
								echo '</td>';
								echo '</tr>';
							 } }
							?>
							</tbody>
							</table>
						</div>
					</div>
            
				
                    
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>