<!-- BEGIN HEADER -->
<?php 
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include "../includes/grid_header.php";
include "../includes/userConfigManage.php";   
$userConfObj    =   new userConfigManager($con,$conmain);

$profile_id1 = $_GET['profile_id1'];
$user_type = $_GET['user_type'];

?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "UserType";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<h3 class="page-title"><?php echo $user_type;?> Role Profile</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li><i class="fa fa-home"></i>
					<a href="#"><?php echo $user_type;?> Role Profile</a></li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				
					<div class="portlet box blue-steel">
						<div class="portlet-title">						
							<div class="caption">Profile view</div>							
							<? if($_SESSION[SESSION_PREFIX."user_role"]=="Admin")  
							{ 
								?>
								<!--<a href="user_profile_conf_edit.php?profile_id1=<?=$profile_id1;?>&user_type=<?=$user_type;?>" 
								class="btn btn-sm btn-default pull-right mt5">Update Permissions</a>-->
							<? } ?>
							
                            <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
						
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th width="27%" style="text-align: left !important">Modules</th>
										<th width="11%" style="text-align: center !important">View</th>
										<th width="11%" style="text-align: center !important">Create</th>
										<th width="11%" style="text-align: center !important">Edit</th>
										<th width="11%" style="text-align: center !important">Delete</th>   
									</tr>
								</thead>
							<tbody>
							<?php
							//$profile_id=$_SESSION[SESSION_PREFIX."user_id"];
					        $sql = "SELECT id,page_name FROM `tbl_pages` where isdeleted!=1";							
                            $result = mysqli_query($con, $sql);							
							$profile_checkboxes=$userConfObj->get_action_profile_checkbox($profile_id1);
							//echo "<pre>";print_r($profile_checkboxes);
							while($row = mysqli_fetch_array($result)){
								$keystring=$profile_id1.''.$row['id'];									
								$ischecked_view=$profile_checkboxes[$keystring]['ischecked_view'];
								$ischecked_add=$profile_checkboxes[$keystring]['ischecked_add'];
								$ischecked_edit=$profile_checkboxes[$keystring]['ischecked_edit'];
								$ischecked_delete=$profile_checkboxes[$keystring]['ischecked_delete'];
								?>
								<tr class="odd gradeX">
									<td data-module-name="Dashboard" data-module-status="1"><img src="easyTree/Enable.png">&nbsp;<?php echo  $row['page_name'];	?></td>
									<td data-action-state="DetailView" data-moduleaction-status="1" style="text-align: center;">
										<?php if($ischecked_view==1){ ?><img src="easyTree/Enable.png"><?php }else{?><img src="easyTree/Disable.png"><?php } ?>
									</td>
									<td data-action-state="CreateView" data-moduleaction-status="" style="text-align: center;">
									<?php if($ischecked_add==1){ ?><img src="easyTree/Enable.png"><?php }else{?><img src="easyTree/Disable.png"><?php } ?>
									</td>
									<td data-action-state="EditView" data-moduleaction-status="" style="text-align: center;">
									<?php if($ischecked_edit==1){ ?><img src="easyTree/Enable.png"><?php }else{?><img src="easyTree/Disable.png"><?php } ?>
									</td>
									<td data-action-state="Delete" data-moduleaction-status="" style="text-align: center;">
									<?php if($ischecked_delete==1){ ?><img src="easyTree/Enable.png"><?php }else{ ?><img src="easyTree/Disable.png"><?php } ?>
									</td>									
								</tr>
						<?php	} ?>	
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>