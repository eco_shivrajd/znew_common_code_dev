<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/orderManage.php";
include "../includes/userManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
$userObj 	= 	new userManager($con,$conmain);

$sp_id = $_GET['id'];
if($sp_id == '')
{
	header("location:order_summary.php");
}
$user_data = $userObj->getLocalUserDetails($sp_id);
if($_SESSION[SESSION_PREFIX.'user_type'] != 'Admin')
{
	/*$belong = $userObj->checkUserBelongTo($sp_id,$user_data['external_id']);
	if($belong == 1)
	{
		header("location:order_summary.php");
	}*/
}
$filter_date = $_GET['param1'];
if($filter_date == '')
{
	header("location:order_summary.php");
}
$date1 = null;
$date2 = null;
if(isset($_GET['param2']))
	$date1=$_GET['param2'];
if(isset($_GET['param3']))
	$date2=$_GET['param3'];

$row = $orderObj->getSPShopOrdersSummary($sp_id, $filter_date,$date1,$date2);
//echo "<pre>";print_r($row); 
$report_title = $orderObj->getReportTitleForSP($filter_date,$date1,$date2);
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Reports"; $activeMenu = "OrderSummary";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<h3 class="page-title">
			Summary
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="javascript:;">Sales Person: <?=$user_data['firstname'];?></a>	
					</li>
				</ul>				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
		    <div class="row">
				<div class="col-md-12"> 				
					<div class="clearfix"></div>   
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption"><i class="icon-puzzle"></i>Shopwise Report</div>
							<button type="button" name="btnExcel" id="btnExcel" onclick="report_download();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
							&nbsp;
							<button type="button" name="btnPrint" id="btnPrint" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
						</div>	
						<div class="portlet-body">
							<table 
								class="table table-striped table-bordered table-hover table-highlight table-checkable" 
								data-provide="datatable" 
								data-display-rows="10"
								data-info="true"
								data-search="true"
								data-length-change="true"
								data-paginate="true"
								id="sample_2">
							<thead>							
							  <tr>
								<th>Shop Name</th>							
								<th  id="rupee">Total Sales <i aria-hidden='true' class='fa fa-inr'></i></th>              
							  </tr>
							</thead>
							<tbody>					
								<?php 
									foreach($row as $key => $val) {
										$param = "?id=".$sp_id."&s_id=".$val['shop_id']."&param1=".$filter_date;
										if($filter_date == 3)
										{
											$from_date = $_GET['param2'];
											$to_date = $_GET['param3'];
											$param.="&param2=".$from_date;
											$param.="&param3=".$to_date;
										}
									
								?>
										<tr class="odd gradeX">
										<td><a href="shop_orders.php<?=$param;?>"><?=$val['shop_name'];?></a></td>											
										<td align='right'><?=fnAmountFormat($val['Total_Sales']);?></td>									
										</tr>			
								<?php	} 	?>	
										
							</tbody>	
							</table>
						</div>
					</div>
				</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->	
	<!-- END QUICK SIDEBAR -->
			</div>
<!-- END CONTAINER -->
		</div>
	</div>
</div>
<div id="table_heading"  style="display:none;"><?=$user_data['firstname'];?>'s Shopwise <?=$report_title;?></div>
<div id="print_div"  style="display:none;"></div>
<form action="../includes/exportToExcel.php" method="post" name="export_excel" id="export_excel">
	<input type="hidden" name="export_data" id="export_data">
</form>
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
function report_download() {
	var td_rec = $("#sample_2 td:last").html();
	if(td_rec != 'No matching records found')
	{
		var divContents = $(".table-striped").html();
		$("#print_div").html('<table id="print_table" style="text-decoration:none;">'+divContents+'</table>');	
		var heading = $("#table_heading").html();
		
		$('<tr><th colspan="2" align="center">'+heading+'</th></tr>').insertBefore('#print_table tr:first');
		$("#print_table tr th i").html("RS");	
		divContents =  $("#print_div").html();
		
		divContents = divContents.replace(/<\/*a.*?>/gi,'');	
		$("#export_data").val(divContents);
		document.forms.export_excel.submit();
	}else{
		alert("No matching records found");
	}
}
$("#btnPrint").live("click", function () {
	var td_rec = $("#sample_2 td:last").html();
	if(td_rec != 'No matching records found')
	{
		var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
		
		var divContents = $(".table-striped").html();
		$("#print_div").html('<table id="print_table">'+divContents+'</table>');	
		var heading = $("#table_heading").html();
		$("#print_table tr th i").html("₹");	
		$('<tr><th colspan="2" align="center">'+heading+'</th></tr>').insertBefore('#print_table tr:first');
		divContents =  $("#print_div").html();
		
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write('<html><head><title>Report</title>');
		printWindow.document.write('<style type="text/css">a{text-decoration: none; color:#333333;} table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
		printWindow.document.write('</head><body >');
		printWindow.document.write(divContents);
		printWindow.document.write('</body></html>');	
		printWindow.focus();	
		if( navigator.userAgent.toLowerCase().indexOf('chrome') > -1 ){		
			setTimeout(function () {
				printWindow.print();
				//printWindow.close();
			}, 500);
		}else if(isIE == true){			
			printWindow.document.execCommand("print", false, null);
			//printWindow.close();
		}
		else{
			setTimeout(function () {				
				printWindow.print();
				//printWindow.close();
			}, 100);
		}
	}else{
		alert("No matching records found");
	}
});
</script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- END JAVASCRIPTS -->