<!-- BEGIN HEADER -->
<?php
//error_reporting(E_ERROR | E_PARSE);
//ini_set('display_errors', 1);
include "../includes/grid_header.php"; 
include "../includes/customerManage.php";
$customerObj    =   new customerManager($con,$conmain);
$user_id = $_SESSION[SESSION_PREFIX . 'user_id'];
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "ManageSupplyChain";
        $activeMenu = "customer_list";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

                <!-- /.modal -->

                <h3 class="page-title">
                    Customer
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="#">Customer</a>
                        </li>
                    </ul>

                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">


                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">
                                    Customer Listing
                                </div>
                                <?php if ($ischecked_add==1)  {
                                ?>
                                <a class="btn btn-sm btn-default pull-right mt5" href="customer-add.php">
                                    Add Customer
                                </a>
                                <?php } ?>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">

                                <table class="table table-striped table-bordered table-hover" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th>
                                                Customer Name
                                            </th>
                                            <th>
                                                Mobile No.
                                            </th>
                                            <th>
                                                Action
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    $result1 = $customerObj->getAllCustomerDetails();
                                    $row_count = mysqli_num_rows($result1);
                                    if ($row_count !=0)
                                     {                                            
                                        while ($row = mysqli_fetch_array($result1)) 
                                        { 
                                        $cust_id = $row['cust_id'];                                         
                                        ?>
                                        <tr class="odd gradeX">
                                            <td>   
                                                <?php if ($ischecked_edit==1 ) { ?>
                                                        <a href="customer-update.php?id=<?php echo base64_encode($cust_id);?>">
                                                            <?php echo $row['cust_name']; ?>
                                                        </a>
                                                        <?php } else {  echo $row['cust_name']; } ?>
                                            </td>
                                            <td><?=$row['cust_mobile'];?></td>
                                            <td>
                                                    <?php if ($ischecked_edit==1 ) { ?>
                                                        <a href="customer-update.php?id=<?php echo base64_encode($cust_id);?>" class="btn btn-xs btn-primary ">
                                                            <span class="glyphicon glyphicon-edit"></span>
                                                        </a>
                                                        <?php 
                                                      } 
                                                      else {   echo '<a title="Can Not Edit This Record" href="#" class="btn btn-xs btn-primary" disabled><span class="glyphicon glyphicon-edit"></span></a>'; } ?>
                                                            <?php if ($row_count > 1 && $ischecked_delete==1 ) { ?>
                                                                <a class="btn btn-xs btn-danger" Onclick="return ConfirmDelete()"
                                                               
 href="ajax_product_manage.php?cust_action=<?=base64_encode('delete_customer');?>&cust_id=<?=base64_encode($row['cust_id']);?>" class="btn btn-xs btn-danger">
                                                                    <span class="glyphicon glyphicon-trash"></span>
                                                                </a>
                                                                <?php } else {  echo '<a title="Can Not Delete This Record" onclick="#" class="btn btn-xs btn-danger" disabled><span class="glyphicon glyphicon-trash"></span></a>';  } ?>
                                                </td>
                                        </tr>
                                    <?php } } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
 <?php include "../includes/grid_footer.php" ?>
    <!-- END FOOTER -->
    <script>
function ConfirmDelete() {  
        return confirm("Are you sure you want to delete this Customer?");
}
</script>
    <script>
        function deleteBrand(brand_id) {
            var response = ajax_call('ajax_product_manage.php?action=check_product_available');
            //alert(response);
            //console.log(response);
            if (response != 'more_than_one_product') {
                if (response != 'no_product') {

                    var record = response.split("####");
                    var product = record[0];
                    var category = record[1];
                    var brand = record[2];
                    if (brand_id == brand) {
                        alert('This Brand cannot be deleted as only one Product is there and which belong to this Brand.');
                        return false;
                    } else {
                        if (confirm('Category, Products under this Brand will also get deleted. Are you sure that you want to delete this Brand?')) {
                            var response = ajax_call('ajax_product_manage.php?action=delete_brand&brand_id=' + brand_id);
                            if (response == 'brand_deleted') {
                                alert('Brand deleted successfully');
                                location.reload();
                            }
                        }
                    }
                }
            } else {
                if (confirm('Category, Products under this Brand will also get deleted. Are you sure that you want to delete this Brand?')) {
                    var response = ajax_call('ajax_product_manage.php?action=delete_brand&brand_id=' + brand_id);
                    if (response == 'brand_deleted') {
                        alert('Brand deleted successfully.');
                        location.reload();
                    }
                }
            }
        }
        function ajax_call(url) {
            var response;
            $.ajax({
                type: "GET",
                url: url,
                async: false,
                success: function (data) {
                    response = data;
                },
                error: function (xhr, status, error) {
                    //alert(xhr.responseText);
                }
            });
            return response;
        }
    </script>
    <script>
$(document).ready( function() {
    //$('#sample_2').destroy();
     var table = $('#sample_2').dataTable({ 
         destroy: true,
        stateSave: true,
        aoColumnDefs: [{ 
              'bSortable': false, 
              'aTargets': [ 1 ] // <-- gets last column and turns off sorting
             }]
        });
         $('input[type=search]').on("input", function() {
            table.fnFilter('');
        });     
    }); 

</script>
</body>
<!-- END BODY -->
</html>