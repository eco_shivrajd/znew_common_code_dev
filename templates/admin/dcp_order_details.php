<!-- BEGIN HEADER -->
<?php
//phpinfo();die();
include "../includes/grid_header.php";

include "../includes/userManage.php";
include "../includes/shopManage.php";
include "../includes/productManage.php";
include "../includes/orderManage.php";
include "../includes/reportManage.php";

$userObj = new userManager($con, $conmain);
$shopObj = new shopManager($con, $conmain);
$prodObj = new productManage($con, $conmain);
$orderObj = new orderManage($con, $conmain);
$reportObj = new reportManage($con, $conmain);
?>
<!-- END HEADER -->
<style>
    .form-horizontal .control-label {
        text-align: left;
    }
</style>

</head>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLEAPIKEY; ?>&callback=initMap" type="text/javascript"></script>
<!-- END HEAD -->
<body class="page-header-fixed page-quick-sidebar-over-content ">

    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "Orders";
        $activeMenu = "DCPOrders";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->

        <div class="page-content-wrapper">
            <div class="page-content">			
                <h3 class="page-title">
                    Manage Orders 
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="#">Manage Orders </a>
                        </li>
                    </ul>				
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">
                                    Manage Orders  
                                </div>
                                <div class="clearfix"></div>
                            </div>						
                            <div class="portlet-body">

                                <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">


                                    <div class="form-group" id="divDaily">
                                        <label class="col-md-3">Select Order Date:</label>
                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <input type="text" class="form-control  date date-picker1" data-date="<?php echo date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years" name="frmdate" id="frmdate" value="">
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                            <!-- /input-group -->								 
                                        </div>
                                    </div><!-- /.form-group -->	
                                    <?php if ($_SESSION[SESSION_PREFIX . "user_type"] == "Admin") { ?>
                                        <div class="form-group">
                                            <label class="col-md-3">Superstockist:</label>
                                            <div class="col-md-4">

                                                <select name="cmbSuperStockist" id="cmbSuperStockist" onchange="fnShowStockist(this)"
                                                        class="form-control">
                                                    <option value="">-Select-</option>
                                                    <?php
                                                    $user_type = "Superstockist";
                                                    $sql = "SELECT firstname,id FROM `tbl_user` where user_type ='$user_type' order by firstname";
                                                    $result1 = mysqli_query($con, $sql);
                                                    while ($row = mysqli_fetch_array($result1)) {
                                                        $cat_id = $row['id'];
                                                        echo "<option value='$cat_id'>" . fnStringToHTML($row['firstname']) . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- /.form-group -->
                                    <?php } else if ($_SESSION[SESSION_PREFIX . "user_type"] == "Superstockist") { ?>

                                        <input type="hidden" name="cmbSuperStockist" id="cmbSuperStockist" value="<?= $_SESSION[SESSION_PREFIX . "user_id"]; ?>">
                                    <?php } else { ?>
                                        <input type="hidden" name="cmbSuperStockist" id="cmbSuperStockist" value="">
                                        <input type="hidden" name="dropdownStockist" id="dropdownStockist" value="<?= $_SESSION[SESSION_PREFIX . "user_id"]; ?>"> 
                                    <?php } ?>


                                    <div class="form-group">
                                        <label class="col-md-3">Delivery Channel Person:</label>
                                        <?php $user_result = $userObj->getAllLocalUser('DeliveryChannelPerson'); ?>		
                                        <div class="col-md-4" id="divsalespersonDropdown">
                                            <select name="dropdownSalesPerson" id="dropdownSalesPerson" class="form-control">
                                                <option value="">-Select-</option>
                                                <?php while ($row_user = mysqli_fetch_assoc($user_result)) {
                                                    ?>									
                                                    <option value="<?= $row_user['id']; ?>"><?= $row_user['firstname']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label class="col-md-3">State:</label>
                                        <div class="col-md-4">
                                            <select name="dropdownState" id="dropdownState"              
                                                    data-parsley-trigger="change"				
                                                    data-parsley-required="#true" 
                                                    data-parsley-required-message="Please select state"
                                                    class="form-control" onChange="fnShowCity(this.value)">
                                                <option selected disabled>-Select-</option>
                                                <?php
                                                $sql = "SELECT id,name FROM tbl_state where country_id=101 order by name";
                                                $result = mysqli_query($con, $sql);
                                                while ($row = mysqli_fetch_array($result)) {
                                                    $cat_id = $row['id'];
                                                    echo "<option value='$cat_id'>" . $row['name'] . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>			
                                    <div class="form-group" id="city_div" style="display:none;">
                                        <label class="col-md-3">City:</label>
                                        <div class="col-md-4" id="div_select_city">
                                            <select name="dropdownCity" id="dropdownCity" data-parsley-trigger="change" class="form-control">
                                                <option selected value="">-Select-</option>										
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group" id="area_div" style="display:none;">
                                        <label class="col-md-3">Region:</label>
                                        <div class="col-md-4" id="div_select_area">
                                            <select name="area" id="area" data-parsley-trigger="change" class="form-control">
                                                <option selected value="">-Select-</option>									
                                            </select>
                                        </div>
                                    </div>									

                                    <div class="form-group">
                                        <label class="col-md-3">Category:</label>
                                        <div class="col-md-4" id="divCategoryDropDown">
                                            <?php $cat_result = $prodObj->getAllCategory(); ?>
                                            <select name="dropdownCategory" id="dropdownCategory" class="form-control" onchange="fnShowProducts(this)">
                                                <option value="">-Select-</option>
                                                <?php while ($row_cat = mysqli_fetch_assoc($cat_result)) {
                                                    ?>									
                                                    <option value="<?= $row_cat['id']; ?>"><?= $row_cat['categorynm']; ?></option>
                                                <?php } ?>								
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label class="col-md-3">Product:</label>
                                        <div class="col-md-4" id="divProductdropdown">
                                            <?php $prod_result = $prodObj->getAllProducts(); ?>
                                            <select name="dropdownProducts" id="dropdownProducts" class="form-control">
                                                <option value="">-Select-</option>
                                                <?php while ($row_prod = mysqli_fetch_assoc($prod_result)) {
                                                    ?>									
                                                    <option value="<?= $row_prod['id']; ?>"><?= $row_prod['productname']; ?></option>
                                                <?php } ?>	
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-3">
                                            <button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>									
                                            <button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnReset();">Reset</button>
                                        </div>
                                    </div><!-- /.form-group -->
                                </form>	


                                <form class="form-horizontal" role="form" name="form" method="post" action="">	
                                    <div id="order_list">
                                        <?php
                                        $orders = $reportObj->getOrderschnage1(); //getOrders
                                         //print"<pre>";print_R($orders);	die();						
                                        $order_count = count($orders);
                                        //echo "".$order_count;
                                        ?>
                                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                            <thead>
                                                <tr id="main_th">

                                                    <th>
                                                        Region
                                                    </th>
                                                    <th>
                                                        Delivery Channel Person
                                                    </th>
                                                    <th>
                                                        Cartons Name
                                                    </th>
                                                    <th>
                                                        Order Id<br><?php echo str_repeat("&nbsp;", 8); ?>Order Date 
                                                    </th>								

                                                    <th>
                                                        Product Name			
                                                    </th>	
                                                    <th>
                                                        Quantity
                                                    </th>
                                                    <th>
                                                        Total Price (₹)
                                                    </th>
                                                    <th>
                                                        Price(₹)<br>(GST+Discount)
                                                    </th>
                                                    <th>
                                                        Action  
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($orders as $key => $value) {
                                                   // echo"<pre>";print_r($value);
                                                    $order_totalcost = 0;
                                                    $order_totalgstcost = 0;
                                                    $product_name = '';
                                                    $prod_qnty = '';
                                                    $totalcost = '';
                                                    $gstcost = '';
                                                    $orderdetails = $reportObj->getOrdersDetailschnage($value['oid']);
                                                    //echo "<pre>";print_r($orderdetails);die();
                                                    //$odid=$orderdetails['order_details'][0]['odid'];
                                                    
                                                        foreach ($orderdetails['order_details'] as $key1 => $value1) {                                                           
                                                            $region_name = $value1['region_name'];
                                                            $order_by_name = $value1['firstname'];
                                                            $cartons_id = $value1['cartons_id'];
                                                            $order_date = $value1['order_date'];
                                                            $product_name =$product_name."". $value1['cat_name'] . "-" . $value1['product_name'] . "<br>";
                                                            $prod_qnty .= '<a onclick="showOrderDetails(\'' . $value['oid'] . '\',\'' . $value1['product_id'] . '\',\'' . $value1['product_variant_id'] . '\')" title="Order Details">' . $value1['product_quantity'];
                                                            if (!empty($value1['product_variant_weight1'])) {
                                                                $prod_qnty .= "(" . $value1['product_variant_weight1'] . "-" . $value1['product_variant_unit1'] . ")";
                                                            }
                                                            if (!empty($value1['product_variant_weight2'])) {
                                                                $prod_qnty .= "(" . $value1['product_variant_weight2'] . "-" . $value1['product_variant_unit2'] . ")";
                                                            }
                                                            $prod_qnty .= "</a>"; //free_product_details
                                                            
                                                            $prod_qnty .= "<br>";
                                                            $totalcost .= $value1['product_total_cost']; //discount_amount//discounted_totalcost
                                                           
                                                            $totalcost .= "<br>";
                                                            $order_totalcost = $order_totalcost + $value1['product_total_cost'];

                                                            $gstcost .= $value1['p_cost_cgst_sgst'] . "<br>";
                                                            $order_totalgstcost = $order_totalgstcost + $value1['p_cost_cgst_sgst'];
                                                        }
                                                    
                                                    ?>
                                                    <tr class="odd gradeX">
                                                        <td ><?php echo $region_name; ?></td>
                                                        <td ><?php echo $order_by_name; ?></td>					
                                                        <td ><?php echo "carton-" . $cartons_id; ?></td>
                                                        <td ><font size="1.4"><?php echo $value["oid"] ?></font><br><?php echo date('d-m-Y H:i:s', strtotime($order_date)); ?></td>										
                                                        <td><?php echo $product_name; ?></td>
                                                        <td ><?php echo $prod_qnty; ?></td>
                                                        <td align="right"><?php echo $totalcost;
                                                        echo "<b>" . $order_totalcost . "</b>";
                                                    ?></td>
                                                        <td align="right"><?php echo $gstcost;
                                                        echo "<b>" . $order_totalgstcost . "</b>";
                                                        ?></td>
                                                        <td>View Invoice
<!--                                                            <a onclick="showInvoice(1,<?= $value['oid']; ?>)" title="View Invoice">View Invoice</a>-->
                                                        </td>
                                                    </tr>
                                                <?php } ?>

                                            </tbody>
                                        </table>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>

    <!-- END CONTAINER -->
    <div class="modal fade" id="view_invoice" role="dialog">
        <div class="modal-dialog" style="width: 980px !important;">    
            <!-- Modal content-->
            <div class="modal-content" id="view_invoice_content">      
            </div>      
        </div>
    </div>
    <div class="modal fade" id="order_details" role="dialog">
        <div class="modal-dialog" style="width: 880px !important;">
            <!-- Modal content-->
            <div class="modal-content" id="order_details_content">
            </div>
        </div>
    </div>

    <div class="modal fade" id="googleMapPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="model_content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                </div>
                <div class="modal-body" style="padding-bottom: 5px !important;"> 
                    <div id="map" style="width: 100%; height: 500px;"></div> 
                </div>
            </div>
        </div>
    </div>



    <!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php" ?>
    <!-- END FOOTER -->
    <script>
        $(document).ready(function () {
            $("#statusbtn").hide();
            $("#select_th").removeAttr("class");
            $("#main_th th").removeAttr("class");
            if ($.fn.dataTable.isDataTable('#sample_2')) {
                table = $('#sample_2').DataTable();
                table.destroy();
                table = $('#sample_2').DataTable({
                    "aaSorting": [],
                });
            }
        });

        $('.date-picker1').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            endDate: "<?php echo date('d-m-Y'); ?>",
            autoclose: true
        });
        $('.date-picker2').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "bottom auto",
            endDate: "<?php echo date('d-m-Y'); ?>",
            autoclose: true
        });
        $('.date-picker3').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            endDate: "<?php echo date('d-m-Y'); ?>",
            autoclose: true
        });
        function fnReset() {
            location.reload();
        }

        function fnShowCity(id_value) {
            $("#city_div").show();
            $("#area").html('<option value="">-Select-</option>');
            $("#subarea").html('<option value="">-Select-</option>');
            var url = "getCityDropDown.php?cat_id=" + id_value + "&select_name_id=city&mandatory=mandatory";
            CallAJAX(url, "div_select_city");
        }
        function FnGetSuburbDropDown(id) {
            $("#area_div").show();
            $("#subarea").html('<option value="">-Select-</option>');
            var url = "getSuburDropdown.php?cityId=" + id.value + "&select_name_id=area&function_name=FnGetSubareaDropDown&mandatory=mandatory";
            CallAJAX(url, "div_select_area");
        }

        function FnGetShopsDropdown(id) {
            $("#shop_div").show();
            $("#divShopdropdown").html('<option value="">-Select-</option>');
            var param = "";
            var state_id = $("#dropdownState").val();
            var city_id = $("#city").val();
            var suburb_id = $("#area").val();
            if (state_id != '')
                param = param + "&state_id=" + state_id;
            if (city_id != '')
                param = param + "&city_id=" + city_id;
            if (suburb_id != '') {
                if (suburb_id != undefined) {
                    param = param + "&suburb_id=" + suburb_id;
                }
            }
            if (id != '') {
                if (id != undefined)
                    param = param + "&suburb_id=" + id.value;
            }

            var url = "getShopDropdownByAddress.php?param=param" + param;
            CallAJAX(url, "divShopdropdown");
        }
        function fnShowProducts(id) {
            var url = "getProductDropdown.php?cat_id=" + id.value;
            CallAJAX(url, "divProductdropdown");
        }
        $("#select_all").click(function () {
            $('input:checkbox').prop('checked', this.checked);
        });

        function ShowReport() {
            //alert("sdfsdf");
            var order_status = $('#order_status').val();
            alert(order_status);
            if (order_status == '13') {
                $("#statusbtn").show();
            } else {
                $("#statusbtn").hide();
            }

            var url = "ajax_show_orders.php";

            var data = $('#frmsearch').serialize();

            jQuery.ajax({
                url: url,
                method: 'POST',
                data: data,
                async: false
            }).done(function (response) {
                $('#order_list').html(response);
                var table = $('#sample_2').dataTable();
                table.fnFilter('');
                $("#select_th").removeAttr("class");
            }).fail(function () { });
            return false;
        }
        function showInvoice(order_status, id) {
            var url = "dcp_invoice.php";
            jQuery.ajax({
                url: url,
                method: 'POST',
                data: 'order_status=' + order_status + '&order_id=' + id,
                async: false
            }).done(function (response) {
                $('#view_invoice_content').html(response);
                $('#view_invoice').modal('show');
            }).fail(function () { });
            return false;
        }
        function showOrderDetails(id,prod_id,prod_var_id) {
            var url = "dcp_order_details_popup.php";
            jQuery.ajax({
                url: url,
                method: 'POST',
                data: 'order_details_id=' + id +'&prod_id='+prod_id+'&prod_var_id='+prod_var_id,
                async: false
            }).done(function (response) {
                $('#order_details_content').html(response);
                $('#order_details').modal('show');
            }).fail(function () { });
            return false;
        }
        function OrderDetailsPrint() {
            var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
            var divContents = '<style>\body {\
                    font-size: 12px;}\
            th {text-align: left;}\
            .printHeading { line-height: 18px;  padding: 10px 0;  font-size: 18px; }\
            table { border-collapse: collapse;  \
                    font-size: 12px; }\
            table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
            body { font-family: "Open Sans", sans-serif;\
            background-color:#fff;\
            font-size: 15px;\
            direction: ltr;}</style>' + $("#divOrderPrintArea").html();
            if (isIE == true) {
                var printWindow = window.open('', '', 'height=400,width=800');
                printWindow.document.write(divContents);
                printWindow.focus();
                printWindow.document.execCommand("print", false, null);
            } else {
                $('<iframe>', {
                    name: 'myiframe',
                    class: 'printFrame'
                }).appendTo('body').contents().find('body').html(divContents);
                window.frames['myiframe'].focus();
                window.frames['myiframe'].print();
                setTimeout(
                        function ()
                        {
                            $(".printFrame").remove();
                        }, 1000);
            }
        }
        function takeprint11() {
            var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
            var divContents = '<style>\body {\
                    font-size: 12px;}\
            th {text-align: left;}\
            .printHeading { line-height: 18px;  padding: 10px 0;  font-size: 18px; }\
            table { border-collapse: collapse;  \
                    font-size: 12px; }\
            table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
            body { font-family: "Open Sans", sans-serif;\
            background-color:#fff;\
            font-size: 15px;\
            direction: ltr;}</style>' + $("#divPrintArea").html();
            if (isIE == true) {
                var printWindow = window.open('', '', 'height=400,width=800');
                printWindow.document.write(divContents);
                printWindow.focus();
                printWindow.document.execCommand("print", false, null);
            } else {
                $('<iframe>', {
                    name: 'myiframe',
                    class: 'printFrame'
                }).appendTo('body').contents().find('body').html(divContents);
                window.frames['myiframe'].focus();
                window.frames['myiframe'].print();
                setTimeout(
                        function ()
                        {
                            $(".printFrame").remove();
                        }, 1000);
            }
        }
        function takeprint_invoice() {
            var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
            var divContents = '<style>\
            .darkgreen{	background-color:#364622; color:#fff!important; font-size:24px; font-weight:600;}\
            .fentgreen1{background-color:#b0b29c;color:#4a5036;	font-size:12px;}\
            .fentgreen{	background-color:#b0b29c;	color:#4a5036;}\
            .font-big{	font-size:20px;	font-weight:600;	color:#364622;}\
            .font-big1{	font-size:18px;	font-weight:600;	color:#364622;}\
            .table-bordered-popup {    border: 1px solid #364622;}\
            .table-bordered-popup > tbody > tr > td, .table-bordered-popup > tbody > tr > th, .table-bordered-popup > thead > tr > td, .table-bordered-popup > thead > tr > th {\
                    border: 1px solid #364622;	color:#4a5036;}\
            .blue{	color:#010057;}\
            .blue1{	color:#574960;	font-size:16px;}\
            .buyer_section{	color:#574960;	font-size:14px;}\
            .pad-5{	padding-left:10px;}\
            .pad-40{	padding-left:40px;}\
            .np{	padding-left:0px;	padding-right:0px;}\
            .bg{	background-image:url(../../assets/global/img/watermark.png); background-repeat:no-repeat;\
             background-size: 200px 200px;}\
            </style>' + $("#divPrintArea").html();
            if (isIE == true) {
                var printWindow = window.open('', '', 'height=400,width=800');
                printWindow.document.write(divContents);
                printWindow.focus();
                printWindow.document.execCommand("print", false, null);
            } else {
                $('<iframe>', {
                    name: 'myiframe',
                    class: 'printFrame'
                }).appendTo('body').contents().find('body').html(divContents);
                window.frames['myiframe'].focus();
                window.frames['myiframe'].print();
                setTimeout(
                        function ()
                        {
                            $(".printFrame").remove();
                        }, 1000);
            }
        }
        var lat = 0;
        var lng = 0;

        $('#googleMapPopup').on('shown.bs.modal', function (e) {

            var latlng = new google.maps.LatLng(lat, lng);
            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 17
            });
            var marker = new google.maps.Marker({
                map: map,
                position: latlng,
                draggable: false,
                //anchorPoint: new google.maps.Point(0, -29)
            });
        });
        function showGoogleMap(getlat, getlng) {

            lat = getlat;
            lng = getlng;
            $('#googleMapPopup').modal('show');
        }

        $("#statusbtn").click(function () {
            var void1 = [];
            $('input[name="select_all[]"]:checked').each(function () {
                void1.push(this.value);
            });
            var energy = void1.join();
            if (energy != '') {
                $("#assign_delivery").modal('show');
                $("#orderids").val(energy);
            } else {
                alert("Please select minimum one order");
                //location.reload();
            }

        });
        $("#statusDbtn").click(function () {

            var void1 = [];
            $('input[name="select_all[]"]:checked').each(function () {
                void1.push(this.value);
            });
            var energy = void1.join();
            //alert(energy);alert("fgdfg");
            if (energy != '') {
                $("#assign_delivered").modal('show');
                $("#orderids4").val(energy);
            } else {
                alert("Please select minimum one order");
                //location.reload();
            }

        });
        $("#btn_assigndelivery").click(function () {
            var selectdp = $('#selectdp').val();
            var orderids = $("#orderids").val();
            var del_assigned_date = $("#del_assigned_date").val();
            //alert(selectdp);
            //alert(orderids);
            //alert(del_assigned_date);

            //var supp_chnz_status = $("#supp_chnz_status1").val();


            if (del_assigned_date != '') {
                var url = 'updateOrderDelivery.php?flag=load&selectdp=' + selectdp + '&del_assigned_date=' + del_assigned_date + '&orderids=' + orderids;//+'&supp_chnz_status='+supp_chnz_status
                //	alert(url);
                $.ajax({
                    url: url,
                    datatype: "JSON",
                    contentType: "application/json",

                    error: function (data) {
                        console.log("error:" + data)
                    },
                    success: function (data) {
                        console.log(data);
                        if (data > 0) {
                            alert("The order is assigned to Delivery Person. ");
                            location.reload();
                        } else {
                            alert("Not updated.");
                        }
                    }
                });
            } else {
                alert("Please select date");
                return false;
            }
        });

        $("#btn_delivered").click(function () {
            var orderids4 = $("#orderids4").val();
            var del_assigned_date = $("#del_assigned_date1").val();
            if (del_assigned_date != '') {
                var url = 'updateOrderDelivery.php?flag=delivered&del_assigned_date=' + del_assigned_date + '&orderids=' + orderids4;
                //alert(url);
                $.ajax({
                    url: url,
                    datatype: "JSON",
                    contentType: "application/json",

                    error: function (data) {
                        console.log("error:" + data)
                    },
                    success: function (data) {
                        console.log(data);
                        if (data > 0) {
                            alert("Orders marked as delivered");
                            location.reload();
                        } else {
                            alert("Not updated");
                        }
                    }
                });
            } else {
                alert("Please select date");
                return false;
            }
        });
        function fnShowStockist(id) {
            var url = "getStockistDropDown.php?cat_id=" + id.value;
            CallAJAX(url, "divStocklistDropdown");
            document.getElementById("divsalespersonDropdown").innerHTML = "<select name='dropdownSalesPerson' id='dropdownSalesPerson' class='form-control'><option value=''>-Select-</option></select>";
        }
        function fnShowSalesperson(id) {
            var url = "getSalesPersonDropDown.php?cat_id=" + id.value;
            CallAJAX(url, "divsalespersonDropdown");
        }

    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
</html>