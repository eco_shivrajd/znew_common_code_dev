<!-- BEGIN HEADER -->
<?php 
include "../includes/grid_header.php";
include "../includes/userManage.php";
include "../includes/targetManage.php";
$userObj 	= 	new userManager($con,$conmain);
$targetObj 	= 	new targetManager($con,$conmain);
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Reports"; $activeMenu = "LeaveReport";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<h3 class="page-title">Leaves</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li><i class="fa fa-home"></i>
					<a href="#">Leaves</a></li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
						
							<div class="caption">Leaves Listing</div>
							
							<!-- <? if($_SESSION[SESSION_PREFIX."user_type"]=="Admin")  { ?>
								<a href="leads-add.php" class="btn btn-sm btn-default pull-right mt5">Add Lead</a>
							<? } ?> -->
							
                            <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_2">
								<thead>
									<tr>
										<th>
											User Name
										</th>										
										<th>
											Start Date
										</th>
										<th>
											End Date
										</th>
										<th>
											Reason
										</th>
										<th>
											Description
										</th>										
										<th>
											Leave Status
										</th>							
									</tr>
								</thead>
							<tbody>
							<?php
					        $leaves = $targetObj->getLeaves();
							$temparr=array("0" => "Pending", "1" => "Approved","2"=>"Rejected");
					      
							foreach($leaves as $key=>$leave){
								$leave_id=$leave['leave_id'];
								$leave = $targetObj->getLeaveDetails($leave_id);
								  foreach($leave as $key1=>$leavedetails){
									  $name=$leavedetails['name'];									 
									  $startdate=$leavedetails['startdate'];
									  $enddate=$leavedetails['enddate'];
									  $reason=$leavedetails['reason'];
									  $description=$leavedetails['description'];
									  $leave_status=$leavedetails['leave_status'];
								  }							
                           ?>
							<tr class="odd gradeX">
								<td>
								 <?php echo $name;?>
								</td> 
								<td><?php echo $startdate;?></td>
								<td><?php echo $enddate;?></td> 
								<td><?php echo $reason;?></td>
								<td><?php echo $description;?></td> 
								<td><?php if($leave_status=='0'){?><a onclick="show_LeaveStatus('<?php echo $leave_id;?>')">
								<?php echo $temparr[$leave_status];?></a><?php }else{ echo $temparr[$leave_status]; } ?>
								</td> 																
						   </tr>	
                            <?php
							} 
							?>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
 <div class="modal fade" id="update_leavestatus" role="dialog" style="height:auto;">
            <div class="modal-dialog">    
                <!-- Modal content-->
                <div class="modal-content"  >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update Leave Status</h4>
                    </div>
                    <div class="modal-body" style="padding-bottom: 5px !important;">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="col-md-6" style="padding-top:5px">Select Status:<span class="mandatory">*</span></label>	
                                <div class="col-md-6" id="divdpDropdown">
                                    <select name="leave_status" id="leave_status" class="form-control">	
										<option value="1" selected>Approved</option>
										<option value="2">Reject</option>
                                    </select>
                                </div>
                            </div><!-- /.form-group -->
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <div class="col-md-offset-5 col-md-9">
                                    <input type="hidden" name="leave_id" id="leave_id">
                                    <button type="button" name="btn_reassigndelivery" id="btn_reassigndelivery" class="btn btn-primary" data-dismiss="modal">Update Leave</button>
                                </div>
                            </div>
                        </form>
                    </div>	
                </div>
            </div>
        </div>
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
 function show_LeaveStatus(leaveid) {
	$('#update_leavestatus').modal('show');
	$('#leave_id').val(leaveid); 
}
$('#btn_reassigndelivery').click(function(){	
	var leaveid = $('#leave_id').val(); 
	var leave_status = $('#leave_status').val(); 	
	var url = "update_spLeave.php";
	jQuery.ajax({
		url: url,
		method: 'POST',
		data: 'leave_id=' + leaveid + '&leave_status='+leave_status,
		async: false
	}).done(function (response) {		
		$('#update_leavestatus').modal('hide');
		alert("Leave status changes successfully!");
		location.reload();
	}).fail(function () { });
	return false;
});

</script>
</body>
<!-- END BODY -->
</html>