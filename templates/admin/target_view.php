<!-- BEGIN HEADER -->
<?php 
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include "../includes/grid_header.php";
include "../includes/userManage.php";
include "../includes/targetManage.php";
$targetObj 	= 	new targetManager($con,$conmain);
$userObj 	= 	new userManager($con,$conmain);

  
?>
<style>
#chartdiv {
	width		: 100%;
	height		: 500px;
	font-size	: 11px;
}			
</style>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Reports"; $activeMenu = "Targetsview";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<h3 class="page-title">Targets</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li><i class="fa fa-home"></i>
					<a href="#">Targets</a></li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
						
							<div class="caption">Target Listing</div>
							
							<!-- <? if($_SESSION[SESSION_PREFIX."user_type"]=="Admin")  { ?>
								<a href="leads-add.php" class="btn btn-sm btn-default pull-right mt5">Add Lead</a>
							<? } ?> -->
							
                            <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							<form class="form-horizontal" data-parsley-validate="" id="myForm" name="myForm" role="form" method="post" action="target_view.php" enctype="multipart/form-data">         
							<div class="form-group">
								<label class="col-md-3">Sales Person:</label>
								<div class="col-md-4" id="divsalespersonDropdown">
								<?php $user_result = $userObj->getAllSP('SalesPerson'); ?>	
									<select name="dropdownSalesPerson" id="dropdownSalesPerson" 
									 class="form-control">
										<option value="" disabled>-Select-</option>
										<?php if($user_result!=0){
										while($row_user = mysqli_fetch_assoc($user_result))
										{											
											?>									
										<option value="<?=$row_user['id'];?>"><?=$row_user['firstname'];?></option>
										<?php } } ?>
									</select>
								</div>
							</div><!-- /.form-group -->	
							<!--<div class="form-group">
								<label class="col-md-3">Report Type:</label>
								<div class="col-md-4">
									<input type="radio" name="targetType" id="targetType_daily" value="daily" checked onclick="fnChangetargetType('daily');"> Daily 
									&nbsp;&nbsp;
									<input type="radio" name="targetType" id="targetType_weekly" value="weekly" onclick="fnChangetargetType('weekly');"> Weekly 
									&nbsp;&nbsp;
									<input type="radio" name="targetType" id="targetType_monthly" value="monthly" onclick="fnChangetargetType('monthly');"> Monthly
									&nbsp;&nbsp;
									<input type="radio" name="targetType" id="targetType_spdate" value="spdate" onclick="fnChangetargetType('spdate');"> Specific Date
								</div>
							</div>

							<div class="form-group" id="divSpdate" style="display: none;">
								<label class="col-md-3">Select Specific date:</label>
								<div class="col-md-8">
								   <div class="col-md-2">
									<label for="inputEmail3" >From Date:</label>
								   </div>
									<div class="col-md-4">
										<div class="input-group  date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
											<input type="text" class="form-control" name="frmdate" id="frmdate" 
												value="<?php echo $frmdate;?>" readonly>
											<span class="input-group-btn">
												<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
										
									</div>
								</div>	
								<label class="col-md-3"></label>
								<div class="col-md-8" style="margin-top: 1%;">
									<div class="col-md-2">
									 <label for="inputEmail3">To Date:</label>
									</div>
								 
									<div class="col-md-4">
										<div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
										<input type="text" class="form-control" name="todate" id="todate" value="<?php echo $todate;?>" readonly>
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
										</div>
									</div>
								</div>
							</div>-->
							<div class="form-group">
								<div class="col-md-4 col-md-offset-3">									
									<button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>
									<!--<button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" 
									onclick="fnChangeReportType('daily');">Reset</button>-->
								</div>
							</div><!-- /.form-group --> 
						</form> 
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="clearfix"></div>
				<div id="chartdiv6"></div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<script src="../../templates/js/amcharts1.js"></script>
<!--<script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
	<script type="text/javascript" src="https://www.amcharts.com/lib/3/pie.js"></script>
-->
<script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script>

<script src="../../templates/js/pie.js"></script>	
<script src="../../templates/js/light.js"></script>		
<!--<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>-->
<script>

function ShowReport(){
		//alert("dsfsd");
		var param = '';
		var dropdownSalesPerson = $("#dropdownSalesPerson").val();
		
		param = param + 'dropdownSalesPerson='+dropdownSalesPerson;	
		
	  if(dropdownSalesPerson == '')
	  {
		  alert("Please select SalesPerson!");
		  return false;
	  }
	  else
	  {		
		$.ajax
		({
			type: "POST",
			url: "ajax_getTargets.php",
			data: param,
			success: function(msg)
			{
			  $("#chartdiv6").html(msg);
			}
		  });
	  }
}

</script>
		
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>