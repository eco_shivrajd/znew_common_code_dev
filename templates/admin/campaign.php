<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/reportManage.php";
$reportObj = new reportManage($con, $conmain);
?>
<!-- END HEADER -->
<?php echo $ischecked_add;?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
		$activeMainMenu = "ManageProducts"; 
		$activeMenu = "Campaign";
		include "../includes/sidebar.php";?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->			
			<h3 class="page-title">
			Campaigns
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Campaigns</a>
					</li>
				</ul>				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Campaigns Listing
							</div>	
						<?php if ($ischecked_add==1){ ?>					
							<a href="campaign-add.php" class="btn btn-sm btn-default pull-right mt5" 
							style="margin-left:5px;">Add Campaign</a>
						<?php } ?>
							<button title="Assign To Sales Persons" id="assign_sp_link"
							onclick="javascript: assign_sp(this)" 
							class="btn btn-sm btn-default pull-right mt5" style="margin-left:5px;">
							Assign Campaign </button>
						 
							<button title="Assigned Sales Persons Listing" id="assigned_sp_listing_link"
							onclick="javascript: assigned_sp_listing(this)" 
							class="btn btn-sm btn-default pull-right mt5" style="margin-left:5px;">
							Campaigns Assigned </button>
							
                              
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">							
							<table class="table table-striped table-bordered table-hover" id="sample_2dgsd">
							<thead>
							<tr>	
								<th id="select_th" data-orderable="false">				
									<input type="checkbox" name="select_all[]" id="select_all"  
									value="0" onchange="javascript:checktotalchecked(this)">
								</th>
								<th id="name_column">
									 Campaign Name
								</th>
								<th>
									 Campaign Type
								</th>
                                <th>
									 Start Date
								</th>
								<th>
									 End Date
								</th>
								<th>
									 Status
								</th>
							</tr>
							</thead>
							<tbody>
								<?php		
								$result_campaign_new=array();
								  $sql="SELECT id AS campaign_id, campaign_name,campaign_type, campaign_start_date,campaign_end_date, status 
								  FROM tbl_campaign 
								  WHERE deleted = 0 ORDER BY campaign_name ASC";//c.status = 0  AND 
								$result_campaign = mysqli_query($con,$sql);
								$campaign_count=0;
								 $today = date('d-m-Y',time()); 
								 $time_today = strtotime($today);
								//echo $today=date('d-m-Y');
								while($row = mysqli_fetch_array($result_campaign))
								{									
										if($row['status'] == 0) $row['status'] ='Active'; else $row['status'] ='Inactive';
										if($row['campaign_type'] == 'discount') $row['campaign_type'] = 'Price Discount';
										if($row['campaign_type'] == 'free_product') $row['campaign_type'] = 'Free Product';
										if($row['campaign_type'] == 'by_weight') $row['campaign_type'] = 'By Weight';
										
										$campaign_start_date=date('d-m-Y',strtotime($row['campaign_start_date']));
										$campaign_end_date=date('d-m-Y',strtotime($row['campaign_end_date']));
										$time_start = strtotime($campaign_start_date);
										$time_end = strtotime($campaign_end_date);
										
										if($row['campaign_type']=='Price Discount' && $row['status'] =='Active' && $time_start<=$time_today && $time_end>=$time_today ){
											$result_campaign_new[$campaign_count]['status']=$row['status'];
											$result_campaign_new[$campaign_count]['campaign_type']=$row['campaign_type'];
											$result_campaign_new[$campaign_count]['campaign_id']=$row['campaign_id'];
											$result_campaign_new[$campaign_count]['campaign_name']=$row['campaign_name'];
											$result_campaign_new[$campaign_count]['campaign_start_date']=$campaign_start_date;
											$result_campaign_new[$campaign_count]['campaign_end_date']=$campaign_end_date;											
											$campaign_count++;
										}	?>
										<tr class="odd gradeX">	
											<td>
												<?php if(($row['campaign_type']=='Free Product' || $row['campaign_type'] =='By Weight' || $row['status'] =='Inactive' || !($time_start <= $time_today && $time_today <= $time_end))){ 
												//|| ($campaign_start_date <= $today && $today <= $campaign_end_date)
												?>
												 <span style="color:red" title="You can not assign Discount Campaign for past & future date, Inactive Campaign & Free Product Campaign" class="glyphicon glyphicon-ban-circle"></span>												
												<?php } else{ ?>
												 <input type="checkbox" name="select_all[]" id="select_all" 
												 value="<?=$row['campaign_id'];?>" onchange="javascript:checktotalchecked(this)">
												<?php } ?>
											</td>
											<td>
											<?php if($ischecked_edit==1){ 
											echo '<a href="campaign-edit.php?id='.base64_encode($row['campaign_id']).'">'.fnStringToHTML($row['campaign_name']).'</a>';
											}else{ echo '' . fnStringToHTML($row['campaign_name']) . '';} ?>
											</td>
											<td><?php echo $row['campaign_type'];?></td>
											<td> <?php echo $campaign_start_date;?> </td>
											<td> <?php echo $campaign_end_date;?></td>
											<td><?php echo $row['status'];?>
											</td>
								<?php }	?>							
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
	<div class="modal fade" id="assignSPModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Assign Sales Person To</h4>	   
                </div>		
                <form role="form" class="form-horizontal" 
					onsubmit="return false;" 
					action="campaign.php" data-parsley-validate="" 
					name="assign_sp_form" 
					id="assign_sp_form">
                    <div class="modal-body" >	  
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-md-4">Select Sales Person:</label>
                            <div class="col-md-6" id="show_dcp_dropdown" >
                               <select name="sp_ids" id="sp_ids" class="js-example-basic-multiple form-control" multiple
                                    data-parsley-trigger="change"
                                    data-parsley-required="#true" 				
                                    data-parsley-required-message="Please select Sales Person" style="width: 350px;overflow-x: auto;">
                                   <option value='0' disabled>-Select-</option>                                   
                                <?php
                                    $sqlsp = "SELECT tu.id,tu.firstname 
                                        FROM tbl_user tu 										
                                        where user_role='SalesPerson' 
										AND  external_id='".$user_id."' 
										AND  isdeleted!='1' 
										order by firstname";
                                          // . "group by tds.sp_ids ";
                                           //. "having ((sum(tds.stock_qnty) <=0) or(sum(tds.stock_qnty)  is null))";
                                    $resultsp = mysqli_query($con, $sqlsp);
                                   
									$sqlcampaigns = "SELECT  tsca.sp_id,tsca.campaign_id,tc.status,tc.deleted
                                        FROM tbl_sp_campaign_assign tsca
										left join tbl_campaign tc on  tsca.campaign_id=tc.id 
										where tc.status=0 and tc.deleted=0
                                        group by tsca.sp_id ";
                                    $result = mysqli_query($con, $sqlcampaigns);
                                    while ($row2 = mysqli_fetch_array($result)){
                                        $sp_id_assigneds[]=$row2['sp_id'];            
                                    } 
                                            
                                    while ($row_sp = mysqli_fetch_array($resultsp)) {
                                        $userid=$row_sp['id'];
                                        if (in_array($userid,$sp_id_assigneds)){ ?>
                                            <option value='<?php echo $row_sp['id'];?>' disabled><?php echo fnStringToHTML($row_sp['firstname']);?></option>
                                <?php   }else{ ?>
                                           <option value='<?php echo $row_sp['id'];?>'><?php echo fnStringToHTML($row_sp['firstname']);?></option>
                                     <?php  }
                                       } ?>
                                </select> 
                            </div>
                        </div><!-- /.form-group --> 
                        
                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-3">					
                                <button type="submit"  name="btnsubmit"  class="btn btn-primary">Submit</button>
                                <a href="campaign.php" class="btn btn-primary">Cancel</a>
                            </div>
                        </div><!-- /.form-group -->
						<input type="hidden" name="sp_ids_selected" id="sp_ids_selected" value="0"/>
                        <input type="hidden" name="input_campaign_ids" id="input_campaign_ids" value="0"/>
                        <input type="hidden" name="action" id="action" value="assign_campaigns"/>
                    </div><!-- /.form-group --> 				
            </div>
            </form>	   	  
        </div>
    </div>
	
	<!--start-->
	<div class="modal fade" id="assignedSPModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">
                    Assigned Campaigns Listing:</h4>	   
                </div>		
                <form role="form" class="form-horizontal" 
					onsubmit="return false;" 
					action="campaign.php" data-parsley-validate="" 
					name="reassign_sp_form" 
					id="reassign_sp_form">
                    <div class="modal-body" style="max-height: 500px;overflow-y: auto;">	  
                        <div class="clearfix"></div>
                        <div id="campaign_assigned_to_sp_table">
						<?php 
						 $campaigndetails = $reportObj->get_assigned_campaign_to_sp($user_id);	
							//$result_campaign
						?>
						<table class="table table-striped table-bordered table-hover" id="sample_sp_listing">
							<thead>
							<tr>
								<th>
									 Sales Person
								</th>
								<th>
									  Campaign Names
								</th>
                                <th style="width: 75px;">
									 Action
								</th>
							</tr>
							</thead>
							<tbody>	
								<?php 
								//echo "1<pre>";print_r($campaigndetails);
								$count_tr=0;
								foreach($campaigndetails as $sps=>$campaigns )
								{ 
									$campaign_ids_status_active = explode(',', $campaigns['campaign_ids']);
									$campaign_status = explode(',', $campaigns['campaign_status']);
									$campaign_names_status = explode(',', $campaigns['campaign_names']);
									//$campaign_status_sum= array_sum($campaign_status); // 150	
									$campaign_names='';$count_active=0;
									foreach($campaign_status as $key_flag=>$camp_status_flag){										
										if($camp_status_flag==0){$count_active++;
											$campaign_names .="".$campaign_names_status[$key_flag]."<br/>";
										}
									}
									//$campaign_names = str_replace(',', '<br />', $campaigns['campaign_names']);
									//exit(); 
								if(!empty($campaigns['campaign_names'])&& $count_active > 0)
								{
									$count_tr++;
									$myArray_campaign_ids[$campaigns['id']] = json_encode(explode(',', $campaigns['campaign_ids']));
									$myArray_campaign_names[$campaigns['id']] = json_encode(explode(',', $campaigns['campaign_names']));
									$myArray_sp_ids[$campaigns['id']] = json_encode(explode(',', $campaigns['id']));
								?>
								<tr class="odd gradeX">	
									<td><?php echo $campaigns['firstname'];?></td>
									<td ><?php echo $campaign_names;?> <span id="assigned_campaigns<?=$campaigns['id'];?>"></span ></td>
									<td>
									<a title="Edit" onclick="javascript: editAssignedCampaign(<?=$campaigns['id'];?>)"  
									class="btn btn-xs btn-danger">
								 <span class="glyphicon glyphicon-edit"></span>								  
								  </a>
								  
									<a title="Delete" 
									onclick="javascript: deleteAssignedCampaign(<?=$campaigns['id'];?>)"  
									class="btn btn-xs btn-danger">
								  <span class="glyphicon glyphicon-trash"></span>								  
								  </a>
								 
								  </td>
								</tr>	
								<?php } } if($count_tr==0){ ?>
									<tr class="odd gradeX">	
										<td colspan="3" align="center">No data available in table.</td>										
									</tr>
								<?php } ?>
							</tbody>
							</table>
						</div>
                    </div><!-- /.form-group --> 	
					</form>	 
            </div>
        </div>
    </div>
	<!--end-->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>	
	$(document).ready(function() {	
		$('.js-example-basic-multiple').select2();
		var table = $('#sample_2dgsd').dataTable({"bPaginate": false,destroy: true,
		"columnDefs": [
			  { "targets": [0], "orderable": false }
		  ],
		  "order": [[ 1, 'asc' ]]
		  });
		$('input[type=search]').on("input", function() {
			table.fnFilter('');
		});	
		$("#select_th").removeAttr("class");		
	} );
	
	function checktotalchecked(obj) {
		$("#select_th").removeAttr("class");
		if ($(obj).is(':checked') == true)
		{			
			var checkbox_count = ($('input:checkbox').length) - 1;
			var check_count = ($('input:checkbox:checked').length);
			if (obj.value == 0) {				
				$('input:checkbox').attr('checked', true);
			} else if (checkbox_count == check_count)				
				$('input:checkbox').attr('checked', true);			
		} else {
			$('#select_all').attr('checked', false);
			if (obj.value == 0)
				$('input:checkbox').attr('checked', false);
		}	
		var numberOfChecked = $('input:checkbox:checked').length;				
		if(numberOfChecked>0){ 				
			$("#assigned_sp_listing_link").attr("disabled", true);					
		}else{				
			$("#assigned_sp_listing_link").attr("disabled", false);		
		}
	}
	
	function assigned_sp_listing() {
		var rowCount = $('#sample_2dgsd tr').length;
		console.log(rowCount);
		if (rowCount > 0) {
			$('#assignedSPModal').modal('show');
		} else {
			alert("Campaign not assigned to sales person.");
		}
    }
    function assign_sp() {
		var void1 = [];
		$('input[name="select_all[]"]:checked').each(function () {
			if(this.value!='' && this.value!=0){void1.push(this.value);}
		});
		var campaign_ids = void1.join();
		if (campaign_ids != '') {
			$('#assignSPModal').modal('show');
			$('#input_campaign_ids').val(campaign_ids); 
		} else {
			alert("Please select minimum one campaign.");
		}
    }
    $('form#assign_sp_form').one("submit",function () {		
		var sp_ids=$('#sp_ids').val();
		$('#sp_ids_selected').val(sp_ids);
         var formData = new FormData($(this)[0]);
        if(sp_ids !== null && sp_ids !== '' && sp_ids != 0) {
            $.ajax({
                url: "assign_campaign.php",
                type: 'POST',
				data: formData,
                success: function (data) {
                     if (data > 0) {
                        alert('Campaign assigned to Sales Person successfully.');
                        $('#assignSPModal').modal('hide');
                        document.forms.assign_sp_form.reset();
                        window.location.reload(true);
                    }  else {
                        alert('Unable to assign to Sales Person.');
                        return false;
                    } 
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }else{
			alert('Please Select Sales Person.');               
			return false;
        } 
    });
</script>
<script>
	function deleteAssignedCampaign(sp_id) {
		if (confirm('Are you sure you want to unassigned campaign(s)?')) {
			CallAJAX('ajax_product_manage.php?action=delete_assigned_campaign&sp_id=' + sp_id );
		}
	}
	
	function editAssignedCampaign(sp_id) {
		//camp_names_
		var camp_ids=<?= json_encode($myArray_campaign_ids); ?>;
		var camp_names=<?= json_encode($myArray_campaign_names); ?>;
		var result_campaign_new =<?= json_encode($result_campaign_new); ?>;
		var myArray_sp_ids=<?= json_encode($myArray_sp_ids); ?>;
		var option_string="";
		console.log(typeof(camp_ids[sp_id]));
		console.log(camp_ids[sp_id]);
		var array_test_test = JSON.parse(camp_ids[sp_id]);
		Object.keys(result_campaign_new).forEach(function(key) {			
			 if(array_test_test.indexOf(result_campaign_new[key]['campaign_id'])!='-1'){//camp_ids[sp_id]
				option_string +="<option value='"+result_campaign_new[key]['campaign_id']+"' selected>"+result_campaign_new[key]['campaign_name']+"</option>";
			}else{
				option_string +="<option value='"+result_campaign_new[key]['campaign_id']+"'>"+result_campaign_new[key]['campaign_name']+"</option>";
			}	
		});	
		 Object.keys(myArray_sp_ids).forEach(function(key) {
			  if(sp_id.toString()==key){
				  $('#assigned_campaigns'+sp_id+'').show();	
				$('#assigned_campaigns'+sp_id+'').html("<div class='clearfix'></div><br><div class='form-group'>"+
			"<div class='col-md-12'>"+
			"<select name='camp_names_"+sp_id+"' id='camp_names_"+sp_id+"'  multiple style='width: inherit;'>"+     
			" <option value=''>--Select Campaign--</option>"+option_string+
			"</select></div></div><div class='clearfix'></div><div class='form-group'>"+
			"<div class='col-md-4'><button type='button' name='buttonasave' class='btn btn-primary' onclick='javascript: saveAssignedCampaign("+sp_id+")'>Assign Campaign</button></div>"+
			"</div> ");	//alert("if");
			}else{
				$('#assigned_campaigns'+key+'').hide();	//alert("else");
			}	 
		});	 
		console.log(result_campaign_new.length);
	}
	function saveAssignedCampaign(sp_id){
		
			var items = [];
			$('#assigned_campaigns'+sp_id+' option:selected').each(function(){ 
				if($(this).val()!=''){items.push($(this).val());}
			});
			console.log(items);
			var action="reassign_campaigns";
			//CallAJAX('ajax_product_manage.php?action=delete_assigned_campaign_and_assign_new&sp_id=' + sp_id +'&campaign_ids='+items);
			 if(items.length > 0 ) {	
				if (confirm('Are you sure you want to reassigned campaign(s)?')) {
					$.ajax({
						url: "reassign_campaign.php?action="+action+"&sp_id="+sp_id+"&campaign_ids="+items+"",				
						success: function (data) {
							  if (data > 0) {
								alert('Campaign reassigned to Sales Person successfully.');
								$('#assignSPModal').modal('hide');
								//document.forms.assign_sp_form.reset();
								window.location.reload(true);
							}  else {
								alert('Unable to assign to Sales Person.');
								return false;
							}  
						},
						cache: false,
						contentType: false,
						processData: false
					});
				}
			}else{
				alert('Please select at least one Campaign');               
				return false;
			} 
		
	}
//delete_assigned_campaign
	function CallAJAX(url) {
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				alert('Assigned campaign unassigned successfully.');
				location.reload();
			}
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}
</script>
</body>
<!-- END BODY -->
</html>