<?php
include "../includes/grid_header.php";
include "../includes/commonManage.php";
$commonObj = new commonManage($con, $conmain);
$commonObjctype = $commonObj->log_get_commonclienttype($con, $conmain);
$user_type = $_SESSION[SESSION_PREFIX . 'user_type'];
$user_id = $_SESSION[SESSION_PREFIX . 'user_id'];
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
         $activeMainMenu = "Stock";
         $activeMenu = "stock_dashboard";
        include "../includes/sidebar.php";
        $commonObj     =   new commonManage($con,$conmain);
        $row_url=$commonObj->getPageIDforUrlAdd($php_page_name);
        $page_id_url = $row_url['page_id'];
        $row_url_add=$commonObj->getURLforAdd($profile_id,$page_id_url);
        $ischecked_add_url = $row_url_add['ischecked_add'];
        if ($ischecked_add_url == 0 && $ischecked_add_url!='') 
        {
        session_set_cookie_params(0);
        session_start();
        session_destroy();
        echo '<script>location.href="../login.php";</script>';
        exit;
        }
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

                <!-- /.modal -->

                <h3 class="page-title">
                    Remove Stock
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                         <li>
                            <i class="fa fa-home"></i>
                            <a href="stock_dashboard.php">Stock Dashboard</a>
                             <i class="fa fa-angle-right"></i>
                        </li>
                            <li>
                            <a href="#">Remove Stock</a>
                            </li>
                    </ul>

                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">

                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">Review Updates </div>	
                                <a id="btnEmpty" class="btn btn-sm btn-default pull-right mt5" onClick="cartActionEmpty();">Empty List</a>
                                <div class="clearfix"></div>
                            </div>						
                            <div class="portlet-body">	
                                <div id="empty_cart_div_id" align="center">Remove Stock cart is empty.</div>
                                    <table class="table table-striped table-bordered table-hover" id="myTable" style="display:none">
                                        <thead>
                                            <tr>					
                                                <th>
                                                    Category
                                                </th>
                                                <th>
                                                    Product Name
                                                </th>
                                                <th>
                                                    Product HSN
                                                </th>
                                                <th>
                                                    Product Price
                                                </th>
                                                <th>
                                                    Product Variant
                                                </th>
                                                <th>
                                                    Total Availble Stock
                                                </th>
                                                <th>
                                                    Remove From Stock
                                                </th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody >

                                        </tbody>
                                    </table>
                                 <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" >
                                     <div class="form-group" id="place_order_div" style="display:none">
                                        <div class="col-md-4 col-md-offset-5">
                                            <button type="button" name="place_order_btnsubmit" id="place_order_btnsubmit" class="btn btn-primary" onclick="placeStock();">Remove from Stock</button>
                                        </div>
                                    </div><!-- /.form-group -->
                                 </form>
                            </div>
                            <!-- END PAGE CONTENT-->
                        </div>
                        <div class="clearfix"></div>   


                        <div class="portlet box blue-steel">
                            <div class="portlet-title">

                                <div class="caption">Product Listing</div>
                                <a id="btnEmpty" class="btn btn-sm btn-default pull-right mt5" onclick="stockAdd();">Remove</a>
                                <div class="clearfix"></div>

                            </div>

                            <div class="portlet-body">
                                <form id="frmCart">
                                   <table class="table table-striped table-bordered table-hover" id="sample_7">
                                        <thead>
                                            <tr>					
                                                <th>
                                                    Category
                                                </th>
                                                <th>
                                                    Product Name
                                                </th>
                                                <th>
                                                    Product HSN
                                                </th>
                                                <th>
                                                    Product Price
                                                </th>
                                                <th>
                                                    Product Variant
                                                </th>
                                                <th>
                                                    Total Available Stock
                                                </th>
                                                <th>
                                                    Remove From Stock
                                                </th>
                                               
                                            </tr>
                                        </thead>
    <tbody>
        <?php 
          $sql1 = "SELECT parent_ids FROM tbl_user where id='$user_id'";
          $result1 = mysqli_query($con, $sql1);
          $row1 = mysqli_fetch_assoc($result1);
          $parent_ids = $row1['parent_ids'];
         $sql = "SELECT a.categorynm,b.productname,b.id,pv.producthsn,pv.price as price,pv.variant_1,pv.variant_2,pv.variant1_unit_id,pv.variant2_unit_id,pv.id as pvid, "
                . " pv.productimage,b.added_by_userid,b.added_by_usertype, a.id AS cat_id  "
                . " FROM tbl_category a,tbl_product b "
                . " left join tbl_product_variant pv on b.id = pv.productid "
                . " where a.id=b.catid AND b.isdeleted != 1 "
                . " and (b.added_by_userid='".$user_id."' OR b.added_by_userid IN (".$parent_ids."))";//((b.added_by_userid=0) OR (b.added_by_userid=1) OR (b.added_by_userid='$user_id'))
        $result = mysqli_query($con, $sql);
        $row_count = mysqli_num_rows($result);
        while ($row = mysqli_fetch_array($result)) 
        { 
            $prodcode = $row['id'] . '_' . $row['pvid'];
            ?>

            <tr class="odd gradeX" id="<?php echo $prodcode; ?>">
                <td id="num1<?php echo $prodcode; ?>">
                    <?php echo fnStringToHTML($row['categorynm']); ?>
                </td>
                <td id="num2<?php echo $prodcode; ?>">
                    <?php echo fnStringToHTML($row['productname']); ?>
                </td>
                <td id="num3<?php echo $prodcode; ?>">
                    <?php echo fnStringToHTML($row['producthsn']); ?>
                </td>
                <td align="right" id="num4<?php echo $prodcode; ?>">
                    <?php echo fnStringToHTML($row['price']); ?>
                </td>										
                <td id="num5<?php echo $prodcode; ?>"> 
                     <?php
                    $sqlvarunit3 = "SELECT unitname FROM tbl_units  where id='" . $row['variant1_unit_id'] . "'";
                    $resultvarunit3 = mysqli_query($con, $sqlvarunit3);
                    while ($rowvarunit3 = mysqli_fetch_array($resultvarunit3)) 
                    {
                               $unitname3 = $rowvarunit3['unitname'];
                    }

                    $sqlvarunit4 = "SELECT unitname FROM tbl_units  where id='" . $row['variant2_unit_id'] . "'";
                    $resultvarunit4 = mysqli_query($con, $sqlvarunit4);
                    while ($rowvarunit4 = mysqli_fetch_array($resultvarunit4)) 
                    {
                               $unitname4 = $rowvarunit4['unitname'];
                    }
                    echo $row['variant_1']."-".$unitname3." , ".$row['variant_2']."-".$unitname4;
                    echo "<br>";
                      ?>

                </td>
                <td align="right" id="num6<?php echo $prodcode; ?>">
         <?php  
          $total_stock_in_quantity=0;
                   if ($user_role=='Admin') 
                   {
                            $sqlvarunit5 = "SELECT sum(product_quantity) as total_stock_in_quantity FROM tbl_stock_management  where product_varient_id='" . $row['pvid'] . "' AND added_by_userid='".$user_id."'";
                                $resultvarunit5= mysqli_query($con, $sqlvarunit5);
                                while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) 
                                {
                                $total_stock_in_quantity = $rowvarunit5['total_stock_in_quantity'];
                                }
                                if (isset($total_stock_in_quantity)) 
                                {
                                echo $total_stock_in_quantity;
                                }
                                else { echo '0'; }  
                   }
                   else
                   {

                             $sqlvarunit7 = "SELECT sum(product_quantity) as total_stock_in_quantity FROM tbl_stock_management  where product_varient_id='" . $row['pvid'] . "' AND added_by_userid='".$user_id."'";
                            $resultvarunit7= mysqli_query($con, $sqlvarunit7);
                            while ($rowvarunit7 = mysqli_fetch_array($resultvarunit7)) 
                            {
                            $total_stock_by_user = $rowvarunit7['total_stock_in_quantity'];
                            }
                           
                             $sqlvarunit5 = "SELECT o.id,sum(od.product_quantity) as total_stock_in_quantity FROM tbl_orders AS o LEFT JOIN tbl_order_details AS od ON od.order_id = o.id where o.order_from='".$user_id."' AND od.product_variant_id='" . $row['pvid'] . "' AND ( od.order_status =2 OR od.order_status =4) ";
                                $resultvarunit5= mysqli_query($con, $sqlvarunit5);
                                while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) 
                                {
                                $total_stock_by_parent = $rowvarunit5['total_stock_in_quantity'];
                                }

                                $total_stock_in_quantity = $total_stock_by_user + $total_stock_by_parent;
            }
                      ?>

                       <?php  
                   $total_stock_out_quantity=0;
                    $sqlvarunit5 = "SELECT o.id,sum(od.product_quantity) as total_stock_out_quantity FROM tbl_orders AS o LEFT JOIN tbl_order_details AS od ON od.order_id = o.id where o.order_to='".$user_id."' AND od.product_variant_id='" . $row['pvid'] . "' AND ( od.order_status =2 OR od.order_status =4)";
                    $resultvarunit5= mysqli_query($con, $sqlvarunit5);
                    while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) 
                    {
                        $total_stock_out_quantity_tochild = $rowvarunit5['total_stock_out_quantity'];
                    }

                    $sqlvarunit5 = "SELECT sum(product_quantity) as total_stock_out_quantity 
                    FROM tbl_customer_orders_new  where order_to='".$user_id."' AND product_variant_id='" . $row['pvid'] . "' AND ( order_status =2 OR order_status =4)";
                    $resultvarunit5= mysqli_query($con, $sqlvarunit5);
                    while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) 
                    {
                        $total_stock_out_quantity_tocust = $rowvarunit5['total_stock_out_quantity'];
                    }

                   $total_stock_out_quantity = $total_stock_out_quantity_tochild + $total_stock_out_quantity_tocust;
                      ?>    


                      <?php  
                      $disabled='';
                    if ($total_stock_in_quantity >= $total_stock_out_quantity) 
                    {
                        $total_available_stock = $total_stock_in_quantity-$total_stock_out_quantity;
                    }
                    else
                    {
                     $total_available_stock=0;
                    }
                    ?>
                    <?php
                    if (isset($total_available_stock) && $total_available_stock>0) 
                    {
                    ?>
                    <div style="color: green;"> 
                     <?php echo $total_available_stock; ?>
                     </div>
                    <?php
                      }
                    else 
                    {
                        $disabled = 'disabled';
                    ?>                     
                        <a href="#" style="color: red;"> 0 </a>                       
                   <?php }  
                      ?>

                      <input type="hidden" name="old_stock_qty" id="old_stock_qty_<?php echo $prodcode; ?>">									 
                </td>
                <td id="num7<?php echo $prodcode; ?>" align="right">
                    <?php
                       $x = $row['id'];
                       $y = $row['pvid'];

                    ?>
                   <input type="number" min="0" <?=$disabled;?>  name="prodqnty" id="qty_<?php echo $prodcode; ?>" value="0" size="2" style="width: 55px; margin: 5px 5px 5px 5px;text-align: right;" max="<?=$total_available_stock;?>"  onkeyup="this.value=minmax(this.value, 0, <?php echo(json_encode($total_available_stock)); ?>); myFunction('<?php echo $x ?>', '<?php echo $y ?>');">  

                     <!-- <input type="number" min="0"  name="prodqnty" id="qty_<?php echo $prodcode; ?>" value="0" size="2" style="width: 55px; margin: 5px 5px 5px 5px;">  -->
                </td>
            </tr>
        <?php } ?>							
    </tbody>
</table>
                                </form>
                            </div>
                        </div> 

                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <?php include "../includes/grid_footer.php" ?>
    <!-- END FOOTER -->
<script type="text/javascript">
 $( document ).ready(function() {  
		$('input[name="prodqnty"]').on("cut copy paste",function(e) {
		  e.preventDefault();
	   });
        $("#place_order_btnsubmit").on('click', function (event) {
           event.preventDefault();
           var el = $(this);
           el.prop('disabled', true);
           setTimeout(function(){el.prop('disabled', false); }, 3000);
     });
 
        var table = $('#sample_7').dataTable({destroy: true,stateSave: false,paging:   false});
             $('input[type=search]').on("input", function() {
                table.fnFilter('');
            });

    }); 

function myFunction(x,y) 
{
     var z =x+'_'+y;
   //  alert(z);
     cartActionEmptyItem(z);
}

function minmax(value, min, max) 
{
   // alert(value);
   // alert(min);
   // alert(max);
   if (value > max) 
   {
    alert('Please Enter minimum Or Equal value compared with existing stcok.');
    return false;
   }
   else
   {
    return value;
   } 
}
</script>
    <script>
        function placeStock(){           
            var orderarray=[];
            $('#myTable tbody tr').each(function () {  
                var void1 = [];
                var newenergy = this.id.replace('mytabletr', '');
                var tempstrip=$('#myqty_' + newenergy + '').html();                            
                void1.push(tempstrip.replace(/ +$/, "")); 
                void1.push(newenergy); 
                 orderarray.push(void1); 
            });
          
            if (orderarray.length > 0) {
                var url = 'stock_remove_ajax.php';               
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {data123 : orderarray},
                    error: function (data) {
                        console.log("error:" + data)
                    },
                    success: function (data) {
                        console.log(data);
                        if (data > 0) {
                            alert("Stock Removed Successfully");
                           // location.reload();
                            window.location.href = "stock_dashboard.php";
                        } else {
                            alert("Not updated");
                        }
                    }
                });
            } else {
                alert("Please add some product!");
                return false;
            }
        }
        function cartActionEmpty(){
            $("#myTable tbody").empty();
            $('#place_order_div').hide();
            $('#myTable').hide();
            $('#empty_cart_div_id').show();
        }
        function cartActionEmptyItem(trid){
            $("#mytabletr"+trid+"").remove();
            var rowCount = $('#myTable tbody tr').length;  
            if(rowCount==0){
                $('#place_order_div').hide();
                $('#myTable').hide();
                 $('#empty_cart_div_id').show();
            }   
        }

        function stockAdd() {
            var void1 = [];
            var void2 = [];
            $('input[name="prodqnty"]').each(function () {
                if (this.value > 0) {
                    void1.push(this.value);
                    void2.push(this.id);
                }
            });

             var void6 = [];
            var void7 = [];
            $('input[name="old_stock_qty"]').each(function () {
                if (this.value > 0) {
                    void6.push(this.value);
                    void7.push(this.id);
                }
            });

         

            var energy = void1.join();
            
            if (energy != '') 
            {
                   /* Object.keys(void6).forEach(function (key) 
                    {
                    var newenergy11 = void7[key].replace('qty_', '#');
                    var newenergy12 = newenergy.replace('#', '');
                    alert(newenergy11);
                    alert(newenergy12);
                    }*/


                   Object.keys(void1).forEach(function (key) {
                   var newenergy = void2[key].replace('qty_', '#');
                   var newenergy1 = newenergy.replace('#', '');
                   //alert(newenergy1);

                    if ($('#mytabletr' + newenergy1 + '').length) 
                    {
                        //alert('hi');
                        var sumvalue = +$('#myqty_' + newenergy1 + '').html() + +void1[key];
                        console.log(sumvalue);
                        $('#myqty_' + newenergy1 + '').html('' + sumvalue + '');
                    } else {
                      // alert('by');
                        $("#myTable").append("<tr class='odd gradeX' id='mytabletr" + newenergy1 + "'>\
                            <td>" + $('#num1' + newenergy1 + '').html() + "</td>\
                            <td>" + $('#num2' + newenergy1 + '').html() + "</td>\
                            <td>" + $('#num3' + newenergy1 + '').html() + "</td>\
                            <td>" + $('#num4' + newenergy1 + '').html() + "</td>\
                            <td>" + $('#num5' + newenergy1 + '').html() + "</td>\
                            <td>" + $('#num6' + newenergy1 + '').html() + "</td>\
                            <td ><button type='button' class='btn btn-primary' onClick='cartActionEmptyItem(\"" + newenergy1 + "\");'>Remove <span id='myqty_" + newenergy1 + "' class='badge'>"+$('#qty_' + newenergy1 + '').val()+"</span></button></td>\
                        </tr>");
                                
                    }

                });
                if ($('#myTable tr').length>0) {                    
                    $('#place_order_div').show();
                    $('#myTable').show();
                    $('#empty_cart_div_id').hide();
                }
                $('input[name="prodqnty"]').each(function () {
                    if (this.value > 0) {
                        $('input[name="prodqnty"]').val('0');
                    }
                });
            } else {
                alert("Please add some quantity!");
            }
        }
    </script>


</body>
<!-- END BODY -->
</html>