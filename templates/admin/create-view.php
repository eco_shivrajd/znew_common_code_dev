
CREATE VIEW `tbl_shop_view1`  AS  (select `sp`.`id` AS `id`,`sp`.`shop_added_by` AS `shop_added_by`,`sp`.`status` AS `status`,`sp`.`status_seen_log` AS `status_seen_log`,`u`.`firstname` AS `shop_added_by_name`,`u`.`parent_ids` AS `parent_ids`,`sp`.`name` AS `shop_name`,`sp`.`contact_person` AS `contact_person`,`sp`.`mobile` AS `mobile`,`c`.`name` AS `cityname`,`s`.`name` AS `statename`,`su`.`suburbnm` AS `suburbnm`,`sub`.`subarea_name` AS `subarea_name`,`sp`.`billing_name` AS `billing_name`,`sp`.`bank_name` AS `bank_name`,`sp`.`bank_acc_name` AS `bank_acc_name`,`sp`.`bank_acc_no` AS `bank_acc_no`,`sp`.`bank_b_name` AS `bank_b_name`,`sp`.`bank_ifsc` AS `bank_ifsc`,`sp`.`added_on` AS `added_on`,`sp`.`state` AS `state_id`,`sp`.`city` AS `city_id`,`sp`.`suburbid` AS `suburbid`,`sp`.`subarea_id` AS `subarea_id` 
,`sp`.`latitude` AS `latitude`,`sp`.`longitude` AS `longitude`
from (((((`tbl_shops` `sp` left join `tbl_user` `u` on((`u`.`id` = `sp`.`shop_added_by`))) left join `tbl_state` `s` on((`sp`.`state` = `s`.`id`))) left join `tbl_city` `c` on((`sp`.`city` = `c`.`id`))) left join `tbl_area` `su` on((`sp`.`suburbid` = `su`.`id`))) left join `tbl_subarea` `sub` on((`sp`.`subarea_id` = `sub`.`subarea_id`))) where (`sp`.`isdeleted` <> '1') order by `sp`.`id` desc) ;





CREATE VIEW `tbl_leave_view`  AS  (select 
`sp`.`id`, `sp`.`sp_id`, `sp`.`tdate`, `sp`.`presenty`, `sp`.`dayendtime`, `sp`.`todays_travelled_distance`, `sp`.`reason`, `sp`.`description`, `sp`.`leave_status`, `sp`.`leave_id`,`u`.`parent_ids`,`u`.`user_role`
from 
`tbl_sp_attendance` `sp` 
left join `tbl_user` `u` on `u`.`id` = `sp`.`sp_id` order by `sp`.`id` desc)





CREATE VIEW pcatbrandvariant AS
(select `b`.`id` AS `brand_id`,`c`.`id` AS `category_id`,`p`.`id` AS `product_id`,`u`.`unitname` AS `variant1_unit_name`,`u2`.`unitname` AS `variant2_unit_name`,`pv`.`id` AS `product_variant_id`,`b`.`name` AS `brand_name`,`c`.`categorynm` AS `category_name`,`c`.`categoryimage` AS `category_image`,`p`.`productname` AS `product_name`,`p`.`added_by_userid` AS `added_by_userid`,`p`.`added_by_usertype` AS `added_by_usertype`,`pv`.`productimage` AS `product_image`,`pv`.`price` AS `product_price`,`pv`.`variant_1` AS `product_variant1`,`pv`.`variant_2` AS `product_variant2`,`pv`.`variant_cnt` AS `variant_cnt`,`pv`.`producthsn` AS `product_hsn`,`pv`.`cgst` AS `product_cgst`,`pv`.`sgst` AS `product_sgst`,`pv`.`productbarcodeimage` AS `product_barcode_img`,`pv`.`carton_quantities` AS `carton_quantities` from 
(((`salzpoin_znew_uatkawreicecream`.`tbl_brand` `b` join `salzpoin_znew_uatkawreicecream`.`tbl_category` `c` on((`b`.`id` = `c`.`brandid`))) 
join `salzpoin_znew_uatkawreicecream`.`tbl_product` `p` on((`c`.`id` = `p`.`catid`))) 
join `salzpoin_znew_uatkawreicecream`.`tbl_product_variant` `pv` on((`p`.`id` = `pv`.`productid`))
join `salzpoin_znew_uatkawreicecream`.`tbl_units` `u` on((`pv`.`variant1_unit_id` = `u`.`id`))
join `salzpoin_znew_uatkawreicecream`.`tbl_units` `u2` on((`pv`.`variant2_unit_id` = `u2`.`id`))
)
 where ((`b`.`isdeleted` <> 1) and (`c`.`isdeleted` <> 1) and (`p`.`isdeleted` <> 1)))



// tbl_user view

SELECT u.id, u.external_id, u.firstname, u.username, u.pwd, u.user_type, u.address, c.name as cityname,u.city as city_id,u.state as state_id, s.name as statename, 
u.suburb_ids, su.suburbnm,u.subarea_ids, sub.subarea_name,
 u.mobile, u.email,u.stockist_id,u1.firstname as stockist_name,u.sstockist_id,u.stockist_id,u2.firstname as sstockist_name,u.office_phone_no,u.accname,u.accno,u.bank_name,u.accbrnm,u.accifsc,u.gst_number_sss,u.latitude,u.longitude,
  u.is_default_user,u.is_common_user, u.added_on 
			FROM tbl_user u 
			LEFT JOIN tbl_state s ON u.state = s.id 
			LEFT JOIN tbl_city c ON u.city = c.id 
			LEFT JOIN tbl_area su ON u.suburb_ids = su.id 
			LEFT JOIN tbl_subarea sub ON u.subarea_ids = sub.subarea_id
			LEFT JOIN tbl_user u1 ON u1.id = u.stockist_id
			LEFT JOIN tbl_user u2 ON u2.id = u.sstockist_id
			where u.isdeleted!='1' AND u.is_common_user!='1' order by u.id desc

// Common User view
SELECT u.id, u.common_user_id, u.external_id, u.firstname, u.username, u.pwd, u.user_type, u.address, c.name as cityname,u.city as city_id,u.state as state_id, s.name as statename, 
u.suburb_ids, su.suburbnm,u.subarea_ids, sub.subarea_name,
 u.mobile, u.email,u.stockist_id,u1.firstname as stockist_name,u.sstockist_id,u2.firstname as sstockist_name,u.office_phone_no,u.accname,u.accno,u.bank_name,u.accbrnm,u.accifsc,u.gst_number_sss,u.latitude,u.longitude,
  u.is_default_user,u.is_common_user, u.added_on 
			FROM tbl_user u 
			LEFT JOIN tbl_state s ON u.state = s.id 
			LEFT JOIN tbl_city c ON u.city = c.id 
			LEFT JOIN tbl_area su ON u.suburb_ids = su.id 
			LEFT JOIN tbl_subarea sub ON u.subarea_ids = sub.subarea_id
			LEFT JOIN tbl_user u1 ON u1.id = u.stockist_id
			LEFT JOIN tbl_user u2 ON u2.id = u.sstockist_id
			where u.isdeleted!='1' AND u.is_common_user='1' order by u.id desc


// tbl_Shop view

 SELECT  sp.id,sp.status,sp.status_seen_log,u.firstname as sstockist,u1.firstname as stockist,sp.name as shop_name,sp.contact_person,sp.mobile,c.name as cityname,s.name as statename,su.suburbnm,sub.subarea_name,sp.billing_name,sp.bank_name,sp.bank_acc_name,sp.bank_acc_no,sp.bank_b_name,sp.	bank_ifsc,sp.added_on 
			FROM tbl_shops sp 
			LEFT JOIN tbl_user u ON u.id = sp.sstockist_id
			LEFT JOIN tbl_user u1 ON u1.id = sp.stockist_id 
			LEFT JOIN tbl_state s ON sp.state = s.id 
			LEFT JOIN tbl_city c ON sp.city = c.id 
			LEFT JOIN tbl_area su ON sp.suburbid = su.id 
			LEFT JOIN tbl_subarea sub ON sp.subarea_id = sub.subarea_id
			where sp.isdeleted!='1' order by sp.id desc


			SELECT `id`, `common_user_id`, `external_id`, `firstname`, `username`, `pwd`, `user_type`, `subarea_ids`, `suburb_ids`, `city`, `state`, `address`, `mobile`, `email`, `stockist_id`, `sstockist_id`, `office_phone_no`, `accname`, `accno`, `bank_name`, `accbrnm`, `accifsc`, `gst_number_sss`, `reset_key`, `is_default_user`, `isdeleted`, `user_status`, `is_common_user`, `added_on` FROM `tbl_user` WHERE 1


CREATE VIEW `tbl_shop_view`  AS  (select `sp`.`id` AS `id`,`sp`.`shop_added_by` AS `shop_added_by`,`sp`.`status` AS `status`,`sp`.`shop_status`,`sp`.`status_seen_log` AS `status_seen_log`,`u`.`firstname` AS `shop_added_by_name`,`u`.`parent_ids` AS `parent_ids`,`sp`.`name` AS `shop_name`,`sp`.`contact_person` AS `contact_person`,`sp`.`mobile` AS `mobile`,`c`.`name` AS `cityname`,`s`.`name` AS `statename`,`su`.`suburbnm` AS `suburbnm`,`sub`.`subarea_name` AS `subarea_name`,`sp`.`billing_name` AS `billing_name`,`sp`.`bank_name` AS `bank_name`,`sp`.`bank_acc_name` AS `bank_acc_name`,`sp`.`bank_acc_no` AS `bank_acc_no`,`sp`.`bank_b_name` AS `bank_b_name`,`sp`.`bank_ifsc` AS `bank_ifsc`,`sp`.`added_on` AS `added_on`,`sp`.`state` AS `state_id`,`sp`.`city` AS `city_id`,`sp`.`suburbid` AS `suburbid`,`sp`.`subarea_id` AS `subarea_id`,`sp`.`latitude` AS `latitude`,`sp`.`longitude` AS `longitude` from (((((`tbl_shops` `sp` left join `tbl_user` `u` on((`u`.`id` = `sp`.`shop_added_by`))) left join `tbl_state` `s` on((`sp`.`state` = `s`.`id`))) left join `tbl_city` `c` on((`sp`.`city` = `c`.`id`))) left join `tbl_area` `su` on((`sp`.`suburbid` = `su`.`id`))) left join `tbl_subarea` `sub` on((`sp`.`subarea_id` = `sub`.`subarea_id`))) where (`sp`.`isdeleted` <> '1') order by `sp`.`id` desc) ;






			CREATE ALGORITHM=UNDEFINED DEFINER=`salzpoint`@`localhost` SQL SECURITY DEFINER VIEW `pcatbrandvariant`  AS  (select `b`.`id` AS `brand_id`,`c`.`id` AS `category_id`,`p`.`id` AS `product_id`,`u`.`unitname` AS `variant1_unit_name`,`u2`.`unitname` AS `variant2_unit_name`,`pv`.`id` AS `product_variant_id`,`b`.`name` AS `brand_name`,`c`.`categorynm` AS `category_name`,`c`.`categoryimage` AS `category_image`,`p`.`productname` AS `product_name`,`p`.`added_by_userid` AS `added_by_userid`,`p`.`added_by_usertype` AS `added_by_usertype`,`pv`.`productimage` AS `product_image`,`pv`.`price` AS `product_price`,`pv`.`variant_1` AS `product_variant1`,`pv`.`variant_2` AS `product_variant2`,`pv`.`variant_cnt` AS `variant_cnt`,`pv`.`producthsn` AS `product_hsn`,`pv`.`cgst` AS `product_cgst`,`pv`.`sgst` AS `product_sgst`,`pv`.`productbarcodeimage` AS `product_barcode_img`,`pv`.`carton_quantities` AS `carton_quantities` from (((((`tbl_brand` `b` join `tbl_category` `c` on((`b`.`id` = `c`.`brandid`))) join `tbl_product` `p` on((`c`.`id` = `p`.`catid`))) join `tbl_product_variant` `pv` on((`p`.`id` = `pv`.`productid`))) join `tbl_units` `u` on((`pv`.`variant1_unit_id` = `u`.`id`))) join `tbl_units` `u2` on((`pv`.`variant2_unit_id` = `u2`.`id`))) where ((`b`.`isdeleted` <> 1) and (`c`.`isdeleted` <> 1) and (`p`.`isdeleted` <> 1))) ;
















(select `b`.`id` AS `brand_id`,`c`.`id` AS `category_id`,`p`.`id` AS `product_id`,`pv`.`id` AS `product_variant_id`,`b`.`name` AS `brand_name`,`c`.`categorynm` AS `category_name`,`c`.`categoryimage` AS `category_image`,`p`.`productname` AS `product_name`,`p`.`added_by_userid` AS `added_by_userid`,`p`.`added_by_usertype` AS `added_by_usertype`,`pv`.`productimage` AS `product_image`,`pv`.`price` AS `product_price`,`pv`.`variant_1` AS `product_variant1`,`pv`.`variant_2` AS `product_variant2`,`pv`.`variant_cnt` AS `variant_cnt`,`pv`.`producthsn` AS `product_hsn`,`pv`.`cgst` AS `product_cgst`,`pv`.`sgst` AS `product_sgst`,`pv`.`productbarcodeimage` AS `product_barcode_img`,`pv`.`carton_quantities` AS `carton_quantities` from (((`salzpoin_znew_uatkawreicecream`.`tbl_brand` `b` join `salzpoin_znew_uatkawreicecream`.`tbl_category` `c` on((`b`.`id` = `c`.`brandid`))) join `salzpoin_znew_uatkawreicecream`.`tbl_product` `p` on((`c`.`id` = `p`.`catid`))) join `salzpoin_znew_uatkawreicecream`.`tbl_product_variant` `pv` on((`p`.`id` = `pv`.`productid`))) where ((`b`.`isdeleted` <> 1) and (`c`.`isdeleted` <> 1) and (`p`.`isdeleted` <> 1)))




SELECT u.id, u.external_id, u.firstname, u.username, u.pwd, 
u.user_type, u.address, c.name as cityname,u.city as city_id,u.state as state_id, s.name as statename, 
u.suburb_ids, su.suburbnm,u.subarea_ids, (select  group_concat(`sub1`.`subarea_name` SEPARATOR ',') from tbl_subarea sub1 where `sub1`.`subarea_id` in (1,2)) as subarea_name, 

(SELECT  u1.id,
        GROUP_CONCAT(sub2.subarea_name ORDER BY sub2.subarea_id) DepartmentName
FROM    tbl_user u1
        INNER JOIN tbl_subarea sub2
            ON FIND_IN_SET(sub2.subarea_id, u1.subarea_ids) > 0
GROUP   BY u1.id),

u.mobile, u.email,u.stockist_id,u.sstockist_id,u.office_phone_no,u.accname,
u.accno,u.bank_name,u.accbrnm,u.accifsc,u.gst_number_sss,
u.is_default_user,u.is_common_user, u.added_on 
FROM tbl_user u 
LEFT JOIN tbl_state s ON u.state = s.id 
LEFT JOIN tbl_city c ON u.city = c.id 
LEFT JOIN tbl_area su ON u.suburb_ids = su.id 
LEFT JOIN tbl_subarea sub ON u.subarea_ids = sub.subarea_id
where u.isdeleted!='1' order by u.id desc




CREATE ALGORITHM=UNDEFINED DEFINER=`salzpoint`@`localhost` SQL SECURITY DEFINER VIEW `tbl_user_view`  AS  select `u`.`id` AS `id`,`u`.`external_id` AS `external_id`,`u`.`firstname` AS `firstname`,`u`.`username` AS `username`,`u`.`pwd` AS `pwd`,`u`.`user_type` AS `user_type`,`u`.`address` AS `address`,`c`.`name` AS `cityname`,`u`.`city` AS `city_id`,`u`.`state` AS `state_id`,`s`.`name` AS `statename`,`u`.`suburb_ids` AS `suburb_ids`,`su`.`suburbnm` AS `suburbnm`,`u`.`subarea_ids` AS `subarea_ids`,`sub`.`subarea_name` AS `subarea_name`,`u`.`mobile` AS `mobile`,`u`.`email` AS `email`,`u`.`stockist_id` AS `stockist_id`,`u1`.`firstname` AS `stockist_name`,`u`.`sstockist_id` AS `sstockist_id`,`u2`.`firstname` AS `sstockist_name`,`u`.`office_phone_no` AS `office_phone_no`,`u`.`accname` AS `accname`,`u`.`accno` AS `accno`,`u`.`bank_name` AS `bank_name`,`u`.`accbrnm` AS `accbrnm`,`u`.`accifsc` AS `accifsc`,`u`.`gst_number_sss` AS `gst_number_sss`,`u`.`latitude` AS `latitude`,`u`.`longitude` AS `longitude`,`u`.`is_default_user` AS `is_default_user`,`u`.`is_common_user` AS `is_common_user`,`u`.`added_on` AS `added_on` from ((((((`tbl_user` `u` left join `tbl_state` `s` on((`u`.`state` = `s`.`id`))) left join `tbl_city` `c` on((`u`.`city` = `c`.`id`))) left join `tbl_area` `su` on((`u`.`suburb_ids` = `su`.`id`))) left join `tbl_subarea` `sub` on((`u`.`subarea_ids` = `sub`.`subarea_id`))) left join `tbl_user` `u1` on((`u1`.`id` = `u`.`stockist_id`))) left join `tbl_user` `u2` on((`u2`.`id` = `u`.`sstockist_id`))) where ((`u`.`isdeleted` <> '1') and (`u`.`is_common_user` <> '1')) order by `u`.`id` desc ;




CREATE  VIEW `tbl_user_view`  AS  
select `u`.`id` AS `id`,`u`.`external_id` AS `external_id`,`u`.`user_role` AS `user_role`,`u`.`reset_key` AS `reset_key`,`u`.`user_status` AS `user_status`,
`u`.`firstname` AS `firstname`,`u`.`username` AS `username`,`u`.`pwd` AS `pwd`,`u`.`user_type` AS `user_type`,`u`.`depth` AS `depth`,`u`.`address` AS `address`,
`c`.`name` AS `cityname`,`u`.`city` AS `city_id`,`u`.`state` AS `state_id`,`s`.`name` AS `statename`,`u`.`suburb_ids` AS `suburb_ids`,
(select group_concat(`su`.`suburbnm` order by `su`.`id` ASC separator ',') from 
(`tbl_user` `innerarea` join `tbl_area` `su` on((find_in_set(`su`.`id`,`innerarea`.`suburb_ids`) > 0))) 
where (`innerarea`.`id` = `u`.`id`) group by `innerarea`.`id`) AS `suburbnm`,`u`.`subarea_ids` AS `subarea_ids`,
(select group_concat(`sub`.`subarea_name` order by `sub`.`subarea_id` ASC separator ',') 
from (`tbl_user` `inneru` join `tbl_subarea` `sub` on((find_in_set(`sub`.`subarea_id`,`inneru`.`subarea_ids`) > 0)))
where (`inneru`.`id` = `u`.`id`) group by `inneru`.`id`) AS `subarea_name`,`u`.`mobile` AS `mobile`,`u`.`email` AS `email`,`u`.`parent_ids` AS `parent_ids`,
(select group_concat(`inneru1`.`firstname` order by `inneru1`.`id` ASC separator ',') from (`tbl_user` `inneru2` join `tbl_user` `inneru1` 
on((find_in_set(`inneru1`.`id`,`inneru2`.`parent_ids`) > 0))) where (`inneru2`.`id` = `u`.`id`) group by `inneru2`.`id`) AS `parent_names`,
(select group_concat(`inner_usertype`.`user_type` order by `inner_usertype`.`id` ASC separator ',') 
from (`tbl_user` `inneru123` join `tbl_user` `inner_usertype` on((find_in_set(`inner_usertype`.`id`,`inneru123`.`parent_ids`) > 0))) 
where (`inneru123`.`id` = `u`.`id`) group by `inneru123`.`id`) AS 
`parent_usertypes`,`u`.`office_phone_no` AS `office_phone_no`,`u`.`accname` AS `accname`,`u`.`accno` AS `accno`,`u`.`bank_name` AS `bank_name`,
`u`.`accbrnm` AS `accbrnm`,`u`.`accifsc` AS `accifsc`,`u`.`gst_number_sss` AS `gst_number_sss`,`u`.`latitude` AS `latitude`,`u`.`longitude` AS 
`longitude`,`u`.`marginType` AS `marginType`,`u`.`userid_margin` AS `userid_margin`,`u`.`is_default_user` AS `is_default_user`,`u`.`is_common_user`
 AS `is_common_user`,`u`.`added_on` AS `added_on` from ((`tbl_user` `u` left join `tbl_state` `s` on((`u`.`state` = `s`.`id`))) 
 left join `tbl_city` `c` on((`u`.`city` = `c`.`id`))) where ((`u`.`isdeleted` <> '1') and (`u`.`is_common_user` <> '1')) group by `u`.`id` order by `u`.`id` desc ;



 CREATE  VIEW `tbl_user_view`  AS  
select `u`.`id` AS `id`,`u`.`external_id` AS `external_id`,`u`.`user_role` AS `user_role`,`u`.`reset_key` AS `reset_key`,`u`.`user_status` AS `user_status`,
`u`.`firstname` AS `firstname`,`u`.`username` AS `username`,`u`.`pwd` AS `pwd`,`u`.`user_type` AS `user_type`,`u`.`depth` AS `depth`,`u`.`address` AS `address`,
`c`.`name` AS `cityname`,`u`.`city` AS `city_id`,`u`.`state` AS `state_id`,`s`.`name` AS `statename`,`u`.`suburb_ids` AS `suburb_ids`,
(select group_concat(`su`.`suburbnm` order by `su`.`id` ASC separator ',') from 
(`tbl_user` `innerarea` join `tbl_area` `su` on((find_in_set(`su`.`id`,`innerarea`.`suburb_ids`) > 0))) 
where (`innerarea`.`id` = `u`.`id`) group by `innerarea`.`id`) AS `suburbnm`,`u`.`subarea_ids` AS `subarea_ids`,
(select group_concat(`sub`.`subarea_name` order by `sub`.`subarea_id` ASC separator ',') 
from (`tbl_user` `inneru` join `tbl_subarea` `sub` on((find_in_set(`sub`.`subarea_id`,`inneru`.`subarea_ids`) > 0)))
where (`inneru`.`id` = `u`.`id`) group by `inneru`.`id`) AS `subarea_name`,`u`.`mobile` AS `mobile`,`u`.`email` AS `email`,`u`.`parent_ids` AS `parent_ids`,
(select group_concat(`inneru1`.`firstname` order by `inneru1`.`id` ASC separator ',') from (`tbl_user` `inneru2` join `tbl_user` `inneru1` 
on((find_in_set(`inneru1`.`id`,`inneru2`.`parent_ids`) > 0))) where (`inneru2`.`id` = `u`.`id`) group by `inneru2`.`id`) AS `parent_names`,

(select group_concat(`inner_usertype`.`user_type` separator ',') 
from (`tbl_user` `inneru123` join `tbl_user` `inner_usertype` on((find_in_set(`inner_usertype`.`id`,`inneru123`.`parent_ids`) > 0))) 
where (`inneru123`.`id` = `u`.`id`) group by `inneru123`.`id`) AS 
`parent_usertypes`,

`u`.`office_phone_no` AS `office_phone_no`,`u`.`accname` AS `accname`,`u`.`accno` AS `accno`,`u`.`bank_name` AS `bank_name`,
`u`.`accbrnm` AS `accbrnm`,`u`.`accifsc` AS `accifsc`,`u`.`gst_number_sss` AS `gst_number_sss`,`u`.`latitude` AS `latitude`,`u`.`longitude` AS 
`longitude`,`u`.`marginType` AS `marginType`,`u`.`userid_margin` AS `userid_margin`,`u`.`is_default_user` AS `is_default_user`,`u`.`is_common_user`
 AS `is_common_user`,`u`.`added_on` AS `added_on` from ((`tbl_user` `u` left join `tbl_state` `s` on((`u`.`state` = `s`.`id`))) 
 left join `tbl_city` `c` on((`u`.`city` = `c`.`id`))) where ((`u`.`isdeleted` <> '1') and (`u`.`is_common_user` <> '1')) group by `u`.`id` order by `u`.`id` desc ;