<?php
include ("../../includes/config.php");
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
extract($_POST);
//echo "<pre>";print_r($_POST['frmdate']);var_dump($_POST['frmdate']);
$all_shops_sql="SELECT `id`, `name`,  `latitude`, `longitude` 
FROM `tbl_shops` 
WHERE latitude!='' and latitude is not null and latitude!='0.0' and 
longitude!='' and longitude is not null and longitude!='0.0'
 ";
 $frmdate=$_POST['frmdate'];
$result_all_shop = mysqli_query($con,$all_shops_sql); 
$total_all_shops=mysqli_num_rows($result_all_shop);
$allshopsarray=array();
while($row_all_shop = mysqli_fetch_array($result_all_shop)){
	$shopid=$row_all_shop['id'];
	$allshopsarray[$shopid]['id']=$row_all_shop['id'];
	$allshopsarray[$shopid]['name']=$row_all_shop['name'];
	$allshopsarray[$shopid]['latitude']=$row_all_shop['latitude'];
	$allshopsarray[$shopid]['longitude']=$row_all_shop['longitude'];
	 $all_shops_visit_sql="SELECT DISTINCT(SV.shop_id),SV.shop_visit_date_time AS date,
						SV.shop_visit_reason, SV.shop_close_reason_type, SV.shop_close_reason_details
							FROM `tbl_shop_visit` SV 
							WHERE date_format(SV.shop_visit_date_time, '%Y-%m-%d') = STR_TO_DATE('".$frmdate."', '%d-%m-%Y')	
							AND is_shop_location=0 
							AND SV.shop_id='".$shopid."'";
	$result_all_shop_visit = mysqli_query($con,$all_shops_visit_sql); 
	$allshopsarray[$shopid]['visit']='';
	while($row_all_shop_visit = mysqli_fetch_array($result_all_shop_visit)){		
		$allshopsarray[$shopid]['visit']['shop_visit_reason']=$row_all_shop_visit['shop_visit_reason'];
		$allshopsarray[$shopid]['visit']['shop_close_reason_type']=$row_all_shop_visit['shop_close_reason_type'];
		$allshopsarray[$shopid]['visit']['shop_close_reason_details']=$row_all_shop_visit['shop_close_reason_details'];
	}
	 $all_shops_order_sql="SELECT sum(o.total_order_gst_cost) as total_order_gst_cost					
							FROM `tbl_orders` o 
							WHERE date_format(o.order_date, '%Y-%m-%d') = STR_TO_DATE('".$frmdate."', '%d-%m-%Y')	
							AND o.shop_id='".$shopid."'";
	$result_all_shop_order = mysqli_query($con,$all_shops_order_sql); 
	while($row_all_shop_order = mysqli_fetch_array($result_all_shop_order)){		
		$allshopsarray[$shopid]['total_order_gst_cost']=$row_all_shop_order['total_order_gst_cost'];
	}
}
//echo "<pre>";print_r($allshopsarray);


?>
<div class="portlet box blue-steel">
	<div class="portlet-title">
		<div class="caption">
			Sales Person Location
		</div>
		<div class="clearfix"></div>
	</div>	
	<div class="portlet-body" style="height:500px;">
		<? $ar[] = array();$i=0;	
		$count=$i;
		foreach($allshopsarray as $keyshops=>$rowshop){
			$lat = '';
			$long = '';			
			if($rowshop["latitude"] !='' && $rowshop["longitude"] !=''){
				$lat = $rowshop["latitude"];
				$long = $rowshop["longitude"];
			}
			if($lat !='' && $long !=''){
				$ar[$i]['lat'] = $lat;
				$ar[$i]['lng'] = $long;				
				
				$ar[$i]['shop']= $rowshop["name"]."";
				$ar[$i]['title']="Shop Name: ".$rowshop["name"]."";
				
				$shop_visit_reason = '';$shop_close_reason_type = '';
				$shop_close_reason_details = '';$total_order_gst_cost='';
				
				if(!empty($rowshop['total_order_gst_cost']) && $rowshop['total_order_gst_cost']!=0){
					$ar[$i]['description']="Total Sale: ".$rowshop['total_order_gst_cost'];
					$ar[$i]['totalsale']=$rowshop['total_order_gst_cost'];
					$ar[$i]['color'] = 'green';
					$ar[$i]['shop_type'] = 'old';
				}else if($rowshop['visit'] != ''){
					$ar[$i]['description']="Reason: ".$rowshop['visit']['shop_visit_reason']." ".$rowshop['visit']['shop_close_reason_type']." ".$rowshop['visit']['shop_close_reason_details'];
					$ar[$i]['totalsale']=0;
					$ar[$i]['color'] = 'red';
					$ar[$i]['shop_type'] = 'old';
				}else{
					$ar[$i]['description']="Not yet visited";
					$ar[$i]['totalsale']=0;
					$ar[$i]['color'] = 'grey';
					$ar[$i]['shop_type'] = 'old';
				}
			
			$i++;
			}
			//echo $lattitude."----".$longitude."*".$rowshop["id"]."*";
		}
		$total_count=count($ar);
		//echo "<pre>";print_r($ar);exit;
		//echo "<pre>";print_r($totalshops);
		if($i == 0){
		?>
		<div class="form-group">
			<div class="col-md-12">				
				<div  id="dvMap" style="min-height:480px">
				No Record available.
				</div>
			</div>
		</div>
		<?php exit; }else{ ?>
		<div class="form-group">
			<div class="col-md-12">
				
				 <div  id="dvMap" style="min-height:480px">
			</div>
		</div>
		</div>
		<?php } ?>
</div>
<script>
var markers=<?php echo json_encode($ar); ?>;
var countertr=<?php echo $count;?>;
var total_count=<?php echo $total_count;?>;
//alert(countertr);

 function initMap() {
		drawingRoute();
		
      }
	  function drawingRoute(){
		  //next function start
		
    var mapOptions = {
        center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
    var infoWindow = new google.maps.InfoWindow();
    var lat_lng = new Array();
    var latlngbounds = new google.maps.LatLngBounds();
    for (i = 0; i < markers.length; i++) {
        var data = markers[i];
        var myLatlng = new google.maps.LatLng(data.lat, data.lng);
        lat_lng.push(myLatlng);
		// if(i == 0||i >= countertr-1){
			/*if(i == 0){
				var marker = new google.maps.Marker({
				position: myLatlng,
				map: map,
				label:'S',
				title: data.title
				});
			}else if(i == countertr-1) {
				var marker = new google.maps.Marker({
				position: myLatlng,
				map: map,
				label:'D',
				title: data.title
				});
			}else*/ {			
				var icon_gr = 'http://maps.google.com/mapfiles/marker_greyS.png';
				var icon_r = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
				var	icon_g = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
				var	icon_b = 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png';
				var	icon_y = 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png';
				
				switch(data.color){
					case 'green':
						var marker = new google.maps.Marker({
						position: myLatlng,
						map: map,						
						title: data.title,
						icon: icon_g
						});
					break;
					case 'grey':
						var marker = new google.maps.Marker({
						position: myLatlng,
						map: map,						
						title: data.title,
						icon: icon_gr
						});
					break;
					case 'red':
						var marker = new google.maps.Marker({
						position: myLatlng,
						map: map,						
						title: data.title,
						icon: icon_r
						});
					break;
				}
				
				
			}
			
		//} 
		
        latlngbounds.extend(marker.position);
        (function (marker, data) {
            google.maps.event.addListener(marker, "click", function (e) {
                infoWindow.setContent(data.description);
                infoWindow.open(map, marker);
            });
        })(marker, data);
    }
	
    map.setCenter(latlngbounds.getCenter());
    map.fitBounds(latlngbounds);

    //***********ROUTING****************//

    //Initialize the Direction Service
    var service = new google.maps.DirectionsService();

    //Loop and Draw Path Route between the Points on MAP
   
	  }
   
</script>
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDMpdO43txdg_zyovvZm9i1tMMFIQTKTU&callback=initMap"
        async defer></script>
