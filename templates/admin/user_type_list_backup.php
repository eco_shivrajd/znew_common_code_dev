<!-- BEGIN HEADER -->
<?php 
include "../includes/grid_header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "UserType";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<h3 class="page-title">User Type</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li><i class="fa fa-home"></i>
					<a href="#">User Type</a></li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				
					<div class="portlet box blue-steel">
						<div class="portlet-title">						
							<div class="caption">User Type Listing</div>							
							<? if($_SESSION[SESSION_PREFIX."user_type"]=="Admin")  { ?>
								<a href="user-type-add.php" class="btn btn-sm btn-default pull-right mt5">Add User Type</a>
							<? } ?>
							
                            <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>
											 User Type 
										</th>
									</tr>
								</thead>
							<tbody>
							<?php
					        $sql = "SELECT id,usertype,ul_added_on FROM `tbl_userlevel` where isdeleted!=1";
                            $result = mysqli_query($con, $sql);
							while($row = mysqli_fetch_array($result))
							{		?>
								<tr class="odd gradeX">
									<td><?php echo  $row['usertype'];	?>	</td>
								</tr>
						<?php	} ?>
	
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>