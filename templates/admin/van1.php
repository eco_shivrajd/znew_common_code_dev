<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);
?>
<!-- END HEADER -->
<?php
if(isset($_POST['submit']))
{
	if(isset($_GET['idu'])){
		$id=$_GET['idu'];
		$van_no		  	= $_POST['van_no'];
		$van_rto		= $_POST['van_rto'];
		$van_type		= $_POST['van_type'];
		$van_owner		= $_POST['van_owner'];
		$mobile			= $_POST['mobile'];
		$van_cap_wght	=$_POST['van_cap_wght'];
		$van_cap_boxes	= $_POST['van_cap_boxes'];
		$update_sql="UPDATE tbl_van SET van_no='$van_no',van_rto='$van_rto',van_type='$van_type',van_owner='$van_owner',mobile='$mobile',van_cap_wght='$van_cap_wght',van_cap_boxes='$van_cap_boxes'  where id='$id'";		
		$result = mysqli_query($con,$update_sql);
		if($result){echo '<script>alert("Vehicle has been updated successfully.");location.href="van.php";</script>';}
		else{echo '<script>alert("Vehicle not updated !");location.href="van.php";</script>';}
	}
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageTransport"; $activeMenu = "Vehicles";
	include "../includes/sidebar.php";
	$commonObj 	= 	new commonManage($con,$conmain);
	$row_url=$commonObj->getPageIDforUrlEdit($php_page_name);
	$page_id_url = $row_url['page_id'];
	$row_url_edit=$commonObj->getURLforEdit($profile_id,$page_id_url);
	$ischecked_edit_url = $row_url_edit['ischecked_edit'];
    if ($ischecked_edit_url == 0 && $ischecked_edit_url!='') 
	{
		session_set_cookie_params(0);
		session_start();
		session_destroy();
		echo '<script>location.href="../login.php";</script>';
	    exit;
	}
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- /.modal -->
			<h3 class="page-title">Edit Vehicle</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="van.php">Vehicle</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#"><? if($_SESSION[SESSION_PREFIX.'user_type']=="Distributor") { echo "Vehicle"; } else { echo "Edit Vehicle"; } ?></a> 
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<? if($_SESSION[SESSION_PREFIX.'user_type']=="Distributor") { echo "Vehicle"; } else { echo "Edit Vehicle"; } ?>
							</div>
							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						 
						<?php
		if(isset($_GET['id'])){
			$id=$_GET['id'];
			$sql="SELECT id,van_no,van_rto,van_type,van_owner,mobile,van_cap_wght,van_cap_boxes,del_status
					FROM tbl_van 
					WHERE id = $id";
			//echo $sql;exit();
			$result = mysqli_query($con,$sql);
			while($row = mysqli_fetch_array($result))
			{
						?>
						<form class="form-horizontal" data-parsley-validate="" role="form" method="post" action="van1.php?idu=<?php echo $row['id'];?>" novalidate="">         
					
							<div class="form-group">
								<label class="col-md-3">Vehicle No.:<span class="mandatory">*</span></label>
								<div class="col-md-4"><input name="van_no" type="text" class="form-control" 
								placeholder="Enter Vehicle No."
								data-parsley-required="#true" 
								data-parsley-required-message="Please enter Vehicle No." 
									value="<?php if($row['van_no']!=""){echo $row['van_no'];}
											?>">
								</div>
							</div>
							
							<div class="form-group">
							<label class="col-md-3">Vehicle RTO Number:<span class="mandatory">*</span></label>
							<div class="col-md-4"><input name="van_rto" type="text" class="form-control" 
							placeholder="Vehicle RTO Number"
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter Vehicle RTO Number"
							value="<?php if($row['van_rto']!=""){echo $row['van_rto'];}
											?>"></div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3">Vehicle Type:</label>
							<div class="col-md-4"><input name="van_type" type="text" class="form-control" value="<?php if($row['van_type']!=""){echo $row['van_type'];}
											?>"></div>
						</div>
						<div class="form-group">
							<label class="col-md-3">Vehicle Capacity Weight:</label>
							<div class="col-md-4"><input name="van_cap_wght" type="text" class="form-control" value="<?php if($row['van_cap_wght']!=""){echo $row['van_cap_wght'];}
											?>"></div>
						</div>
						<div class="form-group">
							<label class="col-md-3">Vehicle Capacity Pcs:</label>
							<div class="col-md-4"><input name="van_cap_boxes" type="text" class="form-control" value="<?php if($row['van_cap_boxes']!=""){echo $row['van_cap_boxes'];}
											?>"></div>
						</div>
							
							<div class="form-group">
								<label class="col-md-3">Transporter/Owner Name:<span class="mandatory">*</span></label>
								<div class="col-md-4"><input name="van_owner" type="text" class="form-control" 
								placeholder="Enter Transporter/Owner Nam" 
								data-parsley-required="#true" 
								data-parsley-required-message="Please enter Transporter/Owner Name"
								value="<?php if($row['van_owner']!=""){echo $row['van_owner'];}
											?>"></div>
							</div>
							
							<div class="form-group">
								<label class="col-md-3">Contact  Number:<span class="mandatory">*</span></label>
								<div class="col-md-4"><input name="mobile" type="text" class="form-control" 
								placeholder="Enter Contact Number" 
								data-parsley-required="#true" 
								data-parsley-required-message="Please enter Contact  Number"
								data-parsley-trigger="change" 
								data-parsley-minlength="10" 
								data-parsley-maxlength="15" 
								data-parsley-maxlength-message="Only 15 characters are allowed" 
								data-parsley-pattern="^(?!\s)[0-9]*$" 
								data-parsley-pattern-message="Please enter numbers only"
								value="<?php if($row['mobile']!=""){echo $row['mobile'];}
											?>"></div>
							</div>
						<div class="form-group">
						  <div class="col-md-4 col-md-offset-3">
						   <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
							<a href="van.php" class="btn btn-primary">Cancel</a>
						  </div>
						</div><!-- /.form-group -->
						
					  </form> <?php } }?>
						                                      
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>