<?php 
include ("../../includes/config.php");
include ("../includes/common.php");
include "../includes/userManage.php";
include "../includes/orderManage.php";
include "../includes/shopManage.php";
$userObj 	= 	new userManager($con,$conmain);
$orderObj 	= 	new orderManage($con,$conmain);
$shopObj 	= 	new shopManager($con,$conmain);

 $order_id = $_POST['order_id'];
//exit();
$order_status = $_POST['order_status'];
/*echo $order_id;
echo "hi";
echo $order_status;*/

$order_sql11 ="SELECT os.id, os.order_no, os.invoice_no, os.ordered_by_userid, os.assign_to_userid, os.ordered_by_usertype, os.order_date,u.firstname,u.address,u.gst_number_sss,u.accname,u.accno,u.bank_name,u.accbrnm,u.accifsc,s.name as state_name,c.name as city_name
FROM tbl_order_stockist as os
left join tbl_user u ON u.id = os.ordered_by_userid 
left join tbl_state s ON s.id = u.state 
left join tbl_city c ON c.id = u.city 
where os.order_no =$order_id 
limit 1";


$result11 = mysqli_query($con,$order_sql11);

       while($row = mysqli_fetch_array($result11))
		{
			$firstname = $row['firstname'];
			$order_no = $row['order_no'];
			$address = $row['address'];
			$state_name = $row['state_name'];
            $city_name = $row['city_name'];
			$gst_number_sss = $row['gst_number_sss'];
			$accname = $row['accname'];
			$accno = $row['accno'];
			$bank_name = $row['bank_name'];
			$accbrnm = $row['accbrnm'];
			$accifsc = $row['accifsc'];

		}
/*echo $order_id1;		
exit();*/

$admin_details_basic = $userObj->getLocalUserDetails($_SESSION[SESSION_PREFIX.'user_id']);
$admin_details = $userObj->getAdminDetails($_SESSION[SESSION_PREFIX.'user_id']);
//var_dump($admin_details);

$order_details = $orderObj->getDirectOrdersbyorderid($order_no);
//var_dump($order_details);
//exit();
//$order_details = $orderObj->getShopOrdersbyorderid($order_id1);



//var_dump($shop_details);
//echo "<pre>";print_r($order_details);//die();
$colspan3=3;
$colspan2=2;
?>
<style>
.darkgreen{
	background-color:#364622; color:#fff!important; font-size:24px;font-weight:600;
}
.fentgreen1{
	background-color:#b0b29c;
	color:#4a5036;
	font-size:12px;
}
.fentgreen{
	background-color:#b0b29c;
	color:#4a5036;
}
.font-big{
	font-size:20px;
	font-weight:600;
	color:#364622;
}
.font-big1{
	font-size:14px;
	font-weight:600;
	color:#364622;
}
.table-bordered-popup {
    border: 1px solid #364622;
}
.table-bordered-popup > tbody > tr > td, .table-bordered-popup > tbody > tr > th, .table-bordered-popup > thead > tr > td, .table-bordered-popup > thead > tr > th {
    border: 1px solid #364622;
	color:#4a5036;
}
.blue{
	color:#010057;
}
.blue1{
	color:#574960;
	font-size:16px;
}
.buyer_section{
	color:#574960;
	font-size:14px;
}
.pad-5{
	padding-left:10px;
}
.pad-40{
	padding-left:40px;
}
.np{
	padding-left:0px;
	padding-right:0px;
}
.bg{
	background-image:url(../../assets/global/img/invoice/<?php echo COMPANYNM;?>_logo-watermark.jpg); background-repeat:no-repeat;
	 background-size: 200px 200px;
}
</style>
<div class="modal-header">
<button type="button" name="btnPrint" id="btnPrint" onclick="takeprint_invoice('<?=SITEURL;?>')" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>

<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel"></h4>	   
</div>
<div class="modal-body" style="padding-bottom: 5px !important;" id="divPrintArea">
<div class="row">
<div class="col-md-12"> 
		<div class="portlet-body">
			<table class="table table-bordered-popup">
				<tbody>
				<tr>				
					<td colspan="4" width="70%" class="darkgreen" valign="top"><img src="../../assets/SiteLogo/<?php echo COMPANYNM;?>_logo-big.png" style="width:60px;"> &nbsp; <?php echo strtoupper(COMPANYNM);?></td>
				<td colspan="<?=$colspan3;?>" class="font-big text-center" valign="top">Tax Invoice</td>
				</tr>
				
				<tr>				
					

                    <td colspan="4" class="fentgreen1"><?php echo $admin_details_basic['address'];?><br/>
					Tel: <b><?php echo $admin_details['phone_no'];?></b> 
					<?php $admin_details['website'];?> Tollfree: <b><?php echo $admin_details['tollfree_no'];?></b>  
					State: <b><?php echo $admin_details_basic['state_name'];?></b> State Code: <b><?php echo $admin_details_basic['state'];?></b> GSTIN :<b><?php echo $admin_details['gst_number_sss'];?></b></td>





					<td colspan='3' rowspan='2'>
					<div class='col-md-8 np'>Invoice No.: <?=$order_details['invoice_no']?>&nbsp;<span class='blue'></span></div><br/>
					<div class='col-md-8 np'>Dated: &nbsp;-<span class='blue'></span></div><br/>
					<div class='col-md-8 np'>D. C. No.: &nbsp;-<span class='blue'></span></div><br/>
					<div class='col-md-8 np'>Vehicle No.: &nbsp;-<span class='blue'></span></div><br/>
					<div class='col-md-8 np'>Transportation Mode: &nbsp;-<span class='blue'></span></div> <br/>
					<!-- <div class='col-md-8 np'>Date & Time of Supply: <?=$order_details['date_time_supply']?>&nbsp;<span class='blue'></div></span><br/>
					<div class='col-md-8 np'>Place of Supply: <?=$order_details['place_of_supply']?>&nbsp;<span class='blue'></span></div>
					</td> -->
				</tr>
				
				<tr>
					<td colspan='4' valign='top'>Buyer 
					<span class='buyer_section'><b><?=$firstname;?></b><br/></span>
					<span class='buyer_section pad-40'><?=$address;?>,<br/></span>
					<span class='buyer_section pad-40'><?=$city_name;?><br/></span>
					<span class='buyer_section pad-40'><?=$state_name;?><br/></span>
					<span class='buyer_section pad-40'>GSTIN NO. <?=$gst_number_sss;?></span>
					</td>                        
				</tr>
				<tr class='fentgreen'>
				<th width='5%' class='text-center'>SI No.</th>
				<th class='text-center'>Name of Goods</th>
				<th class='text-center'>HSN Code</th>
				<th class='text-center'>Qty</th>
				<th class='text-center'>Rate</th>
				<th class='text-center'>UOM</th>
				<th class='text-center'>Value (CGST+SGST)</th>
				</tr>
				<tr style='height:214px;'>
				<?php
				$i = 1;
				$final_qty = 0;
				$final_cost = 0;
				$total_amount = 0;
				$free_product_count=array();
				foreach($order_details as $val){
                  //  echo "<pre>";
				  //	print_r($order_details);
                    //   exit();
					$sr_no.=$i.'<br><br>';
					$product_name.=$val['productname'].'('.$val['variant_1'].' '.$val['unitname1'].')<br><br>';
					//$hsn.=$val['producthsn'].'<br><br>';
					$qty.=$val['product_quantity'].'<br><br>';
					$product_cgst_sgst.="CGST-".$val['product_cgst']."%".", SGST-".$val['product_sgst']."%".'<br><br>';
					$c_o_d = $val['c_o_d'];
					$cod_percent = $val['cod_percent'];
					$final_qty = $final_qty + $val['product_quantity'];
					$unit_cost.=$val['unit_price'].'<br><br>';
					$nos.='nos<br><br>';
					//$total_cost.=($unit_cost*$val['variantunit']).'<br><br>';
					$total_cost.= $val['p_cost_totalwith_tax']."<br><br>";
					$final_cost = $final_cost + $val['p_cost_totalwith_tax'];
					if(!empty($val['free_product_details'])){
						$free_product_count[$i]=$val['free_product_details'];
					}
					
					
                     $cod_final_cost = ($cod_percent / 100) * $final_cost;
                                   $fcod_final_cost = $final_cost -$cod_final_cost;
					$i++; 
					}
					//echo "<pre>";print_r($free_product_count);
					foreach($free_product_count as $valfree){
						$arr=explode(",",$valfree);
						$sr_no.=$i.'<br><br>';
						$productname=	$orderObj->getProductname($arr[4]);
						$product_name.="<span style='float: left'>
							<img src='../../assets/global/img/free-icon.png' title='Free Product'>
						</span>";
						foreach($productname as $keyprod=>$valueprod){								
								$product_name.=$valueprod.'('.$arr[7].')<br><br>';
							}
						//$product_name.=$productname.'<br><br>';
						
						//$hsn.=$val['producthsn'].'<br><br>';
						$qty.=$arr[8].'<br><br>';
						$c_o_d = 0;
						$cod_percent = 0;
						$final_qty = $final_qty + $arr[8];
						$unit_cost.='0<br><br>';
						$nos.='nos<br><br>';
						//$total_cost.=($unit_cost*$val['variantunit']).'<br><br>';
						$total_cost.= "0<br><br>";
						$final_cost = $final_cost + 0;
						
						
						
						 $cod_final_cost = ($cod_percent / 100) * $final_cost;
									   $fcod_final_cost = $final_cost -$cod_final_cost;
						$i++; 
					}
					?>
				
				
				<td class="text-center" valign="top"><span class="blue"><?=$sr_no;?></span></td>
				<td class="bg" valign="top"><span class="blue"><?=$product_name;?></span></td>
				<td class="text-left" valign="top"><span class="blue"><?=$product_cgst_sgst;?></span></td>
				<td class="text-right" valign="top"><span class="blue"><?=$qty;?></span></td>
				<td class="text-right" valign="top"><span class="blue"><?=$unit_cost;?></span></td>
				<td class="text-center" valign="top"><span class="blue"><?=$nos;?></span></td>
				
				<td class="text-right" align="right" valign="top"><span class="blue"><?=$total_cost;?></span></td>
				</tr>
				<tr>
				<td></td>
				<td class="text-right"><b>Total</b></td>
				<td class="fentgreen"></td>
				<td class="fentgreen text-right"><?=$final_qty;?></td>
				<td class="fentgreen"></td>
				<td class="fentgreen"></td>
				
				<td class="fentgreen" align="right"><?=$final_cost;?></td>
				</tr>
				
				<tr>
				<td colspan="4" class="text-center blue"><b><?=$final_cost?> only</b>
				
				<table class="table table-bordered-popup">
				<tbody>
				<tr>
				<td class="text-center"><span class="blue">HSN/SAC</span></td>
				<td class="text-center"><span class="blue">Taxable Value</span></td>
				<td colspan="2" class="text-center"><span class="blue">Central Tax</span> </td>
				<td colspan="2" class="text-center"><span class="blue">State Tax</span></td>
				</tr>
				
				<tr>
				<td></td>
				<td></td>
				<td><span class="blue">Rate</span> </td>
				<td><span class="blue">Amount</span></td>
				<td><span class="blue">Rate</span> </td>
				<td><span class="blue">Amount</span></td>
				</tr>
				
				<tr>
				<td><span class="blue"></span></td>
				<td class="text-right"><span class="blue"></span></td>
				<td><span class="blue"> %</span></td>
				<td class="text-right"><span class="blue"></span></td>
				<td><span class="blue">%</span></td>
				<td class="text-right"><span class="blue"></span></td>
				</tr>
				
				<tr>
				<td class="text-right"><span class="blue">Total</span></td>
				<td class="text-right"><span class="blue"><?=$total_amount;?></td>
				<td></td>
				<td class="text-right"><span class="blue"></span></td>
				<td> </td>
				<td class="text-right"><span class="blue"></span></td>
				</tr>
				
				</tbody>
				</table>
				
				</td>
				<td colspan="<?=$colspan2;?>" rowspan="2" valign="top">
				<span style="display:inline-block; height:40px;" class="blue">CGST @  %</span><br/>
				<span style="display:inline-block; height:40px;" class="blue">SGST @ %</span><br/>
				<span style="display:inline-block; height:40px;" class="blue">Rounding Off</span> <br/>
				<span style="display:inline-block; height:40px;" class="blue"><b>Grand Total</b> </span><br/>
				<span style="display:inline-block; height:40px;" class="blue">Opening Balance</span> <br/>
				<span style="display:inline-block; height:40px;" class="blue">Closing Balance </span>
				</td>
				<td rowspan="2" class="text-right" valign="top">
				<span style="display:inline-block; height:40px;" class="blue"> </span><br/>
				<span style="display:inline-block; height:40px;" class="blue"></span><br/>
				
				
				<span style="display:inline-block; height:40px;" class="blue"><?=$cod_percent;?> % </span><br/>
				<span style="display:inline-block; height:40px;" class="blue"><b> <?=$fcod_final_cost;?></b></span><br/>
				<span style="display:inline-block; height:40px;" class="blue"></span><br/>
				<span style="display:inline-block; height:40px;" class="blue"></span>
				</td>
				</tr>
				
				<tr>
				<td colspan="2" width="30%" valign="top">
				<u>Declaration:</u><br/>				
				</td>
				<td>
					<div class="text-center" width="25%" valign="top"><b><u>BANK DETAILS</u></b>
					</div>
					<?php if ($accno=='') { ?>
                     Sorry..No Bank Account Details.
                     <?php } else { ?>
						BANK NAME: <?=$bank_name;?><br/>
						BRANCH : <?=$accbrnm;?> <br/>
						CC A/C NO.: <?=$accno;?><br/>
						IFSC CODE: <?=$accifsc;?></td>
                     <?php } ?>				
				<td  class="fentgreen font-big1" width="20%" valign="top">For <b><?php echo strtoupper(COMPANYNM);?></b><br/><br/><br/>
				Authorised Signature
				</td>
				</tr>
				</tbody>
				
				</table>
</div>

</div>
</div>
</div>