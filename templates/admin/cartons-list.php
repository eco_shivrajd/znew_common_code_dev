<?php
include "../includes/grid_header.php";
include "../includes/commonManage.php";
include "../includes/reportManage.php";
$commonObj = new commonManage($con, $conmain);
$reportObj = new reportManage($con, $conmain);
$commonObjctype = $commonObj->log_get_commonclienttype($con, $conmain);

$user_type = $_SESSION[SESSION_PREFIX . 'user_type'];
$user_id = $_SESSION[SESSION_PREFIX . 'user_id'];
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content">
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "ManageProducts";
        $activeMenu = "Cartons";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

                <!-- /.modal -->

                <h3 class="page-title">
                    Cartons
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="#">Cartons</a>
                        </li>
                    </ul>
					

                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">

                        <div class="portlet box blue-steel">
                            <div class="portlet-title">

                                <div class="caption">Cartons Listing</div>				
                                <!--<a href="product_variant.php" class="btn btn-sm btn-default pull-right mt5 ml10">Add Product dimensions</a>-->
                                <?php
                                if ($ischecked_add==1) 
                                {
                                ?>
                                <a href="cartons-add.php" class="btn btn-sm btn-default pull-right mt5 ">Add Carton</a><?php } ?>				  
                                <div class="clearfix"></div>

                            </div>

                            <div class="portlet-body">				
                                <table class="table table-striped table-bordered table-hover" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th>
                                                Cartons
                                            </th>
                                            <th>
                                                Category
                                            </th>
                                            <th>
                                                Product Name
                                            </th>                                           
                                            <th>
                                                Product Price
                                            </th>
                                            <th>
                                                Product Variant
                                            </th>
                                            <th>
                                                Quantities
                                            </th>
                                             <th>
                                                Assign Cartons
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                         $sqlcartons = "SELECT distinct(cartons_id) as cartons_id FROM tbl_dcp_cartons  ";
                                        $result = mysqli_query($con, $sqlcartons);
                                        $row_count = mysqli_num_rows($result);
                                        $rowcartonids=array();
                                        
                                        while ($row2 = mysqli_fetch_array($result)) { 
                                            $cartonids=$row2['cartons_id'];
                                            $cartondetails = $reportObj->getCartonDetails($cartonids);
                                           $category_name='';$product_name='';$product_price='';
                                           $product_qnty='';$product_var='';
                                           foreach($cartondetails['carton_details'] as $key =>$value ){
                                                $category_name .= $value['category_name'] . "<br>";
                                                $product_name .= $value['product_name'] . "<br>";
                                                $product_price .=  $value['product_price'] . "<br>";
                                                $product_qnty .=  $value['qnty'] . "<br>";
												 $product_var .=  $value['product_variant1']."-".$value['variant1_unit_name'] ."("
																	.$value['product_variant2']."-".$value['variant2_unit_name'] 
																. ")<br>";												
                                           }
                                            ?>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <?php echo "carton-" . $cartonids; ?>
                                                </td>
                                                <td><?php echo $category_name; ?> </td>
                                                <td><?php echo $product_name; ?> </td>
                                                <td align="right"><?php echo $product_price; ?> </td>
                                                <td><?php echo $product_var; ?> </td>                                                
                                                <td align="right"><?php echo $product_qnty; ?> </td>
                                                <td><?php if ($ischecked_edit==1) { ?>
                     <a title="Assign To Delivery Channel Person" onclick="javascript: assign_cartons('<?php echo $cartonids;?>')" 
					 class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-share"></span> Assign 
                            </a>
                        <?php }else { echo '-'; } ?>
                                                </td>
                                                </tr>
						<?php } ?>							
                                    </tbody>
                                </table>
                            </div>
                        </div> 

                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
        <div class="modal fade" id="assignDCPModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Assign Cartons TO:</h4>	   
                </div>		
                <form role="form" class="form-horizontal" onsubmit="return false;" action="cartons-list.php" data-parsley-validate="" name="assign_dcp_form" id="assign_dcp_form">
                    <div class="modal-body" >	  
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="col-md-4">Select Delivery Channel Person:</label>
                            <div class="col-md-6" id="show_dcp_dropdown">
                               <select name="dcp_id" id="dcp_id" class="form-control" 
                                    data-parsley-trigger="change"
                                    data-parsley-required="#true" 				
                                    data-parsley-required-message="Please select Delivery Channel Person"
                                    parsley-required="true"
                                    data-parsley-required-message="Please select State">
                                   <option value=''>-Select-</option>                                   
                                <?php
                                    $sqldcp = "SELECT tu.id,tu.firstname 
                                        FROM tbl_user tu 
                                        where user_role='DeliveryChannelPerson' AND  external_id='".$user_id."'";
                                          // . "group by tds.dcp_id ";
                                           //. "having ((sum(tds.stock_qnty) <=0) or(sum(tds.stock_qnty)  is null))";
                                    $resultdcp = mysqli_query($con, $sqldcp);
                                    $row_count_dcp = mysqli_num_rows($resultdcp);
                                     $sqlcartons = "SELECT tds.dcp_id,sum(tds.stock_qnty) as stock_qnty 
                                        FROM tbl_dcp_stock tds
                                        group by tds.dcp_id 
                                        having sum(tds.stock_qnty) > 0";
                                    $result = mysqli_query($con, $sqlcartons);
                                    $row_count = mysqli_num_rows($result);
                                    $stockarray=array();
                                    while ($row2 = mysqli_fetch_array($result)){
                                        $dcp_id=$row2['dcp_id'];
                                        $stockarray[$dcp_id]=$row2['stock_qnty'];                                        
                                    }
                                            
                                    while ($row_dcp = mysqli_fetch_array($resultdcp)) {
                                        $userid=$row_dcp['id'];
                                        if (array_key_exists($userid,$stockarray)){?>
                                            <option value='<?php echo $row_dcp['id'];?>' disabled><?php echo fnStringToHTML($row_dcp['firstname'])." qnty:".$stockarray[$userid];?></option>
                                <?php   }else{ ?>
                                            <option value='<?php echo $row_dcp['id'];?>'><?php echo fnStringToHTML($row_dcp['firstname']);?></option>
                                     <?php   }
                                       } ?>
                                </select> 
                            </div>
                        </div><!-- /.form-group --> 
                         <div class="form-group">
                            <label class="col-md-4">No of cartons:</label>
                            <div class="col-md-6">
                                <input type="text"  name="no_of_catons" id="no_of_catons"  placeholder="0" class="form-control"  min="0" max="20000" step="100" 
                                data-parsley-validation-threshold="1" data-parsley-trigger="keyup" 
                                data-parsley-type="digits" required />                             
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-3">					
                                <button type="submit"  name="btnsubmit"  class="btn btn-primary">Submit</button>
                                <a href="cartons-list.php" class="btn btn-primary">Cancel</a>
                            </div>
                        </div><!-- /.form-group -->
                        <input type="hidden" name="input_carton_id" id="input_carton_id" value="0"/>
                        <input type="hidden" name="action" id="action" value="assign_cartons"/>
                    </div><!-- /.form-group --> 				
            </div>
            </form>	   	  
        </div>
    </div>
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <?php include "../includes/grid_footer.php" ?>
    <!-- END FOOTER -->
    <script>
    function assign_cartons(carton_id) {
        $('#assignDCPModal').modal('show');
        $('#input_carton_id').val(carton_id); 
    }
    $('form#assign_dcp_form').submit(function () {
        var formData = new FormData($(this)[0]);
        var userid=$('#dcp_id').val();
        var no_of_catons=$('#no_of_catons').val();
        if(userid !== null && userid !== '' && no_of_catons !== null && no_of_catons !== '' && no_of_catons > 0 ) {
            $.ajax({
                url: "assign_cartons.php",
                type: 'POST',
                data: formData,
                success: function (data) {
                    console.log(data);
                    if (data == 1) {
                        alert('Carton assigned to Delivery Channel Person successfully.');
                        $('#assignDCPModal').modal('hide');
                        document.forms.assign_dcp_form.reset();
                        window.location.reload(true);
                    } else if (data == 'select') {
                        alert('Please select Delivery Channel Person.');
                        return false;
                    } else {
                        alert('Unable to assign to Delivery Channel Person.');
                        return false;
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }else{
            if(userid === null && userid === ''){
                alert('Please Select Delivery Channel Person.');               
                return false;
            }
            if(no_of_catons === null && no_of_catons === '' && !(Number.isInteger(no_of_catons)) && no_of_catons < 0){
                alert('Please Enter Valid No of Cartons.');
                return false;
            }
        }
    });
</script>
</body>
<!-- END BODY -->
</html>