<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php"?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageProducts"; $activeMenu = "Variant";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Variants
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Variants</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
            <div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Variants Listing
							</div>
                            <!-- <a class="btn btn-sm btn-default pull-right mt5" href="variant-add.php">
                                Add Variant
                              </a> -->
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								<th>
									 Variant Name
								</th>
								<th id="not_sorting_desc">
									Unit Names
								</th>
							</tr>
							</thead>
							<tbody>
							<?php
							$sql1="SELECT `id`, `name` FROM  `tbl_variant` ";
							$result1 = mysqli_query($con,$sql1);
							while($row1 = mysqli_fetch_array($result1))
							{
								
									echo	'<tr class="odd gradeX"> <td>';
                                    if ($ischecked_edit==1) 
                                            { 
									        echo fnStringToHTML($row1['name']);
									      }
                                       else {
                                                echo fnStringToHTML($row1['name']);
                                            }
                                    echo '</td><td>';
										$sql_units="SELECT `unitname` FROM  `tbl_units` where variantid='".$row1['id']."' ";
										$result_units = mysqli_query($con,$sql_units);
										$unitnames="";
										while($row_units = mysqli_fetch_array($result_units))
										{
											$unitnames .="".$row_units['unitname'].", ";
										}
										echo rtrim($unitnames,", ");										
									echo '</td> </tr>';
								
								
							}
							?>						
							</tbody>
							</table>
						</div>
					</div>
            
				
                    
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
$(document).ready( function() {
	//$('#sample_2').destroy();
     var table = $('#sample_2').dataTable({	
		 destroy: true,
		stateSave: true,
		aoColumnDefs: [{ 
			  'bSortable': false, 
			  'aTargets': [ 1 ] // <-- gets last column and turns off sorting
			 }]
		});
	     $('input[type=search]').on("input", function() {
			table.fnFilter('');
		});		
	}); 

</script>
</body>
<!-- END BODY -->
</html>