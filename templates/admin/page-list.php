<!-- BEGIN HEADER -->
<?php header("Cache-Control: no-store, must-revalidate, max-age=0");
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
include "../includes/grid_header.php";

include "../includes/sectionManage.php";
$sectionObj = new sectionManager($con, $conmain);
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Settings"; $activeMenu = "All-Pages-List";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<h3 class="page-title">Pages</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li><i class="fa fa-home"></i>
					<a href="#">Pages</a></li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
						
							<div class="caption">Page Listing</div>
							
							     <?php
                                if ($ischecked_add==1) 
                                {
                                ?>
								<a href="page-add.php" class="btn btn-sm btn-default pull-right mt5">Add Page</a>
							     <?php } ?>
							
                            <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_2">
								<thead>
									<tr>
										<th>
									 Page Name
								</th>
								<th>
									Php Page Name
								</th>
								<th>
									Active Status
								</th>
								<th>
                                  Action
                                </th>
										
									</tr>
								</thead>
							<tbody>
				   <?php
					$result = $sectionObj->getAllPagesDetails();
					while($row = mysqli_fetch_array($result))
					{	
					?>						
								<tr class="odd gradeX">
								<td>
								
						<?php if ($ischecked_edit==1) 
                         {
                        ?>
                              <a href="page-update.php?page_id=<?=$row['id'];?>" > <?=$row['page_name'];?> </a>
                        <?php
                        }
                        else
                        {
                        ?>
                            <?php echo '-';?>
                        <?php } ?>  
								</td>
								<td>
								  <?=$row['php_page_name'];?>
								</td>
								<td>
								  <?=$row['page_active_status'];?>
								</td>
								 <td>

						<?php if ($ischecked_edit==1) 
                         {
                        ?>                              
                               <a title="Edit" href="page-update.php?page_id=<?=$row['id'];?>" class="btn btn-xs btn-primary "><span class="glyphicon glyphicon-edit"></span></a>
                        <?php
                        }
                        else
                        {
                        ?>
                            <?php  echo '<a title="Can Not Edit This Record" href="#" class="btn btn-xs btn-primary" disabled><span class="glyphicon glyphicon-edit"></span></a>';?>
                        <?php } ?>   		
						<?php if ($ischecked_delete==1) 
                         {
                        ?>
                               <a title="Delete" href="deleteManager.php?del_page_id=<?=$row['id'];?>" onclick="return confirm('Are you sure you want to Delete This Page?')" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
                        <?php
                        }
                        else
                        {
                        ?>
                            <?php echo '<a title="Can Not Delete This Record" onclick="#" class="btn btn-xs btn-danger" disabled><span class="glyphicon glyphicon-trash"></span></a>';?>
                        <?php } ?>             
                                    
								</td>
								</tr>
				<?php	
				}
				?>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>