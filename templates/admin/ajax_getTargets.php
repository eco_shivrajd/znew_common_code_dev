<?php 
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include ("../../includes/config.php");
include "../includes/userManage.php";
include "../includes/targetManage.php";
$targetObj 	= 	new targetManager($con,$conmain);
$targets=$targetObj->get_sp_targets($_POST['dropdownSalesPerson']);
if($targets!=0){
	$rowsregionwise[0] = array(
    "title" => "Sale",
   "value" => $targets[0]['saleruppes']
     );
	 $rowsregionwise[1] = array(
    "title" => "Remaning",
   "value" => $targets[0]['remain_ruppes']
     );
	 $rowsregionwise1[0] = array(
    "title" => "Sale",
	"value" => $targets[0]['saleweight']
     );
	 $rowsregionwise1[1] = array(
    "title" => "Remaning",
   "value" => $targets[0]['remain_weight']
     );
}
  $txtnmmm="Target In Ruppes";
  $regionwisetrend= json_encode($rowsregionwise);
  $regionwisetrend1= json_encode($rowsregionwise1);  
?>
<style>
#chartdiv5 {
	width		: 100%;
	height		: 500px;
	font-size	: 11px;
}
#chartdivweight {
	width		: 100%;
	height		: 500px;
	font-size	: 11px;
}				
</style>
<?php if($targets!=0){ ?>
<div class="col-md-6">
	<!--<div class="portlet box blue-steel">-->
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-bar-chart-o"></i>
				Target:<?php echo $targets[0]['target_in_rs']." Rs ";?>		
			</div>
		</div>
		<div class="portlet-body">
				<div id="chartdiv5" 
					style="width: 100%; height: 300px; background-color: #FFFFFF;" >
				</div>
		</div>
	<!--</div>-->
</div>
<?php if($targets[0]['target_in_wt']>0){ ?>
<div class="col-md-6">
	<!--<div class="portlet box blue-steel">-->
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-bar-chart-o"></i>
				Target:<?php echo $targets[0]['target_in_wt']." Kg ";?>			
			</div>
		</div>
		<div class="portlet-body">
			<div id="chartdivweight" 
				style="width: 100%; height: 300px; background-color: #FFFFFF;" >
			</div>
		</div>
	<!--</div>-->
</div>
<?php } 
}else{ ?>
<div class="col-md-12">
	<!-- BEGIN PORTLET-->
	<!--<div class="portlet box blue-steel">-->
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-bar-chart-o"></i>
				 No Targets Available!
			</div>
		</div>
	<!--</div>-->
	<!-- END PORTLET-->
</div>
<?php } ?>
<div class="clearfix"></div>
<script type="text/javascript">
	var chart = AmCharts.makeChart( "chartdiv5", {
  "type": "pie",
  "theme": "light",
  "dataProvider": <?php echo $regionwisetrend;?>,
  "titleField": "title",
  "valueField": "value",
  "labelRadius": 5,
  "radius": "42%",
  "innerRadius": "60%",
  "labelText": "[[title]]",
  "export": {
    "enabled": true
  }
} );
var chart = AmCharts.makeChart( "chartdivweight", {
  "type": "pie",
  "theme": "light",
  "dataProvider": <?php echo $regionwisetrend1;?>,
  "titleField": "title",
  "valueField": "value",
  "labelRadius": 5,
  "radius": "42%",
  "innerRadius": "60%",
  "labelText": "[[title]]",
  "export": {
    "enabled": true
  }
} );
</script>	

