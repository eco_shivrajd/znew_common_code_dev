<?php
include "../includes/grid_header.php";
include "../includes/reportManage.php";
include "../includes/userManage.php";
$reportObj = new reportManage($con, $conmain);
$userObj = new userManager($con, $conmain);
?>
<!-- END HEADER -->
<style>
    .form-horizontal .control-label {
        text-align: left;
    }
</style>

</head>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLEAPIKEY; ?>&callback=initMap" type="text/javascript"></script>
<!-- END HEAD -->
<body class="page-header-fixed page-quick-sidebar-over-content ">

    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "Orders";
        $activeMenu = "CUSTOrders";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->

        <div class="page-content-wrapper">
            <div class="page-content">			
                <h3 class="page-title">
                    Manage Orders 
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="#">Manage Orders </a>
                        </li>
                    </ul>				
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">
                                    Manage Orders  
                                </div>
                                <div class="clearfix"></div>
                            </div>						
                            <div class="portlet-body">
                                <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">
                                    <div class="form-group">
                                        <label class="col-md-3">Order Status:</label>
                                        <div class="col-md-4">
                                            <select name="order_status" id="order_status"  data-parsley-trigger="change" class="form-control">
                                                <option value="">-Select-</option>
                                                <option value="1" selected>Received</option>
                                                <option value="2" >Assigned To Delivery Person </option>
                                                <option value="3" >Delivered</option>	
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->	

                                    <div class="form-group" id="divDaily">
                                        <label class="col-md-3">Select Order Date:</label>
                                        <div class="col-md-4">
                                            <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                                                <input type="text" class="form-control" data-date="<?php echo date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years" name="frmdate" id="frmdate" value="" autocomplete="off">
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                            <!-- /input-group -->								 
                                        </div>
                                    </div><!-- /.form-group -->	
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-3">
                                            <button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>									
                                            <button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnReset();">Reset</button>
                                        </div>
                                    </div><!-- /.form-group -->
                                </form>
                                <div>
                                    <button type="button" class="btn btn-success" id="statusbtn" name="statusbtn" data-toggle="modal">Send to Production & Assign for delivery</button>
                                    <button style="display:none" type="button" class="btn btn-success" id="statusDbtn" name="statusDbtn" data-toggle="modal">Mark As Delivered</button>
                                </div><br>
                                <div id="order_list">
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->

            <!-- END QUICK SIDEBAR -->
        </div>

        <!-- END CONTAINER -->
        <div class="modal fade" id="view_invoice" role="dialog">
            <div class="modal-dialog" style="width: 980px !important;">    
                <!-- Modal content-->
                <div class="modal-content" id="view_invoice_content">      
                </div>      
            </div>
        </div>
        <div class="modal fade" id="order_details" role="dialog">
            <div class="modal-dialog" style="width: 880px !important;">
                <!-- Modal content-->
                <div class="modal-content" id="order_details_content">
                </div>
            </div>
        </div>

        <div class="modal fade" id="googleMapPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content" id="model_content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                    </div>
                    <div class="modal-body" style="padding-bottom: 5px !important;"> 
                        <div id="map" style="width: 100%; height: 500px;"></div> 
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="assign_delivery" role="dialog" style="height:auto;">
            <div class="modal-dialog">    
                <!-- Modal content-->
                <div class="modal-content"  >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Send to Production & Assign for Delivery</h4>
                    </div>
                    <div class="modal-body" style="padding-bottom: 5px !important;">
                        <form class="form-horizontal">

                            <div class="form-group">
                                <label class="col-md-6">Assignment Date:<span class="mandatory">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group date date-picker2" data-date-format="dd-mm-yyyy">
                                        <input type="text" name="del_assigned_date" id="del_assigned_date" class="form-control" placeholder="Assignment Date">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label class="col-md-6" style="padding-top:5px">Select Delivery Person:<span class="mandatory">*</span></label>

<?php $user_result = $userObj->getAllLocalUser('DeliveryPerson'); ?>		
                                <div class="col-md-6" id="divdpDropdown">
                                    <select name="selectdp" id="selectdp" class="form-control">						
                                        <?php while ($row_user = mysqli_fetch_assoc($user_result)) { ?>									
                                            <option value="<?= $row_user['id']; ?>"><?= $row_user['firstname']; ?></option>
<?php } ?>
                                    </select>
                                </div>
                            </div><!-- /.form-group -->
                            <div class="clearfix"></div>

                            <div class="form-group">
                                <div class="col-md-offset-5 col-md-9">
                                    <input type="hidden" name="orderids" id="orderids">
                                    <button type="button" name="btn_assigndelivery" id="btn_assigndelivery" class="btn btn-primary" data-dismiss="modal">Assign</button>
                                </div>
                            </div>
                        </form>
                    </div>	
                </div>
            </div>
        </div>
        
         <div class="modal fade" id="reassign_delivery" role="dialog" style="height:auto;">
            <div class="modal-dialog">    
                <!-- Modal content-->
                <div class="modal-content"  >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Send to Production & Assign for Delivery</h4>
                    </div>
                    <div class="modal-body" style="padding-bottom: 5px !important;">
                        <form class="form-horizontal">

                            <div class="form-group">
                                <label class="col-md-6">Assignment Date:<span class="mandatory">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group date date-picker3" data-date-format="dd-mm-yyyy">
                                        <input type="text" name="del_assigned_date1" id="del_assigned_date1" class="form-control" placeholder="Assignment Date">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label class="col-md-6" style="padding-top:5px">Select Delivery Person:<span class="mandatory">*</span></label>

<?php $user_result = $userObj->getAllLocalUser('DeliveryPerson'); ?>		
                                <div class="col-md-6" id="divdpDropdown">
                                    <select name="selectdp1" id="selectdp1" class="form-control">						
                                        <?php while ($row_user = mysqli_fetch_assoc($user_result)) { ?>									
                                            <option value="<?= $row_user['id']; ?>" ><?= $row_user['firstname']; ?></option>
<?php } ?>
                                    </select>
                                </div>
                            </div><!-- /.form-group -->
                             <div class="form-group" id="reassign_time_div">
                                <label class="col-md-6">Time Remain To Re-assign:<span class="mandatory">*</span></label>
                                <div class="col-md-6">
                                   <label><span id="timeremain" name="timeremain"></span></label>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group">
                                <div class="col-md-offset-5 col-md-9">
                                    <input type="hidden" name="orderids1" id="orderids1">
                                   
                                    <button type="button" name="btn_reassigndelivery" id="btn_reassigndelivery" class="btn btn-primary" data-dismiss="modal">Re-Assign</button>
                                </div>
                            </div>
                        </form>
                    </div>	
                </div>
            </div>
        </div>
        


        <!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php" ?>
        <!-- END FOOTER -->
        <script>
            $(document).ready(function () {
                $("#statusbtn").hide();
                $("#select_th").removeAttr("class");
                $("#main_th th").removeAttr("class");
                if ($.fn.dataTable.isDataTable('#sample_2')) {
                    table = $('#sample_2').DataTable();
                    table.destroy();
                    table = $('#sample_2').DataTable({
                        "aaSorting": [],
                    });
                }
                ShowReport();
            });

            $('.date-picker2').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "auto",
                startDate: 'd',
                endDate: '+3m',
                autoclose: true
            });
            $('.date-picker1').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "auto",
                endDate: "<?php echo date('d-m-Y'); ?>",
                autoclose: true
            });
           
            $("#statusbtn").click(function () {
                var void1 = [];
                $('input[name="select_all[]"]:checked').each(function () {
                    void1.push(this.value);
                });
                var energy = void1.join();
                if (energy != '') {
                    $("#assign_delivery").modal('show');
                    $("#orderids").val(energy);
                } else {
                    alert("Please select minimum one order");
                    //location.reload();
                }

            });
            function ShowReport() {
                //alert("sdfsdf");
                var order_status = $('#order_status').val();               
                if (order_status == '1') {
                    $("#statusbtn").show();
                } else {
                    $("#statusbtn").hide();
                }
                var url = "ajax_show_customer_orders.php";

                var data = $('#frmsearch').serialize();

                jQuery.ajax({
                    url: url,
                    method: 'POST',
                    data: data,
                    async: false
                }).done(function (response) {
                    $('#order_list').html(response);
                    var table = $('#sample_2').dataTable();
                    table.fnFilter('');
                    $("#select_th").removeAttr("class");
                }).fail(function () { });
                return false;
            }
            function checktotalchecked(obj) {
                $("#select_th").removeAttr("class");
                if ($(obj).is(':checked') == true)
                {
                    var checkbox_count = ($('input:checkbox').length) - 1;
                    var check_count = ($('input:checkbox:checked').length);
                    if (obj.value == '') {
                        $('input:checkbox').attr('checked', true);
                    } else if (checkbox_count == check_count)
                        $('input:checkbox').attr('checked', true);
                } else {
                    $('#select_all').attr('checked', false);
                    if (obj.value == '')
                        $('input:checkbox').attr('checked', false);
                }
            }
            function fnReset() {
                location.reload();
            }


            $("#select_all").click(function () {
                $('input:checkbox').prop('checked', this.checked);
            });


            function showOrderAssignment(orderstatusid, order_id) {
                if (orderstatusid == '0') {
                    $('#assign_delivery').modal('show');
                    $('#orderids').val(order_id);
                }
            }
            function showOrderAssignment1(orderstatusid, order_id,assigned_date,datediffe,dp_id) {
                console.log(assigned_date);
                console.log("new");
                console.log(datediffe);
                if (orderstatusid == '1') {
                    $('#reassign_delivery').modal('show');
                    $('#orderids1').val(order_id);  
                    
                    if(datediffe<60){
                        $('.date-picker3').datepicker({
                            rtl: Metronic.isRTL(),
                            orientation: "auto",
                            startDate: assigned_date,
                            endDate: '+3m',
                            date:assigned_date,
                            autoclose: true
                        });
                        $('#del_assigned_date1').val(assigned_date);
                        $('#del_assigned_date1').prop('disabled', true);
                        $( "#del_assigned_date1" ).datepicker( "option", "disabled", true );

                        $('#selectdp1').val(dp_id);
                        $('#selectdp1').prop('disabled', true);
                        $('#selectdp1').prop('disabled', true);
                        $('#btn_reassigndelivery').prop('disabled', true);
                        var hours_remain=60-datediffe;
                        hours_remain=hours_remain.toFixed(2);
                        $('#timeremain').html(hours_remain+' Hours');
                    }else{
                       // alert("else");
                        $('.date-picker3').datepicker({
                            rtl: Metronic.isRTL(),
                            orientation: "auto",
                            startDate: <?php echo date('d-m-Y'); ?>,
                            endDate: '+3m',                            
                            autoclose: true
                        });
                        //$('#del_assigned_date1').val(assigned_date);
                        $('#del_assigned_date1').prop('disabled', false);
                        $( "#del_assigned_date1" ).datepicker( "option", "disabled", false );
                        
                        $('#selectdp1').val(dp_id);   
                        $('#selectdp1').prop('disabled', false);
                        $('#btn_reassigndelivery').prop('disabled', false);
                         
                        $('#reassign_time_div').hide();
                        
                    }
                }
            }
            function assign_delivery(selectdp,orderids,del_assigned_date){
                 if (del_assigned_date != '' ) {
                    var url = 'update_customer_order_status.php?flag=load&selectdp=' + selectdp + '&del_assigned_date=' + del_assigned_date + '&orderids=' + orderids;
                    //	alert(url);
                    $.ajax({
                        url: url,
                        datatype: "JSON",
                        contentType: "application/json",

                        error: function (data) {
                            console.log("error:" + data)
                        },
                        success: function (data) {
                            console.log(data);
                            if (data > 0) {
                                alert("The order is assigned to Delivery Person. ");
                                location.reload();
                            } else {
                                alert("Not updated.");
                            }
                        }
                    });
                } else {
                    alert("Please select date");
                    return false;
                } 
            }
            $('#btn_reassigndelivery').click(function(){
                var selectdp = $('#selectdp1').val();
                var orderids = $("#orderids1").val();
                var del_assigned_date = $("#del_assigned_date1").val();
                assign_delivery(selectdp,orderids,del_assigned_date);
            });
            $("#btn_assigndelivery").click(function () {
                var selectdp = $('#selectdp').val();
                var orderids = $("#orderids").val();
                var del_assigned_date = $("#del_assigned_date").val();
               assign_delivery(selectdp,orderids,del_assigned_date);
            });
            

          
            function showOrderDetails(id, prod_id, prod_var_id) {
                var url = "customer_order_details_popup.php";
                jQuery.ajax({
                    url: url,
                    method: 'POST',
                    data: 'order_details_id=' + id + '&prod_id=' + prod_id + '&prod_var_id=' + prod_var_id,
                    async: false
                }).done(function (response) {
                    $('#order_details_content').html(response);
                    $('#order_details').modal('show');
                }).fail(function () { });
                return false;
            }

            function OrderDetailsPrint() {
                var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
                var divContents = '<style>\body {\
                        font-size: 12px;}\
                th {text-align: left;}\
                .printHeading { line-height: 18px;  padding: 10px 0;  font-size: 18px; }\
                table { border-collapse: collapse;  \
                        font-size: 12px; }\
                table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
                body { font-family: "Open Sans", sans-serif;\
                background-color:#fff;\
                font-size: 15px;\
                direction: ltr;}</style>' + $("#divOrderPrintArea").html();
                if (isIE == true) {
                    var printWindow = window.open('', '', 'height=400,width=800');
                    printWindow.document.write(divContents);
                    printWindow.focus();
                    printWindow.document.execCommand("print", false, null);
                } else {
                    $('<iframe>', {
                        name: 'myiframe',
                        class: 'printFrame'
                    }).appendTo('body').contents().find('body').html(divContents);
                    window.frames['myiframe'].focus();
                    window.frames['myiframe'].print();
                    setTimeout(
                            function ()
                            {
                                $(".printFrame").remove();
                            }, 1000);
                }
            }
            function takeprint11() {
                var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
                var divContents = '<style>\body {\
                        font-size: 12px;}\
                th {text-align: left;}\
                .printHeading { line-height: 18px;  padding: 10px 0;  font-size: 18px; }\
                table { border-collapse: collapse;  \
                        font-size: 12px; }\
                table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
                body { font-family: "Open Sans", sans-serif;\
                background-color:#fff;\
                font-size: 15px;\
                direction: ltr;}</style>' + $("#divPrintArea").html();
                if (isIE == true) {
                    var printWindow = window.open('', '', 'height=400,width=800');
                    printWindow.document.write(divContents);
                    printWindow.focus();
                    printWindow.document.execCommand("print", false, null);
                } else {
                    $('<iframe>', {
                        name: 'myiframe',
                        class: 'printFrame'
                    }).appendTo('body').contents().find('body').html(divContents);
                    window.frames['myiframe'].focus();
                    window.frames['myiframe'].print();
                    setTimeout(
                            function ()
                            {
                                $(".printFrame").remove();
                            }, 1000);
                }
            }
            function takeprint_invoice() {
                var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
                var divContents = '<style>\
                .darkgreen{	background-color:#364622; color:#fff!important; font-size:24px; font-weight:600;}\
                .fentgreen1{background-color:#b0b29c;color:#4a5036;	font-size:12px;}\
                .fentgreen{	background-color:#b0b29c;	color:#4a5036;}\
                .font-big{	font-size:20px;	font-weight:600;	color:#364622;}\
                .font-big1{	font-size:18px;	font-weight:600;	color:#364622;}\
                .table-bordered-popup {    border: 1px solid #364622;}\
                .table-bordered-popup > tbody > tr > td, .table-bordered-popup > tbody > tr > th, .table-bordered-popup > thead > tr > td, .table-bordered-popup > thead > tr > th {\
                        border: 1px solid #364622;	color:#4a5036;}\
                .blue{	color:#010057;}\
                .blue1{	color:#574960;	font-size:16px;}\
                .buyer_section{	color:#574960;	font-size:14px;}\
                .pad-5{	padding-left:10px;}\
                .pad-40{	padding-left:40px;}\
                .np{	padding-left:0px;	padding-right:0px;}\
                .bg{	background-image:url(../../assets/global/img/watermark.png); background-repeat:no-repeat;\
                 background-size: 200px 200px;}\
                </style>' + $("#divPrintArea").html();
                if (isIE == true) {
                    var printWindow = window.open('', '', 'height=400,width=800');
                    printWindow.document.write(divContents);
                    printWindow.focus();
                    printWindow.document.execCommand("print", false, null);
                } else {
                    $('<iframe>', {
                        name: 'myiframe',
                        class: 'printFrame'
                    }).appendTo('body').contents().find('body').html(divContents);
                    window.frames['myiframe'].focus();
                    window.frames['myiframe'].print();
                    setTimeout(
                            function ()
                            {
                                $(".printFrame").remove();
                            }, 1000);
                }
            }
            var lat = 0;
            var lng = 0;

            $('#googleMapPopup').on('shown.bs.modal', function (e) {

                var latlng = new google.maps.LatLng(lat, lng);
                var map = new google.maps.Map(document.getElementById('map'), {
                    center: latlng,
                    zoom: 17
                });
                var marker = new google.maps.Marker({
                    map: map,
                    position: latlng,
                    draggable: false,
                    //anchorPoint: new google.maps.Point(0, -29)
                });
            });
            function showGoogleMap(getlat, getlng) {

                lat = getlat;
                lng = getlng;
                $('#googleMapPopup').modal('show');
            }

            function fnShowStockist(id) {
                var url = "getStockistDropDown.php?cat_id=" + id.value;
                CallAJAX(url, "divStocklistDropdown");
                document.getElementById("divsalespersonDropdown").innerHTML = "<select name='dropdownSalesPerson' id='dropdownSalesPerson' class='form-control'><option value=''>-Select-</option></select>";
            }
            function fnShowSalesperson(id) {
                var url = "getSalesPersonDropDown.php?cat_id=" + id.value;
                CallAJAX(url, "divsalespersonDropdown");
            }

        </script>
        <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
</html>