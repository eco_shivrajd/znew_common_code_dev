DELIMITER $$
CREATE TRIGGER tbl_stock_management_hist 
BEFORE UPDATE ON tbl_stock_management 
FOR EACH ROW
BEGIN 
INSERT INTO tbl_stock_management_hist
    SET product_varient_id = OLD.product_varient_id,
        product_id = OLD.product_id; 
END$$
DELIMITER ;

// Before Trigger
DELIMITER $$
CREATE TRIGGER tbl_stock_management_hist 
BEFORE INSERT ON tbl_stock_management 
FOR EACH ROW
BEGIN 
INSERT INTO tbl_stock_management_hist
    SET 
        product_id = NEW.product_id,
        product_varient_id = NEW.product_varient_id,
        added_by_userid = NEW.added_by_userid,
        createdon = NEW.createdon,
        category_name = NEW.category_name,
        product_name = NEW.product_name,
        producthsn = NEW.producthsn,
        product_variant = NEW.product_variant,
        product_quantity = NEW.product_quantity
        ; 
END$$
DELIMITER ;

//After Trigger

DELIMITER $$
CREATE TRIGGER tbl_stock_management_hist 
AFTER INSERT ON tbl_stock_management 
FOR EACH ROW
BEGIN 
INSERT INTO tbl_stock_management_hist
    SET stock_id = NEW.id,
        product_id = NEW.product_id,
        product_varient_id = NEW.product_varient_id,
        added_by_userid = NEW.added_by_userid,
        createdon = NEW.createdon,
        category_name = NEW.category_name,
        product_name = NEW.product_name,
        producthsn = NEW.producthsn,
        product_variant = NEW.product_variant,
        product_quantity = NEW.product_quantity
        ; 
END$$
DELIMITER ;