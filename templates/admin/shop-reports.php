<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
   include "../includes/orderManage.php";
   $orderObj 	= 	new orderManage($con,$conmain);?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
   <div class="clearfix"> </div>
   <!-- BEGIN CONTAINER -->
   <div class="page-container">
   <!-- BEGIN SIDEBAR -->
   <?php
      $activeMainMenu = "Reports"; $activeMenu = "Shop Reports";
      include "../includes/sidebar.php";
      ?>
   <!-- END SIDEBAR -->
   <!-- BEGIN CONTENT -->
   <div class="page-content-wrapper">
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
         <h3 class="page-title">
            Shop Reports
         </h3>
         <div class="page-bar">
            <ul class="page-breadcrumb">
               <li>
                  <i class="fa fa-home"></i>
                  <a href="#">Shop Reports</a>	
               </li>
            </ul>
         </div>
         <!-- END PAGE HEADER-->
         <!-- BEGIN PAGE CONTENT-->
         <div class="row">
            <div class="col-md-12">
               <div class="row">
                  <div class="col-md-12">
                     <div class="portlet box blue-steel">
                        <div class="portlet-title">
                           <div class="caption">Shop Reports</div>
                           <div class="clearfix"></div>
                        </div>
                        <div class="portlet-body">
                           <ul class="nav nav-tabs">
                              <li class="active"><a data-toggle="tab" href="#home">Shop Summary Report</a></li>
                              <li><a data-toggle="tab" href="#menu1">Shop Added Summary Report</a></li>
                              <li><a data-toggle="tab" href="#menu2">Shop Location</a></li>
                           </ul>
                           <div class="tab-content">
                              <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">
                                 <div id="state-div">
                                    <div class="form-group" >
                                       <label class="col-md-3">State:<span class="mandatory">*</span></label>
                                       <div class="col-md-4">
                                          <select name="dropdownState" id="dropdownState" class="form-control" onchange="fnShowCity(this.value)">
                                               <option value="">-Select-</option>
											  <?php
                                                $sql = "SELECT id,name FROM tbl_state where country_id=101 order by name";
                                                $result = mysqli_query($con, $sql);
                                                while ($row = mysqli_fetch_array($result)) {
                                                    $cat_id = $row['id'];
                                                    echo "<option value='$cat_id'>" . $row['name'] . "</option>";
                                                }
                                                ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="form-group" id="city_div" style="display:none;">
                                       <label class="col-md-3">City:</label>
                                       <div class="col-md-4" id="div_select_city">
                                          <select name="city" id="city" data-parsley-trigger="change" class="form-control" >
                                             <option  value="">-Select-</option>
                                          </select>
                                       </div>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group" id="area_div" style="display:none;">
                                       <label class="col-md-3">Area:</label>
                                       <div class="col-md-4" id="div_select_area">
                                          <select name="area" id="area"
                                             data-parsley-trigger="change"
                                             class="form-control">
                                             <option  value="">-Select-</option>
                                          </select>
                                       </div>
                                    </div>
                                    <!-- /.form-group --> 						
                                    <div class="form-group" id="subarea_div" style="display:none;">
                                       <label class="col-md-3">Subarea:</label>
                                       <div class="col-md-4" id="div_select_subarea">
                                          <select name="subarea" id="subarea" data-parsley-trigger="change" class="form-control">
                                             <option  value="">-Select-</option>
                                          </select>
                                       </div>
                                    </div>
                                    <!-- /.form-group --> 
                                 </div>
                                 <div class="form-group" id="shop_div" >
                                    <label class="col-md-3">Shop:<span class="mandatory">*</span></label>
                                    <div class="col-md-4" id="div_select_shop">
                                       <select name="dropdownshops" id="dropdownshops" data-parsley-trigger="change" class="form-control" onchange="getordersearchdata()">
                                          <option  value="">-Select-</option>
                                       </select>
                                    </div>
                                 </div>
                                 <!-- /.form-group --> 
                                 <div class="form-group" id="date-div" style="display: none;">
                                    <label class="col-md-3">Date Options:</label>
                                    <div class="col-sm-4">
                                       <select class="form-control" name="selTest" id="selTest" onChange="fnSelectionBoxTest()">
                                          <option value='5' <?php if($_REQUEST['selTest']=="5")echo 'selected';?>>All</option>
                                          <option value="4" <?php if($_REQUEST['selTest']=="4")echo 'selected';?>>Today</option>
                                          <option value='1' <?php if($_REQUEST['selTest']=="1")echo 'selected';?>>Weekly</option>
                                          <option value='2' <?php if($_REQUEST['selTest']=="2")echo 'selected';?>>Current month</option>
                                          <option value='3' <?php if($_REQUEST['selTest']=="3")echo 'selected';?>>From specific date</option>
                                       </select>
                                       <input type="hidden" name="hdnSelrange" id="hdnSelrange">
                                    </div>
                                 </div>
                                 <div class="form-group" id="date-show" style="display:none;">
                                    <label class="col-md-3">Date:</label>
                                    <div class="col-md-8 nopadl">
                                       <div class="col-md-1" class="nopadl" style="margin-right:-17px;">
                                          <label class="nopadl" style="padding-top:5px;">From:</label>
                                       </div>
                                       <div class="col-md-3">
                                          <div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                                             <input type="text" class="form-control" name="frmdate" id="frmdate" value="<?php echo $frmdate;?>" readonly >
                                             <span class="input-group-btn">
                                             <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                             </span>
                                          </div>
                                       </div>
                                       <div class="col-md-1" class="nopadl" style="margin-right:-35px;margin-left:-8px;">
                                          <label class="nopadl" style="padding-top:5px">To:</label>
                                       </div>
                                       <div class="col-md-3">
                                          <div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                                             <input type="text" class="form-control" name="todate" id="todate" value="<?php echo $todate;?>" readonly >
                                             <span class="input-group-btn">
                                             <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div id="home_tab" >
                                    <input type="hidden" id="order" name="order" value="asc">
                                    <input type="hidden" id="sort_complete" name="sort_complete" value="">
                                    <input type="hidden" id="page" name="page" value="">
                                    <input type="hidden" id="per_page" name="per_page" value=""> 	
                                    <input type="hidden" id="actionType" name="actionType" value=""> 
                                    <input type="hidden" id="search" name="search" value="">
                                    <div class="form-group">
                                       <div class="col-md-4 col-md-offset-3">
                                          <button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>
                                          <button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnChangeReportType();">Reset</button>
                                       </div>
                                    </div>
                                 </div>
                                 <div id="menu1_tab" style="display: none;">
                                    <input type="hidden" id="order" name="order" value="asc">
                                    <input type="hidden" id="sort_complete" name="sort_complete" value="">
                                    <input type="hidden" id="page" name="page" value="">
                                    <input type="hidden" id="per_page" name="per_page" value=""> 	
                                    <input type="hidden" id="actionType" name="actionType" value=""> 
                                    <input type="hidden" id="search" name="search" value="">
                                    <div class="form-group">
                                       <div class="col-md-4 col-md-offset-3">
                                          <button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport1();">Search</button>
                                          <button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnChangeReportType();">Reset</button>
                                       </div>
                                    </div>
                                 </div>
                                 <div id="menu2_tab" style="display: none;">
                                    <div class="form-group">
                                       <div class="col-md-4 col-md-offset-3">
                                          <button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport2();">Search</button>
                                          <button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnChangeReportType();">Reset</button>
                                       </div>
                                    </div>
                                 </div>
                                 <div id="home" class="tab-pane fade in active">
                                 </div>
                                 <div id="menu1" class="tab-pane fade">
                                 </div>
                                 <div id="menu2" class="tab-pane fade">
                                 </div>
                              </form>
                           </div>
                        </div>
                        <!-- END PAGE CONTENT-->
                     </div>
                     <div class="clearfix"></div>
                     <div id="home_1" >
                        <div class="portlet box blue-steel" id="portlet_steel" style=" display: none;">
                           <div class="portlet-title">
                              <div class="caption"><i class="icon-puzzle"></i>Shop Summary Report </div>
                              <button type="button" name="btnExcel" id="btnExcel" onclick="report_download();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
                              &nbsp;
                              <button type="button" name="btnPrint" id="btnPrint"  class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
                           </div>
                           <div class="portlet-body" id="order_summary_details">
                           </div>
                        </div>
                     </div>
                     <div id="menu_1" style=" display: none;">
                        <div class="portlet box blue-steel">
                           <div class="portlet-title">
                              <div class="caption"><i class="icon-puzzle"></i>Shop Added Summary Report</div>
                              <button type="button" name="btnExcel" id="btnExcel" onclick="report_download1();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
                              &nbsp;
                              <button type="button" name="btnPrint1" id="btnPrint1" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
                           </div>
                           <div class="portlet-body" id="order_summary_details1">
                           </div>
                        </div>
                     </div>
                     <div id="menu_2" style=" display: none;">
                        <div class="portlet box blue-steel">
                           <div class="portlet-title">
                              <div class="caption"><i class="icon-puzzle"></i>Shop Location</div>
                           </div>
                           <div class="portlet-body" id="order_summary_details2">
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- END CONTENT -->
                  <!-- BEGIN QUICK SIDEBAR -->
                  <!-- END QUICK SIDEBAR -->
               </div>
               <!-- END CONTAINER -->
            </div>
         </div>
      </div>
   </div>
   <div style="display:none;" class="modal-backdrop fade in"></div>
   <div id="print_div"  style="display:none;"></div>
   <div id="table_heading"  style="display:none;"><?=$user_data['firstname'];?>'s Shopwise <?=$report_title;?></div>
   <form action="../includes/exportToExcel.php" method="post" name="export_excel" id="export_excel">
      <input type="hidden" name="export_data" id="export_data">
   </form>
   <!-- BEGIN FOOTER -->
    <div class="modal fade " id="order_details" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 880px !important;">
            <!-- Modal content-->
            <div class="modal-content" id="order_details_content">
            </div>
        </div>
    </div>
   <?php include "../includes/grid_footer.php"?>
   <!-- END FOOTER -->
   <script>  
      function CallAJAX(url,assignDivName) {
      	if (window.XMLHttpRequest)
      	{
      		var xmlhttp=new XMLHttpRequest();
      	} else {
      		var xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      	}
      	xmlhttp.onreadystatechange=function() {
      		if (xmlhttp.readyState==4 && xmlhttp.status==200)
      		{
      			document.getElementById("" + assignDivName +"").innerHTML	=	xmlhttp.responseText;
      		}
      	}
      	xmlhttp.open("GET",url,true);
      	xmlhttp.send();	
      };
      function CallAJAX1(url,assignDivName) {
      	if (window.XMLHttpRequest)
      	{
      		var xmlhttp=new XMLHttpRequest();
      	} else {
      		var xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      	}
      	xmlhttp.onreadystatechange=function() {
      		if (xmlhttp.readyState==4 && xmlhttp.status==200)
      		{
      			document.getElementById("" + assignDivName +"").innerHTML	=	xmlhttp.responseText;
      		}
      	}
      	xmlhttp.open("GET",url,true);
      	xmlhttp.send();	
      };
      
      
          var tab_id ="#home";
      
      	$('[data-toggle="tab"]').click(function(e) {
      		$("#dropdownState").val('');
      		$("#dropdownshops").val('');
      		$("#city_div").hide();
      		$("#area_div").hide();
      		$("#subarea_div").hide();
      
      		tab_id = $(this).attr('href');
      	
      	   if (tab_id=='#menu1') {
      			$('#home_tab').hide();
      			$('#menu1_tab').show();
      			$('#menu2_tab').hide();
      			$('#state-div').hide();
      			$('#shop_div').hide();
      			$('#date-div').show();
      
      			 $('#menu_1').show();
      			 ShowReport1();
      			 $('#home_1').hide();
      			 $('#menu_2').hide();
      	   } else if(tab_id=='#home'){
      			document.getElementById("frmsearch").reset();
      			$('#home_tab').show();
      			$('#menu1_tab').hide();
      			$('#menu2_tab').hide();
      
      			$('#state-div').show();
      			$("#shop_div").show(); 	
      			$('#date-div').hide();
      			$('#date-show').hide();
      
      			$('#home_1').hide();
      			$('#menu_1').hide();
      			$('#menu_2').hide();
      	   }else if(tab_id=='#menu2') {
      			document.getElementById("frmsearch").reset();
      			$('#home_tab').hide();
      			$('#menu1_tab').hide();
      			$('#menu2_tab').show();	  
      			 $('#date-show').hide();
      			$('#state-div').show(); 
      			$('#shop_div').hide();
      			$('#date-div').hide();
      			 $('#menu_1').hide();	    
      			 $('#home_1').hide();
      			 $('#menu_2').show();
      			 ShowReport2();
      	   } else {
      	     location.reload();
      	   }
      	});	
      
      function fnShowCity(id_value) {
      	//alert(id_value);
      	//alert(tab_id);
      	$("#city_div").show();	
      	$("#area_div").hide();
      	$("#subarea_div").hide();
      	//debugger;
      	$("#div_select_city").html('<option value="">-Select-</option>');
      	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city";		
      	CallAJAX(url,"div_select_city");
      	//debugger;
      	if(tab_id=="#home") 
      	{		
      		//alert("xfgxfg");
      		//debugger;
      		getordersearchdata();
      		//debugger;
      	}	
      	
      }
      
      function FnGetSuburbDropDown(id) {
      	$("#area_div").show();
      	$("#subarea_div").hide();
      	$("#div_select_area").html('<option value="">-Select-</option>');		
      	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&function_name=FnGetSubareaDropDown";
      	CallAJAX(url,"div_select_area");
      	if (tab_id=="#home") {//alert("dfgdf");
      		getordersearchdata();
      	}	
      }//FnGetSubareaDropDown
      function FnGetSubareaDropDown(id) {	    
      	var suburb_str = $("#area").val();	
      	$("#subarea_div").show();	
      	$("#div_select_subarea").html('<option value="">-Select-</option>');
      	if (tab_id == "#home") {
      		//alert(tab_id);
      		var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea&function_name=getordersearchdata";
      	}
      	else
      	{
      		//alert(tab_id);
      	   	var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea";	
      	}
      	CallAJAX(url,"div_select_subarea");
      	if (tab_id=="#home") {//alert("dfgdf");
      		getordersearchdata();
      	}	
      }
      
      
      function getordersearchdata() {	
      	//debugger;
      	//$("#shop_div").show(); 	
      	$("#div_select_shop").html('<option value="">-Select-</option>');
      	if($('#dropdownState').val()!=''){var state=$('#dropdownState').val();}else{var state='0';}
      	if($('#city').val()!=null){var city=$('#city').val();}else{var city='';}
      	if($('#area').val()!=null){var area=$('#area').val();}else{var area='';}
		//alert(area);alert("ddf");
      	if($('#subarea').val()!=null){var subarea=$('#subarea').val();}else{var subarea='';}
      	//debugger;
      	var url = "getShopDropdownByAddress.php?state_id="+state + "&city_id="+city + "&suburb_id="+area + "&subarea_id="+subarea;
      	//debugger;
      	CallAJAX1(url,"div_select_shop");
      }
      
   </script>
   <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
   <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDMpdO43txdg_zyovvZm9i1tMMFIQTKTU&callback=initMap"></script> 
   <script>
      function fnChangeReportType(reportType) {
      		$("#dropdownState").val('');
      		$("#dropdownshops").val('');
      		$("#city_div").hide();
      		$("#area_div").hide();
      		$("#subarea_div").hide();
      }
      function ShowReport(page){
      	//alert("dsfsd");
      		$('#home_1').show();
      	var param = '';	
      	var selshop = $('#dropdownshops').val();	
      	param = param + 'selshop='+selshop;
      	if(selshop == ''){
      		alert('Please select Shop.');
      		$('#dropdownshops').focus();
      		$("#portlet_steel").hide();
      		return false;
      	}
      	$.ajax
      	({
      		type: "POST",
      		url: "order_summary_detail2.php",
      		data: param,
      		success: function(msg)
      		{	
      		  $("#portlet_steel").show();
      		  if(msg!=''){
      			  $("#order_summary_details").html(msg);
      		  }else{$("#order_summary_details").html("<h3 align='center' style='font-size: 13px;'>No Data Available.</h3>");}
      		  
      		}
      	});
      }
      
      
      function ShowReport1(page){
      		//alert("dsfsd");
      		var param = '';	
      		var selprod = $('#selprod').val();
      		param = param + 'selprod='+selprod;
      		var selTest = $('#selTest').val();
      		param = param + '&selTest='+selTest;
      		if(selTest == 3)
      		{
      			var frmdate = $('#frmdate').val();
      			param = param + '&frmdate='+frmdate;
      			var todate = $('#todate').val();
      			param = param + '&todate='+todate;
      			var validation = compaire_dates(frmdate,todate);
      		}
      		//alert(param);
      	  if(selTest == 3 && frmdate != '' && todate != '' && validation == 1)
      	  {
      		  alert("'From' date should not be greater than 'To' date.");
      		  return false;
      	  }
      	  else
      	  {
      		$.ajax
      		({
      			type: "POST",
      			url: "order_summary_detail4.php",
      			data: param,
      			success: function(msg)
      			{
      			  $("#order_summary_details1").html(msg);
      			}
      		  });
      	  }
      }
      
      function ShowReport2(){
      	//alert("ShowReport2");
      	var param = '';	
      	var dropdownState = $('#dropdownState').val();
      	param = param + 'dropdownState='+dropdownState;
      	var city = $('#city').val();
      	param = param + '&city='+city;
      	var area = $('#area').val();
      	param = param + '&area='+area;
      	var subarea = $('#subarea').val();	
      	param = param + '&subarea='+subarea;	
      	//alert(dropdownState2);
      	//if(selshop == ''){
      	//	alert('Please select Shop.');
      	//	$('#dropdownshops').focus();
      	//	return false;
      	//}
      	$.ajax
      	({
      		type: "POST",
      		url: "geolocation-track-details.php",
      		data: param,
      		success: function(msg)
      		{	
      		//alert('asdas');		
      		  $("#portlet_steel").show();
      		  $("#order_summary_details2").html(msg);
      		}
      	});
      }
      function fnSelectionBoxTest()
      {
      	if($('#selTest').val() == '3')
      	{
      	   $('#date-show').show();
      	}
      	else
      	{
      		$('#date-show').hide();
      		//ShowReport(1);
      	}
      }
      function report_download() {
      	var td_rec = $("#sample_2121 td:last").html();
      	if(td_rec != 'No matching records found')
      	{
      		var divContents = $(".table-striped").html();
      		$("#print_div").html('<table id="print_table" style="text-decoration:none;">'+divContents+'</table>');	
      		var heading = $("#table_heading").html();
      		//$("#print_table tr th i").html("RS");	
      		divContents =  $("#print_div").html();
      		
      		divContents = divContents.replace(/<\/*a.*?>/gi,'');	
      		$("#export_data").val(divContents);
      		document.forms.export_excel.submit();
      	}else{
      		alert("No matching records found");
      	}
      }
      function report_download1() {
      	var td_rec = $("#sample_2 td:last").html();
      	if(td_rec != 'No matching records found')
      	{
      		var divContents = $("#sample_2").html();
      		$("#print_div").html('<table id="print_table" style="text-decoration:none;">'+divContents+'</table>');	
      		var heading = $("#table_heading").html();
      		//$("#print_table tr th i").html("RS");	
      		divContents =  $("#print_div").html();
      		
      		divContents = divContents.replace(/<\/*a.*?>/gi,'');	
      		$("#export_data").val(divContents);
      		document.forms.export_excel.submit();
      	}else{
      		alert("No matching records found");
      	}
      }
      
      
        $("#btnPrint").live("click", function () {
      	var td_rec = $("#sample_2121 td:last").html();
      	if(td_rec != 'No matching records found')
      	{
      		var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
      		
      		//var divContents = $(".table-striped").html();
      		var divContents = $("#order_summary_details").html();
      		 $("#print_div").html('<table id="print_table" style="text-decoration:none;">'+divContents+'</table>');	
      		var heading = $("#table_heading").html();
      		//$("#print_table tr th i").html("RS");	
      		divContents =  $("#print_div").html();
      		
      		//divContents = divContents.replace(/<\/*a.*?>/gi,'');	
      		$("#export_data").val(divContents); 
      		
      		$("#print_div").html(divContents);
      		$("#print_div a").removeAttr('href');
      		$("#sample_2121 tr th i").html("₹");	
      		var divContents = $("#print_div").html();
      		var printWindow = window.open('', '', 'height=400,width=800');
      		printWindow.document.write('<html><head><title>Report</title>');
      		printWindow.document.write('<style>a{text-decoration: none; color:#333333;} #sample_2121{margin:20 20 20 20px; width:700px}</style>');
      		printWindow.document.write('<link   rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css"/>');
      		printWindow.document.write('<link  rel="stylesheet" type="text/css" href="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>');
      		
      		if( navigator.userAgent.toLowerCase().indexOf('chrome') > -1 ){
      			printWindow.document.write('</head><body >');
      			printWindow.document.write(divContents);
      			printWindow.document.write('</body></html>');	
      			printWindow.focus();	
      			setTimeout(function () {
      				printWindow.print();
      				//printWindow.close();
      			}, 500);
      		}else if(isIE == true){
      			printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
      			printWindow.document.write('</head><body >');
      			printWindow.document.write(divContents);
      			printWindow.document.write('</body></html>');	
      			printWindow.focus();	
      			printWindow.document.execCommand("print", false, null);
      			//printWindow.close();
      		}
      		else{		
      			printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
      			printWindow.document.write('</head><body >');
      			printWindow.document.write(divContents);
      			printWindow.document.write('</body></html>');	
      			printWindow.focus();	
      			setTimeout(function () {				
      				printWindow.print();
      				//printWindow.close();
      			}, 100);
      		}
      	}else{
      		alert("No matching records found");
      	} 
      });
      
      
         $("#btnPrint1").live("click", function () {
      	var td_rec = $("#sample_2 td:last").html();
      	if(td_rec != 'No matching records found')
      	{
      		var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
      		
      		var divContents1 = $(".table-scrollable").html();
      		$("#print_div").html(divContents1);
      		$("#print_div a").removeAttr('href');
      		$("#sample_2 tr th i").html("");	
      		var divContents = $("#print_div").html();
      		var printWindow = window.open('', '', 'height=400,width=800');
      		printWindow.document.write('<html><head><title>Report</title>');
      		printWindow.document.write('<style>a{text-decoration: none; color:#333333;} #sample_2{margin:20 20 20 20px; width:700px}</style>');
      		printWindow.document.write('<link   rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css"/>');
      		printWindow.document.write('<link  rel="stylesheet" type="text/css" href="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>');
      		
      		if( navigator.userAgent.toLowerCase().indexOf('chrome') > -1 ){
      			printWindow.document.write('</head><body >');
      			printWindow.document.write(divContents);
      			printWindow.document.write('</body></html>');	
      			printWindow.focus();	
      			setTimeout(function () {
      				printWindow.print();
      				//printWindow.close();
      			}, 500);
      		}else if(isIE == true){
      			printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
      			printWindow.document.write('</head><body >');
      			printWindow.document.write(divContents);
      			printWindow.document.write('</body></html>');	
      			printWindow.focus();	
      			printWindow.document.execCommand("print", false, null);
      			//printWindow.close();
      		}
      		else{		
      			printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
      			printWindow.document.write('</head><body >');
      			printWindow.document.write(divContents);
      			printWindow.document.write('</body></html>');	
      			printWindow.focus();	
      			setTimeout(function () {				
      				printWindow.print();
      				//printWindow.close();
      			}, 100);
      		}
      	}else{
      		alert("No matching records found");
      	}
      });
	  //new changes for viraly
	function showShopListDetails(added_by_userid,added_date,added_username) {
		var url = "shop_details_popup.php";
		jQuery.ajax({
			url: url,
			method: 'POST',
			data: 'added_by_userid=' + added_by_userid +'&added_date='+added_date+'&added_username='+added_username,
			async: false
		}).done(function (response) {
			console.log(response);
			$('#order_details_content').html(response);
			$('#order_details').modal('show');
		}).fail(function () { });
		return false;
	}
   </script>
   <!-- END PAGE LEVEL SCRIPTS -->
   <!-- END JAVASCRIPTS -->