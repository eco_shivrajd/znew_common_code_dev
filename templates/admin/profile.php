<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);
?>
<!-- END HEADER -->
<?php 
if(isset($_POST['hidbtnsubmit']))
{
	//echo "</pre>";
   // print_r($_POST);
   // exit();
	$id	= $_SESSION[SESSION_PREFIX.'user_id'];	
	$user_type  = $_SESSION[SESSION_PREFIX.'user_type'];
       $result = mysqli_query($con, "select id,common_user_id from `tbl_user` where id = '$id'");
	$count = mysqli_fetch_array($result);
	$common_user_id=$count['common_user_id'];	
	$userObj->updateLocalUserDetails($user_type, $id);
	$userObj->updateCommonUserDetails($common_user_id);
	echo '<script>alert("Profile saved successfully.");</script>';
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include "../includes/sidebar.php"?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Profile
			</h3>
      
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								User Profile
							</div>
							
						</div>
						<div class="portlet-body">
						
			<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
			<?php
				$id=$_SESSION[SESSION_PREFIX.'user_id'];
				$user_details = $userObj->getLocalUserDetails($id);
				
				$super_stockist_name = '';
				if($_SESSION[SESSION_PREFIX.'user_type'] == 'Distributor'){
					$super_stockist = $userObj->getLocalUserDetails($user_details['external_id']);
					$super_stockist_name = $super_stockist['firstname'];
				}
			?>                       
                          
        <form class="form-horizontal" data-parsley-validate="" role="form" method="post" action="" name="updateform" id="updateform"> 
		<?php if($super_stockist_name != ''){ ?>
			<div class="form-group">
              <label class="col-md-3">Super Stockist:</label>
              <div class="col-md-4">
                <input type="text" class="form-control" value="<?php echo $super_stockist_name;?>" disabled>
              </div>
            </div><!-- /.form-group -->
		<?php } ?>
            <div class="form-group">
              <label class="col-md-3">Name:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="firstname" 
				placeholder="Enter Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"				
				class="form-control" value="<?php echo fnStringToHTML($user_details['firstname'])?>" >
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
			  <label class="col-md-3">Username:<span class="mandatory">*</span></label>

			  <div class="col-md-4">
				<input type="text" id="username"
				placeholder="Enter Username"	
				data-parsley-minlength="6"
				data-parsley-minlength-message="Username should be of minimum 6 characters without blank spaces"          
				data-parsley-maxlength="25"
				data-parsley-maxlength-message="Only 25 characters are allowed"          
				data-parsley-type-message="Please enter Username, blank spaces are not allowed"		   
				data-parsley-required-message="Please enter Username, blank spaces are not allowed"
				data-parsley-trigger="change"
				data-parsley-required="true"
				data-parsley-pattern="/^\S*$/" 
				data-parsley-error-message="Username should be 6-25 characters without blank spaces"
				name="username" class="form-control" value="<?php echo fnStringToHTML($user_details['username'])?>"><span id="user-availability-status"></span>
			  </div>
			</div><!-- /.form-group -->
			
			<div class="form-group">
			  <label class="col-md-3">Address:<span class="mandatory">*</span></label>

			  <div class="col-md-4">
				<textarea name="address"
				 placeholder="Enter Address"
				data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter address"
				data-parsley-maxlength="200"
				data-parsley-maxlength-message="Only 200 characters are allowed"							
				rows="4" class="form-control"><?php echo fnStringToHTML($user_details['address'])?></textarea>
			  </div>
			</div><!-- /.form-group -->
			<div id="working_area_top">		
			<h4>Assign Area</h4>		
			</div>

				<div class="form-group">
			  <label class="col-md-3">State:<!-- <span class="mandatory">*</span> --></label>
			  <div class="col-md-4">
				<?php
					$sql_state="SELECT * FROM tbl_state where country_id=101";
					
					$result_state = mysqli_query($con,$sql_state);		
				?>
				<select name="state" id="state" class="form-control" 
				data-parsley-trigger="change"				
				data-parsley-required-message="Please select State"
				parsley-required="true"
				data-parsley-required-message="Please select State"
				 onChange="fnShowCity(this.value)">
					<?php
						if($user_details['state'] == 0)
							echo "<option value=''>-Select-</option>";
						while($row_state = mysqli_fetch_array($result_state))
						{
							$selected = "";
							if($row_state['id'] == $user_details['state'])
								$selected = "selected";				
						
							echo "<option value='".$row_state['id']."' $selected>" . fnStringToHTML($row_state['name']) . "</option>";
						}
					?>
				</select>
			  </div>
			</div><!-- /.form-group -->

			<div class="form-group" id="city_div">
			  <label class="col-md-3">District:<!-- <span class="mandatory">*</span> --></label>
			  <div class="col-md-4" id="div_select_city">
				<?php
					$option = "<option value=''>-Select-</option>";
					if($user_details['city'] != '')
					{
						$sql_city="SELECT * FROM tbl_city where state_id='".$user_details['state']."' ORDER BY name";
						
						$result_city = mysqli_query($con,$sql_city);
						while($row_city = mysqli_fetch_array($result_city)){
							$cat_id=$row_city['id'];
							$selected = "";
							if($row_city['id'] == $user_details['city'])
								$selected = "selected";				
							
							$option.= "<option value='$cat_id' $selected>".$row_city['name']."</option>";
						}
					}												
				?>
			<!-- 	<select name="city" id="city" class="form-control"  
				data-parsley-trigger="change"
				data-parsley-required-message="Please select District"
				><?=$option;?></select> -->

<select name="city" id="city" class="form-control" onchange="FnGetSuburbDropDown(this)" >
<?=$option;?></select>
	

			  </div>
			</div><!-- /.form-group -->


			 
			<?php if($page_to_update != 'superstockist'){ ?>
			<div class="form-group">
			  <label class="col-md-3">Taluka:</label>
			<?php
				if($user_details['suburb_ids'] != '' && $user_details['suburb_ids'] != '0'){
					 //$sql_area="SELECT `id`, `cityid`, `stateid`, `suburbnm` FROM tbl_area WHERE id IN (".$user_details['suburb_ids'].") and isdeleted!='1' ";

					 	$sql_area="SELECT `id`, `cityid`, `stateid`, `suburbnm` FROM tbl_area WHERE cityid = ".$user_details['city']." and isdeleted!='1'";	
					
				}
				else{
					$sql_area="SELECT `id`, `cityid`, `stateid`, `suburbnm` FROM tbl_area WHERE cityid = '".$user_details['city'] . "' and isdeleted!='1'";
				}
				
				$result_area = mysqli_query($con,$sql_area);
				$suburb_array = explode(',', $user_details['suburb_ids']);
				//var_dump($suburb_array);
				$multiple = '';
				if(mysqli_num_rows($result_area) > 1)
				{
					$multiple = 'multiple';
				}
				
				$selected = "";
			?>
			  <div class="col-md-4"  id="div_select_area">
			  <select name="area[]" id="area" class="form-control" <?=$multiple;?> onchange="FnGetSubareaDropDown(this)">	
				<option value="">-Select-</option>
				<?php
				while($row_area = mysqli_fetch_array($result_area))
				{				
					if(count($suburb_array) > 0)
					{
                       //echo $row_area['id'];
                       $assign_id=$row_area['id'];
						$suburb_ids = explode(',', $user_details['suburb_ids']);
						if(in_array($assign_id, $suburb_ids))
						{
							$selected = "selected";
						}
						else
						{
							 $selected = "";
						}
						
					}
					echo "<option value='".$row_area['id']."' $selected>" . fnStringToHTML($row_area['suburbnm']) . "</option>";
				}
				?>
				</select>				
			  </div>
			</div>
			<input type="hidden" id="selected_taluka" name="selected_taluka" value="<?=$suburb_array[0];?>">
			<!-- /.form-group -->
			
			<div class="form-group" id="subarea_div">
			  <label class="col-md-3">Subarea:</label>
			  <?php
				$rows_subarea = 0;
				if(isset($user_details['subarea_ids']) && $user_details['subarea_ids'] != '')
				{
					$sql_subarea="SELECT `subarea_id`, `state_id`, `city_id`, `suburb_id`, `subarea_name` 
					FROM tbl_subarea WHERE subarea_id IN (".$user_details['subarea_ids'].")";				
					
					$result_subarea = mysqli_query($con,$sql_subarea);
					if($result_subarea != '')
						$rows_subarea = mysqli_num_rows($result_subarea);
					$subarea_array = explode(',', $user_details['subarea_ids']);
					$multiple = '';
					if(count($subarea_array) > 1)
					{
						$multiple = 'multiple';
					}
					
					$selected = '';
				}
			  ?>
			  <div class="col-md-4" id="div_select_subarea">
			  <select name="subarea[]" id="subarea" data-parsley-trigger="change" class="form-control" <?=$multiple;?>>					
					<?php		
					if($rows_subarea == 0)
					{ 
						echo '<option value="">-Select-</option>';
					}
					else
					{
						while($row_subarea = mysqli_fetch_array($result_subarea))
						{		
							if(count($subarea_array) > 0)
							{
								if(in_array($row_subarea['subarea_id'], $subarea_array))
									$selected = "selected";
								
							}
							echo "<option value='".$row_subarea['subarea_id']."' $selected>" . fnStringToHTML($row_subarea['subarea_name']) . "</option>";
						}
					}
					?>
				</select>
			  </div>
			</div><!-- /.form-group --> 
			<?php } ?>
<div id="working_area_bottom">				
</div>
<div class="form-group">
			  <label class="col-md-3">Email:</label>

			  <div class="col-md-4">
				<input type="text" name="email" id="email" 
				placeholder="Enter E-mail"
				data-parsley-maxlength="100"
				data-parsley-maxlength-message="Only 100 characters are allowed"
				data-parsley-type="email"
				data-parsley-type-message="Please enter valid e-mail"
				data-parsley-trigger="change"
				class="form-control" value="<?php echo fnStringToHTML($user_details['email'])?>">
			  </div>
			</div><!-- /.form-group -->
			<?php if($_SESSION[SESSION_PREFIX.'user_type'] == 'Admin'){ 
				$percent = $userObj->get_cod_percent();//echo "<pre>";print_r($percent['cod_percent']);
			?>
			<div class="form-group">
			  <label class="col-md-3">Cash On Delivery %:<span class="mandatory">*</span></label>
			  <div class="col-md-4">
			  <input type="text" id="cod_percent" name="cod_percent" placeholder="0" min="0" max="100" step="1" 
				data-parsley-validation-threshold="1" data-parsley-trigger="keyup" 
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter cash on delivery %"
				data-parsley-type="digits" 
				class="form-control" value="<?php echo $percent['cod_percent'];?>"/>
			  </div>
			</div><!-- /.form-group -->
			<?php  } ?>

			<div class="form-group">
			  <label class="col-md-3">Mobile Number:<span class="mandatory">*</span></label>

			  <div class="col-md-4">
				<input type="text"  name="mobile"
				placeholder="Enter Mobile Number"
				data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter mobile number"
				data-parsley-minlength="10"
				data-parsley-minlength-message="Mobile number should be of minimum 10 digits" 
				data-parsley-maxlength="15"				    
				data-parsley-maxlength-message="Only 15 digits are allowed"
				data-parsley-pattern="^(?!\s)[0-9]*$"
				data-parsley-pattern-message="Please enter numbers only"
				class="form-control" value="<?php echo fnStringToHTML($user_details['mobile'])?>">
			  </div>
			</div><!-- /.form-group -->
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
                <input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
				<input type="hidden" name="hidAction" id="hidAction" value="profile.php">
				<input type="hidden" name="id" id="id" value="<?=$user_details['id'];?>">
				<button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button>
                <a href="index.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group --> 
          </form>
		                                     
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
<style>
.form-horizontal{
font-weight:normal
}
</style>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<script>  
function calculate_data_count(element_value){
	element_value = element_value.toString();
	var element_arr = element_value.split(',');	
	return element_arr.length;
}
function setSelectNoValue(div,select_element){
	var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled value="">-Select-</option></select>';
	document.getElementById(div).innerHTML	=	select_selement_section;
}
function fnShowCity(id_value) {	
	$("#city_div").show();	
	$("#area").html('<option value="">-Select-</option>');	
	$("#subarea").html('<option value="">-Select-</option>');	
	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city";
	CallAJAX(url,"div_select_city");	
}
function FnGetSuburbDropDown(id) {
	$("#area_div").show();	
	$("#subarea").html('<option value="">-Select-</option>');	
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&multiple=multiple&function_name=FnGetSubareaDropDown";
	CallAJAX(url,"div_select_area");
}
function FnGetSubareaDropDown(id) {	
	var suburb_str = $("#area").val();	
	var suburb_arr_count = calculate_data_count(suburb_str);
	if(suburb_arr_count == 1){//If single city selected then only show its related subarea	
		$("#subarea_div").show();	
		var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea&multiple=multiple";
		CallAJAX(url,"div_select_subarea");
	}else if(suburb_arr_count > 1){
		$("#subarea_div").show();	
		var multiple_id = suburb_str.join(", ");
		var url = "getSubareaDropdown.php?multiple_id="+multiple_id+"&select_name_id=subarea&multiple=multiple";
		CallAJAX(url,"div_select_subarea");
	}else{
		setSelectNoValue("div_select_subarea", "subarea");
	}		
}
function checkAvailability() {
	$('#updateform').parsley().validate();
	var email = $("#email").val();
	var username = $("#username").val();
	var id = $("#id").val();
	if(username != '')
	{
		jQuery.ajax({
			url: "../includes/checkUserAvailable.php",
			data:'validation_field=username&username='+username+'&id='+id,
			type: "POST",
			async:false,
			success:function(data){		
				if(data=="exist") {
					alert('Username already exists.');					
					return false;
				} else {
					if(email != '')
					{
						jQuery.ajax({
							url: "../includes/checkUserAvailable.php",
							data:'validation_field=email&email='+email+'&id='+id,
							type: "POST",
							async:false,
							success:function(data){			
								if(data=="exist") {
									alert('E-mail already exists.');									
									return false;
								} else {	
									submitFrm();
								}
							},
							error:function (){}
						});
					}else{
						submitFrm();
					}									
				}
			},
			error:function (){}
		});
	}
}
function submitFrm(){
	var action = $('#hidAction').val();
	$('#updateform').attr('action', action);					
	$('#hidbtnsubmit').val("submit");
	$('#updateform').submit();
}

</script> 