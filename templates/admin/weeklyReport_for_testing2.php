<?php
include ("../../includes/config.php");
extract($_POST);
$arrWeeklyOption  = explode("::",$drpWeeklyOption);
$arrWeeklyOption[1] = date('d-m-Y', strtotime($arrWeeklyOption[1] . ' +1 day'));

$maxDays=7;
$StartDate = $arrWeeklyOption[0];
$MonthName = date('F', strtotime($StartDate . ' +0 day'));
$endDate = date('d-m-Y', strtotime($arrWeeklyOption[1] . ' -1 day'));
$seesion_user_id=$_SESSION[SESSION_PREFIX.'user_id'];
  $StartDate1 = date("Y-m-d", strtotime($StartDate));
    $endDate1 = date("Y-m-d", strtotime($endDate));	
  
   $sql = " SELECT o.order_date,
			o.order_from,tu2.firstname as order_from_firstname,tu2.user_type as order_from_user_type,tu2.user_role as order_from_user_role,
			o.order_to,tu.firstname as order_to_firstname,tu.user_type as order_to_user_type,tu.user_role as order_to_user_role,
			od.brand_id, od.cat_id ,od.product_id,od.product_variant_id,
			od.p_cost_cgst_sgst as total_order_gst_cost,
			s.name,od.product_quantity,
			od.product_variant_weight1, od.product_variant_unit1, od.product_variant_weight2, od.product_variant_unit2,
			tbl_brand.name as brand_name, 
			tbl_category.categorynm as cat_name,
			tbl_product.productname as product_name
			FROM tbl_order_details od
			LEFT JOIN  tbl_orders o  ON  od.order_id =o.id
			LEFT JOIN tbl_shops s ON s.id= o.shop_id
			LEFT JOIN tbl_user tu ON o.order_to = tu.id 
			LEFT JOIN tbl_user tu2 ON o.order_from = tu2.id 
			LEFT JOIN tbl_brand  ON od.brand_id = tbl_brand.id 
			LEFT JOIN tbl_category  ON od.cat_id = tbl_category.id 
			LEFT JOIN tbl_product  ON od.product_id = tbl_product.id 
			where date_format(o.order_date, '%Y-%m-%d') >= '".$StartDate1."' AND  date_format(o.order_date, '%Y-%m-%d') <='".$endDate1."' ";

      	
	$condition = " ";
	if(!empty($dropdownCategory)){
		$condition .= " AND od.cat_id = '".$dropdownCategory."' ";
	}
	
	if(!empty($dropdownUserType)){
		$sql_user_role="SELECT user_role as drop_user_role FROM tbl_user_tree where user_type='".$dropdownUserType."'";
		$result_userrole = mysqli_query($con,$sql_user_role);
		$row_userrole = mysqli_fetch_assoc($result_userrole);
		$drop_user_role = fnStringToHTML($row_userrole["drop_user_role"]);
		$web_user_role=array('Admin','Superstockist','Distributor');
		if (in_array($drop_user_role, $web_user_role)){
			$condition .= " AND tu.user_type = '".$dropdownUserType."' ";
		}else{
			$condition .= " AND tu2.user_type = '".$dropdownUserType."' ";
		}			
	}
	if($u_id !="")
	{
		//$condition .= " AND o.order_to = '".$u_id."' ";
		if (in_array($drop_user_role, $web_user_role)){
			$condition .= " AND o.order_to = '".$u_id."' ";
		}else{
			$condition .= " AND o.order_from = '".$u_id."' ";
		}
	}
	if($dropdownUserType=='')
	{			
	 $condition .= " AND o.order_to = '".$seesion_user_id."' ";
	}		
	if($dropdownshops !="")
	{
		$condition .= " AND o.shop_id = '".$dropdownshops."' ";
	}		
	if($dropdownbrands  !="")
	{
		$condition .= " AND od.brand_id = '".$dropdownbrands."' ";
	} 		
	if($dropdownProducts  !="")
	{
		$condition .= " AND od.product_id = '".$dropdownProducts."' ";
	}		
	if($dropdownSuburbs !="")
	{
		$condition .= " AND s.suburbid = '".$dropdownSuburbs."' ";
	}		
	if($dropdownCity !="")
	{
		$condition .= " AND s.city = '".$dropdownCity."' ";
	}
	if($dropdownState !="")
	{
		$condition .= " AND s.state = '".$dropdownState."' ";
	}
	
	$sql .= $condition;
	$sql .= "  order by o.order_date asc ";	
	//echo $sql;die();
	$result1 = mysqli_query($con,$sql); 
	$sale_report_monthly=array();$count_rec=0;
	while($row = mysqli_fetch_array($result1)){
		$sale_report_monthly[$count_rec]['order_date']=$row['order_date'];			
		$sale_report_monthly[$count_rec]['order_from']=$row['order_from'];			
		$sale_report_monthly[$count_rec]['order_from_firstname']=$row['order_from_firstname'];			
		$sale_report_monthly[$count_rec]['order_from_user_type']=$row['order_from_user_type'];			
		$sale_report_monthly[$count_rec]['order_from_user_role']=$row['order_from_user_role']; 
		
		$sale_report_monthly[$count_rec]['order_to']=$row['order_to'];			
		$sale_report_monthly[$count_rec]['order_to_firstname']=$row['order_to_firstname'];			
		$sale_report_monthly[$count_rec]['order_to_user_type']=$row['order_to_user_type'];			
		$sale_report_monthly[$count_rec]['order_to_user_role']=$row['order_to_user_role'];	
		
		$sale_report_monthly[$count_rec]['brand_id']=$row['brand_id'];			
		$sale_report_monthly[$count_rec]['cat_id']=$row['cat_id'];			
		$sale_report_monthly[$count_rec]['product_id']=$row['product_id'];			
		$sale_report_monthly[$count_rec]['product_variant_id']=$row['product_variant_id'];	
		
		$sale_report_monthly[$count_rec]['total_order_gst_cost']=$row['total_order_gst_cost'];			
		$sale_report_monthly[$count_rec]['name']=$row['name'];			
		$sale_report_monthly[$count_rec]['product_quantity']=$row['product_quantity'];			
		$sale_report_monthly[$count_rec]['product_variant_weight1']=$row['product_variant_weight1'];	
		$sale_report_monthly[$count_rec]['product_variant_unit1']=$row['product_variant_unit1'];	

		$sale_report_monthly[$count_rec]['product_variant_weight2']=$row['product_variant_weight2'];	
		$sale_report_monthly[$count_rec]['product_variant_unit2']=$row['product_variant_unit2'];
		
		$sale_report_monthly[$count_rec]['brand_name']=$row['brand_name'];
		$sale_report_monthly[$count_rec]['cat_name']=$row['cat_name'];
		$sale_report_monthly[$count_rec]['product_name']=$row['product_name'];
		
		$sale_report_monthly[$count_rec]['key']=$row['product_id']."-".$row['product_variant_id'];

		$count_rec++;
	}  
	foreach ($sale_report_monthly as $order_details) {
		$prod_and_var_key_array[] = $order_details['key'];
		$order_date_key_array[] = date('d-m-Y', strtotime($order_details['order_date']));
	}	
	$new_array_counter_date=array_count_values($order_date_key_array);
?>
<?php if($_GET["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
	
.fa {
    height: 18px;
}
.fa {
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    transform: translate(0, 0);
}
</style>
<?php } ?>
<div class="portlet box blue-steel">
	<div class="portlet-title">
	<?php 
	   if($_GET["actionType"]!="excel") 
		{ 
			?>
		<div class="caption"><i class="icon-puzzle"></i>Weekly Sales Report</div>		
		<button type="button" name="btnExcel" id="btnExcel" onclick="ExportToExcel();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
		&nbsp;
		<button type="button" name="btnPrint" id="btnPrint" onclick="takeprint()" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
		<?php } ?>
	</div>
	<div class="portlet-body">
		<div class="table-responsive" id="dvtblResonsive">
		<?php
			if($dropdownshops !=""){
				$sql=" SELECT  name from tbl_shops where id =  $dropdownshops";
				$result1 = mysqli_query($con,$sql);
				$row = mysqli_fetch_array($result1);
				$MarketName = $row["name"];
			} else {
				$MarketName = "ALL";
			}
			if($dropdownUserType !="" && $u_id !=''){	
			    $sql="SELECT firstname FROM tbl_user where id='".$u_id."'";
				$result1 = mysqli_query($con,$sql);
				$row = mysqli_fetch_assoc($result1);
				$user_name = fnStringToHTML($row["firstname"]);			
				$dropdownUserType1 = $dropdownUserType;
			} else{
				$dropdownUserType1 = "None";
				$user_name = "None";
			}
			if($dropdownSuburbs !=""){
				$sql="SELECT suburbnm FROM tbl_area WHERE id=$dropdownSuburbs ";
				$result1 = mysqli_query($con,$sql);
				$row = mysqli_fetch_array($result1);
				$SuburbName = fnStringToHTML($row["suburbnm"]);
			} else {
				$SuburbName = "ALL";
			} 
			
			$totalColumn=3;
			
			$totalColumn += 4;
			$firstColumn = round($totalColumn/2);
			$SecondColumn = $totalColumn - $firstColumn;
			$SecondAColumn = round($SecondColumn/2);
			$SecondBColumn = $SecondColumn - $SecondAColumn;
		?>
			
			<table class="table table-striped table-hover table-bordered" width="100%">
				<thead>
					<tr>
						<th colspan="<?php echo $firstColumn;?>">User Type: <?php if (isset($dropdownUserType) && $dropdownUserType!='') { echo $dropdownUserType;} else { echo "All" ;} ?></th>
						<th colspan="<?php echo $SecondColumn;?>">From Date: <?php echo $StartDate;?> To Date: <?php echo $endDate;?> </th>
					</tr>
					<tr>
						<th colspan="<?php echo $firstColumn;?>">User Name:  <?php if (isset($user_name) && $dropdownUserType!='') { echo $user_name;} else { echo "None" ;} ?></th>
						<th colspan="<?php echo $SecondAColumn;?>">Region: <?php echo $SuburbName;?></th>
						<th colspan="<?php echo $SecondBColumn;?>">Month: <?php echo $MonthName;?></th>
					</tr>
					<tr>
						<th>SR.</th>
						<th>Date</th>
						<th>Day</th>
						<th>SHOP</th>
						<th>Products</th>						
						<th>QNTY</th>						
						<th>COST</th>	
					</tr>					
				</thead>
				<tbody>
					<?php
					$date = strtotime('01-01-2010 -1 year');
					$temp_date=date('d-m-Y', $date);
					$total_record_count=1;$prod_qnty_total=0;$prod_cost_total=0.0;
					foreach($sale_report_monthly as $key_s=>$value_s){
						$order_date=date('d-m-Y', strtotime($value_s['order_date']));
						?>
						<tr>
							<?php if($temp_date!=$order_date){ ?>
							<td rowspan="<?php echo $new_array_counter_date[$order_date];?>"><?php echo $total_record_count;?></td>
							<td rowspan="<?php echo $new_array_counter_date[$order_date];?>"><?php echo $order_date;?></td>
							<td rowspan="<?php echo $new_array_counter_date[$order_date];?>"><?php echo date('D', strtotime($order_date));?></td>
							<td rowspan="<?php echo $new_array_counter_date[$order_date];?>"><?php echo $MarketName;?></td>
							<?php $temp_date=$order_date; $total_record_count++; 
							} ?>
							<td>
							<?php   echo $value_s['brand_name']."  ".$value_s['cat_name']."  ";
									echo $value_s['product_name']." ";
									if(!empty($value_s['product_variant_weight1'])){
										echo $value_s['product_variant_weight1']."-".$value_s['product_variant_unit1']."  ";
									}if(!empty($value_s['product_variant_weight2'])){
										echo $value_s['product_variant_weight2']."-".$value_s['product_variant_unit2']."";
									}
							?>
							</td>
							<td><?php echo $value_s['product_quantity'];
									$prod_qnty_total+=$value_s['product_quantity'];
								?>
							</td>
							<td><?php echo $value_s['total_order_gst_cost'];
									$prod_cost_total+=$value_s['total_order_gst_cost'];?></td>
						</tr>
					<?php } ?>
							<tr>
								<th COLSPAN="5" style="text-align: right;">Total</th>												
								<th><?php echo $prod_qnty_total;?></th>						
								<th><?php echo $prod_cost_total;?></th>	
							</tr>
				</tbody>
			</table>
		</div>
	</div>
</div> 
<?php
if($_GET["actionType"]=="excel") {
	$exportFileName  = str_replace("::","-",$drpWeeklyOption);
	header( "Content-Type: application/vnd.ms-excel");
	header( "Content-disposition: attachment; filename=Report_".$exportFileName.".xls");
}
?>