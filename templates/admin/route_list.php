<!-- BEGIN HEADER -->
<?php
include "../includes/grid_header.php";
include "../includes/userManage.php";
include "../includes/shopManage.php";
$userObj = new userManager($con, $conmain);
$shopObj = new shopManager($con, $conmain);
$user_type = $_SESSION[SESSION_PREFIX . 'user_type'];
$userid = $_SESSION[SESSION_PREFIX . 'user_id'];
?>
<!-- BEGIN PAGE HEADER-->
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "ManageSupplyChain";
        $activeMenu = "routes";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <div class="page-content-wrapper">
            <div class="page-content">			
                <h3 class="page-title">
                   Route List
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="#">Route </a>
                        </li>
                    </ul>				
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">
                                   Route List
                                </div>
                                <a class="btn btn-sm btn-default pull-right mt5" href="route_add.php">
                                    Add Route
                                </a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">	


                                <form class="form-horizontal" role="form" name="form" method="post" action="">	
                                    <div id="route_list">
                                    





<?php   
            $routecount = $shopObj->getRouteCount();
            $route_count = count($routecount);
          
?>

                                         <table class="table table-striped table-brouteed table-hover" id="sample_2">
                                            <thead>
<tr id="main_th">  

                                                                     

                                                    <th>
                                                        Route Name            
                                                    </th>                                                   
                                                    <th>
                                                        Address
                                                    </th>
                                                  
                                                    <th>
                                                        Action
                                                    </th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
            <?php             
         //  echo $route_count."ppp";
            // echo "<pre>";print_r($routecount);exit();
            foreach($routecount as $key => $value) 
            {
                $routedetails=array();
                $routedetails = $shopObj->getRouteCountDetails($value);
                $route_name=$routedetails[0]['route_name'];
                $route_id=$routedetails[0]['route_id']; 
                $Address='';
                $i = 1;       
                    foreach ($routedetails as $key1 => $value1) 
                    {                            
                       // echo "<pre>";print_r($routedetails);
                       // $state_name.=$value1['state_name']."<br>";
                        $Address.= $i.") ".$value1['subarea_name']." ".$value1['area_name']." ".$value1['city_name'].", ".$value1['state_name']."<br>";
                       // $route_no.="".$value1['route_no'];
                         $i++;
                    }
                ?>
                <tr class="odd gradeX">               
                    <td> 
                        <a href="route_update.php?route_id=<?=$route_id;?>">
                            <?php echo $route_name;?>
                        </a>   
                    </td>            
                    <td align="left"><?php echo $Address; ?></td>
                    <td align="right">
           <a onclick="javascript: showRouteMap(<?=$route_id;?>)">
            View Route
            </a>

            <a onclick="javascript: deleteRoute(<?=$route_id;?>)">
            Delete
            </a>
                    </td>
                    
                </tr>
            <?php } ?>

                                            </tbody>
                                        </table>
                                </form>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>

                </div>
                <!-- END PAGE CONTENT-->


            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>

  <div class="modal fade" id="route_details" role="dialog">
        <div class="modal-dialog" style="width: 880px !important;">
            <!-- Modal content-->
            <div class="modal-content" id="route_map_content">
            </div>
        </div>
    </div>

            <!-- BEGIN FOOTER -->
            <?php include "../includes/grid_footer.php" ?>
            <!-- END FOOTER -->
            <!-- START JAVASCRIPTS -->
 <script>
                function deleteRoute(route_id) {
                    if (confirm('Are you sure that you want to delete this Route?')) {
                        CallAJAX('ajax_product_manage.php?action=delete_route&route_id=' + route_id);
                    }
                }

                function CallAJAX(url) {
                    if (window.XMLHttpRequest) {
                        xmlhttp = new XMLHttpRequest();
                    } else {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            alert('Route deleted successfully');
                            location.reload();
                        }
                    }
                    xmlhttp.open("GET", url, true);
                    xmlhttp.send();
                }


        function showRouteMap(route_id) {
            var url = "route_map_popup.php";
            jQuery.ajax({
                url: url,
                method: 'POST',
                data: 'route_id=' + route_id ,
                async: false
            }).done(function (response) {
                $('#route_map_content').html(response);
                $('#route_details').modal('show');
            }).fail(function () { });
            return false;
        }
            </script>


    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
</html>