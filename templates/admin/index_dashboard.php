<!DOCTYPE html>
<?php
session_start();
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include ("../../includes/config.php");
if(isset($_SESSION[SESSION_PREFIX."user_name"]))
{
?>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 4.1.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title><?=SITETITLE;?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<!-- BEGIN THEME STYLES -->
<link href="../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="../../assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon"  href="../../assets/TitleLogo/favicon.png" type="image/gif" size="16x16">
<!-- amCharts javascript sources -->
		<script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
		<script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script>
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/pie.js"></script>
		

		<!-- amCharts javascript code -->
		<script type="text/javascript">
			AmCharts.makeChart("chartdiv",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Total Sale",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"stackType": "regular",
							"title": "Total Sales"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"useGraphSettings": true
					},
					//"titles": [
//						{
//							"id": "Title-1",
//							"size": 14,
//							"text": "Sales Visibility"
//						}
//					],
					"dataProvider": [
						{
							"category": "Distributor",
							"column-1": 3
						},
						{
							"category": "Dealer",
							"column-1": 6
						},
						{
							"category": "Sales Person",
							"column-1": 8
						}
					]
				}
			);
		</script>
        
        <!-- amCharts javascript code -->
		<script type="text/javascript">
			AmCharts.makeChart("chartdiv2",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Sale Person",
							"fillColors": "#7e9e27",
							"lineColor": "#7e9e27",
							"type": "column",
							"valueField": "column-1"
						}
					],
					
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"stackType": "regular",
							"title": "Sale"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"useGraphSettings": true
					},
					//"titles": [
//						{
//							"id": "Title-1",
//							"size": 14,
//							"text": "Performance Base Report"
//						}
//					],
					"dataProvider": [
						{
							"category": "Aakash Shinde",
							"column-1": 10
						},
						{
							"category": "Santosh Mane",
							"column-1": 8
						},
						{
							"category": "Vikram Dev",
							"column-1": 6
						},
						{
							"category": "Shruti Surve",
							"column-1": 4
						},
						{
							"category": "Vaibhav Jadhav ",
							"column-1": 3
						}
					]
				}
			);
		</script>
        
        <!-- amCharts javascript code -->
		<script type="text/javascript">
			AmCharts.makeChart("chartdiv3",
				{
					"type": "pie",
					"balloonText": "[[title]]<br><span style='font-size:12px'><b>[[value]]</b> ([[percents]]%)</span>",
					"labelRadius": 7,
					"titleField": "category",
					"valueField": "column-1",
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"align": "center",
						"markerType": "circle"
					},
					"titles": [],
					"dataProvider": [
						{
							"category": "",
							"column-1": 8
						},
						{
							"category": "",
							"column-1": 6
						},
						{
							"category": "",
							"column-1": 2
						},
						{
							"category": "",
							"column-1": 4
						},
						{
							"category": "",
							"column-1": 3
						},
						{
							"category": "",
							"column-1": 2
						}
					
					]
				}
			);
		</script>
        
        <!-- amCharts javascript code -->
		<script type="text/javascript">
			AmCharts.makeChart("chartdiv4",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Products in Shops",
							"fillColors": "#1cacfe",
							"lineColor": "#1cacfe",
							"type": "column",
							"valueField": "column-1"
						}
					],
					
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"stackType": "regular",
							"title": "Sale"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"useGraphSettings": true
					},
					//"titles": [
//						{
//							"id": "Title-1",
//							"size": 14,
//							"text": "Performance Base Report"
//						}
//					],
					"dataProvider": [
						{
							"category": "Vanraj",
							"column-1": 10
						},
						{
							"category": "Saras Khakhra",
							"column-1": 8
						},
						{
							"category": "Saras Atta Papad",
							"column-1": 6
						},
						{
							"category": "Pudina Papad",
							"column-1": 4
						}
					]
				}
			);
		</script>
        
        
        <!-- amCharts javascript code -->
		<script type="text/javascript">
			AmCharts.makeChart("chartdiv5",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Products in Region",
							"fillColors": "#faa502",
							"lineColor": "#faa502",
							"type": "column",
							"valueField": "column-1"
						}
					],
					
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"stackType": "regular",
							"title": "Sale"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"useGraphSettings": true
					},
					//"titles": [
//						{
//							"id": "Title-1",
//							"size": 14,
//							"text": "Performance Base Report"
//						}
//					],
					"dataProvider": [
						{
							"category": "Pune",
							"column-1": 8
						},
						{
							"category": "Satara",
							"column-1": 6
						},
						{
							"category": "Sangli",
							"column-1": 5
						},
						{
							"category": "Nashik",
							"column-1": 4
						}
					]
				}
			);
		</script>
<style>
.amChartsLegend.amcharts-legend-div {
    display: none;
}
.form-horizontal .control-label {
    text-align: left;
}
</style>
<script>
function fnSelectionBoxTest()
{
	if(document.getElementById('selTest').value == '3')
	{
	   document.getElementById('date-show').style.display = "block";
	   document.getElementById('dvTestData').style.display = "none"; 
	}
	else if(document.getElementById('selTest').value == '2')
	{
	   document.getElementById('dvTest').style.display = "none";
	   document.getElementById('dvTestData').style.display = "block"; 
	}
	else
	{
	   document.getElementById('dvTest').style.display = "none";
	   document.getElementById('dvTestData').style.display = "none";
	}
}
</script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="./index.php">
			<img src="../../assets/TitleLogo/favicon.png" alt="logo" class="logo-default" size="16x16">
			</a>
			<div class="menu-toggler sidebar-toggler hide">
				<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
			</div>
			
		</div>
        <span style="color:white;padding-left:700px;"><?php echo "Welcome    "."<b>".$_SESSION[SESSION_PREFIX.'user_name']."<b>";?></span>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

					<span class="username ">
					<?php echo "Welcome "."<b>".$_SESSION[SESSION_PREFIX.'firstname']."</b>";?> </span>
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
						<li>
							<a href="#">
							<i class="icon-user"></i> My Profile </a>
						</li>
						
						<li>
							<!--<a href="login.php">
							<i class="icon-key"></i> Log Out </a>-->
<?php echo '<a href="../logout.php"><i class="icon-key"></i> Logout </a>';?>
<?php
}
else
{	
header("location:../login.php");
}
?>
						</li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				
				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
			<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
			<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
			<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>
				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<br/>
				
				<li class="active open">
					<a href="javascript:;">
					<i class="icon-home"></i>
					<span class="title">Dashboard</span>
					<span class="selected"></span>
					</a>
				</li>
                
                <li>
					<a href="javascript:;">
					<i class="fa fa-share-alt"></i>
					<span class="title">Manage Supply Chain</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
                    	<li>
							<a href="superstockist.php">
							 Superstockist </a>
						</li>
						<li>
							<a href="distributors.php">
							 Stockist</a>
						</li>
                        <li>
							<a href="sales.php">
							 Sales Person </a>
						</li>
						<!-- <li>
							<a href="javascript:;">
							 Retailer</a>
						</li> -->
						<li>
							<a href="shops.php">
							 Shops</a>
						</li>
						
					</ul>
				</li>
                
                
                <li>
					<a href="javascript:;">
					<i class="fa fa-share-alt"></i>
					<span class="title">Manage Products</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="categories.php">
							 Categories </a>
						</li>
						<li>
							<a href="variant.php">
							 Variant</a>
						</li>
                        <li>
							<a href="product.php">
							Product</a>
						</li>
					</ul>
				</li>
                
                <li class="">
					<a href="Orders.php">
					<i class="icon-home"></i>
					<span class="title">Orders</span>
					</a>
				</li>
               
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<h3 class="page-title">
			Dashboard 
			</h3>
			
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			
            <div class="row">
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
					<div class="dashboard-stat blue-madison">
						<div class="visual">
							<i class="fa fa-briefcase fa-icon-medium"></i>
						</div>
						<div class="details">
							<div class="number">
								 4924
							</div>
							<div class="desc">
								 Today's Sale Quantity
							</div>
						</div>
						<a class="more" href="javascript:;">
						View more <i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
				
				
			</div>
            
            <div class="row">
            
            <div class="col-md-12">
            <form class="form-horizontal">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Options:</label>
                            <div class="col-sm-3">
                              <select class="form-control" name="selTest" id="selTest" onChange="fnSelectionBoxTest()">
                                    <option value='0'>Select </option>
                                    <option value='1'>weekly</option>
                                    <option value='2'>Monthly</option>
                                    <option value='3'>From specific date</option>
                                </select>
                            </div>
                            
             </div>
             
             
             <div class="form-group">
                          <div id="date-show" style="display:none;">
                          
                          <label for="inputEmail3" class="col-sm-2 control-label">From Date:</label>
                            <div class="col-md-2">
											<div class="input-group input-medium date date-picker" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
												<input type="text" class="form-control">
												<span class="input-group-btn">
												<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<!-- /input-group -->
										</div>
                            
                            <label for="inputEmail3" class="col-sm-2 control-label">To Date:</label>
                            <div class="col-md-2">
											<div class="input-group input-medium date date-picker" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
												<input type="text" class="form-control">
												<span class="input-group-btn">
												<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<!-- /input-group -->
										</div>
                          
                          </div>  
                            
             </div>
             
             </form>               
                            
				</div>
                
                <div class="clearfix"></div>
                
                <div class="col-md-6">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-thumb-tack"></i>Today's Top 5 Performers
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								
							</div>
						</div>
						<div class="portlet-body">
                        
						<form class="form-horizontal">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Performer:</label>
                            <div class="col-sm-4">
                              <select class="form-control">
                                  <option>All</option>
                                  <option>Top 5</option>
                                  <option>Top 10</option>
                                  <option>Bottom 5</option>
                                  <option>Bottom 10</option>
                                </select>
                            </div>
                           
                          </div>
                          </form>	
                            
                         <div id="chartdiv2" style="width: 100%; height: 300px; background-color: #FFFFFF;"></div>   
                            
                            
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
                
                
                
                <div class="col-md-6">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-thumb-tack"></i>Today's Most Selling Product
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								
							</div>
						</div>
						<div class="portlet-body">
                        
						<!--<form class="form-horizontal">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Product:</label>
                            <div class="col-sm-4">
                              <select class="form-control">
                                  <option>All</option>
                                  <option>Vanraj</option>
                                  <option>Saras khakre</option>
                                </select>
                            </div>
                            
                          </div>
                          </form>	-->
                            
                        <div id="chartdiv3" style="width: 100%; height: 350px; background-color: #FFFFFF;"></div>
                            
                            
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
                
                <div class="clearfix"></div>
                
                
                <div class="col-md-6">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-thumb-tack"></i>Most selling Products in Shops
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								
							</div>
						</div>
						<div class="portlet-body">
                        
						<form class="form-horizontal">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Shops:</label>
                            <div class="col-sm-4">
                              <select class="form-control">
                                  <option>All</option>
                                  <option>M-Mart</option>
                                  <option>Mahesh Super Market</option>
                                </select>
                            </div>
                           
                          </div>
                          </form>	
                            
                         <div id="chartdiv4" style="width: 100%; height: 300px; background-color: #FFFFFF;"></div>   
                            
                            
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
                
                
                
                <div class="col-md-6">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-thumb-tack"></i>Most selling Products in Region
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								
							</div>
						</div>
						<div class="portlet-body">
                        
						<form class="form-horizontal">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Region:</label>
                            <div class="col-sm-4">
                              <select class="form-control">
                                  <option>All</option>
                                  <option>Pune</option>
                                  <option>Satara</option>
                                </select>
                            </div>
                           
                          </div>
                          </form>	
                            
                         <div id="chartdiv5" style="width: 100%; height: 300px; background-color: #FFFFFF;"></div>   
                            
                            
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
                
                
                
				
			</div>
            
          
            
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 2017 &copy; company.com
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<!--<script src="../../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
--><!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="../../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="../../assets/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="../../assets/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="../../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="../../assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="../../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="../../assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="../../assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="../../assets/admin/pages/scripts/components-pickers.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
        jQuery(document).ready(function() {    
           Metronic.init(); // init metronic core components
Layout.init(); // init current layout
QuickSidebar.init(); // init quick sidebar
Demo.init(); // init demo features
		   ComponentsPickers.init();
        });
    </script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>