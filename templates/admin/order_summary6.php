<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Reports"; $activeMenu = "OrderSummary2";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<h3 class="page-title">
			Shop Summary
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="javascript:;">Shop Summary</a>	
					</li>
				</ul>				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">                
					<div class="row">
				<div class="col-md-12"> 				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">Search Criteria</div>							
                            <div class="clearfix"></div>
						</div>						
						<div class="portlet-body">							
							<form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">								
							
							<div class="form-group">
								<label class="col-md-3">Report Type:</label>
								<div class="col-md-4">
									<select name="report_type_location" id="report_type_location" class="form-control"
											onchange="getordersearchdata(this)" >											
											<option value="1" selected>State</option>
											<option value="2">City</option>
											<option value="3">Area</option>
											<option value="4">Subarea</option>
									</select>
								</div>
							</div>	
							<div class="form-group">
								<label class="col-md-3">Date Options:</label>
								<div class="col-sm-4">						
									<select class="form-control" name="selTest" id="selTest" onChange="fnSelectionBoxTest()">									
									<option value='5' <?php if($_REQUEST['selTest']=="5")echo 'selected';?>>All</option>
									<option value="4" <?php if($_REQUEST['selTest']=="4")echo 'selected';?>>Today</option>
									<option value='1' <?php if($_REQUEST['selTest']=="1")echo 'selected';?>>Weekly</option>
									<option value='2' <?php if($_REQUEST['selTest']=="2")echo 'selected';?>>Current month</option>
									<option value='3' <?php if($_REQUEST['selTest']=="3")echo 'selected';?>>From specific date</option>
									</select>
									<input type="hidden" name="hdnSelrange" id="hdnSelrange">
								</div>
							</div>
							<div class="form-group" id="date-show" style="display:none;">
								<label class="col-md-3">Date:</label>
								<div class="col-md-8 nopadl">
									<div class="col-md-1" class="nopadl" style="margin-right:-17px;">
										<label class="nopadl" style="padding-top:5px;">From:</label>
									</div>
									<div class="col-md-3">										
										<div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
											<input type="text" class="form-control" name="frmdate" id="frmdate" value="<?php echo $frmdate;?>" readonly onchange="callLocal();">
											<span class="input-group-btn">
											<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>			
									<div class="col-md-1" class="nopadl" style="margin-right:-35px;margin-left:-8px;">
										<label class="nopadl" style="padding-top:5px">To:</label>
									</div>
									<div class="col-md-3">
										<div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
											<input type="text" class="form-control" name="todate" id="todate" value="<?php echo $todate;?>" readonly onchange="callLocal();">
											<span class="input-group-btn">
											<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>						
								</div>                          
						</div> 	
								
							                        
					    <input type="hidden" id="order" name="order" value="asc">
						<input type="hidden" id="sort_complete" name="sort_complete" value="">
						<input type="hidden" id="page" name="page" value="">
						<input type="hidden" id="per_page" name="per_page" value=""> 	
						<input type="hidden" id="actionType" name="actionType" value=""> 
						<input type="hidden" id="search" name="search" value="">
						<div class="form-group">
								<div class="col-md-4 col-md-offset-3">
									<button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>
									<button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnChangeReportType('daily');">Reset</button>
								</div>
							</div><!-- /.form-group -->
					</div>
					<!-- END PAGE CONTENT-->
				</div>
					 <div class="clearfix"></div>   
				<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption"><i class="icon-puzzle"></i>Shop Summary Report </div>
								<button type="button" name="btnExcel" id="btnExcel" onclick="report_download();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
								&nbsp;
								<button type="button" name="btnPrint" id="btnPrint"  class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
							</div>	
						<div class="portlet-body" id="order_summary_details">
						</div>
					</form>	
			
			</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
</div>
</div>
</div>
</div>
<div style="display:none;" class="modal-backdrop fade in"></div>

<div id="print_div"  style="display:none;"></div>
<div id="table_heading"  style="display:none;"><?=$user_data['firstname'];?>'s Shopwise <?=$report_title;?></div>
<form action="../includes/exportToExcel.php" method="post" name="export_excel" id="export_excel">
	<input type="hidden" name="export_data" id="export_data">
</form>
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>  
function fnSelectionBoxTest()
{
	if($('#selTest').val() == '3'){
	   $('#date-show').show(); 
	}else{
		$('#date-show').hide();
		loadReport(1);
	}
}
$(document).ready(function() {
	 getordersearchdata();
  });
</script>
<script>

function getordersearchdata() {	
	var param = '';	
	var report_type = $('#report_type_location').val();
	//alert(report_type);
	param = param + '&report_type='+report_type;
	var selTest = $('#selTest').val();
	param = param + '&selTest='+selTest;
	if(selTest == 3)
	{
		var frmdate = $('#frmdate').val();
		param = param + '&frmdate='+frmdate;
		var todate = $('#todate').val();
		param = param + '&todate='+todate;
		var validation = compaire_dates(frmdate,todate);
	}
  if(selTest == 3 && frmdate != '' && todate != '' && validation == 1)
  {
	  alert("'From' date should not be greater than 'To' date.");
	  return false;
  }
  else
  {
	$.ajax
		({
			type: "POST",
			url: "order_summary_detail6.php",
			data: param,
			success: function(msg)
			{
			  $("#order_summary_details").html(msg);
			}
		});
  }	
}
</script>	
<script>


function report_download() {
	var td_rec = $("#sample_2121 td:last").html();
	if(td_rec != 'No matching records found')
	{
		var divContents = $(".table-striped").html();
		$("#print_div").html('<table id="print_table" style="text-decoration:none;">'+divContents+'</table>');	
		var heading = $("#table_heading").html();
		//$("#print_table tr th i").html("RS");	
		divContents =  $("#print_div").html();
		
		divContents = divContents.replace(/<\/*a.*?>/gi,'');	
		$("#export_data").val(divContents);
		document.forms.export_excel.submit();
	}else{
		alert("No matching records found");
	}
}

  $("#btnPrint").live("click", function () {
	var td_rec = $("#sample_2121 td:last").html();
	if(td_rec != 'No matching records found')
	{
		var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
		
		//var divContents = $(".table-striped").html();
		var divContents = $("#order_summary_details").html();
		 $("#print_div").html('<table id="print_table" style="text-decoration:none;">'+divContents+'</table>');	
		var heading = $("#table_heading").html();
		//$("#print_table tr th i").html("RS");	
		divContents =  $("#print_div").html();
		
		//divContents = divContents.replace(/<\/*a.*?>/gi,'');	
		$("#export_data").val(divContents); 
		
		$("#print_div").html(divContents);
		$("#print_div a").removeAttr('href');
		$("#sample_2121 tr th i").html("₹");	
		var divContents = $("#print_div").html();
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write('<html><head><title>Report</title>');
		printWindow.document.write('<style>a{text-decoration: none; color:#333333;} #sample_2121{margin:20 20 20 20px; width:700px}</style>');
		printWindow.document.write('<link   rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css"/>');
		printWindow.document.write('<link  rel="stylesheet" type="text/css" href="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>');
		
		if( navigator.userAgent.toLowerCase().indexOf('chrome') > -1 ){
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');	
			printWindow.focus();	
			setTimeout(function () {
				printWindow.print();
				//printWindow.close();
			}, 500);
		}else if(isIE == true){
			printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');	
			printWindow.focus();	
			printWindow.document.execCommand("print", false, null);
			//printWindow.close();
		}
		else{		
			printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');	
			printWindow.focus();	
			setTimeout(function () {				
				printWindow.print();
				//printWindow.close();
			}, 100);
		}
	}else{
		alert("No matching records found");
	} 
});
</script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- END JAVASCRIPTS -->
