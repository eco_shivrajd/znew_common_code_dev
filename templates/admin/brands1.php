<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/commonManage.php";	
?>
<!-- END HEADER -->
<?php
if(isset($_POST['submit']))
{
	//$id		= $_GET['id'];
	$id=base64_decode($_GET['id']);
	$brandnm= fnEncodeString($_POST['brandnm']);
	$description=fnEncodeString($_POST['description']);
	$sql = "UPDATE `tbl_brand` SET name='$brandnm' , description = '$description' where id='$id'";
	$brands_sql=mysqli_query($con,$sql);
	$commonObj 	= 	new commonManage($con,$conmain);
	$commonObj->log_update_record('tbl_brand',$id,$sql);
	echo '<script>alert("Brand updated successfully.");location.href="brands.php";</script>';
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageProducts"; $activeMenu = "Brands";
	include "../includes/sidebar.php";
		$commonObj 	= 	new commonManage($con,$conmain);
	$row_url=$commonObj->getPageIDforUrlEdit($php_page_name);
	$page_id_url = $row_url['page_id'];
	$row_url_edit=$commonObj->getURLforEdit($profile_id,$page_id_url);
	$ischecked_edit_url = $row_url_edit['ischecked_edit'];
    if ($ischecked_edit_url == 0 && $ischecked_edit_url!='') 
	{
		session_set_cookie_params(0);
		session_start();
		session_destroy();
		echo '<script>location.href="../login.php";</script>';
	    exit;
	}
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Brands
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="brands.php">Brands</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Brand</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Brand
							</div>
							
						</div>
						<div class="portlet-body">
							<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>					
							<?php
							//$id=$_GET['id'];
							$id=base64_decode($_GET['id']);
							$sql1="SELECT * FROM `tbl_brand` where id='$id'";
							$result1 = mysqli_query($con,$sql1);
							$row1 = mysqli_fetch_array($result1);
							?>	
                          
							<form class="form-horizontal" data-parsley-validate="" role="form" method="post" enctype="multipart/form-data" action="">

								<div class="form-group">
									<label class="col-md-3">Brand Name:<span class="mandatory">*</span></label>
									<div class="col-md-4">
									<input type="text" name="brandnm" value="<?php echo fnStringToHTML($row1['name'])?>" 
									placeholder="Enter Brand Name"
									data-parsley-trigger="change"
									data-parsley-required="#true" 
									data-parsley-required-message="Please enter brand name"
									data-parsley-maxlength="50"
									data-parsley-maxlength-message="Only 50 characters are allowed"		
									class="form-control">
									</div>
								</div><!-- /.form-group -->
								<div class="form-group">
									<label class="col-md-3">Brand Description:</label>
									<div class="col-md-4">
									<textarea name="description" 
									placeholder="Enter Brand Description"				
									data-parsley-trigger="change"				
									data-parsley-maxlength="250"
									data-parsley-maxlength-message="Only 250 characters are allowed"
									class="form-control" rows="5"><?php echo fnStringToHTML($row1['description'])?></textarea>
									</div>
								</div><!-- /.form-group -->

								<div class="form-group">
									<div class="col-md-4 col-md-offset-3">
									<button type="submit" name="submit" class="btn btn-primary">Submit</button>
										<a href="brands.php" class="btn btn-primary">Cancel</a>								
									</div>
								</div><!-- /.form-group -->
							</form>  
						  
						  <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
							<div class="modal-dialog" style="width:300px;">
								<div class="modal-content">
									<div class="modal-body">
										<p><h4 style="color:red; text-align:center;">Do you want to delete this record ?</h4></p>                     
									  <center><a href="categories_delete.php?id=<?php echo $row1['id']?>" ><button type="button" class="btn btn-success">Yes</button></a>
									  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
									  </center>
									</div>    
								</div>
							</div>
						</div>
                         
                      
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>