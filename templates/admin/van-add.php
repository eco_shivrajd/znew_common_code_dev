<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
 
include "../includes/userManage.php";	
$userObj 	= 	new userManager($con,$conmain);

if(isset($_POST['submit']))
{	
	$van_no		  	= $_POST['van_no'];
	$van_rto		= $_POST['van_rto'];
	$van_type		= $_POST['van_type'];
	$van_owner		= $_POST['van_owner'];
	$mobile			= $_POST['mobile'];
	$van_cap_wght	=$_POST['van_cap_wght'];
	$van_cap_boxes	= $_POST['van_cap_boxes'];
	$added_by	= $_SESSION[SESSION_PREFIX.'user_id'];
	$del_status	='Empty';

		 $sql="INSERT INTO tbl_van(van_no,van_rto,van_type,van_owner,mobile,van_cap_wght,van_cap_boxes,added_by,del_status)VALUES('".
			$van_no."','".
			$van_rto."','".
			$van_type."','".
			$van_owner.
			"','".$mobile.
			"','".$van_cap_wght.
			"','".$van_cap_boxes.
			"','".$added_by.
			"','".$del_status."')";
		
		$result1 = mysqli_query($con,$sql);
		 if(!empty($result1))
			echo '<script>alert("Vehicle added successfully.");location.href="van.php";</script>';	
		else
			echo '<script>alert("Not added Some error!.");location.href="van-add.php";</script>'; 		 				
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageTransport"; $activeMenu = "Vehicles";
	include "../includes/sidebar.php";

	 $commonObj 	= 	new commonManage($con,$conmain);
	$row_url=$commonObj->getPageIDforUrlAdd($php_page_name);
	$page_id_url = $row_url['page_id'];
	$row_url_add=$commonObj->getURLforAdd($profile_id,$page_id_url);
	$ischecked_add_url = $row_url_add['ischecked_add'];
    if ($ischecked_add_url == 0 && $ischecked_add_url!='') 
	{
		session_set_cookie_params(0);
		session_start();
		session_destroy();
		echo '<script>location.href="../login.php";</script>';
	    exit;
	}
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Vehicle
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="van.php">Vehicle</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Vehicle</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Vehicle
							</div>
							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						  
						<form class="form-horizontal" data-parsley-validate="" role="form" method="post" 
						action="van-add.php" novalidate="">         
					
							<div class="form-group">
								<label class="col-md-3">Vehicle No.:<span class="mandatory">*</span></label>
								<div class="col-md-4"><input name="van_no" type="text" class="form-control" 
								placeholder="Enter Vehicle No."
								data-parsley-required="#true" 
								data-parsley-required-message="Please enter Vehicle No." 
								data-parsley-maxlength="50"
								data-parsley-maxlength-message="Only 50 characters are allowed"
								></div>
							</div>
							
							<div class="form-group">
							<label class="col-md-3">Vehicle RTO Number:<span class="mandatory">*</span></label>
							<div class="col-md-4">
							<input name="van_rto" type="text" class="form-control" value=""
							placeholder="Vehicle RTO Number"
							data-parsley-trigger="change" 
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter Vehicle RTO Number"
							data-parsley-pattern="^(?!\s)[^']*$";
							data-parsley-pattern-message="Single quote not allowed"
							data-parsley-pattern='^(?!\s)[^"]*$';
							data-parsley-pattern-message="Double quote not allowed"
							data-parsley-maxlength="50"
							data-parsley-maxlength-message="Only 50 characters are allowed"
							>
							
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3">Vehicle Type:</label>
							<div class="col-md-4"><input name="van_type" type="text" class="form-control" value=""
							data-parsley-trigger="change" 
							data-parsley-maxlength="50"
							data-parsley-maxlength-message="Only 50 characters are allowed"
							></div>
						</div>
						<div class="form-group">
							<label class="col-md-3">Vehicle Capacity Weight:</label>
							<div class="col-md-4"><input name="van_cap_wght" type="text" class="form-control" value=""	
							data-parsley-trigger="change" 
							data-parsley-maxlength="20"
							data-parsley-maxlength-message="Only 20 characters are allowed"></div>
						</div>
						<div class="form-group">
							<label class="col-md-3">Vehicle Capacity Pcs:</label>
							<div class="col-md-4"><input name="van_cap_boxes" type="text" class="form-control" value=""	
							data-parsley-trigger="change" 
							data-parsley-maxlength="20"
							data-parsley-maxlength-message="Only 20 characters are allowed"></div>
						</div>
							
							<div class="form-group">
								<label class="col-md-3">Transporter/Owner Name:<span class="mandatory">*</span></label>
								<div class="col-md-4"><input name="van_owner" type="text" class="form-control" 
								placeholder="Enter Transporter/Owner Name"
								data-parsley-required="#true" 
								data-parsley-required-message="Please enter Transporter/Owner Name"
								data-parsley-maxlength="50"
								data-parsley-maxlength-message="Only 50 characters are allowed"
								data-parsley-pattern="^(?!\s)[a-zA-Z ]*$"
								data-parsley-pattern-message="Please enter alphabets only"
							></div>
							</div>
							
							<div class="form-group">
								<label class="col-md-3">Contact  Number:<span class="mandatory">*</span></label>
								<div class="col-md-4">
								<input name="mobile" type="text" class="form-control" 
								placeholder="Enter Contact  Number"
								data-parsley-trigger="change" 
								data-parsley-required="#true" 
								data-parsley-required-message="Please enter Contact  Number"
								data-parsley-trigger="change" 
								data-parsley-minlength="10" 
								data-parsley-maxlength="15" 
								data-parsley-minlength-message="Minimum 10 characters are allowed" 
								data-parsley-maxlength-message="Only 15 characters are allowed" 
								data-parsley-pattern="^(?!\s)[0-9!@#$%^&amp;*+_=><,./:' ]*$" 
								data-parsley-pattern-message="Alphabets are not allowed"  >
								</div>
							</div>
						<div class="form-group">
						  <div class="col-md-4 col-md-offset-3">
						   <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
							<a href="van.php" class="btn btn-primary">Cancel</a>
						  </div>
						</div><!-- /.form-group -->
						
					  </form>                                       
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
		$(function() {
            $('.list-group-sortable-connected').sortable({
                placeholderClass: 'list-group-item',
                connectWith: '.connected'
            });
		});
	</script>
</body>
<!-- END BODY -->
</html>