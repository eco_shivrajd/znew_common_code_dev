<?php
include ("../../includes/config.php");
$order_variant_id=$_GET["order_variant_id"]; 

  $sql="SELECT vo.*,p.productname,oa.lat,oa.order_no,oa.order_date,c.categorynm,c.brandid,b.name as brandname, 
 oa.long,s.name AS shopname,(SELECT u.firstname  FROM tbl_user AS u 
 WHERE u.id = oa.ordered_by) AS salespfullnm, 
 oa.shop_id,u1.firstname AS parent_name,u1.user_type as parent_user_type,u1.user_role as parent_user_role ,  
 oa.ordered_by 
 FROM `tbl_order_details` AS vo 
 INNER JOIN tbl_orders AS oa ON  vo.order_id = oa.id 
 LEFT JOIN `tbl_shops` AS s ON s.id = oa.shop_id 
 LEFT JOIN `tbl_category` AS c ON c.id = vo.cat_id 
 LEFT JOIN `tbl_brand` AS b ON b.id = c.brandid 
 LEFT JOIN `tbl_user` AS u1 ON u1.id = oa.order_to 
 LEFT JOIN tbl_product AS p ON vo.product_id=p.id where vo.id = '".$order_variant_id."'";
$result1 = mysqli_query($con,$sql);
$rowcount = mysqli_num_rows($result1);
if($rowcount > 0)
{
	$row_product_count_wt = 0;
	$row = mysqli_fetch_assoc($result1);
	/*print_r($row);*/
	/*Check for free or sale*/
	if($row['campaign_sale_type'] == 'free' && $row['campaign_type'] == 'by_weight')
	{
		$sql_product="SELECT total_wt_purchase, total_wt_purchase_unit 
		FROM `tbl_order_purchase_p_weight` where order_variant_id  = '".$order_variant_id."'";
								
		$result_product = mysqli_query($con,$sql_product);
		$row_product_count=mysqli_num_rows($result_product);
		$row_product=mysqli_fetch_assoc($result_product);	
	}else if($row['campaign_sale_type'] == 'free')
	{
		$sql_product="SELECT c_p_order_variant_id 
		FROM `tbl_order_c_n_f_product` where f_p_order_variant_id = '".$order_variant_id."'";
								
		$result_product = mysqli_query($con,$sql_product);
		$row_product_count=mysqli_num_rows($result_product);
		$row_product=mysqli_fetch_assoc($result_product);	
		$sql="SELECT *,p.productname, vo.id AS order_variant_id 
		FROM `tbl_order_details` AS vo INNER JOIN tbl_orders AS oa ON  vo.order_id = oa.id LEFT JOIN tbl_product AS p ON vo.product_id=p.id where vo.id = '".$row_product['c_p_order_variant_id']."'";
		$result1 = mysqli_query($con,$sql);
		$rowcount = mysqli_num_rows($result1);
		$row = mysqli_fetch_assoc($result1);
		$order_variant_id=$row["order_variant_id"]; 
	}else{
		$sql_product="SELECT id FROM `tbl_order_details` 
		where order_id  = '".$row['order_id']."' AND campaign_type ='by_weight'";								
		$result_product = mysqli_query($con,$sql_product);
		$row_product_count_wt=mysqli_num_rows($result_product);	
		$row_product=mysqli_fetch_assoc($result_product);
		if($row_product_count_wt != 0){
			$sql_product="SELECT total_wt_purchase, total_wt_purchase_unit FROM `tbl_order_purchase_p_weight` where order_variant_id  = '".$row_product['id']."'";
									
			$result_product = mysqli_query($con,$sql_product);
			$row_product_count=mysqli_num_rows($result_product);
			$row_product=mysqli_fetch_assoc($result_product);
		}			
	}
	
	
	$unit_price = $row['totalcost'];
	$quantity=$row['variantunit']*$row['totalcost'];
	$discount = "";
	
	$oplacelat = $row['oplacelat'];
	$oplacelon = $row['oplacelon'];
	$product_varient_id = $row['product_varient_id'];
?>
	  <div class="modal-header">
	  
	  <button type="button" name="btnPrint" id="btnPrint" onclick="takeprint()" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
	  <? if($oplacelat!="" && $oplacelon!="" && $oplacelat!="0.0" && $oplacelon!="0.0") { ?>
	  &nbsp;&nbsp;
	  <button type="button" name="btnPrint" id="btnPrint" onclick="showGoogleMap('<?=$oplacelat;?>','<?=$oplacelon;?>')" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Location</button>
	  <? } ?>
	  
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>	   
      </div>
		<div class="modal-body" style="padding-bottom: 5px !important;" id="divPrintARea">
		<div class="row">
		<div class="col-md-12">   
			<div class="portlet box blue-steel">
				<div class="portlet-title ">
					<div class="caption printHeading">
						Product: <?=$row['productname'];?>
					</div>                          
				</div>
				<div class="portlet-body">
					<table class="table table-striped table-bordered table-hover" id="sample_2" width="100%">
					<tbody>
						<tr>
							<th>Brand Name</th>
							<td><?=$row['brandname'];?></td>
						</tr>
						<tr>
							<th>Category Name</th>
							<td><?=$row['categorynm'];?></td>
						</tr>
						<tr>
							<th>Product Name</th>
							<td><?=$row['productname'];?></td>
						</tr>
						<tr>
							<th>Shop Name</th>
							<td><?=$row['shopname'];?></td>
						</tr>
						<tr>
							<th>Sales Person Name</th>
							<td><?=$row['salespfullnm'];?></td>
						</tr>
							

						<?php if($row['parent_user_type']!="") { ?>
							<tr><th>User Name (Role)</th>
							<td><?php echo $row['parent_name'];?> (<?php echo $row['parent_user_role'];?>)</td></tr>
						<? } ?>
						<tr>
							<th>Order Id</th>
							<td><?=$row['order_no'];?></td>
						</tr>
						<tr>
							<th>Order Date</th>
							<td><?=date('d-m-Y H:i:s',strtotime($row['order_date']));?></td>
						</tr>
						<!--<tr>
							<th>Variant Weight</th>
							<td>
								<?//$row['weightquantity']." ".$row['unit'];
								?>
							</td>
						</tr>-->
						
						<?
						$product_varient_id = $row["product_varient_id"];
						$sqlprd="SELECT variant_1, variant_2 FROM `tbl_product_variant` WHERE id = '$product_varient_id' ";
						$resultprd = mysqli_query($con,$sqlprd);
						$rowprd = mysqli_fetch_array($resultprd);
						
						$exp_variant1 = $rowprd['variant_1'];
						$imp_variant1= split(',',$exp_variant1);
						
						/*$exp_variant2 = $rowprd['variant_2']; $imp_variant2= split(',',$exp_variant2);*/
						if($imp_variant1[1]!="") {
						$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
						$resultunit = mysqli_query($con,$sql);
						$rowunit = mysqli_fetch_array($resultunit);
						$variant_unit1 = $rowunit['unitname'];
						
						/*
						$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant2[1]'";
						$resultunit = mysqli_query($con,$sql);
						$rowunit = mysqli_fetch_array($resultunit);
						$variant_unit2 = $rowunit['unitname'];*/
						?>
						<tr>
							<th>Variant Weight</th>
							<td>
								<?//$imp_variant1[0];
								?> 
								<?=$imp_variant1[0]." ".$variant_unit1;
								?>
							</td>
						</tr>
						<? } ?>
						 
						<? if($row['campaign_applied'] == 0){  ?>							
							<? if($row['campaign_type'] == 'discount')
								{
									$sql_p_discount="SELECT campaign_id, discount_amount, discount_percent, actual_amount FROM `tbl_order_cp_discount` where order_variant_id = '".$order_variant_id."'";
									$result_p_discount = mysqli_query($con,$sql_p_discount);
									$row_p_discount_count=mysqli_num_rows($result_p_discount);
									$row_p_discount=mysqli_fetch_assoc($result_p_discount);
									if($row_p_discount_count > 0)
									{
										$unit_price = $row_p_discount['actual_amount'];
										$discount = $row_p_discount['discount_percent'];
										$quantity_price = $row['variantunit'] * $unit_price ;
										$discount_percent = $discount."%";
										$discount_amount = (($quantity_price * $row_p_discount['discount_percent'])/100);
										$quantity = $quantity_price - $discount_amount;
									
									?>									
								<?	 }
								}
								if($row['campaign_type'] == 'free_product')
								{
									if($row['campaign_sale_type'] == 'sale') 
									$sql_c_n_f_product="SELECT campaign_id, c_p_order_variant_id, f_p_order_variant_id FROM `tbl_order_c_n_f_product` where c_p_order_variant_id = '".$order_variant_id."'";
									
									$result_c_n_f_product = mysqli_query($con,$sql_c_n_f_product);
									$row_c_n_f_product_count=mysqli_num_rows($result_c_n_f_product);
									$i=1;
									if($row_c_n_f_product_count > 0)
									{
										while($row_pro = mysqli_fetch_array($result_c_n_f_product)){
										$sql_free_pro="SELECT vo.*,p.productname, c.categorynm, b.name FROM `tbl_order_details` AS vo LEFT JOIN tbl_product AS p ON vo.product_id=p.id LEFT JOIN tbl_category AS c ON c.id = p.catid LEFT JOIN tbl_brand AS b ON b.id = c.brandid WHERE vo.id = '".$row_pro['f_p_order_variant_id']."'";
										$result_free_pro = mysqli_query($con,$sql_free_pro);
										$rowcount_free_pro = mysqli_num_rows($result_free_pro);
										$row_free_pro = mysqli_fetch_assoc($result_free_pro);
										$free_amount_data[$i]['brandname'] = $row_free_pro['name'];
										$free_amount_data[$i]['catname'] = $row_free_pro['categorynm'];
										$free_amount_data[$i]['productname'] = $row_free_pro['productname'];
										$free_amount_data[$i]['variantunit'] = $row_free_pro['variantunit'];
										$free_amount_data[$i]['totalcost'] = $row_free_pro['totalcost'];
										$free_amount_data[$i]['totalcost'] = $row_free_pro['totalcost'];
									?>														
										<tr>
											<th>Free Product</th>
											<td>
												<?=$row_free_pro['name'].": ".$row_free_pro['categorynm'].": ".$row_free_pro['productname'];?>
											</td>
										</tr>	
										<tr>
											<th>Free Product Variant Weight</th>
											<td>
												<?=$row_free_pro['variantweight'];?>
											</td>
										</tr>			
										<?	$i++;}
									}
								}
								if($row['campaign_type'] == 'by_weight')
								{ ?>
									<tr>											
											<td colspan="2">
												This product is provided as free on purchase of "<?=$row_product['total_wt_purchase'];?> <?=$row_product['total_wt_purchase_unit'];?>" of product under total sales order.
											</td>
										</tr>		
								<?php } 
								
							} 
						?>		
						<?php 
						if($row_product_count_wt != 0)
						{ ?>
							<tr>											
									<td colspan="2">
										This product's sales order has free product (As total sales order is of weight "<?=$row_product['total_wt_purchase'];?> <?=$row_product['total_wt_purchase_unit'];?>").
									</td>
								</tr>		
						<?php } ?>
						</tbody>
						</table>
						<table class="table table-striped table-bordered table-hover">
						<tbody>
						<tr>
							<? if($row_c_n_f_product_count > 0){?><th width="30%" style="text-align: center !important;">Product Name</th><? } ?>
							<th width="5%" style="text-align: center !important;">Quantity</th>
							<th width="25%" style="text-align: center !important;">Unit Price ₹</th>
							<? if($discount != '') {?><th width="10%" style="text-align: center !important;">Discount</th><? } ?>
							<? if($discount != '') {?><th width="25%" style="text-align: center !important;">Discount Amount ₹</th><? } ?>
							<th width="25%" style="text-align: center !important;">Total Cost ₹</th>
						</tr>
						<tr>
							<? if($row_c_n_f_product_count > 0){?><td align="left"><?=$row['productname'];?></td><? } ?>
							<td align="right"><?=$row['product_quantity'];?></td>
							<td align="right"><?=number_format($row['product_unit_cost'],2, '.', '');?></td>
							<? if($discount != '') {?><td align="right"><?=$discount_percent;?></td><? } ?>
							<? if($discount != '') {?><td align="right"><?=number_format($discount_amount,2, '.', '');?></td><? } ?>
							<td align="right"><?=number_format($row['p_cost_cgst_sgst'],2, '.', '');?></td>
						</tr>
						<? if($row_c_n_f_product_count > 0)
						{	foreach($free_amount_data as $key => $val){
						
						?>
						<tr>
							<td align="left"><?=$val['productname'];?></td>
							<td align="right"><?=$val['variantunit'];?></td>
							<td align="right"><?=number_format($val['totalcost'],2, '.', '');?></td>							
							<td align="right"><?=number_format($val['totalcost'],2, '.', '');?></td>
						</tr>
					<? } } ?>
					 
					</tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
</div>
<?}
mysqli_close($con); ?>