<div class="form-group">
	  <label class="col-md-3">State:<span class="mandatory">*</span></label>
	  <div class="col-md-4">
		  <select name="state_campaign_discount[]" id="state_campaign_discount" data-parsley-trigger="change" class="form-control" onChange="fnShowCity(this.value)" multiple>		  
			<?php
			$sql="SELECT * FROM tbl_state where country_id=101";
			$result = mysqli_query($con,$sql);
			while($row = mysqli_fetch_array($result))
			{
			$cat_id=$row['id'];
			echo "<option value='$cat_id'>" . $row['name'] . "</option>";
			}
			?>
			</select>
	  </div>
	</div><!-- /.form-group -->
	<div class="form-group">
		  <label class="col-md-3">District:</label>
		  <div class="col-md-4" id="div_city_campaign_discount">
		  <select name="city_campaign_discount" id="city_campaign_discount" data-parsley-trigger="change" class="form-control">
			<option selected disabled>-Select-</option>										
			</select>
		  </div>
	</div><!-- /.form-group -->
	
	<div class="form-group">
	  <label class="col-md-3">Taluka:</label>
	  <div class="col-md-4" id="div_suburb_campaign_discount">
	  <select name="suburb_campaign_discount" id="suburb_campaign_discount" data-parsley-trigger="change" class="form-control">
		<option selected disabled>-Select-</option>									
		</select>
	  </div>
	</div><!-- /.form-group --> 
	<div class="form-group">
	  <label class="col-md-3">Subarea:</label>
	  <div class="col-md-4" id="div_select_subarea">
	  <select name="subarea" id="subarea" data-parsley-trigger="change" class="form-control">
		<option selected value="">-Select-</option>									
		</select>
	  </div>
	</div><!-- /.form-group --> 
	
<script>
function setSelectNoValue(div,select_element){
	var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled>-Select-</option></select>';
	$("#"+div).html(select_selement_section);
}
function calculate_data_count(element_value){
	element_value = element_value.toString();
	var element_arr = element_value.split(',');	
	return element_arr.length;
}
function fnShowCity(id_value) {	
	var enabled_div = $("#enabled_div").val();	
	
	var state_str = $("#state_campaign_discount:enabled").val(); 	
	var state_arr_count = calculate_data_count(state_str);
	if(state_arr_count == 1){//If single state selected then only show its related Cities
		$("#city_campaign_discount:enabled").html('<option value="">-Select-</option>');
		$("#city_campaign_discount:enabled").removeAttr('multiple');
		$("#suburb_campaign_discount:enabled").html('<option value="">-Select-</option>');
		$("#suburb_campaign_discount:enabled").removeAttr('multiple');
		$("#subarea:enabled").html('<option value="">-Select-</option>');
		$("#subarea:enabled").removeAttr('multiple');
		$("#shop_campaign_discount:enabled").html('<option value="">-Select-</option>');
		$("#shop_campaign_discount:enabled").removeAttr('multiple');
		var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city_campaign_discount&multiple=multiple";//alert($('#free_product_div > div > #div_city_campaign_discount').html());
		CallAJAX(url,enabled_div+" > div > #div_city_campaign_discount");
	}else{
		setSelectNoValue(enabled_div+" > div > #div_city_campaign_discount", "city_campaign_discount");
		setSelectNoValue(enabled_div+" > div > #div_suburb_campaign_discount", "suburb_campaign_discount");
		setSelectNoValue(enabled_div+" > div > #div_select_subarea", "subarea");
		setSelectNoValue(enabled_div+" > div > #div_shop_campaign_discount", "shop_campaign_discount");
	}	
}
function FnGetSuburbDropDown(id) {
	var enabled_div = $("#enabled_div").val();	
	var city_str = $("#city_campaign_discount:enabled").val();	
	var city_arr_count = calculate_data_count(city_str);
	if(city_arr_count == 1){//If single city selected then only show its related suburb
		$("#suburb_campaign_discount:enabled").html('<option value="">-Select-</option>');
		$("#suburb_campaign_discount:enabled").removeAttr('multiple');
		$("#subarea:enabled").html('<option value="">-Select-</option>');
		$("#subarea:enabled").removeAttr('multiple');
		$("#shop_campaign_discount:enabled").html('<option value="">-Select-</option>');
		$("#shop_campaign_discount:enabled").removeAttr('multiple');
		var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=suburb_campaign_discount&multiple=multiple&function_name=FnGetSubareaDropDown";
		CallAJAX(url,enabled_div+" > div > #div_suburb_campaign_discount");//alert($("#"+enabled_div+" > div > #div_suburb_campaign_discount").html());
	}else{
		setSelectNoValue(enabled_div+" > div > #div_suburb_campaign_discount", "suburb_campaign_discount");
		setSelectNoValue(enabled_div+" > div > #div_select_subarea", "subarea");
		setSelectNoValue(enabled_div+" > div > #div_shop_campaign_discount", "shop_campaign_discount");
	}		
}
function FnGetSubareaDropDown(id) {	
	var enabled_div = $("#enabled_div").val();	
	var suburb_str = $("#suburb_campaign_discount:enabled").val();
	var suburb_arr_count = calculate_data_count(suburb_str);
	if(suburb_arr_count == 1){//If single city selected then only show its related subarea			
		$("#subarea:enabled").html('<option value="">-Select-</option>');
		$("#subarea:enabled").removeAttr('multiple');
		$("#shop_campaign_discount:enabled").html('<option value="">-Select-</option>');
		$("#shop_campaign_discount:enabled").removeAttr('multiple');
		var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea&multiple=multiple";
		CallAJAX(url,enabled_div+" > div > #div_select_subarea");
	}else{
		setSelectNoValue(enabled_div+" > div > #div_select_subarea", "subarea");		
		setSelectNoValue(enabled_div+" > div > #div_shop_campaign_discount", "shop_campaign_discount");
	}		
}


</script>