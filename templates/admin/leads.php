<!-- BEGIN HEADER -->
<?php header("Cache-Control: no-store, must-revalidate, max-age=0");
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include "../includes/grid_header.php";
include "../includes/shopManage.php";
include "../includes/userManage.php";
$shopObj 	= 	new shopManager($con,$conmain);
$userObj 	= 	new userManager($con,$conmain);
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Leads";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<h3 class="page-title">Leads</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li><i class="fa fa-home"></i>
					<a href="#">Leads</a></li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
						
							<div class="caption">Lead Listing</div>
							
						<!-- 	<? if($_SESSION[SESSION_PREFIX."user_type"]=="Admin")  { ?>
								<a href="leads-add.php" class="btn btn-sm btn-default pull-right mt5">Add Lead</a>
							<? } ?> -->
							
                            <div class="clearfix"></div>    
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_2">
								<thead>
									<tr>
										<th>
											 Shop Name
										</th>
										<!-- <?php if($_SESSION[SESSION_PREFIX.'user_type'] != 'Superstockist'){ ?>
										<th>
											 Super Stockist
										</th>
										<?php } ?>
										<?php if($_SESSION[SESSION_PREFIX.'user_type'] != 'Distributor'){ ?>
										<th>
											 Stockist
										</th>
										<?php } ?> -->
										<th>
											 Sales Person
										</th>
										<th>
											 Contact Person
										</th>
										<th>
											 Mobile Number
										</th>
										<th>
											Taluka
										</th>
										<th>
											District
										</th>
										<th>
											State
										</th>
										<th>
											Reminder
										</th>
										<th>
											Visited Date
										</th>
										<th>
											Comment
										</th>										
										<th>
										     Status
										</th>
										<th style="width: 80px;">
										     Action
										</th>
									</tr>
								</thead>
							<tbody>
							<?php	
					       $result1 = $shopObj->getShopLeads();
							if( $result1!=0){
							while($row = mysqli_fetch_array($result1))
							{
								$shop_id = $row['id'];
								$lead_id = $row['id'];
								$notification = '';
								$visited_date = '';
								$reminder = '';
								
								$sql3 = "SELECT max(lead_status) as lead_status,max(id) as lead_detail_id FROM `tbl_lead_details` where `shop_id` = $shop_id";
								 $result3 = mysqli_query($con, $sql3);
								 while($row3 = mysqli_fetch_array($result3))
								 {
									 $lead_status=$row3['lead_status'];
									 $lead_detail_id=$row3['lead_detail_id'];
								 }
								 $sql1 = "SELECT `id`, `shop_id`,`reminder`,`visited_date`, `notification`, `created_on`,`lead_status` FROM `tbl_lead_details` where `shop_id` = $shop_id";
								 $result2 = mysqli_query($con, $sql1);
								 while($row2 = mysqli_fetch_array($result2))
								 {	
									//print_r($row2);  
									$notification.=$row2['notification']."<br><br>";
									$visited_date.=date('d-m-Y h:i:s A', strtotime($row2['visited_date']))."<br><br>";
									if($row2['lead_status']=='1'){
										//$reminder.=$row2['reminder']."<br><br>";
										$reminder.=date('d-m-Y h:i:s A', strtotime($row2['reminder']))."<br><br>";   
									}                                              
								 }
                           ?>
							<tr class="odd gradeX" >
								<td>
								<?php if ($ischecked_edit==1) { ?>
									<a href="lead_update.php?id=<?php echo base64_encode($lead_id);?>"><?php echo $row['name'];?></a>
								<?php }else{  echo $row['name']; } ?> 
								</td> 
									<td><?php echo $row['salesperson_name'];?></td>
									<td><?php echo $row['contact_person'];?></td>
									<td align="right"><?php echo $row['mobile'];?></td> 
									<td><?php echo $row['area_name'];?></td> 
									<td><?php echo $row['city_name'];?></td> 
									<td><?php echo $row['state_name'];?></td> 
									<td><?php echo $reminder;?> </td> 
									<td><?php echo $visited_date;?>
									</td> 
									<td><?php echo $notification;?></td> 
									<td>
                                        <?php $lead_status_array=array();
										$lead_status_array= array('1'=>'Pending','2'=>'Confirmed','3'=>'Cancelled','4'=>'Added In Shop');
											if($lead_status==4){ ?>
												<a onclick="showOrderDetails('<?php echo $lead_detail_id; ?>');" >
												<?php echo $lead_status_array[$lead_status];   ?>  <span class="glyphicon glyphicon-eye-open"></span>
												</a>
											<?php }else{ echo $lead_status_array[$lead_status]; } ?>	
									</td> 
									<td>
									 <?php if ($ischecked_edit==1) { ?>
										   <a title="Edit" href="lead_update.php?id=<?php echo base64_encode($lead_id);?>" class="btn btn-xs btn-primary ">
															<span class="glyphicon glyphicon-edit"></span></a>
									<?php } else{ ?>
										<?php echo '<a title="Can Not Edit This Record" href="#" class="btn btn-xs btn-primary" disabled><span class="glyphicon glyphicon-edit"></span></a>'; ?>
									<?php }  if ($ischecked_delete==1)  {	?>
											<a title="Delete" onclick="javascript: deleteProduct(<?php echo $lead_id;?>)"
											  class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
									<?php }	else{?>
										<a title="Can Not Delete This Record" onclick="#" class="btn btn-xs btn-danger" disabled>
										<span class="glyphicon glyphicon-trash"></span></a>
									<?php  } ?> 
									</td>
                               </tr>		
                            <?php
							} 
						} ?>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<div class="modal fade" id="lead_details" role="dialog">
        <div class="modal-dialog" style="width: 880px !important;">
            <!-- Modal content-->
            <div class="modal-content" id="lead_details_content">
            </div>
        </div>
    </div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
function showOrderDetails(lead_detail_id) {   
            var url = "lead_details_popup.php";
            jQuery.ajax({
                url: url,
                method: 'POST',
                data: 'lead_details_id=' + lead_detail_id,
                async: false
            }).done(function (response) {
				console.log(response);
                $('#lead_details_content').html(response);
                $('#lead_details').modal('show');
            }).fail(function () { });
            return false;
        }
</script>
<script>
	function deleteProduct(lead_id) {
		if (confirm('Are you sure you want to delete this lead?')) {
			CallAJAX('ajax_product_manage.php?action=delete_lead&lead_id=' + lead_id );
		}
	}

	function CallAJAX(url) {
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				alert('Lead deleted successfully.');
				location.reload();
			}
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}
</script>
</body>
<!-- END BODY -->
</html>