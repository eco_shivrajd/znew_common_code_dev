<!-- BEGIN HEADER -->
<?php 
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include "../includes/grid_header.php";
include "../includes/userConfigManage.php";   
$userConfObj    =   new userConfigManager($con,$conmain);
$profile_id1 = $_GET['profile_id1'];
$user_type = $_GET['user_type'];
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "UserType";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">		
			<h3 class="page-title"><?php echo $user_type;?> Role Profile</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li><i class="fa fa-home"></i>
					<a href="#"><?php echo $user_type;?> Role Profile</a></li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">				
					<div class="portlet box blue-steel">
						<div class="portlet-title">	
							<div class="caption"><i class="icon-puzzle"></i>Profile view</div>
								<button type="submit" name="submit" id="submit" onclick="profile_conf_save();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Save</button> &nbsp;
								&nbsp;
								<button type="button" name="btnCancel" id="btnCancel" onclick="document.location.href='user_profile_config.php?profile_id1=<?=$profile_id1;?>&user_type=<?=$user_type;?>'" class="btn btn-primary pull-right" 
								style="margin-top: 3px; margin-right: 5px;"> Cancel</button>
							</div>								
                            <div class="clearfix"></div>
						
						<div class="portlet-body">
							<form id="myform" class="form-horizontal" data-parsley-validate="" role="form" method="post" >  
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										 <th width="25%" style="text-align: left !important">
											<label>
												<!--<input class="verticalAlignMiddle" checked="true" type="checkbox" id="mainModulesCheckBox" style="top: -2px;">-->&nbsp; Modules
											</label>
										</th>
										<th class="textAlignCenter" width="14%">
											<label>
												<!--<input class="verticalAlignMiddle" type="checkbox" id="mainAction4CheckBox" style="top: -2px;">-->&nbsp; View
											</label>
										</th>
										<th class="textAlignCenter" width="14%">
											<label>
												<!--<input class="verticalAlignMiddle" type="checkbox" id="mainAction7CheckBox" style="top: -2px;">-->&nbsp; Create
											</label>
										</th>
										<th class="textAlignCenter" width="14%">
											<label>
												<!--<input class="verticalAlignMiddle" type="checkbox" id="mainAction1CheckBox" style="top: -2px;">-->&nbsp; Edit
											</label>
										</th>
										<th class="textAlignCenter" width="14%">
											<label>
												<!--<input class="verticalAlignMiddle" checked="true" type="checkbox" id="mainAction2CheckBox" style="top: -2px;">-->&nbsp; Delete
											</label>
										</th> 
									</tr>
								</thead>
							<tbody>
								
							<?php
							//$profile_id=$_SESSION[SESSION_PREFIX."user_id"];
					        $sql = "SELECT id,page_name FROM `tbl_pages` where isdeleted!=1 ";
                            $result = mysqli_query($con, $sql);
							$num_rows=mysqli_num_rows($result);
							$profile_checkboxes=$userConfObj->get_action_profile_checkbox($profile_id1);
							while($row = mysqli_fetch_array($result))
							{		
								$keystring=$profile_id1.''.$row['id'];									
								$ischecked_view=$profile_checkboxes[$keystring]['ischecked_view'];
								$ischecked_add=$profile_checkboxes[$keystring]['ischecked_add'];
								$ischecked_edit=$profile_checkboxes[$keystring]['ischecked_edit'];
								$ischecked_delete=$profile_checkboxes[$keystring]['ischecked_delete'];
						?>
								<tr class="odd gradeX">
									<td class="verticalAlignMiddleImp">
									  &nbsp;<?php echo  $row['page_name'];	?>
									</td>
									<td>
										<input  class="action4CheckBox" type="checkbox" id="<?php echo $row['id'];?>"
										name="checkbox_view[]" data-action-state="DetailView" 
										<?php  if($ischecked_view==1){ ?>checked="true"<?php }else{ ?> data-action1-unchecked="true" <?php } ?>>
									</td>
									<td>
										<input  class="action4CheckBox" type="checkbox" id="<?php echo $row['id'];?>"
										name="checkbox_add[]" data-action-state="CreateView" 
										<?php  if($ischecked_add==1){ ?>checked="true"<?php }else{ ?> data-action1-unchecked="true" <?php } ?>>
									</td>
									<td>
										<input  class="action4CheckBox" type="checkbox" id="<?php echo $row['id'];?>"
										name="checkbox_edit[]" data-action-state="EditView" 
										<?php  if($ischecked_edit==1){ ?>checked="true"<?php }else{ ?> data-action1-unchecked="true" <?php } ?>>
									</td>
									<td>
										<input class="action4CheckBox" type="checkbox" id="<?php echo $row['id'];?>"
										name="checkbox_delete[]" data-action-state="Delete" 
										<?php  if($ischecked_delete==1){ ?>checked="true"<?php }else{ ?> data-action1-unchecked="true" <?php } ?>>
									</td>								
								</tr>
						<?php	} ?>	
							</tbody>
							</table>
							<input type="hidden" name="update_profile_id" id="update_profile_id" value="<?php echo $profile_id1;?>">
							<input type="hidden" name="total_record" id="total_record" value="<?php echo $num_rows;?>">
							</form>
						</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
function profile_conf_save() {	
	var checkbox_views = [];
	var checkbox_adds = [];
	var checkbox_edits = [];
	var checkbox_deletes = [];
	$('input[name="checkbox_view[]"]:checked').each(function () {
		checkbox_views.push(this.id);
	});	
	$('input[name="checkbox_add[]"]:checked').each(function () {
		checkbox_adds.push(this.id);
	});
	$('input[name="checkbox_edit[]"]:checked').each(function () {
		checkbox_edits.push(this.id);
	});	
	$('input[name="checkbox_delete[]"]:checked').each(function () {
		checkbox_deletes.push(this.id);
	});	
	var update_profile_id=$('#update_profile_id').val();
	//alert(update_profile_id);
	var total_record=$('#total_record').val();
	//var dataString = 'checkbox_views='+ checkbox_views + '&checkbox_adds='+ checkbox_adds + '&checkbox_edits='+ checkbox_edits + '&checkbox_deletes='+ checkbox_deletes+'&update_profile_id='+update_profile_id+'&total_record='+total_record;
	//console.log(dataString);
	var url = "update_profile_config.php";
	jQuery.ajax({
		url: url,
		method: 'POST',
		data:  { checkbox_views: checkbox_views, checkbox_adds: checkbox_adds,checkbox_edits: checkbox_edits, checkbox_deletes: checkbox_deletes,update_profile_id: update_profile_id, total_record: total_record },
		async: false
	}).done(function (response) {
		//console.log(response);
		if(response>0){
			alert("Profile status changes successfully!");
			//window.location.replace("user_type_list.php");
			window.location.replace("user_profile_config.php?profile_id1=<?php echo $profile_id1;?>&user_type=<?php echo $user_type;?>")
		}else{
			alert("Profile not updated!");
			location.reload();
		}		
	}).fail(function () { });
	
	return false;
}
</script>
</body>
<!-- END BODY -->
</html>