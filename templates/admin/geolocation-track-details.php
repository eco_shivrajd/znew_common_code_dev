<?php
include "../includes/grid_header.php";
include "../includes/userManage.php";
$userObj = new userManager($con, $conmain);
$where = "";

    $dropdownState = fnEncodeString($_POST['dropdownState']);
    $city = fnEncodeString($_POST['city']);
    $area = fnEncodeString($_POST['area']);
    $subarea = fnEncodeString($_POST['subarea']);
    if ($dropdownState != ""){
        $where .= " AND state_id = '" . $dropdownState . "' ";
    }
    if ($city != "" && $city != "null"){
        $where .= " AND city_id = '" . $city . "' ";
    }
    if ($area != "" && $area != "null"){
        $where .= " AND suburbid = '" . $area . "' ";
    }
    if ($subarea != "" && $subarea != "null"){
        $where .= " AND subarea_id = '" . $subarea . "' ";
    }

$seesion_user_id = $_SESSION[SESSION_PREFIX."user_id"];
$user_role = $_SESSION[SESSION_PREFIX.'user_role'];
        if ($user_role =='Accountant') 
        {
                $seesion_user_id =1;
        }
//$sql="SELECT id , name FROM tbl_shops WHERE 1=1 ";
  $sqlshop="SELECT id,shop_name AS shopname,shop_added_by_name AS firstname,cityname,state_id,city_id,suburbid,subarea_id,latitude,longitude 
                           FROM `tbl_shop_view` where (find_in_set('".$seesion_user_id."',parent_ids) <> 0 OR shop_added_by=$seesion_user_id)
                           AND latitude!='' and latitude is not null and longitude!='' and  longitude is not null and latitude!='0.0' and longitude!='0.0'   $where ";
$resultshop = mysqli_query($con, $sqlshop);
$totalshops = mysqli_num_rows($resultshop);
$array_shops[] = array();
if ($totalshops > 0) {

    $i = 0;
    while ($row = mysqli_fetch_array($resultshop)) {
        $array_shops[$i]['lat'] = $row["latitude"];
        $array_shops[$i]['lng'] = $row["longitude"];
        $array_shops[$i]['title'] = "Shop Name:-" . $row['shopname'] . "";
        $array_shops[$i]['firstname'] = $row["firstname"];
        $i++;
    }
}
?>

<style>
    body {
        margin: 0;
    }

    #dvMap {
        height: 0;
        overflow: hidden;
        padding-bottom: 22.25%;
        padding-top: 30px;
        position: relative;
    }

    .dvMap-frame {
        left: 0;
        top: 0;
        height: 100%;
        width: 100%;
        position: absolute;
    }

    .kd-tabbed-vert.header-links .kd-tabbutton a {
        color: #757575;
        display: inline-block;
        height: 100%;
        padding: 0 24px;
        width: 100%;
    }

    .kd-tabbed-vert.header-links .kd-tabbutton {
        padding: 0;
    }

    .kd-tabbed-vert.header-links .kd-tabbutton.selected a {
        color: #03a9f4;
    }

    .kd-tabbed-vert.header-links .kd-tabbutton a:focus {
        text-decoration: none;
    }

    p.top-desc {
        padding: 1em 1em .1em 1em;
    }

    p.bottom-desc {
        padding: 0em 1em 1em 1em;
    }
</style>
<?php
if ($totalshops > 0) {
    ?>
 <div  id="dvMap" style="min-height:480px">   </div>

<?php } else { ?>
                                            
<h3 align="center" style="font-size: 13px;">No Shop Available.</h3>
                                                    
<?php } ?>
<script>
    jQuery(document).ready(function () {

        ComponentsPickers.init();
    });

   
</script>
         <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDMpdO43txdg_zyovvZm9i1tMMFIQTKTU&callback=initMap">
        </script>         
        <script>
            var markers_distr =<?= json_encode($array_shops); ?>;
            var markers = new Array();
        //console.log(markers_distr);
        //var markers_distr=JSON.stringify(json); 
            var markerCluster = new Array();


             function initMap() {

                // alert('hi');
                // console.log(obj1);
                for (var i = markers_distr.length - 1; i >= 0; i--)
                {

                    var obj = markers_distr[i];
                    var obj1 = markers_distr[i]['lat'];
                    var obj2 = markers_distr[i]['lng'];
                    var title = markers_distr[i]['title'];
                    //  console.log(obj2);
                    // console.log(obj.lat);
                        var myLatlng = new google.maps.LatLng(obj1, obj2);
                    //  console.log(myLatlng);
                        var myOptions = {
                              zoom: 5,
                        minZoom: 5,
                              center: myLatlng,
                              mapTypeId: google.maps.MapTypeId.ROADMAP
                        }
                        var map = new google.maps.Map(document.getElementById("dvMap"), myOptions);

                        var marker = new google.maps.Marker({
                                position: myLatlng, 
                                map: map,
                                title: title
                        });  
         
                    markers.push(marker);

                  }
                console.log(markers);
                var markerCluster = new MarkerClusterer(map, markers,
                        {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
            }
        </script>