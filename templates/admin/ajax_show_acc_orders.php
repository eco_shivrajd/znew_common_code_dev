<?php 
include ("../../includes/config.php");
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
$order_status = $_POST['order_status'];
$orders = $orderObj->getAccOrderschnage($order_status);
$order_count = count($orders);
?>
<div class="clearfix"></div>
<table class="table table-striped table-bordered table-hover" id="sample_2">
				<thead>
					<tr id="main_th">		
		<th>
			Region
		</th>
		<th>
			Order From
		</th>
		<th>
			Order To
		</th>
		<th>
			Order Id<br>Order Date 
		</th>								
		
		 <th>
			Product Name			
		</th>	
		<th>
			Quantity
		</th>
		<th>
			Total Price (₹)
		</th>
		<th>
			Price(₹)<br>(GST+Discount)
		</th>
		<th>
			Action  
		</th>
	</tr>
				</thead>
				<tbody>
				<?php				
				foreach($orders as $key=>$value)
				{
					$orderdetails = $orderObj->getOrdersDetailschnage($value['oid']);	
					$oid = $value['oid'];
					$product_name='';
					$prod_qnty='';$totalcost='';$gstcost='';
					if(count($orderdetails['order_details'])==1){						
						$product_name=$orderdetails['order_details'][0]['cat_name']."-".$orderdetails['order_details'][0]['product_name']."<br>";
						$prod_qnty='<a onclick="showOrderDetails(\''.$orderdetails['order_details'][0]['odid'].'\',\'Order Details\')" title="Order Details">'.$orderdetails['order_details'][0]['product_quantity'];
						if(!empty($orderdetails['order_details'][0]['product_variant_weight1'])){
							$prod_qnty.="(".$orderdetails['order_details'][0]['product_variant_weight1']."-".$orderdetails['order_details'][0]['product_variant_unit1'].")";
						}
						if(!empty($orderdetails['order_details'][0]['product_variant_weight2'])){
							$prod_qnty.="(".$orderdetails['order_details'][0]['product_variant_weight2']."-".$orderdetails['order_details'][0]['product_variant_unit2'].")";
						}
						$prod_qnty.="</a>";	
						$campaign_applied=$orderdetails['order_details'][0]['campaign_applied'];
						$campaign_type=$orderdetails['order_details'][0]['campaign_type'];
						if($campaign_applied == 0 && $campaign_type=='free_product'){
							$prod_qnty.="<span style='float: left'><img src='../../assets/global/img/free-icon.png' title='Free Product'></span>";	
						}
						$prod_qnty.="<br>";
						
						$totalcost=$orderdetails['order_details'][0]['product_total_cost'];
						if($orderdetails['order_details'][0]['discounted_totalcost']!=0 && $orderdetails['order_details'][0]['discounted_totalcost']!=''){
							$totalcost.="(<font color='green'>".$orderdetails['order_details'][0]['discounted_totalcost']."</font>)";
						}
						$totalcost.="<br>";
						
						$gstcost=$orderdetails['order_details'][0]['p_cost_cgst_sgst']."<br>";
					}else{
						foreach ($orderdetails['order_details'] as $key1=>$value1){
							$product_name.=$value1['cat_name']."-".$value1['product_name']."<br>";
							$prod_qnty.='<a onclick="showOrderDetails(\''.$value1['odid'].'\',\'Order Details\')" title="Order Details">'.$value1['product_quantity'];
							if(!empty($value1['product_variant_weight1'])){
								$prod_qnty.="(".$value1['product_variant_weight1']."-".$value1['product_variant_unit1'].")";
							}
							if(!empty($value1['product_variant_weight2'])){
								$prod_qnty.="(".$value1['product_variant_weight2']."-".$value1['product_variant_unit2'].")";
							}
							$prod_qnty.="</a>";
							$campaign_applied=$value1['campaign_applied'];
							$campaign_type=$value1['campaign_type'];
							if($campaign_applied == 0 && $campaign_type=='free_product'){
								$prod_qnty.="<span style='float: left'><img src='../../assets/global/img/free-icon.png' title='Free Product'></span>";	
							}
							$prod_qnty.="<br>";
							$totalcost.=$value1['product_total_cost'];
							if($value1['discounted_totalcost']!=0 && $value1['discounted_totalcost']!=''){
								$totalcost.="(<font color='green'>".$value1['discounted_totalcost']."</font>)";
							}
							$totalcost.="<br>";
							$gstcost.=$value1['p_cost_cgst_sgst']."<br>";
						}
					}
					
				?>
				<tr class="odd gradeX">				
				 
						<td ><?php if(!empty($value["region_name"])){echo $value["region_name"];}else{echo $value["region_name1"];} ?></td>
						<td ><?php echo $value["order_by_name"]."-".$value["shop_name"];?></td>					
						<td ><?php echo $value["order_to_name"];?></td>
						<td ><font size="1.4"><?php echo $value["order_no"]?></font><br><?php echo date('d-m-Y H:i:s',strtotime($value["order_date"]));?></td>										
						<td><?php echo $product_name;?></td>
						<td align="right"><?php echo $prod_qnty;?></td>
						<td align="right"><?php echo $totalcost;?></td>
						<td align="right"><?php echo $gstcost;?></td>
						<td>
						<a onclick='showInvoice(1,<?=json_encode($oid);?>)'  title="View Invoice">View Invoice</a>
						</td>
					</tr>
				<?php } ?>
				
				 </tbody>
			</table>
<script>
$(document).ready(function() {
	 $("#sample_2").dataTable().fnDestroy()

    $('#sample_2').dataTable( {
	order: [],
	columnDefs: [ { orderable: false, targets: [0] } ]
	});
});

</script>
