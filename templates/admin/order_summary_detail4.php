<?php
   include ("../../includes/config.php");
   include "../includes/common.php";
   include "../includes/orderManage.php";
   $orderObj = new orderManage($con, $conmain);
   $report_title = $orderObj->getReportTitle_forshopadded();   
   $row = $orderObj->get_all_shop_added_on_by();   
   $colspan = "4";
   ?>
<? if($_POST["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
   table, th, td {  border: 1px solid black; } 
   body { font-family: "Open Sans", sans-serif; 
   background-color:#fff;
   font-size: 11px;
   direction: ltr;}
</style>
<? }
   ?>
<table 
   class="table table-striped table-bordered table-hover table-highlight table-checkable" 
   data-provide="datatable" 
   data-display-rows="10"
   data-info="true"
   data-search="true"
   data-length-change="true"
   data-paginate="true"
   id="sample_2">
   <thead>
      <tr>
         <td colspan="<?= $colspan; ?>" align="canter" class="gradeX even" style="text-align:center; font-weight:600;">
            <h4><b><?php if (!empty($report_title)) echo $report_title;
               else echo "Shop Added Report All"; ?></b></h4>
         </td>
      </tr>
      <tr>
         <th data-filterable="false" data-sortable="true" data-direction="desc">Shop Added Date</th>
         <th data-filterable="false" data-sortable="false" data-direction="de">User Name</th>
         <!--  <th data-filterable="false" data-sortable="false" data-direction="desc">User Type</th> -->
         <th data-filterable="false" data-sortable="false" data-direction="de">Total Shops</th>
      </tr>
   </thead>
   <tbody>
      <?php
         if (!empty($row)) {
             $gtotalq = 0;
         
             foreach ($row as $key => $value) {              
                 $gtotalq = $gtotalq + $value['totalunit'];
                 if (!empty($value['totalunit'])) {
                     $unitqnty = $value['totalunit'];
                 } else {
                     $unitqnty = 0;
                 }
                 ?>
      <tr class="odd gradeX">
         <td><?php echo date('d-m-Y h:i:s A', strtotime($value['added_date'])); ?></td>
         <td><?=$value['firstname']; ?></td>
         <!--  <td><?=$value['user_type']; ?></td> -->
         <td align="right">
            <?php if($unitqnty>0){ ?> <a onclick="showShopListDetails(<?=$value['id']; ?>,<?php echo "'".date('Y-m-d', strtotime($value['added_date']))."'"; ?>,<?php echo "'".$value['firstname']."'"; ?>)" 
               title="Shop Details"><?=$unitqnty; ?>
            </a><?php }else {?> <?=$unitqnty; ?><?php } ?>
         </td>
      </tr>
		<?php } 
         }if ($_POST["actionType"] == "excel" && $row == 0) {
             echo "<tr class='odd gradeX'><td colspan='3'>No matching records found</td></tr>";
         }
		?>	
   </tbody>
</table>
<script>
   jQuery(document).ready(function () {
       ComponentsPickers.init();
   });
   jQuery(document).ready(function () {
       TableManaged.init();
   });
   $(document).ready(function () {
       var table = $('#sample_2').dataTable();       
       table.fnFilter('');      
   });
</script>
<!-- END JAVA SCRIPTS -->
<?php
   if ($_POST["actionType"] == "excel") {
       if ($row != 0) {
           header("Content-Type: application/vnd.ms-excel");
           header("Content-disposition: attachment; filename=SP_Summary_Report.xls");
       }
   }
   ?>