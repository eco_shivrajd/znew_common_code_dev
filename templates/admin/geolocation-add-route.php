<?php
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include "../includes/grid_header.php";
include "../includes/routeManage.php";
$routeObj = new routeManage($con, $conmain);
?>
<style>
    body {
        margin: 0;
    }
    #dvMap {
        height: 0;
        overflow: hidden;
        padding-bottom: 22.25%;
        padding-top: 30px;
        position: relative;
    }
    .dvMap-frame {
        left: 0;
        top: 0;
        height: 100%;
        width: 100%;
        position: absolute;
    }
    .kd-tabbed-vert.header-links .kd-tabbutton a {
        color: #757575;
        display: inline-block;
        height: 100%;
        padding: 0 24px;
        width: 100%;
    }
    .kd-tabbed-vert.header-links .kd-tabbutton {
        padding: 0;
    }
    .kd-tabbed-vert.header-links .kd-tabbutton.selected a {
        color: #03a9f4;
    }
    .kd-tabbed-vert.header-links .kd-tabbutton a:focus {
        text-decoration: none;
    }
    p.top-desc {
        padding: 1em 1em .1em 1em;
    }
    p.bottom-desc {
        padding: 0em 1em 1em 1em;
    }
</style>
<!--<link rel="stylesheet" href="https://developers.google.com/maps/documentation/javascript/demos/demos.css">-->

<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
<?php
$activeMainMenu = "ManageSupplyChain";
$activeMenu = "routes";
include "../includes/sidebar.php";
?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                <!-- /.modal -->
                <h3 class="page-title">
                    Geo Location
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="#">Geo Location</a>
                        </li>
                    </ul>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">
                                    Add Route Points
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
							<div id="googleMap" style="width:100%;height:400px;"></div>
                            </div>
                        </div>
						<div class="clearfix"></div>
						<div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">
                                    Route Listing
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">	
								<div id="route_list">
								<?php   
										$routecount = $routeObj->getRouteCount();
										$route_count = count($routecount);
								?>
									<table class="table table-striped table-brouteed table-hover" id="sample_2">
										<thead>
											<tr id="main_th">
												<th>Route Name </th>
												<th>Address </th>
												<th> Action</th>

											</tr>
										</thead>
										<tbody>
											<?php  
										if($routecount>0)
                                        {
										foreach($routecount as $key => $value) {
											$routedetails=array();

											$routedetails = $routeObj->getRouteCountDetails($value);
                                          $route_point_count = count($routedetails);
                                          // echo $route_point_count =  mysqli_num_rows($routedetails);
                                          //  echo "<pre>";
                                          //  print_r($routedetails);

											$route_name=$routedetails[0]['route_name'];
											$route_id=$value; 
											$route_address=$routedetails[0]['address']; 
											$Address='';
											$i = 1; 

                                            if ($route_point_count >= 2) {
                                                foreach ($routedetails as $key1 => $value1) 
                                                {                            
                                                   // echo "<pre>";print_r($routedetails);
                                                   // $state_name.=$value1['state_name']."<br>";
                                                    $Address.= $i.") ".$value1['address']."<br>";
                                                   // $route_no.="".$value1['route_no'];
                                                     $i++;
                                                }
                                            }
											

                                                ?>
                                                <?php 
                                                if ($route_point_count >= 2) { ?>
												<tr class="odd gradeX">
													<td>
															<?php echo $route_name;?>
													</td>
													<td align="left">
														<?php echo $Address; ?>
													</td>
													<td align="right">				

                                <a  title="View Route Details" style="margin-bottom: 5%;" onclick="javascript: showRouteMap(<?=$route_id;?>)" class="btn btn-xs btn-success "><span class="glyphicon glyphicon-eye-open"></span></a>

                                <?php if ($ischecked_delete==1) 
                                {
                                ?>
                              <!--   <a title="Delete" onclick="javascript: deleteRoute(<?=$route_id;?>)"  class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></a> -->
                                <?php
                                }
                                else
                                {
                                ?>
                                <?php echo '-';?>
                                <?php } ?>

													</td>
												</tr>
                                            <?php } ?>
										<?php }
										} ?>
										</tbody>
									</table>
									</div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
		<div class="modal fade" id="route_details" role="dialog">
        <div class="modal-dialog" style="width: 880px !important;">
            <!-- Modal content-->
            <div class="modal-content" id="route_map_content">
            </div>
        </div>
    </div>
        <!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php" ?>
<script>
function myMap() {
	var mapProp= {
		center:new google.maps.LatLng(18.493575284567175, 73.87346177526638),
		zoom:8,
	};
	
	var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);	
	var points=[];	
	 google.maps.event.addListener(map, 'click', function(event) 
     {
		var r = confirm("For adding this point Press ok button!");
		if (r == true) {		
		//code to check address available for this point
		var latt=event.latLng.lat();
		var longg=event.latLng.lng();
        // alert(latt);
        // alert(longg);    		
		var address = getAddress(latt, longg);	
		//debugger;
		//console.log(address);debugger;
        //alert(address);	
			if(address!==""){
				 points.push({
					latt1 : latt,
					longg1 :  longg,
					address1 : address
				});	
				//console.log(points);
			}else{
				alert("Not getting address on this point! Please Select another point!");
			}
			
		}else{	
			var url = 'ajaxSaveRoutePoints.php';               
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {data123 : points},
                    error: function (data) {
                        console.log("error:" + data)
                    },
                    success: function (data) {
						//debugger;
                       // console.log(data);
						//debugger;
                        if (data > 0) {
                            alert("Route Successfully");
                            location.reload();
                        } else {
                            alert("Not updated");
                        }
                    }
                });			
		}
	});
}
</script>
<script>
		
        function getAddress(latt, longitude) {			
           var response = ajax_call('ajax_getAddress.php?lattitude='+latt+'&longitude='+longitude+'');	
			console.log(response);
           return response;
        }				
        function ajax_call(url) {
            var response;
            $.ajax({
                type: "GET",
                url: url,
                async: false,
                success: function (data) {
                    response = data;
                },
                error: function (xhr, status, error) {
                    //alert(xhr.responseText);
                }
            });
            return response;
        }
		 function showRouteMap(route_id) {
            var url = "route_map_popup_by_map.php";
            jQuery.ajax({
                url: url,
                method: 'POST',
                data: 'route_id=' + route_id ,
                async: false
            }).done(function (response) {
                $('#route_map_content').html(response);
                $('#route_details').modal('show');
            }).fail(function () { });
            return false;
        }
    </script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDMpdO43txdg_zyovvZm9i1tMMFIQTKTU&callback=myMap"></script>
        <!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>