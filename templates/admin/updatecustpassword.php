<!-- BEGIN HEADER -->
<?php
 include "../includes/header.php"?>
<!-- END HEADER -->
<?php

$cust_id=$_GET['id'];
if(isset($_POST['submit']))
{
$cust_id=$_POST['cust_id'];	
$pass=$_POST['c_password'];
$password=md5($pass);

$update_sql=mysqli_query($con,"UPDATE tbl_customer SET pwd='$password' where id='$cust_id'");
?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script type="text/javascript"> 
       $(document).ready(function(){
           $('#thankyouModal').modal('show');
       });
      </script>
	  	  <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
              <center>  <h4 class="modal-title" id="myModalLabel">Change Password</h4></center>
            </div>
            <div class="modal-body">
                <p>
				<h5 style="color:green; text-align:center;">Password updated successfully and he should sign in with new password.</h5>
				</p>                  
			<center>
				<?php if($user_type=='Superstockist'){?>   
				  <a href="../admin/superstockist1.php?id=<?php echo base64_decode($_GET['id']);?>">
					<button type="button" class="btn btn-success">OK</button>
				</a><?php } else if($user_type=='Distributor'){?>
				<a href="../admin/distributor1.php?id=<?php echo base64_decode($_GET['id']);?>">
					<button type="button" class="btn btn-success">OK</button>
				</a><?php } else if($user_type=='SalesPerson'){?>
				<a href="../admin/sales1.php?id=<?php echo base64_decode($_GET['id']);?>">
					<button type="button" class="btn btn-success">OK</button>
			  </a><?php } ?>
			  </center>
            </div>    
        </div>
    </div>
</div>

<?php

}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<div class="page-container">
	<?php include "../includes/sidebar.php"; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<h3 class="page-title">
			Password
			</h3>
      
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Change Password
							</div>
						</div>
						<div class="portlet-body">
						
  <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
   <form class="form-horizontal" data-parsley-validate="" role="form" method="post" action="">
            
            <div class="form-group">
              <label class="col-md-3">New Password:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="password" id="password" 
					placeholder="Enter Password" 
					data-parsley-minlength="6" 
					data-parsley-minlength-message="Password should be minimum 6 characters without blank spaces" 
					data-parsley-maxlength="50" 
					data-parsley-maxlength-message="Only 50 characters are allowed" 
					data-parsley-type-message="Password should be minimum 6 characters without blank spaces" 
					data-parsley-required-message="Please enter Password" data-parsley-trigger="change" 
					data-parsley-required="true" 
					data-parsley-pattern="/^\S*$/" 
					data-parsley-error-message="Password should be 6-50 characters without blank spaces" 
				name="password" class="form-control placeholder-no-fix">
              </div>
            </div><!-- /.form-group -->
  
            <div class="form-group">
              <label class="col-md-3">Confirm password:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="password" 
				id="c_password" 
				placeholder="Enter Confirm Password" 
				data-parsley-trigger="change" 
				data-parsley-equalto="#password" 
				data-parsley-equalto-message="Password does not match with confirm password" 
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter confirm password" 
				name="c_password" class="form-control">
              </div>
              <input type="hidden" name="cust_id" value="<?=$cust_id;?>">
            </div>
                       
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
                <button name="submit" id="submit" class="btn btn-primary">Submit</button>
							
              </div>
            </div><!-- /.form-group -->
          </form>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
<style>
.form-horizontal{
font-weight:normal
}
</style>
</body>
<!-- END BODY -->
</html>
