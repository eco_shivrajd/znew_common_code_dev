<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/commonManage.php";	
include "../includes/sectionManage.php";
$sectionObj = new sectionManager($con, $conmain);

$user_type = $_SESSION[SESSION_PREFIX.'user_type'];
$user_id = $_SESSION[SESSION_PREFIX.'user_id'];
?>
<!-- END HEADER -->
<?php
$section_id = $_GET['section_id'];
$result = $sectionObj->getSectionDetailsByID($section_id);
$row_details = mysqli_fetch_assoc($result);
//var_dump($row);
//exit();
if(isset($_POST['submit']))
{
	//print_r($_POST);
	//exit();
	$sectionObj->updateSection();
	echo '<script>alert("Section Updated successfully.");location.href="section-list.php";</script>';
}
?>

<body class="page-header-fixed page-quick-sidebar-over-content ">

<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageProducts"; $activeMenu = "Section";
	include "../includes/sidebar.php";
	$commonObj 	= 	new commonManage($con,$conmain);
	$row_url=$commonObj->getPageIDforUrlEdit($php_page_name);
	$page_id_url = $row_url['page_id'];
	$row_url_edit=$commonObj->getURLforEdit($profile_id,$page_id_url);
	$ischecked_edit_url = $row_url_edit['ischecked_edit'];
    if ($ischecked_edit_url == 0 && $ischecked_edit_url!='') 
	{
		session_set_cookie_params(0);
		session_start();
		session_destroy();
		echo '<script>location.href="../login.php";</script>';
	    exit;
	}
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Section
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="section-list.php">Section</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Update Section</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Update Section
							</div>
							
						</div>
						<div class="portlet-body">
                       <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
                          
  <form class="form-horizontal" data-parsley-validate="" id="form" enctype="multipart/form-data" role="form" method="post" action="section-update.php?section_id=<?php echo $section_id;?>">   
            
            <div class="form-group">
              <label class="col-md-3">Section Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text" name="section_name" value="<?php echo $row_details['section_name'];?>" 
				placeholder="Enter section Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter section name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				class="form-control">
				 <input type="hidden" name="section_id" value="<?php echo $section_id;?>">
              </div>
            </div><!-- /.form-group -->            
            

             <div class="form-group">
              <label class="col-md-3">section Active Status:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text" name="section_active_status" value="<?php echo $row_details['section_active_status'];?>" 
				placeholder="Enter section Active Status"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please section Active Status"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				class="form-control">
              </div>
            </div><!-- /.form-group --> 

             <div class="form-group">
              <label class="col-md-3">PHP section Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text" name="php_section_name" value="<?php echo $row_details['php_section_name'];?>" 
				placeholder="E.g. index.php"
				class="form-control">
              </div>
            </div><!-- /.form-group -->   
			
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                <a href="section-list.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group -->
          </form>        
                            
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->

</body>
<!-- END BODY -->
</html>