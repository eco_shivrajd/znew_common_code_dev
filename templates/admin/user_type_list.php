<!-- BEGIN HEADER -->
<?php 
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);

include "../includes/grid_header.php"; 
include "../includes/userConfigManage.php";
$userConfObj    =   new userConfigManager($con,$conmain);
//get all user role

$useroles=$userConfObj->get_alluserole();
$parentchield=$userConfObj->get_allparentchield();
//echo "<pre>";print_r($useroles);
?>
<?php
if(isset($_POST['submit'])){
	$parent_id = fnEncodeString($_POST['parent_id']);
	$user_role=fnEncodeString($_POST['user_role']);
	$user_type = fnEncodeString($_POST['user_type']);
	$usertype_margin = $_POST['usertype_margin'];
	$sql_user_check = mysqli_query($con, "select id from `tbl_user_tree` where user_type='$user_type' ");//and parent_id='$parent_id'
	if ($rowcount = mysqli_num_rows($sql_user_check) > 0){
		echo '<script>alert("User Type already exist.");location.href="user-type-add.php";</script>';
	}else{
		//$userObj->addUserType();
		$userConfObj->addUserType();
		//die();
		echo '<script>alert("User Type added successfully.");location.href="user-type-add.php";</script>';
	}
}
?> 
<!-- END HEADER -->
<style>
.tree {
    min-height:20px;
    padding:19px;
    margin-bottom:20px;
    background-color:#fbfbfb;
    border:1px solid #999;
    -webkit-border-radius:4px;
    -moz-border-radius:4px;
    border-radius:4px;
    -webkit-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    -moz-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05)
}
.tree li {
    list-style-type:none;
    margin:0;
    padding:10px 5px 0 5px;
    position:relative
}
.tree li::before, .tree li::after {
    content:'';
    left:-20px;
    position:absolute;
    right:auto
}
.tree li::before {
    border-left:1px solid #999;
    bottom:50px;
    height:100%;
    top:0;
    width:1px
}
.tree li::after {
    border-top:1px solid #999;
    height:20px;
    top:25px;
    width:25px
}
.tree li span {
    -moz-border-radius:5px;
    -webkit-border-radius:5px;
    border:1px solid #999;
    border-radius:5px;
    display:inline-block;
    padding:3px 8px;
    text-decoration:none
}
.tree li.parent_li>span {
    cursor:pointer
}
.tree>ul>li::before, .tree>ul>li::after {
    border:0
}
.tree li:last-child::before {
    height:30px
}
.tree li.parent_li>span:hover, .tree li.parent_li>span:hover+ul li span {
    background:#eee;
    border:1px solid #94a0b4;
    color:#000
}
</style>
  
  <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" />-->
  
   <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />
   
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "UserType";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			User Type List
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					 
					<li>
						<i class="fa fa-home"></i>
						<a href="#">User Type List</a>
					</li>                   
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->							
					<div class="clearfix"></div>
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								User Type Listing Tree
							</div>
							<? if($_SESSION[SESSION_PREFIX."user_type"]=="Admin")  { ?>
								<a href="user-type-add.php" class="btn btn-sm btn-default pull-right mt5">Add User Type</a>
							<? } ?>
							
                            <div class="clearfix"></div>							
						</div>
						<div class="portlet-body">
							<div class="tree well">
								<?php 
				function createTreeView($array, $currentParent, $currLevel = 0, $prevLevel = -1) {

										foreach ($array as $categoryId => $category) {

										if ($currentParent == $category['parent']) {
											if ($currLevel > $prevLevel) echo " <ul> "; 

											if ($currLevel == $prevLevel) echo " </li> ";

											echo '<li><span>'.$category['label'].'</span>';

											if ($currLevel > $prevLevel) { $prevLevel = $currLevel; }

											$currLevel++; 

											createTreeView ($array, $categoryId, $currLevel, $prevLevel);

											$currLevel--;
											}   

										}

										if ($currLevel == $prevLevel) echo " </li>  </ul> ";

										}
										$result = mysqli_query($con,"SELECT id, user_type as name, parent_id as parent  FROM tbl_user_tree ORDER BY name");
											$items = array();
											 while($row = mysqli_fetch_array($result))
												 { $items[$row['id']] = array('id' => $row['id'], 'label' => $row['name'], 'parent' => $row['parent']);
											 }

										echo  createTreeView($items, 0);
										?>
										
									</div>
							 <!--<div id="treeview"></div>-->
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								User Type Listing Table
							</div>							
						</div>
						<div class="portlet-body">
							 <table class="table table-striped table-bordered table-hover" id="sample_2">
								<thead>
									<tr>
										<th>Parent User Type </th>
										<th> User Role </th>
										<th> User Type </th>
										<th> Margin </th>
										<th> Action </th>			
									</tr>
								</thead>
								<tbody>
								<?php 
							if(count($parentchield)>0){
								foreach($parentchield as $key=>$value){ 
                                         $user_role = $value['user_role'];
									?>
									<tr class="odd gradeX">
										<td><?php echo  $value['parent_user_type'];	?>	</td>
										<td><?php echo  $value['user_role'];	?>	</td>
										<td><b><?php echo  $value['user_type'];	?></b></td>
										<td align="right"><?php echo  $value['usertype_margin'];	?>	</td>
										<td> 

											
					    <?php if ($user_role=='Admin' || $user_role=='Superstockist' || $user_role=='Distributor') 
                         {
                        ?> 
							<a title="View Web Permissions Of <?php echo  $value['user_type'];	?>" href="user_profile_config.php?profile_id1=<?php echo $value['id'];?>&user_type=<?php echo $value['user_type'];?>" class="btn btn-xs btn-primary">
							<span class="glyphicon glyphicon-edit"></span> Permissions 
							</a>
                        <?php
                        }
                        else
                        {
                        ?>
                            <a title="No Permissions Available For This User Role" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-ban-circle"></span></a>
                        <?php } ?> 

										</td>
									</tr>
								<?php } }?>
								</tbody>
							</table>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<script type="text/javascript">

 function myFunction(varl) {
	if (varl.value != "") {
        var arrtmp = varl.value.split(".");
        if (arrtmp.length > 1) {
			var strTmp = arrtmp[1];
			if (strTmp.length > 2) {
				varl.value = varl.value.substring(0, varl.value.length - 1);
			}
        }
	}
}
</script>
<!--<script type="text/javascript" charset="utf8" 
src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-treeview/1.2.0/bootstrap-treeview.min.js"></script>-->

  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.full.min.js"></script>
<!-- END FOOTER -->
<script>


/*
$(document).ready(function(){		
 	var url = 'response_usertree.php';
			 $.ajax({
			 url: url,
			 datatype:"JSON",
			contentType: "application/json",
			error : function(data){console.log("error:" + data)
			},
			
			success: function(data){				
				 $('#treeview').treeview({data: data});				
			}
		 });
		 
		 
}); 
*/
 function formatResult(node) {
    var $result = $('<span style="padding-left:' + (20 * node.level) + 'px;">' + node.text + '</span>');
    return $result;
  };

  
</script>
<script>
$(function () {
    $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
    $('.tree li.parent_li > span').on('click', function (e) {
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
					children.hide('fast');
            $(this).attr('title', 'Expand this branch').find(' > ul > li').addClass('icon-plus-sign').removeClass('icon-minus-sign');
        } else {
				children.show('fast');
            $(this).attr('title', 'Collapse this branch').find(' > ul > li').addClass('icon-minus-sign').removeClass('icon-plus-sign');	

        }
        e.stopPropagation();
    });
});
//$('#dropdown').val($('.tree well').html());
</script>

<style>
.form-horizontal{
font-weight:normal;
}
</style>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
         
