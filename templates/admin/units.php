<!-- BEGIN HEADER -->
<?php 
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include "../includes/grid_header.php";
include "../includes/commonManage.php";
include "../includes/targetManage.php";
$targetObj 	= 	new targetManager($con,$conmain);
$result_units = $targetObj->getAllUnits($tuid=0);
$result_variant = $targetObj->getAllVariant($tvid=0);
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageProducts"; $activeMenu = "Unit";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->			
			<h3 class="page-title">
			Units
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Units</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
            <div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Units Listing
							</div>
							   <?php
                                if ($ischecked_add==1) 
                                {
                                ?>
                             <a class="btn btn-sm btn-default pull-right mt5" href="unit-add.php">
                                Add Unit
                              </a> 
                               <?php } ?>
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">							
							<table class="table table-striped table-bordered table-hover" id="sample_1sdfd">
							<thead>
							<tr>
								<th>Unit Name</th>
								<th> Variant Name</th>
								<th> Action</th>
							</tr>
							</thead>
							<tbody>
							<?php
							$default = array(1, 2, 3, 4, 5); 
							foreach($result_units as $key=>$units)
							{
								$isfound="";
								$tuid = $units['tuid'];
									if (in_array($tuid, $default)) { 
										$isfound = 1; 
									}
									else { 
										$isfound = 0;
									} 
							?>
								<tr class="odd gradeX"> 
									<td>
										<?php if ($ischecked_edit==1 && $isfound == 0) { ?>				
										<a href="unit1.php?id=<?php echo base64_encode($units['tuid']);?>">
										<?php echo  fnStringToHTML($units['unitname']);?></a>
										<?php } else {  echo  fnStringToHTML($units['unitname']); } ?>
									</td> 
									<td><?php echo  fnStringToHTML($units['vname']);?></td>
									<td>
										<?php if ($ischecked_edit==1 && $isfound == 0){ ?>
										<a title="Edit" href="unit1.php?id=<?php echo base64_encode($units['tuid']);?>" 
											class="btn btn-xs btn-primary " ><span class="glyphicon glyphicon-edit"></span>
										</a>
                                         <?php } else { ?> 
                                           <a title="Can Not Edit This Record" href="#"  class="btn btn-xs btn-primary" disabled><span class="glyphicon glyphicon-edit"></span></a>
                                     <?php } ?>

									     <?php if ($ischecked_delete==1 && $isfound == 0){ ?>
										<a title="Delete"  onclick="javascript: deleteUnit(<?php echo $units['tuid'];?>)" class="btn btn-xs btn-danger "><span class="glyphicon glyphicon-trash"></span>
										</a>				        
										<?php } else { ?> 
                                           <a title="Can Not Delete This Record" href="#"  class="btn btn-xs btn-danger" disabled><span class="glyphicon glyphicon-trash"></span></a>
                                     <?php } ?>
									</td>									
								</tr>
							<?php } ?>						
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
 <script>
        function deleteUnit(unit_id) {
			if (confirm('Are you sure that you want to delete this Unit?')) {
				var url='ajax_product_manage.php?action=delete_unit&unit_id=' + unit_id;
                $.ajax({
                    url: url,
                    datatype: "JSON",
                    contentType: "application/json",

                    error: function (data) {
                        console.log("error:" + data)
                    },
                    success: function (response) {
						console.log(response);
						
						if (response === 'unit_deleted') {
							alert('Unit deleted successfully.');
							location.reload();
						}else{
							alert("Not updated.");
						}
                    }
                });
			}
        }
       
    </script>
</body>
<!-- END BODY -->
</html>