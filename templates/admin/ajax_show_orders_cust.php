<?php 
include ("../../includes/config.php");
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
//echo "<pre>";print_r($orderwise);
//$orderwise='true';
$order_status = $_POST['order_status'];
$order_id = $_POST['order_id'];
$orders = $orderObj->getOrderschnage1_cust($order_status,$order_id);
$order_count = count($orders);
$user_type=$_SESSION[SESSION_PREFIX . "user_type"];
$session_user_id=$_SESSION[SESSION_PREFIX . "user_id"];
?>
<div class="clearfix"></div>
<table class="table table-striped table-bordered table-hover" id="sample_2">
	<thead>
		<tr id="main_th">
			<?php if(($order_status == '1' ||$order_status == '11')&& $order_count != 0 ){ ?>
			<th id="select_th">
				<input type="checkbox" name="select_all[]" id="select_all"  onchange="javascript:checktotalchecked(this)">
			</th>
			<?php  } ?>
			<th>Order From</th>
			<th>Order To</th>
			<th>Order Id<br>Order Date </th>
			<th>Quantity</th>
			<th>Total Price (₹)</th>
			<th>Price(₹)<br>(GST+Discount)</th>
			<?php if($order_status == '0'||$order_status == '10'){  ?>
				<th>Order Status</th>
			<?php } ?>
			<th>Action  </th>
		</tr>
		</thead>
		<tbody>
		<?php				
		foreach($orders as $key=>$value)
		{
			$order_totalcost = 0;
			$order_totalgstcost = 0;
			$prod_qnty_total=0;
			//$orderdetails = $orderObj->getOrdersDetailschnage_cust($value['oid']);	
			$oid = $value['oid'];
			$prod_qnty_total = $value['sum_product_quantity'];
			$order_totalcost = $value['sum_product_total_cost'];
			$order_totalgstcost = $value['sum_p_cost_cgst_sgst'];
			$order_status_myorders=$value['order_status'];
			$totalcost='';$gstcost='';
			
		?>
		<tr class="odd gradeX">
		<?php if(($order_status == '1' ||$order_status == '11')&&  $order_count != 0 ){  ?>
			<td>
				<input type="checkbox" class="mycheckbox" name="select_all[]" id="select_all" onchange="javascript:checktotalchecked(this)" value="<?php echo $value["oid"];?>">
			</td>
		<?php } ?>
			<td ><?php echo $value["order_by_name"];if(!empty($value["shop_name"])){echo "-".$value["shop_name"];}?></td>					
			<td ><?php echo $value["order_to_name"];?></td>
			<td ><font size="1.4"> <?php echo $value["oid"]?></font><br><?php echo date('d-m-Y h:i:s A',strtotime($value["order_date"]));?></td>										
			<td  align="right" >
			<?php echo '<a onclick="showOrderDetails_cust(\''.$order_status.'\',\''.$value['oid'].'\')" title="Order Details">'.$prod_qnty_total.'<a>';?>
			</td>
			<td align="right"> <b><?php echo number_format((float)$order_totalcost, 2, '.', '');?></b></td>
			<td align="right"><b><?php echo number_format((float)$order_totalgstcost, 2, '.', '');?></b> </td>
		<?php if($order_status == '0'||$order_status == '10'){  ?>
			<td><b><?php $order_status_switch_array=array('1'=>'Pending','2'=>'Delivery Assigned','4'=>'Delivered');
			echo $order_status_switch_array[$order_status_myorders];?></b></td>
		<?php } ?>
			<td><!--<a onclick='showInvoice_cust(1,<?=json_encode($oid);?>)'  title="View Invoice">View Invoice</a>-->
				<a title="View Invoice" onclick='showInvoice_cust(1,<?=json_encode($oid);?>)' class="btn btn-xs btn-success ">
					<span class="glyphicon glyphicon-eye-open"></span>
				</a>
				<!--
				<?php if($order_status==1 || $order_status==11){ ?>
				<a 	title="Delete" onclick='javascript: deleteOrderCust(<?=json_encode($oid);?>)' 
					class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span>
				</a><?php } ?>-->
			
			</td>
		</tr>
		<?php } ?>
		
		 </tbody>
	</table>
<script>
$(document).ready(function() {
	 $("#sample_2").dataTable().fnDestroy();			
    $('#sample_2').dataTable( {
	order: [],
	columnDefs: [ { orderable: false, targets: [0] } ]
	});
});
</script>
	