<?php
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);

include ("../../includes/config.php");
include "../includes/testManage.php";
$testObj 	= 	new testManager($con,$conmain);
$getflag	= $_POST["getflag"];
$result_session_user_details = $testObj->getSessionUser();
if($getflag=='getoption'){
	//$depth	= $_POST["depth"];
	$user_typej	= $_POST["user_typej"];
	$result_childs = $testObj->getChields($user_typej);	
	
	//echo "<pre>";print_r($result_session_user_details);
	if($_SESSION[SESSION_PREFIX.'user_role']=='Admin'){
		if($result_childs!=0){
			echo "<option value='' selected>-- Select --</option>";
			while ($row_childs = mysqli_fetch_assoc($result_childs)){ 			
			  echo "<option value=".$row_childs['id']."_".$row_childs['depth']." >".$row_childs['firstname']."</option>";
			} 
		}else{
			echo "no_parent";
		}
	}else if($_SESSION[SESSION_PREFIX.'user_role']=='Superstockist'||$_SESSION[SESSION_PREFIX.'user_role']=='Distributor'){
		if($result_childs!=0){
			echo "<option value='' >-- Select --</option>";
			while ($row_childs = mysqli_fetch_assoc($result_childs)){ 
				//echo "<pre>";print_r($row_childs);
				$parent_usertypes = explode(',',$result_session_user_details[0]['parent_usertypes']);
				$parent_userids = explode(',',$result_session_user_details[0]['parent_ids']);
				$parent_names = explode(',',$result_session_user_details[0]['parent_names']);
				if (false !== $key = array_search($row_childs['user_type'], $parent_usertypes)) {
					if($row_childs['id']==$parent_userids[$key]){
						echo "<option value='".$row_childs['id']."_".$row_childs['depth']."' selected>".$row_childs['firstname']."</option>";
					}		
					//echo "iffifi";
				}else if($row_childs['user_type']==$result_session_user_details[0]['user_type']){
					if($row_childs['id']==$result_session_user_details[0]['id']){
					echo "<option value='".$row_childs['id']."_".$row_childs['depth']."' selected>".$row_childs['firstname']."</option>";
					//echo "elseifif";
					}
				}else{					
					if(in_array($_SESSION[SESSION_PREFIX.'user_id'],  explode(',',$row_childs['parent_ids']))){
						echo "<option value=".$row_childs['id']."_".$row_childs['depth']." >".$row_childs['firstname']."</option>";
					}					
				}	
			} 
		}else{
			echo "no_parent";
		}
	}else{
		echo "no_parent";
	}
	
	
}if($getflag=='getparent'){	
	$id = $_POST["id"];//die();
	$result2['parent_info']= $testObj->checkParentIds($id,$data=array());
	echo json_encode($result2);
	//echo json_encode($result2[$user_typej]['parent_info']);
}
if($getflag=='onchange_getchild'){
	//$depth	= $_POST["depth"];
	$user_id	= $_POST["user_id"];
	$puser_type	= $_POST["puser_type"];
	$result_childs = $testObj->getChields_byuserid($user_id,$puser_type);
	if($result_childs!=0){
		while ($row_childs = mysqli_fetch_array($result_childs)){ 
		  echo "<option value=".$row_childs['id']."_".$row_childs['depth']." >".$row_childs['firstname']."</option>";
		} 
	}else{
		echo "no_parent";
	}
}
if($getflag=='onchange_getchild_edit'){	
	$user_id	= $_POST["user_id"];
	$puser_type	= $_POST["puser_type"];
	$result_childs = $testObj->getChields_byuserid($user_id,$puser_type);	
	if($result_childs!=0){
		echo "<option value='' >-- Select --</option>";
		while ($row_childs = mysqli_fetch_array($result_childs)){ 
		  echo "<option value=".$row_childs['id']." >".$row_childs['firstname']."</option>";
		} 
	}else{
		echo "no_parent";
	}
}
if($getflag=='onchange_getrole'){	
	$usertype_id	= $_POST["usertype_id"];
	$result_role = $testObj->onchange_getrole($usertype_id);
	if($result_role!=0){
		while ($row_role = mysqli_fetch_array($result_role)){ 
		  $user_role = $row_role['user_role'];
		} 
		echo $user_role;
	}else{
		echo "no_userrole";
	}
}
if($getflag=='onchange_get_serviceby_userid'){	
	$usertype	= $_POST["usertype"];
	$userole	= $_POST["userole"];
	$result_users = $testObj->onchange_get_serviceby_userid($usertype,$userole);
	if($result_users!=0){
		echo "<option value='' >-- Select --</option>";
		while ($row_user = mysqli_fetch_array($result_users)){ 		
		  echo "<option value=".$row_user['id']." >".$row_user['firstname']."</option>";
		  } 		
	}else{
		echo "no_users";
	}
}
mysqli_close($con);
?>