<?php
ini_set('display_errors', 1);
include "../includes/grid_header.php";
include "../includes/commonManage.php";
$commonObj = new commonManage($con, $conmain);
$commonObjctype = $commonObj->log_get_commonclienttype($con, $conmain);
$user_id = $_SESSION[SESSION_PREFIX . 'user_id'];
?>
    <!-- END HEADER -->

    <body class="page-header-fixed page-quick-sidebar-over-content ">
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "ManageProducts";
        $activeMenu = "Product";
        include "../includes/sidebar.php";
        ?>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <div class="page-content">
                        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                        <!-- /.modal -->
                        <h3 class="page-title">
                    Products
                </h3>
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="#">Products</a>
                                </li>
                            </ul>
                        </div>
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN PAGE CONTENT-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet box blue-steel">
                                    <div class="portlet-title">
                                        <div class="caption">Products Listing</div>
                                <?php  if ($ischecked_add==1)   {   ?>
                                        <a href="product-add.php" class="btn btn-sm btn-default pull-right mt5 ">Add Product</a>
                                        <?php } ?>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Category
                                                    </th>
                                                    <th>
                                                        Product Name
                                                    </th>
                                                    <th>
                                                        Product HSN
                                                    </th>
                                                    <th>
                                                        Product Price
                                                    </th>
                                                    <th>
                                                        Product Variant
                                                    </th>
                                                    <th>
                                                        Product Image
                                                    </th>
                                                    <th>
                                                        Action
                                                    </th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                                <?php
												//added_by_userid
									    $get_user_sql = "SELECT parent_ids FROM `tbl_user` WHERE id = '".$user_id."' limit 1";
                                        $result_user = mysqli_query($con, $get_user_sql);
										$row_user = mysqli_fetch_assoc($result_user);
										
                                        $get_product_sql = "SELECT p.id ,p.added_by_userid
										FROM `tbl_product` p
										WHERE p.isdeleted !='1' AND
										((find_in_set(p.added_by_userid,'".$row_user['parent_ids']."') <> 0) 
											or p.added_by_userid =  '".$user_id."')
										 order by id ASC";
                                        $result_product = mysqli_query($con, $get_product_sql);
                                        $row__prod_count = mysqli_num_rows($result_product);
                                        $all_product_ids=array();
                                        while($row_product = mysqli_fetch_array($result_product)){
                                            $all_product_ids[]=$row_product['id'];
                                        }
                                        
                                       // echo "<pre>";print_r($all_product_ids );//die();
                                        for($i=0;$i<$row__prod_count;$i++){
                                             $sql_get_varient = "SELECT `brand_id`, `category_id`, `product_id`, `variant1_unit_name`, 
											`variant2_unit_name`, `product_variant_id`, `brand_name`, `category_name`, 
											`category_image`, `product_name`, `added_by_userid`, `added_by_usertype`, `product_image`, `product_price`, `product_variant1`, `product_variant2`,
											`variant_cnt`, `product_hsn`, `product_cgst`, `product_sgst`, 
											`product_barcode_img`, `carton_quantities`  
											FROM `pcatbrandvariant` 											
											where product_id='".$all_product_ids[$i]."'";
                                            $result_varient = mysqli_query($con, $sql_get_varient);
                                            $row_var_count2 = mysqli_num_rows($result_varient);
                                            
                                            
                                            $product_hsn = '';                                          
                                            $product_price = '';
                                            $product_variant1 = '';$variant1_unit_name = '';
                                            $product_variant2 = ''; $variant2_unit_name = '';
                                            $product_image = '';
                                            
                                             while ($row_prod_var = mysqli_fetch_array($result_varient)) {
                                                //echo "<pre>";print_r($row2);
                                                $product_single=array();
                                                $added_by_userid=$row_prod_var['added_by_userid'];  
                                                $category_id=$row_prod_var['category_id'];                                              
                                                $product_id=$row_prod_var['product_id'];
                                                $category_name=$row_prod_var['category_name'];                                              
                                                $product_name=$row_prod_var['product_name'];                                                
                                                
                                                $product_hsn.=$row_prod_var['product_hsn']."<br>";
                                                $product_price.=$row_prod_var['product_price']."<br>";
												if(!empty($row_prod_var['product_variant1'])){
													  $product_variant1.=$row_prod_var['product_variant1']."-".
													$row_prod_var['variant1_unit_name']." ";
												}
												if(!empty($row_prod_var['product_variant2'])){
													  $product_variant1.=$row_prod_var['product_variant2']."-".
													$row_prod_var['variant2_unit_name']."<br> "; 
												}
                                                                                              
                                                
                                                
                                                //$product_image.=$row_prod_var['product_image'];   
												$product_image .="<img src='upload/".COMPANYNM."_upload/".$row_prod_var['product_image']."' 
																		alt='".$row_prod_var[ 'product_image']."' width='100px' /></br>";
                                                $product_single1[]=$product_single;
                                            } ?>
                                           <tr class="odd gradeX">
												<td><?php echo $category_name; ?></td>
												<td>
													<?php if ($ischecked_edit==1 && $added_by_userid==$user_id) { ?>
														<a href="product1.php?id=<?php echo base64_encode($product_id);?>">
															<?php echo $product_name; ?>
														</a>
														<?php } else {  echo $product_name; } ?>
												</td>
												<td><?php echo $product_hsn; ?></td>
												<td align="right" ><?php echo $product_price; ?></td>
												<td><?php echo $product_variant1; ?></td>
												<td><?php echo $product_image; ?></td>
												<td>
													<?php if ($ischecked_edit==1 && $added_by_userid==$user_id) { ?>
														<a href="product1.php?id=<?php echo base64_encode($product_id);?>" class="btn btn-xs btn-primary ">
															<span class="glyphicon glyphicon-edit"></span>
														</a>
														<?php 
                                                      } 
                                                      else {   echo '<a title="Can Not Edit This Record" href="#" class="btn btn-xs btn-primary" disabled><span class="glyphicon glyphicon-edit"></span></a>'; } ?>
															<?php if ($row__prod_count > 1 && $ischecked_delete==1 && $added_by_userid==$user_id) { ?>
																<a class="btn btn-xs btn-danger" onclick="javascript: deleteProduct(<?= $category_id; ?>,<?= $product_id; ?>)">
																	<span class="glyphicon glyphicon-trash"></span>
																</a>
																<?php } else {  echo '<a title="Can Not Delete This Record" onclick="#" class="btn btn-xs btn-danger" disabled><span class="glyphicon glyphicon-trash"></span></a>';  } ?>
												</td>
											</tr>
											<?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- END PAGE CONTENT-->
                    </div>
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->

                <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php include "../includes/grid_footer.php" ?>
            <!-- END FOOTER -->
            <script>
                function deleteProduct(cat_id, product_id) {
                    if (confirm('Are you sure that you want to delete this Product?')) {
                        CallAJAX('ajax_product_manage.php?action=delete_product&cat_id=' + cat_id + '&product_id=' + product_id);
                    }
                }

                function CallAJAX(url) {
                    if (window.XMLHttpRequest) {
                        xmlhttp = new XMLHttpRequest();
                    } else {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            alert('Product deleted successfully.');
                            location.reload();
                        }
                    }
                    xmlhttp.open("GET", url, true);
                    xmlhttp.send();
                }
            </script>
    </body>
    <!-- END BODY -->

    </html>