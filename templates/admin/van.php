<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);
$id=$_GET['id']; 		  
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageTransport"; $activeMenu = "Vehicles";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->			
			<h3 class="page-title">Vehicle</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Vehicle</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Vehicle Listing
							</div>
                            <a href="van-add.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Vehicle
                              </a>
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover dataTable no-footer" id="sample_2" role="grid" aria-describedby="sample_2_info">
							<thead>
							<tr role="row"><th class="sorting" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
									 Vehicle No.
								: activate to sort column ascending" style="width: 106px;">
									 Vehicle No.
								</th><th class="sorting_asc" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
									Vehicle RTO No.
								: activate to sort column ascending" aria-sort="ascending" style="width: 150px;">
									Vehicle RTO No.
								</th><th class="sorting" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
									Vehicle Type
								: activate to sort column ascending" style="width: 118px;">
									Vehicle Type
								</th><th class="sorting" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
									Transporter/Owner Name
								: activate to sort column ascending" style="width: 229px;">
									Transporter/Owner Name
								</th><th class="sorting" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
									Contact Number
								: activate to sort column ascending" style="width: 152px;">
									Contact Number
								</th><th class="sorting" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
									Vehicle Status
								: activate to sort column ascending" style="width: 134px;">
									Vehicle Status
								</th></tr>
							</thead>
							<tbody>

						<?php
							$uid=$_SESSION[SESSION_PREFIX.'user_id'];							
							$sql="SELECT 
								id,
								van_no, 
								van_rto,
								van_type,
								van_owner,
								mobile,
								del_status
							FROM 
							tbl_van 
							where added_by='$uid'";
							$result1 = mysqli_query($con,$sql);
							
							while($row = mysqli_fetch_array($result1))
							{		?>
								 <tr class="odd gradeX">								 
								<td>
									<a href="van1.php?id=<?php echo $row['id'];?>"><?php echo $row['van_no'];?></a>
								</td>
								<td><?php echo $row['van_rto'];?></td>
								<td><?php echo $row['van_type'];?></td>
								<td><?php echo $row['van_owner'];?></td>
								<td align="right"><?php echo $row['mobile'];?></td>
								<?php if($row['del_status']=='Loaded'){?>
								<td><a type="button" data-toggle="modal" href="#view_product_details" 
										onClick = "getLoadedProducts('<?=$row['id']?>');"><?php echo $row['del_status'];?></a></td>
								
								<?php }else{ ?>
								<td>Not Loaded</td>
								<?php }?>
								</tr>
								<?php
								} ?>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
	
</div>
<!-- END CONTAINER -->
<!-- Modal -->
<div id="loader_div"></div>
<!-- START MODAL -->
<div class="modal fade bs-modal-lg" id="view_product_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div id="model_content" class="modal-content">	  
			<div class="modal-header">
				<button style="margin-top: 3px; margin-right: 5px;" class="btn btn-primary" onclick="takeprint()" id="btnPrint" name="btnPrint" type="button">Take a Print</button>
				<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
				<h4 id="myModalLabel" class="modal-title"></h4>	   
			</div>
			<div id="divPrintARea" style="padding-bottom: 5px !important;" class="modal-body">
			<div class="row">
				<div class="col-md-12">   
						<div id="lodedproducts" name="lodedproducts">
						
						</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>

<!-- END MODAL -->



<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->

<!-- END JAVASCRIPTS -->
<!-- END FOOTER -->
<!-- END PAGE LEVEL SCRIPTS -->

		
<script type="text/javascript" src="../../assets/global/scripts/jquery.loader.js"></script>
<script>
function getLoadedProducts(van_id,str){
	var url = 'getLoadedProducts.php?van_id='+van_id+'&str=vanid';
			 $.ajax({
			 url: url,
			 datatype:"JSON",
			contentType: "application/json",
			error : function(data){console.log("error:" + data)
			},
			
			success: function(data){				
				$("#lodedproducts").html(data);
			}
		 });
}
</script>
<script>
function takeprint() {
	var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
	var divContents = '<style>\body {\
		font-size: 12px;}\
	th {text-align: left;}\
	.printHeading { line-height: 18px;  padding: 10px 0;  font-size: 18px; }\
	table { border-collapse: collapse;  \
		font-size: 12px; }\
	table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
	body { font-family: "Open Sans", sans-serif;\
	background-color:#fff;\
	font-size: 15px;\
	direction: ltr;}</style>' + $("#divPrintARea").html();
	if(isIE == true){
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write(divContents);
		printWindow.focus();
		printWindow.document.execCommand("print", false, null);
	}else{
		$('<iframe>', {
			name: 'myiframe',
			class: 'printFrame'
		}).appendTo('body').contents().find('body').html(divContents);
		window.frames['myiframe'].focus();
		window.frames['myiframe'].print();
		setTimeout(
		function() 
		{
			$(".printFrame").remove();
		}, 1000);
	}
};
</script>
</body>
<!-- END BODY -->


</html>