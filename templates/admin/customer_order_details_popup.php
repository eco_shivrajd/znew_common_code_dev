<?php
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/reportManage.php";
$reportObj = new reportManage($con, $conmain);
$order_details_id = $_POST['order_details_id'];
$prod_id = $_POST['prod_id'];
$prod_var_id = $_POST['prod_var_id'];
//echo "<pre>";
//var_dump($order_details_id);die();
$order_type = 'Order Details'; //$_POST['order_type'];
$order_details = $reportObj->get_customer_order_varient_details($order_details_id, $prod_id, $prod_var_id); //
//echo "<pre>";print_R($order_details);die();
//$product_variant = $reportObj->getSProductVariant($order_details['product_variant_id']);
?>
<div class="modal-header">
    <button type="button" name="btnPrint" id="btnPrint" onclick="OrderDetailsPrint()" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
    <?php if ($order_details[0]['lat'] != "" && $order_details[0]['long'] != "" && $order_details[0]['lat'] != "0.0" && $order_details[0]['long'] != "0.0") { ?>
        &nbsp;&nbsp;
        <button type="button" name="btnPrint" id="btnPrint" onclick="showGoogleMap('<?= $order_details[0]['lat']; ?>', '<?= $order_details[0]['long']; ?>')" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Location</button>
    <?php } ?>

    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel"></h4>	   
</div>
<div class="modal-body" style="padding-bottom: 5px !important;" id="divOrderPrintArea">
    <div class="row">
        <div class="col-md-12">   
            <div class="portlet box blue-steel">
                <div class="portlet-title ">
                    <div class="caption printHeading">
                        <?= $order_type; ?>
                    </div>                          
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_2" width="100%">
                        <?php
                        foreach ($order_details as $key => $value) {
                             $cust_details = $value['customer_details']['customer_name'] . "<br>" . $value['customer_details']['customer_phone_no'];
                                                       
                            //$customer_details = $reportObj->getcustomer_details($value['customer_id']);
                            //$cust_details = $customer_details['customer_name'] . "<br>" . $customer_details['customer_phone_no'];
                            ?>
                            <tr>
                                <td>Order Received By</td>
                                <td><?php echo $cust_details; ?></td>				
                            </tr>

                            <tr>
                                <td>Order Id</td>
                                <td><?= $value['order_id']; ?></td>				
                            </tr>
                            <tr>
                                <td>Order Date</td>
                                <td><?php echo date('d-m-Y h:i:s A', strtotime($value['order_date']));?></td>				
                            </tr>
                            <tr>
                                <td>Category Name</td>
                                <td><?= $value['cat_name']; ?></td>				
                            </tr>
                            <tr>
                                <td>Product</td>
                                <?php
                                $product_name = '';
                                $product_name = $value['product_name'];
                                //$varient_name1 = $reportObj->getvarient_details($value['product_variant1']);
                                //$varient_name2 = $reportObj->getvarient_details($value['product_variant2']);
                                $product_name .= "(" . $value['var_details1'];
                                $product_name .= "-" . $value['var_details2'] . ")";
                                ?>
                                <td><?= $product_name; ?></td>				
                            </tr>

                            <tr>
                                <td>Quantity</td>
                                <td><?= $value['product_quantity']; ?></td>				
                            </tr>
                            <tr>
                                <td>Unit Price (₹)</td>
                                <td><?= $value['product_price']; ?></td>				
                            </tr>


                            <tr>
                                <td>Price (₹)</td>
                                <td><?= $value['total_cost']; ?></td>				
                            </tr>
                            <tr>
                                <td>Price(₹)<br>(GST+Discount)</td>
                                <td><?= $value['total_cost_with_tax']; ?></td>				
                            </tr>
<?php } ?>


                    </table>
                </div>
            </div>
        </div>
    </div>
</div>