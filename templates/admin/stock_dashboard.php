<?php include "../includes/header.php";
   include "../includes/userManage.php";
   include "../includes/stockManage.php";
   $userObj    =   new userManager($con,$conmain);
   $stockObj    =   new stockManager($con,$conmain);
   setlocale(LC_MONETARY, 'en_IN');
   ?>
<!-- END HEADER -->
<script type="text/javascript">
   function fnSelectionBoxTest()
   {
      var str = $( "form" ).serialize();  
      document.getElementById('hdnSelrange').value=document.getElementById('selTest').value;
      if(document.getElementById('selTest').value == '3')
      {
         document.getElementById('date-show').style.display = "block";
      }
      else
      document.frmSearch.submit();
   }    
</script>
<?php
   $records = $stockObj->TotalStockInOutproduct();
   $total_stock_in_quantity = $records['total_stock_in_quantity'];
   $total_stock_out_quantity = $records['total_stock_out_quantity'];
   $total_stock_assign_quantity = $records['total_stock_assign_quantity'];
?>
<!-- BEGIN BODY -->
<style type="text/css">
   #sample_6_filter {
   text-align: right;   }
   #sample_2_filter {
   text-align: right;   }
   #sample_2_paginate {
   text-align: right;    }
   #sample_6_paginate {
      text-align: right;   }
table.dataTable thead .sorting {
    background: url(https://salzpoint.net/uatnew-dev/kaysons/assets/global/plugins/datatables/media/images/sort_both.png) center right no-repeat;}
table.dataTable thead .sorting_asc {
    background: url(https://salzpoint.net/uatnew-dev/kaysons/assets/global/plugins/datatables/media/images/sort_asc.png) center right no-repeat;}
table.dataTable thead .sorting_desc {
    background: url(https://salzpoint.net/uatnew-dev/kaysons/assets/global/plugins/datatables/media/images/sort_desc.png) center right no-repeat;}
</style>
<body class="page-header-fixed page-quick-sidebar-over-content ">
   <div class="clearfix"></div>
   <!-- BEGIN CONTAINER -->
   <div class="page-container">
      <?php
         $activeMainMenu = "Stock";
         $activeMenu = "stock_dashboard";
         include "../includes/sidebar.php";
         ?>
      <!-- END SIDEBAR -->
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
         <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">Stock Management <small>Reports & Statistics</small></h3>
            <div class="page-bar">
               <ul class="page-breadcrumb">
                  <li>
                     <i class="fa fa-home"></i>
                     <a href="index.php">Home</a>
                     <i class="fa fa-angle-right"></i>
                  </li>
                  <li>
                     <a href="#">Stock Management</a>
                  </li>
               </ul>
               <!--<a id="treeview" href="#" class="btn btn-sm btn-default pull-right mt5">
                  Treeview
                  </a>-->
            </div>
            <!-- END PAGE HEADER-->   
            <!-- BEGIN DASHBOARD STATS -->
            <div class="row">
               <div class="col-sm-3 col-xs-12">
                  <div class="dashboard-stat blue-madison">
                     <div class="visual">
                        <i class="fa fa-comments"></i>
                     </div>
                     <div class="details">
                        <div class="number">
                           <?php;
                              if($total_stock_in_quantity>0){

                               echo substr(money_format('%!i', $total_stock_in_quantity), 0, -3); }else{echo '0';}
                              ?>&nbsp;<!-- <i aria-hidden="true" class="fa fa-inr fa-6"></i> -->
                        </div>
                        <div class="desc">Total Stock IN</div>
                     </div>
                  </div>
               </div>
                <div class="col-sm-3 col-xs-12">
                  <div class="dashboard-stat blue-madison" style="background-color: #bd6452;">
                     <div class="visual">
                        <i class="fa fa-area-chart"></i>
                     </div>
                     <div class="details">
                        <div class="number">
                          
                            <?php;
                              if($total_stock_assign_quantity>0){echo substr(money_format('%!i', $total_stock_assign_quantity), 0, -3);}else{echo '0';}
                              ?> &nbsp;<!-- <i aria-hidden="true" class="fa fa-inr fa-6"></i> -->
                        </div>
                        <div class="desc">Total Assigned For Delivery</div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-3 col-xs-12">
                  <div class="dashboard-stat red-intense">
                     <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                     </div>
                     <div class="details">
                        <div class="number">
                           <?php 
                              if($total_stock_out_quantity>0){echo substr(money_format('%!i', $total_stock_out_quantity), 0, -3);}else{echo '0';}  
                              ?>&nbsp;
                        </div>
                        <div class="desc">
                           Total Delivered
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-3 col-xs-12">
                  <div class="dashboard-stat green-haze">
                     <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                     </div>
                     <div class="details">
                        <div class="number">
                           <?php
                                  
                              $total_stock_assign_or_delivered = $total_stock_out_quantity + $total_stock_assign_quantity;
                              if ($total_stock_in_quantity>= $total_stock_assign_or_delivered) 
                                     {
                                $total_stock_available =$total_stock_in_quantity -  $total_stock_assign_or_delivered;
                                        if($total_stock_available>0)
                                         {
                                           echo substr(money_format('%!i', $total_stock_available), 0, -3);
                                         } else { echo '0';}  
                              }
                                     else
                                     {
                                        echo '0';
                                     }
                              ?>&nbsp;
                        </div>
                        <div class="desc">
                           Total Available Stock
                        </div>
                     </div>
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
            <div class="row">
               <div class="col-sm-12">
                  <div class="portlet box blue-steel">
                     <div class="portlet-title">
                        <div class="caption">Product Management</div>
                        <?php if($ischecked_edit=='1'){?> 
                        <a style="margin-left: 5px;" id="btnEmpty" class="btn btn-sm btn-default pull-right mt5" href="stock_remove.php">Remove Stock</a>
                        &nbsp;&nbsp;
                        <a id="btnEmpty" class="btn btn-sm btn-default pull-right mt5" href="stock_add.php">Add Stock</a>
                        <?php }?>
                        <div class="clearfix"></div>
                     </div>
                     <div class="portlet-body">
                        <form id="frmCart">
                           <table class="table table-striped table-bordered table-hover" id="sample_2">
                              <thead>
                                 <tr>
                                    <th> Product Name </th>
                                    <th> Product Variant  </th>
                                    <th> Product Price  </th>
                                    <th> Total Stock IN </th>
                                    <th> Total Orders Received</th>
                                    <th>  Delivered </th>
                                    <th> Available Stock  </th>
                                    <th>   Stock Updated Date </th>
                                    <!--  <th>Today’s Production  </th>
                                       <th> Today’s Orders  </th> 
                                       <th> Today’s Delivered Orders </th>  -->
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    /* $sql = "SELECT b.productname,pv.price,pv.variant_1,pv.variant_2,pv.variant1_unit_id,pv.variant2_unit_id,pv.id as pvid, "
                                            . "b.added_by_userid,b.added_by_usertype, a.id AS cat_id  "
                                            . " FROM tbl_category a,tbl_product b "
                                            . " left join tbl_product_variant pv on b.id = pv.productid "
                                            . " where a.id=b.catid AND b.isdeleted != 1 "
                                            . " and ((b.added_by_userid=0) OR (b.added_by_userid=1) OR (b.added_by_userid='$user_id'))";*/
                                    
                                     $sql1 = "SELECT parent_ids FROM tbl_user where id='$user_id'";
                                      $result1 = mysqli_query($con, $sql1);
                                      $row1 = mysqli_fetch_assoc($result1);
                                      $parent_ids = $row1['parent_ids'];
                                     $sql = "SELECT a.categorynm,b.productname,b.id,pv.producthsn,pv.price as price,pv.variant_1,pv.variant_2,pv.variant1_unit_id,pv.variant2_unit_id,pv.id as pvid, "
                                            . " pv.productimage,b.added_by_userid,b.added_by_usertype, a.id AS cat_id  "
                                            . " FROM tbl_category a,tbl_product b "
                                            . " left join tbl_product_variant pv on b.id = pv.productid "
                                            . " where a.id=b.catid AND b.isdeleted != 1 "
                                            . " and (b.added_by_userid='".$user_id."' OR b.added_by_userid IN (".$parent_ids."))";
                                    $result = mysqli_query($con, $sql);
                                    $row_count = mysqli_num_rows($result);
                                    while ($row = mysqli_fetch_array($result)) 
                                    { 
                                     // echo "<pre>";print_r($row);
                                        $prodcode = $row['id'] . '_' . $row['pvid'];
                                        ?>
                                 <tr class="odd gradeX" >
                                    <td >
                                       <?php echo fnStringToHTML($row['productname']); ?>
                                    </td>
                                    <td >
                                       <?php
                                          $sqlvarunit3 = "SELECT unitname FROM tbl_units  where id='" . $row['variant1_unit_id'] . "'";
                                          $resultvarunit3 = mysqli_query($con, $sqlvarunit3);
                                          while ($rowvarunit3 = mysqli_fetch_array($resultvarunit3)){
                       $unitname3 = $rowvarunit3['unitname'];
                                          }
                                          
                                          $sqlvarunit4 = "SELECT unitname FROM tbl_units  where id='" . $row['variant2_unit_id'] . "'";
                                          $resultvarunit4 = mysqli_query($con, $sqlvarunit4);
                                          while ($rowvarunit4 = mysqli_fetch_array($resultvarunit4)) {
                         $unitname4 = $rowvarunit4['unitname'];
                                          }
                                          echo $row['variant_1']."-".$unitname3." , ".$row['variant_2']."-".$unitname4;
                                          echo "<br>";
                                            ?>
                                    </td>
                                    <td align="right">
                                       <?php echo fnStringToHTML($row['price']); ?>
                                    </td>
                                    <td align="right" >
                                       <?php  
                                          $total_stock_in_quantity=0;
                                          if ($user_role=='Admin'){
                        $sqlvarunit5 = "SELECT sum(product_quantity) as total_stock_in_quantity FROM tbl_stock_management  where product_varient_id='" . $row['pvid'] . "' AND added_by_userid='".$user_id."'";
                        $resultvarunit5= mysqli_query($con, $sqlvarunit5);
                        while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5))  {
                        $total_stock_in_quantity = $rowvarunit5['total_stock_in_quantity'];
                        }
                        if (isset($total_stock_in_quantity))  {
                        echo $total_stock_in_quantity;
                        }else { echo '0'; }  
                                         }else{
                                          
                         $sqlvarunit7 = "SELECT sum(product_quantity) as total_stock_in_quantity FROM tbl_stock_management  where product_varient_id='" . $row['pvid'] . "' AND added_by_userid='".$user_id."'";
                         $resultvarunit7= mysqli_query($con, $sqlvarunit7);
                         while ($rowvarunit7 = mysqli_fetch_array($resultvarunit7))  {
                        $total_stock_by_user = $rowvarunit7['total_stock_in_quantity'];
                         }
                                                  
                        $sqlvarunit5 = "SELECT o.id,sum(od.product_quantity) as total_stock_in_quantity FROM tbl_orders AS o LEFT JOIN tbl_order_details AS od ON od.order_id = o.id where o.order_from='".$user_id."' AND od.product_variant_id='" . $row['pvid'] . "' AND ( od.order_status =4) ";//od.order_status =2 OR-- Only deliverd Stock IN Count
                        $resultvarunit5= mysqli_query($con, $sqlvarunit5);
                        while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) {
                        $total_stock_by_parent = $rowvarunit5['total_stock_in_quantity'];
                        }
                        
                         $total_stock_in_quantity = $total_stock_by_user + $total_stock_by_parent;
                        if (isset($total_stock_in_quantity))  {
                        echo $total_stock_in_quantity;
                        }    else { echo '0'; }  
                                         } ?>
                                    </td>
                                    <td align="right" >
                                       <?php  
                                          $sqlvarunit5 = "SELECT o.id,sum(od.product_quantity) as total_product_quantity FROM tbl_orders AS o LEFT JOIN tbl_order_details AS od ON od.order_id = o.id where o.order_to='".$user_id."' AND od.product_variant_id='" . $row['pvid'] . "' ";
                                           $resultvarunit5= mysqli_query($con, $sqlvarunit5);
                                           while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) 
                                           {
                                             $total_product_quantity_bychild = $rowvarunit5['total_product_quantity'];
                                           }   
                                           $sqlvarunit5 = "SELECT sum(product_quantity) as total_product_quantity FROM tbl_customer_orders_new 
                                               where order_to='".$user_id."' AND product_variant_id='" . $row['pvid'] . "' ";
                                           $resultvarunit5= mysqli_query($con, $sqlvarunit5);
                                           while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) 
                                           {
                                             $total_product_quantity_bycust = $rowvarunit5['total_product_quantity'];
                                           }
                                          
                                           $total_product_quantity = $total_product_quantity_bychild + $total_product_quantity_bycust;
                                           if (isset($total_product_quantity)) 
                                           {
                                            echo $total_product_quantity;
                                           }
                                           else { echo '0'; }                   
                                           //echo "<br>";
                                             ?>                  
                                    </td>
                                    <td align="right" >
                                       <?php  
                                          $total_stock_out_quantity=0;
                                           $sqlvarunit5 = "SELECT o.id,sum(od.product_quantity) as total_stock_out_quantity FROM tbl_orders AS o LEFT JOIN tbl_order_details AS od ON od.order_id = o.id where o.order_to='".$user_id."' AND od.product_variant_id='" . $row['pvid'] . "' AND ( od.order_status =2 OR od.order_status =4)";
                                           $resultvarunit5= mysqli_query($con, $sqlvarunit5);
                                           while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) 
                                           {
                                               $total_stock_out_quantity_tochild = $rowvarunit5['total_stock_out_quantity'];
                                           }
                                          
                                           $sqlvarunit5 = "SELECT sum(product_quantity) as total_stock_out_quantity 
                                           FROM tbl_customer_orders_new  where order_to='".$user_id."' AND product_variant_id='" . $row['pvid'] . "' AND ( order_status =2 OR order_status =4)";
                                           $resultvarunit5= mysqli_query($con, $sqlvarunit5);
                                           while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) 
                                           {
                                               $total_stock_out_quantity_tocust = $rowvarunit5['total_stock_out_quantity'];
                                           }
                                          
                                          $total_stock_out_quantity = $total_stock_out_quantity_tochild + $total_stock_out_quantity_tocust;
                                          
                                           if (isset($total_stock_out_quantity)) {
                                            echo $total_stock_out_quantity;
                                           } else { echo '0'; } 
                                             ?> 
                                    </td>
                                    <td align="right" >
                                       <?php  
                                          if ($total_stock_in_quantity >= $total_stock_out_quantity){
                                            $total_available_stock = $total_stock_in_quantity-$total_stock_out_quantity;
                                         }else{
                                             $total_available_stock=0;
                                          }
                                          ?>
                                       <?php
                                          if (isset($total_available_stock) && $total_available_stock>0){
                                          ?>
                                       <div style="color: green;">  
                                          <?php echo $total_available_stock; ?>
                                       </div>
                                       <?php
                                          }
                                          else 
                                          {
                                          ?>                     
                                       <a href="#" style="color: red;"> 0 </a>                       
                                       <?php }  
                                          ?>
                                    </td>
                                    <td align="left" >
                                       <?php  
                                          if ($user_role=='Admin'){
                        $last_date='';
                        $sqlvarunit5 = "SELECT id,createdon as last_date FROM tbl_stock_management where added_by_userid='".$user_id."' AND product_varient_id='" . $row['pvid'] . "' ORDER BY id DESC LIMIT 1";
                        $resultvarunit5= mysqli_query($con, $sqlvarunit5);
                        while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) {
                          $last_date = $rowvarunit5['last_date'];
                        }
                        if (isset($last_date))  { 
                          echo date("d-m-Y H:i:s",strtotime($last_date));
                        } else { echo '-'; }  
                                         }else{
                                                $last_date_added_byuser='';
                                                $last_date_added_byparent='';
                                          
                        $sqlvarunit77 = "SELECT id,createdon as last_date FROM tbl_stock_management  where product_varient_id='" . $row['pvid'] . "' AND added_by_userid='".$user_id."' ORDER BY id DESC LIMIT 1";
                                          
                        $sqlvarunit53 = "SELECT o.id,o.order_date as last_date 
                                                   FROM tbl_orders AS o 
                                                   LEFT JOIN tbl_order_details AS od ON od.order_id = o.id where o.order_from='".$user_id."' AND od.product_variant_id='" . $row['pvid'] . "' AND ( od.order_status =2 OR od.order_status =4) ORDER BY o.id DESC LIMIT 1";
                                          
                        $resultvarunit53= mysqli_query($con, $sqlvarunit53);
                        while ($rowvarunit53 = mysqli_fetch_array($resultvarunit53)){
                          $last_date_added_byparent = date("d-m-Y H:i:s",strtotime($rowvarunit53['last_date']));
                        }
                                                $resultvarunit77= mysqli_query($con, $sqlvarunit77);
                                                while ($rowvarunit77 = mysqli_fetch_array($resultvarunit77)){
                          $last_date_added_byuser = date("d-m-Y H:i:s",strtotime($rowvarunit77['last_date']));
                        }
                        // Use comparison operator to  
                        // compare dates 
                        if (isset($last_date_added_byuser) && $last_date_added_byuser!='' && isset($last_date_added_byparent) && $last_date_added_byparent!=''){
                          if ($last_date_added_byuser >= $last_date_added_byparent){
                            echo $last_date_added_byuser; 
                          }else{
                            echo $last_date_added_byparent; 
                          }
                        } elseif (isset($last_date_added_byuser) && $last_date_added_byuser!='' && $last_date_added_byparent==''){ 
                          echo $last_date_added_byuser;
                        }elseif ($last_date_added_byuser=='' && isset($last_date_added_byparent) && $last_date_added_byparent!=''){
                          echo $last_date_added_byparent;
                        }else{echo "-"; }
                                          }  ?>
                                    </td>
                                 </tr>
                                 <?php } ?>             
                              </tbody>
                           </table>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <div class="portlet box blue-steel">
                     <div class="portlet-title">
                        <div class="caption">
                           Stock History
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_6">
                           <thead>
                              <tr>
                                 <th>Brand</th>
                                 <th>Category</th>
                                 <th>Product Name</th>
                                 <th>Product Variant</th>
                                 <th>Product Price</th>
                                 <th>Updated Quantity</th>
                                 <th>Stock Status</th>
                                 <th>Stock Updated Date</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php
                                 $result1 = $stockObj->getStockHistory();
                                 //echo "<pre>";print_r($result1);
                                 while ($row = mysqli_fetch_array($result1)){ 
                                 ?>
                              <tr class="odd gradeX">
                                 <td><?=$row['brand_name'];?></td>
                                 <td><?=$row['category_name'];?></td>
                                 <td><?=$row['product_name'];?></td>
                                 <td><?=$row['product_variant1'].'-'.$row['variant1_unit_name'].' , '.$row['product_variant2'].'-'.$row['variant2_unit_name'];?></td>
                                 <td align="right"><?=$row['product_price'];?></td>
                                 <td align="right"><?=abs($row['product_quantity']);?></td>
                                 <td>
                                    <?php
                                       if ($row['dummy_column2']=='1') {
                                         echo "Added";
                                       }
                                       if ($row['dummy_column2']=='2') {
                                         echo "Removed";
                                       }
                                       if ($row['dummy_column']=='my_chield_order') {
                                         echo "Assigned";
                                       }
                                       if ($row['dummy_column']=='my_order') {
                                         echo "Approved";
                                       }
                                      if ($row['dummy_column']=='my_cust_order') {
                                         echo "Delivered";//customer
                                       }
                                       ?>
                                 </td>
                                 <td>                               
                                    
                                    <span style="display: none;"><?=date("Y-m-d",strtotime($row['createdon']));?></span>             
                                    <?=date("d-m-Y H:i:s",strtotime($row['createdon']));?>
                                 </td>
                              </tr>
                              <?php } ?>      
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- END CONTENT -->
      <!--Treeview Modal -->
      <div id="dialog" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Modal Header</h4>
               </div>
               <div class="modal-body">
                  <p>Some text in the modal.</p>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <!--Treeview modal end-->
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
   <?php include "../includes/footer.php"?>
  
   <!-- END FOOTER -->
   <script type="text/javascript" src="../../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
   <script type="text/javascript" src="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
   <script type="text/javscript" src="https://cdn.datatables.net/plug-ins/1.10.15/api/fnFilterClear.js"></script>
  
   <script>
      $(document).ready(function () {

   $('table.table table-striped table-bordered table-hover').DataTable();
        var table = $('#sample_2').dataTable({ 
               destroy: true,
             // stateSave: true,
            //  "aaSorting": [],
              "order": [[ 0, "desc" ]]//,//set last (Product Name) column by default desc orer
              //aoColumnDefs: [{ 
              //      'bSortable': false, 
              //      'aTargets': [ 1 ] // <-- gets last column and turns off sorting
               //    }]
              });
               $('input[type=search]').on("input", function() {
                  table.fnFilter('');
              });


       var table6 = $('#sample_6').dataTable(
        {
          destroy: true,
          "order": [[ 7, "desc" ]]//,//set last (Product Name) column by default desc orer
             // aoColumnDefs: [{ 
              //      'bSortable': false, 
              //      'aTargets': [ 1 ] // <-- gets last column and turns off sorting
               //    }]
        });
       $('input[type=search]').on("input", function() {
          table6.fnFilter('');
      });
      $("#btn1").click(function(){            
          var app =$("#prodvar").html();
          $("#demo").append(app);
      }); 


          ShowReport(1);
          $.ajax
          ({
              type: "POST",
              url: "sales_leave_details.php",
              success: function (msg)
              {
                  $("#chartleave").html(msg);
              }
          });
           $.ajax
          ({
              type: "POST",
              url: "sales_location_current.php",
              success: function (msg)
              {
                  $("#chartsp_current").html(msg);
              }
          });
      });
      
      function ShowReport(page) {
          //alert("dsfsd");
          var param = '';
      var date = new Date();
      date.setDate(date.getDate() - 1);
         param = param + 'frmdate=' + date;
      $.ajax
      ({
      type: "POST",
      url: "dashboard_sales_expenses.php",
      data: param,
      success: function (msg)
      {
      $("#order_summary_details").html(msg);
      }
      });
          
      }
      
      function report_download() {
          var td_rec = $("#sample_2 td:last").html();
          if (td_rec != 'No matching records found')
          {
              var divContents = $(".table-striped").html();
              $("#print_div").html('<table id="print_table" style="text-decoration:none;">' + divContents + '</table>');
              var heading = $("#table_heading").html();
              $("#print_table tr th i").html("RS");
              divContents = $("#print_div").html();
      
              divContents = divContents.replace(/<\/*a.*?>/gi, '');
              $("#export_data").val(divContents);
              document.forms.export_excel.submit();
          } else {
              alert("No matching records found");
          }
      }
      
      $("#btnPrint").live("click", function () {
          var td_rec = $("#sample_2 td:last").html();
          if (td_rec != 'No matching records found')
          {
              var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
      
              var divContents1 = $(".table-scrollable").html();
              $("#print_div").html(divContents1);
              $("#print_div a").removeAttr('href');
              $("#sample_2 tr th i").html("");
              var divContents = $("#print_div").html();
              var printWindow = window.open('', '', 'height=400,width=800');
              printWindow.document.write('<html><head><title>Report</title>');
              printWindow.document.write('<style>a{text-decoration: none; color:#333333;} #sample_2{margin:20 20 20 20px; width:700px}</style>');
              printWindow.document.write('<link   rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css"/>');
              printWindow.document.write('<link  rel="stylesheet" type="text/css" href="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>');
      
              if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
                  printWindow.document.write('</head><body >');
                  printWindow.document.write(divContents);
                  printWindow.document.write('</body></html>');
                  printWindow.focus();
                  setTimeout(function () {
                      printWindow.print();
                      //printWindow.close();
                  }, 500);
              } else if (isIE == true) {
                  printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
                  printWindow.document.write('</head><body >');
                  printWindow.document.write(divContents);
                  printWindow.document.write('</body></html>');
                  printWindow.focus();
                  printWindow.document.execCommand("print", false, null);
                  //printWindow.close();
              } else {
                  printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
                  printWindow.document.write('</head><body >');
                  printWindow.document.write(divContents);
                  printWindow.document.write('</body></html>');
                  printWindow.focus();
                  setTimeout(function () {
                      printWindow.print();
                      //printWindow.close();
                  }, 100);
              }
          } else {
              alert("No matching records found");
          }
      });
   </script>
</body>
<!-- END BODY -->
</html>