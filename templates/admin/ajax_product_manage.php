<?php
include ("../../includes/config.php");
include ("../includes/productManage.php");
$objProduct = new productManage($con,$conmain);
$shop_action=base64_decode($_GET['shop_action']);		 
$shop_id=base64_decode($_GET['shop_id']);
$cust_id=base64_decode($_GET['cust_id']);  
$cust_action=base64_decode($_GET['cust_action']);	
if ($shop_action =='delete_shop') 
{
$sql6="SELECT od.order_id,o.order_from,o.shop_id,od.order_status 
FROM tbl_orders o
LEFT JOIN tbl_order_details od ON od.order_id = o.id
where o.shop_id='".$shop_id."' AND (od.order_status=1 OR od.order_status=2) GROUP BY od.order_id";
$result6 = mysqli_query($con,$sql6);
$row_count6 = mysqli_num_rows($result6);
	if ($row_count6 > 0 ) 
	{
	echo '<script>alert("You Can Not Delete This Shop. Because This Shop Orders Are Pending.");location.href="shops.php";</script>';
	}
	else
	{
		$objProduct->deleteShopbyid($shop_id);
		echo '<script>alert("Shop deleted successfully.");location.href="shops.php";</script>';
	}
}

if ($cust_action =='delete_customer') 
{	
	    $objProduct->deleteCustomerbyid($cust_id);
		echo '<script>alert("Customer deleted successfully.");location.href="customer_list.php";</script>';
}
//exit();
switch($_GET['action']) {
	case "delete_brand":
		$objProduct->deleteBrand($_GET['brand_id']);
		echo 'brand_deleted';
	break;
	case "delete_category":													
		$objProduct->deleteCategory($_GET['brand_id'], $_GET['cat_id']);
		echo 'category_deleted';
	break;
	case "delete_product":										
		$objProduct->deleteProduct($_GET['cat_id'], $_GET['product_id']);
		echo 'product_deleted';
	break;
	case "delete_route":
		$objProduct->deleteRoute1($_GET['route_id']);
		echo 'route_deleted';
	break;
	case "check_product_available":
		$objProduct->check_product_available();
	break;
	case "delete_unit":
		$objProduct->deleteUnit($_GET['unit_id']);
		echo 'unit_deleted';
	break;
	case "delete_area":
		$objProduct->deleteAreabyid($_GET['area_id']);
		echo 'area_deleted';
	break;
	case "delete_subarea":
		$objProduct->deleteSubareabyid($_GET['subarea_id']);
		echo 'subarea_deleted';
	break;
	/*case "delete_shop":
		$objProduct->deleteShopbyid($shop_id);
		echo '<script>alert("Shop deleted successfully.");location.href="shops.php";</script>';
		//echo 'shop_deleted';
	break;*/
	case "shop_inactive":
		$objProduct->inactiveShopbyid($_GET['shop_id']);
		echo 'shop_inactive';
	break;
	case "shop_active":
		$objProduct->activeShopbyid($_GET['shop_id']);
		echo 'shop_active';
	break;
	case "delete_lead":
		$objProduct->deleteLeadbyid($_GET['lead_id']);
		echo 'lead_deleted';
	break;
	case "delete_assigned_campaign":
		$objProduct->deleteAssignedCampaign($_GET['sp_id']);
		echo 'delete_assigned_campaign';
	break;
	case "delete_order":
		$objProduct->delete_order($_GET['order_id']);
		echo 'delete_order';
	break;
	case "delete_order_cust":
		$objProduct->delete_order_cust($_GET['cust_order_id']);
		echo 'delete_order_cust';
	break;	
	default: 
	break;	
}