 <div class="form-group">
              <label class="col-md-3">Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text"
				placeholder="Enter Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"				
				name="firstname" class="form-control">
              </div>
            </div><!-- /.form-group -->
             
			<div class="form-group">
              <label class="col-md-3">Username:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" id="username"
				placeholder="Enter Username"
				data-parsley-minlength="6"
				data-parsley-minlength-message="Username should be minimum 6 characters without blank spaces"          
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"          
				data-parsley-type-message="Please enter Username, blank spaces are not allowed"		   
				data-parsley-required-message="Please enter Username, blank spaces are not allowed"
				data-parsley-trigger="change"
				data-parsley-required="true"
				data-parsley-pattern="/^\S*$/" 
				data-parsley-error-message="Username should be 6-50 characters without blank spaces"
				name="username" class="form-control"><span id="user-availability-status"></span>
              </div>
            </div><!-- /.form-group -->
			<div class="form-group">
              <label class="col-md-3">Password:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="password" id="password"
				placeholder="Enter Password"			   
				data-parsley-minlength="6"
				data-parsley-minlength-message="Password should be minimum 6 characters without blank spaces"   
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"   
				data-parsley-type-message="Password should be minimum 6 characters without blank spaces"		   
				data-parsley-required-message="Please enter Password"
				data-parsley-trigger="change"
				data-parsley-required="true"
				data-parsley-pattern="/^\S*$/" 
				data-parsley-error-message="Password should be 6-50 characters without blank spaces"
				name="password" class="form-control placeholder-no-fix"><!--<span toggle="#password" id="toggle-password" class="fa fa-fw fa-eye field-icon toggle-password"></span>-->
              </div>
            </div><!-- /.form-group -->
           <div class="form-group">
              <label class="col-md-3">Confirm Password:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="password" id="c_password"
				placeholder="Enter Confirm Password"
				data-parsley-trigger="change"	
				data-parsley-equalto="#password"
				data-parsley-equalto-message="Password does not match with confirm password"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter confirm password"		
				name="c_password" class="form-control"><span id="user-availability-status"></span>
              </div>
            </div><!-- /.form-group -->
			<div class="form-group">
				<label class="col-md-3">Address:</label>
				<div class="col-md-4">
					<textarea name="address"
					placeholder="Enter Address"
					data-parsley-trigger="change"
					data-parsley-maxlength="200"
					data-parsley-maxlength-message="Only 200 characters are allowed"							
					rows="4" class="form-control" ></textarea>
				</div>
			</div><!-- /.form-group -->
			<?php if($page_to_add != 'sales_person'){ ?>
			<div id="working_area_top">
			<h4>Assign <?php if($page_to_add == 'stockist'){?>Taluka<?php }else{ ?>District<?php }?></h4>
			</div>
			<div class="form-group">
				<label class="col-md-3">State:</label>
				<div class="col-md-4">
				<select name="state"  id="state"
				class="form-control" onChange="fnShowCity(this.value)">
				<option selected disabled>-Select-</option>
				<?php
				$sql="SELECT id,name FROM tbl_state where country_id=101 order by name";
				$result = mysqli_query($con,$sql);
				while($row = mysqli_fetch_array($result))
				{
					$cat_id=$row['id'];
					$selected_state = '';
					if($default_state == $row['id'])
						$selected_state = 'selected';
					echo "<option value='$cat_id' $selected_state>" . $row['name'] . "</option>";
				} ?>
				</select>
				</div>
			</div>			
			<div class="form-group" id="city_div" <?php if($page_to_add != 'stockist'){echo 'style="display:none;"';} ?>>
			  <label class="col-md-3">District:</label>
			  <div class="col-md-4" id="div_select_city">
			  <?php if($page_to_add == 'stockist'){ ?>
				 <select name="city" id="city" 				
				class="form-control" onchange="FnGetSuburbDropDown(this)">
				<?php
				$sql="SELECT id,name FROM tbl_city where state_id=".$default_state." order by name";
				$result = mysqli_query($con,$sql);
				while($row = mysqli_fetch_array($result))
				{
					$cat_id=$row['id'];
					$selected_city = '';
					if($default_city == $row['id'])
						$selected_city = 'selected';
					echo "<option value='$cat_id' $selected_city>" . $row['name'] . "</option>";
				} ?>
				</select>
			  <?php }else{ ?>
			  <select name="city" id="city" 				
				class="form-control">
				<option selected value="">-Select-</option>										
				</select>
				 <?php } ?>
			  </div>
			</div><!-- /.form-group -->
            <div class="form-group" id="area_div" <?php if($page_to_add != 'stockist'){echo 'style="display:none;"';} ?>>
			  <label class="col-md-3">Taluka:</label>
			  <div class="col-md-4" id="div_select_area">
			  <?php if($page_to_add == 'stockist'){ ?>
				 <select name="area" id="area" data-parsley-trigger="change" class="form-control">
				<?php
				$sql="SELECT id,suburbnm FROM tbl_area where cityid=$default_city order by suburbnm";
				$result = mysqli_query($con,$sql);
				$row_count = mysqli_num_rows($result);
				if($row_count > 0){
					while($row = mysqli_fetch_array($result))
					{
						$cat_id=$row['id'];
						$selected_area = '';
						if($default_area == $row['id'])
							$selected_area = 'selected';
						echo "<option value='$cat_id' $selected_area>" . $row['suburbnm'] . "</option>";
					}
				}else{
					echo "<option selected value=''>-Select-</option>";
				} ?>
				</select>
			  <?php }else{ ?>
			  <select name="area" id="area" data-parsley-trigger="change" class="form-control">
				<option selected value="">-Select-</option>									
				</select>
			  <?php } ?>
			  </div>
			</div><!-- /.form-group --> 						
			<div class="form-group" id="subarea_div" style="display:none;">
			  <label class="col-md-3">Subarea:</label>
			  <div class="col-md-4" id="div_select_subarea">
			  <select name="subarea" id="subarea" data-parsley-trigger="change" class="form-control">
				<option selected value="">-Select-</option>									
				</select>
			  </div>
			</div><!-- /.form-group --> 
			<div id="working_area_bottom">				
			</div>
			<?php } ?>
			 <div class="form-group">
              <label class="col-md-3">Email:</label>

              <div class="col-md-4">
                <input type="text" id="email"
				placeholder="Enter E-mail"
				data-parsley-maxlength="100"
				data-parsley-maxlength-message="Only 100 characters are allowed"
				data-parsley-type="email"
				data-parsley-type-message="Please enter valid e-mail" 
				data-parsley-trigger="change"
				name="email" class="form-control"><span id="user-availability-status"></span>
              </div>
            </div><!-- /.form-group -->
			<div class="form-group">
				  <label class="col-md-3">Mobile Number:</label>

				  <div class="col-md-4">
					<input type="text"  name="mobile"
					placeholder="Enter Mobile Number"
					data-parsley-trigger="change"					
					data-parsley-minlength="10"
					data-parsley-maxlength="15"
					data-parsley-maxlength-message="Only 15 characters are allowed"
					data-parsley-pattern="^(?!\s)[0-9]*$"
					data-parsley-pattern-message="Please enter numbers only"
					class="form-control" value="">
				  </div>
				</div><!-- /.form-group -->
				<?php if($page_to_add == 'superstockist'||$page_to_add =='stockist'){ ?>
				<div class="form-group">
              <label class="col-md-3">GST Number:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text"
				placeholder="Enter GST Number"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter GST Number"
				data-parsley-maxlength="15"
				data-parsley-maxlength-message="Only 15 characters are allowed"	
				data-parsley-minlength="15"
				data-parsley-minlength-message="Not Valid GST Number"	
				name="gstnumber" class="form-control">
              </div>
            </div><!-- /.form-group -->
				<?php } ?>
				<input type="hidden" name="page_to_add" id="page_to_add" value="<?=$page_to_add;?>">
<script>
function calculate_data_count(element_value){
	element_value = element_value.toString();
	var element_arr = element_value.split(',');	
	return element_arr.length;
}
function setSelectNoValue(div,select_element){
	var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled value="">-Select-</option></select>';
	document.getElementById(div).innerHTML	=	select_selement_section;
}
function fnShowCity(id_value) {	
	$("#city_div").show();	
	$("#area").html('<option value="">-Select-</option>');	
	$("#subarea").html('<option value="">-Select-</option>');	
	var page_to_add = $("#page_to_add").val();
	var param = '';
	if(page_to_add == 'superstockist')
		param = "&nofunction=nofunction";
	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city&mandatory=mandatory"+param;
	CallAJAX(url,"div_select_city");	
}
function FnGetSuburbDropDown(id) {
	$("#area_div").show();	
	$("#subarea").html('<option value="">-Select-</option>');	
	var page_to_add = $("#page_to_add").val();
	var param = '';
	if(page_to_add == 'stockist')
		param = "&nofunction=nofunction";
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&multiple=multiple&function_name=FnGetSubareaDropDown"+param;
	CallAJAX(url,"div_select_area");
}
function FnGetSubareaDropDown(id) {	
	var suburb_str = $("#area").val();	
	var suburb_arr_count = calculate_data_count(suburb_str);
	if(suburb_arr_count == 1){//If single city selected then only show its related subarea	
		$("#subarea_div").show();	
		var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea&multiple=multiple";
		CallAJAX(url,"div_select_subarea");
	}else if(suburb_arr_count > 1){
		$("#subarea_div").show();	
		var multiple_id = suburb_str.join(", ");
		var url = "getSubareaDropdown.php?multiple_id="+multiple_id+"&select_name_id=subarea&multiple=multiple";
		CallAJAX(url,"div_select_subarea");
	}else{
		setSelectNoValue("div_select_subarea", "subarea");
	}		
}

function checkAvailability() {
	$('#addform').parsley().validate();
	var email = $("#email").val();
	var username = $("#username").val();
	var validate = 1;
	if(username != '')
	{
		validate = 1;
		jQuery.ajax({
			url: "../includes/checkUserAvailable.php",
			data:'validation_field=username&username='+username,
			type: "POST",
			async:false,
			success:function(data){		
				if(data=="exist") {
					alert('Username already exists.');
					validate = 1;
				} else {
					validate = 0;										
				}
			},
			error:function (){}
		});
	}
	
	if(email != '')
	{
		validate = 1;
		jQuery.ajax({
			url: "../includes/checkUserAvailable.php",
			data:'validation_field=email&email='+email,
			type: "POST",
			async:false,
			success:function(data){			
				if(data=="exist") {
					alert('E-mail already exists.');
					validate = 1;
					return false;
				} else {
					validate = 0;
				}
			},
			error:function (){}
		});
	}else if(username != '' && validate == 0)
		validate = 0;
	else
		validate = 1;
	
	if(validate == 0)
	{
		var action = $('#hidAction').val();
		$('#addform').attr('action', action);					
		$('#hidbtnsubmit').val("submit");
		$('#addform').submit();
	}	
}
/*$("#toggle-password").click(function() {
$(this).toggleClass("fa-eye fa-eye-slash");
	var input = $($(this).attr("toggle"));
	if (input.attr("type") == "password") {
		input.attr("type", "text");
	} else {
		input.attr("type", "password");
	}
});*/
</script> 
<style>
.field-icon {
  float: right;
  margin-right:10px;
  margin-top: -30px;
  position: relative;
  z-index: 2;
}
</style>