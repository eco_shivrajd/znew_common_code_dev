<?php
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/orderManage.php";
$orderObj = new orderManage($con, $conmain);
$report_title = $orderObj->getReportTitle_forshopadded();

$row = $orderObj->get_all_shop_added_on_by();
//$rowtodays_sp = $orderObj->fnGet_hours_worked_today($shop_added_by,$added_date);
//fnGet_hours_worked_today
//fnGet_todays_expense_bill

//echo "sdfsd<pre>";print_r($row);
$colspan = "6";
?>
<? if($_POST["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
    table, th, td {  border: 1px solid black; } 
    body { font-family: "Open Sans", sans-serif; 
           background-color:#fff;
           font-size: 11px;
           direction: ltr;}
    </style>
    <? }

    ?>

    <table 
        class="table table-striped table-bordered table-hover table-highlight table-checkable" 
    data-provide="datatable" 
    data-display-rows="10"
    data-info="true"
    data-search="true"
    data-length-change="true"
    data-paginate="true"
    id="sample_2">

    <thead>
        <tr>
            <td colspan="<?= $colspan; ?>" align="canter" class="gradeX even" style="text-align:center; font-weight:600;"><h4><b><?php if (!empty($report_title)) echo $report_title;
else echo "Shop Added Report All"; ?></b></h4></td>              
        </tr>
        <tr>
            <th data-filterable="false" data-sortable="true" data-direction="desc">Added Date</th>
            <th data-filterable="false" data-sortable="false" data-direction="de">Sales Person Name</th>
            <th data-filterable="false" data-sortable="false" data-direction="desc">Total Shops</th>	
             <th data-filterable="false" data-sortable="false" data-direction="desc">No.OF Hours worked</th>	
              <th data-filterable="false" data-sortable="false" data-direction="desc"> KM'S </th>
               <th data-filterable="false" data-sortable="false" data-direction="desc">Total Travel Allowance that day</th>	
        </tr>
    </thead>
    <tbody>					
        <?php
        if (!empty($row)) {
            $gtotalq = 0;

            foreach ($row as $key => $value) {
                //echo "<pre>";print_r($value);
                $rowtodays_sp = $orderObj->fnGet_hours_worked_today($value['shop_added_by'],$value['added_date']);
                
                 $rowtodays_sp_tada = $orderObj->fnGet_todays_expense_bill($value['shop_added_by'],$value['added_date']);
                $total1=0;$google_distance=0;
               // echo "<pre>";print_r($rowtodays_sp_tada);//die();
                foreach($rowtodays_sp_tada as $tadakey => $tadavalue){
                    $total1=$total1+($tadavalue['Current_rate_mot']*$tadavalue['distance_covered'])+$tadavalue['food']+$tadavalue['other'];                    
                    $google_distance=$google_distance+$tadavalue['google_distance'];
                }
                
                $gtotalq = $gtotalq + $value['totalunit'];

                if (!empty($value['totalunit'])) {
                    $unitqnty = $value['totalunit'];
                } else {
                    $unitqnty = 0;
                }
                ?>
                <tr class="odd gradeX">
                    <td  ><?php echo date('d-m-Y h:i:s A', strtotime($value['added_date'])); ?></td>
                    <td  ><?= $value['firstname']; ?></td>
                    <td align='right'><?= $unitqnty; ?></td>
                    
                     <td ><?php  if(count($rowtodays_sp)==1){
                        echo "".round($rowtodays_sp[0]['hours_difference'],2)."";
                          }else{ echo '--'; } ?></td>
                     <td align='right'><?php echo round($google_distance,2); ?></td>
                    <td align='right'><?php echo round($total1,2); ?></td>
                     
                </tr>
            <?php } ?>
    <!--			<tr class="odd gradeX">
                    <td  ></td>
                    <td  ><b>Total</b></td>
                    <td align='right'><b><?= $gtotalq; ?></b></td>
            </tr>-->
            <?php
        }/* else{
          echo "<tr class='odd gradeX'><td colspan='3' align='center'>No matching records found</td></tr>";
          } */
        if ($_POST["actionType"] == "excel" && $row == 0) {
            echo "<tr class='odd gradeX'><td colspan='3'>No matching records found</td></tr>";
        }
        ?>	

    </tbody>	
</table>



<script>
    jQuery(document).ready(function () {

        ComponentsPickers.init();
    });

    jQuery(document).ready(function () {
        TableManaged.init();
    });
    $(document).ready(function () {
        var table = $('#sample_2').dataTable();
        // Perform a filter
        table.fnFilter('');
        // Remove all filtering
        //table.fnFilterClear();

    });
</script>

<!-- END JAVA SCRIPTS -->
<?php
if ($_POST["actionType"] == "excel") {
    if ($row != 0) {
        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=SP_Summary_Report.xls");
    }
}
?>
 