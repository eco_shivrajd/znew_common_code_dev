<!-- BEGIN HEADER -->
<?php 
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include "../includes/grid_header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);
if($_SESSION[SESSION_PREFIX.'user_type']=="Distributor") {
	header("location:../logout.php");
} 
?>
    <!-- END HEADER -->
	<style>
.tree {
    min-height:20px;
    padding:19px;
    margin-bottom:20px;
    background-color:#fbfbfb;
    border:1px solid #999;
    -webkit-border-radius:4px;
    -moz-border-radius:4px;
    border-radius:4px;
    -webkit-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    -moz-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05)
}
.tree li {
    list-style-type:none;
    margin:0;
    padding:10px 5px 0 5px;
    position:relative
}
.tree li::before, .tree li::after {
    content:'';
    left:-20px;
    position:absolute;
    right:auto
}
.tree li::before {
    border-left:1px solid #999;
    bottom:50px;
    height:100%;
    top:0;
    width:1px
}
.tree li::after {
    border-top:1px solid #999;
    height:20px;
    top:25px;
    width:25px
}
.tree li span {
    -moz-border-radius:5px;
    -webkit-border-radius:5px;
    border:1px solid #999;
    border-radius:5px;
    display:inline-block;
    padding:3px 8px;
    text-decoration:none
}
.tree li.parent_li>span {
    cursor:pointer
}
.tree>ul>li::before, .tree>ul>li::after {
    border:0
}
.tree li:last-child::before {
    height:30px
}
.tree li.parent_li>span:hover, .tree li.parent_li>span:hover+ul li span {
    background:#eee;
    border:1px solid #94a0b4;
    color:#000
}
</style>

</script>

<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" />-->
    <body class="page-header-fixed page-quick-sidebar-over-content ">
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Stockist";
	include "../includes/sidebar.php"
	?>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <div class="page-content">
                        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

                        <!-- /.modal -->

                        <h3 class="page-title">
			Tree View
			</h3>

                        <!-- END PAGE HEADER-->
                        <!-- BEGIN PAGE CONTENT-->
                        <div class="row">
							
							<div class="col-md-12">
								<div class="portlet box blue-steel">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-bar-chart-o"></i>
											User Treeview	
										</div>
									</div>
									<div class="portlet-body">
									<div class="tree well">
								<?php 
				function createTreeView($array, $currentParent, $currLevel = 0, $prevLevel = -1) {

										foreach ($array as $categoryId => $category) {

										if ($currentParent == $category['parent']) {
											if ($currLevel > $prevLevel) echo " <ul> "; 

											if ($currLevel == $prevLevel) echo " </li> ";

											echo '<li><span>'.$category['label'].'</span>';

											if ($currLevel > $prevLevel) { $prevLevel = $currLevel; }

											$currLevel++; 

											createTreeView ($array, $categoryId, $currLevel, $prevLevel);

											$currLevel--;
											}   

										}

										if ($currLevel == $prevLevel) echo " </li>  </ul> ";

										}
										$result = mysqli_query($con,"SELECT id, user_type as name, parent_id as parent  FROM tbl_user_tree ORDER BY name");
											$items = array();
											 while($row = mysqli_fetch_array($result))
												 { $items[$row['id']] = array('id' => $row['id'], 'label' => $row['name'], 'parent' => $row['parent']);
											 }

										echo  createTreeView($items, 0);
										?>
										
										</div>
																	
									
								<?php
									function categoryTree($parent_id = 0, $sub_mark = ''){	
									global $con;
									$sql="SELECT id, user_type as name, parent_id   
															FROM tbl_user_tree where parent_id='".$parent_id."' 
															ORDER BY name ASC ";
										 $result = mysqli_query($con,$sql);
															//var_dump($result);
										 $count = mysqli_num_rows($result);
										if($count > 0){
											while($row = mysqli_fetch_assoc($result)){												
												echo '<option value="'.$row['id'].'">'.$sub_mark.$row['name'].'</option>';
												categoryTree($row['id'], $sub_mark.'|->');
											}
										}
										//die();
									} ?>	
								
										 <div class="form-group">
										<label class="col-md-3">Select Parent:<span class="mandatory">*</span></label>
										<div class="col-md-4">
											<!-- <select name="parent_id" id="mySelect" class="form-control">					 
										<?php categoryTree(); ?>
											</select> -->


											<select class="form-control selectpicker" id="select-country" data-live-search="true">
               <?php categoryTree(); ?>
                </select>
										</div>
									</div><!-- /.form-group -->	
										
									</div>
								</div>
							</div>
						</div>
                        <!-- END PAGE CONTENT-->
                    </div>
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->

                <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php include "../includes/grid_footer.php"?>

          <!-- <script type="text/javascript" charset="utf8" 
src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-treeview/1.2.0/bootstrap-treeview.min.js"></script>-->

            
            <!-- END FOOTER -->
    </body>
    <!-- END BODY -->
	<script>
	/*
	$(document).ready(function(){		
 	var url = 'response_usertree.php';
			 $.ajax({
			 url: url,
			 datatype:"JSON",
			contentType: "application/json",
			error : function(data){console.log("error:" + data)
			},
			
			success: function(data){				
				 $('#treeview').treeview({data: data});				
			}
		 });	 
		 
}); 
	*/

	</script>
	<script>
$(function () {
    $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
    $('.tree li.parent_li > span').on('click', function (e) {
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
            children.hide('fast');
            $(this).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
        } else {
            children.show('fast');
            $(this).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
        }
        e.stopPropagation();
    });
});
//$('#dropdown').val($('.tree well').html());
</script>
<!--salesperson/shops Modal --> 
	

    