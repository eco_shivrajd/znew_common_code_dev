<?php
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
$reportType=$_POST['reportType'];
if($reportType=='Rolewise'){
	$report_type_role = $_POST['report_type_role'];
}
if($reportType=='Typewise'){
	$thePostIdArray = explode(',', $_POST['report_type_usertype']);
	$report_type_usertype=$thePostIdArray[0];
	$report_type_role=$thePostIdArray[1];
}
//echo $report_type_role;echo $reportType;
$row = $orderObj->getAllOrdersSummary_total(); 
$grand_total_sales=0;
foreach($row as $key => $val) {
	$grand_total_sales=$grand_total_sales+$val['Total_Sales'];
}
$report_title = $orderObj->getReportTitle_total($report_type_role);

$colspan = "2";

if($report_type_role == 'SalesPerson'&& $reportType=='Typewise'){
	$colspan = "4";	
}
?>
<? if($_POST["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<? } ?>
<table 
	class="table table-striped table-bordered table-hover table-highlight table-checkable" 
	data-provide="datatable" 
	data-display-rows="10"
	data-info="true"
	data-search="true"
	data-length-change="true"
	data-paginate="true"
	id="sample_2">

<thead>
  <?php
   $report_title1 = (string) $report_title;
   if ($report_title1!="null()") 
   {
  ?>  
 <tr>
	<td colspan="<?=$colspan;?>" align="canter" class="gradeX even" style="text-align:center; font-weight:600;"><h4><b><?=ucfirst($report_title);?></b></h4>
	Total Sale In Rupees:<?php echo fnAmountFormat($grand_total_sales);?></td>              
  </tr>
   <?php
   }
   else {
   	?>
     <tr>
	<td colspan="<?=$colspan;?>" align="canter" class="gradeX even" style="text-align:center; font-weight:600;"><h4><b>User Type not available</h4></td>              
  </tr>
   <?php }
  ?> 
  <tr>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Name</th>	
	<?php if($report_type_role == 'SalesPerson'&& $reportType=='Typewise'){ ?>
		<th data-filterable="false" data-sortable="true" data-direction="desc">Shop's order count</th>
		<th data-filterable="false" data-sortable="true" data-direction="desc">Shop's no order count</th>
	<?php } ?>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Total Sales ₹</th>              
  </tr>
</thead>
<tbody>					
	<?php 
			foreach($row as $key => $val) {
			//echo "<pre>";print_r($val);
			if($report_type_role == 'SalesPerson'){				
				$filter_date = $_POST['selTest'];
				$param = "?id=".$val['id']."&param1=".$filter_date;
				if($filter_date == 3)
				{
					$from_date = strtotime($_POST['frmdate']);
					$to_date = strtotime($_POST['todate']);
					$param.="&param2=".$from_date;
					$param.="&param3=".$to_date;
				}
			}
			?>
				<tr class="odd gradeX">
				<td><?=$val['Name'];?></td>	
				<?php //if(fnAmountFormat($val['Total_Sales']) == '0.00'){$val['shop_order']= 0;}
				if($report_type_role == 'SalesPerson' && $_POST["actionType"]!="excel" && $reportType=='Typewise') { ?>
					<td><?php if($val['shop_order'] != 0){echo "<a href='shop_orders_summary.php".$param."'>".$val['shop_order']."</a>";}else{echo $val['shop_order'];} ?></td>
					<td><?php if($val['shop_no_order'] != 0){echo "<a href='no_order_history.php".$param."'>".$val['shop_no_order']."</a>";}else{echo $val['shop_no_order'];} ?></td>
				<?php }else if($report_type_role == 'SalesPerson' && $_POST["actionType"]=="excel" && $reportType=='Typewise'){ ?>
					<td><?php if($val['shop_order'] != 0){echo $val['shop_order'];}else{echo $val['shop_order'];} ?></td>
					<td><?php if($val['shop_no_order'] != 0){echo $val['shop_no_order'];}else{echo $val['shop_no_order'];} ?></td>
				<?php }?>
				<td align='right'><?php 
					echo fnAmountFormat($val['Total_Sales']); 
				?></td>									
				</tr>			
	<?php	} 
		if($_POST["actionType"]=="excel" &&  $row == 0) {
			echo "<tr><td>No matching records found</td></tr>";
		}
	?>	
			
</tbody>	
</table>
<script>
jQuery(document).ready(function() {    
   
   ComponentsPickers.init();
});

jQuery(document).ready(function() { 
	TableManaged.init();
});
$(document).ready(function() {
      var table = $('#sample_2').dataTable();
      // Perform a filter
      table.fnFilter('');
      // Remove all filtering
      //table.fnFilterClear();
	   
  });
</script>
<!-- END JAVASCRIPTS -->
<?
if($_POST["actionType"]=="excel") {
	if($row != 0){
		header("Content-Type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=Report_summary.xls");
		exit;
	}
} ?>
 