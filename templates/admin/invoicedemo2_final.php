<?php
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_STRICT & ~E_DEPRECATED & ~E_NOTICE);
//common database
include ("../../includes/config_pdf.php");


//include ("templates/includes/common.php");
include "../includes/commonManage.php";	
include "../includes/orderManage.php";
include "../includes/shopManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
$shopObj 	= 	new shopManager($con,$conmain);


$order_id = $_GET['orderid'];


  $order_sql11 ="SELECT order_no,order_to,id FROM `tbl_orders` where order_no='".$order_id."'";

$result11 = mysqli_query($con,$order_sql11);

//echo "dfhd<pre>";print_r($result11);
//exit();

while($row = mysqli_fetch_array($result11))
{
	//echo "<pre>";print_r($row);
	$order_id1 = $row["order_no"];
	$order_to1 = $row["order_to"];
	 $invoice_no_by_id = $row["id"];
}

   $order_sql12 ="SELECT firstname,address,mobile,gst_number_sss FROM `tbl_user` where id =".$order_to1;
$result12 = mysqli_query($con,$order_sql12);
while($row2 = mysqli_fetch_array($result12))
{
	$firstname2 = $row2["firstname"];
	$address2 = $row2["address"];
	$mobile2 = $row2["mobile"];
	$gst_number_sss2 = $row2["gst_number_sss"];
}
//echo $firstname2;		
//exit();

$order_details = $orderObj->getShopOrdersbyorderid($order_id1);

//var_dump($order_details[0]['order_date']);
//echo"<pre>";print_r($order_details);die();
 $shop_details="";
 if(!empty($order_details)&&(count($order_details)>0)){
	 if(!empty($order_details[0]['shop_id'])){
		 $shop_details = $shopObj->getShopDetails($order_details[0]['shop_id']);//ordered_by
	 }else{
		 $shop_details = $shopObj->getSessionUserDetails($order_details[0]['ordered_by']);//$_SESSION[SESSION_PREFIX.'user_id']
	 }	
}
//var_dump($shop_details);
//echo "<pre>";print_r($shop_details);//die();
$colspan3=3;
$colspan2=2;
$html="
<style>
.darkgreen{
	background-color:#364622; color:#fff!important; font-size:24px;font-weight:600;
}
.fentgreen1{
	background-color:#b0b29c;
	color:#4a5036;
	font-size:12px;
}
.fentgreen{
	background-color:#b0b29c;
	color:#4a5036;
}
.font-big{
	font-size:20px;
	font-weight:600;
	color:#364622;
}
.font-big1{
	font-size:14px;
	font-weight:600;
	color:#364622;
}
.table-bordered-popup {
    border: 1px solid #364622;
}
.table-bordered-popup > tbody > tr > td, .table-bordered-popup > tbody > tr > th, .table-bordered-popup > thead > tr > td, .table-bordered-popup > thead > tr > th {
    border: 1px solid #364622;
	color:#4a5036;
}
.blue{
	color:#010057;
}
.blue1{
	color:#574960;
	font-size:16px;
}
.buyer_section{
	color:#574960;
	font-size:14px;
}
.pad-5{
	padding-left:10px;
}
.pad-40{
	padding-left:40px;
}
.np{
	padding-left:0px;
	padding-right:0px;
}
.bg{
	background-image:url(../../assets/global/img/invoice/".COMPANYNM."_logo-watermark.jpg); background-repeat:no-repeat;
	 background-size: 200px 200px;
}
</style>

<div id='contentpdf'>
	<table class='table table-bordered-popup' border='1'>
   <tbody>
      <tr>
         <td colspan='7' width='70%' class='darkgreen' valign='top'><img src=".PORTALLOGOPATH."  style='width:60px;'> &nbsp;".strtoupper(COMPANYNM)."</td>
         <td colspan='".$colspan3."' class='font-big text-center' valign='top'>Tax Invoice</td>
      </tr>
      <tr>
         <td colspan='7' class='fentgreen1'>".ucfirst($firstname2)."<br/>
            Address: <b>".$address2."</b><br/>
            Tel: <b>".$mobile2."</b> ";					
           if(!empty($gst_number_sss2)){$html.="GSTIN :<b>".$gst_number_sss2."</b>"; } 
         $html.="</td>
         <td colspan='3' rowspan='2'>
            <div class='col-md-8 np'>
               Invoice No.:  ".str_pad($invoice_no_by_id,6,'0',STR_PAD_LEFT)."<!-- <?=$order_details[0]['invoice_no']?> -->&nbsp;<span class='blue'></span>
            </div>
            <br/>
            <div class='col-md-8 np'>Order Date: ".$order_details[0]['order_date']."&nbsp;<span class='blue'></span></div>
            <br/>
            <div class='col-md-8 np'>Challan No.: &nbsp;-<span class='blue'>".$order_details[0]['challan_no']."</span></div>
            <br/>
            <div class='col-md-8 np'>Vehicle No.: &nbsp;-<span class='blue'>".$order_details[0]['vehicle_no']."</span></div>
            <br/>
            <!--<div class='col-md-8 np'>Transportation Mode: &nbsp;-<span class='blue'>".$order_details[0]['transport_mode']."</span></div> <br/>-->
            <div class='col-md-8 np'>Delivery Date:".$order_details[0]['delivery_date']."&nbsp;<span class='blue'></div>
            </span><br/>
         </td>
      </tr>
      <tr>
         <td colspan='7' valign='top'>Buyer 
            <span class='buyer_section'><b>".$shop_details['name']."</b><br/></span>
            <span class='buyer_section pad-40'>".$shop_details['address'].",<br/></span>
            <span class='buyer_section pad-40'>".$shop_details['city_name']."<br/></span>
            <span class='buyer_section pad-40'>".$shop_details['state_name']."<br/></span>";
            if(!empty($shop_details['gst_number'])){ $html.="<span class='buyer_section pad-40'>GSTIN NO. ".$shop_details['gst_number']."</span>"; } 
        $html.=" </td>
      </tr>
      <tr class='fentgreen'>
         <th width='5%' class='text-center'>SI No.</th>
         <th class='text-center'>Name of Goods</th>
         <th class='text-center'>HSN Code</th>
		 <th class='text-center'>MFG Date</th>	
         <th width='5%' class='text-center'>CGST</th>
         <th width='5%' class='text-center'>SGST</th>
         <th class='text-center'>Qty</th>
         <th class='text-center'>Rate</th>
         <th class='text-center'>UOM</th>
         <th class='text-center'>Value</th>
      </tr>
      <div class=='bg'>";
            $i = 1;
            $final_qty = 0;
            $final_cost = 0;
            $total_amount = 0;
            $free_product_count=array();
            foreach($order_details as $val){
            	$sr_no.=$i.'<br><br>';
            	$product_name.=$val['productname']."(".$val['product_variant_weight1']." ".$val['unit'].")<br><br>";				
            	$product_cgst.=number_format((float)$val['product_cgst'], 2, '.', '').'<br><br>';
            	$product_sgst.=number_format((float)$val['product_sgst'], 2, '.', '').'<br><br>';
            	$qty.=$val['variantunit'].'<br><br>';
            	$c_o_d = $val['c_o_d'];
            	$cod_percent = $val['cod_percent'];
            	$final_qty = $final_qty + $val['variantunit'];
            	$unit_cost.=number_format((float)$val['product_unit_cost'], 2, '.', '').'<br><br>';
            	$nos.='nos<br><br>';
            	//$total_cost.=($unit_cost*$val['variantunit']).'<br><br>';
            	$total_cost.= $val['unitcost'].'<br><br>';
            	$final_cost = $final_cost + $val['unitcost'];
            	if(!empty($val['free_product_details'])){
            		$free_product_count[$i]=$val['free_product_details'];
            	}
            	 $cod_final_cost = ($cod_percent / 100) * $final_cost;
            		$fcod_final_cost = $final_cost -$cod_final_cost;
            	 
        $html.=" <tr >
            <td class='text-center' valign='top'><span class='blue'>".$i."</span></td>
            <td class='text-left' valign='top'><span class='blue'>".$val['productname']."(".$val['product_variant_weight1']." ".$val['unit'].")</span></td>
            <td class='text-left' valign='top'><span class='blue'>".$val['producthsn']."</span></td>
			<td class='text-left' valign='top'><span class='blue'>";
			 if($val['pro_mnf_date']!='0000-00-00'){$html.=date('d-m-Y', strtotime($val['pro_mnf_date']));}else{$html.="NA";}
			$html.=" </span></td>
            <td class='text-right' valign='top'><span class='blue'>".number_format((float)$val['product_cgst'], 2, '.', '')."</span></td>
            <td class='text-right' valign='top'><span class='blue'>". number_format((float)$val['product_sgst'], 2, '.', '')."</span></td>
            <td class='text-right' valign='top'><span class='blue'>".$val['variantunit']."</span></td>
            <td class='text-right' valign='top'><span class='blue'>".number_format((float)$val['product_unit_cost'], 2, '.', '')."</span></td>
            <td class='text-center' valign='top'><span class='blue'>".'nos'."</span></td>
            <td class='text-right' align='right' valign='top'><span class='blue'>".$val['unitcost']."</span></td>
         </tr>";
         $i++; }
            //echo '<pre>';print_r($free_product_count);
            foreach($free_product_count as $valfree){
            	$arr=explode(',',$valfree);
            	$sr_no.=$i.'<br><br>';
            	$product_name='';
            	$productname=	$orderObj->getProductname($arr[4]);
            	$product_name.="<span style='float: left'>
            		<img src='../../assets/global/img/free-icon.png' title='Free Product'>
            	</span>";
            	foreach($productname as $keyprod=>$valueprod){								
            			$product_name.=$valueprod."(".$arr[7].")";
            		}
            	//$product_name.=$productname.'<br><br>';
            	
            	$hsn=$val['producthsn'].'';
            	$product_cgst=number_format((float)$val['product_cgst'], 2, '.', '').'';
            	$product_sgst=number_format((float)$val['product_sgst'], 2, '.', '').'';
            	$qty=$arr[8].'';
            	$c_o_d = 0;
            	$cod_percent = 0;
            	$final_qty = $final_qty + $arr[8];
            	$unit_cost='0';
            	$nos.='nos';
            	//$total_cost.=($unit_cost*$val['variantunit']).'<br><br>';
            	$total_cost= '0';
            	$final_cost = $final_cost + 0;						
            	$cod_final_cost = ($cod_percent / 100) * $final_cost;
            	$fcod_final_cost = $final_cost -$cod_final_cost;
        $html.=" <tr>
            <td class='text-center' valign='top'><span class='blue'>".$i."</span></td>
            <td class='text-left' valign='top'><span class='blue'>".$product_name."</span></td>
            <td class='text-left' valign='top'><span class='blue'>".$hsn."</span></td>
			<td class='text-left' valign='top'><span class='blue'>NA</span></td>
            <td class='text-right' valign='top'><span class='blue'>".$product_cgst."</span></td>
            <td class='text-right' valign='top'><span class='blue'>".$product_sgst."</span></td>
            <td class='text-right' valign='top'><span class='blue'>".$qty."</span></td>
            <td class='text-right' valign='top'><span class='blue'>".$unit_cost."</span></td>
            <td class='text-center' valign='top'><span class='blue'>".'nos'."</span></td>
            <td class='text-right' align='right' valign='top'><span class='blue'>".$total_cost."</span></td>
         </tr>";
          	$i++; }
      $html.="</div>
      <tr>
         <td></td>
         <td class='text-right'><b>Total</b></td>
         <td class='fentgreen'></td>
         <td class='fentgreen'></td>
         <td class='fentgreen'></td>
         <td class='fentgreen'></td>
         <td class='fentgreen text-right'>".$final_qty."</td>
         <td class='fentgreen'></td>
         <td class='fentgreen'></td>
         <td class='fentgreen' align='right'>".number_format((float)$final_cost, 2, '.', '')."</td>
      </tr>
      <tr>
         <td colspan='2' width='30%' valign='top'>
            <u>Declaration:</u>
            <br/>";
			if($order_details[0]['signature_image']!=''){ 
				$html.="<img src='../../uploads/cod/".$order_details[0]['challan_no']."/".$order_details[0]['signature_image']."' 
				height='70' width='200'>";
			} 	
            $html.="<br/>Buyer Signature<br/>";
			if($order_details[0]['sp_signature_image']!=''){ 
				$html.="<img src='../../uploads/cod/sp_signature_".COMPID."/".$order_details[0]['sp_signature_image']."' 
				height='70' width='200'>";
			} 
			$html.="<br/>SalesPerson Signature
         </td>
         <td >
            <div class='text-center'  valign='top'><b><u>BANK DETAILS</u></b>
            </div>";
            if ($shop_details['bank_acc_no']=='') { 
            $html.="Sorry..No Bank Account Details.";
           } else {  $html.="BANK NAME:".$shop_details['bank_name']."<br/> BRANCH :
            ".$shop_details['bank_b_name']."
            <br/> CC A/C NO.:
            ".$shop_details['bank_acc_no']."
            <br/> IFSC CODE:
            ".$shop_details['bank_ifsc']."
         </td>";
          } 
          $html.="<td class='fentgreen font-big1' colspan='4' width='20%' valign='top'>For <b>".ucfirst($firstname2)."</b>
            <br/>
            <br/>
            <br/> Authorised Signature
         </td>
         <td colspan='".$colspan2."'  valign='top'>
            <span style='display:inline-block; height:40px;' class='blue'>Rounding Off</span>
            <br/>
            <span style='display:inline-block; height:40px;' class='blue'><b>Grand Total</b> </span>
            <br/>
            <span style='display:inline-block; height:40px;' class='blue'>Opening Balance</span>
            <br/>
            <span style='display:inline-block; height:40px;' class='blue'>Closing Balance </span>
         </td>
         <td  class='text-right' valign='top'>
            <span style='display:inline-block; height:40px;' class='blue'>".$cod_percent." % </span>
            <br/>
            <span style='display:inline-block; height:40px;' class='blue'><b> ".number_format((float)$fcod_final_cost, 2, '.', '')."</b></span>
            <br/>
            <span style='display:inline-block; height:40px;' class='blue'></span>
            <br/>
            <span style='display:inline-block; height:40px;' class='blue'></span>
         </td>
      </tr>
      <tr>
      </tr>
   </tbody>
</table>
</div>";
$path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;

require_once  $path.'/vendor/autoload.php';				
$mpdf = new \Mpdf\Mpdf([
	'mode' => 'c',
	'margin_left' => 12,
	'margin_right' => 12,
	'margin_top' => 12,
	'margin_bottom' => 12,
	'margin_header' => 1,
	'margin_footer' => 1
]);
//echo "gfadsfafd";
$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list

// Load a stylesheet
$stylesheet = file_get_contents('assets/mpdfstyletables.css');
//$stylesheet = file_get_contents('../../assets/global/plugins/bootstrap/css/bootstrap.min.css');
$mpdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html);
$filename="uploadpdf/".$order_id1.".pdf";
$mpdf->Output($filename, 'F');//in this dynamic file name should be there and seperate folder for upload pdf
$mpdf->Output();
