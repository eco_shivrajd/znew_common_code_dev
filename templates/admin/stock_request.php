<?php
//
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include "../includes/grid_header.php";
//include "../includes/commonManage.php";
include "../includes/campaignManage.php";
include "../includes/testManage.php";
include "../includes/userConfigManage.php";
//$commonObj = new commonManage($con, $conmain);
$campaignObj = new campaignManager($con, $conmain);
$testObj 	= 	new testManager($con,$conmain);
$userconfObj 	= 	new userConfigManager($con,$conmain);

$user_type = $_SESSION[SESSION_PREFIX . 'user_type'];
$user_role = $_SESSION[SESSION_PREFIX . 'user_role'];
$user_id = $_SESSION[SESSION_PREFIX . 'user_id'];
$prod_prodvar_array=array();
$order_id_array=array();
if(isset($_POST['select_all_send']))
{
	foreach($_POST['select_all_send'] as $key=>$order_id){
		$new_order_id=str_replace('ordersubsend_','',$order_id);
		$order_id_array[]=$new_order_id;
		 $order_sql="SELECT o.id,od.product_quantity,pv.productid as productid,od.product_variant_id as pvid 
		FROM `tbl_orders` o 
		LEFT JOIN tbl_order_details AS od ON od.order_id = o.id
		LEFT JOIN tbl_product_variant pv on od.product_variant_id = pv.id
		where o.id='".$new_order_id."'";
		
        $result = mysqli_query($con, $order_sql);
        $row_count = mysqli_num_rows($result);
        while ($row = mysqli_fetch_array($result)) 
        {          
            $prodcode = $row['productid'] . '_' . $row['pvid'];
			if (array_key_exists($prodcode,$prod_prodvar_array)){
				$prod_prodvar_array[$prodcode]=$prod_prodvar_array[$prodcode]+$row['product_quantity'];
			} else {
				$prod_prodvar_array[$prodcode]=$row['product_quantity'];
			}
		}   
	}
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php 
        $activeMainMenu = "Stock";
        $activeMenu = "stock_requests_list";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

                <!-- /.modal -->

                <h3 class="page-title">
                    Request Stock
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                       <li>
                            <i class="fa fa-home"></i>
                            <a href="stock_requests_list.php">Stock Requests</a>
                             <i class="fa fa-angle-right"></i>
                        </li>
                            <li>
                            <a href="#">Request Stock</a>
                            </li>
                    </ul>

                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">Stock Adding Cart </div>	
                                <a id="btnEmpty" class="btn btn-sm btn-default pull-right mt5" onClick="cartActionEmpty();">Empty Cart</a>
                                <div class="clearfix"></div>
                            </div>						
                            <div class="portlet-body">	
								<div id="empty_cart_div_id" align="center">Request Stock cart is empty.</div>
                               <?php
									$margin_percentage=0;
									$sql_all_get="SELECT `tbl_user`.`id`,`tbl_user`.`user_type`, `tbl_user`.`user_role`, 
									 `tbl_user`.`marginType`,`tbl_user`.`parent_ids`, `userid_margin` ,`userrole_margin`,`usertype_margin`
									FROM `tbl_user` 
									left join tbl_userrole on `tbl_user`.`user_role`=tbl_userrole.user_role
									left join tbl_user_tree on `tbl_user`.`user_type`=tbl_user_tree.user_type
									WHERE `tbl_user`.`isdeleted`!='1' 
									AND ( `tbl_user`.`user_role`='Admin' OR `tbl_user`.`user_role`='Superstockist' 
									OR `tbl_user`.`user_role`='Distributor' ) ";
									$get_all_user_result = mysqli_query($con, $sql_all_get);
									while($row_all_users = mysqli_fetch_array($get_all_user_result)){
										$userids=$row_all_users['id'];
										$margin[0]=$row_all_users['userrole_margin'];
										$margin[1]=$row_all_users['usertype_margin'];
										$margin[2]=$row_all_users['userid_margin'];
										$marginType = $row_all_users['marginType'];
										$margin_percentage1[$userids] = $margin[$marginType];
										$parent_ids1[$userids] = $row_all_users['parent_ids'];
									}
                                    $margin_percentage=$margin_percentage1[$user_id];
                                    $parent_ids=$parent_ids1[$user_id];
                                ?>                
                                    <table class="table table-striped table-bordered table-hover" id="myTable" style="display:none">
                                        <thead>
                                            <tr>
												<th> Category</th>
                                                <th>Product Name </th>
                                                <th> Product Price</th>
                                                <th>Price With </br>Margin (<span id="margin_per_span1"><?php echo $margin_percentage;?></span> %) And Tax </th>
                                                <th>Product Variant</th>                                              
                                                <th>Quantity</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody >
                                        </tbody>
                                    </table>
                                 <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" >

                                     <div class="form-group" id="place_order_div" style="display:none">
                                    <?php  
                                    $rand = rand(10000,99999);
                                    $unique_order_number = '00011'."".$rand.$user_id.''. round(microtime(true));     ?>
									   <p style="text-align: center;"><b>ORDER ID : <?=$unique_order_number;?></b></p>
									   <p style="text-align: center;"><b>Total Items : <span id="total_items"></span></b></p>
									   <p style="text-align: center;"><b>Total Cost : <span id="order_total_cost"></span></b></p>
									   <p style="text-align: center;"><b>Total Cost After GST : <span id="order_total_cost_gst"></span></b></p>
                                       <input type="hidden" name="unique_order_number" value="<?=$unique_order_number;?>">
                                        <div class="col-md-4 col-md-offset-5">
                                            <button type="button" name="place_order_btnsubmit" id="place_order_btnsubmit" class="btn btn-primary" onclick="placeOrderStockist();">Request Stock</button>
                                        </div>
                                    </div><!-- /.form-group -->
                                 </form>
                            </div>
                            <!-- END PAGE CONTENT-->
                        </div>
                        <div class="clearfix"></div>   
                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">Products Listing</div>
                                <a id="btnEmpty" class="btn btn-sm btn-default pull-right mt5" 
								onclick="cartActionAdd();" >Add to Cart</a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <form id="frmCart" class="form-horizontal"  enctype="multipart/form-data">
                                    <table class="table table-striped table-bordered table-hover" id="sample_7">
                                        <thead>
                                            <tr>					
                                                <th> Category </th>
                                                <th> Product Name</th>
                                                <th> Product Price </th>
                                                <th> Price With </br>Margin (<span id="margin_per_span2"><?php echo $margin_percentage;?></span> %) And Tax   </th>
                                                <th> Product Variant </th>
                                                <th> Quantity</th>
                                            </tr>
                                        </thead>
											<tbody>
							<?php
							$sql = "SELECT a.categorynm,b.productname,b.id,pv.producthsn,pv.price as price,pv.variant_1,pv.variant_2,pv.variant1_unit_id,pv.variant2_unit_id,pv.id as pvid, "
									. " pv.productimage,pv.cgst,pv.sgst,b.added_by_userid,b.added_by_usertype, a.id AS cat_id  "
									. " FROM tbl_category a,tbl_product b "
									. " left join tbl_product_variant pv on b.id = pv.productid "
									. " where a.id=b.catid AND b.isdeleted != 1 "
									. " and (find_in_set(b.added_by_userid,'".$parent_ids."') <> 0)";
							$result = mysqli_query($con, $sql);
							$row_count = mysqli_num_rows($result);
							while ($row = mysqli_fetch_array($result)) 
							{ 
								$prodcode = $row['id'] . '_' . $row['pvid'];
								$price = $row['price'];
								$cgst = $row['cgst'];
								$sgst = $row['sgst'];
								
								//new code for price update onchange 
								$prod_price_update_array[$prodcode]['price']=$price;
								$prod_price_update_array[$prodcode]['cgst']=$cgst;
								$prod_price_update_array[$prodcode]['sgst']=$sgst;
								//new code for price update onchange 
								
								$price_with_margin = $price - (($margin_percentage/100)*$price);
								$tax_cgst = $price_with_margin*$cgst/100;
								$tax_sgst = $price_with_margin*$sgst/100;
								$price_with_margin_cgst_sgst = $price_with_margin+$tax_cgst+$tax_sgst;
								//$price_with_margin_cgst = $price_with_margin + ($price*$cgst/100);
								//$price_with_margin_cgst_sgst = $price_with_margin_cgst + ($price*$sgst/100);
								?>

													<tr class="odd gradeX" id="<?php echo $prodcode; ?>">
														<td id="num1<?php echo $prodcode; ?>">
															<?php echo fnStringToHTML($row['categorynm']); ?>
														</td>
														<td id="num2<?php echo $prodcode; ?>">
															<?php echo fnStringToHTML($row['productname']); ?>
														</td>
														<td align="right" id="num3<?php echo $prodcode; ?>">
															<?php echo fnStringToHTML($row['price']); ?>
														</td>
														<td align="right" id="num4<?php echo $prodcode; ?>">
															<?php echo fnStringToHTML(number_format((float)$price_with_margin_cgst_sgst, 2, '.', '')); ?>					
														</td>										
														<td id="num5<?php echo $prodcode; ?>">
													<?php
															$sqlvarunit3 = "SELECT unitname FROM tbl_units  where id='" . $row['variant1_unit_id'] . "'";
															$resultvarunit3 = mysqli_query($con, $sqlvarunit3);
															while ($rowvarunit3 = mysqli_fetch_array($resultvarunit3)) {
															   $unitname3 = $rowvarunit3['unitname'];
															}
															$sqlvarunit4 = "SELECT unitname FROM tbl_units  where id='" . $row['variant2_unit_id'] . "'";
															$resultvarunit4 = mysqli_query($con, $sqlvarunit4);
															while ($rowvarunit4 = mysqli_fetch_array($resultvarunit4)){
															   $unitname4 = $rowvarunit4['unitname'];
															}
															if(!empty($row['variant_1'])){ echo $row['variant_1']."-".$unitname3." ";}
															if(!empty($row['variant_2'])){ echo $row['variant_2']."-".$unitname4; }
															echo "<br>";
													?>

														</td>
														<td id="num7<?php echo $prodcode; ?>" align="right">
															<input type="number" min="0" max="100000" oninput="myFunction(this)"  
															name="prodqnty" id="qty_<?php echo $prodcode; ?>" 
																   value="0" size="2"  style="width: 65px; margin: 5px 5px 5px 5px;text-align: right;"> 
														</td>
													</tr>
												<?php } ?>							
											</tbody>
										</table>
                                </form>
                            </div>
                        </div> 

                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <?php include "../includes/grid_footer.php" ?>
    <!-- END FOOTER -->

<script>
var total_price_all = 0.00;
var total_price_all_gst=0.00;
function myFunction(varl) {
    if (varl.value != "") {   
        var your_string=varl.value.toString();  
        var lengthofvalue=varl.value.toString().length;
        if (lengthofvalue > 5) { 
            varl.value = varl.value.substring(0, 5);
        }
        if (your_string.indexOf('.') > -1){
            varl.value = varl.value.substring(0, lengthofvalue-1);
        }       
    }
}

$( document ).ready(function() {	
	var myorderqnty=<?php echo json_encode($prod_prodvar_array); ?>;	
	if(!jQuery.isEmptyObject(myorderqnty)){
		$.each(myorderqnty, function( index, value ) {			
			$('#qty_'+index+'').val(value);
		});
		alert("All selected orders product quantity are added in the following table respectively except my own product.");
	}
   var table = $('#sample_7').dataTable({destroy: true,stateSave: false,paging:   false});
	 $('input[type=search]').on("input", function() {
		table.fnFilter('');
	});
	$("#place_order_btnsubmit").on('click', function (event) {  
           event.preventDefault();
           var el = $(this);
           el.prop('disabled', true);
           setTimeout(function(){el.prop('disabled', false); }, 3000);
     });
});
	
function placeOrderStockist(){	
	var myorderqnty=<?php echo json_encode($prod_prodvar_array); ?>;	
	var order_id_array=<?php echo json_encode($order_id_array); ?>;	
	var unique_order_number=<?php echo json_encode($unique_order_number); ?>; 
	//alert(unique_order_number);
	//console.log(typeof(order_id_array));
	//alert(order_id_array);console.log(order_id_array);debugger;
	
	var orderarray=[];
	$('#myTable tbody tr').each(function () {  
		var void1 = [];
		var newenergy = this.id.replace('mytabletr', '');
		var tempstrip=$('#myqty_' + newenergy + '').html();                            
		void1.push(tempstrip.replace(/ +$/, "")); 
		void1.push(newenergy); 
		 orderarray.push(void1); 
	});
  
	if (orderarray.length > 0) {
		var url = 'updateOrderFromStockist.php';               
		$.ajax({
			type: "POST",
			url: url,
			data: {data123 : orderarray,order_id_array:order_id_array,unique_order_number:unique_order_number},
			error: function (data) {
				console.log("error:" + data)
			},
			success: function (data) {
				console.log(data);
				if (data > 0) {
					alert("Stock Requested Successfully.");
					window.location.href = "my_stock_requests.php";
					//location.reload();
				} else {
					alert("Not updated.");
				}
			}
		});
	} else {
		alert("Please add some product.");
		return false;
	}
}
function cartActionEmpty(){
	$("#myTable tbody").empty();
	$('#place_order_div').hide();
	$('#myTable').hide();
	$('#empty_cart_div_id').show();
	total_price_all = 0.00;
	total_price_all_gst=0.00;
}
function cartActionEmptyItem(trid){
	console.log(trid);
	
	var nthtotal_cost=parseFloat($("#mytabletr"+trid+" td:nth-child(4)").text());
	var nthtotal_cost_gst=parseFloat($("#mytabletr"+trid+" td:nth-child(5)").text());
	//debugger;
	var total_qnty=parseFloat($("#myqty_"+trid+"").text());
	console.log(total_qnty);	
	//debugger;
	var total_cost = parseFloat($('#order_total_cost').text()) - parseFloat(nthtotal_cost*total_qnty);
	var total_cost_gst = parseFloat($('#order_total_cost_gst').text()) - parseFloat(nthtotal_cost_gst*total_qnty);
	
	$('#order_total_cost').html(parseFloat(total_cost).toFixed(2));
	$('#order_total_cost_gst').html(parseFloat(total_cost_gst).toFixed(2));
	total_price_all=parseFloat(total_cost).toFixed(2);
	total_price_all_gst=parseFloat(total_cost_gst).toFixed(2);
	var rowCount = $('#myTable tbody tr').length;			
	var total_items=rowCount-1;
	$("#mytabletr"+trid+"").remove();
	$('#total_items').html("<b>"+total_items+"</b>");
	if(rowCount==1){
		$('#place_order_div').hide();
		$('#myTable').hide();
		 $('#empty_cart_div_id').show();
	}			
}
		
function cartActionAdd() {
	var void1 = [];
	var void2 = [];
	$('input[name="prodqnty"]').each(function () {
		if (this.value > 0) {
			void1.push(this.value);
			void2.push(this.id);
		}
	});
	var energy = void1.join();           
	if (energy != '') {       
		Object.keys(void1).forEach(function (key) { 
		//alert('dfgdfgdf');   
			
			var newenergy = void2[key].replace('qty_', '#');
			var newenergy1 = newenergy.replace('#', '');
			
			var pro_qty = $('#qty_' + newenergy1 + '').val();
			var pro_price = parseFloat($('#num3' + newenergy1 + '').html());
			var pro_price_gst = parseFloat($('#num4' + newenergy1 + '').html());
			var total_price = pro_qty*pro_price;
			var total_price_gst = pro_qty*pro_price_gst;
			total_price_all=parseFloat(total_price_all)+parseFloat(total_price);
			total_price_all_gst=parseFloat(total_price_all_gst)+parseFloat(total_price_gst);
			
			if ($('#mytabletr' + newenergy1 + '').length) {						
				var sumvalue = +$('#myqty_' + newenergy1 + '').html() + +void1[key];
				console.log(sumvalue);						
				$('#myqty_' + newenergy1 + '').html('' + sumvalue + '');
				$('#order_total_cost').html("<b>"+parseFloat(total_price_all).toFixed(2)+"</b>");
				$('#order_total_cost_gst').html("<b>"+parseFloat(total_price_all_gst).toFixed(2)+"</b>");
				//$('#order_from_user').html("<b>"+$('select[name="u_id"] option:selected').text()+"</b>");
				
			} else {   
				$("#myTable").append("<tr class='odd gradeX' id='mytabletr" + newenergy1 + "'><td>" + 
				$('#num1' + newenergy1 + '').html() + "</td><td>" + 
				$('#num2' + newenergy1 + '').html() + "</td><td>" +
				$('#num3' + newenergy1 + '').html() + "</td><td align='right'>" +
				$('#num4' + newenergy1 + '').html() + "</td><td>" +
				$('#num5' + newenergy1 + '').html() + "</td><td>" +
				"<button type='button' class='btn btn-primary' onClick='cartActionEmptyItem(\"" + newenergy1 + "\");'>Remove "+
				"<span id='myqty_" + newenergy1 + "' class='badge'>"+$('#qty_' + newenergy1 + '').val()+"</span></button></td>"+
				"</tr>");
				$('#order_total_cost').html("<b>"+parseFloat(total_price_all).toFixed(2)+"</b>");
				$('#order_total_cost_gst').html("<b>"+parseFloat(total_price_all_gst).toFixed(2)+"</b>");
				//$('#order_from_user').html("<b>"+$('select[name="u_id"] option:selected').text()+"</b>");
			}
		});



		if ($('#myTable tbody tr').length>0) { 
			var total_items =$('#myTable tbody tr').length;
			$('#myTable').show();
			$('#place_order_div').show();
			$('#dropdownUserType').show();
			$('#empty_cart_div_id').hide();
				
			$('#total_items').html("<b>"+total_items+"</b>");
		}
		$('input[name="prodqnty"]').each(function () {
			if (this.value > 0) {
				$('input[name="prodqnty"]').val('0');
			}
		});
	} else  {
		alert("Please add some quantity.");
	}
}
</script>
</body>
<!-- END BODY -->
</html>