<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/shopManage.php";
include "../includes/userManage.php";
$shopObj 	= 	new shopManager($con,$conmain);
$userObj 	= 	new userManager($con,$conmain);
$id=$_GET['id']; 		 
$utype=$_GET['utype']; 
if(isset($_POST['deleteuserbtn']))
{
	$field_type	=	$_POST['fieldtype'];
	$sssuid	=	$_POST['sssuid'];	
	
	/* Local user section */
	
	switch ($field_type) {		
		case "Shops":
				$userid_local = $userObj->deleteShopbyid($sssuid);
			echo '<script>alert("Shop deleted successfully.");location.href="shops.php";</script>';
			break;
		case "Leads":
				$userid_local = $userObj->deleteLeadbyid($sssuid);
			echo '<script>alert("Lead deleted successfully.");location.href="leads.php";</script>';
			break;
		case "Taluka":				
				$userid_local = $userObj->deleteAreabyid($sssuid);
			echo '<script>alert("Taluka deleted successfully.");location.href="suburb.php";</script>';
			break;
		case "Subarea":
			$userid_local = $userObj->deleteSubareabyid($sssuid);
			echo '<script>alert("Subarea deleted successfully.");location.href="subarea.php";</script>';
			break;
		default:
	}
} 
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = $utype;//'Stockist',''Superstockist
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->			
			<h3 class="page-title">Delete  <?php if($activeMenu=='SalesPerson'){echo 'Sales Person';}else{echo $activeMenu;} ?></h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Delete  <?php if($activeMenu=='SalesPerson'){echo 'Sales Person';}else{echo $activeMenu;} ?></a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Delete  <?php if($activeMenu=='SalesPerson'){echo 'Sales Person';}else{echo $activeMenu;} ?>
							</div>
                            
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
						
							<form class="form-horizontal" id="frmsearch"  method="post" action="manageuser.php" data-parsley-validate="">
							 
							 <input type="hidden" name="fieldtype" id="fieldtype" value="<?=$utype;?>">
							  <input type="hidden" name="sssuid" id="sssuid" value="<?=$id;?>">
							
							 <?php if($utype=='Superstockist'||$utype=='Distributor'){?>
								 <div class="form-group">
									<label class="col-md-3">Select Replacement:</label>
									<div class="col-md-4">
									<select name="ssORstockist"  id="ssORstockist" class="form-control" 
										data-parsley-trigger="change"				
										data-parsley-required="#true" 
										data-parsley-required-message="Please select user">
									<option value='' >-Select-</option>
									<?php if($utype=='Superstockist'){ ?>
											<?php
											$user_type="Superstockist";
											$result1 = $userObj->getLocalUserDetailsByUserType($user_type);							
											while($row = mysqli_fetch_array($result1))
											{
												$cat_id=$row['id'];
												if($cat_id==$id){
													echo "";
												}else{
													echo "<option value='$cat_id'>" . fnStringToHTML($row['firstname']) . "</option>";
												}
											}
										 } 
								 if($utype=='Stockist'){ 
										$user_type	= "Distributor";
										$external_id = '';	
										$sql="SELECT external_id FROM `tbl_user` where  id='$id'";
										$result1 = mysqli_query($con,$sql);
										
										$row_count = mysqli_num_rows($result1);
										if($row_count>0){
											$row = mysqli_fetch_assoc($result1);
											$external_id = $row['external_id'];	
										}
										
										
										$result1 = $userObj->getLocalUserDetailsByUserType($user_type,$external_id);																													
										while($row = mysqli_fetch_array($result1))
										{
											$assign_id=$row['id'];
											if($assign_id==$id){
												echo "";
											}else{
												echo "<option value='$assign_id'>" . fnStringToHTML($row['firstname']) . "</option>";
											}
										}
										} ?>
										</select>
										
									</div>
								</div><!-- /.form-group -->	
							<?php } ?>
								
							
							 <button type="submit" class="btn btn-success" name="deleteuserbtn" 
								id="deleteuserbtn" Onclick="return ConfirmDelete()" data-toggle="modal">Confirm Delete </button>		
						
						</form>	
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
</div>
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
		
<script type="text/javascript" src="../../assets/global/scripts/jquery.loader.js"></script>
<script>
function ConfirmDelete() {
	//alert($('#fieldtype').val());
	
	if($('#fieldtype').val()=='Taluka')
		return confirm("Are you sure you want to delete this Taluka and subarea under this Taluka?");
	else if($('#fieldtype').val()=='Subarea')
		return confirm("Are you sure you want to delete this  subarea?");
	else if($('#fieldtype').val()=='Superstockist')
		return confirm("Are you sure you want to delete this Superstockist?");
	else if($('#fieldtype').val()=='Distributor')
		return confirm("Are you sure you want to delete this Distributor?");
	//else if($('#fieldtype').val()=='UserType')
		//return confirm("Are you sure you want to delete this User Type?");
	else if($('#fieldtype').val()=='SalesPerson')
		return confirm("Are you sure you want to delete this Sales Person?");
	else if($('#fieldtype').val()=='Leads')
		return confirm("Are you sure you want to delete this Lead?");
	else{
		return confirm("Are you sure you want to delete this Shops?");
	}
}
</script>
</body>
<!-- END BODY -->
</html>