<?php 
include ("../../includes/config.php");
include "../includes/userConfigManage.php";
$userconfObj 	= 	new userConfigManager($con,$conmain);

$user_details_id = $_POST['user_details_id'];
$user_role = $_POST['user_role'];
$user_details = $userconfObj->getAllUsersViewAllDetails($user_details_id);
//print"<pre>";print_r($user_details);
?>
<div class="modal-header">
<!--<button type="button" name="btnPrint" id="btnPrint" 
onclick="takeprint_userdetails('<?=SITEURL;?>')" class="btn btn-primary" >Take A print</button>-->
<font style="font-size: 17px;"><b>User Details</b></font> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel"></h4>	   
</div>
<div class="modal-body" style="padding-bottom: 5px !important;" id="divOrderPrintArea">
<div class="row">
<div class="col-md-12">   
	<div class="portlet box blue-steel">
		<div class="portlet-title ">
			<div class="caption printHeading">
				<?=$user_role;?>
			</div>                          
		</div>
		<div class="portlet-body">
			<table class="table table-striped table-bordered table-hover" id="sample_1" width="100%">
			<tr>
				<td>Name</td>
				<td><?=$user_details['firstname'];?></td>				
			</tr>

            <tr>
				<td>Parent Names</td>
				<td><table class="table table-striped table-bordered table-hover" width="100%">
					<?php 
						$parent_usertypes=explode(',',$user_details['parent_usertypes']);
					    $parent_names=explode(',',$user_details['parent_names']);
							//$counter_s=count($parent_usertypes);
						for($i=0;$i<count($parent_usertypes);$i++){ ?>
						<tr>
							<td><?php echo $parent_usertypes[$i];?></td>						
							<td><?php echo $parent_names[$i];?></td>	
						</tr>
						<?php } ?>		
					</table>
				
				</td>					
			</tr> 

			<tr>
				<td>Email</td>
				<td><?=$user_details['email'];?></td>				
			</tr>
		    <tr>
				<td>Mobile No.</td>
				<td><?=$user_details['mobile'];?></td>				
			</tr>
			<tr>
				<td>Subarea</td>
				<td><?=$user_details['subarea_name'];?></td>				
			</tr>
			<tr>
				<td>Taluka</td>
				<td>
					<?php echo $user_details['suburbnm'];?>
				</td>				
			</tr>
			
			<tr>
				<td>City</td>
				<td><?=$user_details['cityname'];?></td>				
			</tr>
			<tr>
				<td>State</td>
				<td><?=$user_details['statename'];?></td>				
			</tr>
			<tr>
				<td>Office Phone No</td>
				<td><?=$user_details['office_phone_no'];?></td>				
			</tr>
			<tr>
				<td><b>Other Details</b></td>
				<td></td>				
			</tr>				
			
			<tr>
				<td>Account Name</td>
				<td><?=$user_details['accname'];?></td>				
			</tr>
			<tr>
				<td>Account No</td>
				<td><?=$user_details['accno'];?></td>				
			</tr>
		    <tr>
				<td>Branch</td>
				<td><?=$user_details['accbrnm'];?></td>				
			</tr>
			<tr>
				<td>Bank Name</td>
				<td><?=$user_details['bank_name'];?></td>				
			</tr>
			<tr>
				<td>IFSC Code</td>
				<td><?=$user_details['accifsc'];?></td>				
			</tr>
			<tr>
				<td>GST Number</td>
				<td><?=$user_details['gst_number_sss'];?></td>				
			</tr>

			</table>
</div>
</div>
</div>
</div>
</div>
<script>
function takeprint_userdetails() {
	var divContents = $("#divOrderPrintArea").html(); 
	var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);          
		
	if (isIE == true) {     
			var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write('<html><head>');
		printWindow.document.write('<style type="text/css">a{text-decoration: none; color:#333333;} table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
		printWindow.document.write('</head><body >');
		printWindow.document.write(divContents);
		printWindow.document.write('</body></html>');	
		
		printWindow.document.write(divContents);
		printWindow.focus();
		printWindow.document.execCommand("print", false, null);
	} else {
		$('<iframe>', {
			name: 'myiframe',
			class: 'printFrame'
		}).appendTo('body').contents().find('body').html(divContents);
		window.frames['myiframe'].focus();
		window.frames['myiframe'].print();
		setTimeout(
				function ()
				{
					$(".printFrame").remove();
				}, 1000);
	}
}

</script>