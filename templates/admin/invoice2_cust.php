<?php 
include ("../../includes/config.php");
include ("../includes/common.php");
include "../includes/userManage.php";
include "../includes/orderManage.php";
$userObj 	= 	new userManager($con,$conmain);
$orderObj 	= 	new orderManage($con,$conmain);

$order_id = $_POST['order_id'];
$order_status = $_POST['order_status'];
//echo "fuhgsdhfghksfhgkshdfklghslkjdfhgk";
 $order_sql11 ="SELECT tc.id as odid,tc.order_no,tc.order_to,tc.id ,
u.firstname,u.address,u.mobile,u.gst_number_sss ,
c.cust_name,c.cust_address,c.cust_mobile
FROM `tbl_customer_orders_new` as tc
left join tbl_user u on tc.order_to=u.id  
left join tbl_customer c on tc.order_from=c.cust_id  
where order_no =".$order_id;
$result11 = mysqli_query($con,$order_sql11);
while($row = mysqli_fetch_array($result11))
{
	$order_id1 = $row["order_no"];
	$odid = $row["odid"];
	$order_to1 = $row["order_to"];
	$invoice_no_by_id = $row["id"];
	 
	$firstname2 = $row["firstname"];
	$address2 = $row["address"];
	$mobile2 = $row["mobile"];
	$gst_number_sss2 = $row["gst_number_sss"];
	
	$cust_name = $row["cust_name"];
	$cust_address = $row["cust_address"];
	$cust_mobile = $row["cust_mobile"];
}

$order_details = $orderObj->getShopOrdersbyorderid_cust($order_id);
//echo "asdasdasd<pre>";print_r($order_details);die();
$colspan3=3;
$colspan2=2;
?>
<style>
.darkgreen{
	background-color:#364622; color:#fff!important; font-size:24px;font-weight:600;
}
.fentgreen1{
	background-color:#b0b29c;
	color:#4a5036;
	font-size:12px;
}
.fentgreen{
	background-color:#b0b29c;
	color:#4a5036;
}
.font-big{
	font-size:20px;
	font-weight:600;
	color:#364622;
}
.font-big1{
	font-size:14px;
	font-weight:600;
	color:#364622;
}
.table-bordered-popup {
    border: 1px solid #364622;
}
.table-bordered-popup > tbody > tr > td, .table-bordered-popup > tbody > tr > th, .table-bordered-popup > thead > tr > td, .table-bordered-popup > thead > tr > th {
    border: 1px solid #364622;
	color:#4a5036;
}
.blue{
	color:#010057;
}
.blue1{
	color:#574960;
	font-size:16px;
}
.buyer_section{
	color:#574960;
	font-size:14px;
}
.pad-5{
	padding-left:10px;
}
.pad-40{
	padding-left:40px;
}
.np{
	padding-left:0px;
	padding-right:0px;
}
.bg{
	background-image:url(../../assets/global/img/invoice/<?php echo COMPANYNM;?>_logo-watermark.jpg); background-repeat:no-repeat;
	 background-size: 200px 200px;
}
</style>
<div class="modal-header">
<button type="button" name="btnPrint" id="btnPrint" onclick="takeprint_invoice('<?=SITEURL;?>')" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>

<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel"></h4>	   
</div>
<div class="modal-body" style="padding-bottom: 5px !important;" id="divPrintArea">
<div class="row">
<div class="col-md-12"> 
		<div class="portlet-body">
			<table class="table table-bordered-popup">
				<tbody>
				<tr>				
					<td colspan="6" width="70%" class="darkgreen" valign="top"><img src="../../assets/SiteLogo/<?php echo COMPANYNM;?>_logo-big.png" style="width:60px;"> &nbsp; <?php echo strtoupper(COMPANYNM);?></td>
				<td colspan="<?=$colspan3;?>" class="font-big text-center" valign="top">Tax Invoice</td>
				</tr>
				
				<tr>
			 <td colspan="6" class="fentgreen1"><?php echo ucfirst($firstname2);?><br/>
			 	    Address: <b><?php echo $address2;?></b><br/>
					Tel: <b><?php echo $mobile2;?></b> 					
					<?php if(!empty($gst_number_sss2)){?>GSTIN :<b><?php echo $gst_number_sss2;?></b><?php } ?></td>

					<td colspan='5' rowspan='2'>
					<div class='col-md-8 np'>Invoice No.:  <?=str_pad($invoice_no_by_id,6,"0",STR_PAD_LEFT);?><!-- <?=$order_details[0]['invoice_no']?> -->&nbsp;<span class='blue'></span></div><br/>
					<div class='col-md-8 np'>Order Date: <?php echo $order_details[0]["order_date"];?>&nbsp;<span class='blue'></span></div><br/>
					<div class='col-md-8 np'>Challan No.: &nbsp;-<span class='blue'><?=$order_details[0]['challan_no']?></span></div><br/>
					<div class='col-md-8 np'>Vehicle No.: &nbsp;-<span class='blue'><?=$order_details[0]['vehicle_no']?></span></div><br/>
					<!--<div class='col-md-8 np'>Transportation Mode: &nbsp;-<span class='blue'><?=$order_details[0]['transport_mode']?></span></div> <br/>-->
					<div class='col-md-8 np'>Delivery Date:<?=$order_details[0]['delivery_date']?>&nbsp;<span class='blue'></div></span><br/>
					<!--<div class='col-md-8 np'>Place of Supply: --<?php //echo $order_details[0]['place_of_supply'];?>&nbsp;<span class='blue'></span></div>-->
					</td>
				</tr>
				
				<tr>
					<td colspan='6' valign='top'>Buyer 
						<span class='buyer_section'><b><?=$cust_name;?></b><br/></span>
						<span class='buyer_section pad-40'><?php if(!empty($cust_address)){echo $cust_address.",";}?><br/></span>
						<span class='buyer_section pad-40'><?=$cust_mobile;?><br/></span>
					</td>                        
				</tr>
				<tr class='fentgreen'>
				<th width='5%' class='text-center'>SI No.</th>
				<th class='text-center'>Name of Goods</th>
				<th class='text-center'>HSN Code</th>	
				<th width='5%' class='text-center'>CGST</th>
				<th width='5%' class='text-center'>SGST</th>
				<th class='text-center'>Qty</th>
				<th class='text-center'>Rate</th>
				<th class='text-center'>UOM</th>
				<th class='text-center'>Value</th>
				</tr>
				<div class=="bg">
				<?php
				$i = 1;
				$final_qty = 0;
				$final_cost = 0;
				$total_amount = 0;
				$free_product_count=array();
				foreach($order_details as $val){
					$sr_no.=$i.'<br><br>';
					$product_name.=$val['productname'].'('.$val['product_variant_weight1'].' '.$val['unit'].')<br><br>';				
					$product_cgst.=number_format((float)$val['product_cgst'], 2, '.', '').'<br><br>';
					$product_sgst.=number_format((float)$val['product_sgst'], 2, '.', '').'<br><br>';
					$qty.=$val['variantunit'].'<br><br>';
					$c_o_d = $val['c_o_d'];
					$cod_percent = $val['cod_percent'];
					$final_qty = $final_qty + $val['variantunit'];
					$unit_cost.=number_format((float)$val['product_unit_cost'], 2, '.', '').'<br><br>';
					$nos.='nos<br><br>';
					//$total_cost.=($unit_cost*$val['variantunit']).'<br><br>';
					$total_cost.= $val['unitcost']."<br><br>";
					$final_cost = $final_cost + $val['unitcost'];
					if(!empty($val['free_product_details'])){
						$free_product_count[$i]=$val['free_product_details'];
					}
                     $cod_final_cost = ($cod_percent / 100) * $final_cost;
						$fcod_final_cost = $final_cost -$cod_final_cost;
					  ?>
				<tr >
					<td class="text-center" valign="top"><span class="blue"><?=$i;?></span></td>
					<td class="text-left" valign="top"><span class="blue"><?php echo $val['productname'].'('.$val['product_variant_weight1'].' '.$val['unit'].')';?></span></td>
					<td class="text-left" valign="top"><span class="blue"><?=$val['producthsn'];?></span></td>
					<td class="text-right" valign="top"><span class="blue"><?php echo number_format((float)$val['product_cgst'], 2, '.', '');?></span></td>
					<td class="text-right" valign="top"><span class="blue"><?php echo  number_format((float)$val['product_sgst'], 2, '.', '');?></span></td>
					<td class="text-right" valign="top"><span class="blue"><?=$val['variantunit'];?></span></td>
					<td class="text-right" valign="top"><span class="blue"><?php echo number_format((float)$val['product_unit_cost'], 2, '.', '');?></span></td>
					<td class="text-center" valign="top"><span class="blue"><?php echo 'nos';?></span></td>				
					<td class="text-right" align="right" valign="top"><span class="blue"><?=$val['unitcost'];?></span></td>
				</tr>
				<?php $i++; }?>
					
				</div>
				<tr>
				<td></td>
				<td class="text-right"><b>Total</b></td>
				<td class="fentgreen"></td>
				<td class="fentgreen"></td>
				<td class="fentgreen"></td>
				<td class="fentgreen text-right"><?=$final_qty;?></td>
				<td class="fentgreen"></td>
				<td class="fentgreen"></td>
				
				<td class="fentgreen" align="right"><?=number_format((float)$final_cost, 2, '.', '');?></td>
				</tr>
				
				<tr>
					<td colspan="2" width="30%" valign="top">
						<u>Declaration:</u>
						<br/><?php if($order_details[0]['signature_image']!=''){ ?><img src="../../uploads/cod/<?=$order_details[0]['challan_no']?>/<?=$order_details[0]['signature_image']?>" height="100" width="250"><?php } ?>	
						<br/>Buyer Signature
					</td>
					<td >
						<div class="text-center"  valign="top"><b><u>BANK DETAILS</u></b>
						</div>
						Sorry..No Bank Account Details.
						<td class="fentgreen font-big1" colspan="3" width="20%" valign="top">For <b><?php echo ucfirst($firstname2);?></b>
							<br/>
							<br/>
							<br/> Authorised Signature
						</td>
					<td colspan="<?=$colspan2;?>"  valign="top">
						<span style="display:inline-block; height:40px;" class="blue">Rounding Off</span>
						<br/>
						<span style="display:inline-block; height:40px;" class="blue"><b>Grand Total</b> </span>
						<br/>
						<span style="display:inline-block; height:40px;" class="blue">Opening Balance</span>
						<br/>
						<span style="display:inline-block; height:40px;" class="blue">Closing Balance </span>
					</td>
					<td  class="text-right" valign="top">
						<span style="display:inline-block; height:40px;" class="blue"><?=$cod_percent;?> % </span>
						<br/>
						<span style="display:inline-block; height:40px;" class="blue"><b> <?=number_format((float)$fcod_final_cost, 2, '.', '');?></b></span>
						<br/>
						<span style="display:inline-block; height:40px;" class="blue"></span>
						<br/>
						<span style="display:inline-block; height:40px;" class="blue"></span>
					</td>
				</tr>
				<tr>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>