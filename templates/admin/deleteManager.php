<?php 
include "../includes/header.php";
include "../includes/commonManage.php";	
include "../includes/sectionManage.php";
$sectionObj = new sectionManager($con, $conmain);

$del_page_id = $_GET['del_page_id'];
$del_section_id = $_GET['del_section_id'];
if (isset($del_page_id)) 
{
	$userid_local = $sectionObj->deletePageById($del_page_id);
	echo '<script>alert("Page Deleted Succesfully.");location.href="page-list.php";</script>';
}
if (isset($del_section_id)) 
{
	$userid_local = $sectionObj->deleteSectionById($del_section_id);
	echo '<script>alert("Section Deleted Succesfully.");location.href="section-list.php";</script>';
}
?>