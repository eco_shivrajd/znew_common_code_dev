<?php
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/reportManage.php";
include "../includes/stockManage.php";

$reportObj = new reportManage($con, $conmain);
$stockObj    =   new stockManager($con,$conmain);
$report_title = 'Stock Report All';
$records1 = $stockObj->TotalStockInOutproduct_stock_report();
//echo "dkhkdjs<pre>";print_r($records1);
if(!empty($_POST['dropdownUserType'])){
	$dropdownUserType=$_POST['dropdownUserType'];
}
if(!empty($_POST['u_id'])){
	$u_id=$_POST['u_id'];
}
foreach($records1 as $key=>$value){
	if(!empty($_POST['dropdownUserType'])&& !empty($_POST['u_id'])){
		if($value['user_type']==$dropdownUserType && $key==$u_id){
			$records[$key]=$value;
		}
	}else if(!empty($_POST['dropdownUserType'])&& empty($_POST['u_id'])){
		if($value['user_type']==$dropdownUserType){
			$records[$key]=$value;
		}
	}else{
		$records[$key]=$value;
	}
}
//echo "dkhkdjs<pre>";print_r($records);die();
$colspan = "5";
?>
<?php if ($_POST["actionType"] == "excel") { ?>
    <style>table { border-collapse: collapse; } 
        table, th, td {  border: 1px solid black; } 
        body { font-family: "Open Sans", sans-serif; 
               background-color:#fff;
               font-size: 11px;
               direction: ltr;}
        </style>
    <?php }
    ?>

    <table 
        class="table table-striped table-bordered table-hover table-highlight table-checkable" 
    data-provide="datatable" 
    data-display-rows="10"
    data-info="true"
    data-search="true"
    data-length-change="true"
    data-paginate="true"
    id="sample_5">
    <thead>
        <tr>
            <td colspan="<?= $colspan; ?>" align="canter" class="gradeX even" style="text-align:center; font-weight:600;"><h4><b><?php
                      echo "Stock Report All";
                        ?></b></h4></td> 
        </tr>
        <tr>
            <th data-filterable="false"  data-sortable="true" data-direction="desc" >Sr NO.</th>
            <th data-filterable="false" data-sortable="true" data-direction="desc">Name</th> 
           <!-- <th data-filterable="false" data-sortable="true" data-direction="desc">Cartons</th> -->
            <th data-filterable="false" data-sortable="true" data-direction="desc"> Total Stock IN </th>   
            <th data-filterable="false" data-sortable="false" data-direction="desc">Total Delivered</th>	
            <th data-filterable="false" data-sortable="false" data-direction="desc">Total Available Stock</th>
        </tr>
    </thead>
    <tbody>					
        <?php
        if (!empty($records)) {
			$i=1;
            foreach ($records as $key => $value) {
				if($value['in']!=0 && $value['out']!=0){ ?>
				 <tr class="odd gradeX">				
                    <td align='right' ><?= $i; ?></td>
                    <!--<td align='Left'><?= $value['username']; ?></td> --> 
					 <td align='Left'><a onclick="myFunction('<?=$key;?>','<?=$value['username'];?>')" title="Stock Details ">
						<?php echo $value['username'];?></a></td> 
                    <td align='right'><?= $value['in']; ?></td>
                    <td align='right'><?= $value['out']; ?></td>
                    <td align='right'><?php echo  $value['in']-$value['out']; ?></td>
                </tr>
						
					<?php $i++; } } ?>
            <?php
        } 
        if ($_POST["actionType"] == "excel" && $row == 0) {
                echo "<tr><td>No matching records found</td></tr>";
            }
        ?>	

    </tbody>	
</table>

 <div class="modal fade" id="order_details" role="dialog">
        <div class="modal-dialog" style="width: 880px !important;">
            <!-- Modal content-->
            <div class="modal-content" id="order_details_content">
            </div>
        </div>
    </div>

<script>
    jQuery(document).ready(function () {

        ComponentsPickers.init();
        TableManaged.init();
    });
    function myFunction(userid,username){
		//alert(userid);
        var param="";
        param +="userid="+userid;
        param +="&username="+username;
		//alert(param);
        $.ajax
        ({
            type: "POST",
            url: "stock_report_popup.php",
            data: param,
            success: function (msg)
            {
                console.log(msg);
                $("#order_details_content").html(msg);              
                $('#order_details').modal('show');
            }
        });
    }

    $(document).ready(function () {
        var table = $('#sample_5').dataTable();
        // Perform a filter
        table.fnFilter('');
        // Remove all filtering
        //table.fnFilterClear();

    });
</script> 
<!-- END JAVASCRIPTS -->
<?php
if ($_POST["actionType"] == "excel") {
    if ($row != 0) {
        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=Stock_Report.xls");
    }
}
?>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- END JAVASCRIPTS -->