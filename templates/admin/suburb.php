<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
if($_SESSION[SESSION_PREFIX.'user_type']!="Admin") 
{
	header("location:../logout.php");
}
?>
<!-- END HEADER -->
<style type="text/css">	
a[href="#"]{
    cursor: default;
    text-decoration: none;
}
</style>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">

	<!-- BEGIN SIDEBAR -->
	<?php 
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Taluka";
	include "../includes/sidebar.php"
	?>	
	<!-- END SIDEBAR -->
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Taluka
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<!-- <a style="cursor: default;text-decoration: none;">Taluka</a> -->
						<a href="#">Taluka</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
            <div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Taluka Listing
							</div>
							<?php if ($ischecked_add==1) { ?>
                            <a href="suburb-add.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Taluka
                              </a>
                          <?php } ?>
                             
						</div>
						 <div class="clearfix"></div>
						<div class="portlet-body">
							<div class="clearfix"></div>
							<table 
							class="table table-striped table-bordered table-hover table-highlight table-checkable" 
							data-provide="datatable" 
							data-display-rows="10"
							data-info="true"
							data-search="true"
							data-length-change="true"
							data-paginate="true"
							id="sample_5">
							<!--<table class="table table-striped table-bordered table-hover" id="sample_2">-->
							<thead>
							<tr>
								<th>
									 Taluka Name
								</th>
								<th>
									District
								</th>
								<th>
									State
								</th>
								<th>
                                  Action
                                </th>
							</tr>
							</thead>
							<tbody>
							<?php
							$sql="SELECT * FROM `tbl_area` where tbl_area.isdeleted!='1' ORDER BY tbl_area.id DESC";
							$result1 = mysqli_query($con,$sql);
							 if(mysqli_num_rows($result1)>0){
							while($row = mysqli_fetch_array($result1))
							{						
								echo '<tr class="odd gradeX"><td>';
                                if($ischecked_edit==1){
								echo '<a href="suburb1.php?id='.base64_encode($row['id']).'">'.fnStringToHTML($row['suburbnm']).'</a>';
                                 }else{
                                                echo '' . fnStringToHTML($row['suburbnm']) . '';}
								echo '</td><td>';

								$city_id=$row['cityid'];						
								$sql="SELECT name FROM tbl_city where id = $city_id";
								$result = mysqli_query($con,$sql);
								while($num = mysqli_fetch_array($result))
								{ 
									echo  $num['name'];
								}									
								echo '</td><td>';
								$state_id=$row['stateid'];						
								$sql="SELECT name FROM tbl_state where id = $state_id";
								$result = mysqli_query($con,$sql);
								while($num = mysqli_fetch_array($result))
								{ 
									echo  $num['name'];
								}
								echo '</td>';
								echo '<td>';							

								          if ($ischecked_edit==1) 
                                            { ?>
                                              <a title="Edit" href="suburb1.php?id=<?=base64_encode($row['id']);?>" class="btn btn-xs btn-primary ">
											  <span class="glyphicon glyphicon-edit"></span></a>
                                            <?php } else {
                                                 echo '<a title="Can Not Edit This Record" href="#" class="btn btn-xs btn-primary" disabled><span class="glyphicon glyphicon-edit"></span></a>';
                                            }
                                            if ($ischecked_delete==1) 
                                            { ?>
                                              <a title="Delete" onclick="javascript: deleteProduct(<?=$row['id'];?>)"  class="btn btn-xs btn-danger">
											  <span class="glyphicon glyphicon-trash"></span></a>
                                           <?php } else {
                                                 echo '<a title="Can Not Delete This Record" onclick="#" class="btn btn-xs btn-danger" disabled><span class="glyphicon glyphicon-trash"></span></a>'; 
                                            }  
								echo '</td>';
								echo '</tr>';
							}
						 }
							?>
							</tbody>
							</table>
						</div>
					</div>
            
				
                    
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
                function deleteProduct(area_id) {
                    if (confirm('Are you sure you want to delete this taluka and subarea under this Taluka?')) {
                        CallAJAX('ajax_product_manage.php?action=delete_area&area_id=' + area_id );
                    }
                }

                function CallAJAX(url) {
                    if (window.XMLHttpRequest) {
                        xmlhttp = new XMLHttpRequest();
                    } else {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            alert('Taluka and subarea under this taluka deleted successfully.');
                            location.reload();
                        }
                    }
                    xmlhttp.open("GET", url, true);
                    xmlhttp.send();
                }
            </script>
</body>
<!-- END BODY -->
</html>