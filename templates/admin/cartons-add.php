<?php
include "../includes/grid_header.php";
include "../includes/commonManage.php";
$commonObj = new commonManage($con, $conmain);
$commonObjctype = $commonObj->log_get_commonclienttype($con, $conmain);

$user_type = $_SESSION[SESSION_PREFIX . 'user_type'];
$user_id = $_SESSION[SESSION_PREFIX . 'user_id'];
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "ManageProducts";
        $activeMenu = "Cartons";
        include "../includes/sidebar.php";
        $commonObj  =   new commonManage($con,$conmain);
        $row_url = $commonObj->getPageIDforUrlAdd($php_page_name);
        $page_id_url = $row_url['page_id'];
        $row_url_add=$commonObj->getURLforAdd($profile_id,$page_id_url);
        $ischecked_add_url = $row_url_add['ischecked_add'];
        if ($ischecked_add_url == 0 && $ischecked_add_url!='') 
        {
        session_set_cookie_params(0);
        session_start();
        session_destroy();
        echo '<script>location.href="../login.php";</script>';
        exit;
        }
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

                <!-- /.modal -->

                <h3 class="page-title">
                    Cartons
                </h3>
                <div class="page-bar">                   
					<ul class="page-breadcrumb">					
						<li>
							<i class="fa fa-home"></i>
							<a href="cartons-list.php">Cartons</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Carton Add/Update</a>
						</li>
					</ul>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">

                        <div class="portlet box blue-steel" style="display:none;">
                            <div class="portlet-title">
                                <div class="caption">Cartons Product Cart </div>	
                                <a id="btnEmpty" class="btn btn-sm btn-default pull-right mt5" onClick="cartActionEmpty();">Empty Cart</a>
                                <div class="clearfix"></div>
                            </div>						
                            <div class="portlet-body">	
                                
                                    <table class="table table-striped table-bordered table-hover" id="myTable" >
                                        <thead>
                                            <tr>					
                                                <th>
                                                    Category
                                                </th>
                                                <th>
                                                    Product Name
                                                </th>
                                                <th>
                                                    Product HSN
                                                </th>
                                                <th>
                                                    Product Price
                                                </th>
                                                <th>
                                                    Product Variant
                                                </th>
                                                <th>
                                                    Product Image
                                                </th>
                                                <th>
                                                    Quantity
                                                </th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody >

                                        </tbody>
                                    </table>
                                 <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" >
                                     <div class="form-group" id="place_order_div" style="display:none">
                                        <div class="col-md-4 col-md-offset-5">
                                            <button type="button" name="place_order_btnsubmit" id="place_order_btnsubmit" class="btn btn-primary" onclick="placeOrderStockist();">Place Order</button>
                                        </div>
                                    </div><!-- /.form-group -->
                                 </form>
                            </div>
                            <!-- END PAGE CONTENT-->
                        </div>
                        <div class="clearfix"></div>   


                        <div class="portlet box blue-steel">
                            <div class="portlet-title">

                                <div class="caption">Products Listing</div>
                                <a id="btnEmpty" class="btn btn-sm btn-default pull-right mt5" onclick="cartActionAdd1();">Add Quantities</a>
                                <div class="clearfix"></div>

                            </div>

                            <div class="portlet-body">
                                <form id="frmCart">
                                    <table class="table table-striped table-bordered table-hover" id="sample_2sdfgjd">
                                        <thead>
                                            <tr>					
                                                <th>
                                                    Category
                                                </th>
                                                <th>
                                                    Product Name
                                                </th>
                                                <th>
                                                    Product HSN
                                                </th>
                                                <th>
                                                    Product Price
                                                </th>
                                                <th>
                                                    Product Variant
                                                </th>
                                                <th>
                                                    Product Image
                                                </th>
                                                <th>
                                                    Quantity
                                                </th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
										$get_user_sql = "SELECT parent_ids FROM `tbl_user` WHERE id = '".$user_id."' limit 1";
                                        $result_user = mysqli_query($con, $get_user_sql);
										$row_user = mysqli_fetch_assoc($result_user);
										
                                             $sql = "SELECT a.categorynm,b.productname,b.id,pv.producthsn,pv.price,pv.variant_1,pv.variant1_unit_id,
															pv.variant_2,pv.variant2_unit_id,
															(select unitname from tbl_units tu1 WHERE tu1.id=pv.variant1_unit_id )as unitname1,
															(select unitname from tbl_units tu2 WHERE tu2.id=pv.variant2_unit_id)as unitname2,
															pv.id as pvid, 
															pv.productimage,b.added_by_userid,b.added_by_usertype, a.id AS cat_id  
                                                     FROM  tbl_product b 
													 LEFT JOIN tbl_category a on b.catid = a.id 
                                                     LEFT JOIN tbl_product_variant pv on b.id = pv.productid 
													WHERE  b.isdeleted != 1 AND 
											((find_in_set(b.added_by_userid,'".$row_user['parent_ids']."') <> 0) 
											or b.added_by_userid =  '".$user_id."')";
                                            $result = mysqli_query($con, $sql);
                                            $row_count = mysqli_num_rows($result);
                                            while ($row = mysqli_fetch_array($result)) { //echo "<pre>";print_r($row);
                                                $prodcode = $row['id'] . '_' . $row['pvid'];
                                                ?>

                                                <tr class="odd gradeX" id="<?php echo $prodcode; ?>">
                                                    <td id="num1<?php echo $prodcode; ?>">
                                                        <?php echo fnStringToHTML($row['categorynm']); ?>
                                                    </td>
                                                    <td id="num2<?php echo $prodcode; ?>">
                                                        <?php echo fnStringToHTML($row['productname']); ?>
                                                    </td>
                                                    <td id="num3<?php echo $prodcode; ?>">
                                                        <?php echo fnStringToHTML($row['producthsn']); ?>
                                                    </td>
                                                    <td align="right" id="num4<?php echo $prodcode; ?>">
                                                        <?php echo fnStringToHTML($row['price']); ?>
                                                    </td>										
                                                    <td id="num5<?php echo $prodcode; ?>">
														 <?php if(!empty($row['variant_1'])){echo $row['variant_1'] . "-" . $row['unitname1'] . " ";} ?>
														  <?php if(!empty($row['variant_2'])){ echo $row['variant_2'] . "-" . $row['unitname2'] . " ";} ?></br>
                                                    </td>
                                                    <td id="num6<?php echo $prodcode; ?>">
                                                        <?php if (!empty($row['productimage'])) { ?>
                                                            <img src="upload/<?php echo COMPANYNM; ?>_upload/<?php echo $row['productimage']; ?>" 
                                                                 alt=<?php echo $row['productimage']; ?> 
                                                                 width="100px" />
                                                             <?php } ?>										 
                                                    </td>
                                                    <td id="num7<?php echo $prodcode; ?>" > 
                                                        <?php  $sqlvarunit = "SELECT qnty FROM tbl_dcp_cartons  where prod_id='" . $row['id'] . "' "
                                                                . " and prod_var_id= '".$row['pvid']."' ";
                                                        $resultvarunit = mysqli_query($con, $sqlvarunit);
                                                        $countrow_qnty= mysqli_num_rows($resultvarunit);
                                                        $carton_quantities=0;
                                                        if($countrow_qnty>0){
                                                             while ($rowvarunit = mysqli_fetch_array($resultvarunit)) {
                                                                $carton_quantities=$rowvarunit['qnty'];
                                                            }
                                                        }
                                                        ?>
                                                        <input type="number" min="0"  name="prodqnty" max="100000" oninput="myFunction(this)"   id="qty_<?php echo $prodcode; ?>" 
                                                               value="<?php echo $carton_quantities; ?>" size="2" style="width: 55px; margin: 5px 5px 5px 5px;text-align: right;"> 
                                                    </td>
                                                </tr>
                                            <?php } ?>							
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div> 

                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <?php include "../includes/grid_footer.php" ?>
    <!-- END FOOTER -->

    <script>
	function myFunction(varl) {		
		if (varl.value != "") {		
			var lengthofvalue=varl.value.toString().length;
			if (lengthofvalue > 5) {
				varl.value = varl.value.substring(0, 5);//varl.value.toString().length - 1			
			}
		}
	}
        //useful for add to kart
        function placeOrderStockist(){           
            var orderarray=[];
            $('#myTable tbody tr').each(function () {  
                var void1 = [];
                var newenergy = this.id.replace('mytabletr', '');
                var tempstrip=$('#myqty_' + newenergy + '').html();                            
                void1.push(tempstrip.replace(/ +$/, "")); 
                void1.push(newenergy); 
                 orderarray.push(void1); 
            });
          console.log(orderarray);
            if (orderarray.length > 0) {
                var url = 'update_add_cartons.php';               
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {data123 : orderarray},
                    error: function (data) {
                        console.log("error:" + data)
                    },
                    success: function (data) {
                        console.log(data);
                        if (data > 0) {
                            alert("Cartons added Successfully.");
                            location.reload();
                        } else {
                            alert("Not updated.");
                        }
                    }
                });
            } else {
                alert("Please add some product.");
                return false;
            }
        }
        function cartActionEmpty(){
            $("#myTable tbody").empty();
        }
        function cartActionEmptyItem(trid){
            $("#mytabletr"+trid+"").remove();
        }

        
        //above code is useful for add to kart end
        function cartActionAdd1() {
            var void1 = [];
            var void2 = [];
            $('input[name="prodqnty"]').each(function () {
                if (this.value > 0) {
                    void1.push(this.value);
                    void2.push(this.id);
                }
            });  
            var orderarray=[];
           
            for(var i=0;i<void1.length;i++){               
                var tempqnty=void1[i];
                var tempprovar = void2[i].replace('qty_', '');
                orderarray[i]=[tempqnty,tempprovar];
            }
            console.log(orderarray);
            //return false;
             if (orderarray.length > 0) {
                var url = 'update_add_cartons.php';               
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {data123 : orderarray},
                    error: function (data) {
                        console.log("error:" + data)
                    },
                    success: function (data) {
                        console.log(data);
                        if (data > 0) {
                            alert("Cartons added Successfully.");
							window.location.href = "cartons-list.php";
                            //location.reload();//cartons-list.php
                        } else {
                            alert("Not updated.");
                        }
                    }
                });
            } else {
                alert("Please add some product.");
                return false;
            } 
        }
    </script>


</body>
<!-- END BODY -->
</html>