<?php 
include ("../../includes/config.php");
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
//echo "<pre>";print_r($_POST);die();
$tabpanelids=$_POST['tabpanelids'];
$orderwise='true';
$order_status = $_POST['order_status'];
$order_id = $_POST['order_id'];
$orders = $orderObj->getOrderschnage1($order_status,$order_id);
$order_count = count($orders);
$user_type=$_SESSION[SESSION_PREFIX . "user_type"];
$session_user_id=$_SESSION[SESSION_PREFIX . "user_id"];

?>
<div class="clearfix"></div>
<table class="table table-striped table-bordered table-hover" id="sample_2">
	<thead>
		<tr id="main_th">
			<?php if(($order_status == '1' ||$order_status == '11')&& $order_count != 0 ){ ?>
			 <th >
				For Delivery<br>Production                                                            
			</th>
			<?php  } ?>
			<th>
				Region
			</th>
			<th>
				Order From
			</th>
			<th>
				Order To
			</th>
			<th>
				Order Id<br>Order Date 
			</th>								
			<?php if($orderwise!='true'){ ?>
			 <th>
				Product Name			
			</th>	
			<?php } ?>
			<th>
				Quantity
			</th>
			<th>
				Total Price (₹)
			</th>
			<th>
				Price(₹)<br>(GST+Discount)
			</th>
			<?php if($order_status == '10'){  ?>
				<th>
					Order Status
				</th>
			<?php } ?>
			<th>
				Action  
			</th>
		</tr>
		</thead>
		<tbody>
		<?php				
		foreach($orders as $key=>$value)
		{
			 $order_totalcost = 0;
			 $order_totalgstcost = 0;
			$prod_qnty_total=0;
			$orderdetails = $orderObj->getOrdersDetailschnage($value['oid']);	
			$oid = $value['oid'];
			$product_name='';
			$prod_qnty='';$totalcost='';$gstcost='';
			if(count($orderdetails['order_details'])==1){
				$order_status_myorders=$orderdetails['order_details'][0]['order_status'];
				if($session_user_id==$orderdetails['order_details'][0]['added_by_userid']&& $_SESSION[SESSION_PREFIX . "user_role"]!='Admin'){
					$product_name ="<span id='tooltipid' title='This product can not be sent to production.' style='color:#6a6e75;'>".$orderdetails['order_details'][0]['cat_name']."-".$orderdetails['order_details'][0]['product_name']."</span><br>";
					//$product_name=$orderdetails['order_details'][0]['cat_name']."-".$orderdetails['order_details'][0]['product_name']."<br>my";
				}else{
					$product_name=$orderdetails['order_details'][0]['cat_name']."-".$orderdetails['order_details'][0]['product_name']."<br>";
				}
				
				
				$prod_qnty='<a onclick="showOrderDetails(\''.$orderdetails['order_details'][0]['odid'].'\',\'Order Product Details\')" title="Order Product Details">'.$orderdetails['order_details'][0]['product_quantity'];
				if(!empty($orderdetails['order_details'][0]['product_variant_weight1'])){
					$prod_qnty.="(".$orderdetails['order_details'][0]['product_variant_weight1']."-".$orderdetails['order_details'][0]['product_variant_unit1'].")";
				}
				if(!empty($orderdetails['order_details'][0]['product_variant_weight2'])){
					$prod_qnty.="(".$orderdetails['order_details'][0]['product_variant_weight2']."-".$orderdetails['order_details'][0]['product_variant_unit2'].")";
				}
				$prod_qnty.="</a>";	
				$campaign_applied=$orderdetails['order_details'][0]['campaign_applied'];
				$campaign_type=$orderdetails['order_details'][0]['campaign_type'];
				if($campaign_applied == 0 && $campaign_type=='free_product'){
					$prod_qnty.="<span style='float: left'><img src='../../assets/global/img/free-icon.png' title='Free Product'></span>";	
				}
				$prod_qnty.="<br>";
				$prod_qnty_total=$orderdetails['order_details'][0]['product_quantity'];
				
				$totalcost=$orderdetails['order_details'][0]['product_total_cost'];
				
				if($orderdetails['order_details'][0]['discounted_totalcost']!=0 && $orderdetails['order_details'][0]['discounted_totalcost']!=''){
					$totalcost.="(<font color='green'>".$orderdetails['order_details'][0]['discounted_totalcost']."</font>)";
				}
				$totalcost.="<br>";
				 $order_totalcost= $orderdetails['order_details'][0]['product_total_cost'];
				
				$gstcost=$orderdetails['order_details'][0]['p_cost_cgst_sgst']."<br>";
				$order_totalgstcost = $orderdetails['order_details'][0]['p_cost_cgst_sgst'];
			}else{
				foreach ($orderdetails['order_details'] as $key1=>$value1){
					$order_status_myorders=$orderdetails['order_details'][0]['order_status'];
					if($session_user_id==$value1['added_by_userid']&& $_SESSION[SESSION_PREFIX . "user_role"]!='Admin'){						
						$product_name.="<span id='tooltipid' title='This product can not be sent to production.' style='color:#6a6e75;'>".$value1['cat_name']."-".$value1['product_name']."</span><br>";
						//$product_name.=$value1['cat_name']."-".$value1['product_name']."<br>";
					}else{
						$product_name.=$value1['cat_name']."-".$value1['product_name']."<br>";
					}
					
					$prod_qnty.='<a onclick="showOrderDetails(\''.$value1['odid'].'\',\'Order Product Details\')" title="Order Product Details">'.$value1['product_quantity'];
					if(!empty($value1['product_variant_weight1'])){
						$prod_qnty.="(".$value1['product_variant_weight1']."-".$value1['product_variant_unit1'].")";
					}
					if(!empty($value1['product_variant_weight2'])){
						$prod_qnty.="(".$value1['product_variant_weight2']."-".$value1['product_variant_unit2'].")";
					}
					$prod_qnty.="</a>";
					$campaign_applied=$value1['campaign_applied'];
					$campaign_type=$value1['campaign_type'];
					if($campaign_applied == 0 && $campaign_type=='free_product'){
						$prod_qnty.="<span style='float: left'><img src='../../assets/global/img/free-icon.png' title='Free Product'></span>";	
					}
					$prod_qnty.="<br>";
					$totalcost.=$value1['product_total_cost'];
					
					$prod_qnty_total=$prod_qnty_total+$value1['product_quantity'];
					 $order_totalcost = $order_totalcost + $value1['product_total_cost'];
					 
					if($value1['discounted_totalcost']!=0 && $value1['discounted_totalcost']!=''){
						$totalcost.="(<font color='green'>".$value1['discounted_totalcost']."</font>)";
					}
					$totalcost.="<br>";
					$gstcost.=$value1['p_cost_cgst_sgst']."<br>";
					$order_totalgstcost = $order_totalgstcost + $value1['p_cost_cgst_sgst'];
				}
			}
		?>
		<tr class="odd gradeX">
		<?php if(($order_status == '1' ||$order_status == '11')&&  $order_count != 0 ){  ?>
			<td>
				<input type="checkbox" class="mycheckbox" name="select_all[]" id="select_all" value="<?php echo $value["oid"];?>">Delivery<br>
				<?php if($user_type!='Admin'){ ?>
				<input type="checkbox" class="mycheckbox1" name="select_all_send[]" id="select_all_send" value="<?php echo $value["oid"];?>" 
				<?php if($value["sended_to_production"]=='1'){?> disabled <?php } ?>>Production <?php } ?>
			</td>
		<?php } ?>
			<td ><?php if(!empty($value["region_name"])){echo $value["region_name"];}else{echo $value["region_name1"];} ?></td>
			<td ><?php echo $value["order_by_name"];if(!empty($value["shop_name"])){echo "-".$value["shop_name"];}?></td>					
			<td ><?php echo $value["order_to_name"];?></td>
			<td ><font size="1.4"> <?php echo $value["order_no"]?></font><br><?php echo date('d-m-Y h:i:s A',strtotime($value["order_date"]));?></td>										
			<?php if($orderwise!='true'){ ?><td><?php echo $product_name;?></td><?php } ?>
			<td <?php if($orderwise=='true'){?> align="right" <?php }?>>
				<?php if($orderwise!='true'){echo $prod_qnty;}else{
					echo '<a onclick="showOrderDetails1(\''.$order_status.'\',\''.$value['oid'].'\')" title="Order Details">'.$prod_qnty_total.'<a>';}?>
			</td>
			<td align="right"><?php if($orderwise!='true'){echo $totalcost."</br>";}?>  <b><?php echo number_format((float)$order_totalcost, 2, '.', '');?></b></td>
			<td align="right"><?php if($orderwise!='true'){echo $gstcost."</br>";}?><b><?php echo number_format((float)$order_totalgstcost, 2, '.', '');?></b> </td>
		<?php if($order_status == '10'){  ?>
			<td><b><?php $order_status_switch_array=array('1'=>'Sent To Production','2'=>'Delivery Assigned','4'=>'Delivery Received');
			echo $order_status_switch_array[$order_status_myorders];?></b></td>
		<?php } ?>
			<td><a onclick='showInvoice(1,<?=json_encode($oid);?>)'  title="View Invoice">View Invoice</a></td>
		</tr>
		<?php } ?>
		
		 </tbody>
	</table>
<script>
$(document).ready(function() {
	 $("#sample_2").dataTable().fnDestroy();			
    $('#sample_2').dataTable( {
	order: [],
	columnDefs: [ { orderable: false, targets: [0] } ]
	});
});

</script>
	<script>
jQuery(function () {    
    $("input.mycheckbox").click(function () {
		var checked_boxes = $('input.mycheckbox:checked').length;		
		if(checked_boxes>0){ 
		$("#statusbtnplacedorder").attr("disabled", true);
		}else{
			$("#statusbtnplacedorder").attr("disabled", false);
		}
    })
	$("input.mycheckbox1").click(function () {
		var order_status = $('#order_status').val();
		var checked_boxes = $('input.mycheckbox1:checked').length;		
		if(checked_boxes>0){ 
			if(order_status==1 || order_status==11){
				$("#statusbtn").attr("disabled", true);
				$("#statusDbtn").attr("disabled", true);
			}
					
		}else{
			if(order_status==1 || order_status==11){
				$("#statusbtn").attr("disabled", false);
				$("#statusDbtn").attr("disabled", false);
			}			
		}
    })
}); 
</script>