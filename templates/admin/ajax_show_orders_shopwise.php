<?php 
include ("../../includes/config.php");
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
$order_status = $_POST['order_status'];
$result1 = $orderObj->getOrders_shopwise($order_status);
$order_count = count($result1);
?>
<div class="clearfix"></div>
<table class="table table-striped table-bordered table-hover" id="sample_2">
						<thead>
							<tr>
							<th width="20%">Order Date</th>
							<th width="25%">Shop Name</th>
							<th width="25%">Orders</th>
							<th width="10%"> Quantity</th>
							<th width="10%">Unit Cost</th>
							<th width="10%">Total Cost</th>							
							</tr>
						</thead>
						<tbody>	
						<?php
                       if($order_count > 0)
                       {														
									while($row1 = mysqli_fetch_array($result1)) 
									{ 
										$newarr['order_date']=$row1['order_date'];
										$newarr['shopnm']=$row1['shopnm'];
										$newarr['order_no']=$row1['order_no'];
										$order_noss=$row1['order_no'];
										$newarr['product_quantity']=$row1['product_quantity'];
										$newarr['p_cost_cgst_sgst']=$row1['p_cost_cgst_sgst'];
										 $newarr['product_unit_cost']=$row1['product_unit_cost'];
										$newarr1[]=$newarr;	
									}
									$out = array();
									foreach ($newarr1 as $key => $value){
										$countval=0;
										foreach ($value as $key2 => $value2){
											if($key2=='order_no'){
												 $index = $key2.'-'.$value2;
												if (array_key_exists($index, $out)){
													$out[$index]++;
												} else {
													$out[$index] = 1;
												}
											}
										}
									}
									$out1 = array_values($out);$j=0;$temp=$newarr1[0]["order_no"];$sum=0;
									for($i=0;$i<count($newarr1);$i++){
										$total=$newarr1[$i]['product_quantity']*$newarr1[$i]['product_unit_cost'];	?>
										<tr class="odd gradeX">
											<?php if($temp==$newarr1[$i]["order_no"]){ ?>
										<td rowspan='<?php echo $out1[$j];?>'><?php echo $newarr1[$i]['order_date'];?></td>
										<td rowspan='<?php echo $out1[$j];?>'><?php echo $newarr1[$i]['shopnm'];?></td>
										<td rowspan='<?php echo $out1[$j];?>'>
											<a onclick="showInvoice(<?php echo "'".$newarr1[$i]["order_no"]."'";?>)">Show Invoice</a></br>
                                            <?php echo $newarr1[$i]['order_no'];?></br>
											<a href="invoicedemo2.php?order_id=<?php echo $newarr1[$i]["order_no"];?>" title="PDF Generator">View PDF</a>	
											</a>
										</td>
										<?php   
										$sum=$sum+$out1[$j]; $temp=$newarr1[$sum]["order_no"]; $j++; 
											} 
											else
												{?>
											 <td style="display: none;"><?php echo $newarr1[$i]['order_date'];?></td> 
											 <td style="display: none;"><?php echo $newarr1[$i]['shopnm'];?></td> 
											  <td style="display: none;"><a onclick="showInvoice(<?php echo "'".$newarr1[$i]["order_no"]."'";?>)"><?php echo $newarr1[$i]['order_no'];?></a></td>
											<?php } ?>
										<td align="right"><?php echo $newarr1[$i]['product_quantity'];?></td>
										<td align="right"><?php echo $newarr1[$i]['product_unit_cost'];?></td>
										<td align="right"><?php echo $newarr1[$i]['p_cost_cgst_sgst'];?></td>
										</tr>
									<?php 
								}
					}
					?>							
						</tbody>	
						</table>
<script>
$(document).ready(function() {
	 $("#sample_2").dataTable().fnDestroy()

    $('#sample_2').dataTable( {
	order: [],
	columnDefs: [ { orderable: false, targets: [0] } ]
	});
});
$("#select_all").click(function(){
    $('input:checkbox').prop('checked', this.checked);
});
</script>