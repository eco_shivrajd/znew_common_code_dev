<?php 
include ("../../includes/config.php");
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
$order_status = $_POST['order_status'];
$orders = $orderObj->getOrderschnage($order_status);
$order_count = count($orders);
?>
<div class="clearfix"></div>
<table class="table table-striped table-bordered table-hover" id="sample_2">
<thead>
	<tr>
		<?php 
		if($order_status == 1){ ?>
		<th id="select_th">
			<input type="checkbox" name="select_all[]" id="select_all" disabled onchange="javascript:checktotalchecked(this)">
		</th>
		<?php 
		 } 
		?>
		<th>
			Region
		</th>
		<th>
			Sales Person
		</th>
		<th>
			Shop Name
		</th>
		<th>
			Order Id<br><?php echo str_repeat("&nbsp;", 8); ?>Order Date 
		</th>								
		
		 <th>
			Product Name			
		</th>	
		<th>
			Quantity
		</th>
		<th>
			Price (₹)
		</th>
		<th>
			Price(₹)<br>(GST+Discount)
		</th>
		<th>
			Action  
		</th>
	</tr>
</thead>
<tbody>
				<?php				
				foreach($orders as $key=>$value)
				{
					$orderdetails = $orderObj->getOrdersDetailschnage($value['oid']);
					$product_name='';
					$prod_qnty='';$totalcost='';$gstcost='';
					if(count($orderdetails['order_details'])==1){						
						$product_name=$orderdetails['order_details'][0]['cat_name']."-".$orderdetails['order_details'][0]['product_name']."<br>";
						$prod_qnty='<a onclick="showOrderDetails(\''.$orderdetails['order_details'][0]['id'].'\',\'Order Details\')" title="Order Details">'.$orderdetails['order_details'][0]['product_quantity'];
						if(!empty($orderdetails['order_details'][0]['product_variant_weight1'])){
							$prod_qnty.="(".$orderdetails['order_details'][0]['product_variant_weight1']."-".$orderdetails['order_details'][0]['product_variant_unit1'].")";
						}
						if(!empty($orderdetails['order_details'][0]['product_variant_weight2'])){
							$prod_qnty.="(".$$orderdetails['order_details'][0]['product_variant_weight2']."-".$$orderdetails['order_details'][0]['product_variant_unit2'].")";
						}
						$prod_qnty.="</a><br>";	
						$totalcost=$orderdetails['order_details'][0]['product_total_cost']."<br>";
						$gstcost=$orderdetails['order_details'][0]['p_cost_cgst_sgst']."<br>";
					}else{
						foreach ($orderdetails['order_details'] as $key1=>$value1){
							$product_name.=$value1['cat_name']."-".$value1['product_name']."<br>";
							$prod_qnty.='<a onclick="showOrderDetails(\''.$value1['id'].'\',\'Order Details\')" title="Order Details">'.$value1['product_quantity'];
							if(!empty($value1['product_variant_weight1'])){
								$prod_qnty.="(".$value1['product_variant_weight1']."-".$value1['product_variant_unit1'].")";
							}
							if(!empty($value1['product_variant_weight2'])){
								$prod_qnty.="(".$value1['product_variant_weight2']."-".$value1['product_variant_unit2'].")";
							}
							$prod_qnty.="</a><br>";
							$totalcost.=$value1['product_total_cost']."<br>";
							$gstcost.=$value1['p_cost_cgst_sgst']."<br>";
						}
					}
					
				?>
				<tr class="odd gradeX">
				<?php if($order_status == 1){ ?>
					<td><input type="checkbox" name="select_all[]" id="select_all" value="ordersub_<?php echo $value["oid"];?>_<?php echo $value["odid"];?>"  onchange="javascript:checktotalchecked(this)">
				</td><?php } ?>
						<td ><?php echo $value["region_name"];?></td>
						<td ><?php echo $value["order_by_name"];?></td>					
						<td ><?php echo $value["shop_name"];?></td>
						<td ><font size="1.4"><?php echo $value["order_no"]?></font><br><?php echo str_repeat("&nbsp;", 8)."".date('d-m-Y H:i:s',strtotime($value["order_date"]));?></td>									
						<td><?php echo $product_name;?></td>
						<td align="right"><?php echo $prod_qnty;?></td>
						<td align="right"><?php echo $totalcost;?></td>
						<td align="right"><?php echo $gstcost;?></td>
						<td>
						<a onclick="showInvoice(1,<?=$value['oid'];?>)" title="View Invoice">View Invoice</a>
						</td>
					</tr>
				<?php } ?>
				
				 </tbody>
</table>
<script>
$(document).ready(function() {
	 $("#sample_2").dataTable().fnDestroy()

    $('#sample_2').dataTable( {
	order: [],
	columnDefs: [ { orderable: false, targets: [0] } ]
	});
});
$("#select_all").click(function(){
    $('input:checkbox').prop('checked', this.checked);
});
</script>