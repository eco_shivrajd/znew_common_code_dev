<?php 
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
//echo "<pre>";print_r($_POST);die();
$order_id = $_POST['order_id'];
$order_type = 'Order Details';//Orderwise_Details   Order Product Details
$order_status = $_POST['order_status'];
$order_details1 = $orderObj->getOrderschnage1($order_status,$order_id);
$order_details=$order_details1[1];
$order_details_new = $orderObj->getShopOrdersbyorderid($order_details['order_no']);
//$product_variant = $orderObj->getSProductVariant($order_details['product_variant_id']);
//echo "<pre>";print_r($order_details);
//echo "<pre>";print_r($order_details_new);
//$product_variant = "product_variant";
?>
<div class="modal-header">
<button type="button" name="btnPrint" id="btnPrint" onclick="OrderDetailsPrint()" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
<?php if($order_details['lat']!="" && $order_details['long']!="" && $order_details['lat']!="0.0" && $order_details['long']!="0.0") { ?>
&nbsp;&nbsp;
<button type="button" name="btnPrint" id="btnPrint" onclick="showGoogleMap('<?=$order_details['lat'];?>','<?=$order_details['long'];?>')" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Location</button>
<?php } ?>

<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel"></h4>	   
</div>
<div class="modal-body" style="padding-bottom: 5px !important;" id="divOrderPrintArea">
<div class="row">
<div class="col-md-12">   
	<div class="portlet box blue-steel">
		<div class="portlet-title ">
			<div class="caption printHeading">
				<?=$order_type;?>
			</div>                          
		</div>
		<div class="portlet-body">
			<table class="table table-striped table-bordered table-hover" id="sample_2" width="100%">
			<tr>
				<td>Order From</td>
				<td><?=$order_details['order_by_name'];?></td>				
			</tr>
			<tr>
				<td>Order To</td>
				<td><?=$order_details['order_to_name'];?></td>				
			</tr>			
			<?php if($order_details['shop_name']!=''){ ?>
			<tr>
				<td>Shop Name</td>
				<td><?=$order_details['shop_name'];?></td>				
			</tr>
			<?php } ?>
			
			<?php if($order_details['region_name']!=''){ ?>
			<tr>
				<td>Taluka</td>
				<td><?=$order_details['region_name'];?></td>				
			</tr>
			<?php } ?>
				<?php if($order_details['subarea_name']!=''){ ?>
			<tr>
				<td>Subarea</td>
				<td><?=$order_details['subarea_name'];?></td>				
			</tr>
			<?php } ?>
			
			<tr>
				<td>Order Id</td>
				<td><?=$order_details['order_no'];?></td>				
			</tr>
			<tr>
				<td>Order Date</td>
				<td><?php echo date('d-m-Y h:i:s A', strtotime($order_details['order_date']));?></td>				
			</tr>
			<tr>				
				<td colspan="2">				
					<table class="table table-striped table-bordered table-hover"  width="100%">
						<tr>
							<th>Product</th>
							<th>Quantity</th>
							<th>Unit Price (₹)</th>
							<th>Price (₹)</th>
							<th>GST (₹)</th>							
						</tr>
						<?php $g_total_unitcost=0;$g_tax_total_cost=0;
						foreach($order_details_new as $key_od=>$value_od){ 
									$total_unitcost=($value_od['variantunit']*$value_od['product_unit_cost']);
									$g_total_unitcost=$g_total_unitcost+$total_unitcost;
									$g_tax_total_cost=$g_tax_total_cost+$value_od['unitcost'];
						?>
							<tr>							
								<td><?=$value_od['productname'].' '.$product_variant;?></td>	
								<td><?=$value_od['variantunit'].' ('.$value_od['product_variant_weight1'].''.$value_od['unit'].') ';?></td>
								<td><?=$value_od['product_unit_cost'];?></td>
								<td><?=$total_unitcost;?></td>
								<td><?=$value_od['unitcost'];?></td>							
							</tr>	
							<?php if(!empty($value_od['free_product_details'])){ 
								$arr=explode(",",$value_od['free_product_details']);
								$productname=	$orderObj->getProductname($arr[4]);
							?>
								<tr>							
									<td><?php foreach($productname as $keyprod=>$valueprod){echo $valueprod;}?></td>	
									<td><span style="float: left"><?=$arr[8].' ('.$arr[7].') ';?></span></td>
									<td><span style="float: left"><img src="../../assets/global/img/free-icon.png" title="Free Product"></span></td>									
									<td><span style="float: left"><?=0?></span></td>
									<td><span style="float: left"><?=0?></span></td>															
								</tr>
							<?php } ?>
						<?php } ?>
							<tr>							
								<td colspan="3"><b>Total</b></td>									
								<td><span style="float: left"><b><?=$g_total_unitcost;?></b></span></td>
								<td><span style="float: left"><b><?=$g_tax_total_cost;?></b></span></td>															
							</tr>
					</table>
				</td>				
			</tr>
			</table>
</div>
</div>
</div>
</div>
</div>