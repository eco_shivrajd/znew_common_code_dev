<?php 
   include ("../../includes/config.php");
   include "../includes/common.php";
   include "../includes/orderManage.php";
   $orderObj 	= 	new orderManage($con,$conmain);
   $added_by_userid = $_POST['added_by_userid'];
   $added_date = $_POST['added_date'];
   $added_username = $_POST['added_username'];
   $row = $orderObj->get_all_shop_list_added_on_by($added_by_userid,$added_date);
   //echo "<pre>";print_r($row);die();
   ?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
   <h4 class="modal-title" id="myModalLabel"></h4>
</div>
<div class="modal-body" style="padding-bottom: 5px !important;" id="divOrderPrintArea">
   <div class="row">
      <div class="col-md-12">
         <div class="portlet box blue-steel">
            <div class="portlet-title ">
               <div class="caption printHeading">
                  Shops Added By:<?php echo $added_username;?>
               </div>
            </div>
            <div class="portlet-body">
               <table class="table table-striped table-bordered table-hover" id="sample_2" width="100%">
                  <thead>
                     <tr>
                        <th data-filterable="false" data-sortable="true" data-direction="desc">Shop Added Date</th>
                        <th data-filterable="false" data-sortable="false" data-direction="de">Shop Name</th>
                        <th data-filterable="false" data-sortable="false" data-direction="desc">Address</th>
                        <th data-filterable="false" data-sortable="false" data-direction="de">Contact Person</th>
                        <th data-filterable="false" data-sortable="false" data-direction="de">Mobile</th>
                     </tr>
                  </thead>
                  <tbody>
					<?php
                        if (!empty($row)) {                        	
                        	foreach ($row as $key => $value) {	
					?>
                     <tr class="odd gradeX">
                        <td><?php echo date('d-m-Y h:i:s A', strtotime($value['added_date'])); ?></td>
                        <td><?=$value['name']; ?></td>
                        <td><?=$value['address']; ?></td>
                        <td><?=$value['contact_person']; ?></td>
                        <td><?=$value['mobile']; ?></td>                       
                     </tr>
                     <?php } 
                        } ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>