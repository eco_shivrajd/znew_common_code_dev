<?php 
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include_once ("../../includes/config.php");

if(isset($_POST["export_data"])) 
{
	$sql_query = "SELECT A.id,A.name AS shopname,C.name AS cityname,S.name AS statename,A.mobile,A.address from tbl_shops AS A LEFT JOIN tbl_city C ON C.id = A.city LEFT JOIN tbl_state S ON S.id = A.state where A.isdeleted !='1'";
$resultset = mysqli_query($con, $sql_query);
$developer_records = array();
while( $rows = mysqli_fetch_assoc($resultset) ) 
{
	$developer_records[] = $rows;
}

		$filename = "phpzag_data_export_".date('Ymd') . ".xls";			
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=\"$filename\"");	
	$show_coloumn = false;
	if(!empty($developer_records)) {
	  foreach($developer_records as $record) {
		if(!$show_coloumn) {
		  // display field/column names in first row
		  echo implode("\t", array_keys($record)) . "\n";
		  $show_coloumn = true;
		}
		echo implode("\t", array_values($record)) . "\n";
	  }
	}

	exit;
}
?>