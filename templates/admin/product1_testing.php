<!-- BEGIN HEADER -->
<?php
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include "../includes/header.php";

include "../includes/userManage.php";
include ("../includes/productManage.php");

$userObj = new userManager($con, $conmain);
$objProduct = new productManage($con,$conmain);
?>
<!-- END HEADER -->
<?php
$sqlvariant1 = "SELECT name FROM tbl_variant WHERE id='40'";
$resultprd1 = mysqli_query($con, $sqlvariant1);
$rowvariant1 = mysqli_fetch_array($resultprd1);
//////////////////////////////////////////////////////////////////////
$sqlvariant2 = "SELECT name FROM tbl_variant WHERE id='41'";
$resultprd2 = mysqli_query($con, $sqlvariant2);
$rowvariant2 = mysqli_fetch_array($resultprd2);
/////////////////////////////////////////////////////////////////////
$commonObj = new commonManage($con, $conmain);
$commonObjctype = $commonObj->log_get_commonclienttype($con, $conmain);
if (isset($_POST['submit'])) {    
	//echo "<pre>";print_r($_POST);die();
    $id = $_GET['id'];
    $catid = $_POST['catid'];
    $productname = fnEncodeString($_POST['productname']);
    $sqlprvariant = "SELECT variant_cnt FROM `tbl_product_variant` WHERE `productid`='$id' ORDER BY `id` DESC LIMIT 1";
    $result_prvariant = mysqli_query($con, $sqlprvariant);
    $row_prvariant = mysqli_fetch_array($result_prvariant);
   // echo "<pre>";print_r($row_prvariant);
	
	//die();
	$countuserlevel = $_POST['countuserlevel'];
	
	
    for ($i = 1; $i <= $row_prvariant['variant_cnt']; $i++) {
        //$size	=fnEncodeString($_POST['size_'.$i]).",".fnEncodeString($_POST['cmbUnit_'.$i]);
        //$weight=fnEncodeString($_POST['weight_'.$i]).",".fnEncodeString($_POST['cmbweight_'.$i]);
		$start_pr=( ($i-1) * $countuserlevel)+1;
		$end_pr = $i*$countuserlevel;
		$price_level=array();$k=1;	
		for($j=$start_pr;$j<=$end_pr;$j++){			
			$string_value="price_level".$j."";
			$price_level[$k] = $_POST[$string_value];
			$k++;
		}
		//echo "<pre>";print_r($price_level);die();
        $size = fnEncodeString($_POST['size_' . $i]);
        $weight = fnEncodeString($_POST['weight_' . $i]);

        $variant1_unit_id = fnEncodeString($_POST['cmbUnit_' . $i]);
        $variant2_unit_id = fnEncodeString($_POST['cmbweight_' . $i]);

        //create folder for upload product image
        $directoryName = 'upload/' . COMPANYNM . '_upload/';
        //Check if the directory already exists.
        if (!is_dir($directoryName)) {
            //Directory does not exist, so lets create it.
            mkdir($directoryName, 0755, true);
        }
        $file = $_FILES["productimage_" . $i]["name"];
        //$folder = "upload/";
        $folder = "upload/" . COMPANYNM . "_upload/";
        $upload_image = $folder . basename($_FILES["productimage_" . $i]["name"]);
        $str = move_uploaded_file($_FILES["productimage_" . $i]["tmp_name"], $upload_image);

		
        $p_hsn = $_POST['producthsn_' . $i];
        $cgst = $_POST['cgst_' . $i];
        $sgst = $_POST['sgst_' . $i];
		
		$values = ''; 
		
		foreach($price_level as $key=>$value){
			$values.= ", `price_level".$key."`= '".$value."'";							
		}
		
       
        //print"<pre>";
        //print_r($_FILES['productimage_'.$i]); exit;
        $barcode_file = $_FILES['productbarcodeimage_' . $i]['name'];
        $upload_barcode_image = $folder . basename($_FILES['productbarcodeimage_' . $i]["name"]);
        $str_barcode = move_uploaded_file($_FILES["productbarcodeimage_" . $i]["tmp_name"], $upload_barcode_image);
        $value = '';
        if ($file != '') {
            if ($barcode_file != '')
                $value = ", productbarcodeimage='" . $barcode_file . "'";

              $sql_prod_variant = "UPDATE `tbl_product_variant` 
			SET variant_1='" . $size . "',variant_2='" . $weight . "',price='" . $_POST['price_' . $i] . "',
			productimage='$file' " . $value . ", producthsn = '" . $p_hsn . "', cgst = '" . $cgst . "',variant1_unit_id = '" . $variant1_unit_id . "',variant2_unit_id = '" . $variant2_unit_id . "', 
			sgst = '" . $sgst . "' $values
			WHERE `productid`='$id' AND variant_cnt='" . $i . "'";

            $sql_product_update = mysqli_query($con, $sql_prod_variant);

            $commonObj->log_update_record('tbl_product_variant', $id, $sql_prod_variant);
        }else {
            if ($barcode_file != '')
                $value = ", productbarcodeimage='" . $barcode_file . "'";

               $sql_prod_variant = "UPDATE `tbl_product_variant` 
			 SET variant_1='" . $size . "',variant_2='" . $weight . "',price='" . $_POST['price_' . $i] . "' 
			 " . $value . ", producthsn = '" . $p_hsn . "', cgst = '" . $cgst . "',variant1_unit_id = '" . $variant1_unit_id . "',variant2_unit_id = '" . $variant2_unit_id . "', 
			 sgst = '" . $sgst . "' $values
			 WHERE `productid`='$id' AND variant_cnt='" . $i . "'";
             
            $sql_product_update = mysqli_query($con, $sql_prod_variant);

            $commonObj->log_update_record('tbl_product_variant', $id, $sql_prod_variant);
        }
    }
	//exit();
	
    $sql_product_check = mysqli_query($con, "select id from `tbl_product` where productname='$productname'");

    $sql_prod = "UPDATE `tbl_product` SET catid = '$catid' , productname= '$productname'  WHERE id = '$id'";
    $sql_product_update = mysqli_query($con, $sql_prod);

    $commonObj->log_update_record('tbl_product', $id, $sql_prod);
    $variant1 = fnEncodeString($_POST['variant1']);
    if ($variant1 != "")
        $var1 = implode(',', $variant1);

    echo '<script>alert("Product updated successfully.");location.href="product.php";</script>';
}
?>
<?php
$result_userlevel = $userObj->getAllLocalUserLevels();
$counter = 0;
$array_userlevel = array();
while ($row_userlevel = mysqli_fetch_array($result_userlevel)) {

	$array_userlevel[$counter]['id'] = $row_userlevel['id'];
	$array_userlevel[$counter]['level'] = $row_userlevel['level'];
	$array_userlevel[$counter]['usertype'] = $row_userlevel['usertype'];
	$array_userlevel[$counter]['margin'] = $row_userlevel['margin'];
	$counter++;
}
?>

<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
<?php
$activeMainMenu = "ManageProducts";
$activeMenu = "Product";
include "../includes/sidebar.php";
?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

                <!-- /.modal -->

                <h3 class="page-title">
                    Product
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="product.php">Product</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Edit Product</a>
                        </li>
                    </ul>

                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- Begin: life time stats -->
                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">
                                    Edit Product
                                </div>
                            </div>
                            <div class="portlet-body">
                                <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
                                <form class="form-horizontal" data-parsley-validate="" role="form" method="post" action="" enctype="multipart/form-data">
<?php
$product_id = $_GET['id'];
$result_prd = $objProduct->getSingleProduct($product_id);
//echo "<pre>";print_r($result_prd);		
if (count($result_prd) > 0) {
    ?>

                                        <div class="form-group">
                                            <label class="col-md-3">Category:<span class="mandatory">*</span></label>

                                            <div class="col-md-4">
                                                <select name="catid" data-parsley-trigger="change" data-parsley-required="#true" data-parsley-required-message="Please select Category" class="form-control">
                                                    <option  selected disabled>-Select-</option>
                                        <?php
										$result_brand_cat = $objProduct->get_brand_category();                                     
                                        foreach ($result_brand_cat as $k=>$row) {
                                            $id = $row['id'];
                                        ?>
										<option value='<?php echo $id;?>' <?php if ($result_prd['catid'] == $id) {echo "selected";}?> >
										<?php echo fnStringToHTML($row['name']) . "-" . fnStringToHTML($row['categorynm']); ?>
										</option>
										<?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- /.form-group -->

                                        <div class="form-group">
                                            <label class="col-md-3">Product Name:<span class="mandatory">*</span></label>

                                            <div class="col-md-4">
                                                <input type="text" name="productname" value="<?php echo fnStringToHTML($result_prd['productname']); ?>" placeholder="Enter Product Name" data-parsley-trigger="change" data-parsley-required="#true" data-parsley-required-message="Please enter product name"
                                                       data-parsley-maxlength="50" data-parsley-maxlength-message="Only 50 characters are allowed" class="form-control">
                                            </div>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label class="col-md-3">Price Type:</label>
                                            <div class="col-md-4">
                                                <input type="radio"  name="priceTypess" id="priceTypessmrp" value="1" <?php if ($result_prd['mrp_or_margin_flag'] == '1') echo "checked";else echo "disabled"; ?> > Price By MRP &nbsp;&nbsp;
                                                <input type="radio"  name="priceTypess" id="priceTypessmargin" value="2" <?php if ($result_prd['mrp_or_margin_flag'] == '2') echo "checked";else echo "disabled"; ?> > Price By Margin
                                            </div>
                                        </div>
										<input type="hidden" name="countuserlevel" id="countuserlevel" value="<?php echo $counter;?>">
                                        <!-- /.form-group -->
    <?php
    $product_id = $_GET['id'];
    $sqlprd = "SELECT * from `tbl_product_variant` where productid = '$product_id' ";
    $resultprd = mysqli_query($con, $sqlprd);
	$counter_level=1;
    while ($rowprd = mysqli_fetch_assoc($resultprd)) {
		//echo "<pre>";print_r($rowprd);
        $exp_variant1 = $rowprd['variant_1'];
        //$imp_variant1 = explode(',', $exp_variant1);
        $exp_variant2 = $rowprd['variant_2'];
       // $imp_variant2 = explode(',', $exp_variant2);
        $prod_var_img = $rowprd['productimage'];
        $variant1_unit_id = $rowprd['variant1_unit_id'];
        $variant2_unit_id = $rowprd['variant2_unit_id'];
        ?>

                                            <div class="form-group" <?php if ($commonObjctype == '1') {echo 'style="display: none;"';} ?>>
                                                <label class="col-md-3"><?php echo fnStringToHTML($rowvariant1['name']); ?>:</label>
                                                <div class="col-md-4 nopadl">
                                                    <div class="col-md-4">
                                                        <input type="text" name="size_<?php echo $rowprd['variant_cnt']; ?>" 
														id="numunits1" 
														value="<?php echo fnStringToHTML($exp_variant1); ?>" 
														class="form-control">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <select name="cmbUnit_<?php echo $rowprd['variant_cnt']; ?>" id="units_variant_id1" class="form-control">
        <?php
         $variantid = 40;
		 $varient_units = $objProduct->get_varient_units($variantid); 
			//echo "<pre>";print_r($varient_units);
        foreach ($varient_units as $key_var_unit=>$value_var_units) {
            $unitname = $value_var_units['unitname'];
            $unit = $value_var_units['id'];
        ?>
		<option value='<?php echo $value_var_units['id'];?>' 
		<?php  if ($variant1_unit_id == $unit) { echo "SELECTED"; } ?>><?php echo fnStringToHTML($unitname);?>  
		</option>
	<?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <!-- /.form-group -->

                                            <div class="form-group">
                                                <label class="col-md-3"><?php echo fnStringToHTML($rowvariant2['name']); ?>:</label>

                                                <div class="col-md-4 nopadl">
                                                    <div class="col-md-4 " <?php if ($commonObjctype == '1') {
                                                        echo 'style="display: none;"';
                                                    } ?>>
                                                        <input type="text" name="weight_<?php echo $rowprd['variant_cnt']; ?>" id="numunits2" 
														value="<?php echo fnStringToHTML($variant2_unit_id); ?>" 
														class="form-control">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <select name="cmbweight_<?php echo $rowprd['variant_cnt']; ?>" id="units_variant_id2" class="form-control">
       <?php
         $variantid = 41;
		 $varient_units = $objProduct->get_varient_units($variantid);       
        foreach ($varient_units as $key_var_unit=>$value_var_units) {
            $unitname = $value_var_units['unitname'];
            $unit = $value_var_units['id'];
        ?>
		<option value='<?php echo $value_var_units['id'];?>' 
		<?php  if ($variant1_unit_id == $unit) { echo "SELECTED"; } ?>><?php echo fnStringToHTML($unitname);?>  
		</option>
	<?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
											
                                            <div class="form-group">
                                                <label class="col-md-3">Price:</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="price_<?php echo $rowprd['variant_cnt']; ?>" 
													id="price_1"
													onkeyup="updatemargin();"
													placeholder="Price" 
													value="<?php echo fnStringToHTML($rowprd['price']); ?>" 
													class="form-control price">
                                                </div>
                                            </div>

        <?php	
		
        foreach ($array_userlevel as $key_level=>$value_level) {
			$temp_level= $key_level+1;
			$price_levelas='price_level'.$temp_level.'';
            ?>
											<!-- /.form-group -->
											<div class="form-group">
												<label class="col-md-3">
												<?php if ($result_prd['mrp_or_margin_flag'] == '2'){echo "Margin";}?> For <?php echo  $value_level['usertype'];?>:</label>
												<div class="col-md-4">
													<input type="text" name="price_level<?php echo $counter_level; ?>" id="price_ss_1" 
													placeholder="price ss" <?php if ($result_prd['mrp_or_margin_flag'] == '2'){echo "readonly";}?>
													value="<?php echo $rowprd[$price_levelas]; ?>" class="form-control" 
													data-parsley-trigger="change" data-parsley-pattern="^(?!\s)[0-9-!@#$%^&*+_=><,./:' ]*$">
												</div>
											</div>
												
            <?php $counter_level++;} ?>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label class="col-md-3">CGST (%):</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="cgst_<?php echo $rowprd['variant_cnt']; ?>" id="cgst[]" placeholder="CGST" value="<?= $rowprd['cgst']; ?>" class="form-control" data-parsley-trigger="change" data-parsley-pattern="^(?!\s)[0-9-!@#$%^&*+_=><,./:' ]*$">
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label class="col-md-3">SGST (%):</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="sgst_<?php echo $rowprd['variant_cnt']; ?>" id="sgst[]" placeholder="SGST" value="<?= $rowprd['sgst']; ?>" class="form-control" data-parsley-trigger="change" data-parsley-pattern="^(?!\s)[0-9-!@#$%^&*+_=><,./:' ]*$">
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label class="col-md-3">Product Image:</label>

                                                <div class="col-md-4">
        <?php if ($prod_var_img != "") { ?>
                                                        <img src="upload/<?php echo COMPANYNM; ?>_upload/<?php echo $prod_var_img; ?>" alt="<?php echo $prod_var_img ?>" class="img-responsive" />
        <?php } ?>
                                                    </br>
                                                    <input type="file" id="productimage" data-parsley-trigger="change" data-parsley-fileextension='png,jpg,jpeg' data-parsley-max-file-size="1000" name="productimage_<?php echo $rowprd['variant_cnt']; ?>" multiple="multiple">
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label class="col-md-3">Product Barcode Image:</label>

                                                <div class="col-md-4">
                                                    <?php if ($rowprd['productbarcodeimage'] != "") { ?>
                                                        <img src="upload/<?php echo COMPANYNM; ?>_upload/<?= $rowprd['productbarcodeimage']; ?>" alt="<?= $rowprd['productbarcodeimage']; ?>" class="img-responsive" />
                                                    <?php } ?>
                                                    </br>
                                                    <input type="file" id="productbarcodeimage" data-parsley-trigger="change" data-parsley-fileextension='png,jpeg,jpg' data-parsley-max-file-size="1000" name="productbarcodeimage_<?php echo $rowprd['variant_cnt']; ?>" multiple="multiple">
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label class="col-md-3">Product HSN:</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="producthsn_<?php echo $rowprd['variant_cnt']; ?>" placeholder="Enter Product HSN" data-parsley-trigger="change" data-parsley-maxlength="50" data-parsley-maxlength-message="Only 50 characters are allowed" class="form-control" value="<?= $rowprd['producthsn']; ?>">
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                                <?php }
                                            } ?>

                                    <hr/>
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-3">
                                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                            <a href="product.php" class="btn btn-primary">Cancel</a>
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                </form>


                            </div>
                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
<?php include "../includes/footer.php" ?>
    <!-- END FOOTER -->
    <script>
        $(document).ready(function () {
            window.ParsleyValidator
                    .addValidator('fileextension', function (value, requirement) {
                        // the value contains the file path, so we can pop the extension
                        var arrRequirement = requirement.split(',');
                        var fileExtension = value.split('.').pop();
                        var result = jQuery.inArray(fileExtension, arrRequirement);

                        if (result == -1)
                            return false;
                        else
                            return true;
                    }, 32)
                    .addMessage('en', 'fileextension', 'Only png or jpg image are allowed');
            window.Parsley.addValidator('maxFileSize', {
                validateString: function (_value, maxSize, parsleyInstance) {
                    var files = parsleyInstance.$element[0].files;
                    return files.length != 1 || files[0].size <= maxSize * 1024;
                },
                requirementType: 'integer',
                messages: {
                    en: 'Image should not be larger than %s Kb',
                }
            });

            $("input[name^='price_']").keyup(function (e) {
                if (this.value != "") {
                    var arrtmp = this.value.split(".");
                    if (arrtmp.length > 1) {
                        var strTmp = arrtmp[1];
                        if (strTmp.length > 2) {
                            this.value = this.value.substring(0, this.value.length - 1);
                        }
                    }
                }
            });
        });
        function updatemargin(){
				//alert("fdgdfgjh");
		var priceTypessvalue=$("input[name='priceTypess']:checked").val();			
		if(priceTypessvalue=='2'){
			//alert("222");
			var countlevel=$('#countuserlevel').val();		
			var userlevel_info=<?php echo json_encode($array_userlevel);?>;		
			$('input[id^=price_1]').each(function( index ) {
					//alert(index);
				var values = $("input[id='price_1']").map(function(){return $(this).val();}).get();	
					console.log(values);
				var fields = $('input[id^=price_ss_1]');
					console.log(fields);
					var temploop=0;				
				for(var j = index*countlevel; j < (index*countlevel)+countlevel; j++){
					if(temploop<countlevel)
					var marginper= parseFloat(userlevel_info[temploop].margin);	
					else break;
					var mrppricevalue=parseFloat(values[index]);				
					var newvaue=mrppricevalue-parseFloat(mrppricevalue*(marginper/100));
					$(fields[j]).val(newvaue);
					temploop++;
				} 
			});	
		}
				
	}
    </script>
</body>
<!-- END BODY -->

</html>