<!-- BEGIN HEADER -->
<?php
//phpinfo();die();
include "../includes/grid_header.php";
include "../includes/userManage.php";

include "../includes/productManage.php";
include "../includes/orderManage.php";
$userObj = new userManager($con, $conmain);

$prodObj = new productManage($con, $conmain);
$orderObj = new orderManage($con, $conmain);


?>
<!-- END HEADER -->
<style>
    .form-horizontal .control-label {
        text-align: left;
    }
	small, .small {
	 font-size: 77%;
	} 
</style>

</head>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLEAPIKEY; ?>&callback=initMap" type="text/javascript"></script>
<!-- END HEAD -->
<body class="page-header-fixed page-quick-sidebar-over-content ">

    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "Orders";
        $activeMenu = "Ordersall";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->

        <div class="page-content-wrapper">
            <div class="page-content">			
                <h3 class="page-title">
                    Manage Orders 
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="#">Manage Orders </a>
                        </li>
                    </ul>				
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">
                                    Manage Orders  
                                </div>
                                <div class="clearfix"></div>
                            </div>						
                            <div class="portlet-body">

                                <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">
                                    <div class="form-group">
                                        <label class="col-md-3">Order Status:</label>
                                        <div class="col-md-4">
                                            <select name="order_status" id="order_status"  data-parsley-trigger="change" class="form-control">
                                                <option value="1" selected>Received </option> 
                                                <option value="2">Assigned for Delivery</option> 			
                                                <option value="4">Delivered</option> 
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->	

                                    <div class="form-group" id="divDaily">
                                        <label class="col-md-3">Select Order Date:</label>
                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <input type="text" class="form-control  date date-picker1" data-date="<?php echo date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years" name="frmdate" id="frmdate" value="">
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                            <!-- /input-group -->								 
                                        </div>
                                    </div><!-- /.form-group -->	
								<?php $session_user_id=$_SESSION[SESSION_PREFIX . "user_id"];?>
                                        <!-- /.form-group -->

                                   
                                    <div class="form-group">
                                        <label class="col-md-3">State:</label>
                                        <div class="col-md-4">
                                            <select name="dropdownState" id="dropdownState"              
                                                    data-parsley-trigger="change"				
                                                    data-parsley-required="#true" 
                                                    data-parsley-required-message="Please select state"
                                                    class="form-control" onChange="fnShowCity(this.value)">
                                                <option selected disabled>-Select-</option>
                                                <?php
                                                $sql = "SELECT id,name FROM tbl_state where country_id=101 order by name";
                                                $result = mysqli_query($con, $sql);
                                                while ($row = mysqli_fetch_array($result)) {
                                                    $cat_id = $row['id'];
                                                    echo "<option value='$cat_id'>" . $row['name'] . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>			
                                    <div class="form-group" id="city_div" style="display:none;">
                                        <label class="col-md-3">City:</label>
                                        <div class="col-md-4" id="div_select_city">
                                            <select name="dropdownCity" id="dropdownCity" data-parsley-trigger="change" class="form-control">
                                                <option selected value="">-Select-</option>										
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group" id="area_div" style="display:none;">
                                        <label class="col-md-3">Region:</label>
                                        <div class="col-md-4" id="div_select_area">
                                            <select name="area" id="area" data-parsley-trigger="change" class="form-control">
                                                <option selected value="">-Select-</option>									
                                            </select>
                                        </div>
                                    </div>									
                                    
                                    <div class="form-group">
                                        <label class="col-md-3">Category:</label>
                                        <div class="col-md-4" id="divCategoryDropDown">
                                            <?php $cat_result = $prodObj->getAllCategory(); ?>
                                            <select name="dropdownCategory" id="dropdownCategory" class="form-control" onchange="fnShowProducts(this)">
                                                <option value="">-Select-</option>
                                                <?php while ($row_cat = mysqli_fetch_assoc($cat_result)) {
                                                    ?>									
                                                    <option value="<?= $row_cat['id']; ?>"><?= $row_cat['categorynm']; ?></option>
                                                <?php } ?>								
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label class="col-md-3">Product:</label>
                                        <div class="col-md-4" id="divProductdropdown">
                                            <?php $prod_result = $prodObj->getAllProducts(); ?>
                                            <select name="dropdownProducts" id="dropdownProducts" class="form-control">
                                                <option value="">-Select-</option>
                                                <?php while ($row_prod = mysqli_fetch_assoc($prod_result)) {
                                                    ?>									
                                                    <option value="<?= $row_prod['id']; ?>"><?= $row_prod['productname']; ?></option>
                                                <?php } ?>	
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-3">
                                            <button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>									
                                            <button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnReset();">Reset</button>
                                        </div>
                                    </div><!-- /.form-group -->
                                </form>	
                               

                                <form class="form-horizontal" role="form" name="form" method="post" action="new_order_from_stockist.php">	
								<div>
                                   								
                                    <div id="order_list">
                                        <?php
                                       $order_status = 1; 					
                                        $orders = $orderObj->getAllOrdersForAcc($order_status); 		
											//echo "<pre>";print_r($orders);//die();
                                        $order_count = count($orders);
                                        ?>
                                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                            <thead>
                                                <tr id="main_th">
                                                    <th>
                                                        Region
                                                    </th>
                                                    <th>
                                                        Order From
                                                    </th>
                                                    <th>
                                                        Order To
                                                    </th>
                                                    <th>
                                                        Order Id</br>Order Date 
                                                    </th>								

                                                    <th>
                                                        Product Name			
                                                    </th>	
                                                    <th>
                                                        Quantity
                                                    </th>
                                                    <th>
                                                        Total Price (₹)
                                                    </th>
                                                    <th>
                                                        Price(₹)<br>(GST+Discount)
                                                    </th>
                                                    <th>
                                                        Action  
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($orders as $key => $value) {
                                                    $order_totalcost = 0;
                                                    $order_totalgstcost = 0;
													$product_name = "";
													$prod_qnty = "";
													$totalcost = "";
													$gstcost = "";
                                                    $oid = $value['oid'];
                                                    $orderdetails = $orderObj->getOrdersDetailschnage($value['oid']);
                                                    //echo "<pre>";print_r($orderdetails);die();
                                                    //$odid=$orderdetails['order_details'][0]['odid'];
                                                   
                                                    if (count($orderdetails['order_details']) == 1) {
                                                        $product_name = $orderdetails['order_details'][0]['cat_name'] . "-" . $orderdetails['order_details'][0]['product_name'] . "<br>";
                                                        $prod_qnty = '<a onclick="showOrderDetails(\'' . $orderdetails['order_details'][0]['odid'] . '\',\'Order Details\')" title="Order Details">' . $orderdetails['order_details'][0]['product_quantity'];
                                                        if (!empty($orderdetails['order_details'][0]['product_variant_weight1'])) {
                                                            $prod_qnty .= "(" . $orderdetails['order_details'][0]['product_variant_weight1'] . "-" . $orderdetails['order_details'][0]['product_variant_unit1'] . ")";
                                                        }
                                                        if (!empty($orderdetails['order_details'][0]['product_variant_weight2'])) {
                                                            $prod_qnty .= "(" . $$orderdetails['order_details'][0]['product_variant_weight2'] . "-" . $$orderdetails['order_details'][0]['product_variant_unit2'] . ")";
                                                        }
                                                        $prod_qnty .= "</a>"; //od.campaign_applied, od.campaign_type
                                                        $campaign_applied = $orderdetails['order_details'][0]['campaign_applied'];
                                                        $campaign_type = $orderdetails['order_details'][0]['campaign_type'];
                                                        if ($campaign_applied == 0 && $campaign_type == 'free_product') {
                                                            $prod_qnty .= "<span style='float: left'><img src='../../assets/global/img/free-icon.png' title='Free Product'></span>";
                                                        }
                                                        $prod_qnty .= "<br>";
                                                        $totalcost = $orderdetails['order_details'][0]['product_total_cost'];
                                                        if ($orderdetails['order_details'][0]['discounted_totalcost'] != 0 && $orderdetails['order_details'][0]['discounted_totalcost'] != '') {
                                                            $totalcost .= "(<font color='green'>" . $orderdetails['order_details'][0]['discounted_totalcost'] . "</font>)";
                                                        }
                                                        $totalcost .= "<br>";
														 $order_totalcost= $orderdetails['order_details'][0]['product_total_cost'];

                                                        $gstcost = $orderdetails['order_details'][0]['p_cost_cgst_sgst'] . "<br>";
														$order_totalgstcost = $orderdetails['order_details'][0]['p_cost_cgst_sgst'];
                                                    } else {
														
                                                        //$tempproductname=$orderdetails['order_details'][0]['cat_name']."-".$orderdetails['order_details'][0]['product_name'];
                                                        foreach ($orderdetails['order_details'] as $key1 => $value1) {
                                                            $product_name .= $value1['cat_name'] . "-" . $value1['product_name'] . "<br>";
                                                            $prod_qnty .= '<a onclick="showOrderDetails(\'' . $value1['odid'] . '\',\'Order Details\')" title="Order Details">' . $value1['product_quantity'];
                                                            if (!empty($value1['product_variant_weight1'])) {
                                                                $prod_qnty .= "(" . $value1['product_variant_weight1'] . "-" . $value1['product_variant_unit1'] . ")";
                                                            }
                                                            if (!empty($value1['product_variant_weight2'])) {
                                                                $prod_qnty .= "(" . $value1['product_variant_weight2'] . "-" . $value1['product_variant_unit2'] . ")";
                                                            }
                                                            $prod_qnty .= "</a>"; //free_product_details
                                                            $campaign_applied = $value1['campaign_applied'];
                                                            $campaign_type = $value1['campaign_type'];
                                                            if ($campaign_applied == 0 && $campaign_type == 'free_product') {
                                                                $prod_qnty .= "<span style='float: left'><img src='../../assets/global/img/free-icon.png' title='Free Product'></span>";
                                                            }
                                                            $prod_qnty .= "<br>";
                                                            $totalcost .= $value1['product_total_cost']; //discount_amount//discounted_totalcost
                                                            if ($value1['discounted_totalcost'] != 0 && $value1['discounted_totalcost'] != '') {
                                                                $totalcost .= "(<font color='green'>" . $value1['discounted_totalcost'] . "</font>)";
                                                            }
                                                            $totalcost .= "<br>";
                                                            $order_totalcost = $order_totalcost + $value1['product_total_cost'];

                                                            $gstcost .= $value1['p_cost_cgst_sgst'] . "<br>";
                                                            $order_totalgstcost = $order_totalgstcost + $value1['p_cost_cgst_sgst'];
                                                        }
                                                    }
                                                    ?>
                                                    <tr class="odd gradeX">              
                                                        
                                                        <td ><?php if(!empty($value["region_name"])){echo $value["region_name"];}else{echo $value["region_name1"];} ?></td>
                                                        <td ><?php echo $value["order_by_name"]."-".$value["shop_name"]; ?></td>					
                                                        <td ><?php echo $value["order_to_name"]; ?></td>
                                                        <td ><font size="1.4"><?php echo $value["order_no"] ?></font><br><?php echo date('d-m-Y H:i:s', strtotime($value["order_date"])); ?></td>										
                                                        <td><?php echo $product_name; ?></td>
                                                        <td ><?php echo $prod_qnty; ?></td>
                                                        <td align="right"><?php echo $totalcost;
                                                            echo "<b>" . $order_totalcost . "</b>"; ?></td>
                                                        <td align="right"><?php echo $gstcost;
                                                            echo "<b>" . $order_totalgstcost . "</b>"; ?></td>
                        <td>
                            <a onclick='showInvoice(1,<?=json_encode($oid);?>)'  title="View Invoice">View Invoice</a>
                        </td>
                                                        <!--". $row["orderid"]. "-->
                                                    </tr>
<?php } ?>

                                            </tbody>
                                        </table>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>

    <!-- END CONTAINER -->
    <div class="modal fade" id="view_invoice" role="dialog">
        <div class="modal-dialog" style="width: 980px !important;">    
            <!-- Modal content-->
            <div class="modal-content" id="view_invoice_content">      
            </div>      
        </div>
    </div>
    <div class="modal fade" id="order_details" role="dialog">
        <div class="modal-dialog" style="width: 880px !important;">
            <!-- Modal content-->
            <div class="modal-content" id="order_details_content">
            </div>
        </div>
    </div>


    <!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php" ?>
    <!-- END FOOTER -->

    <script>			
        $(document).ready(function () { 		
			
            $("#select_th").removeAttr("class");
            $("#main_th th").removeAttr("class");
            if ($.fn.dataTable.isDataTable('#sample_2')) {
                table = $('#sample_2').DataTable();
                table.destroy();
                table = $('#sample_2').DataTable({
                    "aaSorting": [],
                });
            }
			
				//$(':input[type="submit"]').prop('disabled', false);
            
			 
        });

        $('.date-picker1').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            endDate: "<?php echo date('d-m-Y'); ?>",
            autoclose: true
        });
        
		 $('.date-picker2').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "auto",
                startDate: 'd',
                endDate: '+3m',
                autoclose: true
            });
        $('.date-picker3').datepicker({
            rtl: Metronic.isRTL(),
			orientation: "auto",
			startDate: 'd',
			endDate: '+3m',
            autoclose: true
        });
        function fnReset() {
            location.reload();
        }

        function fnShowCity(id_value) {
            $("#city_div").show();
            $("#area").html('<option value="">-Select-</option>');
            $("#subarea").html('<option value="">-Select-</option>');
            var url = "getCityDropDown.php?cat_id=" + id_value + "&select_name_id=city&mandatory=mandatory";
            CallAJAX(url, "div_select_city");
        }
        function FnGetSuburbDropDown(id) {
            $("#area_div").show();
            $("#subarea").html('<option value="">-Select-</option>');
            var url = "getSuburDropdown.php?cityId=" + id.value + "&select_name_id=area&function_name=FnGetSubareaDropDown&mandatory=mandatory";
            CallAJAX(url, "div_select_area");
        }

        function FnGetShopsDropdown(id) {
            $("#shop_div").show();
            $("#divShopdropdown").html('<option value="">-Select-</option>');
            var param = "";
            var state_id = $("#dropdownState").val();
            var city_id = $("#city").val();
            var suburb_id = $("#area").val();
            if (state_id != '')
                param = param + "&state_id=" + state_id;
            if (city_id != '')
                param = param + "&city_id=" + city_id;
            if (suburb_id != '') {
                if (suburb_id != undefined) {
                    param = param + "&suburb_id=" + suburb_id;
                }
            }
            if (id != '') {
                if (id != undefined)
                    param = param + "&suburb_id=" + id.value;
            }

            var url = "getShopDropdownByAddress.php?param=param" + param;
            CallAJAX(url, "divShopdropdown");
        }
        function fnShowProducts(id) {
            var url = "getProductDropdown.php?cat_id=" + id.value;
            CallAJAX(url, "divProductdropdown");
        }
       
      
        function ShowReport() {
		  var order_status = $('#order_status').val();		
          
			var user_type='<?=$user_type?>';
            //alert(order_status);
            if (order_status == '0' || order_status == '2'||order_status == '4' ||order_status == '10' ) {
                $("#statusbtn").hide();
				$("#statusDbtn").hide();
				$("#statusbtnplacedorder").hide();
            } if(order_status == '1' || order_status == '11'){
				$("#statusbtn").show();
				$("#statusbtn").attr("disabled", false);
				$("#statusDbtn").hide();
				if(user_type!='Admin'){
					$("#statusbtnplacedorder").show();
					$("#statusbtnplacedorder").attr("disabled", false);
				}else{$("#statusbtnplacedorder").hide();}				
			}/*
			if(order_status == '11'){
				$("#statusbtn").hide();
				$("#statusDbtn").show();
				if(user_type!='Admin'){
					$("#statusbtnplacedorder").show();
				}else{$("#statusbtnplacedorder").hide();}		
			}
			*/  
			

            var url = "ajax_show_acc_orders.php";

            var data = $('#frmsearch').serialize();

            jQuery.ajax({
                url: url,
                method: 'POST',
                data: data,
                async: false
            }).done(function (response) {
                $('#order_list').html(response);
                var table = $('#sample_2').dataTable();
                table.fnFilter('');
                $("#select_th").removeAttr("class");
            }).fail(function () { });
            return false;
        }
        function showInvoice(order_status, id) {
            var url = "invoice2.php";
            jQuery.ajax({
                url: url,
                method: 'POST',
                data: 'order_status=' + order_status + '&order_id=' + id,
                async: false
            }).done(function (response) {
                $('#view_invoice_content').html(response);
                $('#view_invoice').modal('show');
            }).fail(function () { });
            return false;
        }
        function showOrderDetails(id, order_type) {
            var url = "order_details_popup.php";
            jQuery.ajax({
                url: url,
                method: 'POST',
                data: 'order_details_id=' + id + '&order_type=' + order_type,
                async: false
            }).done(function (response) {
                $('#order_details_content').html(response);
                $('#order_details').modal('show');
            }).fail(function () { });
            return false;
        }
        function OrderDetailsPrint() {
            var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
            var divContents = '<style>\body {\
                    font-size: 12px;}\
            th {text-align: left;}\
            .printHeading { line-height: 18px;  padding: 10px 0;  font-size: 18px; }\
            table { border-collapse: collapse;  \
                    font-size: 12px; }\
            table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
            body { font-family: "Open Sans", sans-serif;\
            background-color:#fff;\
            font-size: 15px;\
            direction: ltr;}</style>' + $("#divOrderPrintArea").html();
            if (isIE == true) {
                var printWindow = window.open('', '', 'height=400,width=800');
                printWindow.document.write(divContents);
                printWindow.focus();
                printWindow.document.execCommand("print", false, null);
            } else {
                $('<iframe>', {
                    name: 'myiframe',
                    class: 'printFrame'
                }).appendTo('body').contents().find('body').html(divContents);
                window.frames['myiframe'].focus();
                window.frames['myiframe'].print();
                setTimeout(
                        function ()
                        {
                            $(".printFrame").remove();
                        }, 1000);
            }
        }
        function takeprint11() {
            var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
            var divContents = '<style>\body {\
                    font-size: 12px;}\
            th {text-align: left;}\
            .printHeading { line-height: 18px;  padding: 10px 0;  font-size: 18px; }\
            table { border-collapse: collapse;  \
                    font-size: 12px; }\
            table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
            body { font-family: "Open Sans", sans-serif;\
            background-color:#fff;\
            font-size: 15px;\
            direction: ltr;}</style>' + $("#divPrintArea").html();
            if (isIE == true) {
                var printWindow = window.open('', '', 'height=400,width=800');
                printWindow.document.write(divContents);
                printWindow.focus();
                printWindow.document.execCommand("print", false, null);
            } else {
                $('<iframe>', {
                    name: 'myiframe',
                    class: 'printFrame'
                }).appendTo('body').contents().find('body').html(divContents);
                window.frames['myiframe'].focus();
                window.frames['myiframe'].print();
                setTimeout(
                        function ()
                        {
                            $(".printFrame").remove();
                        }, 1000);
            }
        }
        function takeprint_invoice() {
            var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
            var divContents = '<style>\
            .darkgreen{	background-color:#364622; color:#fff!important; font-size:24px; font-weight:600;}\
            .fentgreen1{background-color:#b0b29c;color:#4a5036;	font-size:12px;}\
            .fentgreen{	background-color:#b0b29c;	color:#4a5036;}\
            .font-big{	font-size:20px;	font-weight:600;	color:#364622;}\
            .font-big1{	font-size:18px;	font-weight:600;	color:#364622;}\
            .table-bordered-popup {    border: 1px solid #364622;}\
            .table-bordered-popup > tbody > tr > td, .table-bordered-popup > tbody > tr > th, .table-bordered-popup > thead > tr > td, .table-bordered-popup > thead > tr > th {\
                    border: 1px solid #364622;	color:#4a5036;}\
            .blue{	color:#010057;}\
            .blue1{	color:#574960;	font-size:16px;}\
            .buyer_section{	color:#574960;	font-size:14px;}\
            .pad-5{	padding-left:10px;}\
            .pad-40{	padding-left:40px;}\
            .np{	padding-left:0px;	padding-right:0px;}\
            .bg{	background-image:url(../../assets/global/img/watermark.png); background-repeat:no-repeat;\
             background-size: 200px 200px;}\
            </style>' + $("#divPrintArea").html();
            if (isIE == true) {
                var printWindow = window.open('', '', 'height=400,width=800');
                printWindow.document.write(divContents);
                printWindow.focus();
                printWindow.document.execCommand("print", false, null);
            } else {
                $('<iframe>', {
                    name: 'myiframe',
                    class: 'printFrame'
                }).appendTo('body').contents().find('body').html(divContents);
                window.frames['myiframe'].focus();
                window.frames['myiframe'].print();
                setTimeout(
                        function ()
                        {
                            $(".printFrame").remove();
                        }, 1000);
            }
        }
        var lat = 0;
        var lng = 0;

        $('#googleMapPopup').on('shown.bs.modal', function (e) {

            var latlng = new google.maps.LatLng(lat, lng);
            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 17
            });
            var marker = new google.maps.Marker({
                map: map,
                position: latlng,
                draggable: false,
                //anchorPoint: new google.maps.Point(0, -29)
            });
        });
        function showGoogleMap(getlat, getlng) {

            lat = getlat;
            lng = getlng;
            $('#googleMapPopup').modal('show');
        }

        $("#statusbtn").click(function () {
            var void1 = [];
            $('input[name="select_all[]"]:checked').each(function () {
                void1.push(this.value);
            });
            var energy = void1.join();
            if (energy != '') {
                $("#assign_delivery").modal('show');
                $("#orderids").val(energy);
            } else {
                alert("Please select minimum one order");
                //location.reload();
            }

        });

       
       
        function fnShowSalesperson(id) {
            var url = "getSalesPersonDropDown.php?cat_id=" + id.value;
            CallAJAX(url, "divsalespersonDropdown");
        }

    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
</html>