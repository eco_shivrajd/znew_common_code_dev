<?php
include ("../../includes/config.php");//user_summary_details
include "../includes/testManage.php";
include "../includes/userConfigManage.php";
$testObj 	= 	new testManager($con,$conmain);
$userconfObj 	= 	new userConfigManager($con,$conmain);
$user_type_info=$_POST['user_type'];
$thePostIdArray = explode(',', $user_type_info);
$user_type=$thePostIdArray[0];
$user_role=$thePostIdArray[1];
//echo "<pre>";print_r($user_type);print_r($user_role);die();
$user_type_title = " ".$user_type." ";

 $profile_id = $_SESSION[SESSION_PREFIX.'profile_id'];
  $sql5="SELECT ischecked_add,ischecked_edit,ischecked_delete FROM tbl_action_profile where profile_id='".$profile_id."' ";//AND  page_id='44'
	$result5 = mysqli_query($con,$sql5);
	$row5 = mysqli_fetch_assoc($result5);
	$ischecked_add = $row5['ischecked_add'];
	$ischecked_edit = $row5['ischecked_edit'];
	$ischecked_delete = $row5['ischecked_delete']; 


//$row = $userObj->getAllUsers(); 
$row = $userconfObj->getAllUsersView();
//print_r($row);
$colspan = "7";
if($user_role == 'salesperson'){
	$colspan = "9";	
}
if($user_role == 'Superstockist' || $user_role == 'Distributor' || $user_role == 'DeliveryPerson' || $user_role == 'DeliveryChannelPerson' || $user_role == 'Shopkeeper' || $user_role == 'Accountant')
{
	$colspan = "8";	
}
?>
<? if($_POST["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<? } ?>
<table 
	class="table table-striped table-bordered table-hover table-highlight table-checkable" 
	data-provide="datatable" 
	data-display-rows="10"
	data-info="true"
	data-search="true"
	data-length-change="true"
	data-paginate="true"
	id="sample_2">

<thead>
<tr>
	<td colspan="<?=$colspan;?>" align="canter" class="gradeX even" style="text-align:center; font-weight:600;"><h4><b><?php if($user_type_title!="  "){echo $user_type_title;} else{
		echo "No User Type available.";
	}?></b></h4></td>              
  </tr>
  <tr>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Name</th>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Email</th>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Mobile No.</th>
	<?php if($user_role == 'Superstockist' || $user_role == 'Distributor' || $user_role == 'DeliveryPerson' || $user_role == 'DeliveryChannelPerson' || $user_role == 'SalesPerson' || $user_role == 'Shopkeeper' || $user_role == 'Accountant'){ ?>
		<th data-filterable="false" data-sortable="true" data-direction="desc">Parent Names</th>
	<?php } ?>
	<th data-filterable="false" data-sortable="true" data-direction="desc">City</th>
	<th data-filterable="false" data-sortable="true" data-direction="desc">State</th>
	<?php if($user_role == 'salesperson'){ ?>
		<th data-filterable="false" data-sortable="true" data-direction="desc">Taluka</th>
		<th data-filterable="false" data-sortable="true" data-direction="desc">Parent Names</th>
	<?php } ?>
	<th data-filterable="false" style="width: 150px;">Action</th>              
  </tr>
</thead>
<tbody>					
	<?php foreach($row as $key => $val) { ?>
				<tr class="odd gradeX">
				<td> 
					<?php if ($ischecked_edit==1) 
                      {
                      	?>
                      	         <a href="user_update_config.php?id=<?php echo base64_encode($val['id']);?>"><?=$val['firstname'];?></a>
                      	<?php
                        }
                      	else
                      	{
                      		?>
                      		<?=$val['firstname'];?>

                      	<?php } ?> 
				

			  </td>
				<td><?=$val['email'];?></td>
				<td align="right"><?=$val['mobile'];?></td>
               <?php if($user_role == 'Superstockist' || $user_role == 'Distributor' || $user_role == 'DeliveryPerson' || $user_role == 'DeliveryChannelPerson' || $user_role == 'SalesPerson' || $user_role == 'Shopkeeper' || $user_role == 'Accountant'){ ?>
				<td><?php
                  $parent_names = str_replace(",",", ",$val['parent_names']);
				 echo $parent_names;?></td>
               <?php } ?>

				<td><?=$val['cityname'];?></td>
				<td><?=$val['statename'];?></td>
				<?php 
				if($user_role == 'salesperson'){ ?>
				<td><?php echo  $val['suburbnm'];?></td>
				<td><?php
                  $parent_names = str_replace(",",", ",$val['parent_names']);
				 echo $parent_names;?></td>
				<?php } ?>
				<td align='left'>
					<?php if ($user_role == 'SalesPerson') { ?>
						<!-- <a title="Assign Target" href="target_add.php?utype=<?=$user_role;?>&id=<?=$val['id'];?>">Target</a> -->
						 <a  title="Assign Target" href="target_add.php?utype=<?=base64_encode($user_role);?>&id=<?=base64_encode($val['id']);?>" class="btn btn-xs btn-info "><span class="glyphicon glyphicon-send"></span> Target</a>
					<?php  } ?>					
					
                      <a  title="View Details" onclick="showUserDetails('<?php echo $val['id']; ?>','<?php echo $user_role; ?>');" class="btn btn-xs btn-success "><span class="glyphicon glyphicon-eye-open"></span></a>

                     <?php if ($ischecked_edit==1) 
                         {
                      	?>
                      	        <a title="Edit" href="user_update_config.php?id=<?php echo base64_encode($val['id']);?>"  class="btn btn-xs btn-primary "><span class="glyphicon glyphicon-edit"></span></a>  
                      	<?php
                        }
                      	else
                      	{
                      	?>
                      		<a title="Can Not Edit This Record" href="#"  class="btn btn-xs btn-primary" disabled><span class="glyphicon glyphicon-edit"></span></a>
                      	<?php } ?> 

					   <?php if ($ischecked_delete==1 && $user_role!='Admin') 
                         {
                      	?>
                      	        <a title="Delete" href="manageuser_config.php?utype=<?=base64_encode($user_type);?>&id=<?=base64_encode($val['id']);?>"  class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
                      	<?php
                        }
                      	else
                      	{
                      	?>
                      		<a title="Can Not Delete This Record" href="#"  class="btn btn-xs btn-danger" disabled><span class="glyphicon glyphicon-trash"></span></a>
                      	<?php } ?> 
				</td>									
			</tr>			
	<?php	} 
		if($_POST["actionType"]=="excel" &&  $row == 0) {
			echo "<tr><td>No matching records found</td></tr>";
		}
	?>		
</tbody>	
</table>
<script>
jQuery(document).ready(function() { 
   ComponentsPickers.init();
});

jQuery(document).ready(function() { 
	TableManaged.init();
});
$(document).ready(function() {
      var table = $('#sample_2').dataTable();    
      table.fnFilter('');    
  });
</script>

<!-- END JAVASCRIPTS -->
<?
if($_POST["actionType"]=="excel") {
	if($row != 0){
		header("Content-Type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=Report_summary.xls");
		exit;
	}
} ?>
 