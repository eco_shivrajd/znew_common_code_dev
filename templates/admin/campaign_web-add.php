<?php
//session_start();
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
include "../includes/header.php";
include "../includes/commonManage.php";
if(isset($_POST['hidbtnsubmit']))
{
	//print"<pre>";
	//print_r($_POST); 
	$fields = "";
	$values = "";
	$added_on_field = ", added_on";
	$added_on_value = ", '".date('Y-m-d')."'";
	//print"<pre>";
	//print_r($_POST);//exit;
	$fields.= " campaign_name, campaign_start_date, campaign_end_date, 	campaign_type ".$added_on_field;
	$values.= "'".fnEncodeString($_POST['campaign_name'])."', '".date("Y-m-d",strtotime($_POST['campaign_start_date']))."', '".date("Y-m-d",strtotime($_POST['campaign_end_date']))."', '".$_POST['criteriaType']."'".$added_on_value;
	if($_POST['campaign_description'] != ''){
		$fields.= ", campaign_description";
		$values.= ", '".fnEncodeString($_POST['campaign_description'])."'";
	}
	if(isset($_POST['select_all_brand'])){
		$fields.= ", brand_all";
		$values.= ", 1";
	}
	$area_fields = "";
	$area_values = "";
	$area_price_fields = "";
	$area_price_values = "";
	
		$level = "";
		$sql_campaign = "INSERT INTO `tbl_campaign` ($fields) VALUES($values)";
		$campaign_add=mysqli_query($con,$sql_campaign);
		$campaign_id= mysqli_insert_id($con);
		
		$commonObj 	= 	new commonManage($con,$conmain);
		$commonObj->log_add_record('tbl_campaign',$campaign_id,$sql_campaign);	
		if($_POST['state_campaign_discount'] != '')
		{
			$area_fields.= ", state_id";
			$state_id = implode(",",$_POST['state_campaign_discount']);
			$area_values.= ", '".$state_id."'";
			$level = "'state'";
		}
		if($_POST['city_campaign_discount'] != '')
		{
			$area_fields.= ", city_id";
			$city_id = implode(",",$_POST['city_campaign_discount']);
			$area_values.= ", '".$city_id."'";
			$level = "'city'";
		}
		if($_POST['suburb_campaign_discount'] != '')
		{
			$area_fields.= ", suburb_id";
			$suburb_id = implode(",",$_POST['suburb_campaign_discount']);
			$area_values.= ", '".$suburb_id."'";
			$level = "'suburb'";
		}
		if($_POST['subarea'] != '')
		{
			$area_fields.= ", subarea_id";
			$subarea_id = implode(",",$_POST['subarea']);
			$area_values.= ", '".$subarea_id."'";
			$level = "'subarea'";
		}
		
		$sql_area = "Update  `tbl_campaign_area` set 
		
		(campaign_id, level $area_fields $added_on_field) 
		VALUES($campaign_id, $level $area_values $added_on_value)";
		$area_added = mysqli_query($con,$sql_area);
		$campaign_area_id = mysqli_insert_id($con);
		$commonObj 	= 	new commonManage($con,$conmain);
		$commonObj->log_add_record('tbl_campaign_area',$campaign_area_id,$sql_area);	
	if($_POST['criteriaType'] == 'discount')
	{
		$area_price = "";
		$total_element = $_POST['total_element'];
		for($i=1; $i<=$total_element; $i++)
		{
			$area_price_fields = "";
			$area_price_values = ""; 
			$area_price_fields.= ", product_price";
			$post_product_price = 'campaign_product_price_'.$i;
			$area_price_values.= ", '".$_POST[$post_product_price]."'"; 
			$area_price_fields.= ", discount_percent";
			$post_discount_percent = 'campaign_product_discount_'.$i;
			$area_price_values.= ", '".$_POST[$post_discount_percent]."'"; 
			
			$sql_area_price = "INSERT INTO `tbl_campaign_area_price` (campaign_id, campaign_area_id $area_price_fields $added_on_field) VALUES($campaign_id, $campaign_area_id $area_price_values $added_on_value)";
			$area_price_added = mysqli_query($con,$sql_area_price);
			$campaign_area_price_id = mysqli_insert_id($con);
			$commonObj 	= 	new commonManage($con,$conmain);
			$commonObj->log_add_record('tbl_campaign_area_price',$campaign_area_price_id,$sql_area_price);	
		}
		
	}
	if($_POST['criteriaType'] == 'free_product')
	{
		if(isset($_POST['select_all_brand'])){
				$free_product_fields = "";
				$free_product_values = ""; 
				$post_brand_free_p = 'brand_free_p_1';
				if($_POST[$post_brand_free_p] != ''){
					$free_product_fields.= ", f_p_brand_id";			
					$free_product_values.= ", ".$_POST[$post_brand_free_p];
				}
				$post_category_free_p = 'category_free_p_1';
				if($_POST[$post_category_free_p] != ''){
					$free_product_fields.= ", f_p_category_id";			
					$free_product_values.= ", ".$_POST[$post_category_free_p];
				}
				$free_product_fields.= ", f_product_id";
				$post_product_free_p = 'product_free_p_1';
				$free_product_values.= ", ".$_POST[$post_product_free_p];
				
				$post_variant_free_p = 'variant_free_p_1';
				$post_variant_free_p_value = $_POST[$post_variant_free_p];
				//$free_p_variant = explode("#",$post_variant_free_p_value);
				if($post_variant_free_p_value != ''){
					$variant_arr = '';
					$sql="SELECT id,variant_1,variant_2 from `tbl_product_variant` where id = $post_variant_free_p_value";
					$result1 = mysqli_query($con,$sql);				
					$row = mysqli_fetch_assoc($result1);
					$exp_variant1 = $row['variant_1'];
					$imp_variant1= split(',',$exp_variant1);
					
					if($exp_variant1 != '')
					{
						$sql1="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant1[1];
						$result2 = mysqli_query($con,$sql1);
						$row_prd_variant1 = mysqli_fetch_array($result2);
						$combine1 = $row['id']." ".$imp_variant1[0]." ".$row_prd_variant1['id'];
						$combine1_display = $imp_variant1[0]." ".$row_prd_variant1['unitname'];				
					}
					$exp_variant2 = $row['variant_2'];
					$imp_variant2= split(',',$exp_variant2);
					if($exp_variant2 != '')
					{
						$sql2="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant2[1];
						$result3 = mysqli_query($con,$sql2);
						$row_prd_variant2 = mysqli_fetch_array($result3);				
						$combine2 = $row['id'];	
						$combine2_display = $imp_variant2[0]." ".$row_prd_variant2['unitname'];						
						$selected = "";
					
						$variant_arr['f_quantity'] = $imp_variant2[0];
						$variant_arr['f_measure'] = $row_prd_variant2['unitname'];
						$variant_arr['f_quantity_measure'] = $combine2_display." (".$combine1_display.")";				
					}				
				}
				$free_product_fields.= ", f_p_quantity";			
				$free_product_values.= ", '".$variant_arr['f_quantity']."'";	
				$free_product_fields.= ", f_p_measure";			
				$free_product_values.= ", '".$variant_arr['f_measure']."'";
				$free_product_fields.= ", f_p_measure_id";			
				$free_product_values.= ", '".$post_variant_free_p_value."'";
				
				$free_product_fields.= ", f_p_quantity_measure";		
				$free_product_values.= ", '".$variant_arr['f_quantity_measure']."'";	
				
			$sql="SELECT id FROM tbl_brand order by name";
			$result1 = mysqli_query($con,$sql);
			while($row = mysqli_fetch_array($result1))
			{
				$sql_cat="SELECT id FROM tbl_category WHERE  brandid =".$row['id'];
				$result_cat = mysqli_query($con,$sql_cat);
				while($row_cat = mysqli_fetch_array($result_cat))
				{
					$sql_prod="SELECT id FROM tbl_product WHERE catid=".$row_cat['id'];
					$result_prod = mysqli_query($con,$sql_prod);
					while($row_prod = mysqli_fetch_array($result_prod))
					{	
						$campaign_product_fields = "";
						$campaign_product_values = ""; 
						$campaign_product_fields.= ", c_p_brand_id";
						$campaign_product_values.= ", ".$row['id'];
						$campaign_product_fields.= ", c_p_category_id";
						$campaign_product_values.= ", ".$row_cat['id'];
						$campaign_product_fields.= ", c_product_id";						
						$campaign_product_values.= ", ".$row_prod['id'];
						
						
						$sql_variant1="SELECT id,variant_1,variant_2 from `tbl_product_variant` where productid = ".$row_prod['id'];
						$result_variant1 = mysqli_query($con,$sql_variant1);				
						
						while($row_variant1 = mysqli_fetch_array($result_variant1))
						{
							$exp_variant1 = $row_variant1['variant_1'];
							$imp_variant1= split(',',$exp_variant1);
							
							if($exp_variant1 != '')
							{
								$sql1="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant1[1];
								$result2 = mysqli_query($con,$sql1);
								$row_prd_variant1 = mysqli_fetch_array($result2);
								$combine1 = $row_variant1['id']." ".$imp_variant1[0]." ".$row_prd_variant1['id'];
								$combine1_display = $imp_variant1[0]." ".$row_prd_variant1['unitname'];				
							}
							$exp_variant2 = $row_variant1['variant_2'];
							$imp_variant2= split(',',$exp_variant2);
							if($exp_variant2 != '')
							{
								$sql2="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant2[1];
								$result3 = mysqli_query($con,$sql2);
								$row_prd_variant2 = mysqli_fetch_array($result3);				
								$combine2 = $row_variant1['id'];	
								$combine2_display = $imp_variant2[0]." ".$row_prd_variant2['unitname'];						
								$selected = "";
							
								$campaign_product_fields_temp.= ", c_p_measure_id";
								$campaign_product_values_temp.= ", '".$row_variant1['id']."'";
								$campaign_product_fields_temp.= ", c_p_quantity";
								$campaign_product_values_temp.= ", '".$imp_variant2[0]."'";
								$campaign_product_fields_temp.= ", c_p_measure";
								$campaign_product_values_temp.= ", '".$row_prd_variant2['unitname']."'";
								$campaign_product_fields_temp.= ", c_p_quantity_measure";
								$campaign_product_values_temp.= ", '".$combine2_display." (".$combine1_display.")'";
								
								$campaign_product_fields1 = $campaign_product_fields.$campaign_product_fields_temp;
								$campaign_product_values1 = $campaign_product_values.$campaign_product_values_temp;
						
								$sql_area_price = "INSERT INTO `tbl_campaign_product` (campaign_id $campaign_product_fields1 $free_product_fields $added_on_field) VALUES($campaign_id $campaign_product_values1 $free_product_values $added_on_value)";
								$area_price_added = mysqli_query($con,$sql_area_price);
								$area_id = mysqli_insert_id($con);
								$commonObj 	= 	new commonManage($con,$conmain);
								$commonObj->log_add_record('tbl_campaign_product',$area_id,$sql_area_price);	
								$campaign_product_fields1= "";
								$campaign_product_values1= "";
								$campaign_product_fields_temp= "";
								$campaign_product_values_temp= "";
								
							}	
						}
					}
				}
			}
		}else{
			$total_element_free = $_POST['total_element_free'];
			for($i=1; $i<=$total_element_free; $i++)
			{
				$campaign_product_fields = "";
				$campaign_product_values = ""; 
				
				$post_brand_campaign_p = 'brand_campaign_p_'.$i;
				if($_POST[$post_brand_campaign_p] != ''){
					$campaign_product_fields.= ", c_p_brand_id";
					$campaign_product_values.= ", ".$_POST[$post_brand_campaign_p];
				}
				$post_category_campaign_p = 'category_campaign_p_'.$i;
				if($_POST[$post_category_campaign_p] != ''){				
					$campaign_product_fields.= ", c_p_category_id";
					$campaign_product_values.= ", ".$_POST[$post_category_campaign_p];
				}
				$campaign_product_fields.= ", c_product_id";
				$post_product_campaign_p = 'product_campaign_p_'.$i;
				$campaign_product_values.= ", ".$_POST[$post_product_campaign_p];
							
				/*Free product variant details start*/
				$free_product_fields = "";
				$free_product_values = ""; 
				$post_brand_free_p = 'brand_free_p_'.$i;
				if($_POST[$post_brand_free_p] != ''){
					$free_product_fields.= ", f_p_brand_id";			
					$free_product_values.= ", ".$_POST[$post_brand_free_p];
				}
				$post_category_free_p = 'category_free_p_'.$i;
				if($_POST[$post_category_free_p] != ''){
					$free_product_fields.= ", f_p_category_id";			
					$free_product_values.= ", ".$_POST[$post_category_free_p];
				}
				$free_product_fields.= ", f_product_id";
				$post_product_free_p = 'product_free_p_'.$i;
				$free_product_values.= ", ".$_POST[$post_product_free_p];
				
				$post_variant_free_p = 'variant_free_p_'.$i;
				$post_variant_free_p_value = $_POST[$post_variant_free_p];
				//$free_p_variant = explode("#",$post_variant_free_p_value);
				if($post_variant_free_p_value != ''){
					$variant_arr = '';
					$sql="SELECT id,variant_1,variant_2 from `tbl_product_variant` where id = $post_variant_free_p_value";
					$result1 = mysqli_query($con,$sql);				
					$row = mysqli_fetch_assoc($result1);
					$exp_variant1 = $row['variant_1'];
					$imp_variant1= split(',',$exp_variant1);
					
					if($exp_variant1 != '')
					{
						$sql1="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant1[1];
						$result2 = mysqli_query($con,$sql1);
						$row_prd_variant1 = mysqli_fetch_array($result2);
						$combine1 = $row['id']." ".$imp_variant1[0]." ".$row_prd_variant1['id'];
						$combine1_display = $imp_variant1[0]." ".$row_prd_variant1['unitname'];	
						$variant_arr['f_quantity'] = $imp_variant1[0];
						$variant_arr['f_measure'] = $row_prd_variant1['unitname'];
					}
					$exp_variant2 = $row['variant_2'];
					$imp_variant2= split(',',$exp_variant2);
					if($exp_variant2 != '')
					{
						$sql2="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant2[1];
						$result3 = mysqli_query($con,$sql2);
						$row_prd_variant2 = mysqli_fetch_array($result3);				
						$combine2 = $row['id'];	
						$combine2_display = $imp_variant2[0]." ".$row_prd_variant2['unitname'];						
						$selected = "";
					
						/*$variant_arr['f_quantity'] = $imp_variant2[0];
						$variant_arr['f_measure'] = $row_prd_variant2['unitname'];*/
						$variant_arr['f_quantity_measure'] = $combine2_display." (".$combine1_display.")";				
					}				
				}
				$free_product_fields.= ", f_p_quantity";			
				$free_product_values.= ", '".$variant_arr['f_quantity']."'";	
				$free_product_fields.= ", f_p_measure";			
				$free_product_values.= ", '".$variant_arr['f_measure']."'";
				$free_product_fields.= ", f_p_measure_id";			
				$free_product_values.= ", '".$post_variant_free_p_value."'";
				
				$free_product_fields.= ", f_p_quantity_measure";		
				$free_product_values.= ", '".$variant_arr['f_quantity_measure']."'";		
				/*Free product variant details end*/
				
				/*Campaign product variant details start*/
				$post_variant_campaign_p = 'variant_campaign_p_'.$i;
				$post_variant_campaign_p_value = $_POST[$post_variant_campaign_p];
				
				
				if($post_variant_campaign_p_value[0] == 'select_all')
				{
					array_shift($post_variant_campaign_p_value);				
				}
				foreach($post_variant_campaign_p_value as $key => $val)
				{
					$campaign_product_fields_variant= "";			
					$campaign_product_values_variant= "";
					$variant_arr = '';
					$sql="SELECT id,variant_1,variant_2 from `tbl_product_variant` where id = $val";
					$result1 = mysqli_query($con,$sql);				
					$row = mysqli_fetch_assoc($result1);
					$exp_variant1 = $row['variant_1'];
					$imp_variant1= split(',',$exp_variant1);
					if($exp_variant1 != '')
					{
						$sql1="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant1[1];
						$result2 = mysqli_query($con,$sql1);
						$row_prd_variant1 = mysqli_fetch_array($result2);
						$combine1 = $row['id']." ".$imp_variant1[0]." ".$row_prd_variant1['id'];
						$combine1_display = $imp_variant1[0]." ".$row_prd_variant1['unitname'];	

						$variant_arr['p_quantity'] = $imp_variant1[0];
						$variant_arr['p_measure'] = $row_prd_variant1['unitname'];
						$exp_variant2 = $row['variant_2'];
						$imp_variant2= split(',',$exp_variant2);
						
						if($exp_variant2 != '')
						{
							$sql2="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant2[1];
							$result3 = mysqli_query($con,$sql2);
							$row_prd_variant2 = mysqli_fetch_array($result3);				
							$combine2 = $row['id'];	
							$combine2_display = $imp_variant2[0]." ".$row_prd_variant2['unitname'];						
							$selected = "";
						
							/*$variant_arr['p_quantity'] = $imp_variant2[0];
							$variant_arr['p_measure'] = $row_prd_variant2['unitname'];*/
							$variant_arr['p_quantity_measure'] = $combine2_display." (".$combine1_display.")";				
						}	

						$campaign_product_fields_variant.= ", c_p_quantity";			
						$campaign_product_values_variant.= ", '".$variant_arr['p_quantity']."'";	
						$campaign_product_fields_variant.= ", c_p_measure";			
						$campaign_product_values_variant.= ", '".$variant_arr['p_measure']."'";	
						$campaign_product_fields_variant.= ", c_p_measure_id";			
						$campaign_product_values_variant.= ", '".$val."'";
						
						$campaign_product_fields_variant.= ", c_p_quantity_measure";		
						$campaign_product_values_variant.= ", '".$variant_arr['p_quantity_measure']."'";
						$campaign_product_fields1 = "";
						$campaign_product_values1 = "";
						$campaign_product_fields1 = $campaign_product_fields.$campaign_product_fields_variant;
						$campaign_product_values1 = $campaign_product_values.$campaign_product_values_variant;

						$sql_area_price = "INSERT INTO `tbl_campaign_product` (campaign_id $campaign_product_fields1 $free_product_fields $added_on_field) VALUES($campaign_id $campaign_product_values1 $free_product_values $added_on_value)";
						$area_price_added = mysqli_query($con,$sql_area_price);
						$area_id = mysqli_insert_id($con);
						$commonObj 	= 	new commonManage($con,$conmain);
						$commonObj->log_add_record('tbl_campaign_product',$area_id,$sql_area_price);	
						$campaign_product_fields1 = "";
						$campaign_product_values1 = "";
					}
				}
				/*Campaign product variant details end*/			
			}
		}
	}

	echo '<script>alert("Campaign added successfully.");location.href="campaign.php";</script>';
	
	
}	
$category_options = '';
$sql="SELECT categorynm , id FROM tbl_category order by categorynm";
$result1 = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result1))
{
	if($row["categorynm"] != '')
		$category_options.="<option value='".$row["id"]."'>" . fnStringToHTML($row["categorynm"]) . "</option>";
}
$category_options_json = json_encode(htmlspecialchars($category_options));
$product_options = '';
$sql="SELECT productname , id   FROM tbl_product order by productname";
$result1 = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result1))
{	
	if($row["productname"] != '')
		$product_options.="<option value='".$row["id"]."'>" . fnStringToHTML($row["productname"]) . "</option>";
}
$product_options_json = json_encode(htmlspecialchars($product_options));
?>
<link href="../../assets/global/css/jquery.loader.css" rel="stylesheet" />

<body class="page-header-fixed page-quick-sidebar-over-content ">

<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
<?php
	$activeMainMenu = "ManageProducts"; $activeMenu = "Campaign";
	include "../includes/sidebar.php"?>	
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Campaign
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="campaign.php">Campaign</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add Campaign</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add Campaign
							</div>
							
						</div>
						<div class="portlet-body">
                        <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
                        <form class="form-horizontal" data-parsley-validate="" role="form" method="post" name="campaign_add" id="campaign_add">
						  <div class="form-group">
							  <label class="col-md-3">Campaign Name:<span class="mandatory">*</span></label>
							  <div class="col-md-4">
								<input type="text" name="campaign_name" id="campaign_name" class="form-control" data-parsley-maxlength="100" data-parsley-maxlength-message="Only 100 characters are allowed" data-parsley-trigger="change" data-parsley-required="#true" data-parsley-required-message="Please enter Campaign Name">
							  </div>
							</div><!-- /.form-group -->	
							<div class="form-group">
							  <label class="col-md-3">Campaign Description:</label>
							  <div class="col-md-4">
								<textarea rows="4" class="form-control" name="campaign_description" id="campaign_description"></textarea>
							  </div>
							</div><!-- /.form-group -->	
							<div class="form-group">
							  <label class="col-md-3">Start Date:<span class="mandatory">*</span></label>
							  <div class="col-md-4">
								<div class="input-group date date-picker2" data-date-format="dd-mm-yyyy">
								<input type="text" name="campaign_start_date" id="campaign_start_date" class="form-control" value="<?=date('d-m-Y');?>">
								<span class="input-group-btn">
								<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
								</span>
								</div>
							  </div>
							</div><!-- /.form-group -->	
							<div class="form-group">
							  <label class="col-md-3">End Date:<span class="mandatory">*</span></label>
							  <div class="col-md-4">
							  <div class="input-group date date-picker3" data-date-format="dd-mm-yyyy">
								<input type="text" name="campaign_end_date" id="campaign_end_date" class="form-control" value="">
								<span class="input-group-btn">
								<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
								</span>
								</div>
							  </div>
							</div><!-- /.form-group -->	
							<!--<div class="clearfix"></div> 
							<div class="form-group">
								<label class="col-md-3">Campaign To:<span class="mandatory">*</span></label>
								<div class="col-md-4">
									<input type="hidden" name="enabled_div1" id="enabled_div1" value="">									
									<input type="radio" name="criteriaTo" id="criteriaTo_web" value="weborders" 
									onclick="javascript:CriteriaSection1('weborders');">&nbsp;&nbsp;For Web User
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="criteriaTo" 
									id="criteriaTo_mobile" value="mobile_orders" 
									onclick="javascript:CriteriaSection1('mobileorders');">&nbsp;&nbsp;For Mobile User
								</div>
							</div> /.form-group -->
							<div class="clearfix"></div> 
							<div class="form-group">
								<label class="col-md-3">Campaign Type:<span class="mandatory">*</span></label>
								<div class="col-md-4">
									<input type="hidden" name="enabled_div" id="enabled_div" value="">									
									<input type="radio" name="criteriaType" id="criteriaType_disc" value="discount" onclick="javascript:CriteriaSection('discount');">&nbsp;&nbsp;%Discount
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="criteriaType" id="criteriaType_free" value="free_product" onclick="javascript:CriteriaSection('free_product');">&nbsp;&nbsp;Free Product 
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="criteriaType" style="display: none;" id="criteriaType_by_weight" value="by_weight" onclick="javascript:CriteriaSection('by_weight');" style="display: none;">&nbsp;&nbsp;<!--By Weight--> 									
								</div>
							</div><!-- /.form-group -->
							
						 	  
							 <div id="discount_div" style="display:none;">				
								<?php include "campaign_add_com_area_section_web.php";	//form common element file with javascript validation ?>  
								<hr/>
								<div><input type="hidden" name="total_element" id="total_element" value="1"></div>
								
								<div id="disccont">								
								<div class="discdet" id="discount_1">
									<div class="form-group">
										<label class="col-md-3">Discount Details:<span class="mandatory">*</span></label>
										<div class="col-md-6 nopadl">		
										
										<div class="col-md-4">
											<input type="text" name="campaign_product_price_1" id="campaign_product_price_1" data-parsley-trigger="change" data-parsley-pattern="[0-9]*(\.?[0-9]{1,2}$)?"  class="form-control" onchange="calculate_discount(1)">
										</div>
										<div class="col-md-1" id="nopad1">
										<label class="nopadl" style="padding-top:5px">Price</label>
										</div>
										<div class="col-md-2" id="camdis">								
											<input type="text" name="campaign_product_discount_1" id="campaign_product_discount_1" data-parsley-trigger="change" data-parsley-pattern="[0-9]*(\.?[0-9]{1,2}$)?"   class="form-control minsz" size ="4" onchange="calculate_discount(1)">
										</div>
										<div class="col-md-1" id="nopad1">
										<label class="nopadl" style="padding-top:5px; ">%</label>
										</div>
										<div class="col-md-1">								
											<button class="btn btn-primary btn-md" type="button" id="remove_discount_1" name="remove_discount_1" onclick="fnRemoveDiscountSection(1)" title="Remove Details" style="display:none;"><i class="fa fa-times"></i></button>
										</div>
										
										</div><!-- /.form-group -->			          
									</div>								
									<hr/>
								</div>
								</div>
								<div class="form-group">
									<div class="col-md-4 col-md-offset-3">
										<div style="margin-top: 10px"><button id="btnAddDisc" class="btn btn-primary btn-md" type="button" title="Add more Discount Details" onclick="javascript:fnAddDisc()">Add More</button></div>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div id="free_product_div" style="display:none;">
							<div><input type="hidden" name="total_element_free" id="total_element_free" value="1"></div>
								<?php include "campaign_add_com_area_section_web.php";	//form common element file with javascript validation ?>    
								<hr/>
								<div id="freecont">
									<div id="free_pro_1">
									<div class="row free">
										<div class="col-sm-6">
										<h4 style="margin-top:30px;"><b>Campaign Product</b>&nbsp;&nbsp;<span style="font-size:12px;" id="select_all_brand_span"><input type="checkbox" name="select_all_brand" id="select_all_brand" value="" onchange="javascript:fnSelectAllBrand(this)">&nbsp;(Select all Brands)</span></h4>
										<hr />
										<div class="form-group" id="brand_div">
											<label class="col-md-6">Brand:<span class="mandatory">*</span></label>
											<div class="col-md-5" id="div_brand_campaign_p_1">
											 <select name="brand_campaign_p_1" id="brand_campaign_p_1" class="form-control" onchange="fnShowCategories(this,'c',1)">
												<option value="">-Select-</option>
												<?php											
												$sql="SELECT name , id FROM tbl_brand order by name";
												$result1 = mysqli_query($con,$sql);
												while($row = mysqli_fetch_array($result1))
												{
													echo "<option value='".$row["id"]."'>" . fnStringToHTML($row["name"]) . "</option>";
												}
												?>
											</select>
											</div>
										</div><!-- /.form-group -->
								
										<div class="form-group" id="category_div">
											<label class="col-md-6">Category:<span class="mandatory">*</span></label>
											<div class="col-md-5" id="div_category_campaign_p_1">
											 <select name="category_campaign_p_1" id="category_campaign_p_1" class="form-control" onchange="fnShowProducts(this,'c',1)">
												<option value="">-Select-</option>
												<?php											
												//echo $category_options;
												?>
											</select>
											</div>
										</div><!-- /.form-group -->
								
										<div class="form-group" id="product_div">
											<label class="col-md-6">Product:<span class="mandatory">*</span></label>
											<div class="col-md-5" id="div_product_campaign_p_1">
											 <select name="product_campaign_p_1" id="product_campaign_p_1" class="form-control" onchange="fnShowProductVariant(this,'c',1)">
												<option value="">-Select-</option>
												<?php											
												//echo $product_options;
												?>
											</select>
											</div>
										</div><!-- /.form-group -->		
										
										<div class="form-group" id="variant_div">
											<label class="col-md-6">Variant:<span class="mandatory">*</span></label>
											<div class="col-md-5" id="div_variant_campaign_p_1">
											 <select name="variant_campaign_p_1" id="variant_campaign_p_1" class="form-control">
												<option value="">-Select-</option>
												
											</select>
											</div>
										</div><!-- /.form-group -->		
										<div class="form-group" id="brand_all_msg" style="display:none;">
											<label class="col-md-6">This campaign is applicable to all Brands.</label>
										</div>
										</div>
									
										<div class="col-sm-5">
										<h4 style="margin-top:30px;"><b>Free Product</b></h4>
										<hr />
										<div class="form-group">
											<label class="col-md-3">Brand:<span class="mandatory">*</span></label>
											<div class="col-md-5" id="div_brand_free_p_1">
											 <select name="brand_free_p_1" id="brand_free_p_1" class="form-control" onchange="fnShowCategories(this,'f',1)">
												<option value="">-Select-</option>
												<?php											
												$sql="SELECT name , id FROM tbl_brand order by name";
												$result1 = mysqli_query($con,$sql);
												while($row = mysqli_fetch_array($result1))
												{
													echo "<option value='".$row["id"]."'>" . fnStringToHTML($row["name"]) . "</option>";
												}
												?>
											</select>
											</div>
										</div><!-- /.form-group -->
								
										<div class="form-group">
											<label class="col-md-3">Category:<span class="mandatory">*</span></label>
											<div class="col-md-5" id="div_category_free_p_1">
											 <select name="category_free_p_1" id="category_free_p_1" class="form-control" onchange="fnShowProducts(this,'f',1)">
												<option value="">-Select-</option>
												<?php											
												//echo $category_options;
												?>
											</select>
											</div>
										</div><!-- /.form-group -->
								
										<div class="form-group">
											<label class="col-md-3">Product:<span class="mandatory">*</span></label>
											<div class="col-md-5" id="div_product_free_p_1">
											 <select name="product_free_p_1" id="product_free_p_1" class="form-control" onchange="fnShowProductVariant(this,'f',1)">
												<option value="">-Select-</option>
												<?php											
												//echo $product_options;
												?>
											</select>
											</div>
										</div><!-- /.form-group -->		
										
										<div class="form-group">
											<label class="col-md-3">Variant:<span class="mandatory">*</span></label>
											<div class="col-md-5" id="div_variant_free_p_1">
											 <select name="variant_free_p_1" id="variant_free_p_1" class="form-control">
												<option value="">-Select-</option>												
											</select>
											</div>
										</div><!-- /.form-group -->		
									</div>
										<div class="col-md-1">								
											<button class="btn btn-primary btn-md" type="button" title="Remove Campaign & Free Product" id="remove_free_product_1" name="remove_free_product_1" onclick="fnRemoveFreeProductSection(1)" style="display:none;"><i class="fa fa-times"></i></button>
										</div>														
									</div>	
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-4 col-md-offset-3">
										<div style="margin-top: 10px"><button id="btnAddFree" class="btn btn-primary btn-md" type="button" title="Add more Campaign & Free Products" onclick="javascript:fnAddFree()">Add More</button></div>
									</div>
								</div>							
							</div>   
							<div id="by_weight_div" style="display:none;">
							<div><input type="hidden" name="total_element_free_wt" id="total_element_free_wt" value="1"></div>
								<?php include "campaign_add_com_area_section_web.php";	//form common element file with javascript validation ?>    
								<hr/>
								<div id="weight_freecont">
									<div id="weight_free_pro_1">
									<div class="row free">
										<div class="col-sm-6">
										<h4 style="margin-top:30px;"><b>Campaign Product</b></h4>
										<hr />			
										<div class="form-group">
											<label class="col-md-6">Weight:<span class="mandatory">*</span></label>
											<div class="col-md-5">
												<div id="nopad1" style="float:left;">
												<input class="nopad1" style="width:92px;" type="text" name="campaign_product_wt_qty_1" id="campaign_product_wt_qty_1" data-parsley-trigger="change" data-parsley-pattern="[0-9]*(\.?[0-9]{1,2}$)?" class="form-control">
												&nbsp;&nbsp;Kg<div class="nameError"></div></div>
												 <div class="clearfix"></div> 
											</div>
										</div><!-- /.form-group -->		
										</div>
									
										<div class="col-sm-5">
										<h4 style="margin-top:30px;"><b>Free Product</b></h4>
										<hr />
										<div class="form-group">
											<label class="col-md-3">Brand:<span class="mandatory">*</span></label>
											<div class="col-md-6" id="div_brand_free_p_1">
											 <select name="brand_free_p_1" id="brand_free_p_1" class="form-control" onchange="fnShowCategories(this,'f',1)">
												<option value="">-Select-</option>
												<?php											
												$sql="SELECT name , id FROM tbl_brand order by name";
												$result1 = mysqli_query($con,$sql);
												while($row = mysqli_fetch_array($result1))
												{
													echo "<option value='".$row["id"]."'>" . fnStringToHTML($row["name"]) . "</option>";
												}
												?>
											</select>
											</div>
										</div><!-- /.form-group -->
								
										<div class="form-group">
											<label class="col-md-3">Category:<span class="mandatory">*</span></label>
											<div class="col-md-6" id="div_category_free_p_1">
											 <select name="category_free_p_1" id="category_free_p_1" class="form-control" onchange="fnShowProducts(this,'f',1)">
												<option value="">-Select-</option>
												<?php											
												//echo $category_options;
												?>
											</select>
											</div>
										</div><!-- /.form-group -->
								
										<div class="form-group">
											<label class="col-md-3">Product:<span class="mandatory">*</span></label>
											<div class="col-md-6" id="div_product_free_p_1">
											 <select name="product_free_p_1" id="product_free_p_1" class="form-control" onchange="fnShowProductVariantUnit(this,'f',1)">
												<option value="">-Select-</option>
												<?php											
												//echo $product_options;
												?>
											</select>
											</div>
										</div><!-- /.form-group -->		
										
										<div class="form-group">
											<label class="col-md-3">Variant:<span class="mandatory">*</span></label>
											<div class="col-md-6" id="div_variant_free_p_1">
											 <select name="variant_free_p_1" id="variant_free_p_1" class="form-control">
												<option value="">-Select-</option>												
											</select>
											</div>
										</div><!-- /.form-group -->	
									</div>
										<div class="col-md-1">								
											<button class="btn btn-primary btn-md" type="button" title="Remove Campaign & Free Product" id="remove_free_product_weight_1" name="remove_free_product_weight_1" onclick="fnRemoveFreePWtSection(1)" style="display:none;"><i class="fa fa-times"></i></button>
										</div>														
									</div>	
									</div>
								</div>
								<!--<div class="form-group">
									<div class="col-md-4 col-md-offset-3">
										<div style="margin-top: 10px"><button id="btnAddFreeWeight" class="btn btn-primary btn-md" type="button" title="Add more Campaign & Free Products" onclick="javascript:fnAddFreeWeight()">Add More</button></div>
									</div>
								</div>-->							
							</div>   
           <div class="clearfix"></div> 
                  
            <hr/>      
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
				<input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
                <button type="button" name="button" class="btn btn-primary" onclick="return validateForm()" >Submit</button>
                <a href="campaign.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group --> 
          </form>          
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	<script type="text/javascript" src="../includes/js/campaign_web.js"></script>
	<!-- END QUICK SIDEBAR -->
</div>
<?php include "../includes/footer.php"?>

<!-- END FOOTER -->	
</body>
<!-- END BODY -->
</html>
<script>
$('.date-picker1').datepicker({	
	autoclose: true
});
 $('.date-picker2').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "auto",
                startDate: 'd',
                endDate: '+3m',
                autoclose: true
            });
 $('.date-picker3').datepicker({
	rtl: Metronic.isRTL(),
	orientation: "auto",
	startDate: '+1d',
	endDate: '+3m',
	autoclose: true
});
</script>
<script type="text/javascript" src="../../assets/global/scripts/jquery.loader.js"></script>
