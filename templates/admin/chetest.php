<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
$user_id = $_SESSION[SESSION_PREFIX.'user_id'];
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "ManageProducts";
        $activeMenu = "Brands";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
                <!-- /.modal -->
                <h3 class="page-title">Brands</h3>

                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                        <li><i class="fa fa-home"></i><a href="#">Brands</a></li>
                    </ul>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">
                                    Brands Listing
                                </div>
                                <?php
                                if ($ischecked_add==1) 
                                {
                                ?>
                                <a class="btn btn-sm btn-default pull-right mt5" href="brands-add.php">
                                    Add brand
                                </a>
                              <?php } ?>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                            
<form action="">
    <input id="bikeCheckbox" class="mycheckbox" type="checkbox" name="bikeCheckbox" value="Bike">I have a bike<br>
     <input id="bikeCheckbox" class="mycheckbox" type="checkbox" name="bikeCheckbox" value="Bike">I have a bike<br>
   <!--  <input id="carCheckbox" class="mycheckbox" type="checkbox" name="carCheckbox" value="Car">I have a car -->
</form>

                            </div>
                        </div>                  
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <?php include "../includes/grid_footer.php" ?>
    <!-- END FOOTER -->

    <script type="text/javascript">
        
    
jQuery(function () {
    // Whenever any of these checkboxes is clicked
    $("input.mycheckbox").click(function () {
        // Loop all these checkboxes which are checked
        $("input.mycheckbox:checked").each(function(){
            alert("it works");
            // Use $(this).val() to get the Bike, Car etc.. value
        });
    })
});
    </script>
    <script>
        function deleteBrand(brand_id) {
            var response = ajax_call('ajax_product_manage.php?action=check_product_available');
            //alert(response);
            //console.log(response);
            if (response != 'more_than_one_product') {
                if (response != 'no_product') {

                    var record = response.split("####");
                    var product = record[0];
                    var category = record[1];
                    var brand = record[2];
                    if (brand_id == brand) {
                        alert('This Brand cannot be deleted as only one Product is there and which belong to this Brand.');
                        return false;
                    } else {
                        if (confirm('Category, Products under this Brand will also get deleted. Are you sure that you want to delete this Brand?')) {
                            var response = ajax_call('ajax_product_manage.php?action=delete_brand&brand_id=' + brand_id);
                            if (response == 'brand_deleted') {
                                alert('Brand deleted successfully');
                                location.reload();
                            }
                        }
                    }
                }
            } else {
                if (confirm('Category, Products under this Brand will also get deleted. Are you sure that you want to delete this Brand?')) {
                    var response = ajax_call('ajax_product_manage.php?action=delete_brand&brand_id=' + brand_id);
                    if (response == 'brand_deleted') {
                        alert('Brand deleted successfully');
                        location.reload();
                    }
                }
            }
        }
        function ajax_call(url) {
            var response;
            $.ajax({
                type: "GET",
                url: url,
                async: false,
                success: function (data) {
                    response = data;
                },
                error: function (xhr, status, error) {
                    //alert(xhr.responseText);
                }
            });
            return response;
        }
    </script>
</body>
<!-- END BODY -->
</html>