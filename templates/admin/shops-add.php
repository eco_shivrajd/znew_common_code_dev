<!-- BEGIN HEADER -->
<?php include "../includes/header.php"; 
include "../includes/userManage.php";
include "../includes/shopManage.php";
include "../includes/testManage.php";
$shopObj 	= 	new shopManager($con,$conmain);
$testObj 	= 	new testManager($con,$conmain);
if(isset($_POST['submit']))
{
	$shopObj->addShopDetails();
	echo '<script>alert("Shop added successfully.");location.href="shops.php";</script>';
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Shops";
	include "../includes/sidebar.php";

		$sql1="SELECT `id` as page_id FROM tbl_pages where php_page_add='".$php_page_name."'";
		$result1 = mysqli_query($con,$sql1);		
		$row_url = mysqli_fetch_assoc($result1);
		$page_id_url = $row_url['page_id'];
		$sql12="SELECT ischecked_add FROM tbl_action_profile where profile_id='".$profile_id."' AND  page_id='".$page_id_url."'";
		$result12 = mysqli_query($con,$sql12);
		$row_url_add = mysqli_fetch_assoc($result12);

		$ischecked_add_url = $row_url_add['ischecked_add'];
		if ($ischecked_add_url == 0 && $ischecked_add_url!='') 
		{
		session_set_cookie_params(0);
		session_start();
		session_destroy();
		echo '<script>location.href="../login.php";</script>';
		exit;
		}
		
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Shops
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					 
					<li>
						<i class="fa fa-home"></i>
						<a href="shops.php">Shops</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Shop</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Shop
							</div>
							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
<form class="form-horizontal" data-parsley-validate="" role="form" method="post" action="shops-add.php">         
            <div class="form-group">
              <label class="col-md-3">Shop Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text" 
				placeholder="Enter Shop Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter shop name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				name="name"class="form-control">
              </div>
            </div><!-- /.form-group -->
			<?php  
				$seesion_user_type=$_SESSION[SESSION_PREFIX.'user_type'];
				$seesion_user_id=$_SESSION[SESSION_PREFIX.'user_id'];
			?>
            <div class="form-group">
				<label class="col-md-3">Select Service By User Type:<span class="mandatory">*</span></label>
				<div class="col-md-4">
				<select name="service_by_user_type" id="service_by_user_type" 
				class="form-control" onchange="getUserID(this)" required>				
				<?php
				 $result1 = $testObj->getUsertype_underme_forshop_byuserid($seesion_user_id);
				// echo "<pre>";print_r($result1);
				if($seesion_user_type=='Admin'){?>
					<option value="">-- Select --</option>
					<?php while ($row = mysqli_fetch_array($result1)){ ?>						
						<option value="<?php echo $row['user_type'].",".$row['user_role'].",".$row['profile_id'];?>" ><?php echo $row['user_type'];?></option>
				<?php }
				}else{?>
					<option selected value="<?php echo $seesion_user_type.",".$_SESSION[SESSION_PREFIX.'user_role'].",".$_SESSION[SESSION_PREFIX.'profile_id'];?>" >
						<?php echo $seesion_user_type;?>
					</option>
				<?php } ?> 
				</select>
				</div>
			</div>                      
			<div class="form-group" id="serviceby_div" name="serviceby_div" 
			<?php if($seesion_user_type=='Admin'){ ?> style="display:none;"<?php } ?> >
				  <label class="col-md-3">Service By User:<span class="mandatory">*</span></label>
				  <div class="col-md-4" >
				  <select name="service_by_user_id" id="service_by_user_id" 
				  data-parsley-trigger="change" class="form-control">
				  <?php if($seesion_user_type=='Admin'){ ?>
						<option selected value="">-Select-</option>	
				  <?php } else{?>
						<option selected  value="<?php echo $seesion_user_id;?>">
						<?php echo $_SESSION[SESSION_PREFIX.'firstname'];?></option>	
				  <?php } ?>
						
					</select>
				  </div>
			</div><!-- /.form-group -->
			
            <div class="form-group">
              <label class="col-md-3">Address:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <textarea name="address" 
				rows="4"
                placeholder="Enter Address"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter address"
				data-parsley-maxlength="200"
				data-parsley-maxlength-message="Only 200 characters are allowed"
				class="form-control"></textarea>
              </div>
            </div><!-- /.form-group -->
			<!--<div class="form-group">
              <label class="col-md-3">Pin Code:<span class="mandatory">*</span></label>
			  <div class="col-md-4">
                <input type="text" name="pincode" onkeyup="getStateCityAreaFromPincode(this)"
                placeholder="Enter Pin Code"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter Pin Code"
				data-parsley-maxlength="6"
				data-parsley-minlength="6"
				data-parsley-maxlength-message="Only 6 characters are allowed"
				data-parsley-minlength-message="Only 6 characters are allowed"
				data-parsley-pattern="^(?!\s)[0-9!@#$%^&*+_=><,./:' ]*$"
				data-parsley-pattern-message="Alphabets are not allowed"
				class="form-control">
              </div>
			</div>-->
            <div class="form-group">
				<label class="col-md-3">State:<span class="mandatory">*</span></label>
				<div class="col-md-4">
				<select name="state"              
				data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please select state"
				class="form-control" onChange="fnShowCity(this.value)">
				<option selected disabled>-Select-</option>
				<?php
				$sql="SELECT id,name FROM tbl_state where country_id=101 order by name";
				$result = mysqli_query($con,$sql);
				while($row = mysqli_fetch_array($result))
				{
					$cat_id=$row['id'];
					echo "<option value='$cat_id'>" . $row['name'] . "</option>";
				} ?>
				</select>
				</div>
				</div>			
				<div class="form-group" id="city_div" style="display:none;">
				  <label class="col-md-3">District:<span class="mandatory">*</span></label>
				  <div class="col-md-4" id="div_select_city">
				  <select name="city" id="city" data-parsley-trigger="change" class="form-control">
					<option selected value="">-Select-</option>										
					</select>
				  </div>
				</div><!-- /.form-group -->
				<div class="form-group" id="area_div" style="display:none;">
				  <label class="col-md-3">Taluka:<span class="mandatory">*</span></label>
				  <div class="col-md-4" id="div_select_area">
				  <select name="area" id="area" data-parsley-trigger="change" class="form-control">
					<option selected value="">-Select-</option>									
					</select>
				  </div>
				</div><!-- /.form-group --> 						
				<div class="form-group" id="subarea_div" style="display:none;">
				  <label class="col-md-3">Subarea:</label>
				  <div class="col-md-4" id="div_select_subarea">
				  <select name="subarea" id="subarea" data-parsley-trigger="change" class="form-control">
					<option selected value="">-Select-</option>									
					</select>
				  </div>
				</div><!-- /.form-group --> 
 
            <div class="form-group">
              <label class="col-md-3">Contact Person 1:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" 
				placeholder="Enter Contact Person Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter contact person name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				name="contact_person"class="form-control">
              </div>
            </div><!-- /.form-group -->
			<div class="form-group">
              <label class="col-md-3">Mobile Number 1:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="mobile"
                placeholder="Enter Mobile Number"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter mobile number"
				data-parsley-maxlength="15"
				data-parsley-minlength="10"
				data-parsley-maxlength-message="Only 15 characters are allowed"
				data-parsley-minlength-message="Mobile number length should be 10 or more"
				data-parsley-pattern="^(?!\s)[0-9!@#$%^&*+_=><,./:' ]*$"
				data-parsley-pattern-message="Alphabets are not allowed"
				class="form-control">
              </div>
            </div><!-- /.form-group -->
            <div class="form-group">
              <label class="col-md-3">Contact Person 2:</label>

              <div class="col-md-4">
                <input type="text" 
				placeholder="Enter Contact Person Name"
                data-parsley-trigger="change"				
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				name="contact_person_other"class="form-control">
              </div>
            </div><!-- /.form-group -->
            
            
            
            <div class="form-group">
              <label class="col-md-3">Mobile Number 2:</label>

              <div class="col-md-4">
                <input type="text" name="mobile_number_other"
                placeholder="Enter Mobile Number"
                data-parsley-trigger="change"				
				data-parsley-maxlength="15"
				data-parsley-minlength="10"
				data-parsley-maxlength-message="Only 15 characters are allowed"
				data-parsley-minlength-message="Mobile number length should be 10 or more"
				data-parsley-pattern="^(?!\s)[0-9!@#$%^&*+_=><,./:' ]*$"
				data-parsley-pattern-message="Alphabets are not allowed"
				class="form-control">
              </div>
            </div><!-- /.form-group -->
			<div class="form-group">
              <label class="col-md-3">Landline:</label>
              <div class="col-md-4">
                <input type="text" name="landline_number_other"
                placeholder="Enter Landline Number"
                data-parsley-trigger="change"				
				data-parsley-maxlength="15"
				data-parsley-minlength="10"
				data-parsley-maxlength-message="Only 15 characters are allowed"
				data-parsley-minlength-message="Landline number length should be 10 or more"
				data-parsley-pattern="^(?!\s)[0-9!@#$%^&*+_=><,./:' ]*$"
				data-parsley-pattern-message="Alphabets are not allowed"
				class="form-control">
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <label class="col-md-3">GST Shop Number:</label>
              <div class="col-md-4">
                <input type="text" name="gst_number"
                placeholder="Enter GST Shop Number"
                data-parsley-trigger="change"				
				data-parsley-maxlength="20"
				data-parsley-maxlength-message="Only 20 characters are allowed"
				class="form-control">
              </div>
            </div><!-- /.form-group -->
			<div class="form-group">
              <label class="col-md-3">PAN Number:</label>
              <div class="col-md-4">
                <input type="text" name="pan_number"
                placeholder="Enter PAN Number"
                data-parsley-trigger="change"				
				data-parsley-maxlength="15"
				data-parsley-minlength="10"
				data-parsley-maxlength-message="Only 15 characters are allowed"
				data-parsley-minlength-message="PAN number length should be 10 or more"
				data-parsley-pattern="/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/"
				data-parsley-pattern-message="Not valid PAN number"
				class="form-control">
              </div>
            </div><!-- /.form-group -->
			<div class="form-group">
              <label class="col-md-3">FSSAI Number:</label>
              <div class="col-md-4">
                <input type="text" name="fssai_number"
                placeholder="Enter FSSAI Number"
                data-parsley-trigger="change"				
				data-parsley-maxlength="15"
				data-parsley-minlength="10"
				data-parsley-maxlength-message="Only 15 characters are allowed"
				data-parsley-minlength-message="FSSAI number length should be 10 or more"				
				class="form-control">
              </div>
            </div><!-- /.form-group -->
			 <div class="form-group">
				<label class="col-md-3">Shop Type:</label>
				<div class="col-md-4">
					<select name="shop_type"  class="form-control">
					<option  disabled>-Select-</option>
					<?php
					$sql="SELECT id,shop_type FROM tbl_shop_type ";
					$result = mysqli_query($con,$sql);
					$i=0;
					while($row = mysqli_fetch_array($result))
					{						
						$cat_id=$row['id'];
						?>
						<option value='<?=$cat_id;?>' <?php if($i==0){echo "selected";}?>><?php echo  $row['shop_type'] ;?></option>
					<?php $i++;} ?>
					</select>
				</div>
				</div>
			
            <div class="form-group">
              <label class="col-md-3">Shop Closed On Day:</label>
              <div class="col-md-4">
			  <select class="form-control" name="closedday">
			  <option value="">-Select-</option>
			  <option value="1">Monday</option>
			  <option value="2">Tuesday</option>
			  <option value="3">Wednesday</option>
			  <option value="4">Thursday</option>
			  <option value="5">Friday</option>
			  <option value="6">Saturday</option>
			  <option value="7">Sunday</option>
			  </select>
              </div>
            </div><!-- /.form-group -->
			<!-- <script src="https://code.jquery.com/jquery-1.9.1.js"></script> -->
            <div class="form-group">
              <label class="col-md-3">Shop Open Time:</label>
              <div class="col-md-4">
                <div class="input-group date">
                    <input type="text" class="form-control" name="opentime" id="datetimepicker1" autocomplete="off">                    
					<label class="input-group-addon btn" for="datetimepicker1">
					<span class="glyphicon glyphicon-time"></span>
					</label>
                </div>
              </div>
            </div><!-- /.form-group -->			     
            <div class="form-group">
              <label class="col-md-3">Shop Closed Time:</label>
              <div class="col-md-4">
                <div class='input-group date'>
                    <input type="text" class="form-control" name="closetime" id="datetimepicker2" autocomplete="off">
                   <label class="input-group-addon btn" for="datetimepicker2">
					<span class="glyphicon glyphicon-time"></span>
					</label>
                </div>
              </div>
            </div><!-- /.form-group -->			
            <div class="form-group">
              <label class="col-md-3">Latitude:</label>
              <div class="col-md-4">
                <input type="text" name="latitude"
                placeholder="Enter Latitude"
				class="form-control" min="-180" max="180" step="0.0000000000000001" 
                            data-parsley-trigger="change"                                       
                            >
              </div>
            </div><!-- /.form-group -->			
            <div class="form-group">
              <label class="col-md-3">Longitude:</label>
              <div class="col-md-4">
                <input type="text" name="longitude"
                placeholder="Enter Longitude"
				class="form-control" min="-180" max="180" step="0.0000000000000001" 
                            data-parsley-trigger="change"                                       
                            >
              </div>
            </div><!-- /.form-group -->	

           <!-- <div class="form-group">
						<label class="col-md-3">Margin(%):</label>
						<div class="col-md-4">
						<input type="text" name="shop_margin" class="form-control" onkeyup="myFunction(this)"
						placeholder="Enter Margin"
						min="0" max="100" step="0.01" 
						data-parsley-trigger="change"                         
						data-parsley-pattern="^[0-9]+?[\.0-9]*$" value=""  > 
						</div>
			</div>-->

            <div class="form-group">
							<label class="col-md-3"><b>Bank Details</b></label>
						</div>
						 <div class="form-group">
							<label class="col-md-3">Billing Name </label>
							<div class="col-md-4">
								<input type="text" 
								placeholder="Billing Name"
								data-parsley-trigger="change"
								data-parsley-maxlength="50"
								data-parsley-maxlength-message="Only 50 characters are allowed"
								name="billing_name" class="form-control">
							</div>
						</div>						
						 <div class="form-group">
							<label class="col-md-3">Account Name </label>
							<div class="col-md-4">
								<input type="text" 
								placeholder="Account Name"
								data-parsley-trigger="change"
								data-parsley-maxlength="50"
								data-parsley-maxlength-message="Only 50 characters are allowed"
								name="bank_acc_name" class="form-control">
							</div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Account Number:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Account Number"
							data-parsley-trigger="change"	
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="bank_acc_no" class="form-control">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Name:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Bank Name"
							data-parsley-trigger="change"	
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="bank_name" class="form-control">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Branch Name:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Branch Name"
							data-parsley-trigger="change"	
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="bank_b_name" class="form-control">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">IFSC Code:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter IFSC Code"
							data-parsley-trigger="change"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="bank_ifsc" class="form-control">
						  </div>
						</div>
						<!-- <div class="form-group">
						  <label class="col-md-3">GSTIN:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter GSTIN"
							data-parsley-trigger="change"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="gstnumber" class="form-control">
						  </div>
						</div>--> 
						<div class="form-group">
						  <label class="col-md-3">Status:</label>
						  <div class="col-md-4">						 			
								<select name="shop_status" id="status" class="form-control">
									<option value="0">Active</option>
									<option value="1">Inactive</option>
								</select>							
						  </div>
						</div> 
						
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
				<button name="getlatlong" id="getlatlong" type="button"  class="btn btn-primary">Get Lat Long</button>
               <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
                <a href="shops.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group -->
          </form>                                       
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
<script type="text/javascript">
            $(document).ready(function(){$("#datetimepicker1").timepicker({format: "yyyy-mm-dd",autoclose: true});});
			$(document).ready(function(){$("#datetimepicker2").timepicker({format: "yyyy-mm-dd",autoclose: true});});
            </script>
<style>
.form-horizontal{
font-weight:normal;
}
</style>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<script type="text/javascript">
function myFunction(varl) {
	if (varl.value != "") {
		var arrtmp = varl.value.split(".");
		if (arrtmp.length > 1) {
			var strTmp = arrtmp[1];
			if (strTmp.length > 2) {
				varl.value = varl.value.substring(0, varl.value.length - 1);
			}
		}
	}
}

</script>
<script>  
function fnShowStockist(id){
	if (window.XMLHttpRequest)
	{
		xmlhttp=new XMLHttpRequest();
	}
	else
	{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById("Subcategory").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","fetchstockist.php?cat_id="+id.value,true);
	xmlhttp.send();
}
function calculate_data_count(element_value){
	element_value = element_value.toString();
	var element_arr = element_value.split(',');	
	return element_arr.length;
}
function setSelectNoValue(div,select_element){
	var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled value="">-Select-</option></select>';
	document.getElementById(div).innerHTML	=	select_selement_section;
}
function fnShowCity(id_value) {	
	$("#city_div").show();	
	$("#area").html('<option value="">-Select-</option>');	
	$("#subarea").html('<option value="">-Select-</option>');	
	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city&mandatory=mandatory";
	CallAJAX(url,"div_select_city");	
}
function FnGetSuburbDropDown(id) {
	$("#area_div").show();		
	$("#subarea").html('<option value="">-Select-</option>');	
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&function_name=FnGetSubareaDropDown&mandatory=mandatory";
	CallAJAX(url,"div_select_area");
}
function FnGetSubareaDropDown(id) {	
	var suburb_str = $("#area").val();	
	$("#subarea_div").show();	
	var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea";
	CallAJAX(url,"div_select_subarea");		
}
function getUserID(UserID) {	
	var UserID1 =UserID.value;
	var usertype_info = UserID1.split(',');	
	var userole=usertype_info[1];	
	var usertype=usertype_info[0];
	//alert(userole);	
	var url = "conf_get_childs.php"; 
	jQuery.ajax({
		url: url,
		method: 'POST',
		data: 'userole='+userole+'&usertype='+usertype+'&getflag=onchange_get_serviceby_userid',
		async: false
	}).done(function (response) {	
		console.log(response);
		if(response!="no_users"){
			$('#serviceby_div').show();
			$('#service_by_user_id').html('');
			$('#service_by_user_id').append(response);
		}				
	}).fail(function () { });
}
</script>    
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLEAPIKEY;?>" type="text/javascript"></script>
<script type="text/javascript">
$( "#getlatlong" ).click(function() {
  //alert( "Handler for .click() called." );
  //$(this).find("option:selected").text();
  var curaddress=$('[name="address"]').val();
  var subarea=$('#subarea').val();
  if(subarea!=''){subarea=", "+$('#subarea').find("option:selected").text();}else{subarea='';}
   var area=$('#area').val();
  if(area!=''){area=", "+$('#area').find("option:selected").text();}else{area='';}
   var city=$('[name="city"]').val();
  if(city!=''){city=", "+$('[name="city"]').find("option:selected").text();}else{city='';}
   var state=$('[name="state"]').val();
  if(state!=''){state=", "+$('[name="state"]').find("option:selected").text();}else{state='';}
	var address = curaddress+''+subarea+''+ area+''+city+''+state;	
					//alert(address);
	//var address123 = address.replace("/^[a-zA-Z\s](\d)?$/","");
	//alert(address123);
					
  var geocoder = new google.maps.Geocoder();
	//var address = "new york";

	geocoder.geocode( { 'address': address}, function(results, status) {

	  if (status == google.maps.GeocoderStatus.OK) {
		var latitude = results[0].geometry.location.lat();
		var longitude = results[0].geometry.location.lng();		
		$('[name="latitude"]').val(latitude);
		$('[name="longitude"]').val(longitude);
	  } else{
		  alert("Not getting proper latitude,longitude!");
	  }
	}); 
});        






//new code
 
function getStateCityAreaFromPincode(varl) {	
	if (varl.value != "" && varl.value.length>5) {		
		var zip = varl.value;
		var lat;
		var lng;
		
		//$ch = curl_init();
       // curl_setopt($ch, CURLOPT_URL, "http://maps.googleapis.com/maps/api/geocode/json?address=" . $geoaddress . "&sensor=true");
       // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       // $geocode = curl_exec($ch);
       // $output2 = json_decode($geocode);
		
		//curl -X GET "https://api.data.gov.in/resource/6176ee09-3d56-4a3b-8115-21841576b2f6?api-key=579b464db66ec23bdd000001cdd3946e44ce4aad7209ff7b23ac571b&format=json&offset=0" -H "accept: application/json"


		//var url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + zip + "&key=AIzaSyDetzH728Nv8PSzU_2gQqZWsukVgTyPE8A";
			//var url="https://postalpincode.in/api/pincode/"+zip+"";
			var url= "https://api.data.gov.in/resource/6176ee09-3d56-4a3b-8115-21841576b2f6?api-key=579b464db66ec23bdd000001cdd3946e44ce4aad7209ff7b23ac571b&format=json";
			$.get(url, function(loc) {
				console.log(loc);
				/* for(var i=0;i<loc.results.length;i++) {
					var address = loc.results[i].formatted_address;
					var city = address.split(',', 1)[0];					
					console.log(city);
					console.log(city);
				} */
			});
		/* var geocoder = new google.maps.Geocoder();
		geocoder.geocode({ 'address': zip }, function (results, status) {
			console.log(results);
			if (status == google.maps.GeocoderStatus.OK) {
				geocoder.geocode({'latLng': results[0].geometry.location}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (results[1]) {
						var loc = getCityState(results);
						//console.log(loc);
					}
				}
			});
			}
		});  */
		
		//alert($('[name="state"]').find("option:selected").text());
		//$('#serviceby_div').show();	
		//alert(loc);
	}
}
function getCityState(results)
    {
        var a = results[0].address_components;
		//console.log(a);
        var city, state;
        for(i = 0; i <  a.length; ++i)
        {
           var t = a[i].types;
           if(compIsType(t, 'administrative_area_level_1'))
              state = a[i].long_name; //store the state
           else if(compIsType(t, 'locality'))
              city = a[i].long_name; //store the city
        }
        return (city + ', ' + state)
    }

function compIsType(t, s) { 
       for(z = 0; z < t.length; ++z) 
          if(t[z] == s)
             return true;
       return false;
    }

</script> 