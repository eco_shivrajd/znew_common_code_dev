<?php
ini_set('display_errors', 1);
include "../includes/grid_header.php";
include "../includes/commonManage.php";
$commonObj = new commonManage($con, $conmain);
$commonObjctype = $commonObj->log_get_commonclienttype($con, $conmain);
$user_id = $_SESSION[SESSION_PREFIX . 'user_id'];
?>
    <!-- END HEADER -->

    <body class="page-header-fixed page-quick-sidebar-over-content ">
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "ManageProducts";
        $activeMenu = "Product";
        include "../includes/sidebar.php";
        ?>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <div class="page-content">
                        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                        <!-- /.modal -->
                        <h3 class="page-title">
                    Products
                </h3>
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="#">Products</a>
                                </li>
                            </ul>
                        </div>
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN PAGE CONTENT-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet box blue-steel">
                                    <div class="portlet-title">
                                        <div class="caption">Products Listing</div>
                                        <?php if ($ischecked_add==1) {  ?>
                                        <a href="product-add.php" class="btn btn-sm btn-default pull-right mt5 ">Add Product</a>
                                        <?php } ?>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Category
                                                    </th>
                                                    <th>
                                                        Product Name
                                                    </th>
                                                    <th>
                                                        Product HSN
                                                    </th>
                                                    <th>
                                                        Product Price
                                                    </th>
                                                    <th>
                                                        Product Variant
                                                    </th>
                                                    <th>
                                                        Product Image
                                                    </th>
                                                    <th>
                                                        Action
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                 <?php
                                         $get_product_sql = "SELECT id FROM `tbl_product` WHERE isdeleted != 1   order by id ASC";
                                        $result_product = mysqli_query($con, $get_product_sql);
                                         /*  $get_product_sql = 'CALL GetAllProduct()';
                                           $result_product = mysqli_query($con, $get_product_sql);*/
                                           //  var_dump($result_product1);
                                           /*  while($row_product = mysqli_fetch_array($result_product)){
                                           echo $id=$row_product['id']."--";
                                        }
                                             exit();*/
                                        $row__prod_count = mysqli_num_rows($result_product);
                                        $all_product_ids=array();
                                        while($row_product = mysqli_fetch_array($result_product)){
                                            $all_product_ids[]=$row_product['id'];
                                        }                                        
                                        //echo "<pre>";print_r($all_product_ids );//die();
                                        for($i=0;$i<$row__prod_count;$i++){
                                            $sql_get_varient = "SELECT * FROM `pcatbrandvariant` where product_id='".$all_product_ids[$i]."'";
                                            $result_varient = mysqli_query($con, $sql_get_varient);
                                            $row_var_count2 = mysqli_num_rows($result_varient);
                                            
                                            
                                            $product_hsn = '';                                          
                                            $product_price = '';
                                            $product_variant1 = '';$variant1_unit_name = '';
                                            $product_variant2 = ''; $variant2_unit_name = '';
                                            $product_image = '';
                                            
                                             while ($row_prod_var = mysqli_fetch_array($result_varient)) {
                                                //echo "<pre>";print_r($row2);
                                                $product_single=array();
                                                $added_by_userid=$row_prod_var['added_by_userid'];  
                                                $category_id=$row_prod_var['category_id'];                                              
                                                $product_id=$row_prod_var['product_id'];
                                                $category_name=$row_prod_var['category_name'];                                              
                                                $product_name=$row_prod_var['product_name'];                                                
                                                
                                                $product_hsn.=$row_prod_var['product_hsn']."<br>";
                                                $product_price.=$row_prod_var['product_price']."<br>";
                                                $product_variant1.=$row_prod_var['product_variant1']."-".
                                                $row_prod_var['variant1_unit_name']." ".
                                                $row_prod_var['product_variant2']."-".
                                                $row_prod_var['variant2_unit_name']."<br> ";                                                
                                                
                                                
                                                $product_image.=$row_prod_var['product_image'];                                             
                                                $product_single1[]=$product_single;
                                            } ?>
                                            <tr class="odd gradeX">
    <td>
        <?php echo $category_name; ?>
    </td>
    <td>
       
         <?php if ($ischecked_edit==1) { ?>
            <a href="product1.php?id=<?php echo $product_id; ?>" >
            <?php echo $product_name; ?>
            </a>
            <?php } else {  echo $product_name; } ?>
    </td>
    <td>
        <?php echo $product_hsn; ?>
    </td>
    <td>  
            <?php echo $product_price; ?>
    </td>
    <td>
        <?php echo $product_variant1; ?> 
    </td>
    <td>
      

                     <?php
                                                    $sqlvarimg = "SELECT productimage,price,variant_1 FROM tbl_product_variant where productid='$product_id'";
                                                    $resultvarimg = mysqli_query($con, $sqlvarimg);
                                                    while ($rowvarimg = mysqli_fetch_array($resultvarimg)) {
                                                        if (!empty($rowvarimg['productimage'])) {
                                                            ?>
                                                            <img src="upload/<?php echo COMPANYNM; ?>_upload/<?php echo $rowvarimg['productimage']; ?>" alt=<?php echo $rowvarimg['productimage']; ?> width="100px" />
                                                        <?php }
                                                    }
                                                    ?>  
    </td>
    <td>
        <?php if ($ischecked_edit==1) { ?>
            <a href="product1.php?id=<?php echo $product_id; ?>" class="btn btn-xs btn-primary ">
            <span class="glyphicon glyphicon-edit"></span>
            </a>
            <?php } else {  echo "-"; } ?>

         <?php if ($row__prod_count > 1 && $ischecked_delete==1) { ?>
            <a class="btn btn-xs btn-danger" onclick="javascript: deleteProduct(<?= $category_id; ?>,<?= $product_id; ?>)">
           <span class="glyphicon glyphicon-trash"></span>
            </a>
            <?php } else {  echo "-"; } ?>



    </td>
</tr>                                   
            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- END PAGE CONTENT-->
                    </div>
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->

                <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php include "../includes/grid_footer.php" ?>
            <!-- END FOOTER -->
            <script>
                function deleteProduct(cat_id, product_id) {
                    if (confirm('Are you sure that you want to delete this Product?')) {
                        CallAJAX('ajax_product_manage.php?action=delete_product&cat_id=' + cat_id + '&product_id=' + product_id);
                    }
                }

                function CallAJAX(url) {
                    if (window.XMLHttpRequest) {
                        xmlhttp = new XMLHttpRequest();
                    } else {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            alert('Product deleted successfully');
                            location.reload();
                        }
                    }
                    xmlhttp.open("GET", url, true);
                    xmlhttp.send();
                }
            </script>
    </body>
    <!-- END BODY -->

    </html>