<!-- BEGIN HEADER -->
<?php
include "../includes/grid_header.php";
$user_id = $_SESSION[SESSION_PREFIX . 'user_id'];
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "ManageProducts";
        $activeMenu = "Categories";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

                <!-- /.modal -->

                <h3 class="page-title">
                    Categories
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="#">Categories</a>
                        </li>
                    </ul>

                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">


                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">
                                    Categories Listing
                                </div>
                                <?php if ($ischecked_add==1)  {
                                ?>
                                <a class="btn btn-sm btn-default pull-right mt5" href="categories-add.php">
                                    Add Category
                                </a>
                                <?php } ?>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">

                                <table class="table table-striped table-bordered table-hover" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th>
                                                Brand
                                            </th>
                                            <th>
                                                Category Name
                                            </th>
                                            <th>
                                                Action
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                      $get_user_sql = "SELECT parent_ids FROM `tbl_user` WHERE id = '".$user_id."' limit 1";
                                    $result_user = mysqli_query($con, $get_user_sql);
                                    $row_user = mysqli_fetch_assoc($result_user);
                                    $sql = "SELECT tc.id ,tc.categorynm,tc.brandid,tc.added_by_userid
                                    FROM `tbl_category` tc
                                    WHERE tc.isdeleted != 1 AND 
                                    ((find_in_set(tc.added_by_userid,'".$row_user['parent_ids']."') <> 0) 
                                    or tc.added_by_userid =  '".$user_id."')
                                    order by id ASC";

                                        $result1 = mysqli_query($con, $sql);
                                        $row_count = mysqli_num_rows($result1);
                                        while ($row = mysqli_fetch_array($result1)) {
                                            $added_by_userid = $row['added_by_userid'];
                                            echo '<tr class="odd gradeX">
					<td>';

                                            $id = $row['brandid'];
                                            $sql1 = "SELECT * FROM `tbl_brand` where id='$id'";
                                            $result2 = mysqli_query($con, $sql1);
                                            while ($row1 = mysqli_fetch_array($result2)) {

                                                echo fnStringToHTML($row1['name']);
                                            }
                                            echo '</td><td>';
                                             if($ischecked_edit==1 && $added_by_userid == $user_id){ 
                                            echo '<a href="categories1.php?id=' . base64_encode($row['id']) . '">' . fnStringToHTML($row['categorynm']) . '</a>';
                                             }else{
                                                 echo '' . fnStringToHTML($row['categorynm']) . '';
                                             }
						                  echo '</td><td>';
                                         if ($ischecked_edit==1 && $added_by_userid == $user_id) 
                                            {
                                               echo '<a title="Edit" href="categories1.php?id=' . base64_encode($row['id']) . '" class="btn btn-xs btn-primary "><span class="glyphicon glyphicon-edit"></span></a>'; 
                                            } else {
                                                 echo '<a title="Can Not Edit This Record" href="#" class="btn btn-xs btn-primary" disabled><span class="glyphicon glyphicon-edit"></span></a>';
                                            }
                                            if ($row_count > 1 && $ischecked_delete==1 && $added_by_userid == $user_id) 
                                            {
                                               echo '<a title="Delete" onclick="javascript: deleteCategory(' . $id . ',' . $row['id'] . ')" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></a>'; 
                                            } else {
                                                echo '<a title="Can Not Delete This Record" onclick="#" class="btn btn-xs btn-danger" disabled><span class="glyphicon glyphicon-trash"></span></a>'; 
                                            }
                                            echo '</td>
							</tr>';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <?php include "../includes/grid_footer.php" ?>
    <!-- END FOOTER -->
    <script>
        function deleteCategory(brand_id, cat_id) {
            var response = ajax_call('ajax_product_manage.php?action=check_product_available');
            if (response != 'more_than_one_product') {
                if (response != 'no_product') {
                    var record = response.split("####");
                    var product = record[0];
                    var category = record[1];
                    var brand = record[2];
                    if (cat_id == category) {
                        alert('This Category cannot be deleted as only one Product is there and which belong to this Category.');
                        return false;
                    } else {
                        if (confirm('Products under this Category will also get deleted. Are you sure that you want to delete this Category?')) {
                            var response = ajax_call('ajax_product_manage.php?action=delete_category&brand_id=' + brand_id + '&cat_id=' + cat_id);
                            if (response == 'category_deleted') {
                                alert('Category deleted successfully');
                                location.reload();
                            }
                        }
                    }
                }
            } else {

                if (confirm('Products under this Category will also get deleted. Are you sure that you want to delete this Category?')) {
                    var response = ajax_call('ajax_product_manage.php?action=delete_category&brand_id=' + brand_id + '&cat_id=' + cat_id);
                    if (response == 'category_deleted') {
                        alert('Category deleted successfully.');
                        location.reload();
                    }
                }
            }
        }
        function ajax_call(url) {
            var response;
            $.ajax({
                type: "GET",
                url: url,
                async: false,
                success: function (data) {
                    response = data;
                },
                error: function (xhr, status, error) {
                    //alert(xhr.responseText);
                }
            });
            return response;
        }
    </script>
</body>
<!-- END BODY -->
</html>