<!-- BEGIN HEADER -->
<?php include "../includes/header.php"?>
<!-- END HEADER -->
<script type="text/javascript">
	function fnSelectionBoxTest()
	{
		var str = $( "form" ).serialize();	
		document.getElementById('hdnSelrange').value=document.getElementById('selTest').value;
		if(document.getElementById('selTest').value == '3')
		{
		   document.getElementById('date-show').style.display = "block";
		}
		else
		document.frmReport.submit();
	}
</script>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Reports"; $activeMenu = "SalesStatistic";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">Reports History <small>Reports & Statistics</small></h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Reports</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<form class="form-horizontal" name="frmReport" id="frmReport" method="post"><!-- action="searchreport.php" -->
                        <div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Options:</label>
							<div class="col-sm-3">						
								<select class="form-control" name="selTest" id="selTest" onChange="fnSelectionBoxTest()">
								<option value='0'>-Select-</option>
								<option value="4" <?php if($_REQUEST['selTest']=="4")echo 'selected';?>>Today</option>
								<option value='1' <?php if($_REQUEST['selTest']=="1")echo 'selected';?>>Weekly</option>
								<option value='2' <?php if($_REQUEST['selTest']=="2")echo 'selected';?>>Current month</option>
								<option value='3' <?php if($_REQUEST['selTest']=="3")echo 'selected';?>>From specific date</option>
								</select>
								<input type="hidden" name="hdnSelrange" id="hdnSelrange">
							</div>
                         </div>
                         <div class="form-group">
						 
						 <?php 
						 if($_REQUEST['selTest']=="3"){
							 $dtdisp="display:block;";
							 $frmdate = $_REQUEST['frmdate'];
							 $todate = $_REQUEST['todate'];
						 }
						 else
						 {
							 $dtdisp="display:none;";
							 $frmdate = "";
							 $todate = "";
						 }
						 ?>
						<div id="date-show" style="<?php echo $dtdisp;?>">
							<label for="inputEmail3" class="col-sm-2 control-label">From Date:</label>
							<div class="col-md-3">
								<div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
								<input type="text" class="form-control" name="frmdate" id="frmdate" value="<?php echo $frmdate;?>" readonly>
								<span class="input-group-btn">
								<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
								</span>
								</div>
							</div>
							<label for="inputEmail3" class="col-sm-1 control-label">To Date:</label>
							<div class="col-md-6">
								<div class="col-md-6">
									<div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
									<input type="text" class="form-control" name="todate" id="todate" value="<?php echo $todate;?>" readonly>
									<span class="input-group-btn">
									<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
									</span>
									</div>
								</div>
								<div class="col-sm-4">
									<button type="submit" name="btnsubmit" id="btnsubmit" class="btn btn-primary">Submit</button>
								</div>
							</div>                          
						</div>                          
					  </div> 
				  </form>                           
			  </div>
		   </div>	
			<?php
			$condnsearch="";
			$selperiod=$_REQUEST['selTest'];
			if($selperiod!=0){
				switch($selperiod){
					case 1:
						$condnsearch=" AND yearweek(order_date) = yearweek(curdate())";
					break;
					case 2:
						$condnsearch=" AND date_format(order_date, '%m')=date_format(now(), '%m')";
					break;
					case 3:
						if($_REQUEST['todate']!="")
						$todat=$_REQUEST['todate'];
						else
						$todat=date("d-m-Y");
						$condnsearch=" AND (date_format(order_date, '%d-%m-%Y') >= '".$_REQUEST['frmdate']."' AND date_format(order_date, '%d-%m-%Y') <= '".$todat."')";
						
						$condnsearch=" AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('".$_REQUEST['frmdate']."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
						
					break;
					case 4:
						$condnsearch=" AND DATE_FORMAT(order_date,'%Y-%m-%d')=DATE(NOW())";
					break;
					case 0:
						$condnsearch="";
					break;
					default:
						$condnsearch="";
					break;

				}
			}
			?>
			<div class="row minht">
			
				<? $displayBox=0;
				if($_SESSION[SESSION_PREFIX."user_type"]=="Admin") { ?>
				<div class="col-md-6">
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-puzzle"></i>Superstockist Wise Sales Order
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-striped table-hover table-bordered">
									<thead>
										<tr>
											<th width="50%">
												 Name
											</th>
											<th width="50%">
												 Total Sales <i aria-hidden='true' class='fa fa-inr'></i>
											</th>											
										</tr>
									</thead>
									<tbody>
									<?php

									$sql="SELECT oa.superstockistnm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type <> 'free'))
									WHERE oa.id!=0 ".$condnsearch." GROUP BY oa.superstockistnm";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										echo "<tr>
										<td>".$row['Name']."</a></td>
										<td align='right'>".number_format($row['Total_Sales'], 2, '.', '')." 
										</td></tr>";
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<? $displayBox++; } ?>
				
				<? if($_SESSION[SESSION_PREFIX."user_type"]!="Distributor") { ?>
			
				<div class="col-md-6">
					<!-- BEGIN PORTLET-->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bar-chart-o"></i>Stockist Wise Sales Order
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-striped table-hover table-bordered">
									<thead>
										<tr>
											<th width="50%">Name</th>
											<th width="50%">Total Sales <i aria-hidden='true' class='fa fa-inr'></i></th>
										</tr>
									</thead>
									<tbody>
									<?php									
									switch($_SESSION[SESSION_PREFIX.'user_type']){
										case "Admin":
											$sql="SELECT oa.distributornm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type <> 'free')) WHERE oa.id!=0 ".$condnsearch." GROUP BY oa.distributornm";
										break;
										case "Superstockist":
											$sql="SELECT oa.distributornm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type <> 'free')) WHERE oa.id!=0 AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." GROUP BY oa.distributornm";
										break;
										 
										break;
									}
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										echo "<tr> <td>
										".$row['Name']."</a>
										</td>
										<td align='right'>".number_format($row['Total_Sales'], 2, '.', '')."
										</td></tr>";
									} ?>										
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
				
				<? $displayBox++; }
				if($displayBox==2) {
					$displayBox=0; ?>
					<div class="clearfix"></div>
				<? } ?>
				 
				<div class="col-md-6">
					 
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bar-chart-o"></i>Sales Person Wise Sales Order
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-striped table-hover table-bordered">
									<thead>
										<tr>
											<th width="50%">
												 Name
											</th>
											<th width="50%">
												 Total Sales <i aria-hidden='true' class='fa fa-inr'></i>
											</th>
										</tr>
									</thead>
									<tbody>
									<?php
									switch($_SESSION[SESSION_PREFIX.'user_type']){
										case "Admin":
											$sql="SELECT oa.salespersonnm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type <> 'free'))
											WHERE oa.id!=0 ".$condnsearch." GROUP BY oa.salespersonnm";
										break;
										case "Superstockist":
											$sql="SELECT oa.salespersonnm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type <> 'free')) WHERE oa.id!=0 AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." GROUP BY oa.salespersonnm";
									
										break;
										case "Distributor":
											// Distributor
											$sql="SELECT oa.salespersonnm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type <> 'free'))
											WHERE oa.id!=0 AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." GROUP BY oa.salespersonnm";
										break;
									}
									
									
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										echo "<tr>
											<td>
												 ".$row['Name']."</a>
											</td>
											<td align='right'>".number_format($row['Total_Sales'], 2, '.', '')."
											</td></tr>";
									}
									?>
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
 				</div>
				
				<? $displayBox++;
				if($displayBox==2) {
					$displayBox=0; ?>
					<div class="clearfix"></div>
				<? } ?>
				<div class="col-md-6">
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-puzzle"></i>Shop Wise Sales Order
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-striped table-hover table-bordered">
									<thead>
										<tr>
											<th width="50%">
												 Name
											</th>
											<th width="50%">
												 Total Sales <i aria-hidden='true' class='fa fa-inr'></i>
											</th>
											
										</tr>
									</thead>
									<tbody>
									<?php										
									switch($_SESSION[SESSION_PREFIX.'user_type']){
										case "Admin":
											$sql="SELECT oa.shopnm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type <> 'free')) WHERE oa.id!=0 ".$condnsearch." GROUP BY oa.shopnm";
										break;
										case "Superstockist":
											$sql="SELECT oa.shopnm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type <> 'free')) WHERE oa.id!=0 AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." GROUP BY oa.shopnm";
										break;
										case "Distributor":
											$sql="SELECT oa.shopnm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type <> 'free'))
											WHERE oa.id!=0 AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." GROUP BY oa.shopnm";
										break;
									}									
									
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										echo "<tr>
											<td>
												 ".$row['Name']."</a>
											</td>
											<td align='right'>".number_format($row['Total_Sales'], 2, '.', '')."
											</td></tr>";
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				
				<? $displayBox++;
				if($displayBox==2) {
					$displayBox=0; ?>
					<div class="clearfix"></div>
				<? } ?>
				<div class="col-md-6">
					
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bar-chart-o"></i>Brand Wise Sales Order
							</div>
						
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-striped table-hover table-bordered">
									<thead>
										<tr>
											<th width="33%">
												 Name
											</th>
											<th width="33%">
												 Total Sales <i aria-hidden='true' class='fa fa-inr'></i>
											</th>
											 
										</tr>
									</thead>
									<tbody>
									<?php
									switch($_SESSION[SESSION_PREFIX.'user_type']){
										case "Admin":
											$sql="SELECT oa.brandnm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type <> 'free')) WHERE oa.id!=0 ".$condnsearch." GROUP BY oa.brandnm";
											break;
										case "Superstockist":
											$sql="SELECT oa.brandnm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type <> 'free')) WHERE oa.id!=0 AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." GROUP BY oa.brandnm";
											break;
										case "Distributor":
											// Distributor
											$sql="SELECT oa.brandnm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type <> 'free'))
											WHERE oa.id!=0 AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." GROUP BY oa.brandnm";
										break;
									}
									
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										echo "<tr>
											<td>
												 ".$row['Name']."
											</td>
											<td align='right'>".number_format($row['Total_Sales'], 2, '.', '')."
											</td></tr>";
									}
									?>					
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
				
				<? $displayBox++;
				if($displayBox==2) {
					$displayBox=0; ?>
					<div class="clearfix"></div>
				<? } ?>
				<div class="col-md-6">
					<!-- BEGIN PORTLET-->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bar-chart-o"></i>Category Wise Sales Order
							</div>
						
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-striped table-hover table-bordered">
									<thead>
										<tr>
											<th width="50%">
												 Category
											</th>
											<th width="50%">
												 Total Sales <i aria-hidden='true' class='fa fa-inr'></i>
											</th>
										</tr>
									</thead>
									<tbody>									
									<?php
									switch($_SESSION[SESSION_PREFIX.'user_type']){
										case "Admin":
											$sql="SELECT oa.categorynm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type <> 'free')) WHERE oa.id!=0 ".$condnsearch." GROUP BY oa.categorynm";
											break;
										case "Superstockist":
											$sql="SELECT oa.categorynm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type <> 'free')) WHERE oa.id!=0 AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." GROUP BY oa.categorynm";
											break;
										case "Distributor":
											// Distributor
											$sql="SELECT oa.categorynm as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales FROM tbl_order_app oa INNER JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type <> 'free')) WHERE oa.id!=0 AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." GROUP BY oa.categorynm";
										break;
									}
									
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										echo "<tr>
											<td>
												 ".$row['Name']."
											</td>
											<td align='right'>".number_format($row['Total_Sales'], 2, '.', '')."
											</td></tr>";
									}
									?>
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
 				
			</div>			
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>