<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/userManage.php";
include "../includes/shopManage.php";
include "../includes/testManage.php";
$userObj 	= 	new userManager($con,$conmain);
$shopObj 	= 	new shopManager($con,$conmain);
$testObj 	= 	new testManager($con,$conmain);
$id=base64_decode($_GET['id']);
/* $access = $shopObj->checkShopAccess($id);
if($access == 0)
{
	echo '<script>location.href="shops.php";</script>';
} */
$mandatory_mark = '';
//if($_SESSION[SESSION_PREFIX."user_type"]=="Admin")  {
	$mandatory_mark = '<span class="mandatory">*</span>';
//}
?>
<!-- END HEADER -->
<?php
if(isset($_POST['submit']))
{	
	$shopObj->updateShopDetails($id);
	echo '<script>alert("Shop has been updated successfully.");location.href="shops.php";</script>';
}
?>

<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Shops";
	include "../includes/sidebar.php";

	    $sql1="SELECT `id` as page_id FROM tbl_pages where php_page_edit='".$php_page_name."'";
		$result1 = mysqli_query($con,$sql1);		
		$row_url = mysqli_fetch_assoc($result1);
		$page_id_url = $row_url['page_id'];

		$sql12="SELECT ischecked_edit FROM tbl_action_profile where profile_id='".$profile_id."' AND  page_id='".$page_id_url."'";
		$result12 = mysqli_query($con,$sql12);
		$row_url_edit = mysqli_fetch_assoc($result12);
		$ischecked_edit_url = $row_url_edit['ischecked_edit'];
		if ($ischecked_edit_url == 0 && $ischecked_edit_url!='') 
		{
		session_set_cookie_params(0);
		session_start();
		session_destroy();
		echo '<script>location.href="../login.php";</script>';
		exit;
		}
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Shops
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="shops.php">Shops</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Shop</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Shop
							</div>
							
						</div>
						<div class="portlet-body">
						<?php if($_SESSION[SESSION_PREFIX."user_type"]=="Admin")  { ?>
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>   
						<?php } ?>
						  <?php
						//$id=$_GET['id'];
						$row1 = $shopObj->getShopDetails($id);
						$shopObj->updateViewStatus($id);
						$stockist_data = $shopObj->getShopAssignToUsers($id);
						$row1['stockist_id'] = $stockist_data['user_id'];
						?>                         
	<form class="form-horizontal" role="form" method="post" data-parsley-validate="" action="">       
            <div class="form-group">
              <label class="col-md-3">Shop Name:<?=$mandatory_mark;?></label>

              <div class="col-md-4">
                <input type="text" name="name"
                placeholder="Enter Shop Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter shop name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				 
				class="form-control" value="<?php echo fnStringToHTML($row1['name'])?>" >
              </div>
            </div><!-- /.form-group -->
			<?php  
				$seesion_user_type=$_SESSION[SESSION_PREFIX.'user_type'];
				$seesion_user_id=$_SESSION[SESSION_PREFIX.'user_id']; 
			?>
			<div class="form-group">
				<label class="col-md-3">Select Service By User Type:<span class="mandatory">*</span></label>
				<div class="col-md-4">
				<select name="service_by_user_type" id="service_by_user_type" 
				class="form-control" onchange="getServiceByUserID(this)" required>
				<option value="">-- Select --</option>
				<?php
				 $result1 = $testObj->getUsertype_underme_forshop_byuserid($seesion_user_id);
				// echo "<pre>";print_r($result1);
				 while ($row = mysqli_fetch_array($result1)){ 	
					
				 ?>
				  <option value="<?php echo $row['user_type'].",".$row['user_role'].",".$row['profile_id'];?>" 
				  <?php if($row1['service_by_usertype_name']==$row['user_type']){echo "selected";} ?>><?php echo $row['user_type'];?></option>
				 <?php   } ?> 
				</select>
				</div>
			</div>                      
			<div class="form-group" id="serviceby_div" name="serviceby_div" >
				  <label class="col-md-3">Service By User:<span class="mandatory">*</span></label>
				  <div class="col-md-4" >
				  <select name="service_by_user_id" id="service_by_user_id" 
				  data-parsley-trigger="change" class="form-control" required>
						<option selected value="<?php echo $row1['service_by_user_id'];?>">
						<?php echo $row1['service_by_user_name'];?></option>										
					</select>
				  </div>
			</div><!-- /.form-group -->
				
            <div class="form-group">
              <label class="col-md-3">Address:<?=$mandatory_mark;?></label>

              <div class="col-md-4">
                <textarea name="address" rows="4" 
				placeholder="Enter Address"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter address"
				data-parsley-maxlength="200"
				data-parsley-maxlength-message="Only 200 characters are allowed"
				class="form-control" ><?php echo fnStringToHTML($row1['address'])?></textarea>
              </div>
            </div><!-- /.form-group -->
            
         <div class="form-group">
			  <label class="col-md-3">State:<?=$mandatory_mark;?></label>
			  <div class="col-md-4">
				<?php
					$sql_state="SELECT * FROM tbl_state where country_id=101";					
					$result_state = mysqli_query($con,$sql_state);		
				?>
				<select name="state" class="form-control" onChange="fnShowCity(this.value)" 
				data-parsley-trigger="change"
				data-parsley-required="#true" 
				data-parsley-required-message="Please select State"
				>
					<?php
						if($row1['state_ids'] == 0)
							echo "<option value=''>-Select-</option>";
						while($row_state = mysqli_fetch_array($result_state))
						{
							$selected = "";
							if($row_state['id'] == $row1['state'])
								$selected = "selected";				
						
							echo "<option value='".$row_state['id']."' $selected>" . fnStringToHTML($row_state['name']) . "</option>";
						}
					?>
				</select>
			  </div>
			</div><!-- /.form-group -->

			<div class="form-group" id="city_div">
			  <label class="col-md-3">District:<?=$mandatory_mark;?></label>
			  <div class="col-md-4" id="div_select_city">
				<?php
					$selected = '';
					if($row1['city'] != '')
					{
						$sql_city="SELECT * FROM tbl_city where state_id='".$row1['state']."' ORDER BY name";
						
						$result_city = mysqli_query($con,$sql_city);						
						while($row_city = mysqli_fetch_array($result_city))
						{
							$cat_id=$row_city['id'];
							$selected = "";		
							if($row_city['id'] == $row1['city'])
								$selected = "selected";			
						
							$option.= "<option value='$cat_id' $selected>".$row_city['name']."</option>";
						}
					}
					else
						$option = "<option value=''>-Select-</option>";
					
							
				?>
				<select name="city" class="form-control"  
				data-parsley-trigger="change"
				data-parsley-required="#true" 
				data-parsley-required-message="Please select District" >
				<?=$option;?></select>
			  </div>
			</div><!-- /.form-group --> 

			<div class="form-group" id="area_div">
			  <label class="col-md-3">Taluka:<?=$mandatory_mark;?></label>
			<?php			
				$sql_area="SELECT `id`, `cityid`, `stateid`, `suburbnm` FROM tbl_area WHERE cityid = ".$row1['city']." and isdeleted!='1'";				
				$result_area = mysqli_query($con,$sql_area);
				$selected = '';				
			?>
			  <div class="col-md-4"  id="div_select_area">
			  <select name="area" id="area" class="form-control" onchange="FnGetSubareaDropDown(this)" 
				data-parsley-trigger="change"
				data-parsley-required="#true" 
				data-parsley-required-message="Please select Taluka" >	
				<option value="">-Select-</option>
				<?php
				while($row_area = mysqli_fetch_array($result_area))
				{	
					$selected = "";		
					if($row_area['id'] == $row1['suburbid'])
						$selected = "selected";						
					
					echo "<option value='".$row_area['id']."' $selected>" . fnStringToHTML($row_area['suburbnm']) . "</option>";
				}
				?>
				</select>
			  </div>
			</div><!-- /.form-group -->
			<div class="form-group" id="subarea_div">
			  <label class="col-md-3">Subarea:</label>
			  <?php
					$sql_subarea="SELECT `subarea_id`, `state_id`, `city_id`, `suburb_id`, `subarea_name` 
					FROM tbl_subarea WHERE suburb_id = ".$row1['suburbid']." and isdeleted!='1' ";	
					$result_subarea = mysqli_query($con,$sql_subarea);
				
					
			  ?>
			  <div class="col-md-4" id="div_select_subarea">
			  <select name="subarea" id="subarea" data-parsley-trigger="change" class="form-control" >	
					<option value="">-Select-</option>
					<?php											
						while($row_subarea = mysqli_fetch_array($result_subarea))
						{					
							$selected = '';
							if($row_subarea['subarea_id'] == $row1['subarea_id'])
								$selected = "selected";
							
							echo "<option value='".$row_subarea['subarea_id']."' $selected>" . fnStringToHTML($row_subarea['subarea_name']) . "</option>";
						}
					?>
				</select>
			  </div>
			</div><!-- /.form-group -->      
            <div class="form-group">
              <label class="col-md-3">Contact Person 1:<?=$mandatory_mark;?></label>

              <div class="col-md-4">
                <input type="text" name="contact_person" 
				placeholder="Enter Contact Person Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter contact person name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"				 
				class="form-control" value="<?php echo fnStringToHTML($row1['contact_person'])?>" >
              </div>
            </div><!-- /.form-group -->
            <div class="form-group">
              <label class="col-md-3">Mobile Number 1:<?=$mandatory_mark;?></label>

              <div class="col-md-4">
                <input type="text" name="mobile" 
				 placeholder="Enter Mobile Number"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter mobile number"
				data-parsley-maxlength="15"
				data-parsley-minlength="10"
				data-parsley-maxlength-message="Only 15 characters are allowed"
				data-parsley-minlength-message="Mobile number length should be 10 or more"
				data-parsley-pattern="^(?!\s)[0-9!@#$%^&*+_=><,./:' ]*$"
				data-parsley-pattern-message="Alphabets are not allowed"
				class="form-control" value="<?php echo $row1['mobile']?>" 
				>
              </div>
            </div><!-- /.form-group -->
            <div class="form-group">
              <label class="col-md-3">Contact Person 2:</label>

              <div class="col-md-4">
                <input type="text" 
				placeholder="Enter Contact Person Name"
                data-parsley-trigger="change"				
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				name="contact_person_other"class="form-control" value="<?php echo fnStringToHTML($row1['contact_person_other'])?>" 
				>
              </div>
            </div><!-- /.form-group -->
            
            
             <div class="form-group">
              <label class="col-md-3">Mobile Number 2:</label>

              <div class="col-md-4">
                <input type="text" name="mobile_number_other"
                placeholder="Enter Mobile Number"
                 data-parsley-trigger="change"				
				data-parsley-maxlength="15"
				data-parsley-minlength="10"
				data-parsley-maxlength-message="Only 15 characters are allowed"
				data-parsley-minlength-message="Mobile number length should be 10 or more"
				data-parsley-pattern="^(?!\s)[0-9!@#$%^&*+_=><,./:' ]*$"
				data-parsley-pattern-message="Alphabets are not allowed"
				class="form-control" value="<?php echo $row1['mobile_number_other'] ?>"
				>
              </div>
            </div><!-- /.form-group -->
			<div class="form-group">
              <label class="col-md-3">Landline:</label>

              <div class="col-md-4">
                <input type="text" name="landline_number_other"
                placeholder="Enter Landline Number"
                 data-parsley-trigger="change"				
				data-parsley-maxlength="15"
				data-parsley-minlength="10"
				data-parsley-maxlength-message="Only 15 characters are allowed"
				data-parsley-minlength-message="Landline number length should be 10 or more"
				data-parsley-pattern="^(?!\s)[0-9!@#$%^&*+_=><,./:' ]*$"
				data-parsley-pattern-message="Alphabets are not allowed"
				class="form-control" value="<?php echo $row1['landline_number_other'] ?>"
				>
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <label class="col-md-3">GST Shop Number:</label>
              <div class="col-md-4">
                <input type="text" name="gst_number"
                placeholder="Enter GST Shop Number"
                data-parsley-trigger="change"				
				data-parsley-maxlength="20"
				data-parsley-maxlength-message="Only 20 characters are allowed"
				class="form-control" value="<?php echo fnStringToHTML($row1['gst_number'])?>"
				>
              </div>
               </div> 



<div class="form-group">
              <label class="col-md-3">PAN Number:</label>
              <div class="col-md-4">
                <input type="text" name="pan_number"
                placeholder="Enter PAN Number"
                data-parsley-trigger="change"				
				data-parsley-maxlength="15"
				data-parsley-minlength="10"
				data-parsley-maxlength-message="Only 15 characters are allowed"
				data-parsley-minlength-message="PAN number length should be 10 or more"
				data-parsley-pattern="/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/"
				data-parsley-pattern-message="Not valid PAN number"
				class="form-control" value="<?php echo fnStringToHTML($row1['pan_number'])?>"
				>
              </div>
            </div><!-- /.form-group -->
			<div class="form-group">
              <label class="col-md-3">FSSAI Number:</label>
              <div class="col-md-4">
                <input type="text" name="fssai_number"
                placeholder="Enter FSSAI Number"
                data-parsley-trigger="change"				
				data-parsley-maxlength="15"
				data-parsley-minlength="10"
				data-parsley-maxlength-message="Only 15 characters are allowed"
				data-parsley-minlength-message="FSSAI number length should be 10 or more"				
				class="form-control" value="<?php echo fnStringToHTML($row1['fssai_number'])?>"
				>
              </div>
            </div><!-- /.form-group -->
			 <div class="form-group">
				<label class="col-md-3">Shop Type:</label>
				<div class="col-md-4">
					<select name="shop_type_id"  class="form-control">
					<option selected disabled>-Select-</option>
					<?php
					$shop_type_id=$row1['shop_type_id'];					
					$sql="SELECT id,shop_type FROM tbl_shop_type ";
					$result = mysqli_query($con,$sql);
					while($row = mysqli_fetch_array($result))
					{                        
						$sel="";
						$cat_id=$row['id'];
						if ($cat_id==$shop_type_id) {
							$sel = "selected";
						}
						echo "<option value='$cat_id' $sel>" . $row['shop_type'] . "</option>";
					} ?>
					</select>
				</div>
				</div>
            <div class="form-group">
              <label class="col-md-3">Shop Closed On Day:</label>
              <div class="col-md-4">
			  <?php $day = $row1['closedday']?>
			  <select class="form-control" name="closedday" >
			  <option value="">-Select-</option>
			  <option value="1" <?php if($day == 1){echo "selected";}?>>Monday</option>
			  <option value="2" <?php if($day == 2){echo "selected";}?>>Tuesday</option>
			  <option value="3" <?php if($day == 3){echo "selected";}?>>Wednesday</option>
			  <option value="4" <?php if($day == 4){echo "selected";}?>>Thursday</option>
			  <option value="5" <?php if($day == 5){echo "selected";}?>>Friday</option>
			  <option value="6" <?php if($day == 6){echo "selected";}?>>Saturday</option>
			  <option value="7" <?php if($day == 7){echo "selected";}?>>Sunday</option>
			  </select>
              </div>
            </div><!-- /.form-group -->
			<!-- <script src="https://code.jquery.com/jquery-1.9.1.js"></script> -->
            <div class="form-group">
              <label class="col-md-3">Shop Open Time:</label>
              <div class="col-md-4">
                <div class="input-group date" >
                    <input type="text" class="form-control" name="opentime" id="datetimepicker1"  value="<?php echo $row1['opentime']?>"
					 autocomplete="off">
                   <label class="input-group-addon btn" for="datetimepicker1">
					<span class="glyphicon glyphicon-time"></span>
					</label>
                </div>
              </div>
            </div><!-- /.form-group -->			     
            <div class="form-group">
              <label class="col-md-3">Shop Closed Time:</label>
              <div class="col-md-4">
                <div class='input-group date'>
                    <input type="text" class="form-control" name="closetime" id="datetimepicker2"  value="<?php echo $row1['closetime']?>"
					autocomplete="off">
                    <label class="input-group-addon btn" for="datetimepicker2">
					<span class="glyphicon glyphicon-time"></span>
					</label>
                </div>
              </div>
            </div><!-- /.form-group -->			
            <div class="form-group">
              <label class="col-md-3">Latitude:</label>
              <div class="col-md-4">
                <input type="text" name="latitude" value="<?php echo $row1['latitude']?>"
                placeholder="Enter Latitude" 
				min="-180" max="180" step="0.0000000000000001" 
                            data-parsley-trigger="change"                                       
                            
				class="form-control"
				>
              </div>
            </div><!-- /.form-group -->			
            <div class="form-group">
              <label class="col-md-3">Longitude:</label>
              <div class="col-md-4">
                <input type="text" name="longitude" value="<?php echo $row1['longitude']?>"
                placeholder="Enter Longitude" data-parsley-pattern="^(?!\s)[a-zA-Z0-9!@#$%^&*+_=><,./:' ]*$" data-parsley-trigger="change"
				class="form-control"
				>
              </div>
            </div><!-- /.form-group -->	

            <!-- <div class="form-group">
						<label class="col-md-3">Margin(%):</label>
						<div class="col-md-4">
						<input type="text" name="shop_margin"  class="form-control" onkeyup="myFunction(this)"
						placeholder="Enter Margin"
						min="0" max="100" step="0.01" 
						data-parsley-trigger="change"                         
						data-parsley-pattern="^[0-9]+?[\.0-9]*$" value="<?php echo $row1['shop_margin']?>"  > 
						</div>
			</div>-->

            	<div class="form-group">
							<label class="col-md-3"><b>Bank Details</b></label>
						</div>	
						<div class="form-group">
							<label class="col-md-3">Billing Name </label>
							<div class="col-md-4">
								<input type="text" 
								placeholder="Account Name"
								data-parsley-trigger="change"
								data-parsley-maxlength="50"
								data-parsley-maxlength-message="Only 50 characters are allowed"
								name="billing_name" class="form-control" value="<?php echo fnStringToHTML($row1['billing_name'])?>">
							</div>
						</div>					
						 <div class="form-group">
							<label class="col-md-3">Account Name </label>
							<div class="col-md-4">
								<input type="text" 
								placeholder="Account Name"
								data-parsley-trigger="change"
								data-parsley-maxlength="50"
								data-parsley-maxlength-message="Only 50 characters are allowed"
								name="bank_acc_name" class="form-control" value="<?php echo fnStringToHTML($row1['bank_acc_name'])?>">
							</div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Account Number:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Account Number"
							data-parsley-trigger="change"	
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="bank_acc_no" class="form-control" value="<?php echo fnStringToHTML($row1['bank_acc_no'])?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Name:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Bank Name"
							data-parsley-trigger="change"	
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="bank_name" class="form-control" value="<?php echo fnStringToHTML($row1['bank_name'])?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Branch Name:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Branch Name"
							data-parsley-trigger="change"	
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="bank_b_name" class="form-control" value="<?php echo fnStringToHTML($row1['bank_b_name'])?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">IFSC Code:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter IFSC Code"
							data-parsley-trigger="change"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="bank_ifsc" class="form-control" value="<?php echo fnStringToHTML($row1['bank_ifsc'])?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Status:</label>
						  <div class="col-md-4">
								<select name="shop_status" id="shop_status" class="form-control">
									<option value="0" <?php if($row1['shop_status'] == '0') echo "selected";?>>Active</option>
									<option value="1" <?php if($row1['shop_status'] == '1' || $row1['shop_status'] == '') echo "selected";?>>Inactive</option>
								</select>
						  </div>
						</div><!-- /.form-group -->				   
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
			   
			   <button name="getlatlong" id="getlatlong" type="button"  class="btn btn-primary">Get Lat Long</button>
               <button name="submit" id="submit" class="btn btn-primary">Submit</button>
			   
                <a href="shops.php" class="btn btn-primary">Cancel</a>
                <!--<a data-toggle="modal" href="#thankyouModal"  class="btn btn-primary">Delete</a>-->
              </div>
            </div><!-- /.form-group -->
          </form>  
                            
 		  
	  	  <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
          <div class="modal-dialog" style="width:300px;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
				<h4 style="color:red; text-align:center;">Do you want to delete this record ?</h4>
				</p>                     
        	  <center><a href="shop_delete.php?id=<?php echo $row1['id']?>" ><button type="button" class="btn btn-success">Yes</button></a>
			  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
			  </center>
            </div>    
        </div>
    </div>
</div>		  
                    
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			 </div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>
<script type="text/javascript">
function myFunction(varl) {
if (varl.value != "") {
var arrtmp = varl.value.split(".");
if (arrtmp.length > 1) {
	var strTmp = arrtmp[1];
	if (strTmp.length > 2) {
					varl.value = varl.value.substring(0, varl.value.length - 1);
	}
}
}
}
</script>
<script type="text/javascript">
$(document).ready(function(){$("#datetimepicker1").timepicker({format: "yyyy-mm-dd",autoclose: true});});
$(document).ready(function(){$("#datetimepicker2").timepicker({format: "yyyy-mm-dd",autoclose: true});});  
function fnShowStockist(id){
	if (window.XMLHttpRequest)
	{
		xmlhttp=new XMLHttpRequest();
	}
	else
	{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById("Subcategory").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","fetchstockist.php?cat_id="+id.value,true);
	xmlhttp.send();
}
/* To change city on change of state*/
function calculate_data_count(element_value){
	element_value = element_value.toString();
	var element_arr = element_value.split(',');	
	return element_arr.length;
}
function setSelectNoValue(div,select_element){
	var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled value="">-Select-</option></select>';
	document.getElementById(div).innerHTML	=	select_selement_section;
}
function fnShowCity(id_value) {	
	$("#city_div").show();	
	$("#area").html('<option value="">-Select-</option>');	
	$("#subarea").html('<option value="">-Select-</option>');	
	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city&mandatory=mandatory";
	CallAJAX(url,"div_select_city");	
}
function FnGetSuburbDropDown(id) {
	$("#area_div").show();	
	$("#subarea").html('<option value="">-Select-</option>');		
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&function_name=FnGetSubareaDropDown";
	CallAJAX(url,"div_select_area");
}
function FnGetSubareaDropDown(id) {	
	var suburb_str = $("#area").val();
	$("#subarea_div").show();	
	var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea";
	CallAJAX(url,"div_select_subarea");
		
}
</script> 
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLEAPIKEY;?>" type="text/javascript"></script>
<script type="text/javascript">
$( "#getlatlong" ).click(function() {
  //alert( "Handler for .click() called." );
  //$(this).find("option:selected").text();
  var curaddress=$('[name="address"]').val();
  var subarea=$('#subarea').val();
  if(subarea!=''){subarea=", "+$('#subarea').find("option:selected").text();}else{subarea='';}
   var area=$('#area').val();
  if(area!=''){area=", "+$('#area').find("option:selected").text();}else{area='';}
   var city=$('[name="city"]').val();
  if(city!=''){city=", "+$('[name="city"]').find("option:selected").text();}else{city='';}
   var state=$('[name="state"]').val();
  if(state!=''){state=", "+$('[name="state"]').find("option:selected").text();}else{state='';}
	var address = curaddress+''+subarea+''+ area+''+city+''+state;	
					//alert(address);
	//var address123 = address.replace("/^[a-zA-Z\s](\d)?$/","");
	//alert(address123);
					
  var geocoder = new google.maps.Geocoder();
	//var address = "new york";

	geocoder.geocode( { 'address': address}, function(results, status) {

	  if (status == google.maps.GeocoderStatus.OK) {
		var latitude = results[0].geometry.location.lat();
		var longitude = results[0].geometry.location.lng();		
		$('[name="latitude"]').val(latitude);
		$('[name="longitude"]').val(longitude);
	  } else{
		  alert("Not getting proper latitude,longitude!");
	  }
	}); 
});
function getServiceByUserID(UserID) {	
	var UserID1 =UserID.value;
	var usertype_info = UserID1.split(',');	
	var userole=usertype_info[1];	
	var usertype=usertype_info[0];
	//alert(userole);	
	var url = "conf_get_childs.php"; 
	jQuery.ajax({
		url: url,
		method: 'POST',
		data: 'userole='+userole+'&usertype='+usertype+'&getflag=onchange_get_serviceby_userid',
		async: false
	}).done(function (response) {	
		console.log(response);
		if(response!="no_users"){
			$('#serviceby_div').show();
			$('#service_by_user_id').html('');
			$('#service_by_user_id').append(response);
		}				
	}).fail(function () { });
}
</script>