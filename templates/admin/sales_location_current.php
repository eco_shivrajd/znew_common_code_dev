<?php
include ("../../includes/config.php");
include "../includes/reportManage.php";
$reportObj = new reportManage($con, $conmain);
$result_sp = $reportObj->get_all_sp_current_location();

$array_sp = array();
if ($result_sp != 0) {
    $j = 0;
    foreach ($result_sp as $key => $value) {
        $array_sp[$j]['lat'] = $value["lattitude"];
        $array_sp[$j]['lng'] = $value["longitude"];
        $array_sp[$j]['title'] = $value["firstname"];
        $j++;
    }
}


if ($j == 0) {
    ?>	
    <div  id="dvMap" style="min-height:300px">
        No Data Available.
    </div>
<?php } else { ?>
    <div  id="dvMap" style="min-height:300px">
    </div>
<?php } ?>

<script>
    var markers =<?php echo json_encode($array_sp); ?>;
	 console.log('aaaa');
    console.log(markers.length);
    console.log('aaaa');

    function initMap() {
		if(markers.length>0){
			drawingRoute();
		}        
    }
    function drawingRoute() {
        //next function start

        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
        var infoWindow = new google.maps.InfoWindow();
        var lat_lng = new Array();

        var latlngbounds = new google.maps.LatLngBounds();


        for (i = 0; i < markers.length; i++) {			
            var data = markers[i];
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            lat_lng.push(myLatlng);
			//alert(data.title.charAt(0));
            var marker = new google.maps.Marker({
                position: lat_lng[i],
                map: map,
                label: data.title.charAt(0),
                title: data.title
            });
            latlngbounds.extend(marker.position);
            (function (marker, data) {
                //google.maps.event.addListener(marker, "click", function (e) {
                 //   infoWindow.setContent(data.description);
                //    infoWindow.open(map, marker);
               // });
            })(marker, data);
        }

        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);

        //***********ROUTING****************//

        //Initialize the Direction Service
        var service = new google.maps.DirectionsService();
        //this is for salesperson traveling line draw
    }//end outer

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDMpdO43txdg_zyovvZm9i1tMMFIQTKTU&callback=initMap"
async defer></script>
