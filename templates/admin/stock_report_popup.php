<?php
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/stockManage.php";
$stockObj    =   new stockManager($con,$conmain);
$user_id = $_POST['userid'];
$username = $_POST['username'];
//echo "xchvkxk<pre>";print_r($_POST);
$report_type = 'Stock Report Details'; 
//$order_details = $stockObj->getMyuserStockDetails($userid); 
?>
<div class="modal-header">
    <button type="button" name="btnPrint1" id="btnPrint1" onclick="OrderDetailsPrint()" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel"></h4>	   
</div>
<div class="modal-body" style="padding-bottom: 5px !important;" id="divOrderPrintArea">
    <div class="row">
        <div class="col-md-12">   
            <div class="portlet box blue-steel">
                <div class="portlet-title ">
                    <div class="caption printHeading">
                        <?= $report_type; ?>
                    </div>                          
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_2" width="100%">
                        <tr>
                            <td><b>Stock By</b></td>
                            <td><b><?php echo $username; ?></b></td>				
                        </tr>
                        <tr>
                            <td colspan="2">
                               <table class="table table-striped table-bordered table-hover" id="sample_2">
                              <thead>
                                 <tr>
                                    <th> Product Name </th>
                                    <th> Product Variant  </th>
                                    <th> Product Price  </th>
                                    <th> Total Stock IN </th>
                                    <th> Total Orders Received</th>
                                    <th>  Delivered </th>
                                    <th> Available Stock  </th>
                                    <th>   Stock Updated Date </th>
                                    <!--  <th>Today’s Production  </th>
                                       <th> Today’s Orders  </th> 
                                       <th> Today’s Delivered Orders </th>  -->
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    /* $sql = "SELECT b.productname,pv.price,pv.variant_1,pv.variant_2,pv.variant1_unit_id,pv.variant2_unit_id,pv.id as pvid, "
                                            . "b.added_by_userid,b.added_by_usertype, a.id AS cat_id  "
                                            . " FROM tbl_category a,tbl_product b "
                                            . " left join tbl_product_variant pv on b.id = pv.productid "
                                            . " where a.id=b.catid AND b.isdeleted != 1 "
                                            . " and ((b.added_by_userid=0) OR (b.added_by_userid=1) OR (b.added_by_userid='$user_id'))";*/
                                    
                                     $sql1 = "SELECT parent_ids FROM tbl_user where id='$user_id'";
                                      $result1 = mysqli_query($con, $sql1);
                                      $row1 = mysqli_fetch_assoc($result1);
                                      $parent_ids = $row1['parent_ids'];
                                     $sql = "SELECT a.categorynm,b.productname,b.id,pv.producthsn,pv.price as price,pv.variant_1,pv.variant_2,pv.variant1_unit_id,pv.variant2_unit_id,pv.id as pvid, "
                                            . " pv.productimage,b.added_by_userid,b.added_by_usertype, a.id AS cat_id  "
                                            . " FROM tbl_category a,tbl_product b "
                                            . " left join tbl_product_variant pv on b.id = pv.productid "
                                            . " where a.id=b.catid AND b.isdeleted != 1 "
                                            . " and (b.added_by_userid='".$user_id."' OR b.added_by_userid IN (".$parent_ids."))";
                                    $result = mysqli_query($con, $sql);
                                    $row_count = mysqli_num_rows($result);
                                    while ($row = mysqli_fetch_array($result)) 
                                    { 
                                     // echo "<pre>";print_r($row);
                                        $prodcode = $row['id'] . '_' . $row['pvid'];
                                        ?>
                                 <tr class="odd gradeX" >
                                    <td >
                                       <?php echo fnStringToHTML($row['productname']); ?>
                                    </td>
                                    <td >
                                       <?php
                                          $sqlvarunit3 = "SELECT unitname FROM tbl_units  where id='" . $row['variant1_unit_id'] . "'";
                                          $resultvarunit3 = mysqli_query($con, $sqlvarunit3);
                                          while ($rowvarunit3 = mysqli_fetch_array($resultvarunit3)){
											 $unitname3 = $rowvarunit3['unitname'];
                                          }
                                          
                                          $sqlvarunit4 = "SELECT unitname FROM tbl_units  where id='" . $row['variant2_unit_id'] . "'";
                                          $resultvarunit4 = mysqli_query($con, $sqlvarunit4);
                                          while ($rowvarunit4 = mysqli_fetch_array($resultvarunit4)) {
												 $unitname4 = $rowvarunit4['unitname'];
                                          }
                                          echo $row['variant_1']."-".$unitname3." , ".$row['variant_2']."-".$unitname4;
                                          echo "<br>";
                                            ?>
                                    </td>
                                    <td align="right">
                                       <?php echo fnStringToHTML($row['price']); ?>
                                    </td>
                                    <td align="right" >
                                       <?php  
                                          $total_stock_in_quantity=0;
                                          if ($user_role=='Admin'){
											  $sqlvarunit5 = "SELECT sum(product_quantity) as total_stock_in_quantity FROM tbl_stock_management  where product_varient_id='" . $row['pvid'] . "' AND added_by_userid='".$user_id."'";
											  $resultvarunit5= mysqli_query($con, $sqlvarunit5);
											  while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5))  {
											  $total_stock_in_quantity = $rowvarunit5['total_stock_in_quantity'];
											  }
											  if (isset($total_stock_in_quantity))  {
												echo $total_stock_in_quantity;
											  }else { echo '0'; }  
                                         }else{
                                          
											   $sqlvarunit7 = "SELECT sum(product_quantity) as total_stock_in_quantity FROM tbl_stock_management  where product_varient_id='" . $row['pvid'] . "' AND added_by_userid='".$user_id."'";
											   $resultvarunit7= mysqli_query($con, $sqlvarunit7);
											   while ($rowvarunit7 = mysqli_fetch_array($resultvarunit7))  {
												$total_stock_by_user = $rowvarunit7['total_stock_in_quantity'];
											   }
                                                  
											  $sqlvarunit5 = "SELECT o.id,sum(od.product_quantity) as total_stock_in_quantity FROM tbl_orders AS o LEFT JOIN tbl_order_details AS od ON od.order_id = o.id where o.order_from='".$user_id."' AND od.product_variant_id='" . $row['pvid'] . "' AND ( od.order_status =2 OR od.order_status =4) ";
											  $resultvarunit5= mysqli_query($con, $sqlvarunit5);
											  while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) {
												$total_stock_by_parent = $rowvarunit5['total_stock_in_quantity'];
											  }
											  
											   $total_stock_in_quantity = $total_stock_by_user + $total_stock_by_parent;
											  if (isset($total_stock_in_quantity))  {
												echo $total_stock_in_quantity;
											  }    else { echo '0'; }  
                                         } ?>
                                    </td>
                                    <td align="right" >
                                       <?php  
                                          $sqlvarunit5 = "SELECT o.id,sum(od.product_quantity) as total_product_quantity FROM tbl_orders AS o LEFT JOIN tbl_order_details AS od ON od.order_id = o.id where o.order_to='".$user_id."' AND od.product_variant_id='" . $row['pvid'] . "' ";
                                           $resultvarunit5= mysqli_query($con, $sqlvarunit5);
                                           while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) 
                                           {
                                             $total_product_quantity_bychild = $rowvarunit5['total_product_quantity'];
                                           }   
                                           $sqlvarunit5 = "SELECT sum(product_quantity) as total_product_quantity FROM tbl_customer_orders_new 
                                               where order_to='".$user_id."' AND product_variant_id='" . $row['pvid'] . "' ";
                                           $resultvarunit5= mysqli_query($con, $sqlvarunit5);
                                           while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) 
                                           {
                                             $total_product_quantity_bycust = $rowvarunit5['total_product_quantity'];
                                           }
                                          
                                           $total_product_quantity = $total_product_quantity_bychild + $total_product_quantity_bycust;
                                           if (isset($total_product_quantity)) 
                                           {
                                           	echo $total_product_quantity;
                                           }
                                           else { echo '0'; }                   
                                           //echo "<br>";
                                             ?>									 
                                    </td>
                                    <td align="right" >
                                       <?php  
                                          $total_stock_out_quantity=0;
                                           $sqlvarunit5 = "SELECT o.id,sum(od.product_quantity) as total_stock_out_quantity FROM tbl_orders AS o LEFT JOIN tbl_order_details AS od ON od.order_id = o.id where o.order_to='".$user_id."' AND od.product_variant_id='" . $row['pvid'] . "' AND ( od.order_status =2 OR od.order_status =4)";
                                           $resultvarunit5= mysqli_query($con, $sqlvarunit5);
                                           while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) 
                                           {
                                               $total_stock_out_quantity_tochild = $rowvarunit5['total_stock_out_quantity'];
                                           }
                                          
                                           $sqlvarunit5 = "SELECT sum(product_quantity) as total_stock_out_quantity 
                                           FROM tbl_customer_orders_new  where order_to='".$user_id."' AND product_variant_id='" . $row['pvid'] . "' AND ( order_status =2 OR order_status =4)";
                                           $resultvarunit5= mysqli_query($con, $sqlvarunit5);
                                           while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) 
                                           {
                                               $total_stock_out_quantity_tocust = $rowvarunit5['total_stock_out_quantity'];
                                           }
                                          
                                          $total_stock_out_quantity = $total_stock_out_quantity_tochild + $total_stock_out_quantity_tocust;
                                          
                                           if (isset($total_stock_out_quantity)) {
                                           	echo $total_stock_out_quantity;
                                           } else { echo '0'; } 
                                             ?>	
                                    </td>
                                    <td align="right" >
                                       <?php  
                                          if ($total_stock_in_quantity >= $total_stock_out_quantity){
                                          	$total_available_stock = $total_stock_in_quantity-$total_stock_out_quantity;
                                         }else{
                                             $total_available_stock=0;
                                          }
                                          ?>
                                       <?php
                                          if (isset($total_available_stock) && $total_available_stock>0){
                                          ?>
                                       <div style="color: green;">	
                                          <?php echo $total_available_stock; ?>
                                       </div>
                                       <?php
                                          }
                                          else 
                                          {
                                          ?>                     
                                       <a href="#" style="color: red;"> 0 </a>                       
                                       <?php }  
                                          ?>
                                    </td>
                                    <td align="right" >
                                       <?php  
                                          if ($user_role=='Admin'){
												$last_date='';
												$sqlvarunit5 = "SELECT id,createdon as last_date FROM tbl_stock_management where added_by_userid='".$user_id."' AND product_varient_id='" . $row['pvid'] . "' ORDER BY id DESC LIMIT 1";
												$resultvarunit5= mysqli_query($con, $sqlvarunit5);
												while ($rowvarunit5 = mysqli_fetch_array($resultvarunit5)) {
													$last_date = $rowvarunit5['last_date'];
												}
												if (isset($last_date))  { 
													echo date("d-m-Y H:i:s",strtotime($last_date));
												} else { echo '-'; }  
                                         }else{
                                                $last_date_added_byuser='';
                                                $last_date_added_byparent='';
                                          
												$sqlvarunit77 = "SELECT id,createdon as last_date FROM tbl_stock_management  where product_varient_id='" . $row['pvid'] . "' AND added_by_userid='".$user_id."' ORDER BY id DESC LIMIT 1";
                                          
												$sqlvarunit53 = "SELECT o.id,o.order_date as last_date 
                                                   FROM tbl_orders AS o 
                                                   LEFT JOIN tbl_order_details AS od ON od.order_id = o.id where o.order_from='".$user_id."' AND od.product_variant_id='" . $row['pvid'] . "' AND ( od.order_status =2 OR od.order_status =4) ORDER BY o.id DESC LIMIT 1";
                                          
												$resultvarunit53= mysqli_query($con, $sqlvarunit53);
												while ($rowvarunit53 = mysqli_fetch_array($resultvarunit53)){
													$last_date_added_byparent = date("d-m-Y H:i:s",strtotime($rowvarunit53['last_date']));
												}
                                                $resultvarunit77= mysqli_query($con, $sqlvarunit77);
                                                while ($rowvarunit77 = mysqli_fetch_array($resultvarunit77)){
													$last_date_added_byuser = date("d-m-Y H:i:s",strtotime($rowvarunit77['last_date']));
												}
												// Use comparison operator to  
												// compare dates 
												if (isset($last_date_added_byuser) && $last_date_added_byuser!='' && isset($last_date_added_byparent) && $last_date_added_byparent!=''){
													if ($last_date_added_byuser >= $last_date_added_byparent){
														echo $last_date_added_byuser; 
													}else{
														echo $last_date_added_byparent; 
													}
												} elseif (isset($last_date_added_byuser) && $last_date_added_byuser!='' && $last_date_added_byparent==''){ 
													echo $last_date_added_byuser;
												}elseif ($last_date_added_byuser=='' && isset($last_date_added_byparent) && $last_date_added_byparent!=''){
													echo $last_date_added_byparent;
												}else{echo "-"; }
                                          }  ?>
                                    </td>
                                 </tr>
                                 <?php } ?>							
                              </tbody>
                           </table>
                            </td>				
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>