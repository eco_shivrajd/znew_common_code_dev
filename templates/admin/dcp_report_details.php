<?php
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/reportManage.php";
$reportObj = new reportManage($con, $conmain);
$report_title = 'Delivery Channel Person Report All';
$row = $reportObj->getDCPStock();

$record_count = mysqli_num_rows($row);
//echo "sdfsd<pre>";print_r($row);die();
$colspan = "6";
?>
<?php if ($_POST["actionType"] == "excel") { ?>
    <style>table { border-collapse: collapse; } 
        table, th, td {  border: 1px solid black; } 
        body { font-family: "Open Sans", sans-serif; 
               background-color:#fff;
               font-size: 11px;
               direction: ltr;}
        </style>
    <?php }
    ?>

    <table 
        class="table table-striped table-bordered table-hover table-highlight table-checkable" 
    data-provide="datatable" 
    data-display-rows="10"
    data-info="true"
    data-search="true"
    data-length-change="true"
    data-paginate="true"
    id="sample_5">
    <thead>
        <tr>
            <td colspan="<?= $colspan; ?>" align="canter" class="gradeX even" style="text-align:center; font-weight:600;"><h4><b><?php
                      echo "Delivery Channel Person Report All";
                        ?></b></h4></td> 
        </tr>
        <tr>
            <th data-filterable="false"  data-sortable="true" data-direction="desc" >Sr NO.</th>
            <th data-filterable="false" data-sortable="true" data-direction="desc">Name</th> 
            <th data-filterable="false" data-sortable="true" data-direction="desc">Cartons</th> 
            <th data-filterable="false" data-sortable="true" data-direction="desc">Assigned Quantity</th>   
            <th data-filterable="false" data-sortable="false" data-direction="desc">Stock Quantity</th>	
            <th data-filterable="false" data-sortable="false" data-direction="desc">Sale Quantity</th>
        </tr>
    </thead>
    <tbody>					
        <?php
        if (!empty($row)) {
            foreach ($row as $key => $value) {
                ?>
                <tr class="odd gradeX">				
                    <td align='right' ><?= $key + 1; ?></td>
                    <td align='Left'><?= $value['firstname']; ?></td>  
                    <td align='Left'><?php $cartonsid=$value['cartons_id'];$userid=$value['dcp_id'];
                    echo '<a onclick="myFunction(\'' . $cartonsid . '\',\''.$userid.'\')" title="Cartons Details ">'. "carton-".$value['cartons_id'].'</a></td>'; ?>       
                    <td align='right'><?= $value['assigned_qnty']; ?></td>
                    <td align='right'><?= $value['stock_qnty']; ?></td>
                    <td align='right'><?= $value['sale_qnty']; ?></td>
                </tr>
            <?php } ?>
            <?php
        } 
        if ($_POST["actionType"] == "excel" && $row == 0) {
                echo "<tr><td>No matching records found</td></tr>";
            }
        ?>	

    </tbody>	
</table>

 <div class="modal fade" id="order_details" role="dialog">
        <div class="modal-dialog" style="width: 880px !important;">
            <!-- Modal content-->
            <div class="modal-content" id="order_details_content">
            </div>
        </div>
    </div>

<script>
    jQuery(document).ready(function () {

        ComponentsPickers.init();
        TableManaged.init();
    });
    function myFunction(cartonid,dcpid){
        var param='';
        param="cartonid="+cartonid;
        param="&dcpid="+dcpid;
        $.ajax
        ({
            type: "POST",
            url: "dcp_report_popup.php",
            data: param,
            success: function (msg)
            {
                console.log(msg);
                $("#order_details_content").html(msg);              
                $('#order_details').modal('show');
            }
        });
    }

    $(document).ready(function () {
        var table = $('#sample_5').dataTable();
        // Perform a filter
        table.fnFilter('');
        // Remove all filtering
        //table.fnFilterClear();

    });
</script> 
<!-- END JAVASCRIPTS -->
<?php
if ($_POST["actionType"] == "excel") {
    if ($row != 0) {
        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=SP_Summary_Report.xls");
    }
}
?>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- END JAVASCRIPTS -->