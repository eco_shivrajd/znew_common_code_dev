<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/orderManage.php";
$orderObj   =   new orderManage($con,$conmain);?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php
    $activeMainMenu = "ManageSupplyChain"; $activeMenu = "User List";
    include "../includes/sidebar.php";
    ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->           
            <h3 class="page-title">
            User List
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="javascript:;">User List</a>  
                    </li>
                </ul>               
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">                
                    <div class="row">
                <div class="col-md-12">                 
                    <div class="portlet box blue-steel">
                        <div class="portlet-title">
                            <div class="caption">Search Criteria</div>                          
                            <div class="clearfix"></div>
                        </div>                      
                        <div class="portlet-body">  
                                             
                            <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">                               
                            <div class="form-group">
                                <label class="col-md-3">User Type:</label>
                                <div class="col-md-4">                              
                                    <select name="user_type" id="user_type" onchange="loadReport(1);" class="form-control">     
                                        <?php if($_SESSION[SESSION_PREFIX.'user_type'] == "Admin") { ?>
                                        
                                        <option value="superstockist" <?php if($_REQUEST['user_type']=="superstockist")echo 'selected';?>>Super Stockist</option>
                                        
                                        <?php } if($_SESSION[SESSION_PREFIX.'user_type'] != "Distributor") {?>
                                        <option value="Distributor" <?php if($_REQUEST['user_type']=="Distributor")echo 'selected';?>>Stockist</option>
                                        <?php } ?>
                                        <option value="salesperson" <?php if($_REQUEST['user_type']=="salesperson")echo 'selected';?>>Sales Person</option>
										<option value="Shopkeeper" <?php if($_REQUEST['user_type']=="Shopkeeper")echo 'selected';?>>Shopkeeper</option>

                                        <option value="DeliveryPerson" <?php if($_REQUEST['user_type']=="DeliveryPerson")echo 'selected';?>>Delivery Person</option>
                               
                               <?php  if($_SESSION[SESSION_PREFIX.'user_type'] != "Distributor" && $_SESSION[SESSION_PREFIX.'user_type'] != "Superstockist") {?>
                                <option value="Accountant" <?php if($_REQUEST['user_type']=="Accountant")echo 'selected';?>>Accountant</option>
                           <?php
                            }
                            ?>

                                <option value="DeliveryChannelPerson" <?php if($_REQUEST['user_type']=="DeliveryChannelPerson")echo 'selected';?>>Delivery Channel Person</option>

                                    </select>
                                </div>
                            </div><!-- /.form-group --> 
                           
                                                 
                        <input type="hidden" id="order" name="order" value="asc">
                        <input type="hidden" id="sort_complete" name="sort_complete" value="">
                        <input type="hidden" id="page" name="page" value="">
                        <input type="hidden" id="per_page" name="per_page" value="">    
                        <input type="hidden" id="actionType" name="actionType" value=""> 
                        <input type="hidden" id="search" name="search" value="">                        
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                     <div class="clearfix"></div>   
                <div class="portlet box blue-steel">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-puzzle"></i>User List</div>
                              <a class="btn btn-sm btn-default pull-right mt5" href="add-users.php">
                                    Add User
                                </a>
                            </div>  
                        <div class="portlet-body" id="user_summary_details">
                        </div>
                    </form> 
            
            </div>
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    
    <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
</div>
</div>
</div>
</div>

 <div class="modal fade" id="order_details" role="dialog">
        <div class="modal-dialog" style="width: 880px !important;">
            <!-- Modal content-->
            <div class="modal-content" id="order_details_content">
            </div>
        </div>
    </div>



<div style="display:none;" class="modal-backdrop fade in"></div>
<div aria-hidden="false" style="display: none;" id="basicModal" class="modal fade in">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick="close_modal();" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title">User Details</h3>
      </div>
      <div class="modal-body">

<div id="ajax_list_div">
</div>
      </div>
       
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<div id="print_div"  style="display:none;"></div>
<div id="table_heading"  style="display:none;"><?=$user_data['firstname'];?>'s Shopwise <?=$user_type_title;?></div>
<form action="../includes/exportToExcel.php" method="post" name="export_excel" id="export_excel">
    <input type="hidden" name="export_data" id="export_data">
</form>
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
function callLocal(){
    loadReport(1);
}
$(document).ready(function() {
    loadReport(1);
});
function fnSelectionBoxTest()
{
    if($('#selTest').val() == '3')
    {
       $('#date-show').show();
    }
    else
    {
        $('#date-show').hide();
        loadReport(1);
    }
}
function loadReport(page) {
    var param = ''; 
    var user_type = $('#user_type').val();
    param = param + '&user_type='+user_type;
    var selTest = $('#selTest').val();
    param = param + '&selTest='+selTest;
    if(selTest == 3)
    {
        var frmdate = $('#frmdate').val();
        param = param + '&frmdate='+frmdate;
        var todate = $('#todate').val();
        param = param + '&todate='+todate;
        var validation = compaire_dates(frmdate,todate);
    }

  if(selTest == 3 && frmdate != '' && todate != '' && validation == 1)
  {
      alert("'From' date should not be greater than 'To' date.");
      return false;
  }
  else
  {
    $.ajax
    ({
        type: "POST",
        url: "user_summary_details.php",
        data: param,
        success: function(msg)
        {
          $("#user_summary_details").html(msg);
        }
      });
  }
}

 function showOrderDetails(id, user_type) {
   // alert(id);
   // alert(user_type);
            var url = "user_details_popup.php";
            jQuery.ajax({
                url: url,
                method: 'POST',
                data: 'user_details_id=' + id + '&user_type=' + user_type,
                async: false
            }).done(function (response) {
                $('#order_details_content').html(response);
                $('#order_details').modal('show');
            }).fail(function () { });
            return false;
        }
/*function report_download() {
    var filtered_data_count = $("#sample_2").DataTable().rows( { filter : 'applied'} ).nodes().length;
    var td_rec = $("#sample_2 td:last").html();
    if(td_rec != 'No matching records found')
    {
        var search = $('input[type="search"]').val();
        if(search !='')
            $('#search').val(search);
        else
            $('#search').val('');
        
        var sortInfo = $("#sample_2").dataTable().fnSettings().aaSorting;
        $("#sort_complete").val(sortInfo);  
        var table = $("#sample_2").DataTable().page.info();
        
        var rec_per_page = table.length;
        $("#per_page").val(rec_per_page);
        var page = table.page;
        $("#page").val(page);
        $("#actionType").val('excel');
        //return false;
        var url = "order_summary_detail.php";
        $("#frmsearch").attr("action", url);
        $('#frmsearch').submit();
    }else{
        alert("No matching records found");
    }
}*/
function report_download() {
    var td_rec = $("#sample_2 td:last").html();
    if(td_rec != 'No matching records found')
    {
        var divContents = $(".table-striped").html();
        $("#print_div").html('<table id="print_table" style="text-decoration:none;">'+divContents+'</table>');  
        var heading = $("#table_heading").html();
        $("#print_table tr th i").html("RS");   
        divContents =  $("#print_div").html();
        divContents = divContents.replace(/₹/g,'Rs');
        divContents = divContents.replace(/<\/*a.*?>/gi,'');    
        $("#export_data").val(divContents);
        document.forms.export_excel.submit();
    }else{
        alert("No matching records found");
    }
}

 $("#btnPrint").live("click", function () {
    var td_rec = $("#sample_2 td:last").html();
    if(td_rec != 'No matching records found')
    {
        var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
        
        var divContents1 = $(".table-scrollable").html();
        $("#print_div").html(divContents1);
        $("#print_div a").removeAttr('href');
        //$("#sample_2 tr th i").html("₹"); 
        var divContents = $("#print_div").html();
        var printWindow = window.open('', '', 'height=400,width=800');
        printWindow.document.write('<html><head><title>Report</title>');
        printWindow.document.write('<style>a{text-decoration: none; color:#333333;} #sample_2{margin:20 20 20 20px; width:700px}</style>');
        printWindow.document.write('<link   rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css"/>');
        printWindow.document.write('<link  rel="stylesheet" type="text/css" href="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>');
        
        if( navigator.userAgent.toLowerCase().indexOf('chrome') > -1 ){
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');   
            printWindow.focus();    
            setTimeout(function () {
                printWindow.print();
                //printWindow.close();
            }, 500);
        }else if(isIE == true){
            printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');   
            printWindow.focus();    
            printWindow.document.execCommand("print", false, null);
            //printWindow.close();
        }
        else{       
            printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');   
            printWindow.focus();    
            setTimeout(function () {                
                printWindow.print();
                //printWindow.close();
            }, 100);
        }
    }else{
        alert("No matching records found");
    }
});
</script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- END JAVASCRIPTS -->