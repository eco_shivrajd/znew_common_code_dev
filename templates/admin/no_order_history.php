<!-- BEGIN HEADER -->
<?php 
include "../includes/grid_header.php";
include "../includes/userManage.php";
include "../includes/orderManage.php";
include "../includes/shopManage.php";
$userObj 	= 	new userManager($con,$conmain);
$orderObj 	= 	new orderManage($con,$conmain);
$shopObj 	= 	new shopManager($con,$conmain);
$sp_id = $_GET['id'];

$user_data = $userObj->getLocalUserDetails($sp_id);
$filter_date = $_GET['param1'];

$date1 = null;
$date2 = null;
if(isset($_GET['param2']))
	$date1=$_GET['param2'];
if(isset($_GET['param3']))
	$date2=$_GET['param3'];

$result1 = $orderObj->no_order_history_details($sp_id, $filter_date,$date1,$date2);
$report_title = $orderObj->getReportTitleForSP($filter_date,$date1,$date2);
if($sp_id != ''){
	$heading1 = "Sales Person: ".$user_data['firstname'];
	$heading2 = "No Order Accept History ".$report_title;
	$heading3 = $user_data['firstname']."'s Shopwise No Order Accept History ".$report_title;
	$activeMainMenu = "Reports"; $activeMenu = "OrderSummary";
}else{
	$heading1 = "No Order Accept History";
	$heading2 = "Details";
	$heading3 = "No Order Accept History";
	$activeMainMenu = "Reports"; $activeMenu = "No-Order-Accept-History";
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include "../includes/sidebar.php"; ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">		
			<h3 class="page-title">No Order Accept History</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li><i class="fa fa-home"></i>
					<a href="#"><?=$heading1?></a></li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">				
					<div class="portlet box blue-steel">
						<div class="portlet-title">						
							<div class="caption"><i class="icon-puzzle"></i>No Order Accept History</div>	
							<button type="button" name="btnExcel" id="btnExcel" onclick="report_download();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
							&nbsp;
							<button type="button" name="btnPrint" id="btnPrint" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
                            <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_2">
								<thead>
									<tr>
										<th width="10">Shop Name</th>	
										<th width="10">Parent Names</th>		
										<?php if($sp_id == ''){ echo '<th width="10">Sales Person Name</th>'; } ?>										
										<th width="20">Date</th>
										<th width="5">Reason Type</th>
										<th width="50">Reason Details</th>
									</tr>
								</thead>
							<tbody>
							<?php				
							
							if($result1 != 0){
							while($row = mysqli_fetch_array($result1))
							{
								$shop_details = $shopObj->getShopDetails($row['shop_id']);								
								$shop_name = $shop_details['name'];
								if($sp_id == ''){ 
									$user_details = $userObj->getLocalUserDetails($row['salesperson_id']);
									$sales_person_name = $user_details['firstname'];
								}
								$sql_parent="SELECT parent_names FROM tbl_user_view where id='".$row['salesperson_id']."'";
								 $result_parent = mysqli_query($con, $sql_parent);
								if(mysqli_num_rows($result_parent)>0){
									while($row1 = mysqli_fetch_array($result_parent)){
										$parent_names=$row1['parent_names']; 
									}
								}
								
							?>
								<tr class="odd gradeX">
									<td><?=fnStringToHTML($shop_name);?></td>	
									<td><?=fnStringToHTML($parent_names);?></td>															
									<?php if($sp_id == ''){ echo '<td>'.fnStringToHTML($sales_person_name).'</td>'; } ?>																			
									<td><?php echo $row['shop_visit_date_time'];?></td>  
									<td><?=fnStringToHTML($row['shop_close_reason_type']);?></td>
									<td><?=fnStringToHTML($row['shop_close_reason_details']);?></td>  
								</tr>                             
							<? } } ?>	
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<div id="table_heading"  style="display:none;"><?=$heading3;?></div>
<div id="print_div"  style="display:none;"></div>
<form action="../includes/exportToExcel.php" method="post" name="export_excel" id="export_excel">
	<input type="hidden" name="export_data" id="export_data">	
</form>
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
function report_download() {
	var td_rec = $("#sample_2 td:last").html();
	if(td_rec != 'No matching records found')
	{
		var divContents = $(".table-striped").html();
		$("#print_div").html('<table id="print_table" style="text-decoration:none; display: block;">'+divContents+'</table>');	
		var heading = $("#table_heading").html();
		var colspan = $('#sample_2 thead th').length;
		$('<tr><th colspan="'+colspan+'" align="center">'+heading+'</th></tr>').insertBefore('#print_table tr:first');
		divContents =  $("#print_div").html();
		divContents = divContents.replace(/<\/*a.*?>/gi,'');	//alert(divContents);
		
		$("#export_data").val(divContents);
		document.forms.export_excel.submit();
	}else{
		alert("No matching records found");
	}
}
$("#btnPrint").live("click", function () {
	var td_rec = $("#sample_2 td:last").html();
	if(td_rec != 'No matching records found')
	{
		var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
		
		var divContents = $(".table-striped").html();
		$("#print_div").html('<table id="print_table">'+divContents+'</table>');	
		var heading = $("#table_heading").html();
		var colspan = $('#sample_2 thead th').length;
		$('<tr><th colspan="'+colspan+'" align="center">'+heading+'</th></tr>').insertBefore('#print_table tr:first');
		divContents =  $("#print_div").html();
		
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write('<html><head><title>Report</title>');
		printWindow.document.write('<style type="text/css">a{text-decoration: none; color:#333333;} table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
		printWindow.document.write('</head><body >');
		printWindow.document.write(divContents);
		printWindow.document.write('</body></html>');	
		printWindow.focus();	
		if( navigator.userAgent.toLowerCase().indexOf('chrome') > -1 ){		
			setTimeout(function () {
				printWindow.print();
				printWindow.close();
			}, 500);
		}else if(isIE == true){			
			printWindow.document.execCommand("print", false, null);
			printWindow.close();
		}
		else{
			setTimeout(function () {				
				printWindow.print();
				printWindow.close();
			}, 100);
		}
	}else{
		alert("No matching records found");
	}
});
</script>
</body>
<!-- END BODY -->
</html>