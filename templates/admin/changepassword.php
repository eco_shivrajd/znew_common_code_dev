<!-- BEGIN HEADER -->
<?php include "../includes/header.php"?>
<!-- END HEADER -->
<?php
if(isset($_POST['submit']))
{	
	$id=$_SESSION[SESSION_PREFIX.'user_id'];
	$old_pass=$_POST['old_password'];
	$session_pass=$_SESSION[SESSION_PREFIX.'password'];
	$pass=$_POST['pwd'];
	$password=md5($pass);

	$result = mysqli_query($con, "select id,common_user_id from `tbl_user` where id = '$id'");
	$count = mysqli_fetch_array($result);
	$common_user_id=$count['common_user_id'];


	if($session_pass == $old_pass)
	{
		$update_sql=mysqli_query($con,"UPDATE tbl_user SET pwd='$password' where id='$id'");
		$update=mysqli_query($conmain,"UPDATE tbl_users SET passwd='$password' where uid='$common_user_id'");
?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script type="text/javascript"> 
       $(document).ready(function(){
           $('#thankyouModal').modal('show');
       });
      </script>
	  	  <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" 
		  aria-labelledby="thankyouLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
              <center>  <h4 class="modal-title" id="myModalLabel">Change Password</h4></center>
            </div>
            <div class="modal-body">
                <p>
				<h5 style="color:green; text-align:center;">Password updated successfully.</h5>
				</p>                     
        	  <center><a href="../logout.php"><button type="button" class="btn btn-success">OK</button></a></center>
            </div>    
        </div>
    </div>
</div>

<?php
}
else
{
	$e="You have entered wrong old password";
}
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include "../includes/sidebar.php"?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Password
			</h3>
      
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Change Password
							</div>
						</div>
						<div class="portlet-body">
						
  <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
   <form class="form-horizontal" data-parsley-validate="" role="form" method="post" action="">             
            <div class="form-group">
              <label class="col-md-3">Password:<span class="mandatory">*</span></label>
              <div class="col-md-4">
			    <input type="hidden" value="<?php echo $_SESSION[SESSION_PREFIX.'password']?>" id="check_password">
                <input type="password" name="old_password" id="old_password" 
				placeholder="Enter Old Password"
                data-parsley-trigger="change"				
				data-parsley-required="#true"
                data-parsley-equalto="#check_password"
				data-parsley-equalto-message="Please enter correct old password"
				data-parsley-required-message="Please enter old password"
				class="form-control" value="" >
				<span style="color:red"><?php echo $e; ?></span>
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <label class="col-md-3">New Password:<span class="mandatory">*</span></label>

              <div class="col-md-4"> 
				<input type="password" id="new_password" 
					placeholder="Enter Password" 
					data-parsley-minlength="6" 
					data-parsley-minlength-message="Password should be minimum 6 characters without blank spaces" 
					data-parsley-maxlength="15" 
					data-parsley-maxlength-message="Only 15 characters are allowed" 
					data-parsley-type-message="Password should be minimum 6 characters without blank spaces" 
					data-parsley-required-message="Please enter Password" data-parsley-trigger="change" 
					data-parsley-required="true" 
					data-parsley-pattern="/^\S*$/" 
					data-parsley-error-message="Password should be 6-15 characters without blank spaces" 
				name="new_password" class="form-control placeholder-no-fix">
				
              </div>
            </div><!-- /.form-group -->
  
            <div class="form-group">
              <label class="col-md-3">Confirm password:<span class="mandatory">*</span></label>

              <div class="col-md-4">
				<input type="password" 
				id="pwd" 
				placeholder="Enter Confirm Password" 
				data-parsley-trigger="change" 
				data-parsley-equalto="#new_password" 
				data-parsley-equalto-message="Password does not match with confirm password" 
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter confirm password" 
				name="pwd" class="form-control">
              </div>
            </div><!-- /.form-group -->
                       
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
                <button name="submit" id="submit" class="btn btn-primary">Submit</button>
                <a href="index.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group -->
          </form>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
<style>
.form-horizontal{
font-weight:normal
}
</style>
</body>
<!-- END BODY -->
</html>
