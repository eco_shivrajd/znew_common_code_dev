<?php
include ("../../includes/config.php");
include "../includes/userManage.php";
include "../includes/orderManage.php";
$userObj 	= 	new userManager($con,$conmain);
$orderObj 	= 	new orderManage($con,$conmain);
extract($_POST);
$record_sp = $userObj->getLocalUserDetails($dropdownSalesPerson);

//$result_product = $orderObj->getSalesPOrdersProducts($frmdate,$dropdownSalesPerson);
//$record_product_count = mysqli_num_rows($result_product);

/*
    [order_id] => 1
    [shop_id] => 90
    [shopname] => test
    [address] => 1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA Mountain View United States
    [mobile] => 3557437437   
    [order_date] => 2019-04-10 11:02:54   
    [categorynm] => test cat   
    [cat_id] => 1   
    [oplacelat] => 37.4219983   
    [oplacelon] => -122.084   
    [no_o_lat] =>     
    [no_o_long] =>     
    [sum_product_quantity] => 1   
    [sum_total_order_gst_cost] => 104    
    [cityname] => Gurgaon    
    [statename] => Haryana   
    [date] => 2019-04-10 11:02:54  
    [shop_visit_reason] =>    
    [shop_close_reason_type] =>    
    [shop_close_reason_details] => 
*/
$resultsplocation = $orderObj->getSPLocationPoints($frmdate,$dropdownSalesPerson);
$record_count_sp = mysqli_num_rows($resultsplocation);

$resultsp_startlocation = $orderObj->getSPStartLocationPoints($frmdate,$dropdownSalesPerson);
$record_count_sp_startlocation = mysqli_num_rows($resultsp_startlocation);

if($record_count_sp_startlocation > 0){
	while($record11 = mysqli_fetch_array($resultsp_startlocation)){ 
		$lattitude11 = $record11['lattitude'];
		$longitude11 = $record11['longitude'];
		$start_date_time = $record11['start_date_time'];
	}	
	$start_location11 = "<br>".$orderObj->getLocationsp($lattitude11,$longitude11);
	$start_time11 = "<br>(Time: ".date('H:i:s',strtotime($start_date_time)).")";
}

$resultsp_endlocation = $orderObj->getSPEndLocationPoints($frmdate,$dropdownSalesPerson);
$record_count_sp_endlocation = mysqli_num_rows($resultsp_endlocation);
if($record_count_sp_endlocation > 0){
	while($record22 = mysqli_fetch_array($resultsp_endlocation)){ 
		$lattitude22 = $record22['lattitude'];
		$longitude22 = $record22['longitude'];
		$end_date_time = $record22['end_date_time'];
	}	
	$end_location22 = "<br>".$orderObj->getLocationsp($lattitude22,$longitude22);
	$end_time22 = "<br>(Time: ".date('H:i:s',strtotime($end_date_time)).")";
}

$result = $orderObj->getSalesPOrders($frmdate,$dropdownSalesPerson);
$record_count = mysqli_num_rows($result);
$start_location = "";	
$end_location = "";


$records_sp_day_time = $orderObj->getSalesPStartEndDay($frmdate,$dropdownSalesPerson);

?>
<? if($_GET["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<? } ?>

	
	<div class="portlet-body">
		<div class="table-responsive" id="dvtblResonsive">
			<table class="table table-bordered" id="report_table">
				<thead>
					<tr>
						<th valign="top" colspan="4"><b>DATE:<?=$frmdate;?></b></th>	
						<th colspan="3" align="left"><b><?=$record_sp['firstname'];?></b>
							<input type="hidden" name="date" id="date" value="<?=$frmdate;?>">
							<input type="hidden" name="salespname" id="salespname" value="<?=$record_sp['firstname'];?>">
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td valign="top" colspan="4" >STARTING POINT: <?=$start_location11.$start_time11;?></td>						
						<td valign="top" colspan="3">
					<?php 
					$i = 1;
					$countrec=0;$c_array_temp=array();
					if($record_count_sp > 0){						
						while($record = mysqli_fetch_array($resultsplocation)){	
							 $frmdate = $record['tdate'];
							 $dropdownSalesPerson = $record['userid'];
							$c_array_temp[$countrec]['id'] = $record['id'];
							$c_array_temp[$countrec]['lattitude'] = $record['lattitude'];
							$c_array_temp[$countrec]['longitude'] = $record['longitude'];
							$countrec++;
						}
					}                 
					$google_distance=0;
					for($i=0;$i<count($c_array_temp)-1;$i++){
						$google_d= $orderObj->getDistanceBetweenPoints($c_array_temp[$i]['lattitude'],$c_array_temp[$i+1]['lattitude'],$c_array_temp[$i]['longitude'],$c_array_temp[$i+1]['longitude']);
						$google_distance=$google_distance+$google_d['distance'];
					} 
					$google_distance=bcdiv($google_distance, 1000, 3);	
					$SPattendance = $userObj->updateSPattendance($frmdate,$dropdownSalesPerson,$google_distance);                     
                   ?>
						End Point: <?=$end_location22.$end_time22;?>	
						Total Distance Travelled By Salesperson  : <?php echo $google_distance ;?> KM
						</td>						
					</tr>
					
					<?  if($record_count > 0) { ?>
					<tr>
						<td valign="top">S. No.</td>
						<td valign="top" style="text-align:center">OUTLET NAME</td>
						<td valign="top" style="text-align:center;width: 150px;">TIME VISITED & ADDRESS</td>
						<td valign="top" style="text-align:center;width: 150px;">PUNCHED LOCATION</td>
						<td valign="top" style="text-align:center">CONTACT NO. OF SHOP</td>	
						<td valign="top" colspan="2" style="text-align:center">PRODUCTS QNTY/COST</td>	
					</tr>
					<?php 
					$i = 1;
					$shop_total = 0;
					$totalcost = 0;
					if($record_count > 0){
					$total_qnty=0;$total_cost_total=0;
					while($record = mysqli_fetch_array($result)){ 
                        //echo "<pre>";
						//print_r($record);
						$shop_total = 0;
						$order_time = " (Time: ".date('H:i:s',strtotime($record['date'])).")";
						
						$order_location = "";
						$order_location = $orderObj->getLocation($record['oplacelat'],$record['oplacelon']);
						//echo $record['order_id'];
					?>
					<tr>
						<td valign="top"><?=$i;?></td>
						<td valign="top"><?=$record['shopname'];?></td>
						<td valign="top" ><?=$order_time."<br>".$record['address']."<br>".$record['cityname']."<br>".$record['statename'];?></td>
						<td valign="top"><?=$order_location;?></td>		
						<td valign="top"><?=($record['mobile'] == 0) ? '-': $record['mobile'];?></td>	
						<td valign="top" colspan="2" align="left"><?php
						//echo $record['sum_product_quantity'];
							
						if(!empty($record['sum_product_quantity'])){
							//echo "jghdfjgbsd";
							if(!empty($record['order_id'])){
								//echo "khkfkk";
								$orderdetails = $orderObj->getOrdersDetailschnage($record['order_id']);
								?><table class="table table-striped table-bordered table-hover">
										<tbody>
								<?php $total_date_qnty=0;$total_date_cost=0;
								foreach($orderdetails['order_details'] as $key_order=>$value_s){ ?>
										<tr>
											<td align="left"><?php  echo $value_s['brand_name']."  ".$value_s['cat_name'];?></td>
											<td align="left"><?php  echo $value_s['product_name']." ";
												if(!empty($value_s['product_variant_weight1'])){
													echo $value_s['product_variant_weight1']."-".$value_s['product_variant_unit1']."  ";
												}if(!empty($value_s['product_variant_weight2'])){
													echo $value_s['product_variant_weight2']."-".$value_s['product_variant_unit2']."";
												}?>
											</td>
											<td align="right"><?php  	echo $value_s['product_quantity'];?></td>
											<td align="right"><?php  	echo $value_s['p_cost_cgst_sgst'];?></td>
											
										</tr>
								<?php 	$total_date_qnty+=$value_s['product_quantity'];
										$total_date_cost+=$value_s['p_cost_cgst_sgst'];
								} ?>
										<tr>
											<td colspan="2" style="text-align:right;"><b>Total</b></td>
											<td><?php  	echo $total_date_qnty;?></td>
											<td><?php  	echo $total_date_cost;?></td>
										</tr>
								</tbody>
									</table>
							<?php }
							//echo "<br>";
							$total_qnty=$total_qnty+$record['sum_product_quantity'];
							$total_cost_total=$total_cost_total+$record['sum_total_order_gst_cost'];
							//echo $record['sum_product_quantity']."/".$record['sum_total_order_gst_cost'];
						}else{echo "-";}?></td>	
						
					</tr>
					<?php $i++; }  ?>	
					<tr>
						<td valign="top" colspan="5" style="text-align:right;"><b>Total ₹</b></td>	
						<td valign="top" align="right"><?php 
						if($total_qnty>0){
							echo $total_qnty."/".number_format($total_cost_total,2, '.', '');
						}else{echo "-";}
						?></td>	
					</tr>
				<?php } ?>
				</tbody>
					<?php }else{
						echo "<tr ><td align='center' colspan='14'>No Record available.</td></tr>";
					} ?>
			</table>
		</div>
	</div>
