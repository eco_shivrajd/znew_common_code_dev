<div class="form-group">
	  <label class="col-md-3">State:</label>
	  <div class="col-md-4">
		  <select name="state_campaign_discount[]" id="state_campaign_discount" data-parsley-trigger="change" disabled class="form-control" onChange="fnShowCity(this.value)" multiple>
		 
			<?php 
			$sql="SELECT * FROM tbl_state where country_id=101 AND id IN (".$row_campaign['campaign_area']['state_id'].") ";
			$result = mysqli_query($con,$sql);
			while($row = mysqli_fetch_array($result))
			{
				$cat_id=$row['id'];
				$state_selected = ''; 
				if(in_array($row['id'],$row_campaign['campaign_area']['state_id_array']))
					$state_selected = 'selected'; 
				
				echo "<option value='$cat_id' $state_selected>" . $row['name'] . "</option>";
			}
			?>
			</select>
			<?php 
			//count($row_campaign['campaign_area']['state_id_array']) > 1 &&  
			if($row_campaign['campaign_area']['level'] == 'state'){
				echo "<br>This Campaign will get applied to All Shops which comes under these States";
			}?>
	  </div>
	</div><!-- /.form-group -->
	<?php if($row_campaign['campaign_area']['state_id_array'][0] != ''  && count($row_campaign['campaign_area']['state_id_array']) <= 1)
			{
				if($row_campaign['campaign_area']['city_id_array'][0] != ''  && count($row_campaign['campaign_area']['city_id_array']) != 0)
				{?>
	<div class="form-group">
		  <label class="col-md-3">District:</label>
		  <div class="col-md-4" id="div_city_campaign_discount">									 
				<?php
						
							 $sql="SELECT name,id FROM tbl_city 
							WHERE state_id IN (".$row_campaign['campaign_area']['state_id_array'][0].") 
									AND id IN (".$row_campaign['campaign_area']['city_id'].") order by name";
							$result1 = mysqli_query($con,$sql);
					?>
							<select name="city_campaign_discount" id="city_campaign_discount" 
								disabled 
								class="form-control" 
							onchange="FnGetSuburbDropDown(this)" multiple>
							
							<?php while($row = mysqli_fetch_array($result1))
							{
								$selected = "";
								if(in_array($row["id"],$row_campaign['campaign_area']['city_id_array']))
									$selected = "selected";
								$assign_id=$row['id']; 
							?>
								<option value="<?=$assign_id;?>" <?=$selected; ?>><?=$row['name']; ?></option>
							<?php } 
						?>
			</select>
			<?php 
			//count($row_campaign['campaign_area']['city_id_array']) > 1 && 
			if( $row_campaign['campaign_area']['level'] == 'city'){
				echo "<br>This Campaign will get applied to All Shops which comes under these Districts";
			}?>
		  </div>
	</div><!-- /.form-group -->
			<?php }
			} ?>
	<?php
			if($row_campaign['campaign_area']['city_id_array'][0] != '' && count($row_campaign['campaign_area']['city_id_array']) <= 1)
			{
				if($row_campaign['campaign_area']['suburb_id_array'][0] != ''  && count($row_campaign['campaign_area']['suburb_id_array']) != 0)
				{ ?>
	<div class="form-group">
	  <label class="col-md-3">Taluka:</label>
	  <div class="col-md-4" id="div_suburb_campaign_discount">	
		
			<select name="suburb_campaign_discount" id="suburb_campaign_discount" class="form-control" onchange="FnGetSubareaDropDown(this)" disabled multiple>
		
			<?php			
			$sql="SELECT id , suburbnm FROM tbl_area 
			WHERE cityid IN  (".$row_campaign['campaign_area']['city_id_array'][0].") 
			AND id IN (".$row_campaign['campaign_area']['suburb_id'].") order by suburbnm ";
			$result1 = mysqli_query($con,$sql);
			$rowcount=mysqli_num_rows($result1);
			while($row = mysqli_fetch_array($result1))
			{	
				$selected = "";
				if(in_array($row["id"],$row_campaign['campaign_area']['suburb_id_array']))
					$selected = "selected";
			?>
					<option value='<?=$row["id"];?>' <?=$selected;?>><?=$row["suburbnm"];?></option>
		<?php	}
		 ?>
		</select>
		<?php 
		//count($row_campaign['campaign_area']['suburb_id_array']) > 1 &&  
			if($row_campaign['campaign_area']['level'] == 'suburb'){
				echo "<br>This Campaign will get applied to All Shops which comes under these Taluka";
			}?>
	  </div>
	</div><!-- /.form-group --> 
	<?php }
	} ?>
	<?php if($row_campaign['campaign_area']['suburb_id_array'][0] != '' && count($row_campaign['campaign_area']['suburb_id_array']) <= 1)
			{
				if($row_campaign['campaign_area']['subarea_id_array'][0] != ''  && count($row_campaign['campaign_area']['subarea_id_array']) != 0)
				{ ?>
	<div class="form-group">
	  <label class="col-md-3">Subarea:</label>
	  <div class="col-md-4" id="div_select_subarea">	
		
			<select name="subarea" id="subarea" class="form-control" onchange="FnGetShopsDropdown(this)" disabled multiple>
		
			<?php			
			$sql="SELECT subarea_id , subarea_name FROM tbl_subarea 
			WHERE suburb_id IN (".$row_campaign['campaign_area']['suburb_id_array'][0].") 
			AND subarea_id IN (".$row_campaign['campaign_area']['subarea_id'].") order by subarea_name ";
			$result1 = mysqli_query($con,$sql);
			$rowcount=mysqli_num_rows($result1);
			while($row = mysqli_fetch_array($result1))
			{	
				$selected = "";
				if(in_array($row["subarea_id"],$row_campaign['campaign_area']['subarea_id_array']))
					$selected = "selected";
			?>
					<option value='<?=$row["subarea_id"];?>' <?=$selected;?>><?=$row["subarea_name"];?></option>
		<?php	}
		 ?>
		</select>
		<?php 
		//count($row_campaign['campaign_area']['suburb_id_array']) > 1 &&
		//echo $row_campaign['campaign_area']['level'];  
			if($row_campaign['campaign_area']['level'] == 'subarea'){
				echo "<br>This Campaign will get applied to All Shops which comes under these Subarea";
			}?>
	  </div>
	</div><!-- /.form-group --> 
	<?php }
	} ?>
	  <?php 
			if($row_campaign['campaign_area']['suburb_id_array'][0] != '' && count($row_campaign['campaign_area']['suburb_id_array']) <= 1)
			{
				if($row_campaign['campaign_area']['shop_id_array'][0] != ''  && count($row_campaign['campaign_area']['shop_id_array']) != 0)
				{
		?>
	<div class="form-group">
	  <label class="col-md-3">Shop:</label>
	  <div class="col-md-4" id="div_shop_campaign_discount">
	
	  <select name="shop_campaign_discount" id="shop_campaign_discount" data-parsley-trigger="change" disabled multiple class="form-control">
	
		<?php
		$sql="SELECT id , name FROM tbl_shops 
		where suburbid IN (".$row_campaign['campaign_area']['suburb_id_array'][0].") 
		AND id IN (".$row_campaign['campaign_area']['shop_id'].")  order by name";
		$result1 = mysqli_query($con,$sql);
		while($row = mysqli_fetch_array($result1))
		{
			$selected = "";
			if(in_array($row["id"],$row_campaign['campaign_area']['shop_id_array']))
				$selected = "selected";
			$assign_id=$row['id']; 
		?>
			<option value='<?=$assign_id;?>' <?=$selected;?>><?=$row['name'];?></option>
		<?php }
		?>									
		</select>

	  </div>
	</div><!-- /.form-group --> 
		<?php }
		} ?>
<script>
function setSelectNoValue(div,select_element){
	var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled>-Select-</option></select>';
	document.getElementById(div).innerHTML	=	select_selement_section;
}
function calculate_data_count(element_value){
	element_value = element_value.toString();
	var element_arr = element_value.split(',');	
	return element_arr.length;
}
function fnShowCity(id_value) {	
	var state_str = $("#state_campaign_discount").val(); 	
	var state_arr_count = calculate_data_count(state_str);
	if(state_arr_count == 1){//If single state selected then only show its related Cities
		var hid_city = $("#hid_city").val();
		var param = "";
		if(hid_city != '')
			param = "&selectedval="+hid_city;
		var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city_campaign_discount&multiple=multiple"+param;
		CallAJAX(url,"div_city_campaign_discount");
	}else{
		setSelectNoValue("div_city_campaign_discount", "city_campaign_discount");
	}	
}
function FnGetSuburbDropDown(id) {
	var data_id_value = '';
	if(typeof id === 'string')
		data_id_value = id;
	else
		data_id_value = id.value;
		
	var city_str = $("#city_campaign_discount").val();
	if(city_str != null)
	{
		var city_arr_count = calculate_data_count(city_str);
		if(city_arr_count == 1){//If single city selected then only show its related suburb
			var hid_suburb = $("#hid_suburb").val();
			var param = "";
			if(hid_suburb != '')
				param = "&selectedval="+hid_suburb;
			var url = "getSuburDropdown.php?cityId="+data_id_value+"&select_name_id=suburb_campaign_discount&multiple=multiple"+param;
			CallAJAX(url,"div_suburb_campaign_discount");
		}else{
			setSelectNoValue("div_suburb_campaign_discount", "suburb_campaign_discount");
		}	
	}else{
		var hid_suburb = $("#hid_suburb").val();
		var url = "getSuburDropdown.php?cityId="+data_id_value+"&select_name_id=suburb_campaign_discount&multiple=multiple&selectedval="+hid_suburb;		
		CallAJAX(url,"div_suburb_campaign_discount");
	}
}
function FnGetShopsDropdown(id) {	
	var data_id_value = '';
	if(typeof id === 'string')
		data_id_value = id;
	else
		data_id_value = id.value;
	
	var suburb_str = $("#suburb_campaign_discount").val();	
	if(suburb_str != null)
	{	
		var suburb_arr_count = calculate_data_count(suburb_str);
		if(suburb_arr_count == 1){//If single suburb selected then only show its related Shops
			var hid_shop = $("#hid_shop").val();
			var param = "";
			if(hid_shop != '')
				param = "&selectedval="+hid_shop;
			var url = "getShopDropdown.php?suburbid="+data_id_value+"&select_name_id=shop_campaign_discount&multiple=multiple"+param;
			CallAJAX(url,"div_shop_campaign_discount");
		}else{
			setSelectNoValue("div_shop_campaign_discount", "shop_campaign_discount");
		}
	}else{
		var hid_shop = $("#hid_shop").val();
		var url = "getShopDropdown.php?suburbid="+data_id_value+"&select_name_id=shop_campaign_discount&multiple=multiple&selectedval="+hid_shop;
			CallAJAX(url,"div_shop_campaign_discount");
	}
}		
</script>