<!-- BEGIN HEADER -->
<?php
//phpinfo();die();
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include "../includes/grid_header.php";
include "../includes/userManage.php";

include "../includes/productManage.php";
include "../includes/orderManage.php";
$userObj = new userManager($con, $conmain);

$prodObj = new productManage($con, $conmain);
$orderObj = new orderManage($con, $conmain);


?>
<!-- END HEADER -->
<style>
    .form-horizontal .control-label {
        text-align: left;
    }
	small, .small {
	 font-size: 77%;
	} 
	/*new*/
	
nav > .nav.nav-tabs{

  border: none;
    color:#fff;
    background:#272e38;
    border-radius:0;

}
nav > div a.nav-item.nav-link,
nav > div a.nav-item.nav-link.active
{
  border: none;
    padding: 18px 25px;
    color:#fff;
    background:#272e38;
    border-radius:0;
}

nav > div a.nav-item.nav-link.active:after
 {
  content: "";
  position: relative;
  bottom: -60px;
  left: -10%;
  border: 15px solid transparent;
  border-top-color: #e74c3c ;
}
.tab-content{
  background: #fdfdfd;
    line-height: 25px;
    border: 1px solid #ddd;
    border-top:5px solid #e74c3c;
    border-bottom:5px solid #e74c3c;
    padding:30px 25px;
}

nav > div a.nav-item.nav-link:hover,
nav > div a.nav-item.nav-link:focus
{
  border: none;
    background: #e74c3c;
    color:#fff;
    border-radius:0;
    transition:background 0.20s linear;
}
	/*new*/
	</style>
	<!--style for on//off switch-->
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">


</head>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLEAPIKEY; ?>&callback=initMap" type="text/javascript"></script>
<!-- END HEAD -->
<body class="page-header-fixed page-quick-sidebar-over-content ">

    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "Orders";
        $activeMenu = "Ordersall";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->

        <div class="page-content-wrapper">
            <div class="page-content">			
                <h3 class="page-title">
                    Orders 
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="#">Orders </a>
                        </li>
                    </ul>				
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">
                                    Orders 
                                </div>
								<div class="clearfix"></div>	
                            </div>						
                            <div class="portlet-body">
								<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">							
								<li role="presentation" class="active">
									<a href="#orders_from_sp_sk" aria-controls="orders_from_sp_sk" 
									role="tab" data-toggle="tab">From Retailers</a>
								</li>
								<li role="presentation" >
									<a href="#orders_from_cust" aria-controls="orders_from_cust" 
									role="tab" data-toggle="tab">From Customers</a>
								</li>							
								<!--<li role="presentation">
									<a href="#orders_all" aria-controls="orders_all" 
									role="tab" data-toggle="tab">All </a>
								</li>-->
							</ul>
						
							<!-- Tab panes -->							
							<div id="orders_form_table">
                                <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">
									<input type="hidden" value="sp_user" id="tabpanelids" name="tabpanelids">
                                    <div class="form-group" id="order_status_tab_result">
                                        <label class="col-md-3">Order Status:</label>
                                        <div class="col-md-4">
                                            <select name="order_status" id="order_status" autocomplete="off" data-parsley-trigger="change" class="form-control" onchange="fnShowUser(this.value)">
												<option value="1" selected>Received </option><!--To Me(<small class="muted text-muted">By Sales Person Or Shop Keeper</small>)-->
                                                <option value="2">Assigned for Delivery</option>   			
                                                <option value="4">Delivered</option> 
												<option value="0" >All </option>												
												<option value="11" >Received</option><!-- To Me(<small class="muted text-muted">By Other User</small>)-->
												<!--<option value="22">Assigned for Delivery</option>	-->		
                                                <option value="44">Delivered</option>
												<option value="10">All Customers</option>
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->	
									
                                    <div class="form-group" id="divDaily">
                                        <label class="col-md-3">Select Order Date:</label>
                                        <div class="col-md-4">
                                            <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                                                <input type="text" class="form-control" data-date="<?php echo date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years" name="frmdate" id="frmdate" value="" autocomplete="off"> 
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                            <!-- /input-group -->								 
                                        </div>
                                    </div><!-- /.form-group -->	
								<?php $session_user_id=$_SESSION[SESSION_PREFIX . "user_id"];?>
                                     

									<div class="form-group" >
                                        <label class="col-md-3">Orders From:</label>
                                        <div class="col-md-4" id="div_select_user">
                                            <select name="order_from" id="order_from" class="form-control">
                                                   <?php          
                                                    $sql = "SELECT firstname as name,id,user_role FROM `tbl_user` where external_id ='$session_user_id' and id!='1' and (user_role = 'Shopkeeper' OR user_role = 'SalesPerson') order by firstname";
                                                    $result1 = mysqli_query($con, $sql); 
													$sql_cust = "SELECT cust_name as name,cust_id as id FROM `tbl_customer` where  isdeleted!='1' 
													order by cust_name";
                                                    $result1_cust = mysqli_query($con, $sql_cust);
													$cust_string="";
													while ($row_cust = mysqli_fetch_array($result1_cust)) {
                                                        $cust_string.= "<option class='cust_user_class' value='".$row_cust['id']."'>" . fnStringToHTML($row_cust['name']) . "</option>";
                                                    }
													?>
												<option value="">-Select User-</option>
                                                 <?php $sp_string="";
                                                    while ($row = mysqli_fetch_array($result1)) {
                                                        $cat_id = $row['id'];
														$sp_string.="<option class='sp_user_class' value='".$row['id']."'>" . fnStringToHTML($row['name']) . "</option>";
                                                    }
													echo $sp_string;
													echo $cust_string;
													?>
											</select>
                                        </div>
                                    </div><!-- /.form-group -->
                                   
                                    <div class="form-group" id="state_div">
                                        <label class="col-md-3">State:</label>
                                        <div class="col-md-4">
                                            <select name="dropdownState" id="dropdownState"              
                                                    data-parsley-trigger="change"				
                                                    data-parsley-required="#true" 
                                                    data-parsley-required-message="Please select state"
                                                    class="form-control" onChange="fnShowCity(this.value)">
                                                <option selected disabled>-Select-</option>
                                                <?php
                                                $sql = "SELECT id,name FROM tbl_state where country_id=101 order by name";
                                                $result = mysqli_query($con, $sql);
                                                while ($row = mysqli_fetch_array($result)) {
                                                    $cat_id = $row['id'];
                                                    echo "<option value='$cat_id'>" . $row['name'] . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>			
                                    <div class="form-group" id="city_div" style="display:none;">
                                        <label class="col-md-3">City:</label>
                                        <div class="col-md-4" id="div_select_city">
                                            <select name="dropdownCity" id="dropdownCity" data-parsley-trigger="change" class="form-control">
                                                <option selected value="">-Select-</option>										
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group" id="area_div" style="display:none;">
                                        <label class="col-md-3">Region:</label>
                                        <div class="col-md-4" id="div_select_area">
                                            <select name="area" id="area" data-parsley-trigger="change" class="form-control">
                                                <option selected value="">-Select-</option>									
                                            </select>
                                        </div>
                                    </div>									
                                    
                                    <div class="form-group">
                                        <label class="col-md-3">Category:</label>
                                        <div class="col-md-4" id="divCategoryDropDown">
                                            <?php $cat_result = $prodObj->getAllCategory(); ?>
                                            <select name="dropdownCategory" id="dropdownCategory" class="form-control" onchange="fnShowProducts(this)">
                                                <option value="">-Select-</option>
                                                <?php while ($row_cat = mysqli_fetch_assoc($cat_result)) {
                                                    ?>									
                                                    <option value="<?= $row_cat['id']; ?>"><?= $row_cat['categorynm']; ?></option>
                                                <?php } ?>								
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label class="col-md-3">Product:</label>
                                        <div class="col-md-4" id="divProductdropdown">
                                            <?php $prod_result = $prodObj->getAllProducts(); ?>
                                            <select name="dropdownProducts" id="dropdownProducts" class="form-control">
                                                <option value="">-Select-</option>
                                                <?php while ($row_prod = mysqli_fetch_assoc($prod_result)) {
                                                    ?>									
                                                    <option value="<?= $row_prod['id']; ?>"><?= $row_prod['productname']; ?></option>
                                                <?php } ?>	
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-3">											
                                            <button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>									
                                            <button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnReset();">Reset</button>
                                        </div>
                                    </div><!-- /.form-group -->
                                </form>	
                               
								<div>
                                    <button type="button" class="btn btn-success" id="statusbtn" name="statusbtn" data-toggle="modal">  
                                        <span id="delivery_id">Assign For Delivery/Mark As Delivered</span>
                                       <span id="delivered_id" style="display:none;">Mark As Delivered</span>
                                    </button>
                                </div><br>									
                                    <div id="order_list"> </div>
							</div>
                        </div>
						<!--portable body-->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>

    <!-- END CONTAINER -->
    <div class="modal fade" id="view_invoice" role="dialog">
        <div class="modal-dialog" style="width: 980px !important;">    
            <!-- Modal content-->
            <div class="modal-content" id="view_invoice_content">      
            </div>      
        </div>
    </div>
    <div class="modal fade " id="order_details" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 880px !important;">
            <!-- Modal content-->
            <div class="modal-content" id="order_details_content">
            </div>
        </div>
    </div>

    <div class="modal fade" id="googleMapPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="model_content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                </div>
                <div class="modal-body" style="padding-bottom: 5px !important;"> 
                    <div id="map" style="width: 100%; height: 500px;"></div> 
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="assign_delivery" role="dialog" style="height: auto;">
        <div class="modal-dialog">    
            <!-- Modal content-->
            <div class="modal-content"  >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Assign For Delivery Or Mark As Delivered</h4>
                </div>
                <div class="modal-body" style="min-height: 170px;padding-bottom: 5px !important;">
                    <form class="form-horizontal" role="form" data-parsley-validate=""  id="assign_delivery_form"  enctype="multipart/form-data">
						<div class="form-group">
							<label class="col-md-4">Delivery Type:</label>
							 <div class="col-md-8">
                                <span id="custnew_id"><input type="radio" name="deliveryType" id="deliveryType_assign" value="assign" checked onclick="fnChangeDeliveryType('assign');">&nbsp;Assign For Delivery 
                                &nbsp;</span>
                                <input type="radio" name="deliveryType" id="deliveryType_delivered" value="delivered" onclick="fnChangeDeliveryType('delivered');">&nbsp;Mark As Delivered 
                            </div>
						</div><!-- /.form-group -->
						<input type="hidden" value="" id="orderids" name="orderids">
						<div id="pro_table_div" >
						</div>
						<div id="assigned_div" >
							<div class="form-group">
								<label class="col-md-4">Assignment Date:<span class="mandatory">*</span></label>
								<div class="col-md-6">
									<div class="input-group date date-picker3" data-date-format="dd-mm-yyyy">
										<input type="text" name="del_assigned_date" id="del_assigned_date" class="form-control" 
										placeholder="Assignment Date" value="<?php echo date('d-m-Y'); ?>">
										<!--readonly style="cursor: not-allowed;background-color: #eee;" -->
										<span class="input-group-btn">
											<button class="btn default" type="button" ><i class="fa fa-calendar"></i></button>
										</span>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="form-group">
								<label class="col-md-4" style="padding-top:5px">Select Delivery Person:<span class="mandatory">*</span></label>

										<?php $user_result = $userObj->getAllLocalUserNew('DeliveryPerson'); ?>		
								<div class="col-md-6" id="divdpDropdown">
									<select name="selectdp" id="selectdp" class="form-control"  >
										<option value="">-- Select --</option>
										<?php while ($row_user = mysqli_fetch_assoc($user_result)) { ?>									
											<option value="<?= $row_user['id']; ?>"><?= $row_user['firstname']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div><!-- /.form-group --> 
							<div class="clearfix"></div>
							<div class="form-group">
								<div class="col-md-offset-5 col-md-9">
									<button type="button" name="btn_assigndelivery" id="btn_assigndelivery"  onclick="return assign_delivery_clicked();"  class="btn btn-primary" >Assign</button><!--data-dismiss="modal"-->
								</div>
							</div>
						</div>
						
						<div id="delivered_div" style="display:none;">							                    
							<div class="form-group">
								<div class="col-md-offset-5 col-md-9">
									<button type="button" name="btn_delivered" id="btn_delivered" onclick="return btn_delivered_clicked();" class="btn btn-primary" data-dismiss="modal">Set Delivered</button>
								</div>
							</div>
						</div>
						<div id="add_stock_div" style="display:none;">							                    
							<div class="form-group">
								<div class="col-md-offset-5 col-md-9">
									<a  id="btn_add_stock" href="stock_add.php" class="btn btn-primary" >Add Stock</a>
								</div>
							</div>
						</div>
						
                    </form>
                </div>	
            </div>
        </div>
    </div>
    

    <!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php" ?>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- END FOOTER -->
	<script>
	
	$('[data-toggle="tab"]').click(function(e) {
		//var activeTab = $(".tab-content").find(".active");
		var id = $(this).attr('href');	
        if(id=='#orders_from_cust')
        {
             $('#custnew_id').hide();
             fnChangeDeliveryType('delivered');
             $("#deliveryType_delivered").prop("checked", true);
             $('#delivery_id').hide();
             $('#delivered_id').show();
        }
        if(id=='#orders_from_sp_sk')
        {
             $('#custnew_id').show();
             $('#delivery_id').show();
             $('#delivered_id').hide();
        }	
		if(id=='#orders_all'){
			//alert(id);
			$('#order_status').val('0');
			$('#state_div').hide();
			$('#tabpanelids').val('customer');
			$('#order_status_tab_result').hide();
			ShowReport();
			$('#order_from').val('');
			$('#order_from').prop('disabled', true);
		
		}else if(id=='#orders_from_cust'){	
			$('#state_div').hide();
			$('#tabpanelids').val('customer');
			$('#order_status_tab_result').show();
			$("#order_status > option").each(function() {
				if(this.value=='0'|| this.value=='1'||this.value=='2'||this.value=='4'){
					$(this).hide();
				}else{
					$(this).show();
				}
			});
			$('#order_status').val('11');
			ShowReport();
			$('#order_from').prop('disabled', false);
			$("#order_from option[class='sp_user_class']").each(function() {
				$(this).hide();
			});
			$("#order_from option[class='cust_user_class']").each(function() {
				$(this).show();
			});
			$('#order_from option[value=""]').attr("selected",true);
		}else{
			//alert(id);
			$('#state_div').show();
			$('#tabpanelids').val('sp_user');
			$('#order_status_tab_result').show();			
			$("#order_status > option").each(function() {
				if(this.value=='10'|| this.value=='11'||this.value=='22'||this.value=='44'){
					$(this).hide();
				}else{
					$(this).show();
				}
			});	
			$('#order_status').val('1');
			ShowReport();
			$('#order_from').prop('disabled', false);
			$("#order_from option[class='sp_user_class']").each(function() {
				$(this).show();
			});
			$("#order_from option[class='cust_user_class']").each(function() {
				$(this).hide();
			});			
			$('#order_from option[value=""]').attr("selected",true);
			//var url = "getUserForOrderSearch.php?cat_id=1&select_name_id=order_from";
			//CallAJAX(url, "div_select_user");			
		}	
	});	
jQuery(function () { 		
	$("#order_status > option").each(function() {
		if(this.value=='10'|| this.value=='11'||this.value=='22'||this.value=='44'){
			$(this).hide();
		}else{
			$(this).show();
		}
	});	
	$("#order_from option[class='cust_user_class']").each(function() {
		$(this).hide();
	});
	$("#order_from option[class='sp_user_class']").each(function() {
		$(this).show();
	});
	$('#order_from option[value=""]').attr("selected",true);
}); 
</script>
<script>			
	$(document).ready(function () { 		
		$("#select_th").removeAttr("class");
		$("#main_th th").removeAttr("class");
		if ($.fn.dataTable.isDataTable('#sample_2')) {
			table = $('#sample_2').DataTable({});
			table.destroy();
			table = $('#sample_2').DataTable({
				"aaSorting": [],
			});
		}	
		ShowReport();
	});
 function checktotalchecked(obj){
	//alert(obj.value);
	if($(obj).is(':checked') == true)
	{		
		var checkbox_count = ($('input:checkbox').length) - 1;
		var check_count = ($('input:checkbox:checked').length);
		if(obj.value == 'on'){
			$('input:checkbox').attr('checked', true);
		}
		else if(checkbox_count == check_count)
			$('input:checkbox').attr('checked', true);
	}else{		
		$('#select_all').attr('checked', false);
		if(obj.value == 'on')
			$('input:checkbox').attr('checked', false);
	}
} 
/* function doWithThisElement(eleid){
			console.log(eleid);
			var dfghkdhf="'#"+eleid+"'";
			//debugger;
			  $('#'+eleid).datepicker({
                todayHighlight:true,
                format:'dd-mm-yyyy',
                rtl: Metronic.isRTL(),
				orientation: "auto",
				//startDate: 'd',
				endDate: 'd',
				autoclose:true
			}) 
			//debugger;
		} */
	$('.date-picker1').datepicker({
		rtl: Metronic.isRTL(),
		orientation: "auto",
		endDate: "<?php echo date('d-m-Y'); ?>",
		autoclose: true
	});
	
	 $('.date-picker2').datepicker({
			rtl: Metronic.isRTL(),
			orientation: "auto",
		   format: "dd-mm-yyyy",
			todayHighlight: true,
			startDate: "<?php echo date('d-m-Y'); ?>",
			endDate: "<?php echo date('d-m-Y'); ?>",
			setDate: "<?php echo date('d-m-Y'); ?>",
			autoclose: true
		});
	$('.date-picker3').datepicker({
		rtl: Metronic.isRTL(),
		orientation: "auto",
		startDate: 'd',
		endDate: '+3m',
		autoclose: true
	});
	function fnReset() {
		location.reload();
	}
	function fnChangeDeliveryType(deliery_type){
		if(deliery_type=='assign'){
			$('#assigned_div').show();
			$('#delivered_div').hide();
		}else{
			$('#delivered_div').show();
			$('#assigned_div').hide();
		}
		var radioValue = $("input[name='deliveryType']:checked"). val();
		if($('#flag_avail_stock').val()=='allred'){
			if(radioValue=='assign')$("button[id^='btn_assigndelivery']" ).hide();
			if(radioValue=='delivered')$("button[id^='btn_delivered']" ).hide();
		}else{
			if(radioValue=='assign')$("button[id^='btn_assigndelivery']" ).show();
			if(radioValue=='delivered')$("button[id^='btn_delivered']" ).show();
		}
	}

	function fnShowCity(id_value) {
		$("#city_div").show();
		$("#area").html('<option value="">-Select-</option>');
		$("#subarea").html('<option value="">-Select-</option>');
		var url = "getCityDropDown.php?cat_id=" + id_value + "&select_name_id=city&mandatory=mandatory";
		CallAJAX(url, "div_select_city");
	}

	function fnShowUser(id_value) 
	{
	   if(id_value=='11'||id_value=='1'){
			$('#order_from').prop('disabled', false);
			if(id_value=='1'){
			   $("#statusbtn").show();
				 	$("#order_from option[class='sp_user_class']").each(function() {
						$(this).show();
					});
					$("#order_from option[class='cust_user_class']").each(function() {
						$(this).hide();
					});
		   }else{				  
			   $("#statusbtn").hide();
			    	$("#order_from option[class='sp_user_class']").each(function() {
						$(this).hide();
					});
					$("#order_from option[class='cust_user_class']").each(function() {
						$(this).show();
					});
		   }
		  
			//$('#order_from').prop('disabled', false);
			//var url = "getUserForOrderSearch.php?cat_id=" + id_value + "&select_name_id=order_from";
			//CallAJAX(url, "div_select_user");
		}else{				
			$("#statusbtn").hide();	
			$('#order_from').val('');
			$('#order_from').prop('disabled', true);
		}
		//ShowReport();
	}

	function FnGetSuburbDropDown(id) {
		$("#area_div").show();
		$("#subarea").html('<option value="">-Select-</option>');
		var url = "getSuburDropdown.php?cityId=" + id.value + "&select_name_id=area&function_name=FnGetSubareaDropDown&mandatory=mandatory";
		CallAJAX(url, "div_select_area");
	}

	function FnGetShopsDropdown(id) {
		$("#shop_div").show();
		$("#divShopdropdown").html('<option value="">-Select-</option>');
		var param = "";
		var state_id = $("#dropdownState").val();
		var city_id = $("#city").val();
		var suburb_id = $("#area").val();
		if (state_id != '')
			param = param + "&state_id=" + state_id;
		if (city_id != '')
			param = param + "&city_id=" + city_id;
		if (suburb_id != '') {
			if (suburb_id != undefined) {
				param = param + "&suburb_id=" + suburb_id;
			}
		}
		if (id != '') {
			if (id != undefined)
				param = param + "&suburb_id=" + id.value;
		}

		var url = "getShopDropdownByAddress.php?param=param" + param;
		CallAJAX(url, "divShopdropdown");
	}
	function fnShowProducts(id) {
		var url = "getProductDropdown.php?cat_id=" + id.value;
		CallAJAX(url, "divProductdropdown");
	}
  
  
	function ShowReport() {
		var order_status = $('#order_status').val();	
		var user_type='<?=$user_type?>';
		//alert(order_status);
		if (order_status == '0' || order_status == '10'||order_status == '2'||order_status == '4' ||order_status == '22'||order_status == '44' ) {
			$("#statusbtn").hide();			
		} if(order_status == '1' || order_status == '11'){
			$("#statusbtn").show();
			$("#statusbtn").attr("disabled", false);
		} 
		if(order_status == '0'||order_status == '1'||order_status == '2'||order_status == '4' ){
		   var url = "ajax_show_orders_sp.php";
		}
		if(order_status == '10'||order_status == '11'||order_status == '22'||order_status == '44' ){
		   var url = "ajax_show_orders_cust.php";
		}
		var data = $('#frmsearch').serialize();

		jQuery.ajax({
			url: url,
			method: 'POST',
			data: data,
			async: false
		}).done(function (response) {
			//console.log(response);
			$('#order_list').html(response);
			var table = $('#sample_2').dataTable();
			table.fnFilter('');
			$("#select_th").removeAttr("class");
		}).fail(function () { });
		return false;
	}
	
	function showInvoice(order_status, id) {
		var url = "invoice2.php";
		jQuery.ajax({
			url: url,
			method: 'POST',
			data: 'order_status=' + order_status + '&order_id=' + id,
			async: false
		}).done(function (response) {
			$('#view_invoice_content').html(response);
			$('#view_invoice').modal('show');
		}).fail(function () { });
		return false;
	}
	function showInvoice_cust(order_status, id) {
		//alert(id);
		 var url = "invoice2_cust.php";
		jQuery.ajax({
			url: url,
			method: 'POST',
			data: 'order_status=' + order_status + '&order_id=' + id,
			async: false
		}).done(function (response) {
			$('#view_invoice_content').html(response);
			$('#view_invoice').modal('show');
		}).fail(function () { });
		return false; 
	}
   
	function showOrderDetails(order_status,oid) {
		var url = "order_details_popup.php";
		jQuery.ajax({
			url: url,
			method: 'POST',
			data: 'order_id=' + oid + '&order_status=' + order_status,
			async: false
		}).done(function (response) {
			$('#order_details_content').html(response);
			$('#order_details').modal('show');
		}).fail(function () { });
		return false;
	}
	function showOrderDetails_cust(order_status,oid) {
		var url = "order_details_popup_cust.php";
		jQuery.ajax({
			url: url,
			method: 'POST',
			data: 'order_id=' + oid + '&order_status=' + order_status,
			async: false
		}).done(function (response) {
			$('#order_details_content').html(response);
			$('#order_details').modal('show');
		}).fail(function () { });
		return false;
	}
	
	function OrderDetailsPrint() {
		var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
		var divContents = '<style>\body {\
				font-size: 12px;}\
		th {text-align: left;}\
		.printHeading { line-height: 18px;  padding: 10px 0;  font-size: 18px; }\
		table { border-collapse: collapse;  \
				font-size: 12px; }\
		table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
		body { font-family: "Open Sans", sans-serif;\
		background-color:#fff;\
		font-size: 15px;\
		direction: ltr;}</style>' + $("#divOrderPrintArea").html();
		if (isIE == true) {
			var printWindow = window.open('', '', 'height=400,width=800');
			printWindow.document.write(divContents);
			printWindow.focus();
			printWindow.document.execCommand("print", false, null);
		} else {
			$('<iframe>', {
				name: 'myiframe',
				class: 'printFrame'
			}).appendTo('body').contents().find('body').html(divContents);
			window.frames['myiframe'].focus();
			window.frames['myiframe'].print();
			setTimeout(
					function ()
					{
						$(".printFrame").remove();
					}, 1000);
		}
	}
	function takeprint11() {
		var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
		var divContents = '<style>\body {\
				font-size: 12px;}\
		th {text-align: left;}\
		.printHeading { line-height: 18px;  padding: 10px 0;  font-size: 18px; }\
		table { border-collapse: collapse;  \
				font-size: 12px; }\
		table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
		body { font-family: "Open Sans", sans-serif;\
		background-color:#fff;\
		font-size: 15px;\
		direction: ltr;}</style>' + $("#divPrintArea").html();
		if (isIE == true) {
			var printWindow = window.open('', '', 'height=400,width=800');
			printWindow.document.write(divContents);
			printWindow.focus();
			printWindow.document.execCommand("print", false, null);
		} else {
			$('<iframe>', {
				name: 'myiframe',
				class: 'printFrame'
			}).appendTo('body').contents().find('body').html(divContents);
			window.frames['myiframe'].focus();
			window.frames['myiframe'].print();
			setTimeout(
					function ()
					{
						$(".printFrame").remove();
					}, 1000);
		}
	}
	function takeprint_invoice() {
		var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
		var divContents = '<style>\
		.darkgreen{	background-color:#364622; color:#fff!important; font-size:24px; font-weight:600;}\
		.fentgreen1{background-color:#b0b29c;color:#4a5036;	font-size:12px;}\
		.fentgreen{	background-color:#b0b29c;	color:#4a5036;}\
		.font-big{	font-size:20px;	font-weight:600;	color:#364622;}\
		.font-big1{	font-size:18px;	font-weight:600;	color:#364622;}\
		.table-bordered-popup {    border: 1px solid #364622;}\
		.table-bordered-popup > tbody > tr > td, .table-bordered-popup > tbody > tr > th, .table-bordered-popup > thead > tr > td, .table-bordered-popup > thead > tr > th {\
				border: 1px solid #364622;	color:#4a5036;}\
		.blue{	color:#010057;}\
		.blue1{	color:#574960;	font-size:16px;}\
		.buyer_section{	color:#574960;	font-size:14px;}\
		.pad-5{	padding-left:10px;}\
		.pad-40{	padding-left:40px;}\
		.np{	padding-left:0px;	padding-right:0px;}\
		.bg{	background-image:url(../../assets/global/img/invoice/<?php echo COMPANYNM;?>_logo-watermark.jpg); background-repeat:no-repeat;\
		 background-size: 200px 200px;}\
		</style>' + $("#divPrintArea").html();
		
		if (isIE == true) {
			var printWindow = window.open('', '', 'height=400,width=800');
			printWindow.document.write(divContents);
			printWindow.focus();
			printWindow.document.execCommand("print", false, null);
		} else {
			$('<iframe>', {
				name: 'myiframe',
				class: 'printFrame'
			}).appendTo('body').contents().find('body').html(divContents);
			window.frames['myiframe'].focus();
			window.frames['myiframe'].print();
			setTimeout(
					function ()
					{
						$(".printFrame").remove();
					}, 1000);
		}
	}
	var lat = 0;
	var lng = 0;

	$('#googleMapPopup').on('shown.bs.modal', function (e) {

		var latlng = new google.maps.LatLng(lat, lng);
		var map = new google.maps.Map(document.getElementById('map'), {
			center: latlng,
			zoom: 17
		});
		var marker = new google.maps.Marker({
			map: map,
			position: latlng,
			draggable: false,
			//anchorPoint: new google.maps.Point(0, -29)
		});
	});
	function showGoogleMap(getlat, getlng) {
		lat = getlat;
		lng = getlng;
		$('#googleMapPopup').modal('show');
	}
	
	$("#statusbtn").click(function () {
		var void1 = [];
		$('input[name="select_all[]"]:checked').each(function () {
			void1.push(this.value);
		});
		var radioValue = $("input[name='deliveryType']:checked"). val();
		var tabid=$('#tabpanelids').val();
		var energy = void1.join();
		 energy = energy.replace('on,','');
		if (energy != '') {
			$("#assign_delivery").modal('show');
			$("#orderids").val(energy);
			var url = "ajax_get_stock.php";
			jQuery.ajax({
				url: url,
				method: 'POST',
				data: 'order_ids=' + energy+'&tabid='+ tabid,
				async: false
			}).done(function (response) {
				console.log(response);
				$('#pro_table_div').html(response);
				if($('#flag_avail_stock').val()=='allred'){
					$('#stock_avail_table').append('<tr><td colspan="4" style="color:red;">You don\'t have sufficient stock to assign. Please add stock and assign it again.</td></tr>');
					if(radioValue=='assign')$("button[id^='btn_assigndelivery']" ).hide();
					if(radioValue=='delivered')$("button[id^='btn_delivered']" ).hide();
					$("#add_stock_div").show();
				}else{
					if(radioValue=='assign')$("button[id^='btn_assigndelivery']" ).show();
					if(radioValue=='delivered')$("button[id^='btn_delivered']" ).show();
				}
			}).fail(function () { });
			return false;
		} else {
			alert("Please select minimum one order");
		}
	});
   
	function assign_delivery_clicked() {			
		var sp_or_cust=$('#tabpanelids').val();
		var selectdp = $('#selectdp').val();			
		var orderids = $("#orderids").val();
        var proid_varid = $("#proid_varid").val();
        var mnf_date = $("#mnf_date").val();  
        var void1 = [];
        $('input[name="proid_varid[]"]').each(function () {
            void1.push(this.value);
        });       
        var proid_varid1 = void1.join();
        var void12 = [];
        $('input[name="mnf_date[]"]').each(function () {
            void12.push(this.value);
        });       
        var mnf_date12 = void12.join();
       // alert(mnf_date12);
       // alert(proid_varid1);
		//alert(orderids); 
       // debugger;
		 var del_assigned_date = $("#del_assigned_date").val();			   
		if (del_assigned_date != ''&& selectdp!='') {
			if (confirm('Do you want to submit?')) {
				var url = 'updateOrderDelivery.php?flag=load&flag_user='+sp_or_cust+'&selectdp=' + selectdp + '&del_assigned_date=' + del_assigned_date + '&orderids=' + orderids + '&proid_varid1=' + proid_varid1 + '&mnf_date12=' + mnf_date12;
              // debugger;
                //+'&supp_chnz_status='+supp_chnz_status
				//	alert(url);
				$.ajax({
					url: url,
					datatype: "JSON",
					contentType: "application/json",
					error: function (data) {
						console.log("error:" + data)
					},
					success: function (data) {
						console.log(data);
						 if (data > 0) {
							alert("The order is assigned to Delivery Person");
						   // location.reload();
							window.location.reload();
						} else {
							alert("Not updated.");
						} 
					}
				});
			}else {
				return false;
			}
		} else {
			if (del_assigned_date == ''){
				  alert("Please select date!");               
			} if(selectdp==''){
				 alert("Please select delivery person!");       
			}
			 //e.preventDefault();
			return false;				  
		} 
	}

	//$("#btn_delivered").click(function () {
	function btn_delivered_clicked() {
		var sp_or_cust=$('#tabpanelids').val();
		var orderids = $("#orderids").val();
		//var del_assigned_date = $("#del_assigned_date1").val();
         var proid_varid = $("#proid_varid").val();
        var mnf_date = $("#mnf_date").val();  
        var void1 = [];
        $('input[name="proid_varid[]"]').each(function () {
            void1.push(this.value);
        });       
        var proid_varid1 = void1.join();
        var void12 = [];
        $('input[name="mnf_date[]"]').each(function () {
            void12.push(this.value);
        });       
        var mnf_date12 = void12.join()
		if (confirm('Do you want to submit?')) {
			var url = 'updateOrderDelivery.php?flag=delivered&flag_user='+sp_or_cust+'&del_assigned_date="test"&orderids=' + orderids + '&proid_varid1=' + proid_varid1 + '&mnf_date12=' + mnf_date12;
			//alert(url);
			$.ajax({
				url: url,
				datatype: "JSON",
				contentType: "application/json",

				error: function (data) {
					console.log("error:" + data)
				},
				success: function (data) {
					console.log(data);
					if (data > 0) {
						alert("Orders marked as delivered");
						location.reload();
					} else {
						alert("Not updated");
					}
				}
			});
		} else {
			//alert("Please select date");
			return false;
		}
	}
	//});
	
   
	function fnShowSalesperson(id) {
		var url = "getSalesPersonDropDown.php?cat_id=" + id.value;
		CallAJAX(url, "divsalespersonDropdown");
	}
	function deleteOrder(order_id) {
		if (confirm('Are you sure you want to delete this order?')) {
			CallAJAX1('ajax_product_manage.php?action=delete_order&order_id=' + order_id );
		}
	}
	function deleteOrderCust(cust_order_id) {
		alert("dfsdf");
		if (confirm('Are you sure you want to delete this order?')) {
			CallAJAX1('ajax_product_manage.php?action=delete_order_cust&cust_order_id=' + cust_order_id );
		}
	}
	function CallAJAX1(url) {
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				alert('Order deleted successfully.');
				location.reload();
			}
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}
</script>
<script>
$(document).ready(function () {
$('#stock_avail_table').DataTable({
"scrollX": true,
"scrollY": 200,
});
$('.dataTables_length').addClass('bs-select');
});	
</script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
</html>