<?php 
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include "../includes/header.php"; 
include "../includes/testManage.php";
include "../includes/userConfigManage.php";
$testObj    =   new testManager($con,$conmain);
$userconfObj    =   new userConfigManager($con,$conmain);
if(isset($_POST['hidbtnsubmit']))
{   
    //print_r($_POST);
    //exit();
    $user_type=$_POST['user_type'];  
    $common_user_id = $userconfObj->addCommonUserDetails($user_type);
    if(isset($common_user_id) && $common_user_id!='')
    {
        $userid_local = $userconfObj->addAllLocalUserDetails($user_type,$common_user_id);
    }
    else
    {
        echo '<script>alert("Something Wrong..!!");</script>';   
    }
    $email      =   fnEncodeString($_POST['email']);
    if($email != '') {
        $userconfObj->sendUserCreationEmail();
    }    
    echo '<script>alert("User added successfully.");location.href="user_list_config.php";</script>';
}
$margin_percentage = $userconfObj->getAllLocalMarginDetailsByUsertype();
//echo "<pre>";print_r($margin_percentage);die();

?>
<!-- END HEADER -->

<link rel="stylesheet" href="../../assets/multipal-select/build.min.css">
<script src="../../assets/multipal-select/build.min.js"></script>
<link rel="stylesheet" href="../../assets/multipal-select/fastselect.min.css">
<script src="../../assets/multipal-select/fastselect.standalone.js"></script>


<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<?php
$activeMainMenu = "ManageSupplyChain"; $activeMenu = "User List";
include "../includes/sidebar.php";

    $commonObj     =   new commonManage($con,$conmain);
    $row_url=$commonObj->getPageIDforUrlAdd($php_page_name);
    $page_id_url = $row_url['page_id'];
    $row_url_add=$commonObj->getURLforAdd($profile_id,$page_id_url);
    $ischecked_add_url = $row_url_add['ischecked_add'];
    if ($ischecked_add_url == 0 && $ischecked_add_url!='') 
    {
        session_set_cookie_params(0);
        session_start();
        session_destroy();
        echo '<script>location.href="../login.php";</script>';
        exit;
    }
?>
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">   
        <h3 class="page-title">User Add</h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">                 
                <li>
                    <i class="fa fa-home"></i>
                    <a href="user_list_config.php">User List</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Add New User</a>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet box blue-steel">
                <div class="portlet-title"><div class="caption">Add New User</div></div>
                <div class="portlet-body">
                    <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
                    <form name="addform" id="addform" class="form-horizontal" role="form" data-parsley-validate=""  method="post" action="">          

                        
                        <div class="form-group">
                            <label class="col-md-3">Select User Type:<span class="mandatory">*</span></label>
                            <div class="col-md-4">
                            <select name="user_type" id="user_type" 
                            class="form-control" onchange="getUserID(this)" required>
                            <option value="" selected>-- Select --</option>
                            <?php   
                            $seesion_user_id=$_SESSION[SESSION_PREFIX.'user_id'];
                             $result1 = $testObj->getTest();
                             while ($row = mysqli_fetch_array($result1)){ ?>
                              <option value="<?php echo $row['id'];?>_<?php echo $row['depth'];?>" ><?php echo $row['user_type'];?></option>
                             <?php  } ?> 
                            </select>
                            </div>
                        </div>                      
                        <div id="parent_hierarchy_div"></div>
                        
                        <div class="form-group">
              <label class="col-md-3">Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text"
                placeholder="Enter Name"
                data-parsley-trigger="change"               
                data-parsley-required="#true" 
                data-parsley-required-message="Please enter name"
                data-parsley-maxlength="50"
                data-parsley-maxlength-message="Only 50 characters are allowed"             
                name="firstname" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3">Username:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" id="username"
                placeholder="Enter Username"
                data-parsley-minlength="6"
                data-parsley-minlength-message="Username should be minimum 6 characters without blank spaces"          
                data-parsley-maxlength="25"
                data-parsley-maxlength-message="Only 25 characters are allowed"          
                data-parsley-type-message="Please enter Username, blank spaces are not allowed"        
                data-parsley-required-message="Please enter Username, blank spaces are not allowed"
                data-parsley-trigger="change"
                data-parsley-required="true"              
				data-parsley-pattern="/^[A-Za-z0-9]+(?:[_-][A-Za-z0-9]+)*$/"
                data-parsley-pattern-message="Username should be 6-25 alphanumeric characters & underscore, hyphen are allowed in between."
                name="username" class="form-control"><span id="user-availability-status"></span>
              </div>
            </div> <!-- data-parsley-pattern="/^[']*$|^[\x22]*$|^[\S]*$/" -->
            <div class="form-group">
              <label class="col-md-3">Password:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="password" id="password"
                placeholder="Enter Password"               
                data-parsley-minlength="6"
                data-parsley-minlength-message="Password should be minimum 6 characters without blank spaces"   
                data-parsley-maxlength="15"
                data-parsley-maxlength-message="Only 15 characters are allowed"   
                data-parsley-type-message="Password should be minimum 6 characters without blank spaces"           
                data-parsley-required-message="Please enter Password"
                data-parsley-trigger="change"
                data-parsley-required="true"
                data-parsley-pattern="/^\S*$/" 
                data-parsley-error-message="Password should be 6-15 characters without blank spaces"
                name="password" class="form-control placeholder-no-fix">
              </div>
            </div>
           <div class="form-group">
              <label class="col-md-3">Confirm Password:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="password" id="c_password"
                placeholder="Enter Confirm Password"
                data-parsley-trigger="change"   
                data-parsley-equalto="#password"
                data-parsley-equalto-message="Password does not match with confirm password"                
                data-parsley-required="#true" 
                data-parsley-required-message="Please enter confirm password"       
                name="c_password" class="form-control"><span id="user-availability-status"></span>
              </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Address:<span class="mandatory">*</span></label>
                <div class="col-md-4">
                    <textarea name="address"
                    placeholder="Enter Address"
                    data-parsley-trigger="change"
                    data-parsley-required="#true" 
                    data-parsley-maxlength="200"
                    data-parsley-maxlength-message="Only 200 characters are allowed"                            
                    rows="4" class="form-control" ></textarea>
                </div>
            </div>
           
            <div id="working_area_top">
            <h4>User Details </h4>
            </div>
            <div class="form-group" id="hidden_div_shops" style="display: none;">
                <label class="col-md-3">Shops:</label>
                <div class="col-md-4" id="div_select_shops"> 
                         <select name="assignshopids[]"  id="assignshopids"
                            class="form-control" multiple >
                            <option  disabled>-Select-</option>
                            <?php
                            $sql="SELECT id,name FROM tbl_shops where shop_owner_id=0 and isdeleted!='1' order by name";
                            $result = mysqli_query($con,$sql);
                            while($row = mysqli_fetch_array($result))
                            {
                                $cat_id=$row['id'];                             
                                echo "<option value='$cat_id' >" . $row['name'] . "</option>";
                            } ?>
                        </select>
                         <!--
<select name="assignshopids[]"  id="assignshopids" class="multipleSelect" multiple data-parsley-required="#true">
<?php
$sql="SELECT id,name FROM tbl_shops where shop_owner_id=0 and isdeleted!='1' order by name";
$result = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result))
{
$cat_id=$row['id'];                             
echo "<option value='$cat_id' >" . $row['name'] . "</option>";
} ?>
</select>

<script>
$('.multipleSelect').fastselect();
</script>-->

                      

                </div>
            </div>


            
            <div class="form-group">
                <label class="col-md-3">State:</label>
                <div class="col-md-4">
                <select name="state"  id="state"
                class="form-control" onChange="fnShowCity(this.value)">
                <option selected disabled>-Select-</option>
                <?php
                $sql="SELECT id,name FROM tbl_state where country_id=101 order by name";
                $result = mysqli_query($con,$sql);
                while($row = mysqli_fetch_array($result))
                {
                    $cat_id=$row['id'];
                    $selected_state = '';
                    if($default_state == $row['id']) {
                        $selected_state = 'selected';
                    }
                    echo "<option value='$cat_id' $selected_state>" . $row['name'] . "</option>";
                } ?>
                </select>
                </div>
            </div>          
            <div class="form-group" id="city_div" >
              <label class="col-md-3">District:</label>
              <div class="col-md-4" id="div_select_city">             
              <select name="city" id="city"                 
                class="form-control">
                <option selected value="">-Select-</option>                                     
                </select>                
              </div>
            </div>

            <div class="form-group" id="area_div">
              <label class="col-md-3">Taluka:</label>
              <div class="col-md-4" id="div_select_area">              
              <select name="area" id="area" data-parsley-trigger="change" class="form-control">
                <option selected value="">-Select-</option>                                 
                </select>
             
              </div>
            </div>                     
            <div class="form-group" id="subarea_div" style="display:none;">
              <label class="col-md-3">Subarea:</label>
              <div class="col-md-4" id="div_select_subarea">
              <select name="subarea" id="subarea" data-parsley-trigger="change" class="form-control">
                <option selected value="">-Select-</option>                                 
                </select>
              </div>
            </div>
            <div id="working_area_bottom">              
            </div>
          
             <div class="form-group">
              <label class="col-md-3">Email:</label>

              <div class="col-md-4">
                <input type="text" id="email"
                placeholder="Enter E-mail"
                data-parsley-maxlength="100"
                data-parsley-maxlength-message="Only 100 characters are allowed"
                data-parsley-type="email"
                data-parsley-type-message="Please enter valid e-mail" 
                data-parsley-trigger="change"
                name="email" class="form-control"><span id="user-availability-status"></span>
              </div>
            </div>
            <div class="form-group">
                  <label class="col-md-3">Mobile Number:</label>

                  <div class="col-md-4">
                    <input type="text"  name="mobile"
                    placeholder="Enter Mobile Number"
                    data-parsley-trigger="change"                   
                    data-parsley-minlength="10"
                    data-parsley-maxlength="15"
                    data-parsley-maxlength-message="Only 15 characters are allowed"
                    data-parsley-pattern="^(?!\s)[0-9]*$"
                    data-parsley-pattern-message="Please enter numbers only"
                    class="form-control" value="">
                  </div>
                </div>
               
            <div class="form-group">
              <label class="col-md-3">GST Number:<!-- <span class="mandatory">*</span> --></label>
              <div class="col-md-4">
                <input type="text"
                placeholder="Enter GST Number"
                data-parsley-trigger="change"  
                data-parsley-required-message="Please enter GST Number"
                data-parsley-maxlength="15"
                data-parsley-maxlength-message="Only 15 characters are allowed" 
                data-parsley-minlength="15"
                data-parsley-minlength-message="Not Valid GST Number"   
                name="gstnumber" class="form-control">
              </div>
            </div>
             
                <input type="hidden" name="page_to_add" id="page_to_add" value="<?=$page_to_add;?>">

                        
                       
                        <div class="form-group">
                          <label class="col-md-3">Office Phone No.:</label>
                          <div class="col-md-4">
                            <input type="text"
                            placeholder="Office Phone No."
                            data-parsley-trigger="change"                   
                            data-parsley-minlength="10"
                            data-parsley-maxlength="15"
                            data-parsley-maxlength-message="Only 15 characters are allowed"
                            data-parsley-pattern="^(?!\s)[0-9]*$"
                            data-parsley-pattern-message="Please enter numbers only"
                            name="phone_no" 
                            class="form-control">
                          </div>
                        </div>   

                        <div class="form-group">
                        <label class="col-md-3">Latitude:</label>
                        <div class="col-md-4">
                        <input type="text" name="latitude"
                        placeholder="Enter Latitude"
                        class="form-control"  min="-180" max="180" step="0.0000000000000001" 
                            data-parsley-trigger="change"                                       
                             >
                        </div>
                        </div>        
                        <div class="form-group">
                        <label class="col-md-3">Longitude:</label>
                        <div class="col-md-4">
                        <input type="text" name="longitude"
                        placeholder="Enter Longitude"
                        class="form-control"  min="-180" max="180" step="0.0000000000000001" 
                            data-parsley-trigger="change" >
                        </div>
                        </div> 
                        <div class="form-group" id="marginTypediv" style="display: none;">
                            <label class="col-md-3">Margin Type:</label>
                            <div class="col-md-4">
                                <input type="radio" name="marginType" id="marginType_rolewise" value="0" checked onclick="fnChangeMarginType(this);"> Role Wise 
                                &nbsp;&nbsp;
                                <input type="radio" name="marginType" id="marginType_usertypewise" value="1" onclick="fnChangeMarginType(this);"> User Type Wise 
                                &nbsp;&nbsp;
                                <input type="radio" name="marginType" id="marginType_userid" value="2" onclick="fnChangeMarginType(this);"> User ID Wise
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group" id="userid_margindiv" style="display: none;">
                          <label class="col-md-3">Margin(%):</label>
                          <div class="col-md-4">
                            <input type="text"   id="userid_margin" name="userid_margin" class="form-control" onkeyup="myFunction(this)"
                            placeholder="Enter Margin"
                            min="0" max="100" step="0.01" 
                            data-parsley-trigger="change"                                       
                            data-parsley-pattern="^[0-9]+?[\.0-9]*$" disabled >
                          </div>
                        </div>
						<div class="form-group" id="userrole_margindiv" style="display: none;">
                          <label class="col-md-3">Margin(%):</label>
                          <div class="col-md-4">
                            <input type="text"   id="userrole_margin" name="userrole_margin" class="form-control" onkeyup="myFunction(this)"
                            placeholder="Enter User Role Margin" 
                            min="0" max="100" step="0.01" 
                            data-parsley-trigger="change"                                       
                            data-parsley-pattern="^[0-9]+?[\.0-9]*$" disabled >
                          </div>
                        </div>
						<div class="form-group" id="usertype_margindiv" style="display: none;">
                          <label class="col-md-3">Margin(%):</label>
                          <div class="col-md-4">
                            <input type="text"   id="usertype_margin" name="usertype_margin" class="form-control" onkeyup="myFunction(this)"
                            placeholder="Enter User Type Margin"
                            min="0" max="100" step="0.01" 
                            data-parsley-trigger="change"                                       
                            data-parsley-pattern="^[0-9]+?[\.0-9]*$" disabled >
                          </div>
                        </div>
                        
                         <div class="form-group">
                            <label class="col-md-3"><b>Bank Details</b></label>
                        </div>                      
                         <div class="form-group">
                            <label class="col-md-3">Account Name </label>
                            <div class="col-md-4">
                                <input type="text" 
                                placeholder="Account Name"
                                data-parsley-trigger="change" 
                                data-parsley-required-message="Please enter accouont number"
                                data-parsley-maxlength="50"
                                data-parsley-maxlength-message="Only 50 characters are allowed"
                                name="accname" 
                                
                                class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-3">Account Number:</label>
                          <div class="col-md-4">
                            <input type="text"
                            placeholder="Enter Account Number"
                            data-parsley-trigger="change"
                            data-parsley-required-message="Please enter accouont number"
                            data-parsley-maxlength="30"
                            data-parsley-maxlength-message="Only 30 characters are allowed"                     
                            name="accno" class="form-control" >
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-3">Bank Name:</label>
                          <div class="col-md-4">
                            <input type="text"
                            placeholder="Enter Bank Name"
                            data-parsley-trigger="change"  
                            data-parsley-required-message="Please enter Bank Name"
                            data-parsley-maxlength="30"
                            data-parsley-maxlength-message="Only 30 characters are allowed"                         
                            name="bank_name" class="form-control" >
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-3">Bank Branch Name:</label>
                          <div class="col-md-4">
                            <input type="text"
                            placeholder="Enter Branch Name"
                            data-parsley-trigger="change"  
                            data-parsley-required-message="Please enter branch Name"
                            data-parsley-maxlength="30"
                            data-parsley-maxlength-message="Only 30 characters are allowed"                         
                            name="accbrnm" class="form-control" >
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-3">IFSC Code:</label>
                          <div class="col-md-4">
                            <input type="text"
                            placeholder="Enter IFSC Code"
                            data-parsley-trigger="change" 
                            data-parsley-required-message="Please enter ifsc Code"
                            data-parsley-maxlength="30"
                            data-parsley-maxlength-message="Only 30 characters are allowed"                         
                            name="accifsc" class="form-control">
                          </div>
                        </div>
                        
                        <!-- <div class="form-group">
                          <label class="col-md-3">Status:</label>
                          <div class="col-md-4">
                            
                                <select name="user_status" id="user_status" class="form-control">
                                    <option value="Active" selected>Active</option>
                                    <option value="Inactive" >Inactive</option>
                                </select>
                            
                          </div>
                        </div> -->
                        
                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-3">
                            <input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
                            <input type="hidden" name="hidAction" id="hidAction" value="add-user-config.php">
                             <button name="getlatlong" id="getlatlong" type="button"  class="btn btn-primary">Get Lat Long</button>
                            <button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button>
                            <a href="user_list_config.php" class="btn btn-primary">Cancel</a>
                            </div>
                        </div>
                    </form>  
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
</div>
<?php include "../includes/footer.php";?>
<script type="text/javascript">
var margin_percentage=<?php echo json_encode($margin_percentage); ?>;
 function myFunction(varl) {
    if (varl.value != "") {
        var arrtmp = varl.value.split(".");
        if (arrtmp.length > 1) {
            var strTmp = arrtmp[1];
            if (strTmp.length > 2) {
                varl.value = varl.value.substring(0, varl.value.length - 1);
            }
        }
    }
}
function fnChangeMarginType(margintype) {		
	$("#userid_margin").val('0.00');
	var user_typej = $("#user_type option:selected").text();	
    if(margintype.value==0){		
        $("#userrole_margindiv").show();
		$("#usertype_margindiv").hide();
		$("#userid_margindiv").hide();       
		$("#userrole_margin").prop('disabled', true);
		$("#userrole_margin").val(margin_percentage['user_role'][user_typej]);
    }
	if(margintype.value==1){		
        $("#usertype_margindiv").show();
		$("#userrole_margindiv").hide();
		$("#userid_margindiv").hide();		
		$("#usertype_margin").prop('disabled', true);
		$("#usertype_margin").val(margin_percentage['user_type'][user_typej]);		
    }
	if(margintype.value==2){		
        $("#userid_margindiv").show();
		$("#usertype_margindiv").hide();
		$("#userrole_margindiv").hide();
        $("#userid_margin").prop('disabled', false);		
    }	
}

function CallAJAX(url,assignDivName) {
    if (window.XMLHttpRequest)
    {
        xmlhttp=new XMLHttpRequest();
    } else {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("" + assignDivName +"").innerHTML   =   xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET",url,true);
    xmlhttp.send(); 
}

function getUserID(UserID) {    
    var UserID =UserID.value;
    var user_typej = $("#user_type option:selected").text(); 	
    var usertype_info = UserID.split('_');  
    var depth=usertype_info[1]; 
    var usertype_id=usertype_info[0];   
    
    var url = "conf_get_childs.php"; 
    jQuery.ajax({
        url: url,
        method: 'POST',
        data: 'usertype_id='+usertype_id+'&getflag=onchange_getrole',
        async: false
    }).done(function (response) {
        //console.log(response);
        if(response!='no_userrole' && response=='Shopkeeper'){
            $('#hidden_div_shops').show();
        }else{
            $('#hidden_div_shops').hide();
        }
        if(response!='no_userrole' && (response=='Distributor' ||response=='Superstockist')){
            $('#marginTypediv').show(); 
			$("#userrole_margindiv").show();
			$("#usertype_margindiv").hide();
			$("#userid_margindiv").hide();
			$("#marginType_rolewise").prop("checked", true);
			$("#marginType_usertypewise").prop("checked", false);
			$("#userrole_margin").prop('disabled', true);
			$("#userrole_margin").val(margin_percentage['user_role'][user_typej]);
        }else{
            $('#marginTypediv').hide();         
            $('#userid_margindiv').hide();  
			$('#usertype_margindiv').hide();  
			$('#userrole_margindiv').hide();  
        }
        child_options=response;             
    }).fail(function () { });
    
    
    if (depth > 2){
        $('#parent_hierarchy_div').empty();
        //console.log("daksjdkj:"+depth);
        var url = "conf_get_childs.php"; 
            jQuery.ajax({
                url: url,
                method: 'POST',
                data: 'id='+usertype_id+'&getflag=getparent',
                async: false
            }).done(function (response) {
                 obj = JSON.parse(response);
                console.log(obj);
                for(var i=obj.parent_info.length;i>0;i--){
                    if (typeof obj.parent_info[i] != 'undefined'){
                        //console.log(obj.parent_info[i].user_type);
                        var user_type_parent=obj.parent_info[i].user_type;
                        var user_depth='user_depth_'+obj.parent_info[i].parent_id;
                        var child_options;
                        var url = "conf_get_childs.php"; 
                        jQuery.ajax({
                            url: url,
                            method: 'POST',
                            data: 'depth='+i+'&user_typej='+user_type_parent+'&getflag=getoption',
                            async: false
                        }).done(function (response) {
                            console.log(response);
                            child_options=response;             
                        }).fail(function () { });
                        var functioncallvalue="";
                        if(i!=1){
                            var temp_fornextid=i-1;
							var stringusertype=obj.parent_info[temp_fornextid].user_type;
							functioncallvalue='onchange="getUserID1(this,'+obj.parent_info[temp_fornextid].parent_id+','+i+',\''+stringusertype+'\')"';
                            //functioncallvalue="onchange='getUserID1(this,"+obj.parent_info[temp_fornextid].parent_id+","+i+","+obj.parent_info[temp_fornextid].user_type+")'";
                        }
                                                
                        if(child_options!='no_parent'){							
                            $("#parent_hierarchy_div").append("<div class='form-group'>"+
                            "<label class='col-md-3'>Select Parent "+user_type_parent+":<span class='mandatory'>*</span></label>"+
                            "<div class='col-md-4'>"+
                            "<select name='"+user_depth+"[]' id='parent_user_"+obj.parent_info[i].parent_id+"' class='form-control' "+
                            " "+ functioncallvalue +">"+     
                            " "+child_options+
                            "</select></div>"+
                            "</div> ");							
							var selectedvalue = $("select#parent_user_"+obj.parent_info[i].parent_id+" option:selected").val();
							if(selectedvalue!=''){								
								$("select#parent_user_"+obj.parent_info[i].parent_id+"").attr('disabled',true);
								$("#parent_hierarchy_div").append(" <input type='hidden' name='"+user_depth+"[]' value='"+selectedvalue+"' /> ");								
							}else{
								$("select#parent_user_"+obj.parent_info[i].parent_id+"").attr('required', true);								
							}
							
                        }else{
                            alert("You can not add this user type as there is no parent exist!");
                            $("#user_type").val("");
							break;							
                        }
                    } 
                }               
                //child_options=response.;              
            }).fail(function () { });       
    }else{
        $('#parent_hierarchy_div').empty();
    }
}

function getUserID1(UserID1,nextid,i,puser_type) {
	
    var UserID1 =UserID1.value;
    var usertype_info = UserID1.split('_'); 
    var depth=usertype_info[1]; 
    var user_id=usertype_info[0];
    //console.log($('#parent_user_'+nextid+'').id());
    
    var child_options;
    var url = "conf_get_childs.php"; 
    jQuery.ajax({
        url: url,
        method: 'POST',
        data: 'user_id='+user_id+'&puser_type='+puser_type+'&getflag=onchange_getchild',
        async: false
    }).done(function (response) {
        //console.log(response);
        child_options=response;             
    }).fail(function () { });
    var functioncallvalue="";
    if(i!=1){
        var temp_fornextid=i-1;
		var stringusertype=obj.parent_info[temp_fornextid].user_type;
		//'onchange="ravi(\''+rvalue+'\');">'
         functioncallvalue='onchange="getUserID1(this,'+obj.parent_info[temp_fornextid].parent_id+','+i+',\''+stringusertype+'\')"';
    }                   
    if(child_options!='no_parent'){
        $('#parent_user_'+nextid+'')
            .find('option')
            .remove()
            .end()
            .append("<option value='' selected>-- Select --</option>"+child_options+"");            
    }else{
        alert("You can not add this user type as there is no parent exist!");
		previd=nextid-1;
		$('#parent_user_'+nextid+'')
            .find('option')
            .remove()
            .end()
            .append("<option value='' selected>-- Select --</option>");
		$('#parent_user_'+previd+'').val("");
		//$("#user_type").val("");
    }   
}

function calculate_data_count(element_value){
    element_value = element_value.toString();
    var element_arr = element_value.split(','); 
    return element_arr.length;
}
function setSelectNoValue(div,select_element){
    var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled value="">-Select-</option></select>';
    document.getElementById(div).innerHTML  =   select_selement_section;
}
function fnShowCity(id_value) { 
    $("#city_div").show();  
    $("#area").html('<option value="">-Select-</option>');  
    $("#subarea").html('<option value="">-Select-</option>');   
    var page_to_add = $("#page_to_add").val();
    var param = '';
    if(page_to_add == 'superstockist')
        param = "&nofunction=nofunction";
    var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city"+param;
    CallAJAX(url,"div_select_city");    
}
function FnGetSuburbDropDown(id) {
    $("#area_div").show();  
    $("#subarea").html('<option value="">-Select-</option>');   
    var page_to_add = $("#page_to_add").val();
    var param = '';
    if(page_to_add == 'stockist')
        param = "&nofunction=nofunction";
    var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&multiple=multiple&function_name=FnGetSubareaDropDown"+param;
    CallAJAX(url,"div_select_area");
}
function FnGetSubareaDropDown(id) { 
    var suburb_str = $("#area").val();  
    var suburb_arr_count = calculate_data_count(suburb_str);
    if(suburb_arr_count == 1){//If single city selected then only show its related subarea  
        $("#subarea_div").show();   
        var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea&multiple=multiple";
        CallAJAX(url,"div_select_subarea");
    }else if(suburb_arr_count > 1){
        $("#subarea_div").show();   
        var multiple_id = suburb_str.join(", ");
        var url = "getSubareaDropdown.php?multiple_id="+multiple_id+"&select_name_id=subarea&multiple=multiple";
        CallAJAX(url,"div_select_subarea");
    }else{
        setSelectNoValue("div_select_subarea", "subarea");
    }       
}

function checkAvailability() {
    $('#addform').parsley().validate();
    var email = $("#email").val();
    var username = $("#username").val();
    var validate_user = 1;
    if(username != '')
    {
        validate_user = 1;
        jQuery.ajax({
            url: "../includes/checkUserAvailable.php",
            data:'validation_field=username&username='+username,
            type: "POST",
            async:false,
            success:function(data){     
                if(data=="exist") {
                    alert('Username already exists.');
                    validate_user = 1;
                } else {
                    validate_user = 0;                                       
                }
            },
            error:function (){}
        });
    }
    
    if(email != '')
    {
        validate = 1;
        jQuery.ajax({
            url: "../includes/checkUserAvailable.php",
            data:'validation_field=email&email='+email,
            type: "POST",
            async:false,
            success:function(data){         
                if(data=="exist") {
                    alert('E-mail already exists.');
                    validate = 1;
                    return false;
                } else {
                    validate = 0;
                }
            },
            error:function (){}
        });
    } 
    else if(username != '' && validate_user == 0)
    {
        validate = 0;
    }
    else 
    {
        validate = 1;
    }
    
    if(validate == 0 && validate_user == 0)
    {
        var action = $('#hidAction').val();
        $('#addform').attr('action', action);                   
        $('#hidbtnsubmit').val("submit");
        $('#addform').submit();
    }   
}
</script> 
<style>
.field-icon {
  float: right;
  margin-right:10px;
  margin-top: -30px;
  position: relative;
  z-index: 2;
}
</style>
<style>
.form-horizontal { font-weight:normal; }
</style>
</body>
<!-- END BODY -->
</html>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLEAPIKEY;?>" type="text/javascript"></script>
<script type="text/javascript">
$( "#getlatlong" ).click(function() {
  //alert( "Handler for .click() called." );
  //$(this).find("option:selected").text();
  var curaddress=$('[name="address"]').val();
  var subarea=$('#subarea').val();
  if(subarea!=''){subarea=", "+$('#subarea').find("option:selected").text();}else{subarea='';}
   var area=$('#area').val();
  if(area!=''){area=", "+$('#area').find("option:selected").text();}else{area='';}
   var city=$('[name="city"]').val();
  if(city!=''){city=", "+$('[name="city"]').find("option:selected").text();}else{city='';}
   var state=$('[name="state"]').val();
  if(state!=''){state=", "+$('[name="state"]').find("option:selected").text();}else{state='';}
    var address = curaddress+''+subarea+''+ area+''+city+''+state;  
                    //alert(address);
    //var address123 = address.replace("/^[a-zA-Z\s](\d)?$/","");
    //alert(address123);
                    
  var geocoder = new google.maps.Geocoder();
    //var address = "new york";

    geocoder.geocode( { 'address': address}, function(results, status) {

      if (status == google.maps.GeocoderStatus.OK) {
        var latitude = results[0].geometry.location.lat();
        var longitude = results[0].geometry.location.lng();     
        $('[name="latitude"]').val(latitude);
        $('[name="longitude"]').val(longitude);
      } else{
          alert("Not getting proper latitude,longitude!");
      }
    }); 
});

</script>