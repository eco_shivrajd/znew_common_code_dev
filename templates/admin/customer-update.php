<!-- BEGIN HEADER -->
<?php 
//error_reporting(E_ERROR | E_PARSE);
//ini_set('display_errors', 1);
include "../includes/header.php"; 
include "../includes/customerManage.php";
$customerObj 	= 	new customerManager($con,$conmain);
if($_SESSION[SESSION_PREFIX.'user_type']!="Admin") {
	header("location:../logout.php");
} 
if(isset($_POST['submit']))
{
    $cust_id=base64_decode($_GET['id']);

    $cust_mobile = $_POST['cust_mobile'];
    $sql = "SELECT cust_mobile FROM `tbl_customer` WHERE cust_mobile = '".$cust_mobile."' AND cust_id != '".$cust_id."' ";
    $result = mysqli_query($con, $sql);
    $row_count = mysqli_num_rows($result);   
   
    if($row_count=='0')
    {
    $customer = $customerObj->updateCustomerDetails($cust_id);	
    echo '<script>alert("Customer Updated successfully.");location.href="customer_list.php";</script>';

    } else  {
            echo '<script>alert("Mobile No Already Exist.Please Enter Another Mobile No.");</script>';
    }   
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "customer_list";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->			
			<h3 class="page-title">Customer</h3>
			
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="customer_list.php">Customer</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Customer</a>
					</li>
				</ul>				
			</div>
			
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Customer
							</div>
							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
	<?php
       $id=base64_decode($_GET['id']);
       $result1 = $customerObj->getCustomerDetailsById($id);
       $row = mysqli_fetch_assoc($result1);
    ?>				
       <form class="form-horizontal" data-parsley-validate="" role="form" method="post" enctype="multipart/form-data" action="">     
             <div class="form-group">
              <label class="col-md-3">Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text"
                placeholder="Enter Name"
                data-parsley-trigger="change"               
                data-parsley-required="#true" 
                data-parsley-required-message="Please enter name"
                data-parsley-maxlength="50"
                data-parsley-maxlength-message="Only 50 characters are allowed"             
                name="cust_name" value="<?php echo fnStringToHTML($row['cust_name'])?>" class="form-control">
              </div>
            </div>

               <div class="form-group">
              <label class="col-md-3">Email:</label>

              <div class="col-md-4">
                <input type="text" id="email"
                placeholder="Enter E-mail"
                data-parsley-maxlength="100"
                data-parsley-maxlength-message="Only 100 characters are allowed"
                data-parsley-type="email"
                data-parsley-type-message="Please enter valid e-mail" 
                data-parsley-trigger="change"
                name="cust_email" value="<?php echo fnStringToHTML($row['cust_email'])?>" class="form-control"><span id="user-availability-status"></span>
              </div>
            </div>
            <div class="form-group">
                  <label class="col-md-3">Mobile Number:</label>

                  <div class="col-md-4">
                    <input type="text"  name="cust_mobile"
                    placeholder="Enter Mobile Number"
                    data-parsley-trigger="change"                   
                    data-parsley-minlength="10"
                    data-parsley-maxlength="15"
                    data-parsley-maxlength-message="Only 15 characters are allowed"
                    data-parsley-pattern="^(?!\s)[0-9]*$"
                    data-parsley-pattern-message="Please enter numbers only"
                    class="form-control" value="<?php echo fnStringToHTML($row['cust_mobile'])?>">
                  </div>
                </div>

           <div class="form-group">
                <label class="col-md-3">Address:<span class="mandatory">*</span></label>
                <div class="col-md-4">
                    <textarea name="cust_address"
                    placeholder="Enter Address"
                    data-parsley-trigger="change"
                    data-parsley-required="#true" 
                    data-parsley-maxlength="200"
                    data-parsley-maxlength-message="Only 200 characters are allowed"                            
                    rows="4" class="form-control" ><?php echo fnStringToHTML($row['cust_address'])?></textarea>
                </div>
            </div>
      
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
				<button type="submit" name="submit" class="btn btn-primary">Submit</button>
                <a href="customer_list.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group -->
          </form>     
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<script type="text/javascript">
	function checkAvailability() {
    $('#addform').parsley().validate();
    var cust_email = $("#cust_email").val();
 
    
    if(cust_email != '')
    {
        validate = 1;
        jQuery.ajax({
            url: "../includes/checkCustomerAvailable.php",
            data:'validation_field=cust_email&cust_email='+cust_email,
            type: "POST",
            async:false,
            success:function(data){         
                if(data=="exist") {
                    alert('E-mail already exists.');
                    validate = 1;
                    return false;
                } else {
                    validate = 0;
                }
            },
            error:function (){}
        });
    } 
    else 
    {
        validate = 1;
    }
    
    if(validate == 0 )
    {
        var action = $('#hidAction').val();
        $('#addform').attr('action', action);                   
        $('#hidbtnsubmit').val("submit");
        $('#addform').submit();
    }   
}
</script>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>
