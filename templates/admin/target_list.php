<!-- BEGIN HEADER -->
<?php 
include "../includes/grid_header.php";
include "../includes/userManage.php";
include "../includes/targetManage.php";
$userObj 	= 	new userManager($con,$conmain);
$targetObj 	= 	new targetManager($con,$conmain);
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Targets";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<h3 class="page-title">Targets</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li><i class="fa fa-home"></i>
					<a href="#">Targets</a></li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
						
							<div class="caption">Target Listing</div>
							
							<!-- <? if($_SESSION[SESSION_PREFIX."user_type"]=="Admin")  { ?>
								<a href="leads-add.php" class="btn btn-sm btn-default pull-right mt5">Add Lead</a>
							<? } ?> -->
							
                            <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_2">
								<thead>
									<tr>
										<th>
											User Name
										</th>
										<th>
											User Type
										</th>
										<th>
											Target Type
										</th>
										<th>
											RS
										</th>
										<th>
											Weight
										</th>										
										<th>
											Start Date
										</th>
										<th>
											End Date
										</th>										
									</tr>
								</thead>
							<tbody>
							<?php							
							$result1 = $targetObj->getTargets();
							//echo "<pre>";print_r($result1);
					       if( $result1!=0){
							while($row = mysqli_fetch_array($result1))
							{
								$target_type=$row['target_type'];
								if($target_type=='weekly'){
									$weekly_date=explode('::', $row['start_date']);
									$start_date=$weekly_date[0];
									$end_date=$weekly_date[1];
								}
								if($target_type=='monthly'){
									$monthly_date = $row['start_date'];
									$start_date="01-".$monthly_date;
									 $timestamp1    = strtotime($start_date);
									$end_date = date('t-m-Y', $timestamp1);
								}
								if($target_type=='daily'){
									$daily_date = date('d-m-Y',strtotime($row['start_date']));
									$start_date=$daily_date;
									$end_date=$daily_date;
								}
								if($target_type=='spdate'){
									$daily_date = date('d-m-Y',strtotime($row['start_date']));
									$end_date1 = date('d-m-Y',strtotime($row['end_date']));
									$start_date=$daily_date;
									$end_date=$end_date1;
								}
                           ?>
							<tr class="odd gradeX">
								<td>
								<?php echo $row['name'];?>
								</td> 
									<td><?php echo $row['assign_user_type'];?></td>
									<td><?php echo $row['target_type'];?></td> 
									<td align="right"><?php echo number_format((float)$row['target_in_rs'], 2, '.', '');?></td> 
									<td><?php echo $row['target_in_wt']."-".$row['unitname1'];?></td>
									<td><?php echo $start_date;?></td>
									<td><?php echo $end_date;?></td> 									
                               </tr>	
                            <?php
							} 
						   }
							?>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>