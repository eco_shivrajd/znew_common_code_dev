<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/shopManage.php";
include "../includes/userConfigManage.php";
$shopObj 	= 	new shopManager($con,$conmain);
$userconfObj 	= 	new userConfigManager($con,$conmain);
$id=base64_decode($_GET['id']);		 
$utype=base64_decode($_GET['utype']); 

$sql5="SELECT external_id,firstname,user_type,user_role FROM tbl_user where id='".$id."'";
$result5 = mysqli_query($con,$sql5);
$row5 = mysqli_fetch_assoc($result5);
$external_id = $row5['external_id'];
$firstname = $row5['firstname'];
$user_role = $row5['user_role'];
$user_type = $row5['user_type']; 

/*$sql6="SELECT od.order_id,o.order_from,o.order_to,od.order_status 
FROM tbl_orders o
LEFT JOIN tbl_order_details od ON od.order_id = o.id
where o.order_to='".$id."' AND (od.order_status!=1 OR od.order_status!=2) GROUP BY od.order_id";
$sql7="SELECT od.order_id,o.order_from,o.order_to,od.order_status 
FROM tbl_orders o
LEFT JOIN tbl_order_details od ON od.order_id = o.id
where o.order_from='".$id."' AND (od.order_status!=1 OR od.order_status!=2) GROUP BY od.order_id";
$result6 = mysqli_query($con,$sql6);
$row_count6 = mysqli_num_rows($result6);
$result7 = mysqli_query($con,$sql7);
$row_count7 = mysqli_num_rows($result7);

if ($row_count6 == 0 || $row_count7 == 0) 
{
	echo '<script>alert("You Can Not Delete This User Because This Users Orders Are Pending.");location.href="user_list_config.php";</script>';
}
exit();*/


if(isset($_POST['deleteuserbtn']))
{
	$field_type	=	$_POST['fieldtype'];
	$u_id	=	$_POST['u_id'];	
	$u_id_replace	=	$_POST['u_id_replace'];
	//print_r($_POST);
	//exit();
	switch ($field_type) {
		case "Superstockist":
			//echo "Superstockist";
			$userid_local = $userconfObj->deleteSorSSById($u_id,$u_id_replace);
			//echo "<pre>";print_r($userid_local);
			echo '<script>alert("Superstockist deleted successfully.");location.href="user_list_config.php";</script>';
			break;
		case "Distributor":		
			$userid_local = $userconfObj->deleteSorSSById($u_id,$u_id_replace);
			echo '<script>alert("Distributor deleted successfully.");location.href="user_list_config.php";</script>';
			break;
		case "DeliveryPerson":			
			$userid_local = $userconfObj->deleteDeliveryPersonById($u_id);			
			echo '<script>alert("Delivery Person deleted successfully.");location.href="user_list_config.php";</script>';
			break;	
		case "SalesPerson":			
			$userid_local = $userconfObj->deleteSalespersonById($u_id);			
			echo '<script>alert("Sales Person deleted successfully.");location.href="user_list_config.php";</script>';
			break;		
		case "Shopkeeper":
				$userid_local = $userconfObj->deleteShopkeeperById($u_id);			
			   echo '<script>alert("Shopkeeper deleted successfully.");location.href="user_list_config.php";</script>';
			break;
		case "DeliveryChannelPerson":
			$userid_local = $userconfObj->deletedcpById($u_id);			
		   echo '<script>alert("Delivery Channel Person deleted successfully.");location.href="user_list_config.php";</script>';
		break;
			case "Accountant":
			$userid_local = $userconfObj->deleteAccountantById($u_id);			
		   echo '<script>alert("Accountant deleted successfully.");location.href="user_list_config.php";</script>';
		break;
		default:
	}
} 
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "User List";//'Stockist',''Superstockist
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->			
			<h3 class="page-title">Delete <?php echo $user_role;?></h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">

					<li>
                    <i class="fa fa-home"></i>
                    <a href="user_list_config.php">User List</a>
                    <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                    <a href="#">Delete <?php echo $user_role;?></a>
                    </li>

				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Delete <?php echo $firstname; ?> 							</div>
                            
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
							<form class="form-horizontal" id="frmsearch"  method="post" action="manageuser_config.php" data-parsley-validate="">
							 
							 <input type="hidden" name="fieldtype" id="fieldtype" value="<?=$user_role;?>">
							  <input type="hidden" name="u_id" id="u_id" value="<?=$id;?>">
							
							 <?php if($user_role=='Superstockist'||$user_role=='Distributor'){ ?>
								 <div class="form-group">
									<label class="col-md-3">Select Replacement:<span class="mandatory">*</span></label>
									<div class="col-md-4">
									<select name="u_id_replace"  id="u_id_replace" class="form-control" 
										data-parsley-trigger="change"				
										data-parsley-required="#true" 
										data-parsley-required-message="Please select user" required="">
									      <option value='' >-Select-</option>
											<?php
										 
									      $result1 = $userconfObj->getUserTypeConfig($user_type);							
											while($row = mysqli_fetch_array($result1))
											{
												//echo "<pre";
												//print_r($row);
												$cat_id=$row['id'];
												if($cat_id==$id){
													echo "";
												}else{
													echo "<option value='$cat_id'>" . fnStringToHTML($row['firstname']) . "</option>";
												}
											}
                                       ?>
										</select>
										
									</div>
								</div><!-- /.form-group -->	
							<?php } ?>
								
							
							 <button type="submit" class="btn btn-success" name="deleteuserbtn" 
								id="deleteuserbtn" Onclick="return ConfirmDelete()" data-toggle="modal">Confirm Delete </button>		
						
						</form>	
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
</div>
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
		
<script type="text/javascript" src="../../assets/global/scripts/jquery.loader.js"></script>
<script>
function ConfirmDelete() {
	//alert($('#fieldtype').val());
	
	if($('#fieldtype').val()=='Taluka')
		return confirm("Are you sure you want to delete this Taluka and subarea under this Taluka?");
	else if($('#fieldtype').val()=='Subarea')
		return confirm("Are you sure you want to delete this  subarea?");
	else if($('#fieldtype').val()=='Superstockist')
		return confirm("Are you sure you want to delete this Superstockist?");
	else if($('#fieldtype').val()=='Distributor')
		return confirm("Are you sure you want to delete this Distributor?");
	else if($('#fieldtype').val()=='Accountant')
		return confirm("Are you sure you want to delete this Accountant?");
	else if($('#fieldtype').val()=='DeliveryChannelPerson')
		return confirm("Are you sure you want to delete this DeliveryChannelPerson?");
	//else if($('#fieldtype').val()=='UserType')
		//return confirm("Are you sure you want to delete this User Type?");
	else if($('#fieldtype').val()=='SalesPerson')
		return confirm("Are you sure you want to delete this SalesPerson?");
	else if($('#fieldtype').val()=='Shopkeeper')
		return confirm("Are you sure you want to delete this Shopkeeper?");
	else if($('#fieldtype').val()=='DeliveryPerson')
		return confirm("Are you sure you want to delete this DeliveryPerson?");
	else if($('#fieldtype').val()=='DeliveryChannelPerson')
		return confirm("Are you sure you want to delete this DeliveryChannelPerson?");
	else if($('#fieldtype').val()=='Accountant')
		return confirm("Are you sure you want to delete this Accountant?");
	else{
		return confirm("Are you sure you want to delete Record?");
	}
}
</script>
</body>
<!-- END BODY -->
</html>