<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/commonManage.php";
if($_SESSION[SESSION_PREFIX.'user_type']!="Admin") 
{
	header("location:../logout.php");
}
$id=base64_decode($_GET['id']);

if(isset($_POST['submit']))
{
	$name=fnEncodeString($_POST['suburbnm']);

	$sql_product_check=mysqli_query($con,"select id,isdeleted from `tbl_area` where suburbnm='$name' and id!='$id'");
	
	if($rowcount = mysqli_num_rows($sql_product_check)>0){	
		while($row_record=mysqli_fetch_assoc($sql_product_check))
		{
			$isdeleted=$row_record['isdeleted'];
			$exist_area_id=$row_record['id'];
		}
		if($isdeleted==0){
			echo '<script>alert("Duplicate Taluka not added.");location.href="suburb1.php?id='.base64_encode($id).'";</script>';
		}else{
			$sql_insert = "UPDATE tbl_area set isdeleted='0' where id='$exist_area_id'";
			$sql1 = mysqli_query($con,$sql_insert);
			$sql_insert = "UPDATE tbl_area set isdeleted='1' where id='$id'";
			$sql1 = mysqli_query($con,$sql_insert);
			echo '<script>alert("Taluka updated successfully.");location.href="suburb.php";</script>';
		}
	} else {
		$sql = "UPDATE tbl_area SET suburbnm='$name' where id='$id'";
		$update_sql=mysqli_query($con,$sql);
		$commonObj 	= 	new commonManage($con,$conmain);
		$commonObj->log_update_record('tbl_area',$id,$sql);
		echo '<script>alert("Taluka updated successfully.");location.href="suburb.php";</script>';
	}	
}

$sql1="SELECT stateid,cityid,suburbnm  FROM tbl_area where id = '$id' ";
$result1 = mysqli_query($con,$sql1);
$row1 = mysqli_fetch_array($result1);
$stateid = $row1["stateid"];
$cityid = $row1['cityid'];
$suburbnm = fnStringToHTML($row1['suburbnm']);

?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php 
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Taluka";
	include "../includes/sidebar.php";

	$commonObj 	= 	new commonManage($con,$conmain);
	$row_url=$commonObj->getPageIDforUrlEdit($php_page_name);
	$page_id_url = $row_url['page_id'];
	$row_url_edit=$commonObj->getURLforEdit($profile_id,$page_id_url);
	$ischecked_edit_url = $row_url_edit['ischecked_edit'];
    if ($ischecked_edit_url == 0 && $ischecked_edit_url!='') 
	{
		session_set_cookie_params(0);
		session_start();
		session_destroy();
		echo '<script>location.href="../login.php";</script>';
	    exit;
	}
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->			
			<h3 class="page-title">Taluka</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					
					<li>
						<i class="fa fa-home"></i>
						<a href="suburb.php">Taluka</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Taluka</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Taluka
							</div>							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						  
						<form class="form-horizontal" data-parsley-validate="" role="form" method="post">         
						<div class="form-group">
						  <label class="col-md-3">State:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
						  <select name="state"              
						  data-parsley-trigger="change"				
						  data-parsley-required="#true" 
						  data-parsley-required-message="Please select state"  disabled 
						  class="form-control" onChange="showUser(this.value)">
						  <option >-Select-</option>
							<?php
							$sql="SELECT * FROM tbl_state where country_id=101";
							$result = mysqli_query($con,$sql);
							while($row = mysqli_fetch_array($result))
							{
								$cat_id=$row['id'];
								if($stateid == $cat_id){
									$sel="SELECTED";
								}
								else{
									$sel="";
								}
								echo "<option value='$cat_id' $sel>" . $row['name'] . "</option>";
							} ?>
							</select>
						  </div>
						</div><!-- /.form-group -->
						
						<div id="Subcategory"></div> 
						
						<div class="form-group">
						  <label class="col-md-3">Taluka Name:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text" 
							placeholder="Enter Taluka Name"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter Taluka name"
							data-parsley-maxlength="50"
							data-parsley-maxlength-message="Only 50 characters are allowed"
							name="suburbnm" class="form-control" value="<?=$suburbnm;?>">
						  </div>
						</div><!-- /.form-group -->
						<div class="form-group">
						  <div class="col-md-4 col-md-offset-3">
						   <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
							<a href="suburb.php" class="btn btn-primary">Cancel</a>
						  </div>
						</div><!-- /.form-group -->
					  </form>                                       
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->

<style>
.form-horizontal{
font-weight:normal;
}
</style>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<script>  
function showUser(str,action)
{
if (str=="")
{
document.getElementById("Subcategory").innerHTML="";
return;
}
if (window.XMLHttpRequest)
{
xmlhttp=new XMLHttpRequest();
}
else
{
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
	if (xmlhttp.readyState==4 && xmlhttp.status==200)
	{
		document.getElementById("Subcategory").innerHTML=xmlhttp.responseText;
		if(action=="onload") {
			$("#city").val("<?=$cityid;?>");
			$('#city').attr("disabled", true); 			
		}
	}
}
xmlhttp.open("GET","fetch.php?cat_id="+str,true);
xmlhttp.send();
}
showUser('<?=$stateid;?>','onload');


</script>            