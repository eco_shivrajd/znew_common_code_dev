<!-- BEGIN HEADER -->
<?php
//phpinfo();die();
include "../includes/grid_header.php";
include "../includes/userManage.php";

include "../includes/productManage.php";
include "../includes/orderManage.php";
$userObj = new userManager($con, $conmain);

$prodObj = new productManage($con, $conmain);
$orderObj = new orderManage($con, $conmain);

$session_user_id=$_SESSION[SESSION_PREFIX . "user_id"];
$session_user_role=$_SESSION[SESSION_PREFIX . "user_role"];

?>
<!-- END HEADER -->
<style>
    .form-horizontal .control-label {
        text-align: left;
    }
	small, .small {
	 font-size: 77%;
	} 
	</style>
	<!--style for on//off switch-->
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">


</head>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLEAPIKEY; ?>&callback=initMap" type="text/javascript"></script>
<!-- END HEAD -->
<body class="page-header-fixed page-quick-sidebar-over-content ">

    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "Stock";
        $activeMenu = "stock_requests_list";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->

        <div class="page-content-wrapper">
            <div class="page-content">			
                <h3 class="page-title">
                    Stock Requests 
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                     <li>
                     <i class="fa fa-home"></i>
                     <a href="index.php">Home</a>
                     <i class="fa fa-angle-right"></i>
                  </li>					
                        <li>                           
                            <a href="#">Stock Requests </a>
                        </li>                         


                    </ul>				
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">
                                    Stock Requests 
                                </div>
								<a  href="stock_assign.php" class="btn btn-sm btn-default pull-right mt5">
								  <span class="glyphicon glyphicon-arrow-down"></span> Assign Stock
								</a>
								
								<div class="clearfix"></div>	
                            </div>						
                            <div class="portlet-body">
								<!-- Nav tabs -->
							
						
							<!-- Tab panes -->							
							<div id="orders_form_table">
                                <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">
									
                                    <div class="form-group" id="order_status_tab_result">
                                        <label class="col-md-3">Requests Status:</label>
                                        <div class="col-md-4">
                                            <select name="order_status" id="order_status" autocomplete="off" data-parsley-trigger="change" class="form-control" ><!--onchange="fnShowUser(this.value)"-->
												<option value="11" selected >Received</option><!-- To Me(<small class="muted text-muted">By Other User</small>)-->
												<option value="44">Approved</option>
												<option value="9">Rejected</option>		
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->	
									
                                    <div class="form-group" id="divDaily">
                                        <label class="col-md-3">Select Request Date:</label>
                                        <div class="col-md-4">
                                            <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                                                <input type="text" class="form-control" data-date="<?php echo date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years" name="frmdate" id="frmdate" value="" autocomplete="off"> 
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                            <!-- /input-group -->								 
                                        </div>
                                    </div><!-- /.form-group -->	
								
                                      

                              <div class="form-group" >
                                        <label class="col-md-3">Requests From:</label>
                                        <div class="col-md-4" id="div_select_user">
                                            <select name="order_from" id="order_from" class="form-control">
                                                   <?php          
                                                    $sql = "SELECT firstname as name,id,user_role FROM `tbl_user` where external_id ='$session_user_id' and id!='1' and (user_role = 'Superstockist' OR user_role = 'Distributor') order by firstname";
                                                    $result1 = mysqli_query($con, $sql);?>
                                                    <option value="">-Select User-</option>
                                                 <?php
                                                    while ($row = mysqli_fetch_array($result1)) {
                                                        $cat_id = $row['id'];
                                                        echo "<option value='$cat_id'>" . fnStringToHTML($row['name']) . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                   
                                    <div class="form-group" style="display:none;">
                                        <label class="col-md-3">State:</label>
                                        <div class="col-md-4">
                                            <select name="dropdownState" id="dropdownState"              
                                                    data-parsley-trigger="change"				
                                                    data-parsley-required="#true" 
                                                    data-parsley-required-message="Please select state"
                                                    class="form-control" onChange="fnShowCity(this.value)">
                                                <option selected disabled>-Select-</option>
                                                <?php
                                                $sql = "SELECT id,name FROM tbl_state where country_id=101 order by name";
                                                $result = mysqli_query($con, $sql);
                                                while ($row = mysqli_fetch_array($result)) {
                                                    $cat_id = $row['id'];
                                                    echo "<option value='$cat_id'>" . $row['name'] . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>			
                                    <div class="form-group" id="city_div" style="display:none;">
                                        <label class="col-md-3">City:</label>
                                        <div class="col-md-4" id="div_select_city">
                                            <select name="dropdownCity" id="dropdownCity" data-parsley-trigger="change" class="form-control">
                                                <option selected value="">-Select-</option>										
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group" id="area_div" style="display:none;">
                                        <label class="col-md-3">Region:</label>
                                        <div class="col-md-4" id="div_select_area">
                                            <select name="area" id="area" data-parsley-trigger="change" class="form-control">
                                                <option selected value="">-Select-</option>									
                                            </select>
                                        </div>
                                    </div>									
                                    
                                    <div class="form-group">
                                        <label class="col-md-3">Category:</label>
                                        <div class="col-md-4" id="divCategoryDropDown">
                                            <?php $cat_result = $prodObj->getAllCategory(); ?>
                                            <select name="dropdownCategory" id="dropdownCategory" class="form-control" onchange="fnShowProducts(this)">
                                                <option value="">-Select-</option>
                                                <?php while ($row_cat = mysqli_fetch_assoc($cat_result)) {
                                                    ?>									
                                                    <option value="<?= $row_cat['id']; ?>"><?= $row_cat['categorynm']; ?></option>
                                                <?php } ?>								
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label class="col-md-3">Product:</label>
                                        <div class="col-md-4" id="divProductdropdown">
                                            <?php $prod_result = $prodObj->getAllProducts(); ?>
                                            <select name="dropdownProducts" id="dropdownProducts" class="form-control">
                                                <option value="">-Select-</option>
                                                <?php while ($row_prod = mysqli_fetch_assoc($prod_result)) {
                                                    ?>									
                                                    <option value="<?= $row_prod['id']; ?>"><?= $row_prod['productname']; ?></option>
                                                <?php } ?>	
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-3">											
                                            <button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>									
                                            <button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnReset();">Reset</button>
                                        </div>
                                    </div><!-- /.form-group -->
                                </form>	
                               									
                                    <div id="order_list"> </div>
							</div>
                        </div>
						<!--portable body-->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>

    <!-- END CONTAINER -->
    <div class="modal fade" id="view_invoice" role="dialog">
        <div class="modal-dialog" style="width: 980px !important;">    
            <!-- Modal content-->
            <div class="modal-content" id="view_invoice_content">      
            </div>      
        </div>
    </div>
    <div class="modal fade " id="order_details" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 880px !important;">
            <!-- Modal content-->
            <div class="modal-content" id="order_details_content">
            </div>
        </div>
    </div>

    <div class="modal fade" id="googleMapPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="model_content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                </div>
                <div class="modal-body" style="padding-bottom: 5px !important;"> 
                    <div id="map" style="width: 100%; height: 500px;"></div> 
                </div>
            </div>
        </div>
    </div>

    
	<div class="modal fade" id="update_stock_request_status" role="dialog" style="height:auto;">
		<div class="modal-dialog" >    <!--style="width: 400px !important;"-->
			<!-- Modal content-->
			<div class="modal-content"  >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Approve/Reject Request</h4>
				</div>
				<div class="modal-body" style="padding-bottom: 5px !important;">
					<form class="form-horizontal">
						<div class="form-group">
							<label class="col-md-6" style="padding-top:5px">Request Status:<span class="mandatory">*</span></label>	
							<div class="col-md-6" id="divdpDropdown">
								<select name="order_status_approve" id="order_status_approve" class="form-control" onChange="fnSelectionBoxStatus()">	
									<option value="4" selected>Approved</option>
									<option value="9">Rejected</option>
								</select>
							</div>
						</div><!-- /.form-group -->
						<div class="clearfix"></div>
						<div id="pro_table_div" ></div>
						<div class="clearfix"></div>
						<div class="form-group">
							<div class="col-md-offset-5 col-md-9">
								<input type="hidden" name="order_stock_id" id="order_stock_id">
								<button type="button" name="btn_stock_approve_reject" id="btn_stock_approve_reject" 
								class="btn btn-primary" data-dismiss="modal">Submit</button>
                                &nbsp;&nbsp;
                                <span id="add_stock_div" style="display:none;">
                                    <a  id="btn_add_stock" href="stock_add.php" class="btn btn-primary" >Add Stock</a>
                                </span>
							</div>
						</div>
						<!-- <div id="add_stock_div" style="display:none;">							                    
							<div class="form-group">
								<div class="col-md-offset-5 col-md-9">
									<a  id="btn_add_stock" href="stock_add.php" class="btn btn-primary" >Add Stock</a>
								</div>
							</div>
						</div> -->
					</form>
				</div>	
			</div>
		</div>
	</div>
    <!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php" ?>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- END FOOTER -->

<script>	
function fnSelectionBoxStatus()
{   
    //var str = $( "form" ).serialize();    
    var status =document.getElementById('order_status_approve').value;
    functiontest(status);
//  alert(status);
}		
$(document).ready(function () { 		
	/* $("#tooltipid").tooltip({
		items: "span",
		content: "This product can not be sent to production."
	}).tooltip("open"); */
	$("#select_th").removeAttr("class");
	$("#main_th th").removeAttr("class");
	if ($.fn.dataTable.isDataTable('#sample_2')) {
		table = $('#sample_2').DataTable();
		table.destroy();
		table = $('#sample_2').DataTable({
			"aaSorting": [],
		});
	}	
	ShowReport();
});

$('.date-picker1').datepicker({
	rtl: Metronic.isRTL(),
	orientation: "auto",
	endDate: "<?php echo date('d-m-Y'); ?>",
	autoclose: true
});

 $('.date-picker2').datepicker({
		rtl: Metronic.isRTL(),
		orientation: "auto",
	   format: "dd-mm-yyyy",
		todayHighlight: true,
		startDate: "<?php echo date('d-m-Y'); ?>",
		endDate: "<?php echo date('d-m-Y'); ?>",
		setDate: "<?php echo date('d-m-Y'); ?>",
		autoclose: true
	});
$('.date-picker3').datepicker({
	rtl: Metronic.isRTL(),
	orientation: "auto",
	startDate: 'd',
	endDate: '+3m',
	autoclose: true
});
function fnReset() {
	location.reload();
}

function fnShowCity(id_value) {
	$("#city_div").show();
	$("#area").html('<option value="">-Select-</option>');
	$("#subarea").html('<option value="">-Select-</option>');
	var url = "getCityDropDown.php?cat_id=" + id_value + "&select_name_id=city&mandatory=mandatory";
	CallAJAX(url, "div_select_city");
}


function FnGetSuburbDropDown(id) {
	$("#area_div").show();
	$("#subarea").html('<option value="">-Select-</option>');
	var url = "getSuburDropdown.php?cityId=" + id.value + "&select_name_id=area&function_name=FnGetSubareaDropDown&mandatory=mandatory";
	CallAJAX(url, "div_select_area");
}

function FnGetShopsDropdown(id) {
	$("#shop_div").show();
	$("#divShopdropdown").html('<option value="">-Select-</option>');
	var param = "";
	var state_id = $("#dropdownState").val();
	var city_id = $("#city").val();
	var suburb_id = $("#area").val();
	if (state_id != '')
		param = param + "&state_id=" + state_id;
	if (city_id != '')
		param = param + "&city_id=" + city_id;
	if (suburb_id != '') {
		if (suburb_id != undefined) {
			param = param + "&suburb_id=" + suburb_id;
		}
	}
	if (id != '') {
		if (id != undefined)
			param = param + "&suburb_id=" + id.value;
	}

	var url = "getShopDropdownByAddress.php?param=param" + param;
	CallAJAX(url, "divShopdropdown");
}
function fnShowProducts(id) {
	var url = "getProductDropdown.php?cat_id=" + id.value;
	CallAJAX(url, "divProductdropdown");
}


function ShowReport() {
  var order_status = $('#order_status').val();		
  
	var user_type='<?=$user_type?>';

	var url = "ajax_show_orders_request_stock_list.php";

	var data = $('#frmsearch').serialize();

	jQuery.ajax({
		url: url,
		method: 'POST',
		data: data,
		async: false
	}).done(function (response) {
		$('#order_list').html(response);
		var table = $('#sample_2').dataTable();
		table.fnFilter('');
		$("#select_th").removeAttr("class");
	}).fail(function () { });
	return false;
}
function showInvoice(order_status, id) {
	var url = "invoice2.php";
	jQuery.ajax({
		url: url,
		method: 'POST',
		data: 'order_status=' + order_status + '&order_id=' + id,
		async: false
	}).done(function (response) {
		$('#view_invoice_content').html(response);
		$('#view_invoice').modal('show');
	}).fail(function () { });
	return false;
}
function showApproveORReject(order_stock_id) {
	$('#update_stock_request_status').modal('show');
	$('#order_stock_id').val(order_stock_id); 
	var url = "ajax_get_stock.php";
	jQuery.ajax({
		url: url,
		method: 'POST',
		data: 'order_ids=' + order_stock_id+'&tabid=sp_user',
		async: false
	}).done(function (response) {
		$('#pro_table_div').html(response);
		if($('#flag_avail_stock').val()=='allred'){
			$('#stock_avail_table').append('<tr><td colspan="4" style="color:red;">You dont have sufficient available stock.Please add Stock.</td></tr>');
			$("button[id^='btn_stock_approve_reject']" ).hide();
			$("#add_stock_div").show();
		}else{
			$("button[id^='btn_stock_approve_reject']" ).show();
		}
	}).fail(function () { });
	return false;
}
$('#btn_stock_approve_reject').click(function(){	
	var order_stock_id = $('#order_stock_id').val(); 
	var order_status_approve = $('#order_status_approve').val();
	var order_status_approve_text=$("#order_status_approve option:selected").text();

        var proid_varid = $("#proid_varid").val();
        var mnf_date = $("#mnf_date").val();  
        var void1 = [];
        $('input[name="proid_varid[]"]').each(function () {
            void1.push(this.value);
        });       
        var proid_varid1 = void1.join();
        var void12 = [];
        $('input[name="mnf_date[]"]').each(function () {
            void12.push(this.value);
        });       
        var mnf_date12 = void12.join();
      

	var url = 'updateOrderDelivery.php?flag=approve_or_reject&order_stock_id=' + order_stock_id + '&order_status_approve=' + order_status_approve + '&proid_varid1=' + proid_varid1 + '&mnf_date12=' + mnf_date12;
	$.ajax({
		url: url,
		datatype: "JSON",
		contentType: "application/json",
		error: function (data) {
			console.log("error:" + data)
		},success: function (data) {
			console.log(data);
			if (data > 0) {
				alert("The stock request is "+order_status_approve_text+".");
				window.location.reload();
			} else {
				alert("Not updated.");
			}
		}
	});
});

function showOrderDetails(order_status,oid) {
	var url = "order_details_popup.php";
	jQuery.ajax({
		url: url,
		method: 'POST',
		data: 'order_id=' + oid + '&order_status=' + order_status,
		async: false
	}).done(function (response) {
		$('#order_details_content').html(response);
		$('#order_details').modal('show');
	}).fail(function () { });
	return false;
}

function OrderDetailsPrint() {
	var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
	var divContents = '<style>\body {\
			font-size: 12px;}\
	th {text-align: left;}\
	.printHeading { line-height: 18px;  padding: 10px 0;  font-size: 18px; }\
	table { border-collapse: collapse;  \
			font-size: 12px; }\
	table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
	body { font-family: "Open Sans", sans-serif;\
	background-color:#fff;\
	font-size: 15px;\
	direction: ltr;}</style>' + $("#divOrderPrintArea").html();
	if (isIE == true) {
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write(divContents);
		printWindow.focus();
		printWindow.document.execCommand("print", false, null);
	} else {
		$('<iframe>', {
			name: 'myiframe',
			class: 'printFrame'
		}).appendTo('body').contents().find('body').html(divContents);
		window.frames['myiframe'].focus();
		window.frames['myiframe'].print();
		setTimeout(
				function ()
				{
					$(".printFrame").remove();
				}, 1000);
	}
}
function takeprint11() {
	var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
	var divContents = '<style>\body {\
			font-size: 12px;}\
	th {text-align: left;}\
	.printHeading { line-height: 18px;  padding: 10px 0;  font-size: 18px; }\
	table { border-collapse: collapse;  \
			font-size: 12px; }\
	table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
	body { font-family: "Open Sans", sans-serif;\
	background-color:#fff;\
	font-size: 15px;\
	direction: ltr;}</style>' + $("#divPrintArea").html();
	if (isIE == true) {
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write(divContents);
		printWindow.focus();
		printWindow.document.execCommand("print", false, null);
	} else {
		$('<iframe>', {
			name: 'myiframe',
			class: 'printFrame'
		}).appendTo('body').contents().find('body').html(divContents);
		window.frames['myiframe'].focus();
		window.frames['myiframe'].print();
		setTimeout(
				function ()
				{
					$(".printFrame").remove();
				}, 1000);
	}
}
function takeprint_invoice() {
	var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
	var divContents = '<style>\
	.darkgreen{	background-color:#364622; color:#fff!important; font-size:24px; font-weight:600;}\
	.fentgreen1{background-color:#b0b29c;color:#4a5036;	font-size:12px;}\
	.fentgreen{	background-color:#b0b29c;	color:#4a5036;}\
	.font-big{	font-size:20px;	font-weight:600;	color:#364622;}\
	.font-big1{	font-size:18px;	font-weight:600;	color:#364622;}\
	.table-bordered-popup {    border: 1px solid #364622;}\
	.table-bordered-popup > tbody > tr > td, .table-bordered-popup > tbody > tr > th, .table-bordered-popup > thead > tr > td, .table-bordered-popup > thead > tr > th {\
			border: 1px solid #364622;	color:#4a5036;}\
	.blue{	color:#010057;}\
	.blue1{	color:#574960;	font-size:16px;}\
	.buyer_section{	color:#574960;	font-size:14px;}\
	.pad-5{	padding-left:10px;}\
	.pad-40{	padding-left:40px;}\
	.np{	padding-left:0px;	padding-right:0px;}\
	.bg{	background-image:url(../../assets/global/img/invoice/<?php echo COMPANYNM;?>_logo-watermark.jpg); background-repeat:no-repeat;\
	 background-size: 200px 200px;}\
	</style>' + $("#divPrintArea").html();
	
	if (isIE == true) {
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write(divContents);
		printWindow.focus();
		printWindow.document.execCommand("print", false, null);
	} else {
		$('<iframe>', {
			name: 'myiframe',
			class: 'printFrame'
		}).appendTo('body').contents().find('body').html(divContents);
		window.frames['myiframe'].focus();
		window.frames['myiframe'].print();
		setTimeout(
				function ()
				{
					$(".printFrame").remove();
				}, 1000);
	}
}
var lat = 0;
var lng = 0;

$('#googleMapPopup').on('shown.bs.modal', function (e) {

	var latlng = new google.maps.LatLng(lat, lng);
	var map = new google.maps.Map(document.getElementById('map'), {
		center: latlng,
		zoom: 17
	});
	var marker = new google.maps.Marker({
		map: map,
		position: latlng,
		draggable: false,
		//anchorPoint: new google.maps.Point(0, -29)
	});
});
function showGoogleMap(getlat, getlng) {

	lat = getlat;
	lng = getlng;
	$('#googleMapPopup').modal('show');
}
	
   
function fnShowSalesperson(id) {
	var url = "getSalesPersonDropDown.php?cat_id=" + id.value;
	CallAJAX(url, "divsalespersonDropdown");
}

</script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
</html>