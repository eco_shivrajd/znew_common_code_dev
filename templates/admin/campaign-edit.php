<?php
session_start();
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include "../includes/header.php";
include "../includes/commonManage.php";
$id = base64_decode($_GET['id']);
if(isset($_POST['submit']))
{ 
	//print_r($_POST);
	//exit();
	$id = $_POST['id'];	
	$added_on_field = ", added_on";
	
	$added_on_field1="";
	if($_POST['left_count'] != ''){
		$added_on_field1.= ", left_count";
		$added_on_value1.= ", '".fnEncodeString($_POST['left_count'])."'";
	}
	if($_POST['right_count'] != ''){
		$added_on_field1.= ", right_count";
		$added_on_value1.= ", '".fnEncodeString($_POST['right_count'])."'";
	}
	
	$added_on_value = ", '".date('Y-m-d')."'";
	$updated_on_value = date('Y-m-d');
	$values.= " campaign_end_date='".date("Y-m-d",strtotime($_POST['campaign_end_date']))."', updated_on='".$updated_on_value."'";
	
	if($_POST['status'] == '1'){		
		 $sql_delete_assigned_user = "DELETE FROM tbl_sp_campaign_assign
							WHERE campaign_id = ".$id;
		 $result2 = mysqli_query($con,$sql_delete_assigned_user);
	}
//exit();
	if($_POST['status'] != ''){		
		$values.= ", status='".$_POST['status']."' ";
	}
	$sql_campaign = "UPDATE `tbl_campaign` SET $values WHERE id=".$id;
	$sql_campaign_edit=mysqli_query($con,$sql_campaign);	
	$commonObj 	= 	new commonManage($con,$conmain);
	$commonObj->log_update_record('tbl_campaign',$id,$sql_campaign);
	echo '<script>alert("Campaign updated successfully.");location.href="campaign.php";</script>';
}
$sql_campaign="SELECT `id` AS campaign_id, `campaign_name`, `campaign_description`, `campaign_start_date`, `campaign_end_date`, `campaign_type`, `status`, `brand_all` FROM `tbl_campaign` WHERE deleted = 0 AND id='$id'";
$result_campaign = mysqli_query($con,$sql_campaign);
if(mysqli_num_rows($result_campaign)>0)
{
	$row_campaign = mysqli_fetch_assoc($result_campaign);
	$sql_campaign_area="SELECT `level`, `state_id`, `city_id`, `suburb_id`, `subarea_id`,`shop_id` FROM `tbl_campaign_area` WHERE deleted = 0 AND campaign_id=".$row_campaign['campaign_id'];
	$result_campaign_area = mysqli_query($con,$sql_campaign_area);
	if(mysqli_num_rows($result_campaign_area)>0)
	{
		$row_campaign['campaign_area'] = mysqli_fetch_assoc($result_campaign_area);
		if($row_campaign['campaign_area']['level'] == 'state')
		{
			$state_array = explode(",",$row_campaign['campaign_area']['state_id']);
			$row_campaign['campaign_area']['state_id_array'] = $state_array;
		}else{
			$row_campaign['campaign_area']['state_id_array'] = array($row_campaign['campaign_area']['state_id']);
		}
		if($row_campaign['campaign_area']['level'] == 'city')
		{
			$city_array = explode(",",$row_campaign['campaign_area']['city_id']);
			$row_campaign['campaign_area']['city_id_array'] = $city_array;
		}else{
			$row_campaign['campaign_area']['city_id_array'] = array($row_campaign['campaign_area']['city_id']);
		}
		if($row_campaign['campaign_area']['level'] == 'suburb')
		{
			$suburb_array = explode(",",$row_campaign['campaign_area']['suburb_id']);
			$row_campaign['campaign_area']['suburb_id_array'] = $suburb_array;
		}else{
			if($row_campaign['campaign_area']['suburb_id'] != '')
			{
				$row_campaign['campaign_area']['suburb_id_array'] = array($row_campaign['campaign_area']['suburb_id']);
			}
			else {
				$row_campaign['campaign_area']['suburb_id_array'] = '';
			}
		}
		if($row_campaign['campaign_area']['level'] == 'subarea')
		{
			$subarea_array = explode(",",$row_campaign['campaign_area']['subarea_id']);
			$row_campaign['campaign_area']['subarea_id_array'] = $subarea_array;
		}else{
			if($row_campaign['campaign_area']['subarea_id'] != '')
				{ $row_campaign['campaign_area']['subarea_id_array'] = array($row_campaign['campaign_area']['subarea_id']); }
			else {
				$row_campaign['campaign_area']['subarea_id_array'] = '';
			}
		}
		if($row_campaign['campaign_area']['level'] == 'shop')
		{
			$shop_array = explode(",",$row_campaign['campaign_area']['shop_id']);
			$row_campaign['campaign_area']['shop_id_array'] = $shop_array;
		}else{
			$row_campaign['campaign_area']['shop_id_array'] = array($row_campaign['campaign_area']['shop_id']);
		}
	}
	if($row_campaign['campaign_type'] == 'discount')
	{		
		$sql_campaign_area_price="SELECT `product_price`, `discount_percent` FROM `tbl_campaign_area_price` WHERE deleted = 0 AND campaign_id=".$row_campaign['campaign_id'];
		$result_campaign_area_price = mysqli_query($con,$sql_campaign_area_price);
		$count_price = mysqli_num_rows($result_campaign_area_price);
		if($count_price>0)
		{
			$row_campaign['campaign_area_price_count'] = $count_price;
			while($row = mysqli_fetch_assoc($result_campaign_area_price)) {
				$row_campaign['campaign_area_price'][] = $row;
			}
		}		
	}else if($row_campaign['campaign_type'] == 'free_product')
	{
		$sql_campaign_free_product="SELECT `c_p_brand_id`, `c_p_category_id`, `c_product_id`, `c_p_quantity`, `c_p_measure`,`c_p_measure_id`, `c_p_quantity_measure`, `f_p_brand_id`, `f_p_category_id`, `f_product_id`, `f_p_quantity`, `f_p_measure`,`f_p_measure_id`, `f_p_quantity_measure`,`left_count`,`right_count` FROM `tbl_campaign_product` WHERE deleted = 0 AND campaign_id=".$row_campaign['campaign_id'];
		$result_campaign_free_product = mysqli_query($con,$sql_campaign_free_product);
		$count_free_product = mysqli_num_rows($result_campaign_free_product);
		if($count_free_product>0)
		{
			if($row_campaign['brand_all'] == 1){
				$row_campaign['campaign_free_product_count'] = 1;
			}
			else{
				$row_campaign['campaign_free_product_count'] = $count_free_product;
			}
			while($row = mysqli_fetch_assoc($result_campaign_free_product)) {
					$row_campaign['campaign_free_product'][] = $row;
					if($row_campaign['brand_all'] == 1)
						break;
				}
		}
	}
	else if($row_campaign['campaign_type'] == 'by_weight')
	{
		$sql_campaign_free_product="SELECT `c_weight`, `c_unit`, `f_p_brand_id`, `f_p_category_id`, `f_product_id`, `f_p_quantity`, `f_p_measure`, `f_p_measure_id`, `f_p_quantity_measure` FROM `tbl_campaign_product_weight` WHERE deleted = 0 AND campaign_id=".$row_campaign['campaign_id'];
		$result_campaign_free_product = mysqli_query($con,$sql_campaign_free_product);
		$count_free_product = mysqli_num_rows($result_campaign_free_product);
		if($count_free_product>0)
		{
			$row_campaign['campaign_free_product_count'] = $count_free_product;			
			while($row = mysqli_fetch_assoc($result_campaign_free_product)) {
					$row_campaign['campaign_free_product'][] = $row;
				}
		}
	}
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
<?php
	$activeMainMenu = "ManageProducts"; $activeMenu = "Campaign";
	include "../includes/sidebar.php";
	$commonObj 	= 	new commonManage($con,$conmain);
	$row_url=$commonObj->getPageIDforUrlEdit($php_page_name);
	$page_id_url = $row_url['page_id'];
	$row_url_edit=$commonObj->getURLforEdit($profile_id,$page_id_url);
	$ischecked_edit_url = $row_url_edit['ischecked_edit'];
    if ($ischecked_edit_url == 0 && $ischecked_edit_url!='') 
	{
		session_set_cookie_params(0);
		session_start();
		session_destroy();
		echo '<script>location.href="../login.php";</script>';
	    exit;
	}?>	
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Campaign
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					 
					<li>
					<i class="fa fa-home"></i>
						<a href="campaign.php">Campaign</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Campaign</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Campaign
							</div>
							
						</div>
						<div class="portlet-body">
                        <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
                        <form onsubmit="return validateForm()" class="form-horizontal" data-parsley-validate="" role="form" method="post">
						  <div class="form-group">
							  <label class="col-md-3">Campaign Name:<!--<span class="mandatory">*</span>--></label>
							  <div class="col-md-4">
								<input type="text" name="campaign_name" id="campaign_name" readonly value="<?php echo fnStringToHTML($row_campaign['campaign_name']);?>" class="form-control">
							  </div>
							</div><!-- /.form-group -->	
							<div class="form-group">
							  <label class="col-md-3">Campaign Description:</label>
							  <div class="col-md-4">
								<textarea rows="4" class="form-control" name="campaign_description" readonly id="campaign_description"><?php echo fnStringToHTML($row_campaign['campaign_description']);?></textarea>
							  </div>
							</div><!-- /.form-group -->	
							<div class="form-group">
							  <label class="col-md-3">Start Date:<!--<span class="mandatory">*</span>--></label>
							  <div class="col-md-4">
								<input type="text" name="campaign_start_date" id="campaign_start_date" readonly value="<?php echo date('d-m-Y',strtotime($row_campaign['campaign_start_date']));?>" class="form-control">
							  </div>							
							</div><!-- /.form-group -->	
							<div class="form-group">
							  <label class="col-md-3">End Date:<span class="mandatory">*</span></label>
							  <div class="col-md-4">
							  <div class="input-group date date-picker3" data-date-format="dd-mm-yyyy">
								<input type="text" name="campaign_end_date" id="campaign_end_date" class="form-control" value="<?php echo date('d-m-Y',strtotime($row_campaign['campaign_end_date']));?>">
								<span class="input-group-btn">
								<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
								</span>
								</div>
							  </div>
							</div><!-- /.form-group -->	
							<div class="form-group">
							  <label class="col-md-3">Status:</label>
							  <div class="col-md-4">
							  <div class="input-group">					
									<select name="status" id="status" 
									class="form-control"
									
									>
										<option value="0" <?php if($row_campaign['status'] == 0) echo "selected";?>>Active</option>
										<option value="1" <?php if($row_campaign['status'] == 1) echo "selected";?>>Inactive</option>
									</select>
								</div>
							  </div>
							</div><!-- /.form-group -->	
							<div class="clearfix"></div> 
							<div class="form-group">
								<label class="col-md-3">Campaign Type:<!--<span class="mandatory">*</span>--></label>
								<div class="col-md-4">
									<input type="radio" name="criteriaType" id="criteriaType_disc" disabled value="discount" onclick="fnCriteriaSection('discount');" <?php if($row_campaign['campaign_type'] == 'discount'){ echo "checked";}?>>&nbsp;&nbsp;%Discount
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="criteriaType" id="criteriaType_free" disabled value="free_product" onclick="fnCriteriaSection('free_product');" <?php if($row_campaign['campaign_type'] == 'free_product') { echo "checked";}?>>&nbsp;&nbsp;Free Product 
								</div>
							</div><!-- /.form-group -->
							
						 	 
							 <div id="discount_div" <?php if($row_campaign['campaign_type'] == 'discount'){?>style="display:block;"<?php }else{ ?>style="display:none;" <?php }?>>				
								<?php include "campaign_edit_com_area_section.php";	?>  
								<hr/>
								<div><input type="hidden" name="id" id="id" value="<?php echo $id;?>"><input type="hidden" name="total_element" id="total_element" value="<?php echo $row_campaign['campaign_area_price_count'];?>"></div>
								<div><input type="hidden" name="total_element_free" id="total_element_free" value="<?php echo $row_campaign['campaign_free_product_count'];?>"></div>
								<div id="disccont">	
									<?php 
										$state_cnt = count($row_campaign['campaign_area']['state_id_array']);
										$city_cnt = count($row_campaign['campaign_area']['city_id_array']);
										$suburb_cnt = count($row_campaign['campaign_area']['suburb_id_array']);
										$shop_cnt = count($row_campaign['campaign_area']['shop_id_array']);
										$json_state = json_encode($row_campaign['campaign_area']['state_id_array']);
										$json_city = json_encode($row_campaign['campaign_area']['city_id_array']);
										$json_suburb = json_encode($row_campaign['campaign_area']['suburb_id_array']);
										$json_shop = json_encode($row_campaign['campaign_area']['shop_id_array']);
									?>
									<div>
										<input type="hidden" name="hid_state_cnt" id="hid_state_cnt" value='<?php echo $state_cnt;?>'>
										<input type="hidden" name="hid_city_cnt" id="hid_city_cnt" value='<?php echo $city_cnt;?>'>
										<input type="hidden" name="hid_suburb_cnt" id="hid_suburb_cnt" value='<?php echo $suburb_cnt;?>'>
										<input type="hidden" name="hid_shop_cnt" id="hid_shop_cnt" value='<?php echo $shop_cnt;?>'>
										<input type="hidden" name="hid_state" id="hid_state" value='<?php echo $json_state;?>'>
										<input type="hidden" name="hid_city" id="hid_city" value='<?php echo $json_city;?>'>
										<input type="hidden" name="hid_suburb" id="hid_suburb" value='<?php echo $json_suburb;?>'>
										<input type="hidden" name="hid_shop" id="hid_shop" value='<?php echo $json_shop;?>'>
									</div>
									<?php 
										$j = 0;
									for($i = 1; $i<=$row_campaign['campaign_area_price_count']; $i++){ 		
											$calculated_amount = (($row_campaign['campaign_area_price'][$j]['product_price']*$row_campaign['campaign_area_price'][$j]['discount_percent'])/100)
									?>								
									<div class="discdet" id="discount_<?php echo $i; ?>">
										<div class="form-group">
											<label class="col-md-3">Discount Details:</label>
											<div class="col-md-6 nopadl">		
											
											<div class="col-md-4">
												<input type="text" name="campaign_product_price_<?php echo $i; ?>" id="campaign_product_price_<?php echo $i; ?>" data-parsley-trigger="change" data-parsley-pattern="^(?!\s)[0-9.' ]*$" disabled class="form-control" value="<?php echo number_format($row_campaign['campaign_area_price'][$j]['product_price'],2, '.', '');?>" onchange="calculate_discount(<?php echo $i; ?>)">
											</div>
											<div class="col-md-1" id="nopad1">
											<label class="nopadl" style="padding-top:5px">Price</label>
											</div>										
											<div class="col-md-2"  id="camdis">	
												<input type="text" name="campaign_product_discount_<?php echo $i; ?>" id="campaign_product_discount_<?php echo $i; ?>" data-parsley-trigger="change" data-parsley-pattern="^(?!\s)[0-9.' ]*$" disabled class="form-control minsz" size="3" value="<?php echo $row_campaign['campaign_area_price'][$j]['discount_percent'];?>" onchange="calculate_discount(<?php echo $i; ?>)">
											</div>
											<div class="col-md-1" id="nopad1">
										<label class="nopadl" style="padding-top:5px; ">%</label>
										</div>	
											</div>			          
										</div>										
										<hr/>
									</div>
									<?php $j++; } ?>
								</div>								
							</div>
							<div id="free_product_div" <?php if($row_campaign['campaign_type'] == 'free_product'){?>style="display:block;"<?php }else{ ?>style="display:none;" <?php }?>>
								<?php include "campaign_edit_com_area_section.php"; ?>  
								<div id="freecont">
									<input type="hidden" name="campaign_free_product_count" id="campaign_free_product_count" value='<?php echo $row_campaign['campaign_free_product_count'];?>'>
								<?php $l=0;
								for($k = 1; $k<=$row_campaign['campaign_free_product_count']; $k++){?>
										<input type="hidden" name="hid_c_brand_id<?php echo $k;?>" id="hid_c_brand_id<?php echo $k;?>" value='<?php echo $row_campaign['campaign_free_product'][$l]['c_p_brand_id'];?>'>
										<input type="hidden" name="hid_c_category_id<?php echo $k;?>" id="hid_c_category_id<?php echo $k;?>" value='<?php echo $row_campaign['campaign_free_product'][$l]['c_p_category_id'];?>'>
										<input type="hidden" name="hid_c_product_id<?php echo $k;?>" id="hid_c_product_id<?php echo $k;?>" value='<?php echo $row_campaign['campaign_free_product'][$l]['c_product_id'];?>'>
										<input type="hidden" name="hid_c_variant_id<?php echo $k;?>" id="hid_c_variant_id<?php echo $k;?>" value='<?php echo $row_campaign['campaign_free_product'][$l]['c_p_quantity']." ".$row_campaign['campaign_free_product'][$l]['c_p_measure_id'];?>'>
										<input type="hidden" name="hid_f_brand_id<?php echo $k;?>" id="hid_f_brand_id<?php echo $k;?>" value='<?php echo $row_campaign['campaign_free_product'][$l]['f_p_brand_id'];?>'>
										<input type="hidden" name="hid_f_category_id<?php echo $k;?>" id="hid_f_category_id<?php echo $k;?>" value='<?php echo $row_campaign['campaign_free_product'][$l]['f_p_category_id'];?>'>
										<input type="hidden" name="hid_f_product_id<?php echo $k;?>" id="hid_f_product_id<?php echo $k;?>" value='<?php echo $row_campaign['campaign_free_product'][$l]['f_product_id'];?>'>
										<input type="hidden" name="hid_f_variant_id<?php echo $k;?>" id="hid_f_variant_id<?php echo $k;?>" value='<?php echo $row_campaign['campaign_free_product'][$l]['f_p_quantity']." ".$row_campaign['campaign_free_product'][$l]['f_p_measure_id'];?>'>
									<div id="free_pro_<?php echo $k; ?>">
									<div class="row free">
										<div class="col-sm-6">
										<h4 style="margin-top:30px;"><b>Campaign Product</b></h4>
										<hr />
										<div class="form-group" <?php if($row_campaign['brand_all'] == 1) echo "style='display:none;'"; ?>>
											<label class="col-md-6">Brand:</label>
											<div class="col-md-6" id="div_brand_campaign_p_<?php echo $k; ?>">
											<?php	
												$sql="SELECT name FROM tbl_brand WHERE id = ".$row_campaign['campaign_free_product'][$l]['c_p_brand_id'];
												$result1 = mysqli_query($con,$sql);
												$brand = mysqli_fetch_assoc($result1);
											?>
											<input type="text" name="brand_campaign_p_<?php echo $k; ?>" id="brand_campaign_p_<?php echo $k; ?>" readonly value="<?=fnStringToHTML($brand['name']);?>" class="form-control">
											</div>
										</div><!-- /.form-group -->
								
										<div class="form-group" <?php if($row_campaign['brand_all'] == 1) echo "style='display:none;'"; ?>>
											<label class="col-md-6">Category:</label>
											<div class="col-md-6" id="div_category_campaign_p_<?php echo $k; ?>">
											<?php											
												$sql="SELECT categorynm FROM tbl_category WHERE id = ".$row_campaign['campaign_free_product'][$l]['c_p_category_id'];
												$result1 = mysqli_query($con,$sql);
												$category = mysqli_fetch_assoc($result1);
											?>
											<input type="text" name="category_campaign_p_<?php echo $k; ?>" id="category_campaign_p_<?php echo $k; ?>" readonly value="<?=fnStringToHTML($category['categorynm']);?>" class="form-control">	
											</div>
										</div><!-- /.form-group -->
								
										<div class="form-group" <?php if($row_campaign['brand_all'] == 1) echo "style='display:none;'"; ?>>
											<label class="col-md-6">Product:</label>
											<div class="col-md-6" id="div_product_campaign_p_<?php echo $k; ?>">
											<?php											
												$sql="SELECT productname FROM tbl_product WHERE id = ".$row_campaign['campaign_free_product'][$l]['c_product_id'];
												$result1 = mysqli_query($con,$sql);
												$product = mysqli_fetch_assoc($result1);										
											?>
											<input type="text" name="product_campaign_p_<?php echo $k; ?>" id="product_campaign_p_<?php echo $k; ?>" readonly value="<?=fnStringToHTML($product['productname']);?>" class="form-control">
											</div>
										</div><!-- /.form-group -->		
										
										<div class="form-group" <?php if($row_campaign['brand_all'] == 1) echo "style='display:none;'"; ?>>
											<label class="col-md-6">Variant:</label>
											<div class="col-md-6" id="div_variant_campaign_p_<?php echo $k; ?>">
												<?php											
												if($row_campaign['campaign_free_product'][$l]['c_p_measure_id']!="") {
													$sql="SELECT * from `tbl_product_variant` where id = ".$row_campaign['campaign_free_product'][$l]['c_p_measure_id'];
													$result1 = mysqli_query($con,$sql);
													$row = mysqli_fetch_assoc($result1);	
													$exp_variant1 = $row['variant_1'];
													$imp_variant1= split(',',$exp_variant1);
													
													if($exp_variant1 != '')
													{
														$sql1="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant1[1];
														$result2 = mysqli_query($con,$sql1);
														$row_prd_variant1 = mysqli_fetch_array($result2);
														$combine1 = $row['id']." ".$imp_variant1[0]." ".$row_prd_variant1['id'];
														$combine1_display = $imp_variant1[0]." ".$row_prd_variant1['unitname'];							
													}
													$exp_variant2 = $row['variant_2'];
													$imp_variant2= split(',',$exp_variant2);
													if($exp_variant2 != '')
													{
														$sql2="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant2[1];
														$result3 = mysqli_query($con,$sql2);
														$row_prd_variant2 = mysqli_fetch_array($result3);
														$combine2 = $row['id'];	
														$selected = "";		
														
														if($combine2==$row_campaign['campaign_free_product'][$l]['c_p_measure_id']){
															$selected = "selected";
														}					
														$display_value = $combine2_display;
													}
													$display_value = $combine2_display.$combine1_display;
												} ?>
											</select>
											<input type="text" name="variant_campaign_p_<?php echo $k; ?>" id="variant_campaign_p_<?php echo $k; ?>" readonly value="<?=$display_value;?>" class="form-control">		
											</div>
										</div><!-- /.form-group -->	
										<div class="form-group" id="variant_div">
											<label class="col-md-6">No Of Products:<span class="mandatory">*</span></label>
											<div class="col-md-6" id="div_left_count">
											 <select name="left_count" id="left_count" class="form-control" disabled>
													<?php $left_count = $row_campaign['campaign_free_product'][$l]['left_count'];?>
													<option value="<?php if($left_count!=0) { echo $left_count;}else{ echo 1;} ?>">
														<?php if($left_count!=0) { echo $left_count;}else{ echo 1;} ?>
													</option>	
													
												</select>
											</div>
										</div><!-- /.form-group -->
										<div class="form-group" <?php if($row_campaign['brand_all'] == 1){ echo "style='display:block;'"; }else{ echo "style='display:none;'"; }?>>
											<label class="col-md-6">This campaign is applicable to all Brands.</label>
										</div>
										</div>									
										<div class="col-sm-5">
										<h4 style="margin-top:30px;"><b>Free Product</b></h4>
										<hr />
										<div class="form-group">
											<label class="col-md-3">Brand:</label>
											<div class="col-md-6" id="div_brand_free_p_<?php echo $k; ?>">
											<?php	
												$sql="SELECT name FROM tbl_brand WHERE id = ".$row_campaign['campaign_free_product'][$l]['f_p_brand_id'];
												$result1 = mysqli_query($con,$sql);
												$brand_free = mysqli_fetch_assoc($result1);	
											?>
											<input type="text" name="brand_free_p_<?php echo $k; ?>" id="brand_free_p_<?php echo $k; ?>" readonly value="<?=fnStringToHTML($brand_free['name']);?>" class="form-control">
											</div>
										</div><!-- /.form-group -->
										<div class="form-group">
											<label class="col-md-3">Category:</label>
											<div class="col-md-6" id="div_category_free_p_<?php echo $k; ?>">
											<?php											
												$sql="SELECT categorynm FROM tbl_category WHERE id = ".$row_campaign['campaign_free_product'][$l]['f_p_category_id'];
												$result1 = mysqli_query($con,$sql);
												$category_free = mysqli_fetch_assoc($result1);										
											?>
											<input type="text" name="category_free_p_<?php echo $k; ?>" id="category_free_p_<?php echo $k; ?>" readonly value="<?=fnStringToHTML($category_free['categorynm']);?>" class="form-control">
											</div>
										</div><!-- /.form-group -->
										<div class="form-group">
											<label class="col-md-3">Product:</label>
											<div class="col-md-6" id="div_product_free_p_<?php echo $k; ?>">
											<?php											
												$sql="SELECT productname FROM tbl_product WHERE id = ".$row_campaign['campaign_free_product'][$l]['f_product_id'];
												$result1 = mysqli_query($con,$sql);
												$product_free = mysqli_fetch_assoc($result1);										
											?>
											<input type="text" name="product_free_p_<?php echo $k; ?>" id="product_free_p_<?php echo $k; ?>" readonly value="<?=fnStringToHTML($product_free['productname']);?>" class="form-control">	
											</div>
										</div><!-- /.form-group -->		
										
										<div class="form-group">
											<label class="col-md-3">Variant:</label>
											<div class="col-md-6" id="div_variant_free_p_<?php echo $k; ?>">
											<?php											
											if($row_campaign['campaign_free_product'][$l]['f_product_id']!="") {
												$sql="SELECT * from `tbl_product_variant` where id = ".$row_campaign['campaign_free_product'][$l]['f_p_measure_id'];
												$result1 = mysqli_query($con,$sql);
												$row = mysqli_fetch_assoc($result1);
												$exp_variant1 = $row['variant_1'];
												$imp_variant1= split(',',$exp_variant1);
												if($exp_variant1 != '')
												{
													$sql1="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant1[1];
													$result2 = mysqli_query($con,$sql1);
													$row_prd_variant1 = mysqli_fetch_array($result2);
													$combine1 = $row['id']." ".$imp_variant1[0]." ".$row_prd_variant1['id'];
													$combine1_display = $imp_variant1[0]." ".$row_prd_variant1['unitname'];
												}
												$exp_variant2 = $row['variant_2'];
												$imp_variant2= split(',',$exp_variant2);
												if($exp_variant2 != '')
												{
													$sql2="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant2[1];
													$result3 = mysqli_query($con,$sql2);
													$row_prd_variant2 = mysqli_fetch_array($result3);
													$combine2 = $row['id'];					
													$selected = "";					
													if($combine2==$row_campaign['campaign_free_product'][$l]['f_p_measure_id']){
														$selected = "selected";
													}
													$display_value = $combine2_display;	
												}
												$display_value_free = $combine2_display.$combine1_display;
											}
											 ?>											
											<input type="text" name="variant_free_p_<?php echo $k; ?>" id="variant_free_p_<?php echo $k; ?>" readonly value="<?=$display_value_free;?>" class="form-control">
											</div>
										</div><!-- /.form-group -->		
										<div class="form-group" id="variant_div">
											<label class="col-md-3">No Of Products:<span class="mandatory">*</span></label>
											<div class="col-md-6" id="div_right_count">
												<select name="right_count" id="right_count" class="form-control" disabled>
													<?php $right_count = $row_campaign['campaign_free_product'][$l]['right_count'];?>
													<option value="<?php if($right_count!=0) { echo $right_count;}else{ echo 1;} ?>">
														<?php if($right_count!=0) { echo $right_count;}else{ echo 1;} ?>
													</option>	
													
												</select>
											</div>
										</div><!-- /.form-group -->
									</div>							
									</div>	
									</div><hr/>    
									<?php $l++; } ?>
								</div>										
							</div>  
							<div id="by_weight_div" <?php if($row_campaign['campaign_type'] == 'by_weight'){?>style="display:block;"<?php }else{ ?>style="display:none;" <?php } ?> >
								<div><input type="hidden" name="total_element_free_wt" id="total_element_free_wt" value='<?php echo $row_campaign['campaign_free_product_count'];?>'></div>
								<?php include "campaign_edit_com_area_section.php";	 ?>
								<hr/>
								<?php 
								$l = 0;
								for($k = 1; $k<=$row_campaign['campaign_free_product_count']; $k++){?>
								<div id="weight_freecont">
									<div id="weight_free_pro_1">
									<div class="row free">
										<div class="col-sm-6">
										<h4 style="margin-top:30px;"><b>Campaign Product</b></h4>
										<hr />
																				
										<div class="form-group">
											<label class="col-md-6">Weight:</label>
											<div class="col-md-5">
												<div id="nopad1" style="float:left;">
												<input class="nopad1" style="width:92px;" type="text" name="campaign_product_wt_qty_1" id="campaign_product_wt_qty_1" disabled readonly value="<?=$row_campaign['campaign_free_product'][$l]['c_weight'];?>" class="form-control">
												&nbsp;&nbsp;</div>
												<div style="float:left;"  id="div_variant_unit_campaign_p_1">
												Kg
												</div>
												 <div class="clearfix"></div> 
											</div>
										</div><!-- /.form-group -->		
										</div>
									
										<div class="col-sm-5">
										<h4 style="margin-top:30px;"><b>Free Product</b></h4>
										<hr />
										<div class="form-group">
											<label class="col-md-3">Brand:</label>
											<div class="col-md-6" id="div_brand_free_p_1">
											 <?php	
												$sql="SELECT name FROM tbl_brand WHERE id = ".$row_campaign['campaign_free_product'][$l]['f_p_brand_id'];
												$result1 = mysqli_query($con,$sql);
												$brand_free = mysqli_fetch_assoc($result1);	
											?>
											<input type="text" name="brand_free_p_<?php echo $k; ?>" id="brand_free_p_<?php echo $k; ?>" readonly value="<?=fnStringToHTML($brand_free['name']);?>" class="form-control">	
											</div>
										</div><!-- /.form-group -->
								
										<div class="form-group">
											<label class="col-md-3">Category:</label>
											<div class="col-md-6" id="div_category_free_p_1">
											 <?php											
												$sql="SELECT categorynm FROM tbl_category WHERE id = ".$row_campaign['campaign_free_product'][$l]['f_p_category_id'];
												$result1 = mysqli_query($con,$sql);
												$category_free = mysqli_fetch_assoc($result1);										
											?>
											<input type="text" name="category_free_p_<?php echo $k; ?>" id="category_free_p_<?php echo $k; ?>" readonly value="<?=fnStringToHTML($category_free['categorynm']);?>" class="form-control">
											</div>
										</div><!-- /.form-group -->
								
										<div class="form-group">
											<label class="col-md-3">Product:</label>
											<div class="col-md-6" id="div_product_free_p_1">
											 <?php											
												$sql="SELECT productname FROM tbl_product WHERE id = ".$row_campaign['campaign_free_product'][$l]['f_product_id'];
												$result1 = mysqli_query($con,$sql);
												$product_free = mysqli_fetch_assoc($result1);										
											?>
											<input type="text" name="product_free_p_<?php echo $k; ?>" id="product_free_p_<?php echo $k; ?>" readonly value="<?=fnStringToHTML($product_free['productname']);?>" class="form-control">
											</div>
										</div><!-- /.form-group -->		
										
										<div class="form-group">
											<label class="col-md-3">Variant:</label>
											<div class="col-md-6" id="div_variant_free_p_<?php echo $k; ?>">											 
											<?php											
											if($row_campaign['campaign_free_product'][$l]['f_product_id']!="") {
												$sql="SELECT * from `tbl_product_variant` where id = ".$row_campaign['campaign_free_product'][$l]['f_p_measure_id'];
												$result1 = mysqli_query($con,$sql);
												$row = mysqli_fetch_assoc($result1);													
												$exp_variant1 = $row['variant_1'];
												$imp_variant1= split(',',$exp_variant1);
													
												if($exp_variant1 != '')
												{
													$sql1="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant1[1];
													$result2 = mysqli_query($con,$sql1);
													$row_prd_variant1 = mysqli_fetch_array($result2);
													$combine1 = $row['id']." ".$imp_variant1[0]." ".$row_prd_variant1['id'];
													$combine1_display = $imp_variant1[0]." ".$row_prd_variant1['unitname'];															
												}
												$exp_variant2 = $row['variant_2'];
												$imp_variant2= split(',',$exp_variant2);
													
												if($exp_variant2 != '')
												{
													$sql2="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant2[1];
													$result3 = mysqli_query($con,$sql2);
													$row_prd_variant2 = mysqli_fetch_array($result3);
													$combine2 = $row['id'];	
													$selected = "";		
													
													if($combine2==$row_campaign['campaign_free_product'][$l]['f_p_measure_id']){
														$selected = "selected";
													}
													
													$display_value = $combine2_display;	
												}
												$display_value_free = $combine2_display." (".$combine1_display.")";
											}
											 ?>											
											<input type="text" name="variant_free_p_<?php echo $k; ?>" id="variant_free_p_<?php echo $k; ?>" readonly value="<?=$display_value_free;?>" class="form-control">
											</div>
									</div>												
									</div>	
									</div>
								</div>
							</div>
								<?php $l++; } ?>							   	
						</div>	

           <div class="clearfix"></div> 
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                <a href="campaign.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group --> 
          </form>          
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>
<!--<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>-->
<script> 
function validateForm(){
	var campaign_name = $("#campaign_name").val();
	var campaign_start_date = $("#campaign_start_date").val();
	var campaign_end_date = $("#campaign_end_date").val();
	
	if(campaign_end_date == '')
	{
		alert('Please enter End Date');
		$('#campaign_end_date').focus();
		return false;
	}	
	if(campaign_start_date != '' && campaign_end_date != '')
	{
		var date_validate = compaire_dates(campaign_start_date,campaign_end_date);
		if(date_validate == 1)
		{
			alert("'End Date' should be greater than 'Start Date'.");
			return false;
		}
	}
	
}
$('.date-picker1').datepicker({	
	autoclose: true
});
 $('.date-picker3').datepicker({
	rtl: Metronic.isRTL(),
	orientation: "auto",
	startDate: 'd',
	autoclose: true
});
</script>