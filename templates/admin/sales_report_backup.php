<!-- BEGIN HEADER -->
<?php include "../includes/header.php";

include "../includes/testManage.php";
include "../includes/userConfigManage.php";
$testObj 	= 	new testManager($con,$conmain);
$userconfObj 	= 	new userConfigManager($con,$conmain);

function weekDayToTime($week, $year, $dayOfWeek = 0) {
	$dayOfWeekRef = date("w", mktime (0,0,0,1,4,$year));
	if ($dayOfWeekRef == 0) $dayOfWeekRef = 7;
	$resultTime = mktime(0,0,0,1,4,$year) + ((($week - 1) * 7 + ($dayOfWeek - $dayOfWeekRef)) * 86400);
	$resultTime = cleanTime($resultTime);  //Cleaning daylight saving time hours
	return $resultTime;
};

function cleanTime($time) {
	//This function strips all hours, minutes and seconds from time.
	//For example useful of cleaning up DST hours from time
	$cleanTime = mktime(0,0,0,date("m", $time),date("d", $time),date("Y", $time));
	return $cleanTime;
}
function weeks($year)
{   
	return date("W",mktime(0,0,0,12,28,$year));
}
?>
<!-- END HEADER -->
 
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Reports"; $activeMenu = "SalesReport";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Sales Report History <small>Sales Report</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Sales Report</a>
					</li>
				</ul>

			</div>
			<!-- END PAGE HEADER-->
			
			<div class="row">
				<div class="col-md-12"> 
				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">Search Criteria</div>
						</div>
						<div class="portlet-body">					
						
						<form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">
							
							<div class="form-group">
								<label class="col-md-3">Report Type:</label>
								<div class="col-md-4">
									<input type="radio" name="reportType" id="reportType_daily" value="daily" checked onclick="fnChangeReportType('daily');"> Daily 
									&nbsp;&nbsp;
									<input type="radio" name="reportType" id="reportType_weekly" value="weekly" onclick="fnChangeReportType('weekly');"> Weekly 
									&nbsp;&nbsp;
									<input type="radio" name="reportType" id="reportType_monthly" value="monthly" onclick="fnChangeReportType('monthly');"> Monthly
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group" id="divDaily" style="display:none;">
								<label class="col-md-3">Select date:</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" class="form-control  date date-picker1" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years" name="frmdate" id="frmdate" value="<?php echo date('d-m-Y');?>">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<!-- /input-group -->								 
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group" id="divWeekly" style="display:none;">
								<label class="col-md-3">Select Week:</label>
								<div class="col-md-4">
									<select name="drpWeeklyOption" id="drpWeeklyOption" class="form-control">
									
										<? $yearStart = 2017; $yearEnd = date("Y");
										for($year=$yearStart;$year<=$yearEnd;$year++) {
											for($i=1;$i<=weeks($year);$i++)
											{
												$start = weekDayToTime($i, $year);
												$end   = cleanTime(518400 + $start);
												$selected = '';
												if(weekDayToTime(date("W"), date("Y")) == $start)
												{
													$selected = "selected = 'selected'";
												}
												$startDate = strftime("%d-%m-%Y", $start);
												$endDate   = strftime("%d-%m-%Y", $end);
												
												echo "<option value='".$startDate . "::" . $endDate . "' $selected>".strftime("%d-%m-%Y", $start)." To ".strftime("%d-%m-%Y", $end)."</option> \n";

												if($selected!="")
													break;
											}
										} ?>
										</select> 
									<!-- /input-group -->								 
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group" id="divMonthly" style="display:none;">
								<label class="col-md-3">Select Month:</label>
								<div class="col-md-4">
									<select name="drpMonthlyOption" id="drpMonthlyOption" class="form-control">
										<?  
										$yearStart = 2017; $yearEnd = date("Y");
										$currentMonth = date("m-Y") ;
										for($year=$yearStart;$year<=$yearEnd;$year++) { 
											for ($m=1; $m<=12; $m++) {
												$optionValue = "";
												if($m<10)
													$optionValue="0".$m . '-' . $year;
												else 
													$optionValue=$m . '-' . $year;
												
												$selected = "";
												if($currentMonth==$optionValue)
													$selected = " selected = 'selected'";
												
												echo '<option value="' . $optionValue . '" '.$selected.'>' . date('M', mktime(0,0,0,$m)) . '-' . $year . '</option>';
												if($currentMonth==$optionValue)
													break;
											}
										} ?>
										</select> 
									<!-- /input-group -->							 
								</div>
							</div><!-- /.form-group -->

							 <div class="form-group">
                            <label class="col-md-3">Select User Type:<span class="mandatory">*</span></label>
                            <div class="col-md-4">
                            <select name="user_type" id="user_type" 
                            class="form-control" onchange="getUserID(this)" required>
                            <option value="" selected>-- Select --</option>
							<?php   
                            $seesion_user_id=$_SESSION[SESSION_PREFIX.'user_id'];                            
							 $result1 = $testObj->getTest();
                            // $result1 = $testObj->getChieldsFromUsertree($seesion_user_id);
							//echo "<pre>";print_r($result1);
                             //var_dump($result1);
                            //exit();
							 while ($row = mysqli_fetch_array($result1)){ ?>
							  <option value="<?php echo $row['id'];?>_<?php echo $row['depth'];?>" ><?php echo $row['user_type'];?></option>
							 <?php  } ?> 
                            </select>
                            </div>
                        </div>                      
						<div id="parent_hierarchy_div"></div>

							
							<div class="form-group" style="display:none">
								<label class="col-md-3">State:</label>
								<div class="col-md-4">
								<select name="dropdownState" id="dropdownState" class="form-control" onChange="fnShowCity(this.value)">
									<option value="">-Select-</option>
									<?php
									$sql="SELECT id,name FROM tbl_state where country_id=101 order by name";
									$result = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result))
									{
										$cat_id=$row['id'];
										echo "<option value='$cat_id'>" . $row['name'] . "</option>";
									} ?>
									</select>
								</div>
							</div>	
							
							<div class="form-group" id="city_div" style="display:none;">
							  <label class="col-md-3">District:</label>
							  <div class="col-md-4" id="div_select_city">
							  <select name="dropdownCity" id="dropdownCity" data-parsley-trigger="change" class="form-control">
								<option selected value="">-Select-</option>										
								</select>
							  </div>
							</div><!-- /.form-group -->
							
							<div class="form-group" id="area_div" style="display:none;">
							  <label class="col-md-3">Taluka:</label>
							  <div class="col-md-4" id="div_select_area">
							  <select name="dropdownSuburbs" id="dropdownSuburbs"  class="form-control">
								<option selected value="">-Select-</option>									
								</select>
							  </div>
							</div><!-- /.form-group --> 
							
							<div class="form-group" id="subarea_div" style="display:none;">
							  <label class="col-md-3">Subarea:</label>
							  <div class="col-md-4" id="div_select_subarea">
							  <select name="subarea" id="subarea" data-parsley-trigger="change" class="form-control" >
								<option selected value="">-Select-</option>									
								</select>
							  </div>
							</div><!-- /.form-group -->  
							
							<div class="form-group" >
								<label class="col-md-3">Shops:</label>
								<div class="col-md-4" id="divShopdropdown">
								 <select name="dropdownshops" id="dropdownshops" class="form-control">
									<option value="">-Select-</option>
									<?php											
									$sql="SELECT name , id FROM tbl_shops ORDER BY name";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										echo "<option value='".$row["id"]."'>" . fnStringToHTML($row["name"]) . "</option>";
									} ?>
								</select>
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group">
								<label class="col-md-3">Brand:</label>
								<div class="col-md-4">
								 <select name="dropdownbrands" id="dropdownbrands" class="form-control" onchange="fnShowCategories(this)">
									<option value="">-Select-</option>
									<?php											
									$sql="SELECT name , id FROM tbl_brand order by name";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										echo "<option value='".$row["id"]."'>" . fnStringToHTML($row["name"]) . "</option>";
									} ?>
								</select>
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group">
								<label class="col-md-3">Category:</label>
								<div class="col-md-4" id="divCategoryDropDown">
								 <select name="dropdownCategory" id="dropdownCategory" class="form-control" onchange="fnShowProducts(this)">
									<option value="">-Select-</option>
									<?php											
									$sql="SELECT categorynm , id FROM tbl_category order by categorynm";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										echo "<option value='".$row["id"]."'>" . fnStringToHTML($row["categorynm"]) . "</option>";
									}
									?>
								</select>
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group">
								<label class="col-md-3">Product:</label>
								<div class="col-md-4" id="divProductdropdown">
								 <select name="dropdownProducts" id="dropdownProducts" class="form-control">
									<option value="">-Select-</option>
									<?php											
									$sql="SELECT productname , id ,catid  FROM tbl_product order by productname";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
									$prodid=$row["id"];
										$catid=$row["catid"];
										$sqlprd="SELECT variant_1, variant_2 FROM `tbl_product_variant` WHERE productid = '$prodid' ";
										$resultprd = mysqli_query($con,$sqlprd);
										$rowprd = mysqli_fetch_array($resultprd);
										$exp_variant1 = $rowprd['variant_1'];
										$imp_variant1= explode(',',$exp_variant1);
										$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
										$resultunit = mysqli_query($con,$sql);
										$rowunit = mysqli_fetch_array($resultunit);
										$variant_unit1 = $rowunit['unitname'];
										
										$sql="SELECT categorynm , id FROM `tbl_category` WHERE id='$catid'";
										$resultcat = mysqli_query($con,$sql);
										$rowcat = mysqli_fetch_array($resultcat);
										$catname = $rowcat['categorynm'];
									echo "<option value='".$row["id"]."'>" .$catname."-". fnStringToHTML($row["productname"]) ."-".$variant_unit1. "</option>";
									}
									?>
								</select>
								</div>
							</div><!-- /.form-group -->

							<div class="form-group">
								<div class="col-md-4 col-md-offset-3">
									<button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>
									
									<button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnChangeReportType('daily');">Reset</button>
								</div>
							</div><!-- /.form-group -->
						
						</form>							
						
						</div>
					   <div class="clearfix"></div>
					</div>
					
					<div id="divReportHTML"></div>
			
		</div>			
	</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>

<script>
function CallAJAX(url,assignDivName) {
	if (window.XMLHttpRequest)
	{
		var xmlhttp=new XMLHttpRequest();
	} else {
		var xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById("" + assignDivName +"").innerHTML	=	xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET",url,true);
	xmlhttp.send();	
};


function getUserID(UserID) {
	var UserID =UserID.value;
	var user_typej = $("#user_type option:selected").text();	
	var usertype_info = UserID.split('_');	
	var depth=usertype_info[1];	
	var usertype_id=usertype_info[0];	
	if (depth > 2){
		$('#parent_hierarchy_div').empty();
		console.log("daksjdkj:"+depth);
		var url = "conf_get_childs.php"; 
			jQuery.ajax({
				url: url,
				method: 'POST',
				data: 'id='+usertype_id+'&getflag=getparent',
				async: false
			}).done(function (response) {
				 obj = JSON.parse(response);
				console.log(obj.parent_info);
				for(var i=obj.parent_info.length;i>0;i--){
					if (typeof obj.parent_info[i] != 'undefined'){
						console.log(obj.parent_info[i].user_type);
						var user_type_parent=obj.parent_info[i].user_type;
						var user_depth='user_depth_'+obj.parent_info[i].parent_id;
						var child_options;
						var url = "conf_get_childs.php"; 
						jQuery.ajax({
							url: url,
							method: 'POST',
							data: 'depth='+i+'&user_typej='+user_type_parent+'&getflag=getoption',
							async: false
						}).done(function (response) {
							console.log(response);
							child_options=response;				
						}).fail(function () { });
						var functioncallvalue="";
						if(i!=1){
							var temp_fornextid=i-1;
							 functioncallvalue="onchange='getUserID1(this,"+obj.parent_info[temp_fornextid].parent_id+","+i+")'";
						}
												
						if(child_options!='no_parent'){
							$("#parent_hierarchy_div").append("<div class='form-group'>"+
							"<label class='col-md-3'>Select Parent "+user_type_parent+":<span class='mandatory'>*</span></label>"+
							"<div class='col-md-4'>"+
							"<select name='"+user_depth+"[]' id='parent_user_"+obj.parent_info[i].parent_id+"' class='form-control' "+
							" "+ functioncallvalue +">"+	 
							"<option value='' selected>-- Select --</option> "+child_options+
							"</select></div>"+
							"</div> ");
						}else{
							alert("You can not add this user type as there is no parent exist!");
							$("#user_type").val("");
						}
					}					
					
				}				
				//child_options=response.;				
			}).fail(function () { });		
    }else{
		$('#parent_hierarchy_div').empty();
	}
}

function getUserID1(UserID1,nextid,i) {
	var UserID1 =UserID1.value;
	var usertype_info = UserID1.split('_');	
	var depth=usertype_info[1];	
	var user_id=usertype_info[0];
	//console.log($('#parent_user_'+nextid+'').id());
	
	var child_options;
	var url = "conf_get_childs.php"; 
	jQuery.ajax({
		url: url,
		method: 'POST',
		data: 'user_id='+user_id+'&getflag=onchange_getchild',
		async: false
	}).done(function (response) {
		console.log(response);
		child_options=response;				
	}).fail(function () { });
	var functioncallvalue="";
	if(i!=1){
		var temp_fornextid=i-1;
		 functioncallvalue="onchange='getUserID1(this,"+obj.parent_info[temp_fornextid].parent_id+","+i+")'";
	}					
	if(child_options!='no_parent'){
		$('#parent_user_'+nextid+'')
			.find('option')
			.remove()
			.end()
			.append("<option value='' selected>-- Select dfsdfs --</option>"+child_options+"");			
	}else{
		alert("You can not add this user type as there is no parent exist!");
	}	
}


function fnShowCategories(id) {
	var url = "getCategoryDropdown.php?brandid="+id.value;
	CallAJAX(url,"divCategoryDropDown");
} 

function fnShowProducts(id) {
	var url = "getProductDropdown.php?cat_id="+id.value;
	CallAJAX(url,"divProductdropdown");
}

function ShowReport() {
	
	var reportType = $('input[name=reportType]:checked', '#frmsearch').val();
	var url = "dailyReport.php";
	switch(reportType) {
		case "daily" :
			url = "dailyReport.php";
		break;
		case "weekly" :
			url = "weeklyReport.php";
		break;
		case "monthly" :
			url = "monthlyReport.php";
		break;
	}
	
	var data = $('#frmsearch').serialize();
	
    jQuery.ajax({
		url: url,
		method: 'POST',
		data: data,
		async: false
	}).done(function (response) {
		$("#divReportHTML").show();
		$('#divReportHTML').html(response);
	}).fail(function () { });
	
	return false;
}

function ExportToExcel() {
	
	var reportType = $('input[name=reportType]:checked', '#frmsearch').val();
	var url = "dailyReport.php";
	switch(reportType) {
		case "daily" :
			url = "dailyReport.php";
		break;
		case "weekly" :
			url = "weeklyReport.php";
		break;
		case "monthly" :
			url = "monthlyReport.php";
		break;
	} 
	$("#frmsearch").attr("action", url + '?actionType=excel');
	$('#frmsearch').submit();
	
	//alert('dd');
	//var data = $('#frmsearch').serialize();
    /*jQuery.ajax({
		url: url + '?actionType=excel',
		method: 'POST',
		data: data,
		async: false
	}).done(function (response) {
		$("#divReportHTML").show();
		$('#divReportHTML').html(response);
	}).fail(function () { });*/
	
	//return false;
}

$('.date-picker1').datepicker({
	rtl: Metronic.isRTL(),
	orientation: "left",
	endDate: "<?php echo date('d-m-Y');?>",
	autoclose: true
});

fnChangeReportType('daily');
 
function takeprint() {
	var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
	var divContents = '<style type="text/css">@page{size: landscape;}\	table { border-collapse: collapse; }\
	table, th, td {  border: 1px solid black; }\
	body { font-family: "Open Sans", sans-serif;\
	background-color:#fff;\
	font-size: 11px;\
	direction: ltr;}</style>' + $("#dvtblResonsive").html();
	
	
	if(isIE == true){
		var printWindow = window.open('', '', '');
		printWindow.document.write(divContents);
		printWindow.focus();
		printWindow.document.execCommand("print", false, null);
	}else{
		$('<iframe>', {
			name: 'myiframe',
			class: 'printFrame'
		}).appendTo('body').contents().find('body').html(divContents);
		window.frames['myiframe'].focus();
		window.frames['myiframe'].print();		
		setTimeout(
		function() 
		{
			$(".printFrame").remove();
		}, 1000);
	}
	
};

function fnChangeReportType(reportType) {
	$("#divReportHTML").hide();
	switch(reportType) {
		case "daily":
			$("#divDaily").show();
			$("#divWeekly").hide();
			$("#divMonthly").hide();
			break;
		case "weekly":
			$("#divDaily").hide();
			$("#divWeekly").show();
			$("#divMonthly").hide();
			break;
		case "monthly":
			$("#divDaily").hide();
			$("#divWeekly").hide();
			$("#divMonthly").show();
			break;
	}
}

function fnShowCity(id_value) {
	
	$("#city_div").show();	
	$("#area").html('<option value="">-Select-</option>');	
	$("#subarea").html('<option value="">-Select-</option>');	
	
	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=dropdownCity";
	CallAJAX(url,"div_select_city");
	
	FnGetShopsDropdown(id_value, "","","");
}

function FnGetSuburbDropDown(id) {
	$("#area_div").show();		
	$("#subarea").html('<option value="">-Select-</option>');	
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=dropdownSuburbs&function_name=FnGetSubareaDropDown";
	CallAJAX(url,"div_select_area");
	
	var state_id = $("#dropdownState").val();
	if(state_id==undefined)
		state_id = "";
	var city_id = $("#dropdownCity").val();
	if(city_id==undefined)
		city_id = "";
	 
	FnGetShopsDropdown(state_id, id.value,"","");
}

function FnGetSubareaDropDown(id) {	

	var suburb_str = $("#area").val();	
	$("#subarea_div").show();	
	var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea&function_name=FnShowShopsDropdown";
	CallAJAX(url,"div_select_subarea");
	
	var state_id = $("#dropdownState").val();
	if(state_id==undefined)
		state_id = "";
	var city_id = $("#dropdownCity").val();
	if(city_id==undefined)
		city_id = "";
	var suburb_id = $("#dropdownSuburbs").val();
	if(suburb_id==undefined)
		suburb_id = ""; 
	 
	FnGetShopsDropdown(state_id,city_id,id.value,"");
}

function FnShowShopsDropdown(obj) {
	var state_id = $("#dropdownState").val();
	if(state_id==undefined)
		state_id = "";
	var city_id = $("#dropdownCity").val();
	if(city_id==undefined)
		city_id = "";
	var suburb_id = $("#dropdownSuburbs").val();
	if(suburb_id==undefined)
		suburb_id = "";
	var subarea_id = $("#subarea").val();
	if(subarea_id==undefined)
		subarea_id = "";
	
	FnGetShopsDropdown(state_id,city_id,suburb_id,subarea_id);
}

function FnGetShopsDropdown(state_id,city_id,suburb_id,subarea_id) {
	var url = "getShopDropdownByAddress.php?state_id="+state_id + "&city_id="+city_id + "&suburb_id="+suburb_id + "&subarea_id="+subarea_id;
	CallAJAX(url,"divShopdropdown");
}

//$("#subarea").change(function() { alert("The text has been changed.");FnShowShopsDropdown()});

</script>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>