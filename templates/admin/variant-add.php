<!-- BEGIN HEADER -->
<?php include "../includes/header.php"?>
<!-- END HEADER -->
<?php
if(isset($_POST['submit']))
{
	$name=fnEncodeString($_POST['name']);
	$sql1=mysqli_query($con,"INSERT INTO `tbl_variant` (`name`) VALUES ('$name')");
	$sql_id= mysqli_insert_id($con);
	$ArrunitName = fnEncodeString($_POST['unitname']);
	foreach ($ArrunitName as $unitname) {
		$sql3=mysqli_query($con,"INSERT INTO `tbl_units` (variantid,unitname) VALUES ('$sql_id','$unitname')");	
	}	
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
   <?php
	$activeMainMenu = "ManageProducts"; $activeMenu = "Variant";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Variant
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="variant.php">Variant Products</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Variant</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Variant
							</div>
							
						</div>
						<div class="portlet-body">
                        
                            <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>                  
                          <form class="form-horizontal" data-parsley-validate="" role="form" action="variant-add.php" method="post">
       
            <div class="form-group">
              <label class="col-md-3">Variant Name:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="name" 
				 placeholder="Enter Variant Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter variant name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				data-parsley-pattern="^(?!\s)[a-zA-Z ]*$"
				data-parsley-pattern-message="Please enter alphabets only"
				class="form-control">
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <label class="col-md-3">Unit:<span class="mandatory">*</span></label>

              <div class="col-md-4">
			  <select name="unitname[]" id="unitname" 
			  data-parsley-trigger="change"				
              data-parsley-required="#true" 
              data-parsley-required-message="Please select unit"
			  class="form-control">
			  <option disabled selected>-Select-</option>
			  <?php
$sql="SELECT  * FROM `tbl_units`";
$result1 = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result1))
{
$id=$row['id'];
echo "<option value='$id'>" . $row['unitname'] . "</option>";
}
?>
			  </select>
              </div>
              
               
              <div class="col-md-2">
                <a type="button" class="btn btn-primary btn-sm" id="btn1">Add More</a>
              </div>
            
            </div><!-- /.form-group -->
            
           <div id="demo">
		   </div>    
            
			
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                <a href="variant.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group -->
            
            

          </form>  
                            
                            
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->

<!-- END JAVASCRIPTS -->
<script>
$(document).ready(function(){
    $("#btn1").click(function(){
$.ajax({
   url: 'fetch_unit.php',
   success: function(data){
     $("#demo").append(data);
   }
 });
     });
});

</script>  
</body>
<!-- END BODY -->
</html>
