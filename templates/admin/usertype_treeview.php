<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://cdn.bootcss.com/twitter-bootstrap/3.0.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="easyTree/easyTree2.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="easyTree/easyTree.js"></script>
    <style>
        body {
            background: #eee;
        }
    </style>
</head>
<body>
<div class="col-md-3">
    <h3 class="text-success">A demo of Easy Tree</h3>
    <div class="easy-tree">
        <ul>
            <li>Parent 1</li>
            <li>Parent 2</li>
            <li>Parent 3
                <ul>
                    <li>Child 1</li>
                    <li>Child 2
                        <ul>
                            <li>Grand-child 1</li>
                            <li>Grand-child 2</li>
                            <li>Grand-child 3</li>
                            <li>Grand-child 4</li>
                        </ul>
                    </li>
                    <li>Child 3</li>
                    <li>Child 4</li>
                </ul>
            </li>
            <li>Parent 4
                <ul>
                    <li>Parent 4 Child 1</li>
                    <li>Parent 4 Child 2</li>
                    <li>Parent 4 Child 3</li>
                    <li>Parent 4 Child 4
                        <ul>
                            <li>Grand-child 1</li>
                            <li>Grand-child 2</li>
                            <li>Grand-child 3</li>
                            <li>Grand-child 4</li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<script>
    (function ($) {
        function init() {
            $('.easy-tree').EasyTree({
                addable: true,
                editable: true,
                deletable: true
            });
        }

        window.onload = init();
    })(jQuery)
</script>
</body>
</html>

