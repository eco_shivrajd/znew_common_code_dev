<!-- BEGIN HEADER -->
<?php include "../includes/header.php"; 
include "../includes/userManage.php";
include "../includes/shopManage.php";
$shopObj 	= 	new shopManager($con,$conmain);?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Shops";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Shops
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					 
					<li>
						<i class="fa fa-home"></i>
						<a href="shops.php">Shops</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Shop</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Shops
							</div>
							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
<?php
if(isset($_POST['submit']))
{
	$shopObj->addShopDetails();
	echo '<script>alert("Shop added successfully.");location.href="shops.php";</script>';
}
?>  
<form class="form-horizontal" data-parsley-validate="" role="form" method="post" action="shops-add.php">         
            <div class="form-group">
              <label class="col-md-3">Shop Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text" 
				placeholder="Enter Shop Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter shop name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				name="name"class="form-control">
              </div>
            </div><!-- /.form-group -->
            <?php if($_SESSION[SESSION_PREFIX."user_type"]=="Admin") { ?>
				<div class="form-group">
				<label class="col-md-3">Super Stockist:<span class="mandatory">*</span></label>
				<div class="col-md-4">
					<select name="cmbSuperStockist" id="cmbSuperStockist" 	
					data-parsley-trigger="change"				
					data-parsley-required="#true" 
					data-parsley-required-message="Please select Super Stockist"
					onchange="fnShowStockist(this)" class="form-control">						
						<?php 
						$userObj 	= 	new userManager($con,$conmain);
						$user_type="Superstockist";
						$result1 = $userObj->getLocalUserDetailsByUserType($user_type);		
						while($row = mysqli_fetch_array($result1)) 
						{
							$assign_id	=	$row['id'];
							if($parentid == $assign_id)
								$sel="SELECTED";
							else
								$sel="";
							echo "<option value='$assign_id' $sel>" . fnStringToHTML($row['firstname']) . "</option>";
						} ?>
					</select>
				</div>
				</div><!-- /.form-group -->    
				<?php } else { ?>
							<input type="hidden" name="cmbSuperStockist" id="cmbSuperStockist" value="<?=$parentid;?>">
						<?php } ?>		
				<?php if($_SESSION[SESSION_PREFIX."user_type"]!="Distributor") { ?>
						<div id="Subcategory">
							<div class="form-group">
								<label class="col-md-3">Stockist:<span class="mandatory">*</span></label>
								<div class="col-md-4">
									<select name="assign" class="form-control"
									data-parsley-trigger="change"				
									data-parsley-required="#true" 
									data-parsley-required-message="Please select Stockist">											
									<?php
									$user_type="Distributor";						
									if($parentid!="")
										$external_id = $parentid;	
									
									$result1 = $userObj->getLocalUserDetailsByUserType($user_type,$external_id);		
									while($row = mysqli_fetch_array($result1))
									{
										$assign_id=$row['id'];
										if($row1['external_id'] == $assign_id)
											$sel="SELECTED";
										else
											$sel="";
										echo "<option value='$assign_id' $sel>" . fnStringToHTML($row['firstname']) . "</option>";
									} ?>
									</select>
								</div>
							</div><!-- /.form-group -->  
						</div>
						<?php } else {?>
							<input type="hidden" name="assign" id="assign" value="<?=$row1['external_id'];?>">
						<?php } ?>
            <div class="form-group">
              <label class="col-md-3">Address:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <textarea name="address" 
				rows="4"
                placeholder="Enter Address"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter address"
				data-parsley-maxlength="200"
				data-parsley-maxlength-message="Only 200 characters are allowed"
				class="form-control"></textarea>
              </div>
            </div><!-- /.form-group -->
            <div class="form-group">
				<label class="col-md-3">State:<span class="mandatory">*</span></label>
				<div class="col-md-4">
				<select name="state"              
				data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please select state"
				class="form-control" onChange="fnShowCity(this.value)">
				<option selected disabled>-Select-</option>
				<?php
				$sql="SELECT id,name FROM tbl_state where country_id=101 order by name";
				$result = mysqli_query($con,$sql);
				while($row = mysqli_fetch_array($result))
				{
					$cat_id=$row['id'];
					echo "<option value='$cat_id'>" . $row['name'] . "</option>";
				} ?>
				</select>
				</div>
				</div>			
				<div class="form-group" id="city_div" style="display:none;">
				  <label class="col-md-3">District:<span class="mandatory">*</span></label>
				  <div class="col-md-4" id="div_select_city">
				  <select name="city" id="city" data-parsley-trigger="change" class="form-control">
					<option selected value="">-Select-</option>										
					</select>
				  </div>
				</div><!-- /.form-group -->
				<div class="form-group" id="area_div" style="display:none;">
				  <label class="col-md-3">Taluka:<span class="mandatory">*</span></label>
				  <div class="col-md-4" id="div_select_area">
				  <select name="area" id="area" data-parsley-trigger="change" class="form-control">
					<option selected value="">-Select-</option>									
					</select>
				  </div>
				</div><!-- /.form-group --> 						
				<div class="form-group" id="subarea_div" style="display:none;">
				  <label class="col-md-3">Subarea:</label>
				  <div class="col-md-4" id="div_select_subarea">
				  <select name="subarea" id="subarea" data-parsley-trigger="change" class="form-control">
					<option selected value="">-Select-</option>									
					</select>
				  </div>
				</div><!-- /.form-group --> 
 
            <div class="form-group">
              <label class="col-md-3">Contact Person 1:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" 
				placeholder="Enter Contact Person Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter contact person name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				name="contact_person"class="form-control">
              </div>
            </div><!-- /.form-group -->
			<div class="form-group">
              <label class="col-md-3">Mobile Number 1:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="mobile"
                placeholder="Enter Mobile Number"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter mobile number"
				data-parsley-maxlength="15"
				data-parsley-minlength="10"
				data-parsley-maxlength-message="Only 15 characters are allowed"
				data-parsley-minlength-message="Mobile number length should be 10 or more"
				data-parsley-pattern="^(?!\s)[0-9!@#$%^&*+_=><,./:' ]*$"
				data-parsley-pattern-message="Alphabets are not allowed"
				class="form-control">
              </div>
            </div><!-- /.form-group -->
            <div class="form-group">
              <label class="col-md-3">Contact Person 2:</label>

              <div class="col-md-4">
                <input type="text" 
				placeholder="Enter Contact Person Name"
                data-parsley-trigger="change"				
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				name="contact_person_other"class="form-control">
              </div>
            </div><!-- /.form-group -->
            
            
            
            <div class="form-group">
              <label class="col-md-3">Mobile Number 2:</label>

              <div class="col-md-4">
                <input type="text" name="mobile_number_other"
                placeholder="Enter Mobile Number"
                data-parsley-trigger="change"				
				data-parsley-maxlength="15"
				data-parsley-minlength="10"
				data-parsley-maxlength-message="Only 15 characters are allowed"
				data-parsley-minlength-message="Mobile number length should be 10 or more"
				data-parsley-pattern="^(?!\s)[0-9!@#$%^&*+_=><,./:' ]*$"
				data-parsley-pattern-message="Alphabets are not allowed"
				class="form-control">
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <label class="col-md-3">GST Shop Number:</label>
              <div class="col-md-4">
                <input type="text" name="gst_number"
                placeholder="Enter GST Shop Number"
                data-parsley-trigger="change"				
				data-parsley-maxlength="20"
				data-parsley-maxlength-message="Only 20 characters are allowed"
				class="form-control">
              </div>
            </div><!-- /.form-group -->
			
            <div class="form-group">
              <label class="col-md-3">Shop Closed On Day:</label>
              <div class="col-md-4">
			  <select class="form-control" name="closedday">
			  <option value="">-Select-</option>
			  <option value="1">Monday</option>
			  <option value="2">Tuesday</option>
			  <option value="3">Wednesday</option>
			  <option value="4">Thursday</option>
			  <option value="5">Friday</option>
			  <option value="6">Saturday</option>
			  <option value="7">Sunday</option>
			  </select>
              </div>
            </div><!-- /.form-group -->
			<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
            <div class="form-group">
              <label class="col-md-3">Shop Open Time:</label>
              <div class="col-md-4">
                <div class="input-group date" id="datetimepicker1">
                    <input type="text" class="form-control" name="opentime"  readonly>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div>
              </div>
            </div><!-- /.form-group -->			     
            <div class="form-group">
              <label class="col-md-3">Shop Closed Time:</label>
              <div class="col-md-4">
                <div class='input-group date'>
                    <input type="text" class="form-control" name="closetime" id="datetimepicker2" readonly>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div>
              </div>
            </div><!-- /.form-group -->			
            <div class="form-group">
              <label class="col-md-3">Latitude:</label>
              <div class="col-md-4">
                <input type="text" name="latitude"
                placeholder="Enter Latitude"
				class="form-control" min="-180" max="180" step="0.0000000000000001" 
                            data-parsley-trigger="change"                                       
                            >
              </div>
            </div><!-- /.form-group -->			
            <div class="form-group">
              <label class="col-md-3">Longitude:</label>
              <div class="col-md-4">
                <input type="text" name="longitude"
                placeholder="Enter Longitude"
				class="form-control" min="-180" max="180" step="0.0000000000000001" 
                            data-parsley-trigger="change"                                       
                            >
              </div>
            </div><!-- /.form-group -->	
            <div class="form-group">
							<label class="col-md-3"><b>Bank Details</b></label>
						</div>
						 <div class="form-group">
							<label class="col-md-3">Billing Name </label>
							<div class="col-md-4">
								<input type="text" 
								placeholder="Billing Name"
								data-parsley-trigger="change"
								data-parsley-maxlength="50"
								data-parsley-maxlength-message="Only 50 characters are allowed"
								name="billing_name" class="form-control">
							</div>
						</div>						
						 <div class="form-group">
							<label class="col-md-3">Account Name </label>
							<div class="col-md-4">
								<input type="text" 
								placeholder="Account Name"
								data-parsley-trigger="change"
								data-parsley-maxlength="50"
								data-parsley-maxlength-message="Only 50 characters are allowed"
								name="bank_acc_name" class="form-control">
							</div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Account Number:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Account Number"
							data-parsley-trigger="change"	
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="bank_acc_no" class="form-control">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Name:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Bank Name"
							data-parsley-trigger="change"	
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="bank_name" class="form-control">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Branch Name:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Branch Name"
							data-parsley-trigger="change"	
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="bank_b_name" class="form-control">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">IFSC Code:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter IFSC Code"
							data-parsley-trigger="change"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="bank_ifsc" class="form-control">
						  </div>
						</div>
						<!-- <div class="form-group">
						  <label class="col-md-3">GSTIN:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter GSTIN"
							data-parsley-trigger="change"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="gstnumber" class="form-control">
						  </div>
						</div> 
						<div class="form-group">
						  <label class="col-md-3">Status:</label>
						  <div class="col-md-4">						 			
								<select name="shop_status" id="status" class="form-control">
									<option value="Active">Active</option>
									<option value="Inactive">Inactive</option>
								</select>								
						  </div>
						</div> -->	
					<input type="hidden" name="shop_status" id="shop_status" value="0">
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">			  
               <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
                <a href="shops.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group -->
          </form>                                       
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
<script type="text/javascript">
            $(document).ready(function(){$("#datetimepicker1").timepicker({format: "yyyy-mm-dd",autoclose: true});});
			$(document).ready(function(){$("#datetimepicker2").timepicker({format: "yyyy-mm-dd",autoclose: true});});
            </script>
<style>
.form-horizontal{
font-weight:normal;
}
</style>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<script>  
function fnShowStockist(id){
	if (window.XMLHttpRequest)
	{
		xmlhttp=new XMLHttpRequest();
	}
	else
	{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById("Subcategory").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","fetchstockist.php?cat_id="+id.value,true);
	xmlhttp.send();
}
function calculate_data_count(element_value){
	element_value = element_value.toString();
	var element_arr = element_value.split(',');	
	return element_arr.length;
}
function setSelectNoValue(div,select_element){
	var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled value="">-Select-</option></select>';
	document.getElementById(div).innerHTML	=	select_selement_section;
}
function fnShowCity(id_value) {	
	$("#city_div").show();	
	$("#area").html('<option value="">-Select-</option>');	
	$("#subarea").html('<option value="">-Select-</option>');	
	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city&mandatory=mandatory";
	CallAJAX(url,"div_select_city");	
}
function FnGetSuburbDropDown(id) {
	$("#area_div").show();		
	$("#subarea").html('<option value="">-Select-</option>');	
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&function_name=FnGetSubareaDropDown&mandatory=mandatory";
	CallAJAX(url,"div_select_area");
}
function FnGetSubareaDropDown(id) {	
	var suburb_str = $("#area").val();	
	$("#subarea_div").show();	
	var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea";
	CallAJAX(url,"div_select_subarea");
		
}
</script>            
