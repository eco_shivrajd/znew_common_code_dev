<?php
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
$report_title = $orderObj->getReportTitle();
$row = $orderObj->getDeliveryReport();
//echo "sdfsd<pre>";print_r($row);
//exit();
$colspan = "10";
?>
<? if($_POST["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<? }
 ?>
<table 
	class="table table-striped table-bordered table-hover table-highlight table-checkable" 
	data-provide="datatable" 
	data-display-rows="10"
	data-info="true"
	data-search="true"
	data-length-change="true"
	data-paginate="true"
	id="sample_2">
<thead>
<tr>
  <td colspan="<?=$colspan;?>" align="canter" class="gradeX even" style="text-align:center; font-weight:600;"><h4><b><?php if(!empty($report_title))echo $report_title; else echo "Delivery Report All";?></b></h4></td>              
  </tr>
  <tr>
	<th data-filterable="false" data-sortable="true" data-direction="asc">Delivery Person </th>
	<th data-filterable="false">Delivery From</th>
	<th data-filterable="false" >Delivery To</th>
	<th data-filterable="false" data-sortable="false" data-direction="desc">Order No.</th>
	<th data-filterable="false" data-sortable="false" data-direction="desc">Total Price <i aria-hidden='true' class='fa fa-inr'></i></th>	
	<th data-filterable="false" >Delivery Assing Date </th>   
	<th data-filterable="false" >Delivered Date</th>
	<th data-filterable="false" >Remark</th>
	<th data-filterable="false" >Signature</th>
	<th data-filterable="false" >Payment Status</th>	           
  </tr>
</thead>
<tbody>					
	<?php 
	if(!empty($row)){
		//echo "<pre>";print_r($row);
//exit();
			$gtotalq=0;
			foreach($row as $key => $value)
			{  
			  // $gtotalq=$gtotalq+$value['total_order_gst_cost'];		 
			?>
			<tr class="odd gradeX">
				<td rowspan='1' ><?=$value['dp_fisrtname']?></td>
				<td><?=$value['ufrom_firstname']?></td>
				<td><?=$value['uto_firstname']?></td>
				<td><?=$value['order_no']?></td>
				<td align='right'><?=fnAmountFormat($value['total_order_gst_cost'])?></td>
				<td><?=$value['delivery_assing_date']?></td>
				<td><?=$value['delivery_date']?></td>
				<td> </td>
				<td>
					<?php
                     if ($value['signature_image']!='' && $value['challan_no']!='') 
                     {
                     ?>
                     	<img src="../../uploads/cod/<?=$value['challan_no'];?>/<?=$value['signature_image'];?>" width="70px" height="40px;" alt="Loading..">
                    <?php
                     }
                     else
                     {
                     ?>
                     <?='-';?>
                     <?php }	
					?>					
				</td>
				<td>
					<?php echo $value['amount_paid'];?>
				</td>
			</tr>
				<?php } ?>
			<!-- <tr class="odd gradeX">
				<td></td>
				<td></td>
				<td></td>
				<td><b>Total</b></td>
				<td><b><?=$gtotalq?></b></td>
				<td></td>
				<td></td>
			</tr> -->
			<?php
	}/*else{
		echo "<tr class='odd gradeX'><td colspan='3' align='center'>No matching records found</td></tr>";
	}*/
	if($_POST["actionType"]=="excel" &&  $row == 0) 
	{
			echo "<tr class='odd gradeX'><td colspan='3'>No matching records found</td></tr>";
	}
	?>	
			
</tbody>	
</table>



<script>
jQuery(document).ready(function() {    
   
   ComponentsPickers.init();
});

jQuery(document).ready(function() { 
	TableManaged.init();
});
$(document).ready(function() {
      var table = $('#sample_2').dataTable();
      // Perform a filter
      table.fnFilter('');
      // Remove all filtering
      //table.fnFilterClear();
	   
  });
</script>

<!-- END JAVASCRIPTS -->
<?
if($_POST["actionType"]=="excel") {
	if($row != 0){
		header("Content-Type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=SP_Summary_Report.xls");
	}
} ?>
 