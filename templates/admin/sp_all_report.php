<!-- BEGIN HEADER -->
<?php 
include "../includes/grid_header.php";
include "../includes/userManage.php";
include "../includes/targetManage.php";
$userObj 	= 	new userManager($con,$conmain);
$targetObj 	= 	new targetManager($con,$conmain);
?>
<!-- END HEADER -->
 <link href="../../assets/global/css/jquery.loader.css" rel="stylesheet" />
 <style>
body {
  margin: 0;
}
#dvMap {
   height: 0;
    overflow: hidden;
    padding-bottom: 22.25%;
    padding-top: 30px;
    position: relative;
}
.dvMap-frame {
  left:0;
    top:0;
    height:100%;
    width:100%;
    position:absolute;
}
.kd-tabbed-vert.header-links .kd-tabbutton a {
  color: #757575;
  display: inline-block;
  height: 100%;
  padding: 0 24px;
  width: 100%;
}
.kd-tabbed-vert.header-links .kd-tabbutton {
  padding: 0;
}
.kd-tabbed-vert.header-links .kd-tabbutton.selected a {
  color: #03a9f4;
}
.kd-tabbed-vert.header-links .kd-tabbutton a:focus {
  text-decoration: none;
}
p.top-desc {
  padding: 1em 1em .1em 1em;
}
p.bottom-desc {
  padding: 0em 1em 1em 1em;
}
</style>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Reports"; $activeMenu = "SP Reports";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			SP Reports
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">SP Reports</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->			
			<div class="row">
				<div class="col-md-12"> 				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">Search Criteria</div>
						</div>
						<div class="portlet-body">
						<form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">
							<div class="form-group">
								<label class="col-md-3">Select Report:</label>								
								<div class="col-md-4" id="sp_all_report_type">									
									<select name="dropdown_report_type" id="dropdown_report_type" onChange="fnSelectionReportType()"
									class="form-control">										
										<option value="daily_status_report" selected>Daily Status Report</option>
										<option value="sp_summary" >SP Summary</option>
										<option value="spwise_summary" >SP Retail Summary</option>
										<!--<option value="daily_stock_sp_report" >SP Stock Report</option>-->
										<option value="geo_location">Sales Person Location</option>
										<option value="expense_report">Expense Report</option>
										<option value="sales_person_attendance">Sales Person Attendance</option>
										<option value="targets">Targets</option>
										<!--<option value="all_sp_sales">All SP Sales</option>sss-->
									</select>
								</div>	
							</div><!-- /.form-group -->
							<div class="form-group"  id="sp_div">
								<label class="col-md-3">Sales Person:<span class="mandatory">*</span></label>
								<div class="col-md-4" id="divsalespersonDropdown">
								<?php $user_result = $userObj->getAllSP('SalesPerson'); ?>	
									<select name="dropdownSalesPerson" id="dropdownSalesPerson" class="form-control">
										<option value="">-Select-</option>
										<?php while($row_user = mysqli_fetch_assoc($user_result))
										{ ?>									
										<option value="<?=$row_user['id'];?>"><?=$row_user['firstname'];?></option>
										<?php } ?>
									</select>
								</div>
							</div><!-- /.form-group -->
							<div class="form-group" id="date_div" >
								<label class="col-md-3">Select date:</label>
								<div class="col-md-4">
									<div  class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
										<input type="text" class="form-control" data-date="<?php echo date('d-m-Y');?>" 
										data-date-format="dd-mm-yyyy" data-date-viewmode="years" 
										name="frmdate1" id="frmdate1" value="<?php echo date('d-m-Y');?>" autocomplete="off">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<!-- /input-group -->								 
								</div>
							</div><!-- /.form-group -->						
							<div class="form-group"  id="date_option_div" style="display:none;">
								<label class="col-md-3">Date Options:</label>
								<div class="col-sm-4">						
									<select class="form-control" name="selTest" id="selTest" onChange="fnSelectionBoxTest()">									
									<option value='5' <?php if($_REQUEST['selTest']=="5")echo 'selected';?>>All</option>
									<option value="4" <?php if($_REQUEST['selTest']=="4")echo 'selected';?>>Today</option>
									<option value='1' <?php if($_REQUEST['selTest']=="1")echo 'selected';?>>Weekly</option>
									<option value='2' <?php if($_REQUEST['selTest']=="2")echo 'selected';?>>Current month</option>
									<option value='3' <?php if($_REQUEST['selTest']=="3")echo 'selected';?>>From specific date</option>
									</select>
									<input type="hidden" name="hdnSelrange" id="hdnSelrange">
								</div>
							</div>
							<div class="form-group" id="date-show" style="display:none;">
								<label class="col-md-3">Date:</label>
								<div class="col-md-8 nopadl">
									<div class="col-md-1" class="nopadl" style="margin-right:-17px;">
										<label class="nopadl" style="padding-top:5px;">From:</label>
									</div>
									<div class="col-md-3">										
										<div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
											<input type="text" class="form-control" name="frmdate2" id="frmdate2" value="<?php echo $frmdate;?>" readonly >
											<span class="input-group-btn">
											<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>			
									<div class="col-md-1" class="nopadl" style="margin-right:-35px;margin-left:-8px;">
										<label class="nopadl" style="padding-top:5px">To:</label>
									</div>
									<div class="col-md-3">
										<div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
											<input type="text" class="form-control" name="todate" id="todate" value="<?php echo $todate;?>" readonly >
											<span class="input-group-btn">
											<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>						
								</div>                          
							</div>     
							<div class="form-group" id="product_div" style="display:none;">
								<label class="col-md-3">Select Product:</label>
								<div class="col-md-4">								
									<select name="selprod" id="selprod"  class="form-control">	
										<option value="">Select Product</option>
										<?php 
									$sql="SELECT b.productname,b.id FROM tbl_product b ";
									$result = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result))
									{
										?>
										<option value="<?php echo $row['id'];?>"><?php echo fnStringToHTML($row['productname']);?></option>
										<?php } ?>
									</select>
								</div>
							</div><!-- /.form-group -->	
							<input type="hidden" id="order" name="order" value="asc">
							<input type="hidden" id="sort_complete" name="sort_complete" value="">
							<input type="hidden" id="page" name="page" value="">
							<input type="hidden" id="per_page" name="per_page" value=""> 	
							<input type="hidden" id="actionType" name="actionType" value=""> 
							<input type="hidden" id="frmdate" name="frmdate" value=""> 
							<input type="hidden" id="search" name="search" value="">
							<div class="form-group">
								<div class="col-md-4 col-md-offset-3">
									<button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="$('#loader_div').loader('show'); ShowReport();">Search</button>
									
									<button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnChangeReportType('reset');">Reset</button>
								</div>
							</div><!-- /.form-group -->
						</form>	
						</div>
					   <div class="clearfix"></div>
					</div>
					<div id="loader_div"></div>
					<div class="portlet box blue-steel" id="order_summary_details_spsummary" style="display:none;">
						<div class="portlet-title">
							<div class="caption"><i class="icon-puzzle"></i>
								<div id="report_caption" style="min-width: 300px;"></div>
							</div>
							<button type="button" name="btnExcel" id="btnExcel" 
								onclick="ExportToExcel();" 
								class="btn btn-primary pull-right" 
								style="margin-top: 3px;">Export to Excel</button> &nbsp;&nbsp;
							<button type="button" name="btnPrint" id="btnPrint" 
								class="btn btn-primary pull-right" 
								style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
							<button type="button" name="btnPrint1" id="btnPrint1" 
								class="btn btn-primary pull-right" 
								style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
						</div>	
						<div class="portlet-body" id="order_summary_details">
						</div>
					</div>
					
		</div>			
	</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div id="print_div"  style="display:none;"></div>
<form action="../includes/exportToExcel.php" method="post" name="export_excel" id="export_excel">
	<input type="hidden" name="export_data" id="export_data">
	<input type="hidden" name="file_name" id="file_name">
</form>
<iframe id="txtArea1" style="display:none"></iframe>
<?php include "../includes/grid_footer.php"?>
<script src="../../templates/js/amcharts1.js"></script>
<script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="../../templates/js/pie.js"></script>	
<script src="../../templates/js/light.js"></script>	
<!--<script src="../../templates/js/canvasjs.min.js"></script>-->
<script>
function fnExcelReport()
{
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j=0;
    tab = document.getElementById('sample_2'); // id of table

    for(j = 0 ; j < tab.rows.length ; j++) 
    {     
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text=tab_text+"</table>";
    //tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    //tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
   // tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE "); 

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus(); 
        sa=txtArea1.document.execCommand("SaveAs",true,"Reports.xls");
    }  
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

    return (sa);
}
function fnChangeReportType(reportType) {
	$('#dropdown_report_type').val('daily_status_report');
	fnSelectionReportType();
}
function fnSelectionBoxTest()
{
	if($('#selTest').val() == '3'){
	   $('#date-show').show();
	}else{
		$('#date-show').hide();
		//ShowReport(1);
	}
}
function fnSelectionReportType(){
	//dropdown_report_type
	//sp_div,date_div,date_option_div,product_div
	$('#order_summary_details_spsummary').hide();
	var report_type=$('#dropdown_report_type').val();
	//var report_type_text=$('#dropdown_report_type option:selected').text();
	//$('.page-title').html(report_type_text);
	switch(report_type) {
	  case "daily_status_report":
		$('#date_div').show();		
		$('#sp_div').show();
		$("span.mandatory").show();
		$('#product_div').hide();
		$('#date_option_div').hide();	
		$('#divsalespersonDropdown option[value=""]').text('-Select-');
		break;
	  case "sp_summary":
		$('#product_div').show();
		$('#date_option_div').show();	
		$('#selTest').val("5");
		$('#date_div').hide();
		$('#sp_div').hide();
		break;
	case "spwise_summary":
		$('#product_div').hide();
		$('#date_option_div').show();	
		$('#selTest').val("5");
		$('#date_div').hide();
		$('#sp_div').show();
		$("span.mandatory").show();
		$('#divsalespersonDropdown option[value=""]').text('-Select-');
		break;
	case "daily_stock_sp_report":
		$('#date_div').show();
		$('#sp_div').show();
		$("span.mandatory").hide();
		$('#product_div').hide();
		$('#date_option_div').hide();
		//$('select[name=divsalespersonDropdown] > option:first-child').text(' Select All SP ');
		$('#divsalespersonDropdown option[value=""]').text('Select All SP');
		break;
	  case "geo_location":
		$('#date_div').show();
		$('#sp_div').show();
		$("span.mandatory").show();
		$('#product_div').hide();
		$('#date_option_div').hide();	
		$('#divsalespersonDropdown option[value=""]').text('-Select-');
		break;
	  case "expense_report":
		$('#sp_div').hide();
		$('#date_option_div').show();
		$('#selTest').val("5");
		$('#date_div').hide();
		$('#product_div').hide();
		break;
	  case "sales_person_attendance":
		$('#sp_div').show();
		$("span.mandatory").hide();
		$('#date_option_div').show();
		$('#selTest').val("5");
		$('#date_div').hide();
		$('#product_div').hide();
		$('#divsalespersonDropdown option[value=""]').text('Select All SP');
		break;
	 case "targets":
		$('#sp_div').show();
		$("span.mandatory").show();
		$('#date_option_div').hide();
		$('#date_div').hide();
		$('#product_div').hide();
		$('#divsalespersonDropdown option[value=""]').text('-Select-');
		break;
	case "all_sp_sales":
		$('#sp_div').hide();
		$('#date_option_div').show();
		$('#selTest').val("5");
		$('#date_div').hide();
		$('#product_div').hide();
		break;
	  default:
		//dgfdf
	}
	$('#date-show').hide();
}
function ShowReport(){
	var report_type=$('#dropdown_report_type').val();
	//alert("dsfsd");report_caption
		$('#order_summary_details_spsummary').hide();		
	switch(report_type) {
		case "daily_status_report":
		ShowReport_dsr_geo('dsr');
		break;
	  case "sp_summary":
		ShowReport_spsummary_spattendance_spexpense('sp_summary');
		break;
	  case "spwise_summary":
		ShowReport_spsummary_spattendance_spexpense('spwise_summary');
		break; 
	case "daily_stock_sp_report":
		ShowReport_dsr_geo('daily_stock_sp_report');
		break;
	  case "geo_location":
		ShowReport_dsr_geo('geo');	
		break;
	 case "targets":
		ShowReport_dsr_geo('targets');	
		break;
	case "all_sp_sales":
		ShowReport_spsummary_spattendance_spexpense('all_sp_sales');	
		break;
	  case "expense_report":
		ShowReport_spsummary_spattendance_spexpense('expense_report');
		//ShowReport_expense_report();
		break;
	  case "sales_person_attendance":
		ShowReport_spsummary_spattendance_spexpense('sales_person_attendance');
		break;	
	  default:
	}
}
function ShowReport_dsr_geo(dsr_or_geo) {
	 $("#loader_div").loader('show');
	 if(dsr_or_geo=='geo'){		
		 var url = "geolocation-track-ajax2.php";
		 var report_caption="Sales Person Location";
	 }else if(dsr_or_geo=='dsr'){
		 var url = "dailyStatusReport.php";	
		 var report_caption="Daily Status Report";
	 }else if(dsr_or_geo=='daily_stock_sp_report'){
		 var url = "daily_stock_sp_report.php";	
		 var report_caption="SP Stock Report";
	 }else{
		 var url = "ajax_getTargets.php";	
		 var report_caption="Targets";
	 }
	
	 if(report_caption!="Targets"){
		 var date = $('#frmdate1').val();
		$('#frmdate').val(date);		
		if(date == ''){
			alert('Please select date.');
			$('#frmdate1').focus();
			$("#loader_div").loader('hide');
			return false;
		}
	 }
	
	var data = $('#frmsearch').serialize();
	var salespersonid = $('#dropdownSalesPerson').val();
	if(salespersonid == '' && dsr_or_geo !='daily_stock_sp_report'){
		alert('Please select Sales Person.');
		$('#dropdownSalesPerson').focus();
		$("#loader_div").loader('hide');
		return false;
	}	
    jQuery.ajax({
		url: url,
		method: 'POST',
		data: data,
		async: false
	}).done(function (response) {
		$("#order_summary_details_spsummary").show();		
		$('#order_summary_details').html(response);
		$('#report_caption').html(report_caption);
		if(dsr_or_geo=='dsr'){
			$('#btnPrint1').show();	
			$('#btnExcel').show();	
		}else{
			$('#btnPrint1').hide();			
			$('#btnExcel').hide();			
		}
		$('#btnPrint').hide();	
		//$('#btnExcel').hide();
		
		$("#loader_div").loader('hide');
	}).fail(function () { });
	
	return false; 
}

function ShowReport_spsummary_spattendance_spexpense(spsummary_or_spattendance){
		if(spsummary_or_spattendance=='sp_summary'){
			var url = "order_summary_detail1.php";
			var report_caption="SP Summary Report";
		 }else if(spsummary_or_spattendance=='spwise_summary'){
			var url = "spwise_summary.php";
			var report_caption="SP Retail Summary Report";
		 }else if(spsummary_or_spattendance=='sales_person_attendance'){
			var url = "sales_person_attendance_detail.php";
			var report_caption="SP Attendance Report";
		 }else if(spsummary_or_spattendance=='all_sp_sales'){
			 var url = "sp_sale_compare.php";	
			 var report_caption="Total Sales";
		}else{
			var url = "tadareportmonthlyajax.php";
			var report_caption="Expense Report";
		 }
		var param = '';	
		var selprod = $('#selprod').val();
		param = param + 'selprod='+selprod;
		var selTest = $('#selTest').val();		
		param = param + '&selTest='+selTest;
		var salespersonid = $('#dropdownSalesPerson').val();
		param = param + '&dropdownSalesPerson='+salespersonid;
		if(selTest == 3)
		{
			var frmdate = $('#frmdate2').val();
			$('#frmdate').val(frmdate);
			param = param + '&frmdate='+frmdate;
			var todate = $('#todate').val();
			param = param + '&todate='+todate;
			var validation = compaire_dates(frmdate,todate);
		}
		//alert(param);
	  if(selTest == 3 && frmdate != '' && todate != '' && validation == 1)
	  {
		  alert("'From' date should not be greater than 'To' date.");
		  $("#loader_div").loader('hide');	
		  return false;
	  }else if(spsummary_or_spattendance=='spwise_summary' && salespersonid == ''){
			alert('Please select Sales Person.');
			$('#dropdownSalesPerson').focus();
			$("#loader_div").loader('hide');
			return false;
	  }else{
		$.ajax
		({
			type: "POST",
			url: url,
			data: param,
			success: function(msg)
			{
				console.log(msg);
				$("#order_summary_details_spsummary").show();
				$("#order_summary_details").html(msg);
				$('#report_caption').html(report_caption);
				if(report_caption=="Expense Report" ||report_caption=="Total Sales"){
					$('#btnPrint').hide();$('#btnPrint1').hide();$('#btnExcel').hide();
				}else{
					$('#btnPrint').show();$('#btnPrint1').hide();$('#btnExcel').show();
				}				
				$("#loader_div").loader('hide');				
			}
		  });
	  }
}

 function ExportToExcel() {
	 var report_name=$('#report_caption').html();
	// alert(report_name);
	 if(report_name=='SP Summary Report'||report_name=='SP Retail Summary Report'){
		 var selector='#sample_2';
	 }else if(report_name=='Daily Status Report'){
		var selector='#report_table'; 
	 }else{
		var selector='#sample_5'; 
	 }
	 //var report_caption="Daily Status Report";
	 //report_table
	 var td_rec = $(""+selector+" td:last").html();
	if(td_rec != 'No matching records found')
	{
		var divContents = $(""+selector+"").html();
		$("#print_div").html('<table id="print_table" style="text-decoration:none;">'+divContents+'</table>');	
		
		divContents =  $("#print_div").html();
		divContents = divContents.replace(/₹/g,'Rs');
		var file_name = "Report_"+report_name+".xls";
		$("#file_name").val(file_name);
		$("#export_data").val(divContents);
		document.forms.export_excel.submit();
	}else{
		alert("No matching records found");
	} 
} 

$('.date-picker1').datepicker({
	rtl: Metronic.isRTL(),
	orientation: "left",
	endDate: "<?php echo date('d-m-Y');?>",
	autoclose: true
});

function report_download() {
	var td_rec = $("#sample_2 td:last").html();
	if(td_rec != 'No matching records found')
	{
		var divContents = $(".table-striped").html();
		$("#print_div").html('<table id="print_table" style="text-decoration:none;">'+divContents+'</table>');	
		var heading = $("#table_heading").html();
		$("#print_table tr th i").html("RS");	
		divContents =  $("#print_div").html();
		
		divContents = divContents.replace(/<\/*a.*?>/gi,'');	
		$("#export_data").val(divContents);
		document.forms.export_excel.submit();
	}else{
		alert("No matching records found");
	}
}

 $("#btnPrint").live("click", function () {
	var td_rec = $("#sample_2 td:last").html();
	if(td_rec != 'No matching records found')
	{
		var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
		
		var divContents1 = $(".table-scrollable").html();
		$("#print_div").html(divContents1);
		$("#print_div a").removeAttr('href');
		$("#sample_2 tr th i").html("");	
		var divContents = $("#print_div").html();
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write('<html><head><title>Report</title>');
		printWindow.document.write('<style>a{text-decoration: none; color:#333333;} #sample_2{margin:20 20 20 20px; width:700px}</style>');
		printWindow.document.write('<link   rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css"/>');
		printWindow.document.write('<link  rel="stylesheet" type="text/css" href="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>');
		
		if( navigator.userAgent.toLowerCase().indexOf('chrome') > -1 ){
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');	
			printWindow.focus();	
			setTimeout(function () {
				printWindow.print();
				//printWindow.close();
			}, 500);
		}else if(isIE == true){
			printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');	
			printWindow.focus();	
			printWindow.document.execCommand("print", false, null);
			//printWindow.close();
		}
		else{		
			printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');	
			printWindow.focus();	
			setTimeout(function () {				
				printWindow.print();
				//printWindow.close();
			}, 100);
		}
	}else{
		alert("No matching records found");
	}
});
$("#btnPrint1").live("click", function () {
	var td_rec = $("#report_table td:last").html();
	if(td_rec != 'No matching records found')
	{
		var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
		
		var divContents1 = $("#dvtblResonsive").html();
		$("#print_div").html(divContents1);
		$("#print_div a").removeAttr('href');
		$("#report_table tr th i").html("");	
		var divContents = $("#print_div").html();
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write('<html><head><title>Report</title>');
		printWindow.document.write('<style>a{text-decoration: none; color:#333333;} #report_table{margin:20 20 20 20px; width:700px}</style>');
		printWindow.document.write('<link   rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css"/>');
		printWindow.document.write('<link  rel="stylesheet" type="text/css" href="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>');
		
		if( navigator.userAgent.toLowerCase().indexOf('chrome') > -1 ){
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');	
			printWindow.focus();	
			setTimeout(function () {
				printWindow.print();
				//printWindow.close();
			}, 500);
		}else if(isIE == true){
			printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');	
			printWindow.focus();	
			printWindow.document.execCommand("print", false, null);
			//printWindow.close();
		}
		else{		
			printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');	
			printWindow.focus();	
			setTimeout(function () {				
				printWindow.print();
				//printWindow.close();
			}, 100);
		}
	}else{
		alert("No matching records found");
	}
});

</script>

<script type="text/javascript" src="../../assets/global/scripts/jquery.loader.js"></script>

<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>