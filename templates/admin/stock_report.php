<!-- BEGIN HEADER -->
<?php
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include "../includes/grid_header.php";
include "../includes/orderManage.php";
include "../includes/userManage.php";

$orderObj = new orderManage($con, $conmain);
$userObj = new userManager($con, $conmain);


 
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix"> </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "Reports";
        $activeMenu = "stock_report";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
                <h3 class="page-title">
                    Stock Report
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li><i class="fa fa-home"></i>
                            <a href="#">Stock Report</a>	
                        </li>
                    </ul>				
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">                
                        <div class="row">
                            <div class="col-md-12"> 				
                                <div class="portlet box blue-steel">
                                    <div class="portlet-title">
                                        <div class="caption">Search Criteria</div>							
                                        <div class="clearfix"></div>
                                    </div>						
                                    <div class="portlet-body">							
                                        <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">								

                                           <div class="form-group">
										<label class="col-md-3">User Type:<span class="mandatory">*</span></label>
										<div class="col-md-4">                              
											<select name="dropdownUserType" id="dropdownUserType" onChange="getUserByUserTypeDropDown(this.value)" class="form-control">
											<option value="" > --Select-- </option>
											<?php  
											//$seesion_user_type=$_SESSION[SESSION_PREFIX.'user_type'];
											$session_user_id=$_SESSION[SESSION_PREFIX.'user_id'];
											
											$sql = "SELECT distinct user_type,user_role FROM `tbl_user` 
											where  find_in_set('$session_user_id',parent_ids) <> 0
											and id!='1' 
											and (user_role = 'Superstockist' OR user_role = 'Distributor') 
											order by user_type";
											$result1 = mysqli_query($con, $sql);
										
										 while ($row = mysqli_fetch_array($result1)){ 
											if($row['user_role']!='SalesPerson'){?>
										  <option value="<?php echo $row['user_type'];?>" ><?php echo $row['user_type'];?></option>
										 <?php }  } ?> 
											</select>
											
										</div>
									</div><!-- /.form-group --> 
									<div class="clearfix"></div>
									<div class="form-group" id="user_div" style="display:none;">
									  <label class="col-md-3">Users:<span class="mandatory">*</span></label>
									  <div class="col-md-4" id="div_select_user">
									 
									  </div>
									</div><!-- /.form-group -->                         
                                            <input type="hidden" id="order" name="order" value="asc">
                                            <input type="hidden" id="sort_complete" name="sort_complete" value="">
                                            <input type="hidden" id="page" name="page" value="">
                                            <input type="hidden" id="per_page" name="per_page" value=""> 	
                                            <input type="hidden" id="actionType" name="actionType" value=""> 
                                            <input type="hidden" id="search" name="search" value="">
                                            <div class="form-group">
                                                <div class="col-md-4 col-md-offset-3">
                                                    <button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>
                                                    <button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnChangeReportType('daily');">Reset</button>
                                                </div>
                                            </div><!-- /.form-group -->
                                    </div>
                                    <!-- END PAGE CONTENT-->
                                </div>
                                <div class="clearfix"></div>   
                                <div class="portlet box blue-steel">
                                    <div class="portlet-title">
                                        <div class="caption"><i class="icon-puzzle"></i>Stock Report</div>
                                        <button type="button" name="btnExcel" id="btnExcel" onclick="report_download();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
                                        &nbsp;
                                        <button type="button" name="btnPrint" id="btnPrint" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
                                    </div>	
                                    <div class="portlet-body" id="order_summary_details">
                                    </div>
                                    </form>	

                                </div>
                            </div>
                            <!-- END CONTENT -->
                            <!-- BEGIN QUICK SIDEBAR -->
   
                            <!-- END QUICK SIDEBAR -->
                        </div>
                        <!-- END CONTAINER -->
                    </div>
                </div>
            </div>
        </div>
        <div style="display:none;" class="modal-backdrop fade in"></div>
        <div aria-hidden="false" style="display: none;" id="basicModal" class="modal fade in">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="close_modal();" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="modal-title">Order Details</h3>
                    </div>
                    <div class="modal-body">

                        <div id="ajax_list_div">
                        </div>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <div id="print_div"  style="display:none;"></div>
        <div id="table_heading"  style="display:none;"><?= $user_data['firstname']; ?>'s Shopwise <?= $report_title; ?></div>
        <form action="../includes/exportToExcel.php" method="post" name="export_excel" id="export_excel">
            <input type="hidden" name="export_data" id="export_data">
        </form>
        <!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php" ?>
        <!-- END FOOTER -->
<script>
$(document).ready(function () {
	ShowReport(1);
});
function fnChangeReportType(reportType) {
	$("#u_id").val('');$("#user_div").hide();
}

function ShowReport(page) {
	//alert("dsfsd");
	var param = '';
	
	var dropdownUserType = $('#dropdownUserType').val();//alert(dropdownUserType);
	//debugger;
	if(dropdownUserType!=""){
		param = param + 'dropdownUserType=' + dropdownUserType;//alert(u_id);
		var u_id = $("select[name=u_id] option:selected").val();
		param = param + '&u_id=' + u_id;//alert(u_id);
	}
	
	$.ajax({
		type: "POST",
		url: "stock_report_details.php",
		data: param,
		success: function (msg)
		{
			$("#order_summary_details").html(msg);
		}
	});
}

function getUserByUserTypeDropDown(user_type){	
	$("#user_div").show();
	var url ="getUserConfigTypeByDropDown.php?user_type="+user_type+"&select_user_type=dropdownUserType";
	CallAJAX(url,"div_select_user");
}
function report_download() {
	var td_rec = $("#sample_2 td:last").html();
	if (td_rec != 'No matching records found')
	{
		var divContents = $(".table-striped").html();
		$("#print_div").html('<table id="print_table" style="text-decoration:none;">' + divContents + '</table>');
		var heading = $("#table_heading").html();
		$("#print_table tr th i").html("RS");
		divContents = $("#print_div").html();

		divContents = divContents.replace(/<\/*a.*?>/gi, '');
		$("#export_data").val(divContents);
		document.forms.export_excel.submit();
	} else {
		alert("No matching records found");
	}
}

$("#btnPrint").live("click", function () {
	var td_rec = $("#sample_2 td:last").html();
	if (td_rec != 'No matching records found')
	{
		var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);

		var divContents1 = $(".table-scrollable").html();
		$("#print_div").html(divContents1);
		$("#print_div a").removeAttr('href');
		$("#sample_2 tr th i").html("");
		var divContents = $("#print_div").html();
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write('<html><head><title>Report</title>');
		printWindow.document.write('<style>a{text-decoration: none; color:#333333;} #sample_2{margin:20 20 20 20px; width:700px}</style>');
		printWindow.document.write('<link   rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css"/>');
		printWindow.document.write('<link  rel="stylesheet" type="text/css" href="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>');

		if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');
			printWindow.focus();
			setTimeout(function () {
				printWindow.print();
				//printWindow.close();
			}, 500);
		} else if (isIE == true) {
			printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');
			printWindow.focus();
			printWindow.document.execCommand("print", false, null);
			//printWindow.close();
		} else {
			printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');
			printWindow.focus();
			setTimeout(function () {
				printWindow.print();
				//printWindow.close();
			}, 100);
		}
	} else {
		alert("No matching records found");
	}
});
function OrderDetailsPrint() {
	var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
	var divContents = '<style>\body {\
			font-size: 12px;}\
	th {text-align: left;}\
	.printHeading { line-height: 18px;  padding: 10px 0;  font-size: 18px; }\
	table { border-collapse: collapse;  \
			font-size: 12px; }\
	table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
	body { font-family: "Open Sans", sans-serif;\
	background-color:#fff;\
	font-size: 15px;\
	direction: ltr;}</style>' + $("#divOrderPrintArea").html();
	if (isIE == true) {
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write(divContents);
		printWindow.focus();
		printWindow.document.execCommand("print", false, null);
	} else {
		$('<iframe>', {
			name: 'myiframe',
			class: 'printFrame'
		}).appendTo('body').contents().find('body').html(divContents);
		window.frames['myiframe'].focus();
		window.frames['myiframe'].print();
		setTimeout(
				function ()
				{
					$(".printFrame").remove();
				}, 1000);
	}
}
</script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- END JAVASCRIPTS -->