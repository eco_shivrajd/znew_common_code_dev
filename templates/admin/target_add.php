<!-- BEGIN HEADER -->
<?php 
include "../includes/header.php";
include "../includes/userManage.php";
include "../includes/targetManage.php";
$userObj 	= 	new userManager($con,$conmain);
$targetObj 	= 	new targetManager($con,$conmain);
?>
<!-- END HEADER -->
<?php
$u_type=base64_decode($_GET['u_type']);
if(isset($_POST['submit']))
{
	//	print_r($_POST);
	//	exit();
	$id=$_POST['id'];
	$user_type=$_POST['user_type']; 	
	$targetObj->addTarget($user_type, $id);
	  echo '<script>alert("Target Added Successfully.");location.href="user_list_config.php";</script>';
}


function weekDayToTime($week, $year, $dayOfWeek = 0) {
	$dayOfWeekRef = date("w", mktime (0,0,0,1,4,$year));
	if ($dayOfWeekRef == 0) $dayOfWeekRef = 7;
	$resultTime = mktime(0,0,0,1,4,$year) + ((($week - 1) * 7 + ($dayOfWeek - $dayOfWeekRef)) * 86400);
	$resultTime = cleanTime($resultTime);  //Cleaning daylight saving time hours
	return $resultTime;
};

function cleanTime($time) {
	//This function strips all hours, minutes and seconds from time.
	//For example useful of cleaning up DST hours from time
	$cleanTime = mktime(0,0,0,date("m", $time),date("d", $time),date("Y", $time));
	return $cleanTime;
}
function weeks($year)
{   
	return date("W",mktime(0,0,0,12,28,$year));
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Targets";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
		   Add Target
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Add Target</a>
					</li>
				</ul>

			</div>
			<!-- END PAGE HEADER-->
			
			<div class="row">
				<div class="col-md-12"> 
				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">Add Target</div>
						</div>
						<div class="portlet-body">					
						<? if($_SESSION[SESSION_PREFIX."user_type"]!="Distributor") { ?>
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						<? } ?>
					    <?php
						$id			= base64_decode($_GET['id']);
						$userObj 	= 	new userManager($con,$conmain);
						$row1 = $userObj->getAllLocalUserDetailss($id);
                       // $row1['state_ids'] = $all_user_details['state'];
						//$row1['city_ids'] = $all_user_details['city'];
						?>   
	<form class="form-horizontal" name="form" enctype="multipart/form-data" method="post" action="target_add.php">

			<div class="form-group">
				<label class="col-md-3">User Type:<span class="mandatory">*</span></label>
				<div class="col-md-4">
				<input type="text" name="user_type1" class="form-control" value="<?php echo fnStringToHTML($row1['user_type']);?>" disabled>
				<input type="hidden" name="user_type" class="form-control" value="<?php echo $row1['user_type'];?>" >
				</div>
			</div>				
					  
			<div class="form-group">
              <label class="col-md-3">Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text"
				placeholder="Enter Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"				
				name="firstname" class="form-control" value="<?php echo fnStringToHTML($row1['firstname'])?>" disabled>
              </div>
            </div><!-- /.form-group --> 

            <div class="form-group">
              <label class="col-md-3">Target In RS:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="number"
				placeholder="Enter Target"		
				name="target_in_rs" class="form-control" value="" min="0" max="1000000" maxlength="6" required>
              </div>
            </div><!-- /.form-group --> 
			<div class="form-group">
              <label class="col-md-3">Target In Weight:<span class="mandatory">*</span></label>
              <div class="col-md-2">
                <input type="number"
				placeholder="Enter Target"		
				name="target_in_wt" class="form-control" value=""  min="0" max="1000000" maxlength="6" required>
              </div>
			  <div class="col-md-2">
					<select name="variant1_wt" id="units_variant_id1"  class="form-control">
						<?php
						$sql_id = "SELECT  * FROM `tbl_units` WHERE variantid =40";
						$result_id = mysqli_query($con, $sql_id);
						while ($row_id = mysqli_fetch_array($result_id)) {
							$unitname = $row_id['unitname'];
							$unit = $row_id['id'];
							echo "<option value='$unit'>" . fnStringToHTML($unitname) . "</option>";
						}
						?>
					</select>
				</div>
            </div><!-- /.form-group --> 
			
							
							<div class="form-group">
								<label class="col-md-3">Report Type:</label>
								<div class="col-md-4">
									<input type="radio" name="targetType" id="targetType_daily" value="daily" checked onclick="fnChangetargetType('daily');"> Daily 
									&nbsp;&nbsp;
									<input type="radio" name="targetType" id="targetType_weekly" value="weekly" onclick="fnChangetargetType('weekly');"> Weekly 
									&nbsp;&nbsp;
									<input type="radio" name="targetType" id="targetType_monthly" value="monthly" onclick="fnChangetargetType('monthly');"> Monthly
									&nbsp;&nbsp;
									<input type="radio" name="targetType" id="targetType_spdate" value="spdate" onclick="fnChangetargetType('spdate');"> Specific Date
								</div>
							</div><!-- /.form-group -->

				<div class="form-group" id="divSpdate" style="display: none;">
					<label class="col-md-3">Select Specific date:</label>
					

					<div class="col-md-8">
                       <div class="col-md-2">
						<label for="inputEmail3" >From Date:</label>
					   </div>
						<div class="col-md-4">
							<div class="input-group  date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
							<input type="text" class="form-control" name="frmdate" id="frmdate" value="<?php echo $frmdate;?>" readonly>
							<span class="input-group-btn">
							<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
							</span>
							</div>
							<!-- /input-group -->
						</div>
					</div>	
					<label class="col-md-3"></label>
                     <div class="col-md-8" style="margin-top: 1%;">
                        <div class="col-md-2">
						 <label for="inputEmail3">To Date:</label>
						</div>
						 
							<div class="col-md-4">
								<div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
								<input type="text" class="form-control" name="todate" id="todate" value="<?php echo $todate;?>" readonly>
								<span class="input-group-btn">
								<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
								</span>
								</div>
							</div>
							</div>
					</div>
							
							<!-- <div class="form-group" id="divDaily" style="display:none;">
								<label class="col-md-3">Select date:</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" class="form-control  date date-picker1" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years" name="frmdate" id="frmdate" value="<?php echo date('d-m-Y');?>">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>					 
								</div>
							</div> -->
							
							<div class="form-group" id="divWeekly" style="display:none;">
								<label class="col-md-3">Select Week:</label>
								<div class="col-md-4">
									<select name="drpWeeklyOption" id="drpWeeklyOption" class="form-control">
									
										<? 
										$yearStart = date("Y");
										//$yearStart = 2017;  
										//$yearEnd = date("Y");
										$yearEnd = 2020;
										for($year=$yearStart;$year<=$yearEnd;$year++) {
											for($i=1;$i<=weeks($year);$i++)
											{
												$start = weekDayToTime($i, $year);
												$end   = cleanTime(518400 + $start);
												$selected = '';
												if(weekDayToTime(date("W"), date("Y")) == $start)
												{
													$selected = "selected = 'selected'";
												}
												$startDate = strftime("%d-%m-%Y", $start);
												$endDate   = strftime("%d-%m-%Y", $end);
												
												echo "<option value='".$startDate . "::" . $endDate . "' $selected>".strftime("%d-%m-%Y", $start)." To ".strftime("%d-%m-%Y", $end)."</option> \n";

												if($selected!="")
													break;
											}
										} ?>
										</select> 
									<!-- /input-group -->								 
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group" id="divMonthly" style="display:none;">
								<label class="col-md-3">Select Month:</label>
								<div class="col-md-4">
									<select name="drpMonthlyOption" id="drpMonthlyOption" class="form-control">
										<?  
										$yearStart = date("Y");
										//$yearStart = 2018;
										// $yearEnd = date("Y");
										 $yearEnd = 2020;
										$currentMonth = date("m-Y") ;
										for($year=$yearStart;$year<=$yearEnd;$year++) { 
											for ($m=1; $m<=12; $m++) {
												$optionValue = "";
												if($m<10)
													$optionValue="0".$m . '-' . $year;
												else 
													$optionValue=$m . '-' . $year;
												
												$selected = "";
												if($currentMonth==$optionValue)
													$selected = " selected = 'selected'";
												
												echo '<option value="' . $optionValue . '" '.$selected.'>' . date('M', mktime(0,0,0,$m)) . '-' . $year . '</option>';
												if($currentMonth==$optionValue)
													break;
											}
										} ?>
										</select> 
									<!-- /input-group -->								 
								</div>
							</div><!-- /.form-group -->

						

							<div class="form-group">
								<div class="col-md-4 col-md-offset-3">
									<input type="hidden" name="id" id="id" value="<?=$row1['id'];?>">
								<button type="submit" name="submit" id="submit" class="btn btn-primary" >Submit</button>
									
									<button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnChangetargetType('daily');">Reset</button>
								</div>
							</div><!-- /.form-group -->
						
						</form>							
						
						</div>
					   <div class="clearfix"></div>
					</div>
					
					<div id="divReportHTML"></div>
			
		</div>			
	</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>

<script>



$('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "auto",
                startDate: 'd',
                endDate: '+3m',
                autoclose: true
            });

fnChangetargetType('daily');


function fnChangetargetType(targetType) {
	$("#divReportHTML").hide();
	switch(targetType) {
		case "daily":
			$("#divDaily").hide();
			$("#divWeekly").hide();
			$("#divMonthly").hide();
			$("#divSpdate").hide();
			break;
		case "weekly":
			$("#divDaily").hide();
			$("#divWeekly").show();
			$("#divMonthly").hide();
			$("#divSpdate").hide();
			break;
		case "monthly":
			$("#divDaily").hide();
			$("#divWeekly").hide();
			$("#divMonthly").show();
			$("#divSpdate").hide();
			break;
	    case "spdate":
	        $("#divDaily").hide();
			$("#divWeekly").hide();
			$("#divMonthly").hide();
			$("#divSpdate").show();
			break;
	}
}
$('form').submit(function () {	
	var targetType = $('input[name=targetType]:checked').val();
	if(targetType == 'spdate')
	{
		var frmdate = $('#frmdate').val();		
		var todate = $('#todate').val();		
		var validation = compaire_dates(frmdate,todate);
	}
	if(targetType == 'spdate' && frmdate != '' && todate != '' && validation == 1){
		alert("'From' date should not be greater than 'To' date.");
		return false;
	}
});
</script>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>