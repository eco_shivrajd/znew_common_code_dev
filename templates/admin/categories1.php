<?php
include "../includes/header.php";
include "../includes/commonManage.php";	
if(isset($_POST['submit']))
{
	//$id=$_GET['id'];
	$id=base64_decode($_GET['id']);
	$brand_id=$_POST['brandid'];
	$categorynm=fnEncodeString($_POST['categorynm']);

	$file=$_FILES['categoryimage']['name'];

	//$folder = "upload/";
	 //create folder for upload product image
            $directoryName = 'upload/'.COMPANYNM.'_upload/'; 
            //Check if the directory already exists.
            if(!is_dir($directoryName)){
            //Directory does not exist, so lets create it.
            mkdir($directoryName, 0755, true);
            }
    $folder = "upload/".COMPANYNM."_upload/";  
	$upload_image = $folder . basename($_FILES["categoryimage"]["name"]);
	$str=move_uploaded_file($_FILES["categoryimage"]["tmp_name"], $upload_image);

	if($file!="")
	{
		$sql = "UPDATE `tbl_category` SET brandid='$brand_id',categorynm='$categorynm',categoryimage='$file' where id='$id'";
		$category_sql=mysqli_query($con,$sql);	
	}
	else{
		$sql = "UPDATE `tbl_category` SET brandid='$brand_id',categorynm='$categorynm' where id='$id'";
		$category_sql=mysqli_query($con,$sql);
	}
	$commonObj 	= 	new commonManage($con,$conmain);
	$commonObj->log_update_record('tbl_category',$id,$sql);
	echo '<script>alert("Category updated successfully.");location.href="categories.php";</script>';
}
?>
<!DOCTYPE html>
<!-- BEGIN HEADER -->

<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">

<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageProducts"; $activeMenu = "Categories";
	include "../includes/sidebar.php";
	$commonObj 	= 	new commonManage($con,$conmain);
	$row_url=$commonObj->getPageIDforUrlEdit($php_page_name);
	$page_id_url = $row_url['page_id'];
	$row_url_edit=$commonObj->getURLforEdit($profile_id,$page_id_url);
	$ischecked_edit_url = $row_url_edit['ischecked_edit'];
    if ($ischecked_edit_url == 0 && $ischecked_edit_url!='') 
	{
		session_set_cookie_params(0);
		session_start();
		session_destroy();
		echo '<script>location.href="../login.php";</script>';
	    exit;
	}
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">Categories</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="categories.php">Categories</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Category</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Category
							</div>
							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
 <?php
 //$id=$_GET['id'];
 $id=base64_decode($_GET['id']);
$sql1="SELECT * FROM `tbl_category` where id='$id'";
$result1 = mysqli_query($con,$sql1);
if(mysqli_num_rows($result1)>0)
{
	$row1 = mysqli_fetch_array($result1);
?>	                       
          <form class="form-horizontal" data-parsley-validate="" role="form" method="post" enctype="multipart/form-data" action="">
            <div class="form-group">
              <label class="col-md-3">Brand:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <select name="brandid" 
				data-parsley-trigger="change"				
                data-parsley-required="#true" 
                data-parsley-required-message="Please select brand"
				class="form-control">
<option selected disabled>-Select-</option>
<?php
$sql="SELECT * FROM tbl_brand  WHERE isdeleted != 1";
$result = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result))
{
$brand_id=$row['id'];
if($row1['brandid'] == $brand_id)
	$sel="SELECTED";
else
	$sel="";
echo "<option value='$brand_id' $sel>" . fnStringToHTML($row['name']) . "</option>";
}
?>
                </select>
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <label class="col-md-3">Category Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
            <input type="text" name="categorynm" value="<?php echo fnStringToHTML($row1['categorynm'])?>" 
			placeholder="Enter Category Name"
            data-parsley-trigger="change"				
		    data-parsley-required="#true" 
		    data-parsley-required-message="Please enter category name"
			data-parsley-maxlength="50"
			data-parsley-maxlength-message="Only 50 characters are allowed"
			class="form-control">
              </div>
            </div><!-- /.form-group -->        
            <div class="form-group">
              <label class="col-md-3">Category Image:</label>

              <div class="col-md-4">
			  <?php if($row1['categoryimage']!=""){?>
			  <img src="upload/<?php echo COMPANYNM;?>_upload/<?php echo $row1['categoryimage']?>" alt="<?php echo $row1['categoryimage']?>" class="img-responsive"/> 
			  <?php }?>
			  </br>
         <input type="file" id="categoryimage" 
		 data-parsley-trigger="change"				
		 data-parsley-fileextension='png,jpg,jpeg' 
		 data-parsley-max-file-size="1000"  
		 name="categoryimage">
              </div>
            </div><!-- /.form-group -->
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                <a href="categories.php" class="btn btn-primary">Cancel</a>
                <!--<a data-toggle="modal" href="#thankyouModal" class="btn btn-primary">Delete</a>-->
              </div>
            </div><!-- /.form-group -->
          </form>  
   	  	  <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
				<h4 style="color:red; text-align:center;">Do you want to delete this record ?</h4>
				</p>                     
        	  <center><a href="categories_delete.php?id=<?php echo $row1['id']?>" ><button type="button" class="btn btn-success">Yes</button></a>
			  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
			  </center>
            </div>    
        </div>
    </div>
</div>
                         
<?php } ?>                          
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->

<script>
$(document).ready(function() {
    window.ParsleyValidator
        .addValidator('fileextension', function (value, requirement) {
            // the value contains the file path, so we can pop the extension
			var arrRequirement = requirement.split(',');			
            var fileExtension = value.split('.').pop();
			var result = jQuery.inArray( fileExtension, arrRequirement );
			 
			if(result==-1)
            return false;
			else 
			return true;
        }, 32)
        .addMessage('en', 'fileextension', 'Only png or jpg image are allowed');	
window.Parsley.addValidator('maxFileSize', {
  validateString: function(_value, maxSize, parsleyInstance) {
    var files = parsleyInstance.$element[0].files;
    return files.length != 1  || files[0].size <= maxSize * 1024;
  },
  requirementType: 'integer',
  messages: {
    en: 'Image should not be larger than %s Kb',
  }
});
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>