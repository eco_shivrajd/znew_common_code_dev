<!-- BEGIN HEADER -->
<?php 
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);

include "../includes/grid_header.php";
include "../includes/shopManage.php";
include "../includes/userManage.php";
include "../includes/testManage.php";
$shopObj 	= 	new shopManager($con,$conmain);
$testObj 	= 	new testManager($con,$conmain);
$userObj 	= 	new userManager($con,$conmain);

?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Shops";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<h3 class="page-title">Shops</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li><i class="fa fa-home"></i>
					<a href="#">Shops</a></li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				
				<div class="col-md-12">
				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
						
							<div class="caption">Shop Listing</div>	
							<?php if ($ischecked_add==1) { ?>
								<a style="margin-left: 5px;" href="shops-add.php" class="btn btn-sm btn-default pull-right mt5">Add Shop</a>
							<?php } ?>
&nbsp;&nbsp;
							 <button type="button" name="btnExcel" id="btnExcel" 
								onclick="ExportToExcel();" 
								 class="btn btn-sm btn-default pull-right mt5" 
								>Export to Excel</button> 
								
                            <div class="clearfix"></div>
						</div>
						<div class="portlet-body">


                        <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">
							<div class="form-group">
								<label class="col-md-3">User Name:</label>
								<div class="col-md-4">	
							    <select name="shop_added_by_user" id="shop_added_by_user" onchange="loadReport(1);" class="form-control">  
							    <option value="all" selected>All</option>   
									<?php  
									$seesion_user_id=$_SESSION[SESSION_PREFIX.'user_id'];
					        $sql = "SELECT  `shop_added_by`,`service_by_user_id`,shop_added_by_name,							service_by_user_id,(SELECT firstname from tbl_user where id=service_by_user_id) as service_by_user_name
						   FROM `tbl_shop_view` where find_in_set('".$seesion_user_id."',parent_ids) <> 0 
						   OR shop_added_by='$seesion_user_id' OR service_by_user_id='$seesion_user_id' GROUP BY shop_added_by_name";
                             $result = mysqli_query($con, $sql);
							 if(mysqli_num_rows($result)>0){
								 while($row = mysqli_fetch_array($result))
							{ ?>
						  <option value="<?php echo $row['shop_added_by_name'];?>">
								  <?php echo $row['shop_added_by_name'];?></option>
								 <?php  } } ?> 
                                </select>
								</div>
							</div><!-- /.form-group -->
						</form>	
						<hr>
                        <div class="portlet-body" id="shop_summary_details">
						</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div id="print_div"  style="display:none;">
<table class="table table-striped table-bordered table-hover" id="print_table">
<thead>
   <tr role="row">
      <th class="sorting" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
         Shop Name
         : activate to sort column ascending" style="width: 124px;">
         Shop Name
      </th>
	<th class="sorting" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
         Address
         : activate to sort column ascending" style="width: 124px;">
         Address
      </th>
      <th class="sorting_asc" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
         Shop Added By
         : activate to sort column ascending" style="width: 104px;" aria-sort="ascending">
         Shop Added By
      </th>
	  <th class="sorting_asc" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
         Shop Service By
         : activate to sort column ascending" style="width: 104px;" aria-sort="ascending">
         Shop Service By
      </th>
      <th class="sorting" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
         Contact Person
         : activate to sort column ascending" style="width: 112px;">
         Contact Person
      </th>
      <th class="sorting" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
         Mobile Number
         : activate to sort column ascending" style="width: 109px;">
         Mobile Number
      </th>
      <th class="sorting" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
         Subarea
         : activate to sort column ascending" style="width: 97px;">
         Subarea
      </th>
	  <th class="sorting" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
         Taluka
         : activate to sort column ascending" style="width: 97px;">
         Taluka
      </th>
      <th class="sorting" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
         District
         : activate to sort column ascending" style="width: 91px;">
         District
      </th>
      <th class="sorting" tabindex="0" aria-controls="sample_2" rowspan="1" colspan="1" aria-label="
         State
         : activate to sort column ascending" style="width: 117px;">
         State
      </th>      
   </tr>
</thead>
<tbody>
   
</tbody>
</table>
</div>
<form action="../includes/exportToExcel.php" method="post" name="export_excel" id="export_excel">
	<input type="hidden" name="export_data" id="export_data">
	<input type="hidden" name="file_name" id="file_name">
</form>
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->

<script>
function ConfirmDelete() {	
		return confirm("Are you sure you want to delete this shop?");
}
</script>
<script>
$(document).ready(function() {
	loadReport(1);
});
function loadReport(page) {
	var param = '';	
	var shop_added_by_user = $('#shop_added_by_user').val();
	param = param + '&shop_added_by_user='+shop_added_by_user;
		$.ajax
		({
			type: "POST",
			url: "ajax_shopaddedby.php",
			data: param,
			success: function(msg)
			{
			  console.log(msg);
			  $("#shop_summary_details").html(msg);
			}
		  });
	
}
	function deleteShop(shop_id) {
		if (confirm('Are you sure you want to delete this shop?')) {
			CallAJAX('ajax_product_manage.php?action=delete_shop&shop_id=' + shop_id );
		}
	}

	function shopStatus(shop_id,shop_status) {
		//alert(shop_status);
		if (shop_status == 0) 
		{
			 if (confirm('Are you sure you want to Inactivate this shop?')) 
				{
					CallAJAX1('ajax_product_manage.php?action=shop_inactive&shop_id=' + shop_id );
				}
		}
		if (shop_status == 1) 
		{
			 if (confirm('Are you sure you want to Activate this shop?')) 
				{
					CallAJAX2('ajax_product_manage.php?action=shop_active&shop_id=' + shop_id );
				}
		}
		
	}

	function CallAJAX(url) {
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) 
			{
				alert('Shop deleted successfully.');
				location.reload();
			}
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}
	function CallAJAX1(url) {
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) 
			{
				alert('Shop Inactivated successfully.');
				location.reload();
			}
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}
	function CallAJAX2(url) {
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) 
			{
				alert('Shop Activated successfully.');
				location.reload();
			}
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}
	function excel_call() {
		//CallAJAX('export_data.php');	
		alert('hi');
		window.location.href = "export_data.php";	
	}

</script>
</body>
<!-- END BODY -->
</html>