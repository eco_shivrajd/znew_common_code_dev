<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/orderManage.php";
include "../includes/userManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
$userObj 	= 	new userManager($con,$conmain);
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Reports"; $activeMenu = "Sales Person Attendance";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<h3 class="page-title">
			Sales Person Attendance
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li><i class="fa fa-home"></i>
						<a href="#">Sales Person Attendance</a>	
					</li>
				</ul>				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">                
					<div class="row">
				<div class="col-md-12"> 				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">Search Criteria</div>
                            <div class="clearfix"></div>
						</div>						
						<div class="portlet-body">							
							<form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">	
							<div class="form-group">
								<label class="col-md-3">Sales Person:</label>
								<?php $user_result = $userObj->getAllSP('SalesPerson'); ?>		
								<div class="col-md-4" id="divsalespersonDropdown">
									<select name="dropdownSalesPerson" id="dropdownSalesPerson" class="form-control">
										<option value="">-Select-</option>
										<?php while($row_user = mysqli_fetch_assoc($user_result))
										{ ?>									
										<option value="<?=$row_user['id'];?>"><?=$row_user['firstname'];?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						
							<div class="form-group">
								<label class="col-md-3">Date Options:</label>
								<div class="col-sm-4">						
									<select class="form-control" name="selTest" id="selTest" onChange="fnSelectionBoxTest()">
									<option value='5' <?php if($_REQUEST['selTest']=="5")echo 'selected';?>>All</option>
									<option value="4" <?php if($_REQUEST['selTest']=="4")echo 'selected';?>>Today</option>
									<option value='1' <?php if($_REQUEST['selTest']=="1")echo 'selected';?>>Weekly</option>
									<option value='2' <?php if($_REQUEST['selTest']=="2")echo 'selected';?>>Current month</option>
									<option value='3' <?php if($_REQUEST['selTest']=="3")echo 'selected';?>>From specific date</option>
									</select>
									<input type="hidden" name="hdnSelrange" id="hdnSelrange">
								</div>
							</div>
							<div class="form-group" id="date-show" style="display:none;">
								<label class="col-md-3">Date:</label>
								<div class="col-md-8 nopadl">
									<div class="col-md-1" class="nopadl" style="margin-right:-17px;">
										<label class="nopadl" style="padding-top:5px;">From:</label>
									</div>
									<div class="col-md-3">	
										<div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
											<input type="text" class="form-control" name="frmdate" id="frmdate" value="<?php echo $frmdate;?>" readonly >
											<span class="input-group-btn">
											<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>			
									<div class="col-md-1" class="nopadl" style="margin-right:-35px;margin-left:-8px;">
										<label class="nopadl" style="padding-top:5px">To:</label>
									</div>
									<div class="col-md-3">
										<div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
											<input type="text" class="form-control" name="todate" id="todate" value="<?php echo $todate;?>" readonly >
											<span class="input-group-btn">
											<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>						
								</div>                          
						</div>                          
					    <input type="hidden" id="order" name="order" value="asc">
						<input type="hidden" id="sort_complete" name="sort_complete" value="">
						<input type="hidden" id="page" name="page" value="">
						<input type="hidden" id="per_page" name="per_page" value=""> 	
						<input type="hidden" id="actionType" name="actionType" value=""> 
						<input type="hidden" id="search" name="search" value="">
						<div class="form-group">
								<div class="col-md-4 col-md-offset-3">
									<button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>
									<button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnChangeReportType('daily');">Reset</button>
								</div>
							</div><!-- /.form-group -->
					</div>
					<!-- END PAGE CONTENT-->
				</div>
					 <div class="clearfix"></div>   
				<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption"><i class="icon-puzzle"></i>SP Attendance Report</div>
								<button type="button" name="btnExcel" id="btnExcel" onclick="report_download();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
								&nbsp;
								<button type="button" name="btnPrint" id="btnPrint" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
							</div>	
						<div class="portlet-body" id="order_summary_details">
						</div>
					</form>	
			
			</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
</div>
</div>
</div>
</div>
<div style="display:none;" class="modal-backdrop fade in"></div>
<div aria-hidden="false" style="display: none;" id="basicModal" class="modal fade in">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick="close_modal();" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title">Order Details</h3>
      </div>
      <div class="modal-body">

<div id="ajax_list_div">
</div>
      </div>
       
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<div id="print_div"  style="display:none;"></div>
<div id="table_heading"  style="display:none;"><?=$user_data['firstname'];?>'s Shopwise <?=$report_title;?></div>
<form action="../includes/exportToExcel.php" method="post" name="export_excel" id="export_excel">
	<input type="hidden" name="export_data" id="export_data">
</form>
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>

$(document).ready(function() {
	ShowReport(1);
});
function fnChangeReportType(reportType) {
		$("#dropdownSalesPerson").val('');
		$("#selTest").val('');
		$('#date-show').hide();
	}
function fnSelectionBoxTest()
{
	if($('#selTest').val() == '3')
	{
	   $('#date-show').show();
	}
	else
	{
		$('#date-show').hide();
		//ShowReport(1);
	}
}
function ShowReport(page){
		//alert("dsfsd");
		var param = '';	
		var dropdownSalesPerson = $('#dropdownSalesPerson').val();
		param = param + 'dropdownSalesPerson='+dropdownSalesPerson;
		var selTest = $('#selTest').val();
		param = param + '&selTest='+selTest;
		if(selTest == 3)
		{
			var frmdate = $('#frmdate').val();
			param = param + '&frmdate='+frmdate;
			var todate = $('#todate').val();
			param = param + '&todate='+todate;
			var validation = compaire_dates(frmdate,todate);
		}
		//alert(param);
	  if(selTest == 3 && frmdate != '' && todate != '' && validation == 1)
	  {
		  alert("'From' date should not be greater than 'To' date.");
		  return false;
	  }
	  else
	  {
		$.ajax
		({
			type: "POST",
			url: "sales_person_attendance_detail.php",
			data: param,
			success: function(msg)
			{
			  $("#order_summary_details").html(msg);
			}
		  });
	  }
}

/*function report_download() {
	var filtered_data_count = $("#sample_2").DataTable().rows( { filter : 'applied'} ).nodes().length;
	var td_rec = $("#sample_2 td:last").html();
	if(td_rec != 'No matching records found')
	{
		var search = $('input[type="search"]').val();
		if(search !='')
			$('#search').val(search);
		else
			$('#search').val('');
		
		var sortInfo = $("#sample_2").dataTable().fnSettings().aaSorting;
		$("#sort_complete").val(sortInfo);	
		var table = $("#sample_2").DataTable().page.info();
		
		var rec_per_page = table.length;
		$("#per_page").val(rec_per_page);
		var page = table.page;
		$("#page").val(page);
		$("#actionType").val('excel');
		//return false;
		var url = "order_summary_detail.php";
		$("#frmsearch").attr("action", url);
		$('#frmsearch').submit();
	}else{
		alert("No matching records found");
	}
}*/
function report_download() {
	var td_rec = $("#sample_2 td:last").html();
	if(td_rec != 'No matching records found')
	{
		var divContents = $(".table-striped").html();
		$("#print_div").html('<table id="print_table" style="text-decoration:none;">'+divContents+'</table>');	
		var heading = $("#table_heading").html();
		$("#print_table tr th i").html("RS");	
		divContents =  $("#print_div").html();
		
		divContents = divContents.replace(/<\/*a.*?>/gi,'');	
		$("#export_data").val(divContents);
		document.forms.export_excel.submit();
	}else{
		alert("No matching records found");
	}
}

 $("#btnPrint").live("click", function () {
	var td_rec = $("#sample_2 td:last").html();
	if(td_rec != 'No matching records found')
	{
		var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
		
		var divContents1 = $(".table-scrollable").html();
		$("#print_div").html(divContents1);
		$("#print_div a").removeAttr('href');
		$("#sample_2 tr th i").html("");	
		var divContents = $("#print_div").html();
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write('<html><head><title>Report</title>');
		printWindow.document.write('<style>a{text-decoration: none; color:#333333;} #sample_2{margin:20 20 20 20px; width:700px}</style>');
		printWindow.document.write('<link   rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css"/>');
		printWindow.document.write('<link  rel="stylesheet" type="text/css" href="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>');
		
		if( navigator.userAgent.toLowerCase().indexOf('chrome') > -1 ){
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');	
			printWindow.focus();	
			setTimeout(function () {
				printWindow.print();
				//printWindow.close();
			}, 500);
		}else if(isIE == true){
			printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');	
			printWindow.focus();	
			printWindow.document.execCommand("print", false, null);
			//printWindow.close();
		}
		else{		
			printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');	
			printWindow.focus();	
			setTimeout(function () {				
				printWindow.print();
				//printWindow.close();
			}, 100);
		}
	}else{
		alert("No matching records found");
	}
});
</script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- END JAVASCRIPTS -->