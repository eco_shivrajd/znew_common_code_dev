<?php
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/orderManage.php";
include "../includes/commonManage.php";	
$commonObj 	= 	new commonManage($con,$conmain);
$commonObjctype 	= 	$commonObj->log_get_commonclienttype($con,$conmain);
$orderObj 	= 	new orderManage($con,$conmain);
$report_title = $orderObj->getReportTitleForShop();

$row = $orderObj->getAllOrders2();
if(!empty($row)){
	$out = array();
	foreach ($row as $key => $value){
		$countval=0;
		foreach ($value as $key2 => $value2){
			if($key2=='id'){
				 $index = $key2.'-'.$value2;
				if (array_key_exists($index, $out)){
					$out[$index]++;
				} else {
					$out[$index] = 1;
				}
			}
			
		}
	}
	$out1 = array_values($out);
}


//echo "sdfsd<pre>";print_r($row);
$colspan = "6";

?>
<? if($_POST["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<? }

 ?>

<table 
	class="table table-striped table-bordered table-hover table-highlight table-checkable" 
	
	name="sample_2121" id="sample_2121">


<thead>
<tr>
	<td colspan="<?=$colspan;?>" align="canter" class="gradeX even" style="text-align:center; font-weight:600;"><h4><b><?php if(!empty($report_title))echo $report_title; else echo "Sales Report All";?></b></h4></td>              
  </tr>
  <tr>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Month</th>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Product Name</th>
	<th data-filterable="false" data-sortable="false" data-direction="desc">Unit</th>
	<th data-filterable="false" data-sortable="false" data-direction="desc">Quantity</th>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Unit Cost</th>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Total Sales <i aria-hidden='true' class='fa fa-inr'></i></th>              
  </tr>
</thead>
<tbody>					
	<?php if(!empty($row)){
		$j=0;$temp=$row[0]["productname"];
		
		$sum=0;
				$gtotalq=0;$gtotalp=0;
				for($i=0;$i<count($row);$i++){	
				$product_varient_id = $row[$i]['product_varient_id'];
				$sqlprd="SELECT variant_1, variant_2 FROM `tbl_product_variant` WHERE id = '$product_varient_id' ";
				$resultprd = mysqli_query($con,$sqlprd);
				$rowprd = mysqli_fetch_array($resultprd);
				$exp_variant1 = $rowprd['variant_1'];										
				$imp_variant1= split(',',$exp_variant1);
				$exp_variant2 = $rowprd['variant_2']; $imp_variant2= split(',',$exp_variant2);
				$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
				$resultunit = mysqli_query($con,$sql);
				$rowunit = mysqli_fetch_array($resultunit);
				$variant_unit1 = $rowunit['unitname'];
				
				$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant2[1]'";
				$resultunit = mysqli_query($con,$sql);
				$rowunit = mysqli_fetch_array($resultunit);
				$variant_unit2 = $rowunit['unitname'];
				
					$units=$row[$i]['product_variant_weight1'].'-'.$row[$i]['unit'];
					$wq=$row[$i]['variantunit'];
			?>
				<tr class="odd gradeX">
				<?php
				if($temp==$row[$i]["productname"]){	
				$totalq=0;$totalp=0;
					?>
				<td rowspan='<?=$out1[$j]+1;?>' ><?php echo $row[$i]['month'];?></td>
				<td rowspan='<?=$out1[$j]+1;?>' ><?php echo $row[$i]['productname'];?></td>
				<?php $sum=$sum+$out1[$j]; $temp=$row[$sum]["productname"]; $j++;  } ?>
				<td align='left'><?php if(!empty($units))echo $units;else echo 0;?></td>
				<td align='right'><?php if(!empty($wq))echo $wq;else echo 0;?></td>
				<td align='right'><?php if(!empty($row[$i]['unitcost']))echo fnAmountFormat($row[$i]['unitcost']);else echo 0;?></td>
				<td align='right'><?php echo fnAmountFormat($row[$i]['p_cost_cgst_sgst']);?></td>
				</tr>	
					<? 
					$totalq=$totalq+$row[$i]["variantunit"];
					$totalp=$totalp+fnAmountFormat($row[$i]['p_cost_cgst_sgst']);
					if($i==($sum-1)){ 
					    $gtotalq=$gtotalq+$totalq; 
						$gtotalp=$gtotalp+$totalp;
					?>	
					<tr class="odd gradeX">
					
						<td align='right'><b> Total</b></td>
						<td align='right'><b><?= $totalq;?></b></td>
						<td align='right'><b>--</b></td>
						<td align='right'><b><?=fnAmountFormat($totalp);?></b></td>
					</tr>
						<?  } ?>	
					
			<?php	 } ?>
			<tr class="odd gradeX">
						<td align='right'><b>--</b></td>
						<td align='right'><b>--</b></td>
						<td align='right'><b>Grand Total</b></td>
						<td align='right'><b><?= $gtotalq;?></b></td>
						<td align='right'><b>--</b></td>
						<td align='right'><b><?=fnAmountFormat($gtotalp);?></b></td>
					</tr>
	<?php }
	else{
		echo "<tr><td colspan='6' align='center'>No matching records found</td></tr>";
	}
	if($_POST["actionType"]=="excel" &&  $row == 0) {
			echo "<tr><td>No matching records found</td></tr>";
		}
	?>	
			
</tbody>	
</table>

<!-- END JAVASCRIPTS -->
<?
if($_POST["actionType"]=="excel") {
	if($row != 0){
		header("Content-Type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=Shop_Summary_Report.xls");
		exit;
	}
} ?>
 