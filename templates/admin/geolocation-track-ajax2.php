<?php
include ("../../includes/config.php");
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
extract($_POST);
//echo "<pre>";print_r($_POST);
$sql = "SELECT GROUP_CONCAT(external_id) AS all_external_id 
FROM tbl_user WHERE id = '".$dropdownSalesPerson."'";
$resultid = mysqli_query($con,$sql); 
$extIdRow 	= mysqli_fetch_array($resultid); 
	


  $sqlshop ="
			SELECT DISTINCT(SV.shop_id), s.name AS shopname, o.lat, o.long, no_o_lat, no_o_long, s.latitude, s.longitude,				
			SV.shop_visit_date_time AS date, o.total_order_gst_cost, s.added_on,					
			SV.shop_visit_reason, SV.shop_close_reason_type, SV.shop_close_reason_details
			FROM tbl_shop_visit AS SV										
			LEFT JOIN tbl_shops s ON s.id= SV.shop_id
			LEFT JOIN tbl_orders o ON o.shop_id= SV.shop_id  AND o.order_date =SV.shop_visit_date_time
			WHERE date_format(SV.shop_visit_date_time, '%d-%m-%Y') = '".$frmdate."' 
			AND SV.salesperson_id = '".$dropdownSalesPerson."'
			AND SV.is_shop_location = '0' AND 
        ((o.lat is not null AND  o.long is not null) or (no_o_lat is not null AND  no_o_long is not null)
		or (s.latitude is not null AND  s.longitude is not null) )
			GROUP BY s.id
			ORDER BY SV.shop_visit_date_time ASC,case when date is null then 1 else 0 end, date";

$resultshop = mysqli_query($con,$sqlshop); 
$totalshops=mysqli_num_rows($resultshop);


 $sql_sp_location_travel ="SELECT `id`, `userid`, `lattitude`, `longitude`, `tdate`, `shop_id`
			FROM tbl_user_location 
			WHERE date_format(tdate, '%d-%m-%Y') = '".$frmdate."' AND userid = '".$dropdownSalesPerson."'
			and lattitude > 0 and lattitude is not null and longitude > 0 and longitude is not null
			ORDER BY id ASC";
$result_sp = mysqli_query($con,$sql_sp_location_travel); 
$total_sp_locations=mysqli_num_rows($result_sp);
$array_sp = array();
if($total_sp_locations > 0){
	
	$j=0;
	while($row_sp = mysqli_fetch_array($result_sp)) 
	{ 
		$array_sp[$j]['lat']=$row_sp["lattitude"];
		$array_sp[$j]['lng']=$row_sp["longitude"];
		$array_sp[$j]['title']="";		
		$j++;
	}
}

//echo "<pre>";print_r($array_sp);die();
?>

	<div class="portlet-body" style="height:500px;">
		<? $ar[] = array();$i=0;		
		if($totalshops>0){
			while($rowshop = mysqli_fetch_array($resultshop)){
				$lat = '';
				$long = '';
				$shop_visit = 0;
				if($rowshop["lat"] !='' && $rowshop["long"] !=''){
					$lat = $rowshop["lat"];
					$long = $rowshop["long"];
					$shop_visit = 1;
				}else if($rowshop["no_o_lat"] !='' && $rowshop["no_o_long"] !=''){
					$lat = $rowshop["no_o_lat"];
					$long = $rowshop["no_o_long"];
					$shop_visit = 1;
				}else if($rowshop["latitude"] !='' && $rowshop["longitude"] !=''){
					$lat = $rowshop["latitude"];
					$long = $rowshop["longitude"];
				}
				if($lat !='' && $long !=''){
					$ar[$i]['lat'] = $lat;
					$ar[$i]['lng'] = $long;
					$distance_between_two_address = "";
					if($i != 0 && $shop_visit == 1){
						$latitude1 = $ar[$i-1]['lat'];
						$longitude1 = $ar[$i-1]['lng'];
						$latitude2 = $lat;
						$longitude2 = $long;
						$distance = $orderObj->getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2);
						$distance_between_two_address = "\n Total distance travelled between ".$ar[$i-1]['shop']." and ".$rowshop["shopname"].' is '.$distance." KM";
					}
					
					$ar[$i]['shop']= $rowshop["shopname"]."";
					$ar[$i]['title']="Shop Name: ".$rowshop["shopname"]."".$distance_between_two_address;
					$shop_added_on = '';
					if($rowshop['added_on'] != '')
						$shop_added_on = date('d-m-Y', strtotime($rowshop['added_on']));
					$shop_visit_date_time = '';
					if($rowshop['shop_visit_date_time'] != '')
						$shop_visit_date_time = date('d-m-Y', strtotime($rowshop['shop_visit_date_time']));
					if($shop_added_on == $frmdate && $rowshop['total_order_gst_cost'] == 0){
						//not have order
						$ar[$i]['description']="New Shop added, currently no orders received";
						$ar[$i]['totalsale']=0;
						$ar[$i]['color'] = 'yellow';
						$ar[$i]['shop_type'] = 'new';
					}else if($shop_added_on == $frmdate && $rowshop['total_order_gst_cost'] > 0){
						//not have order
						$ar[$i]['description']="New Shop added, Total Sale: ".$rowshop['total_order_gst_cost'];
						$ar[$i]['totalsale']=$rowshop['total_order_gst_cost'];
						$ar[$i]['color'] = 'blue';
						$ar[$i]['shop_type'] = 'new';
					}else if(!empty($rowshop['total_order_gst_cost']) && $rowshop['total_order_gst_cost']!=0){
						$ar[$i]['description']="Total Sale: ".$rowshop['total_order_gst_cost'];
						$ar[$i]['totalsale']=$rowshop['total_order_gst_cost'];
						$ar[$i]['color'] = 'green';
						$ar[$i]['shop_type'] = 'old';
					}else if($shop_visit_date_time == $frmdate && $rowshop['total_order_gst_cost'] == 0){				
						$ar[$i]['description']="No orders ".$rowshop['shop_visit_reason']." ".$rowshop['shop_close_reason_type']." ".$rowshop['shop_close_reason_details'];
						$ar[$i]['totalsale']=0;
						$ar[$i]['color'] = 'red';
						$ar[$i]['shop_type'] = 'old';
					}else{
						$ar[$i]['description']="No order received";
						$ar[$i]['totalsale']=0;
						$ar[$i]['color'] = 'red';
						$ar[$i]['shop_type'] = 'old';
					}
				
				$i++;
				}
				//echo $lattitude."----".$longitude."*".$rowshop["id"]."*";
			}
		}
		
		$total_count=count($ar);
		$total_count_sp=count($array_sp);
		
		if($i == 0 && $total_count_sp==0){
		?>
		<div class="form-group">
			<div class="col-md-12">				
				<div  id="dvMap" style="min-height:480px">
				No Record available.
				</div>
			</div>
		</div>
		<?php exit; }else{ ?>
		<div class="form-group">
			<div class="col-md-12">
				
				 <div  id="dvMap" style="min-height:480px">
			</div>
		</div>
		</div>
		<?php } ?>

<script>
var markers=<?php echo json_encode($ar); ?>;
var markers_sp=<?php echo json_encode($array_sp); ?>;

var countertr_sp=<?php echo count($array_sp);?>;
var total_count=<?php echo $total_count;?>;
console.log(markers_sp);//alert(total_count);alert(countertr_sp);
function initMap() {
	drawingRoute();
}
function drawingRoute(){
    var mapOptions = {
        center: new google.maps.LatLng(markers_sp[0].lat, markers_sp[0].lng),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
    var infoWindow = new google.maps.InfoWindow();
    var lat_lng = new Array();
	 var lat_lng1 = new Array();
    var latlngbounds = new google.maps.LatLngBounds();
	for (i = 0; i < markers_sp.length; i++) {
        var data1 = markers_sp[i];
        var myLatlng1 = new google.maps.LatLng(data1.lat, data1.lng);
		lat_lng1.push(myLatlng1);
	}
	var count_new=0;
		//console.log(markers_sp);
    for (i = 0; i < (total_count+2); i++) {
    	var data;
		if(i == 0){
			data = markers_sp[i];//alert("if");
		}else if(i == (total_count+1)) {
			var last_sp_key=countertr_sp-1;//alert("else if");
			data = markers_sp[last_sp_key];	
		}else{
			data= markers[count_new];//alert("else");
			count_new++;
		}  
		console.log("start");
		console.log(data);
		console.log("end");
		var myLatlng = new google.maps.LatLng(data.lat, data.lng);
        lat_lng.push(myLatlng);		 	 
			if(i == 0){
				//alert(i);
			var marker = new google.maps.Marker({
				position: {lat: parseFloat(data.lat), lng: parseFloat(data.lng)},
				map: map,
				label:'S',
				title: data.title
				});
			}else if(i == (total_count+1)) {
				//alert(i);
				var marker = new google.maps.Marker({
				position: {lat: parseFloat(data.lat), lng: parseFloat(data.lng)},
				map: map,
				label:'D',
				title: data.title
				});
			}else {
				if (data.totalsale>0) {
				  var icon = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
				}else{ 
				   var icon = 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png';
				} 
				var marker = new google.maps.Marker({
				position: {lat: parseFloat(data.lat), lng: parseFloat(data.lng)},
				map: map,
				title: data.title,
				icon: icon
				});
			}
			//console.log(lat_lng);
        latlngbounds.extend(marker.position);
        (function (marker, data) {
            google.maps.event.addListener(marker, "click", function (e) {
                infoWindow.setContent(data.description);
                infoWindow.open(map, marker);
            });
        })(marker, data);
    }
	
    map.setCenter(latlngbounds.getCenter());
    map.fitBounds(latlngbounds);

    //***********ROUTING****************//

    //Initialize the Direction Service
    var service = new google.maps.DirectionsService();
	//this is for salesperson traveling line draw
	   
	
	
	for (var i = 0; i < lat_lng1.length; i++) {
        if ((i + 1) < lat_lng1.length) {
            var src = lat_lng1[i];
            var des = lat_lng1[i + 1];
            service.route({
                origin: src,
                destination: des,
                travelMode: google.maps.DirectionsTravelMode.TRANSIT
            }, function (result, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    //Initialize the Path Array
                    var path = new google.maps.MVCArray();

                    //Set the Path Stroke Color
                    var poly = new google.maps.Polyline({
                        map: map,
                        strokeColor: '#000080'
                    });
                    poly.setPath(path);
                    for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                        path.push(result.routes[0].overview_path[i]);
                    }
                }
            });
        }
    }
  }//end outer
   
</script>
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDMpdO43txdg_zyovvZm9i1tMMFIQTKTU&callback=initMap"
        async defer></script>
