<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/testManage.php";
include "../includes/userConfigManage.php";
$testObj 	= 	new testManager($con,$conmain);
$userconfObj 	= 	new userConfigManager($con,$conmain);
?>
<!-- END HEADER -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
<script type="text/javascript">
    $(function() {
  $('.selectpicker').selectpicker();
  alert('ji');
});
</script> -->

<link rel="stylesheet" href="../../assets/search-select/style.css">
<script src="../../assets/search-select/jquery.js"></script>
<script src="../../assets/search-select/choosen.js"></script>

<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php
    $activeMainMenu = "ManageSupplyChain"; $activeMenu = "User List";
    include "../includes/sidebar.php";
    ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->           
            <h3 class="page-title">
            User List
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="#">User List</a>  
                    </li>
                </ul>               
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">                
                    <div class="row">
                <div class="col-md-12">                 
                    <div class="portlet box blue-steel">
                        <div class="portlet-title">
                            <div class="caption">Search Criteria</div>                          
                            <div class="clearfix"></div>
                        </div>                      
                        <div class="portlet-body">  
                              <?php ?>               
                            <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">                               
                            <div class="form-group">
                                <label class="col-md-3">User Type:</label>
                                <div class="col-md-4"> 

<select name="user_type" id="user_type" onchange="loadReport();" class="chosen" style="width: 100%;font-size: 14px;">


<?php  
    $seesion_user_type=$_SESSION[SESSION_PREFIX.'user_type'];
    $seesion_user_id=$_SESSION[SESSION_PREFIX.'user_id'];
 $result1 = $testObj->getUsertype_underme_byuserid($seesion_user_id);
 //echo "<pre>";print_r($result1);
 if($result1!=0){
 while ($row = mysqli_fetch_array($result1)){ ?>
  <option value="<?php echo $row['user_type'].",".$row['user_role'];?>" ><?php echo $row['user_type'];?></option>
 <?php  } }else{ ?>
     <option value="" >--Select--</option>
 <?php } ?> 
</select>


                                </div>
                            </div><!-- /.form-group --> 
                           
                                                 
                        <input type="hidden" id="order" name="order" value="asc">
                        <input type="hidden" id="sort_complete" name="sort_complete" value="">
                        <input type="hidden" id="page" name="page" value="">
                        <input type="hidden" id="per_page" name="per_page" value="">    
                        <input type="hidden" id="actionType" name="actionType" value=""> 
                        <input type="hidden" id="search" name="search" value="">                        
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                     <div class="clearfix"></div>   
                <div class="portlet box blue-steel"  >
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-puzzle"></i>User List</div>
                            <?php if ($ischecked_add==1) { ?>
                              <a class="btn btn-sm btn-default pull-right mt5" href="add-user-config.php">
                                    Add User
                                </a>
                            <?php } ?>
                            </div>  
                        <div class="portlet-body" id="user_summary_details">
                        </div>
                    </form>             
				</div>
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    
    <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
</div>
</div>
</div>
</div>

 <div class="modal fade" id="user_details" role="dialog">
        <div class="modal-dialog" style="width: 880px !important;">
            <!-- Modal content-->
            <div class="modal-content" id="user_details_content">
            </div>
        </div>
    </div>



<div style="display:none;" class="modal-backdrop fade in"></div>
<div aria-hidden="false" style="display: none;" id="basicModal" class="modal fade in">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick="close_modal();" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title">User Details</h3>
      </div>
      <div class="modal-body">

<div id="ajax_list_div">
</div>
      </div>
       
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<div id="print_div"  style="display:none;"></div>
<div id="table_heading"  style="display:none;"><?=$user_data['firstname'];?>'s Shopwise <?=$user_type_title;?></div>
<form action="../includes/exportToExcel.php" method="post" name="export_excel" id="export_excel">
    <input type="hidden" name="export_data" id="export_data">
</form>
<!-- BEGIN FOOTER -->
<script type="text/javascript">
$(".chosen").chosen();
$(document).ready(function() {
    loadReport(1);
});
function loadReport() {   
    var user_type = $('#user_type').val();   
   //  alert(user_type);
    $.ajax
    ({
        type: "POST",
        url: "user_summary_details.php",
        data: 'user_type='+user_type,
        success: function(msg)
        {
            //console.log(msg);
          $("#user_summary_details").html(msg);
        }
      });  
}
</script>
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->


<script>

/*$(document).ready(function() {
    loadReport(1);
});

function loadReport(page) {   
    var user_type = $('#user_type').val();   
    $.ajax
    ({
        type: "POST",
        url: "user_summary_details.php",
        data: 'user_type='+user_type,
        success: function(msg)
        {
			//console.log(msg);
          $("#user_summary_details").html(msg);
        }
      });  
}*/

 function showUserDetails(id, user_role) {
   // alert(id);
   // alert(user_type);
            var url = "user_details_popup.php";
            jQuery.ajax({
                url: url,
                method: 'POST',
                data: 'user_details_id=' + id + '&user_role=' + user_role,
                async: false
            }).done(function (response) {
				console.log(response);
                $('#user_details_content').html(response);
                $('#user_details').modal('show');
            }).fail(function () { });
            return false;
        }

function report_download() {
    var td_rec = $("#sample_2 td:last").html();
    if(td_rec != 'No matching records found')
    {
        var divContents = $(".table-striped").html();
        $("#print_div").html('<table id="print_table" style="text-decoration:none;">'+divContents+'</table>');  
        var heading = $("#table_heading").html();
        $("#print_table tr th i").html("RS");   
        divContents =  $("#print_div").html();
        divContents = divContents.replace(/₹/g,'Rs');
        divContents = divContents.replace(/<\/*a.*?>/gi,'');    
        $("#export_data").val(divContents);
        document.forms.export_excel.submit();
    }else{
        alert("No matching records found");
    }
}


</script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- END JAVASCRIPTS -->