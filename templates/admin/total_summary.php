<!-- BEGIN HEADER -->
<?php 
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include "../includes/grid_header.php";
include "../includes/orderManage.php";
include "../includes/testManage.php";
include "../includes/userConfigManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
$testObj 	= 	new testManager($con,$conmain);
$userconfObj 	= 	new userConfigManager($con,$conmain);
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Reports"; $activeMenu = "OrderSummary";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<h3 class="page-title">
			Summary
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Summary</a>	
					</li>
				</ul>				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">                
					<div class="row">
				<div class="col-md-12"> 				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">Search Criteria</div>							
                            <div class="clearfix"></div>
						</div>						
						<div class="portlet-body">							
							<form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">
							<div class="form-group">
								<label class="col-md-3">Report Type:</label>
								<div class="col-md-6">
									<input type="radio" name="reportType" id="reportType_Rolewise" value="Rolewise" checked onclick="fnChangeReportType('Rolewise');"> Rolewise 
									&nbsp;&nbsp;
									<input type="radio" name="reportType" id="reportType_Utypewise" value="Typewise"  onclick="fnChangeReportType('Typewise');"> Typewise 
									&nbsp;&nbsp;
									<input type="radio" name="reportType" id="reportType_Productwise" value="Productwise" onclick="fnChangeReportType('Productwise');"> Productwise 
									&nbsp;&nbsp;
									<input type="radio" name="reportType" id="reportType_Shopwise" value="Shopwise" onclick="fnChangeReportType('Shopwise');"> Shopwise
								</div>
							</div><!-- /.form-group -->
							<div class="form-group" id="userrole_div">
								<label class="col-md-3">User Role:</label>
								<div class="col-md-4">	
							    <select name="report_type_role" id="report_type_role" onchange="loadReport(1);" class="form-control">
									<?php  
									$seesion_user_type=$_SESSION[SESSION_PREFIX.'user_type'];
									$seesion_user_id=$_SESSION[SESSION_PREFIX.'user_id'];
								 $result1 = $testObj->getUserrole_underme_byuseridrole($seesion_user_id);
								// echo "<pre>";print_r($result1);
								 while ($row = mysqli_fetch_array($result1)){ 
								 if($row['user_role']!='Shopkeeper'){ ?>
								  <option value="<?php echo $row['user_role'];?>"><?php echo $row['user_role'];?></option>
								 <?php } } ?> 
                                </select>
								</div>
							</div><!-- /.form-group -->	
							<div class="form-group" id="usertype_div" style="display:none;">
								<label class="col-md-3">User Type:</label>
								<div class="col-md-4">	
							    <select name="report_type_usertype" id="report_type_usertype" onchange="loadReport(1);" class="form-control">     
									<?php  
									$seesion_user_type=$_SESSION[SESSION_PREFIX.'user_type'];
									$seesion_user_id=$_SESSION[SESSION_PREFIX.'user_id'];
								 $result1 = $testObj->getUsertype_underme_byuseridrole($seesion_user_id);
								// echo "<pre>";print_r($result1);
								 while ($row = mysqli_fetch_array($result1)){ ?>
								  <option value="<?php echo $row['user_type'].",".$row['user_role'];?>"><?php echo $row['user_type'];?></option>
								 <?php  } ?> 
                                </select>
								</div>
							</div><!-- /.form-group -->	
							
							
							<div class="form-group">
								<label class="col-md-3">Date Options:</label>
								<div class="col-sm-4">						
									<select class="form-control" name="selTest" id="selTest" onChange="fnSelectionBoxTest()">
									<option value='5' <?php if($_REQUEST['selTest']=="5")echo 'selected';?>>All</option>
									<option value="4" <?php if($_REQUEST['selTest']=="4")echo 'selected';?>>Today</option>
									<option value='1' <?php if($_REQUEST['selTest']=="1")echo 'selected';?>>Weekly</option>
									<option value='2' <?php if($_REQUEST['selTest']=="2")echo 'selected';?>>Current month</option>
									<option value='3' <?php if($_REQUEST['selTest']=="3")echo 'selected';?>>From specific date</option>
									</select>
									<input type="hidden" name="hdnSelrange" id="hdnSelrange">
								</div>
							</div>
							<div class="form-group" id="date-show" style="display:none;">
								<label class="col-md-3">Date:</label>
								<div class="col-md-8 nopadl">
									<div class="col-md-1" class="nopadl" style="margin-right:-17px;">
										<label class="nopadl" style="padding-top:5px;">From:</label>
									</div>
									<div class="col-md-3">										
										<div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
											<input type="text" class="form-control" name="frmdate" id="frmdate" value="<?php echo $frmdate;?>" readonly onchange="callLocal();">
											<span class="input-group-btn">
											<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>			
									<div class="col-md-1" class="nopadl" style="margin-right:-35px;margin-left:-8px;">
										<label class="nopadl" style="padding-top:5px">To:</label>
									</div>
									<div class="col-md-3">
										<div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
											<input type="text" class="form-control" name="todate" id="todate" value="<?php echo $todate;?>" readonly onchange="callLocal();">
											<span class="input-group-btn">
											<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>						
								</div>                          
						</div>                          
					    <input type="hidden" id="order" name="order" value="asc">
						<input type="hidden" id="sort_complete" name="sort_complete" value="">
						<input type="hidden" id="page" name="page" value="">
						<input type="hidden" id="per_page" name="per_page" value=""> 	
						<input type="hidden" id="actionType" name="actionType" value=""> 
						<input type="hidden" id="search" name="search" value=""> 						
					</div>
					<!-- END PAGE CONTENT-->
				</div>
					 <div class="clearfix"></div>   
				<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption"><i class="icon-puzzle"></i>Report Summary</div>
								<button type="button" name="btnExcel" id="btnExcel" onclick="report_download();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
								&nbsp;
								<button type="button" name="btnPrint" id="btnPrint" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
							</div>	
						<div class="portlet-body" id="order_summary_details">
						</div>
					</form>	
			
			</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
</div>
</div>
</div>
</div>
<div style="display:none;" class="modal-backdrop fade in"></div>
<div aria-hidden="false" style="display: none;" id="basicModal" class="modal fade in">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick="close_modal();" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title">Order Details</h3>
      </div>
      <div class="modal-body">

<div id="ajax_list_div">
</div>
      </div>
       
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<div id="print_div"  style="display:none;"></div>
<div id="table_heading"  style="display:none;"><?=$user_data['firstname'];?>'s Shopwise <?=$report_title;?></div>
<form action="../includes/exportToExcel.php" method="post" name="export_excel" id="export_excel">
	<input type="hidden" name="export_data" id="export_data">
</form>
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
function callLocal(){
	loadReport(1);
}
$(document).ready(function() {
	loadReport(1);
});
function fnSelectionBoxTest(){
	if($('#selTest').val() == '3'){
	   $('#date-show').show();
	}else{
		$('#date-show').hide();
		loadReport(1);
	}
}
function fnChangeReportType(reportType) {
	$("#divReportHTML").hide();
	switch(reportType) {
		case "Rolewise":
			$("#userrole_div").show();			
			$("#usertype_div").hide();			
			break;
		case "Typewise":
			$("#userrole_div").hide();			
			$("#usertype_div").show();			
			break;
		case "Productwise":
			$("#userrole_div").hide();	
			$("#usertype_div").hide();
			$("#report_type_role").val('Admin');			
			$("#report_type_usertype").val('Admin,Admin');			
			break;
		case "Shopwise":
			$("#userrole_div").hide();
			$("#usertype_div").hide();
			$("#report_type_role").val('Admin');
			$("#report_type_usertype").val('Admin,Admin');
			break;
	}
	loadReport(1);
}
function loadReport(page) {
	var param = '';	
	var report_type_role = $('#report_type_role').val();
	var report_type_usertype = $('#report_type_usertype').val();
	var reportType = $("input[name=reportType]:checked").val();
	param = param + '&report_type_role='+report_type_role;
	param = param + '&report_type_usertype='+report_type_usertype;
	param = param + '&reportType='+reportType;
	var selTest = $('#selTest').val();
	param = param + '&selTest='+selTest;
	
	if(selTest == 3)
	{
		var frmdate = $('#frmdate').val();
		param = param + '&frmdate='+frmdate;
		var todate = $('#todate').val();
		param = param + '&todate='+todate;
		var validation = compaire_dates(frmdate,todate);
	}
	if(selTest == 3 && frmdate != '' && todate != '' && validation == 1){
		  alert("'From' date should not be greater than 'To' date.");
		  return false;
	  }else{
		$.ajax
		({
			type: "POST",
			url: "total_summary_detail.php",
			data: param,
			success: function(msg)
			{
				console.log(msg);
			  $("#order_summary_details").html(msg);
			}
		  });
	}
}

function report_download() {
	var td_rec = $("#sample_2 td:last").html();
	if(td_rec != 'No matching records found')
	{
		var divContents = $(".table-striped").html();
		$("#print_div").html('<table id="print_table" style="text-decoration:none;">'+divContents+'</table>');	
		var heading = $("#table_heading").html();
		$("#print_table tr th i").html("RS");	
		divContents =  $("#print_div").html();
		divContents = divContents.replace(/₹/g,'Rs');
		divContents = divContents.replace(/<\/*a.*?>/gi,'');	
		$("#export_data").val(divContents);
		document.forms.export_excel.submit();
	}else{
		alert("No matching records found");
	}
}

 $("#btnPrint").live("click", function () {
	var td_rec = $("#sample_2 td:last").html();
	if(td_rec != 'No matching records found')
	{
		var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
		
		var divContents1 = $(".table-scrollable").html();
		$("#print_div").html(divContents1);
		$("#print_div a").removeAttr('href');
		//$("#sample_2 tr th i").html("₹");	
		var divContents = $("#print_div").html();
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write('<html><head><title>Report</title>');
		printWindow.document.write('<style>a{text-decoration: none; color:#333333;} #sample_2{margin:20 20 20 20px; width:700px}</style>');
		printWindow.document.write('<link   rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css"/>');
		printWindow.document.write('<link  rel="stylesheet" type="text/css" href="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>');
		
		if( navigator.userAgent.toLowerCase().indexOf('chrome') > -1 ){
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');	
			printWindow.focus();	
			setTimeout(function () {
				printWindow.print();
				//printWindow.close();
			}, 500);
		}else if(isIE == true){
			printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');	
			printWindow.focus();	
			printWindow.document.execCommand("print", false, null);
			//printWindow.close();
		}
		else{		
			printWindow.document.write('<style type="text/css">table{border-spacing: 0; border-collapse: separate;}table th, table td { border:1px solid #ddd; vertical-align: top; padding: 8px;}</style>');
			printWindow.document.write('</head><body >');
			printWindow.document.write(divContents);
			printWindow.document.write('</body></html>');	
			printWindow.focus();	
			setTimeout(function () {				
				printWindow.print();
				//printWindow.close();
			}, 100);
		}
	}else{
		alert("No matching records found");
	}
});
</script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- END JAVASCRIPTS -->