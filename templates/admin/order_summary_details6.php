<?php
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
//
$report_type=$_POST['report_type'];
$selTest=$_POST['selTest'];

$report_title = $orderObj->getReportTitle_new($user_role);
$row = $orderObj->getAllOrdersSummaryByLocation($user_role,$user_type); 
?>
<? if($_POST["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<? } ?>
<table 
	class="table table-striped table-bordered table-hover table-highlight table-checkable" 
	data-provide="datatable" 
	data-display-rows="10"
	data-info="true"
	data-search="true"
	data-length-change="true"
	data-paginate="true"
	id="sample_2">
<thead>
<tr>
	<td colspan="<?=$colspan;?>" align="canter" class="gradeX even" style="text-align:center; font-weight:600;"><h4><b><?=ucfirst($report_title);?></b></h4></td>              
  </tr>
  <tr>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Name</th>
	
	<th data-filterable="false" data-sortable="true" data-direction="desc">Total Sales ₹</th>              
  </tr>
</thead>
<tbody>					
	<?php 
			foreach($row as $key => $val) { ?>
				<tr class="odd gradeX">
					<td><?=$val['Name'];?></td>			
					<td align='right'><?php echo fnAmountFormat($val['Total_Sales']); ?></td>									
				</tr>			
	<?php	} 
		if($_POST["actionType"]=="excel" &&  $row == 0) {
			echo "<tr><td>No matching records found</td></tr>";
		}
	?>	
			
</tbody>	
</table>
<script>
jQuery(document).ready(function() {    
   
   ComponentsPickers.init();
});

jQuery(document).ready(function() { 
	TableManaged.init();
});
$(document).ready(function() {
      var table = $('#sample_2').dataTable();
      // Perform a filter
      table.fnFilter('');
      // Remove all filtering
      //table.fnFilterClear();
	   
  });
</script>
<!-- END JAVASCRIPTS -->
<?
if($_POST["actionType"]=="excel") {
	if($row != 0){
		header("Content-Type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=Report_summary.xls");
		exit;
	}
} ?>
 